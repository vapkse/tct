declare module tct {
    interface UsageViewerOptions {
        tubeData: TubeData;
        usageIndex: number;
        graphIndex: number;
    }
    class UsageViewer {
        private options;
        private backdrop;
        private window;
        private element;
        private chart;
        private _trigger;
        private _create();
        protected oncreated: () => void;
        refresh(): void;
        setOptions(options: UsageViewerOptions): UsageViewer;
        show(): UsageViewer;
        hide(): UsageViewer;
    }
}
interface JQuery {
    usageViewer(): JQuery;
    usageViewer(method: string): tct.UsageViewer;
    usageViewer(method: 'show'): tct.UsageViewer;
    usageViewer(method: 'hide'): tct.UsageViewer;
    usageViewer(options: tct.UsageViewerOptions): JQuery;
    usageViewer(method: string, options: tct.UsageViewerOptions): tct.UsageViewer;
    usageViewer(method: 'setOptions', options: tct.UsageViewerOptions): tct.UsageViewer;
}

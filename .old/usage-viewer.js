'use strict';
var tct;
(function (tct) {
    var UsageViewer = (function () {
        function UsageViewer() {
            this.oncreated = function () {
                var self = this;
                var e = jQuery.Event("created");
                self._trigger('created', e);
            };
        }
        UsageViewer.prototype._create = function () {
            var self = this;
            var options = this.options;
            // Resource need to be renderized, so the call must be like this
            $.get('/resource?r=widgets%2Fusage-viewer.html').done(function (html) {
                self.window = $(html).appendTo(self.element);
                self.window.find('.btn-close').on('click', function () {
                    self.hide();
                });
                self.backdrop = $('<div id="usage-viewer-backdrop" class="backdrop bg-darker"></div>').appendTo('body').on('click', function () {
                    self.hide();
                });
                self.refresh();
                self.oncreated();
            });
        };
        ;
        UsageViewer.prototype.refresh = function () {
            var self = this;
            var $chart = self.window.find('#chart');
            var usage = self.options.tubeData.usages[self.options.usageIndex];
            var graphId = usage.traces[self.options.graphIndex];
            /*if (self.options.graphData) {
                $chart.show();
                var options: AnodeTransfertChartOptions = {
                    graphData: self.options.graphData,
                    title: self.options.graphTitle,
                    height: '100%'
                }
                if (!self.chart) {
                    requirejs(['anodeChartTransfert'], function() {
                        self.chart = $chart.anodeChartTransfert(options);
                    })
                } else {
                    self.chart.anodeChartTransfert('setOptions', options);
                }
            }*/
        };
        UsageViewer.prototype.setOptions = function (options) {
            var self = this;
            self.options = options;
            self.refresh();
            setTimeout(function () {
                self.show();
            }, 1);
            return self;
        };
        UsageViewer.prototype.show = function () {
            var self = this;
            if (self.backdrop) {
                self.backdrop.show();
            }
            self.window.css('opacity', 0);
            self.window.show();
            self.window.animate({ opacity: 1 }, 500);
            return self;
        };
        UsageViewer.prototype.hide = function () {
            var self = this;
            self.window.animate({ opacity: 0 }, 200, function () {
                self.backdrop.hide();
                self.window.hide();
            });
            return self;
        };
        return UsageViewer;
    })();
    tct.UsageViewer = UsageViewer;
})(tct || (tct = {}));
$.widget("tct.usageViewer", new tct.UsageViewer());

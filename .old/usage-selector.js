'use strict';
var tct;
(function (tct) {
    var UsageSelector = (function () {
        function UsageSelector() {
            this.oncreated = function () {
                var self = this;
                var e = jQuery.Event('created');
                self._trigger('created', e);
            };
        }
        UsageSelector.prototype._create = function () {
            var self = this;
            var options = this.options;
            $.get('/resource?r=widgets%2Fusage-selector.html').done(function (html) {
                self.window = $(html).appendTo(self.element);
                self.window.find('.btn-close').on('click', function () {
                    self.hide();
                });
                self.backdrop = $('<div id="doc-selector-backdrop" class="backdrop bg-darker"></div>').appendTo('body').on('click', function () {
                    self.hide();
                });
                self.window.find('#title').html(options.title);
                self.usageContainer = self.window.find('#usages');
                self.template = self.window.find('#usageTemplate').html();
                self.refresh();
                self.oncreated();
            });
        };
        ;
        UsageSelector.prototype.refresh = function () {
            var self = this;
            self.usageContainer.empty();
            self.window.find('#tname').text(' - ' + self.options.tubeData.name);
            self.options.tubeData.usages.map(function (usage, index) {
                for (var d = 0; d < usage.traces.length; d++) {
                    $(self.template).appendTo(self.usageContainer).attr('data-index', index).attr('graph-index', d).find('#label').text(usage.name + ' trace' + (d + 1));
                }
            });
            self.usageContainer.on('click', function (e) {
                var target = $(e.target).closest('[data-click]');
                var func = target.attr('data-click');
                switch (func) {
                    case 'view':
                        var $dataIndex = target.closest('[data-index]');
                        var dataIndex = parseInt($dataIndex.attr('data-index'));
                        var graphIndex = parseInt($dataIndex.attr('graph-index'));
                        var usageViewerOptions = {
                            usageIndex: dataIndex,
                            graphIndex: graphIndex,
                            tubeData: self.options.tubeData
                        };
                        if (!self.usageViewer) {
                            requirejs(['usageViewer'], function () {
                                self.usageViewer = $('[usage-viewer]');
                                self.usageViewer.usageViewer(usageViewerOptions).bind('fileviewercreated', function () {
                                    self.usageViewer.usageViewer('show');
                                });
                            });
                        }
                        else {
                            self.usageViewer.usageViewer('setOptions', usageViewerOptions);
                        }
                        break;
                }
            });
        };
        UsageSelector.prototype.setOptions = function (options) {
            var self = this;
            self.options = options;
            self.refresh();
            setTimeout(function () {
                self.show();
            }, 1);
            return self;
        };
        UsageSelector.prototype.show = function () {
            var self = this;
            if (self.backdrop) {
                self.backdrop.show();
            }
            self.window.css('opacity', 0);
            self.window.show();
            self.window.animate({ opacity: 1 }, 500);
            return self;
        };
        UsageSelector.prototype.hide = function () {
            var self = this;
            self.window.animate({ opacity: 0 }, 200, function () {
                self.backdrop.hide();
                self.window.hide();
            });
            return self;
        };
        return UsageSelector;
    })();
    tct.UsageSelector = UsageSelector;
})(tct || (tct = {}));
$.widget("tct.usageSelector", new tct.UsageSelector());

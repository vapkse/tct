'use strict';

module tct {
    export interface UsageViewerOptions {
        tubeData: TubeData,
        usageIndex: number,
        graphIndex: number
    }

    export class UsageViewer {
        private options: UsageViewerOptions;
        private backdrop: JQuery;
        private window: JQuery;
        private element: JQuery;
        private chart: JQuery;
        private _trigger: (eventName: string, event: JQueryEventObject) => void;
        private _create() {
            var self = <UsageViewer>this;

            var options = <UsageViewerOptions>this.options;

            // Resource need to be renderized, so the call must be like this
            $.get('/resource?r=widgets%2Fusage-viewer.html').done(function(html) {
                self.window = $(html).appendTo(self.element);

                self.window.find('.btn-close').on('click', function() {
                    self.hide();
                })

                self.backdrop = $('<div id="usage-viewer-backdrop" class="backdrop bg-darker"></div>').appendTo('body').on('click', function() {
                    self.hide();
                })

                self.refresh();
                self.oncreated();
            });
        };

        protected oncreated = function() {
            var self = <UsageViewer>this;
            var e = jQuery.Event("created");
            self._trigger('created', e);
        }

        public refresh() {
            var self = <UsageViewer>this;
            var $chart = self.window.find('#chart');
            var usage = self.options.tubeData.usages[self.options.usageIndex];
            var graphId = usage.traces[self.options.graphIndex];
            
            /*if (self.options.graphData) {
                $chart.show();
                var options: AnodeTransfertChartOptions = {
                    graphData: self.options.graphData,
                    title: self.options.graphTitle,
                    height: '100%'
                }
                if (!self.chart) {
                    requirejs(['anodeChartTransfert'], function() {
                        self.chart = $chart.anodeChartTransfert(options);
                    })
                } else {
                    self.chart.anodeChartTransfert('setOptions', options);
                }
            }*/
        }

        public setOptions(options: UsageViewerOptions) {
            var self = <UsageViewer>this;
            self.options = options;
            self.refresh();
            setTimeout(function() {
                self.show();
            }, 1);
            return self;
        }

        public show() {
            var self = <UsageViewer>this;
            if (self.backdrop) {
                self.backdrop.show();
            }
            self.window.css('opacity', 0);
            self.window.show();
            self.window.animate({ opacity: 1 }, 500);
            return self;
        }

        public hide() {
            var self = <UsageViewer>this;
            self.window.animate({ opacity: 0 }, 200, function() {
                self.backdrop.hide();
                self.window.hide();
            });
            return self;
        }
    }
}

$.widget("tct.usageViewer", new tct.UsageViewer());

interface JQuery {
    usageViewer(): JQuery;
    usageViewer(method: string): tct.UsageViewer;
    usageViewer(method: 'show'): tct.UsageViewer;
    usageViewer(method: 'hide'): tct.UsageViewer;
    usageViewer(options: tct.UsageViewerOptions): JQuery;
    usageViewer(method: string, options: tct.UsageViewerOptions): tct.UsageViewer;
    usageViewer(method: 'setOptions', options: tct.UsageViewerOptions): tct.UsageViewer;
}
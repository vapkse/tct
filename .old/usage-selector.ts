'use strict';

module tct {
    export interface UsageSelectorOptions {
        tubeData: TubeData,
        title: string,
    }

    export class UsageSelector {
        private options: UsageSelectorOptions;
        private backdrop: JQuery;
        private window: JQuery;
        private usageViewer: JQuery;
        private template: string;
        private usageContainer: JQuery;
        private element: JQuery;
        private _trigger: (eventName: string, event: JQueryEventObject) => void;
        private _create() {
            var self = <UsageSelector>this;

            var options = <UsageSelectorOptions>this.options;

            $.get('/resource?r=widgets%2Fusage-selector.html').done(function(html) {
                self.window = $(html).appendTo(self.element);

                self.window.find('.btn-close').on('click', function() {
                    self.hide();
                })

                self.backdrop = $('<div id="doc-selector-backdrop" class="backdrop bg-darker"></div>').appendTo('body').on('click', function() {
                    self.hide();
                })

                self.window.find('#title').html(options.title);

                self.usageContainer = self.window.find('#usages');
                self.template = self.window.find('#usageTemplate').html();

                self.refresh();
                self.oncreated();
            })
        };

        protected oncreated = function() {
            var self = <UsageSelector>this;
            var e = jQuery.Event('created');
            self._trigger('created', e);
        }

        public refresh() {
            var self = <UsageSelector>this;

            self.usageContainer.empty();
            self.window.find('#tname').text(' - ' + self.options.tubeData.name);
            self.options.tubeData.usages.map(function(usage, index) {
                for (var d=0; d<usage.traces.length; d++) {
                    $(self.template).appendTo(self.usageContainer).attr('data-index', index).attr('graph-index', d).find('#label').text(usage.name + ' trace' + (d+1));
                }
            })

            self.usageContainer.on('click', function(e) {
                var target = $(e.target).closest('[data-click]');
                var func = target.attr('data-click');
                switch (func) {
                    case 'view':
                        var $dataIndex = target.closest('[data-index]');
                        var dataIndex = parseInt($dataIndex.attr('data-index'));
                        var graphIndex = parseInt($dataIndex.attr('graph-index'));
                        var usageViewerOptions: tct.UsageViewerOptions = {
                            usageIndex: dataIndex,
                            graphIndex: graphIndex,
                            tubeData: self.options.tubeData
                        }
                        if (!self.usageViewer) {
                            requirejs(['usageViewer'], function() {
                                self.usageViewer = $('[usage-viewer]');
                                self.usageViewer.usageViewer(usageViewerOptions).bind('fileviewercreated', function() {
                                    self.usageViewer.usageViewer('show');
                                });
                            })
                        } else {
                            self.usageViewer.usageViewer('setOptions', usageViewerOptions);
                        }
                        break;
                }
            })
        }

        public setOptions(options: UsageSelectorOptions) {
            var self = <UsageSelector>this;
            self.options = options;
            self.refresh();
            setTimeout(function() {
                self.show();
            }, 1);
            return self;
        }

        public show() {
            var self = <UsageSelector>this;
            if (self.backdrop) {
                self.backdrop.show();
            }
            self.window.css('opacity', 0);
            self.window.show();
            self.window.animate({ opacity: 1 }, 500);
            return self;
        }

        public hide() {
            var self = <UsageSelector>this;
            self.window.animate({ opacity: 0 }, 200, function() {
                self.backdrop.hide();
                self.window.hide();
            });
            return self;
        }
    }
}

$.widget("tct.usageSelector", new tct.UsageSelector());

interface JQuery {
    usageSelector(): JQuery;
    usageSelector(method: string): tct.UsageSelector;
    usageSelector(method: 'show'): tct.UsageSelector;
    usageSelector(method: 'hide'): tct.UsageSelector;
    usageSelector(options: tct.UsageSelectorOptions): JQuery;
    usageSelector(method: string, options: tct.UsageSelectorOptions): tct.UsageSelector;
    usageSelector(method: 'setOptions', options: tct.UsageSelectorOptions): tct.UsageSelector;
}
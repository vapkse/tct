declare module tct {
    interface UsageSelectorOptions {
        tubeData: TubeData;
        title: string;
    }
    class UsageSelector {
        private options;
        private backdrop;
        private window;
        private usageViewer;
        private template;
        private usageContainer;
        private element;
        private _trigger;
        private _create();
        protected oncreated: () => void;
        refresh(): void;
        setOptions(options: UsageSelectorOptions): UsageSelector;
        show(): UsageSelector;
        hide(): UsageSelector;
    }
}
interface JQuery {
    usageSelector(): JQuery;
    usageSelector(method: string): tct.UsageSelector;
    usageSelector(method: 'show'): tct.UsageSelector;
    usageSelector(method: 'hide'): tct.UsageSelector;
    usageSelector(options: tct.UsageSelectorOptions): JQuery;
    usageSelector(method: string, options: tct.UsageSelectorOptions): tct.UsageSelector;
    usageSelector(method: 'setOptions', options: tct.UsageSelectorOptions): tct.UsageSelector;
}

// Tingodb Finder.js
function steep(ps, path, i, inarray) {
    ps+="['"+path[i]+"']";
    if (path.length==i)
        return "";
    i++;
    var s = "";
    s+= "v"+i+"="+ps+"\n";
    s+= "if (!v"+i+")"+ (inarray ? "continue" : "return null") + ";\n"
    if (path.length!=i) {
        s+= "if (Array.isArray(v"+i+")) {\n";
        s+= "for (i"+i+"=0; i"+i+"<v"+i+".length; i"+i+"++) {\n";
        s+= steep("v"+i+"[i"+i+"]",path,i, true);
        s+= "}\n}\nelse\n{\n";
        s+= steep("v"+i,path,i);
        s+= "}\n"
    } else
        s+= "res.push(v"+i+".valueOf())\n";
    return s;
};
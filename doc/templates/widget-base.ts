'use strict';

module tct {
    export interface AnodeChartInfosOptions {
        infos: GraphInfos,
    }

    export class AnodeChartInfos {
        protected element: JQuery;
        protected options: AnodeChartInfosOptions;

        contructor() {
            var self = this as AnodeChartInfos;

            // Resource need to be renderized, so the call must be like this
            $.get('/resource?r=widgets%2Fanode-chart-infos.html').done(function(html) {

                self.element.append(html).addClass('modern-highcharts');
                self.refresh();
            })
        }
        
        public setOptions(options: AnodeChartInfosOptions) {
            var self: AnodeChartInfos = this;
            
        }
        
        public refresh() {
            var self: AnodeChartInfos = this;
        }

    }
}

$.widget("tct.anodeChartInfos", new tct.AnodeChartInfos());

interface JQuery {
    anodeChartInfos(): JQuery;
    anodeChartInfos(method: string): JQuery;
    anodeChartInfos(method: string, options: tct.AnodeChartInfosOptions): JQuery;
    anodeChartInfos(method: 'setOptions', options: tct.AnodeChartInfosOptions): JQuery;
    anodeChartInfos(method: 'refresh'): JQuery;
    anodeChartInfos(options: tct.AnodeChartInfosOptions): JQuery;
}
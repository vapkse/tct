echo off
cd Conversions
echo Bitmap Conversion
rem convert with image-magik
forfiles /m *.gif /c "cmd /c convert.exe @file @file.ppm"
echo SVG Conversion
forfiles /m *.ppm /c "cmd /c C:\git\tct\utils\potrace\potrace @file -o ../svg/@file.svg -s --tight -t 0.01 -u 100"
pause
interface NewsData {
    _id?: string,
    date?: string,
    text?: string,
    uid?: string,
    params?: { [key: number]: string }
}

interface TubePins {
    pins: Array<string>
}

interface TubeType {
    _id: number,
    text: string,
    uid: string,
    sym?: boolean,
    cfg: Array<TubePins>
}

interface TubeBase {
    _id: number,
    name: string
}

interface TubePinout {
    _id: number,
    name: string
}

interface TubeDoc {
    _id?: string,
    filename?: string,
    description?: string
}

interface TubeUnitData {
    vamax?: number,
    vamaxp?: number,
    pamax?: number,
    vg2max?: number,
    vg2maxp?: number,
    ig2max?: number,
    ig2maxp?: number,
    pg2max?: number,
    vhknmax?: number,
    vhkpmax?: number,
    ikmax?: number,
    ikmaxp?: number,
    vg1nmax?: number,
    vg1nmaxp?: number,
    ig1max?: number,
    ig1maxp?: number,
    cgk?: number,
    cak?: number,
    cga?: number
}

interface ZoomData {
    minX?: number,
    maxX?: number,
    minY?: number,
    maxY?: number,
}

interface TubeUsageData {
    _id: string,
    name?: string,
    note?: string,
    unit?: number,
    mode: string,
    va?: number,
    iazero?: number,
    iamax?: number,
    vg2?: number,
    ig2zero?: number,
    ig2max?: number,
    vg1?: number,
    ig1zero?: number,
    ig1max?: number,
    load?: number,
    pout?: number,
    gain?: number,
    traces: string,
    zoom?: ZoomData,
    series?: { [vg1: string]: boolean }
}

interface TubeData {
    _text?: string, // Hidden if no inedexation here. Can be hidden if the entry is only in the base to reserve the id.
    _id: number, // Unique id   
    _userId?: string,
    name?: string, // Name is unique also
    type?: number, // Lookup
    base?: number,   // Lookup
    pinout?: number, // Lookup
    vh1?: number,
    ih1?: number,
    vh2?: number,
    ih2?: number,
    units?: Array<TubeUnitData>,
    notes?: string,
    documents?: Array<TubeDoc>,
    usages?: Array<TubeUsageData>
}

interface TubeSearchResult {
    tubeData: TubeData,
    pinout?: TubePinout,
    base?: TubeBase,
    type?: TubeType,
    merged?: boolean,
}

interface UserTubeSearchResult extends TubeSearchResult {
    userInfos: {
        _id: string,
        userName: string,
        email: string,
    }
}

// Home page
interface TubeResultData {
    [name: string]: any,
    name?: string,
    type?: string,
    typeuid?: string,
    base?: string,
    pinout?: string,
    vh1?: string,
    ih1?: string,
    h2tt?: string,
    vamax?: string,
    pamax?: string,
    vg2max?: string,
    pg2max?: string,
    ikmax?: string,
    unitsCount: number,
    unitIndex: number,
    docsCount: number,
    usagesCount: number,
    userId: string,
    userEmail: any
}

interface SearchResult {
    users?: { [id: string]: any }
    pageCount?: number,
    page?: number,
    template?: string,
}

interface NewsSearchResult extends SearchResult {
    news?: Array<NewsData>,
}

interface TubesSearchResult extends SearchResult {
    bases?: Array<TubeBase>,
    basesError?: string,
    types?: Array<TubeType>,
    typesError?: string,
    pinouts?: Array<TubePinout>,
    pinoutsError?: string,
    tubes?: Array<TubeData>,
}

interface SearchState extends TubesSearchResult {
    scrollPos: number
}

interface UploadResultFile {
    filename: string,
    tubeGraph?: TubeGraph
    error?: Error,
}

interface UploadResult {
    files: Array<UploadResultFile>,
    tubeResult?: TubeSearchResult // Filled only when the tube is directly saved 
}

interface SaveUsageResult extends TubeSearchResult {
    usageId: string
}

// For traces
interface TubeMeasure {
    va: number,
    vg1?: number,
    ig1?: number,
    vg2?: number,
    ig2?: number,
    ik: number
}

interface TubeCurve {
    p: Array<TubeMeasure>,
    vg1: number
}

interface TubeGraph {
    c: Array<TubeCurve>
    unit?: number,
    vg2?: number,
    logX?: boolean,
    logY?: boolean,
    name: string,
    docId?: string, // Id linked to the graph when the graph is created from a document  
    raw: boolean,
    triode?: boolean
}

interface DatabaseTable {
    name: string,
    size: number,
    version: {
        major: number,
        minor: number
    }
}

interface DocumentFile {
    name: string,
    size: number,
    linked: boolean,
    linkedTo: Array<string>,
    missing: boolean
}

interface TranslationData {
    _id: string,
    value: string,
    version?: number,
    isBest?: boolean,
}
    
interface TranslationRecord extends TranslationData {
    selected?: string,
    current?: string,
}

interface TranslationProposal{
    _id?: any,
    uid: string,
    original: string,
    proposal: string,
    locale: string,
    userName?: string,
}

interface ProposalData extends TranslationProposal{
    current?: string,    
}
/// <reference path="tct.d.ts" />
/// <reference path="translator.d.ts" />
/// <reference path="validation.d.ts" />
/// <reference path="dbengine/dbengine.d.ts" />

interface IndexedObject {
	[index: string]:any 
}

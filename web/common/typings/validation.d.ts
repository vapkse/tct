declare module validation {
    interface Message extends translator.Message {
        tooltip?: string | boolean,
        fieldCss?: string,
        fieldName?: string,
        dataIndex?: number
    }
}

declare module Express {
    export interface Request {
        flash(event: string, message: any): any;
    }
}

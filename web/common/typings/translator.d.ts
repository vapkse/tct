declare module translator {
    interface Message extends Error{
        type?: string,
        params?: { [index: string]: any},
    }

    interface Credentials {
        clientId: string,
        clientSecret: string
    }

    interface AccessToken {
        token_type?: string,
        access_token?: string,
        expires_in?: string,
        scope?: string,
        error?: any,
        error_description?: string
    }

    interface Language {
        id: string,
        text: string
    }

    interface LocaleLanguages {
        languages?: Array<Language>;
        locale?: string
    }

    interface Languages {
        [locale: string]: LocaleLanguages,
    }

    interface Translation {
        [id: string]: string,
    }

    interface LocaleTranslations {
        translations: Translation
        locale: string
    }

    interface Translations {
        [locale: string]: LocaleTranslations,
    }
   
    interface TranslationsParams {
        from?: string,
        to: string,
        translations: Translation,
        nocache: boolean,
    }

    interface RequestResult {
        locale: string,
        error?: Error;
        languages?: LocaleLanguages,
        translations?: Translation
    }

    interface SessionLocales {
        [id: string]: string, //locale
    }
}

declare module "http" {
    interface ServerResponse {
        setEncoding(value: string): void
    }
}

declare module Express {
    interface Session {
        id: string
    }
}
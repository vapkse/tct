'use strict';

// ************ IMPORTANT ***********************
// The only pass compatible with client and server directories structure
import DataProviding = require('../../common/ts/data-providing');
import TubeUnit = require('../../common/ts/tube-unit');
import TubeUsage = require('../../common/ts/tube-usage');
import Document = require('../../common/ts/document');

module tct {
    export var tubeModel = {
        _text: {
            type: "string"
        },
        name: {
            type: "string",
            maxLength: 30,
            required: true
        },
        vh1: {
            type: "number",
            min: 0,
            max: 999
        },
        ih1: {
            type: "number",
            min: 0,
            max: 99
        },
        vh2: {
            type: "number",
            min: 0,
            max: 999
        },
        ih2: {
            type: "number",
            min: 0,
            max: 99
        },
        base: {
            type: "objectid",
            valueField: "_id",
            textField: "name",
            lookupurl: "/resource?c=tbases"
        },
        type: {
            type: "objectid",
            valueField: "_id",
            textField: "text",
            lookupurl: "/resource?c=ttypes",
            required: true
        },
        pinout: {
            type: "objectid",
            valueField: "_id",
            textField: "name",
            lookupurl: "/resource?c=tpins"
        },
        notes: {
            type: "string",
            maxLength: 255
        },
        documents: {
            type: "array"
        },
        usages: {
            type: "array"
        },
        units: {
            type: "array"
        }
    }

    // Decorator example
    /*function field(target: DataProviding.Dataset, key: string, descriptor: any){
        debugger;
        DataProviding.Dataset._fieldNames.push(key);        
    }*/
    export class Tube extends DataProviding.Dataset {
        constructor(tubeInfos: TubeSearchResult) {
            super(tubeInfos);
        }

        protected oncreate() {
            var self = this as Tube;
            var tubeInfos = self._datas as TubeSearchResult
            
            // Create fields from model
            self.createFields(tubeModel);
            
            // Fill standard values from model
            self.fillValues(tubeModel, tubeInfos.tubeData);
            
            // Fill other values            
            Object.keys(tubeModel).map(function(name) {
                var f = self._fields[name];
                // Check field type
                switch (f.type) {
                    case 'objectid':
                        f.value = (<any>tubeInfos)[name];
                        break;
                    case 'array':
                        if (name === 'documents') {
                            // Fill documents subsets            
                            if (tubeInfos.tubeData.documents) {
                                tubeInfos.tubeData.documents.map(doc => f.value.push(new Document.Document(doc)));
                            }
                        } else if (name === 'units') {
                            // Fill units subsets            
                            if (tubeInfos.tubeData.units) {
                                tubeInfos.tubeData.units.map(unit => f.value.push(new TubeUnit.TubeUnit(unit)));
                            }
                        } else if (name === 'usages') {
                            // Fill usages subsets            
                            if (tubeInfos.tubeData.usages) {
                                tubeInfos.tubeData.usages.map(usage => f.value.push(new TubeUsage.TubeUsage(usage)));
                            }
                        }
                        break;
                }
            }) 
           
            // Fill other values                        
            self._fields['dh'] = {
                type: 'boolean',
                value: tubeInfos.tubeData.vh2 || tubeInfos.tubeData.ih2 ? true : false,
            }
                          
            // Call base to initialize original values
            super.oncreate();
        }

        public createArrayFieldSubset(name: string, json?: any): DataProviding.Dataset {
            var self = this as Tube;
            var tubeInfos = self._datas as TubeSearchResult

            if (name === 'unit') {
                return new TubeUnit.TubeUnit(json || {});
            } else if (name === 'document') {
                return new Document.Document(json || {});
            } else if (name === 'usage') {
                return new TubeUsage.TubeUsage(json || {});
            } else {
                return super.createArrayFieldSubset(name)
            }
        }

        public get _id() {
            var self = this as Tube;
            var tubeInfos = self._datas as TubeSearchResult
            return tubeInfos.tubeData._id;
        }

        public get name(): DataProviding.Field {
            return this._fields['name'];
        }

        public get vh1(): DataProviding.Field {
            return this._fields['vh1'];
        }

        public get ih1(): DataProviding.Field {
            return this._fields['ih1'];
        }

        public get vh2(): DataProviding.Field {
            return this._fields['vh2'];
        }

        public get ih2(): DataProviding.Field {
            return this._fields['ih2'];
        }

        public get dh(): DataProviding.Field {
            return this._fields['dh'];
        }

        public get notes(): DataProviding.Field {
            return this._fields['notes'];
        }

        public get type(): DataProviding.Field {
            return this._fields['type'];
        }

        public get base(): DataProviding.Field {
            return this._fields['base'];
        }

        public get pinout(): DataProviding.Field {
            return this._fields['pinout'];
        }

        public get units(): Array<TubeUnit.TubeUnit> {
            return this._fields['units'].value as Array<TubeUnit.TubeUnit>;
        }

        public get documents(): DataProviding.Field {
            return this._fields['documents'];
        }

        public get usages(): DataProviding.Field {
            return this._fields['usages'];
        }

        public getDocumentById = function(docId: string) {
            var self = this as Tube;
            var filter = (<Array<Document.Document>>self.documents.value).filter(d=> d._id === docId);
            return filter.length ? filter[0] : null;
        }
        
        protected validateFieldSet(verrors: Array<validation.Message>, field: DataProviding.Field, name: string, dataIndex?: number) {
            var self = this as Tube;
            
            super.validateFieldSet(verrors, field, name, dataIndex);
            
            if (name === 'name') {
                // No spaces on field name
                if (/\s/.test(field.value)) {
                     verrors.push({
                        type: 'error',
                        name: 'msg-nospaceontubename',
                        message: 'The space character is not allowed on for the tube name.',
                        fieldName: name,
                        tooltip: true,
                        dataIndex: dataIndex,
                        params: { 0: name }
                    })               
                }
            }
        }
        
        // Override because we need to remove the unused units in case of the type has changed
        public toJson() {
            var self = this as Tube;

            var type = self.type.value;
            var field = self.getField('units');
            var units = field.value;
            field.value = [];
            var numberOfUnits = Math.min(!type || type.sym ? 1 : type.cfg.length, units.length);
            if (units) {
                for (var i = 0; i < numberOfUnits; i++) {
                    field.value.push(units[i]);
                }
            }

            return super.toJson();
        }
    }
}
export = tct;
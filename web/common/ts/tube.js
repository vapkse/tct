'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var DataProviding = require('../../common/ts/data-providing');
var TubeUnit = require('../../common/ts/tube-unit');
var TubeUsage = require('../../common/ts/tube-usage');
var Document = require('../../common/ts/document');
var tct;
(function (tct) {
    tct.tubeModel = {
        _text: {
            type: "string"
        },
        name: {
            type: "string",
            maxLength: 30,
            required: true
        },
        vh1: {
            type: "number",
            min: 0,
            max: 999
        },
        ih1: {
            type: "number",
            min: 0,
            max: 99
        },
        vh2: {
            type: "number",
            min: 0,
            max: 999
        },
        ih2: {
            type: "number",
            min: 0,
            max: 99
        },
        base: {
            type: "objectid",
            valueField: "_id",
            textField: "name",
            lookupurl: "/resource?c=tbases"
        },
        type: {
            type: "objectid",
            valueField: "_id",
            textField: "text",
            lookupurl: "/resource?c=ttypes",
            required: true
        },
        pinout: {
            type: "objectid",
            valueField: "_id",
            textField: "name",
            lookupurl: "/resource?c=tpins"
        },
        notes: {
            type: "string",
            maxLength: 255
        },
        documents: {
            type: "array"
        },
        usages: {
            type: "array"
        },
        units: {
            type: "array"
        }
    };
    var Tube = (function (_super) {
        __extends(Tube, _super);
        function Tube(tubeInfos) {
            _super.call(this, tubeInfos);
            this.getDocumentById = function (docId) {
                var self = this;
                var filter = self.documents.value.filter(function (d) { return d._id === docId; });
                return filter.length ? filter[0] : null;
            };
        }
        Tube.prototype.oncreate = function () {
            var self = this;
            var tubeInfos = self._datas;
            self.createFields(tct.tubeModel);
            self.fillValues(tct.tubeModel, tubeInfos.tubeData);
            Object.keys(tct.tubeModel).map(function (name) {
                var f = self._fields[name];
                switch (f.type) {
                    case 'objectid':
                        f.value = tubeInfos[name];
                        break;
                    case 'array':
                        if (name === 'documents') {
                            if (tubeInfos.tubeData.documents) {
                                tubeInfos.tubeData.documents.map(function (doc) { return f.value.push(new Document.Document(doc)); });
                            }
                        }
                        else if (name === 'units') {
                            if (tubeInfos.tubeData.units) {
                                tubeInfos.tubeData.units.map(function (unit) { return f.value.push(new TubeUnit.TubeUnit(unit)); });
                            }
                        }
                        else if (name === 'usages') {
                            if (tubeInfos.tubeData.usages) {
                                tubeInfos.tubeData.usages.map(function (usage) { return f.value.push(new TubeUsage.TubeUsage(usage)); });
                            }
                        }
                        break;
                }
            });
            self._fields['dh'] = {
                type: 'boolean',
                value: tubeInfos.tubeData.vh2 || tubeInfos.tubeData.ih2 ? true : false,
            };
            _super.prototype.oncreate.call(this);
        };
        Tube.prototype.createArrayFieldSubset = function (name, json) {
            var self = this;
            var tubeInfos = self._datas;
            if (name === 'unit') {
                return new TubeUnit.TubeUnit(json || {});
            }
            else if (name === 'document') {
                return new Document.Document(json || {});
            }
            else if (name === 'usage') {
                return new TubeUsage.TubeUsage(json || {});
            }
            else {
                return _super.prototype.createArrayFieldSubset.call(this, name);
            }
        };
        Object.defineProperty(Tube.prototype, "_id", {
            get: function () {
                var self = this;
                var tubeInfos = self._datas;
                return tubeInfos.tubeData._id;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Tube.prototype, "name", {
            get: function () {
                return this._fields['name'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Tube.prototype, "vh1", {
            get: function () {
                return this._fields['vh1'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Tube.prototype, "ih1", {
            get: function () {
                return this._fields['ih1'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Tube.prototype, "vh2", {
            get: function () {
                return this._fields['vh2'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Tube.prototype, "ih2", {
            get: function () {
                return this._fields['ih2'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Tube.prototype, "dh", {
            get: function () {
                return this._fields['dh'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Tube.prototype, "notes", {
            get: function () {
                return this._fields['notes'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Tube.prototype, "type", {
            get: function () {
                return this._fields['type'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Tube.prototype, "base", {
            get: function () {
                return this._fields['base'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Tube.prototype, "pinout", {
            get: function () {
                return this._fields['pinout'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Tube.prototype, "units", {
            get: function () {
                return this._fields['units'].value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Tube.prototype, "documents", {
            get: function () {
                return this._fields['documents'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Tube.prototype, "usages", {
            get: function () {
                return this._fields['usages'];
            },
            enumerable: true,
            configurable: true
        });
        Tube.prototype.validateFieldSet = function (verrors, field, name, dataIndex) {
            var self = this;
            _super.prototype.validateFieldSet.call(this, verrors, field, name, dataIndex);
            if (name === 'name') {
                if (/\s/.test(field.value)) {
                    verrors.push({
                        type: 'error',
                        name: 'msg-nospaceontubename',
                        message: 'The space character is not allowed on for the tube name.',
                        fieldName: name,
                        tooltip: true,
                        dataIndex: dataIndex,
                        params: { 0: name }
                    });
                }
            }
        };
        Tube.prototype.toJson = function () {
            var self = this;
            var type = self.type.value;
            var field = self.getField('units');
            var units = field.value;
            field.value = [];
            var numberOfUnits = Math.min(!type || type.sym ? 1 : type.cfg.length, units.length);
            if (units) {
                for (var i = 0; i < numberOfUnits; i++) {
                    field.value.push(units[i]);
                }
            }
            return _super.prototype.toJson.call(this);
        };
        return Tube;
    })(DataProviding.Dataset);
    tct.Tube = Tube;
})(tct || (tct = {}));
module.exports = tct;
//# sourceMappingURL=tube.js.map
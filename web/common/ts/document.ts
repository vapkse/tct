'use strict';

// ************ IMPORTANT ***********************
// The only pass compatible with client and server directories structure
import DataProviding = require('../../common/ts/data-providing');

module Document {
    export var documentModel = {
        _id: {
            type: "string"
        },
        filename: {
            type: "string",
            maxLength: 255,
            required: true
        },
        description: {
            type: "string",
            maxLength: 255,
            required: true
        }
    }

    export class Document extends DataProviding.Dataset {
        constructor(tubeDoc: TubeDoc) {
            var self = this as Document;
            super(tubeDoc);
        }

        protected oncreate() {
            var self = this as Document;
            var tubeFile = self._datas as TubeDoc 
            
            // Create fields from model
            self.createFields(documentModel);

            // Fill standard values from model
            self.fillValues(documentModel, tubeFile);
                        
            // Call base to initialize original values
            super.oncreate();
        }

        public get filename(): DataProviding.Field {
            return this._fields['filename'];
        }

        public get description(): DataProviding.Field {
            return this._fields['description'];
        }
    }
}
export = Document
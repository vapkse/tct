import DataProviding = require('../../common/ts/data-providing');
import TubeUnit = require('../../common/ts/tube-unit');
import Document = require('../../common/ts/document');
declare module tct {
    var tubeModel: {
        _text: {
            type: string;
        };
        name: {
            type: string;
            maxLength: number;
            required: boolean;
        };
        vh1: {
            type: string;
            min: number;
            max: number;
        };
        ih1: {
            type: string;
            min: number;
            max: number;
        };
        vh2: {
            type: string;
            min: number;
            max: number;
        };
        ih2: {
            type: string;
            min: number;
            max: number;
        };
        base: {
            type: string;
            valueField: string;
            textField: string;
            lookupurl: string;
        };
        type: {
            type: string;
            valueField: string;
            textField: string;
            lookupurl: string;
            required: boolean;
        };
        pinout: {
            type: string;
            valueField: string;
            textField: string;
            lookupurl: string;
        };
        notes: {
            type: string;
            maxLength: number;
        };
        documents: {
            type: string;
        };
        usages: {
            type: string;
        };
        units: {
            type: string;
        };
    };
    class Tube extends DataProviding.Dataset {
        constructor(tubeInfos: TubeSearchResult);
        protected oncreate(): void;
        createArrayFieldSubset(name: string, json?: any): DataProviding.Dataset;
        _id: number;
        name: DataProviding.Field;
        vh1: DataProviding.Field;
        ih1: DataProviding.Field;
        vh2: DataProviding.Field;
        ih2: DataProviding.Field;
        dh: DataProviding.Field;
        notes: DataProviding.Field;
        type: DataProviding.Field;
        base: DataProviding.Field;
        pinout: DataProviding.Field;
        units: Array<TubeUnit.TubeUnit>;
        documents: DataProviding.Field;
        usages: DataProviding.Field;
        getDocumentById: (docId: string) => Document.Document;
        protected validateFieldSet(verrors: Array<validation.Message>, field: DataProviding.Field, name: string, dataIndex?: number): void;
        toJson(): any;
    }
}
export = tct;

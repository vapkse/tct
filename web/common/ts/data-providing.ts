'use strict';

module DataProviding {
    export interface Field {
        _originalValue?: any,
        value: any,
        type: string, // date, number, string, boolean, object, dataset
        required?: boolean,
        min?: any,
        max?: any,
        maxLength?: number,
        valueField?: string,
        textField?: string,
        lookupurl?: string,
    }

    export class Dataset {
        protected _isNew: boolean
        protected _datas: any;
        protected _fields: { [index: string]: Field } = {};

        constructor(data: any) {
            var self = this as Dataset;
            self._datas = data;
            self.oncreate()
        }

        public get _id() {
            var self = this as Dataset;
            return self._datas._id;
        }

        public get isNew() {
            var self = this as Dataset;
            return self._isNew;
        }

        public set isNew(value: boolean) {
            var self = this as Dataset;
            self._isNew = value;
            self._datas._id = undefined;
        }

        protected oncreate() {
            var self = this as Dataset;
            self.resetOriginalValues();
        }

        protected createFields(model: any) {
            var self = this as Dataset;
            
            // Create generic fields from model
            for (var name in model) {
                var m = model[name];
                var f: any = {};
                Object.keys(m).map(n => f[n] = (<any>m)[n]);
                self._fields[name] = f;
            }

            return model;
        }

        protected fillValues(model: any, json: any) {
            var self = this as Dataset;

            if (!model) {
                throw 'undefined model';
            }

            Object.keys(model).map(function(name) {
                var f = self._fields[name];
                // Check field type
                switch (f.type) {
                    case 'string':
                    case 'number':
                    case 'date':
                    case 'boolean':
                        f.value = json[name];
                        break;
                    case 'array':
                        f.value = [];
                        break;
                    case 'object':
                        f.value = json[name] || {};
                        break;
                }
            })
        }

        public resetOriginalValues = function() {
            var self = this as Dataset;
            self._isNew = self._id === 0;
            for (var name in self._fields) {
                var f = self._fields[name];
                switch (f.type) {
                    case 'string':
                    case 'number':
                    case 'date':
                    case 'boolean':
                        f._originalValue = f.value;
                        break;
                    case 'objectid':
                        var valuefield = f.valueField || '_id';
                        f._originalValue = f.value && f.value[valuefield];
                        break;
                    case 'array':
                        f._originalValue = f.value ? f.value.length : undefined;
                        break;
                    case 'object':
                        if (f.value) {
                            if (f.value.resetOriginalValues) {
                                f.value.resetOriginalValues();
                            } else {
                                f._originalValue = {};
                                Object.keys(f.value).map(n=> f._originalValue[n] = f.value[n]);
                            }
                        } else {
                            f._originalValue = f.value;
                        }
                        break;
                }
            }
        }

        public createArrayFieldSubset(name: string, json?: any): Dataset {
            throw 'Create subset method must be overrided and return a new dataset of the sepcified name.';
        }

        public getField = function(name: string): Field {
            var self = this as Dataset;
            return self._fields[name];
        }

        public isModified = function() {
            var self = this as Dataset;

            if (self._isNew) {
                // New item
                return true;
            }

            for (var name in self._fields) {
                var f = self._fields[name];
                if (!self.compareFieldValues(f)) {
                    return true;
                }
            }
            return false;
        }

        public isDirtyField(field: Field) {
            var self = this as Dataset;
            return !self.compareFieldValues(field);
        }

        protected compareFieldValues(field: Field) {
            var self = this as Dataset;

            var current = field.value;
            var original = field._originalValue;

            if (current === null) {
                current = undefined;
            }

            if (original === null) {
                original = undefined;
            }

            if (current === undefined && original === undefined) {
                return true;
            } else if ((current === undefined && original !== undefined) || (original === undefined && current !== undefined)) {
                return false;
            }

            switch (field.type) {
                case 'objectid':
                    var valuefield = field.valueField || '_id';
                    return current[valuefield] === original;

                case 'array':
                    if (current.length != original) {
                        return false;
                    } else {
                        for (var i = 0; i < current.length; i++) {
                            if (current[i].isModified) {
                                if (current[i].isModified()) {
                                    return false;
                                }
                            }
                        }
                    }
                    return true;

                case 'object':
                    if (current.isModified) {
                        return !current.isModified();
                    } else {
                        for (var name in current) {
                            if (current[name] !== original[name]) {
                                return false;
                            }
                        }
                        return true;
                    }

                default:
                    return current === original;

            }
        }

        public toJson() {
            var self = this as Dataset;
            var result: any = {
                _id: self._id
            };
            for (var name in self._fields) {
                if (self._fields[name].value !== undefined) {
                    var f = self._fields[name];
                    var value: any
                    switch (f.type) {
                        case 'objectid':
                            var valuefield = f.valueField || '_id';
                            value = f.value[valuefield];
                            break;
                        case 'array':
                            value = [];
                            f.value.map(function(v: any) {
                                if (v.toJson) {
                                    value.push(v.toJson());
                                } else {
                                    value.push(v);
                                }
                            });
                            break;
                        case 'object':
                            value = f.value.toJson ? f.value.toJson() : f.value;
                            break;
                        default:
                            value = f.value;
                            break;
                    }

                    result[name] = value;
                }
            }
            return result;
        }

        public validate = function() {
            var self = this as Dataset;
            var verrors: Array<validation.Message> = [];
            self.validateSet(verrors);
            return verrors;
        }

        public validateField = function(field: Field, name?: string) {
            var self = this as Dataset;
            var verrors: Array<validation.Message> = [];
            self.validateFieldSet(verrors, field, name);
            return verrors;
        }

        protected validateSet(verrors: Array<validation.Message>, dataIndex?: number) {
            var self = this as Dataset;
            for (var name in self._fields) {
                var f = self._fields[name];
                switch (f.type) {
                    case 'array': // Array subset
                        var array = f.value as Array<Dataset>;
                        if (f.required && (!array || array.length === 0)) {
                            verrors.push({
                                type: 'error',
                                name: 'msg-mand',
                                message: 'The field \\0 is mandatory.',
                                fieldName: name,
                                params: { 0: name }
                            })
                            //    isDirty: self.compareFieldValues(f),
                        }

                        if (array) {
                            for (var v = 0; v < array.length; v++) {
                                if (array[v].validateSet) {
                                    array[v].validateSet(verrors, v);
                                }
                            };
                        }
                        break;
                    case 'object':
                        self.validateFieldSet(verrors, f, name);
                        var subset = f.value as Dataset;
                        if (subset && subset.validateFieldSet) {
                            subset.validateSet(verrors, dataIndex);
                        }
                        break;
                    default:
                        self.validateFieldSet(verrors, f, name, dataIndex);
                        break;
                }
            }
        }

        protected validateFieldSet(verrors: Array<validation.Message>, field: Field, name: string, dataIndex?: number) {
            var self = this as Dataset;
            var value = field.value;
            if (value === undefined || value === null || value === '' || (value === 0 && field.type === 'objectid')) {
                if (field.required) {
                    verrors.push({
                        type: 'error',
                        name: 'msg-mand',
                        message: 'The field \\0 is mandatory.',
                        fieldName: name,
                        tooltip: true,
                        dataIndex: dataIndex,
                        params: { 0: name }
                    })
                }
            } else {
                if (field.maxLength && value && value.toString().length > field.maxLength) {
                    verrors.push({
                        type: 'error',
                        name: 'msg-maxlength',
                        message: 'The length of the field \\0 exceeds the maximum allowed of \\1',
                        fieldName: name,
                        tooltip: true,
                        dataIndex: dataIndex,
                        params: {
                            0: name,
                            1: field.maxLength
                        }
                    })
                }

                if (field.min !== undefined || field.max !== undefined) {
                    var num = parseFloat(value);
                    if (isNaN(num)) {
                        verrors.push({
                            type: 'error',
                            name: 'msg-wrongtype',
                            message: 'Invalid value type for the field \\0',
                            fieldName: name,
                            tooltip: true,
                            dataIndex: dataIndex,
                            params: {
                                0: name
                            }
                        })
                    } else if (field.max !== undefined && num > field.max) {
                        verrors.push({
                            type: 'error',
                            name: 'msg-maxerr',
                            message: 'The value of the field \\0 exceeds the maximum allowed of \\1',
                            fieldName: name,
                            tooltip: true,
                            dataIndex: dataIndex,
                            params: {
                                0: name,
                                1: field.max
                            }
                        })
                    } else if (field.min !== undefined && num < field.min) {
                        verrors.push({
                            type: 'error',
                            name: 'msg-minerr',
                            message: 'The value of the field \\0 is less than the minimum value allowed of \\1',
                            fieldName: name,
                            tooltip: true,
                            dataIndex: dataIndex,
                            params: {
                                0: name,
                                1: field.min
                            }
                        })
                    }
                }
            }
        }
    }
}

export = DataProviding

'use strict';

// ************ IMPORTANT ***********************
// The only pass compatible with client and server directories structure
import DataProviding = require('../../common/ts/data-providing');

module News {
    export var newsModel = {
        _id: {
            type: "string"
        },
        date: {
            type: "date"
        },
        text: {
            type: "string"
        },
        uid: {
            type: "string"
        },
        params: {
            type: "Object"
        },
    }

    export class News extends DataProviding.Dataset {
        constructor(newsData: NewsData) {
            var self = this as News;
            super(newsData);
        }

        protected oncreate() {
            var self = this as News;
            var tubeFile = self._datas as TubeDoc 
            
            // Create fields from model
            self.createFields(newsModel);

            // Fill standard values from model
            self.fillValues(newsModel, tubeFile);
                        
            // Call base to initialize original values
            super.oncreate();
        }

        public get date(): DataProviding.Field {
            return this._fields['date'];
        }

        public get text(): DataProviding.Field {
            return this._fields['text'];
        }

        public get uid(): DataProviding.Field {
            return this._fields['uid'];
        }

        public get params(): DataProviding.Field {
            return this._fields['params'];
        }
    }
}
export = News
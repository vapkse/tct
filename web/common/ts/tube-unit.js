'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var DataProviding = require('../../common/ts/data-providing');
var TubeUnit;
(function (TubeUnit_1) {
    TubeUnit_1.tubeUnitModel = {
        vamax: {
            type: "number",
            min: 0,
            max: 9999
        },
        vamaxp: {
            type: "number",
            min: 0,
            max: 99999
        },
        pamax: {
            type: "number",
            min: 0,
            max: 9999
        },
        vg2max: {
            type: "number",
            min: 0,
            max: 9999
        },
        vg2maxp: {
            type: "number",
            min: 0,
            max: 9999
        },
        ig2max: {
            type: "number",
            min: 0,
            max: 9999
        },
        ig2maxp: {
            type: "number",
            min: 0,
            max: 9999
        },
        pg2max: {
            type: "number",
            min: 0,
            max: 999
        },
        vhknmax: {
            type: "number",
            min: 0,
            max: 9999
        },
        vhkpmax: {
            type: "number",
            min: 0,
            max: 9999
        },
        ikmax: {
            type: "number",
            min: 0,
            max: 9999
        },
        ikmaxp: {
            type: "number",
            min: 0,
            max: 9999
        },
        vg1nmax: {
            type: "number",
            min: 0,
            max: 999
        },
        vg1nmaxp: {
            type: "number",
            min: 0,
            max: 999
        },
        ig1max: {
            type: "number",
            min: 0,
            max: 9999
        },
        ig1maxp: {
            type: "number",
            min: 0,
            max: 9999
        },
        cgk: {
            type: "number",
            min: 0,
            max: 999
        },
        cak: {
            type: "number",
            min: 0,
            max: 999
        },
        cga: {
            type: "number",
            min: 0,
            max: 999
        }
    };
    var TubeUnit = (function (_super) {
        __extends(TubeUnit, _super);
        function TubeUnit(unitInfo) {
            var self = this;
            _super.call(this, unitInfo);
        }
        TubeUnit.prototype.oncreate = function () {
            var self = this;
            var unitInfo = self._datas;
            self.createFields(TubeUnit_1.tubeUnitModel);
            self.fillValues(TubeUnit_1.tubeUnitModel, unitInfo);
            _super.prototype.oncreate.call(this);
        };
        Object.defineProperty(TubeUnit.prototype, "vamax", {
            get: function () {
                return this._fields['vamax'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "vamaxp", {
            get: function () {
                return this._fields['vamaxp'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "pamax", {
            get: function () {
                return this._fields['pamax'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "vg2max", {
            get: function () {
                return this._fields['vg2max'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "vg2maxp", {
            get: function () {
                return this._fields['vg2maxp'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "ig2max", {
            get: function () {
                return this._fields['ig2max'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "ig2maxp", {
            get: function () {
                return this._fields['ig2maxp'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "pg2max", {
            get: function () {
                return this._fields['pg2max'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "vhknmax", {
            get: function () {
                return this._fields['vhknmax'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "vhkpmax", {
            get: function () {
                return this._fields['vhkpmax'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "ikmax", {
            get: function () {
                return this._fields['ikmax'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "ikmaxp", {
            get: function () {
                return this._fields['ikmaxp'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "vg1nmax", {
            get: function () {
                return this._fields['vg1nmax'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "vg1nmaxp", {
            get: function () {
                return this._fields['vg1nmaxp'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "ig1max", {
            get: function () {
                return this._fields['ig1max'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "ig1maxp", {
            get: function () {
                return this._fields['ig1maxp'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "cgk", {
            get: function () {
                return this._fields['cgk'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "cak", {
            get: function () {
                return this._fields['cak'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TubeUnit.prototype, "cga", {
            get: function () {
                return this._fields['cga'];
            },
            enumerable: true,
            configurable: true
        });
        return TubeUnit;
    })(DataProviding.Dataset);
    TubeUnit_1.TubeUnit = TubeUnit;
})(TubeUnit || (TubeUnit = {}));
module.exports = TubeUnit;
//# sourceMappingURL=tube-unit.js.map
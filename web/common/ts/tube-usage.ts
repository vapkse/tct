'use strict';

// ************ IMPORTANT ***********************
// The only pass compatible with client and server directories structure
import DataProviding = require('../../common/ts/data-providing');

module TubeUsage {
    export var tubeUsageModel = {
        name: {
            type: "string",
            maxLength: 255,
            required: true
        },
        note: {
            type: "string",
            maxLength: 10000
        },
        unit: {
            type: "number",
            required: true
        },
        mode: {
            type: "string",
            required: true
        },
        va: {
            type: "number",
            min: 0,
            max: 9999
        },
        iazero: {
            type: "number",
            min: 0,
            max: 9999
        },
        vinpp: {
            type: "number",
            min: -999,
            max: 999
        },
        load: {
            type: "number",
            min: 0,
            max: 9999999
        },
        /*iamax: {
            type: "number",
            min: 0,
            max: 9999
        },
        vg2: {
            type: "number",
            min: 0,
            max: 9999
        },
        ig2zero: {
            type: "number",
            min: 0,
            max: 9999
        },
        ig2max: {
            type: "number",
            min: 0,
            max: 9999
        },
        vg1: {
            type: "number",
            min: -999,
            max: 999
        },
        ig1zero: {
            type: "number",
            min: 0,
            max: 9999
        },
        ig1max: {
            type: "number",
            min: 0,
            max: 9999
        },
        pout: {
            type: "number",
            min: 0,
            max: 9999
        },
        gain: {
            type: "number",
            min: 0,
            max: 9999
        },*/
        traces: {
            type: "string"
        },
        zoom: {
            type: "object"
        },
        series: {
            type: "object"
        }
    }

    export class TubeUsage extends DataProviding.Dataset {
        constructor(usageInfo: TubeUsageData) {
            super(usageInfo);
        }

        protected oncreate() {
            var self = this as TubeUsage;
            var usageInfo = self._datas as TubeUsageData;
            
            // Create fields from model
            self.createFields(tubeUsageModel);
            
            // Fill standard values from model
            self.fillValues(tubeUsageModel, usageInfo);
 
            // Call base to initialize original values
            super.oncreate();
        }
        public get name(): DataProviding.Field {
            return this._fields['name'];
        }
        public get note(): DataProviding.Field {
            return this._fields['note'];
        }
        public get unit(): DataProviding.Field {
            return this._fields['unit'];
        }
        public get mode(): DataProviding.Field {
            return this._fields['mode'];
        }
        public get va(): DataProviding.Field {
            return this._fields['va'];
        }
        public get iazero(): DataProviding.Field {
            return this._fields['iazero'];
        }
        public get vinpp(): DataProviding.Field {
            return this._fields['vinpp'];
        }
        public get load(): DataProviding.Field {
            return this._fields['load'];
        }

        /*public get iamax(): DataProviding.Field {
            return this._fields['iamax'];
        }
        public get vg2(): DataProviding.Field {
            return this._fields['vg2'];
        }
        public get ig2zero(): DataProviding.Field {
            return this._fields['ig2zero'];
        }
        public get ig2max(): DataProviding.Field {
            return this._fields['ig2max'];
        }
        public get vg1(): DataProviding.Field {
            return this._fields['vg1'];
        }
        public get ig1zero(): DataProviding.Field {
            return this._fields['ig1zero'];
        }
        public get ig1max(): DataProviding.Field {
            return this._fields['ig1max'];
        }
        public get pout(): DataProviding.Field {
            return this._fields['pout'];
        }
        public get gain(): DataProviding.Field {
            return this._fields['gain'];
        }*/
        public get traces(): DataProviding.Field {
            return this._fields['traces'];
        }
        public get zoom(): DataProviding.Field {
            return this._fields['zoom'];
        }
        public get series(): DataProviding.Field {
            return this._fields['series'];
        }
        public get effectiveLoad() {
            var self = this as TubeUsage;
            
            // Calc new load param
            switch (self.mode.value) {
                case 'se':
                    return self.load.value;
                case 'pp':
                    return self.load.value / 2;
                case '2pp':
                    return self.load.value;
                case '3pp':
                    return self.load.value * 3 / 2;
                case '4pp':
                    return self.load.value * 2;
                case '5pp':
                    return self.load.value * 5 / 2;
                case '6pp':
                    return self.load.value * 3;
            }
        }
        public set effectiveLoad(load: number) {
            var self = this as TubeUsage;
            
            // Calc new load param
            switch (self.mode.value) {
                case 'se':
                    self.load.value = load;
                    break;
                case 'pp':
                    self.load.value = load * 2;
                    break;
                case '2pp':
                    self.load.value = load;
                    break;
                case '3pp':
                    self.load.value = load * 2 / 3;
                    break;
                case '4pp':
                    self.load.value = load / 2;
                    break;
                case '5pp':
                    self.load.value = load * 2 / 5;
                    break;
                case '6pp':
                    self.load.value = load / 3;
                    break;
            }
        }
    }
}
export = TubeUsage
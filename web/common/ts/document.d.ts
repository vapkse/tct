import DataProviding = require('../../common/ts/data-providing');
declare module Document {
    var documentModel: {
        _id: {
            type: string;
        };
        filename: {
            type: string;
            maxLength: number;
            required: boolean;
        };
        description: {
            type: string;
            maxLength: number;
            required: boolean;
        };
    };
    class Document extends DataProviding.Dataset {
        constructor(tubeDoc: TubeDoc);
        protected oncreate(): void;
        filename: DataProviding.Field;
        description: DataProviding.Field;
    }
}
export = Document;

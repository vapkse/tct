module user {
    export interface UserEmail {
        userName?: string,
        email?: string
    }

    export interface DbUser extends UserEmail {
        _id: string,
        _password?: string, // For local provider
        displayName?: string,
        validated?: boolean,
        blocked?: boolean
        role?: string, // Default: user     
        validationToken?: string,
        validationTime?: number
        provider?: string,
        failAttempts?: number,
        lastLogin?: number,
        lastIP?: string,
    }

    export interface User extends DbUser {
        token?: string, // For external providers
        photo?: string
    }

    export class Validator {
        private validatePassword(password: string, password2: string, cb: (verrors: Array<validation.Message>) => void) {
            var verrors: Array<validation.Message> = [];

            if (!password) {
                verrors.push({
                    type: 'error',
                    name: 'msg-pwdmand',
                    message: 'Password is mandatory.',
                    tooltip: true,
                    fieldCss: 'error',
                    fieldName: '#user_password'
                });
                verrors.push({
                    type: 'error',
                    name: 'msg-pwdmand',
                    message: 'Password is mandatory.',
                    tooltip: true,
                    fieldCss: 'error',
                    fieldName: '#pwd2'
                });
            } else if (password !== password2) {
                verrors.push({
                    type: 'error',
                    name: 'msg-pwddosntmatch',
                    message: 'The passwords doesnt match.',
                    tooltip: true,
                    fieldCss: 'error',
                    fieldName: '#user_password'
                });
                verrors.push({
                    type: 'error',
                    name: 'msg-pwddosntmatch',
                    message: 'The passwords doesnt match.',
                    tooltip: true,
                    fieldCss: 'error',
                    fieldName: '#pwd2'
                });
            } else if (password.length < 5) {
                var uid = '';
                verrors.push({
                    type: 'error',
                    name: 'msg-pwdtooshort',
                    message: 'The password must be at least 5 characters.',
                    tooltip: true,
                    fieldCss: 'error',
                    fieldName: '#user_password'
                });
                verrors.push({
                    type: 'error',
                    name: 'msg-pwdtooshort',
                    message: 'The password must be at least 5 characters.',
                    tooltip: true,
                    fieldCss: 'error',
                    fieldName: '#pwd2'
                });
            }
            cb(verrors);
        }

        private validateEmail(email: string, cb: (verrors: Array<validation.Message>) => void) {
            var verrors: Array<validation.Message> = [];

            if (!email) {
                verrors.push({
                    type: 'error',
                    name: 'msg-emailmand',
                    message: 'Email is mandatory.',
                    tooltip: true,
                    fieldCss: 'error',
                    fieldName: '#user_email'
                });
            } else {
                // validate mail
                var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (!re.test(email)) {
                    verrors.push({
                        type: 'error',
                        name: 'msg-invemail',
                        message: 'Invalid email.',
                        tooltip: true,
                        fieldCss: 'error',
                        fieldName: '#user_email',
                    });
                }
            }

            cb(verrors);
        }

        public validatePasswordChange(password: string, password2: string, cb: (verrors: Array<validation.Message>) => void) {
            var self = this as Validator;
            self.validatePassword(password, password2, function(verrors: Array<validation.Message>) {
                cb(verrors);
            })
        }

        public validatePasswordReset(email: string, cb: (verrors: Array<validation.Message>) => void) {
            var self = this as Validator;
            self.validateEmail(email, function(verrors: Array<validation.Message>) {
                cb(verrors);
            })
        }

        public validateSignup(userEmail: UserEmail, password: string, password2: string, cb: (verrors: Array<validation.Message>) => void) {
            var self = this as Validator;
            var verrors: Array<validation.Message> = [];

            if (!userEmail.userName) {
                verrors.push({
                    type: 'error',
                    name: 'msg-usermand',
                    message: 'User is mandatory.',
                    tooltip: true,
                    fieldCss: 'error',
                    fieldName: '#user_username'
                });
            } else if (userEmail.userName.length < 5) {
                verrors.push({
                    type: 'error',
                    name: 'msg-usernametooshort',
                    message: 'The user name must be at least 5 characters.',
                    tooltip: true,
                    fieldCss: 'error',
                    fieldName: '#user_username'
                });
            }

            self.validateEmail(userEmail.email, function(vee: Array<validation.Message>) {
                vee.map(v => verrors.push(v));
                self.validatePassword(password, password2, function(vep: Array<validation.Message>) {
                    vep.map(v => verrors.push(v));
                    cb(verrors);
                })
            })
        }

        public validateLogin(login: string, password: string, cb: (verrors: Array<validation.Message>) => void) {
            var verrors: Array<validation.Message> = [];

            if (!login) {
                verrors.push({
                    type: 'error',
                    name: 'msg-logmand',
                    message: 'Login is mandatory.',
                    tooltip: true,
                    fieldCss: 'error',
                    fieldName: '#user_login'
                });
            }

            if (!password) {
                var uid = "";
                verrors.push({
                    type: 'error',
                    name: 'msg-pwdmand',
                    message: 'Password is mandatory.',
                    tooltip: true,
                    fieldCss: 'error',
                    fieldName: '#user_password'
                });
            }

            cb(verrors);
        }
    }
}

export = user;
import DataProviding = require('../../common/ts/data-providing');
declare module TubeUnit {
    var tubeUnitModel: {
        vamax: {
            type: string;
            min: number;
            max: number;
        };
        vamaxp: {
            type: string;
            min: number;
            max: number;
        };
        pamax: {
            type: string;
            min: number;
            max: number;
        };
        vg2max: {
            type: string;
            min: number;
            max: number;
        };
        vg2maxp: {
            type: string;
            min: number;
            max: number;
        };
        ig2max: {
            type: string;
            min: number;
            max: number;
        };
        ig2maxp: {
            type: string;
            min: number;
            max: number;
        };
        pg2max: {
            type: string;
            min: number;
            max: number;
        };
        vhknmax: {
            type: string;
            min: number;
            max: number;
        };
        vhkpmax: {
            type: string;
            min: number;
            max: number;
        };
        ikmax: {
            type: string;
            min: number;
            max: number;
        };
        ikmaxp: {
            type: string;
            min: number;
            max: number;
        };
        vg1nmax: {
            type: string;
            min: number;
            max: number;
        };
        vg1nmaxp: {
            type: string;
            min: number;
            max: number;
        };
        ig1max: {
            type: string;
            min: number;
            max: number;
        };
        ig1maxp: {
            type: string;
            min: number;
            max: number;
        };
        cgk: {
            type: string;
            min: number;
            max: number;
        };
        cak: {
            type: string;
            min: number;
            max: number;
        };
        cga: {
            type: string;
            min: number;
            max: number;
        };
    };
    class TubeUnit extends DataProviding.Dataset {
        constructor(unitInfo: TubeUnitData);
        protected oncreate(): void;
        vamax: DataProviding.Field;
        vamaxp: DataProviding.Field;
        pamax: DataProviding.Field;
        vg2max: DataProviding.Field;
        vg2maxp: DataProviding.Field;
        ig2max: DataProviding.Field;
        ig2maxp: DataProviding.Field;
        pg2max: DataProviding.Field;
        vhknmax: DataProviding.Field;
        vhkpmax: DataProviding.Field;
        ikmax: DataProviding.Field;
        ikmaxp: DataProviding.Field;
        vg1nmax: DataProviding.Field;
        vg1nmaxp: DataProviding.Field;
        ig1max: DataProviding.Field;
        ig1maxp: DataProviding.Field;
        cgk: DataProviding.Field;
        cak: DataProviding.Field;
        cga: DataProviding.Field;
    }
}
export = TubeUnit;

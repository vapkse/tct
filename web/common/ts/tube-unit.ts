'use strict';

// ************ IMPORTANT ***********************
// The only pass compatible with client and server directories structure
import DataProviding = require('../../common/ts/data-providing');

module TubeUnit {
    export var tubeUnitModel = {
        vamax: {
            type: "number",
            min: 0,
            max: 9999
        },
        vamaxp: {
            type: "number",
            min: 0,
            max: 99999
        },
        pamax: {
            type: "number",
            min: 0,
            max: 9999
        },
        vg2max: {
            type: "number",
            min: 0,
            max: 9999
        },
        vg2maxp: {
            type: "number",
            min: 0,
            max: 9999
        },
        ig2max: {
            type: "number",
            min: 0,
            max: 9999
        },
        ig2maxp: {
            type: "number",
            min: 0,
            max: 9999
        },
        pg2max: {
            type: "number",
            min: 0,
            max: 999
        },
        vhknmax: {
            type: "number",
            min: 0,
            max: 9999
        },
        vhkpmax: {
            type: "number",
            min: 0,
            max: 9999
        },
        ikmax: {
            type: "number",
            min: 0,
            max: 9999
        },
        ikmaxp: {
            type: "number",
            min: 0,
            max: 9999
        },
        vg1nmax: {
            type: "number",
            min: 0,
            max: 999
        },
        vg1nmaxp: {
            type: "number",
            min: 0,
            max: 999
        },
        ig1max: {
            type: "number",
            min: 0,
            max: 9999
        },
        ig1maxp: {
            type: "number",
            min: 0,
            max: 9999
        },
        cgk: {
            type: "number",
            min: 0,
            max: 999
        },
        cak: {
            type: "number",
            min: 0,
            max: 999
        },
        cga: {
            type: "number",
            min: 0,
            max: 999
        }
    }

    export class TubeUnit extends DataProviding.Dataset {
        constructor(unitInfo: TubeUnitData) {
            var self = this as TubeUnit;
            super(unitInfo);
        }

        protected oncreate() {
            var self = this as TubeUnit;
            var unitInfo = self._datas as TubeUnitData;
        
            // Create fields from model
            self.createFields(tubeUnitModel);
        
            // Fill standard values from model
            self.fillValues(tubeUnitModel, unitInfo);

            // Call base to initialize original values
            super.oncreate();
        }
        public get vamax(): DataProviding.Field {
            return this._fields['vamax'];
        }
        public get vamaxp(): DataProviding.Field {
            return this._fields['vamaxp'];
        }
        public get pamax(): DataProviding.Field {
            return this._fields['pamax'];
        }
        public get vg2max(): DataProviding.Field {
            return this._fields['vg2max'];
        }
        public get vg2maxp(): DataProviding.Field {
            return this._fields['vg2maxp'];
        }
        public get ig2max(): DataProviding.Field {
            return this._fields['ig2max'];
        }
        public get ig2maxp(): DataProviding.Field {
            return this._fields['ig2maxp'];
        }
        public get pg2max(): DataProviding.Field {
            return this._fields['pg2max'];
        }
        public get vhknmax(): DataProviding.Field {
            return this._fields['vhknmax'];
        }
        public get vhkpmax(): DataProviding.Field {
            return this._fields['vhkpmax'];
        }
        public get ikmax(): DataProviding.Field {
            return this._fields['ikmax'];
        }
        public get ikmaxp(): DataProviding.Field {
            return this._fields['ikmaxp'];
        }
        public get vg1nmax(): DataProviding.Field {
            return this._fields['vg1nmax'];
        }
        public get vg1nmaxp(): DataProviding.Field {
            return this._fields['vg1nmaxp'];
        }
        public get ig1max(): DataProviding.Field {
            return this._fields['ig1max'];
        }
        public get ig1maxp(): DataProviding.Field {
            return this._fields['ig1maxp'];
        }
        public get cgk(): DataProviding.Field {
            return this._fields['cgk'];
        }
        public get cak(): DataProviding.Field {
            return this._fields['cak'];
        }
        public get cga(): DataProviding.Field {
            return this._fields['cga'];
        }
    }
}

export = TubeUnit
'use strict';

import DataProviding = require('../../common/ts/data-providing');
import TubeUnit = require('../../common/ts/tube-unit');
import Tube = require('../../common/ts/tube');

module tct {
    export interface OperatorExpression {
        [andOr: string]: Array<FieldExpression>
    }

    export interface FieldExpression {
        [fieldName: string]: RegExp | number | OperatorExpression | FieldExpression
    }

    export interface QueryExpression {
        fieldName: string,
        value?: string | number,
        wholeWord: boolean,
        from?: number,
        to?: number
    }

    export class TubeSearch {
        private _expressions: Array<QueryExpression> = [];
        private _isAdvancedExpression = false;
        private static _searchDataModel: { [name: string]: DataProviding.Field };

        constructor() {
            var self = this as TubeSearch;
        }

        public get queryExpressions() {
            var self = this as TubeSearch;
            return self._expressions;
        }

        public static get searchDataModel() {
            if (!TubeSearch._searchDataModel) {
                TubeSearch._searchDataModel = {};
                Object.keys(Tube.tubeModel).forEach(function(name: string) {
                    TubeSearch._searchDataModel[name] = (<any>Tube.tubeModel)[name];
                });
                Object.keys(TubeUnit.tubeUnitModel).forEach(function(name: string) {
                    var field = (<any>TubeUnit.tubeUnitModel)[name];
                    field.isUnit = true;
                    TubeSearch._searchDataModel[name] = field;
                });
            }
            return TubeSearch._searchDataModel;
        }

        public static FromSearchExpressions(expressions: Array<QueryExpression>) {
            var instance = new TubeSearch();
            instance._expressions = expressions;
            return instance;
        }
        
        // Parse expression to a fieldExpr array and retirn the class when available
        public static FromQueryExpression(expression: string, done: (err: Error, tubeSearch?: TubeSearch) => void) {
            var self = new TubeSearch();

            try {
                // Split by expressions ignore quotes               
                var exps = expression.match(/(?:[^\s=<>"]+|"[^"]*"|[=<>\s])?/g);

                var getExpression = function(index: number, cb: (index: number) => void) {
                    // Add expression to the class collection and return index
                    var addExpression = function(qexp: QueryExpression) {
                        self._expressions.push(qexp);
                        cb(index);
                    }

                    var name = exps[index];
                    if (++index >= exps.length || exps[index] === '' || exps[index] === ' ') {
                        var wholeWord = name.match(/"([^"]*)"/);
                        addExpression({
                            fieldName: '_text',
                            value: wholeWord ? wholeWord[1] : name,
                            wholeWord: wholeWord !== null
                        });
                    } else {
                        if (!exps[index].match('[=<>]')) {
                            done({
                                name: 'invalid_expr',
                                message: 'Invalid search parameters.'
                            });
                            return;
                        }

                        var operator = exps[index];
                        if (++index >= exps.length || exps[index] === '' || exps[index] === ' ') {
                            done({
                                name: 'invalid_expr',
                                message: 'Invalid search parameters.'
                            });
                            return;
                        }

                        var value = exps[index];
                        ++index;
                        self._isAdvancedExpression = true;
                        if (name.match(/^user$/i)) {
                            var wholeWord = value.match(/"([^"]*)"/);
                            addExpression({
                                fieldName: name,
                                value: wholeWord ? wholeWord[1] : value,
                                wholeWord: wholeWord !== null
                            });
                        } else {
                            // Search if a field with this name exist in the data model
                            var field = TubeSearch.searchDataModel[name];
                            if (!field || field.type === 'string') {
                                var wholeWord = value.match(/"([^"]*)"/);
                                addExpression({
                                    fieldName: name,
                                    value: wholeWord ? wholeWord[1] : value,
                                    wholeWord: wholeWord !== null
                                });
                            } else if (field.type === 'number') {
                                var fval = parseFloat(value);
                                if (isNaN(fval)) {
                                    done({
                                        name: 'invalid_value',
                                        message: 'Invalid value parameter: ' + value
                                    });
                                    return;
                                }

                                var fillValue = function(q: QueryExpression) {
                                    if (operator === '<') {
                                        q.to = fval;
                                    } else if (operator === '>') {
                                        q.from = fval;
                                    } else {
                                        q.value = fval;
                                    }
                                }
                            
                                // Search an existing expression
                                var qe: QueryExpression;
                                for (var i = 0; i < self._expressions.length; i++) {
                                    var q = self._expressions[i];
                                    if (q.fieldName === name) {
                                        fillValue(q);
                                        cb(index);
                                        return;
                                    }
                                }

                                qe = {
                                    fieldName: name,
                                    wholeWord: false,
                                }
                                fillValue(qe);
                                addExpression(qe);
                            } else {
                                // lookups, value is an object id
                                addExpression({
                                    fieldName: name,
                                    value: parseInt(value),
                                    wholeWord: false
                                });
                            }
                        }
                    }
                }

                var next = function(iexp: number) {
                    if (++iexp >= exps.length) {
                        done(null, self);
                    } else {
                        getExpression(iexp, next);
                    }
                }

                getExpression(0, next);
            } catch (e) {
                done({
                    name: 'unmanaged_error',
                    message: e.toString()
                })
            }
        }

        public get isAdvancedExpression() {
            var self = this as TubeSearch;
            return self._isAdvancedExpression;
        }

        private addFieldExpression = function(result: FieldExpression, qe: QueryExpression, fields: { [field: string]: number }) {
            var self = this as TubeSearch;

            var getField = function(name: string): { type: string, name: string } {
                
                var dataModel = TubeSearch.searchDataModel;
                var field = dataModel[name] as any;
                if (field && field.isUnit) {
                    fields['_tiar.units.' + name] = 0;
                    name = 'units.' + name;
                }
                return {
                    type: !field ? 'string' : field.type,
                    name: name
                };
            }
                    
            // No field or text field, search anyway
            var fieldExpr: FieldExpression = {};
                                                        
            // Search if a filed with this name exist in the data model
            var fieldInfos = getField(qe.fieldName);
            var value: any;
            if (qe.value !== undefined) {
                if (fieldInfos.type === 'string') {
                    value = TubeSearch.getStringFieldValue(qe);
                } else {
                    value = qe.value;
                }
            } else if (qe.from !== undefined || qe.to !== undefined) {
                value = {};
                if (qe.from !== undefined) {
                    value.$gte = qe.from;
                }
                if (qe.to !== undefined) {
                    value.$lte = qe.to;
                }
            }

            if (value !== undefined) {
                fieldExpr[fieldInfos.name] = value;
                if (!result[fieldInfos.name]) {
                    result[fieldInfos.name] = fieldExpr;
                } else {
                    var opex = result[fieldInfos.name] as OperatorExpression;
                    if (opex['$or']) {
                        opex['$or'].push(fieldExpr);
                    } else {
                        var prevExpr = result[fieldInfos.name] as FieldExpression;
                        opex = result[fieldInfos.name] = {
                            $or: [
                                prevExpr,
                                fieldExpr
                            ]
                        } as OperatorExpression
                    }
                }
            } else {
                throw 'invalid expression';
            }
        }

        private static getStringFieldValue = function(qe: QueryExpression) {
            if (qe.wholeWord) {
                return new RegExp('(^|\\s)' + qe.value + '($|\\s)', 'i');
            } else {
                return new RegExp(qe.value.toString(), 'i');
            }
        }

        public getFieldExpressions = function(done: (err: Error, result?: Array<FieldExpression>, fields?: { [field: string]: number }, users?: OperatorExpression, allUserSearch?: boolean) => void) {
            var self = this as TubeSearch;
            var result: FieldExpression = {};
            var fields: { [field: string]: number } = {};
            var quser: Array<QueryExpression> = [];
            var allUsers = false;

            try {
                self._expressions.map(function(qe: QueryExpression) {
                    if (qe.fieldName.match(/^user$/i)) {
                        if (!allUsers) {
                            var userName = qe.value.toString();
                            allUsers = userName.match(/^all$/i) !== null;
                            if (allUsers) {
                                quser = [];
                            } else {
                                quser.push(qe);
                            }
                        }
                    } else {
                        self.addFieldExpression(result, qe, fields);
                    }
                })
                
                // Map object to and array
                var arr = [] as Array<FieldExpression>;
                Object.keys(result).map(name => arr.push(result[name] as FieldExpression));
                
                // Map users to query expression
                var users: OperatorExpression;
                if (quser.length) {
                    var feexps = [] as Array<FieldExpression>;
                    users = {
                        $or: feexps
                    };

                    var getUserFieldValue = function(qe: QueryExpression) {
                        if (qe.wholeWord) {
                            return new RegExp('^' + qe.value + '$', 'i');
                        } else {
                            return new RegExp(qe.value.toString(), 'i');
                        }
                    }

                    quser.forEach(function(qe) {
                        feexps.push({ email: getUserFieldValue(qe) });
                        feexps.push({ userName: getUserFieldValue(qe) });
                    })
                }

                done(null, arr, fields, users, allUsers);
            } catch (e) {
                done({
                    name: 'unmanaged_error',
                    message: e.toString()
                })
            }
        }

        public toString = function() {
            var self = this as TubeSearch;
            var result: Array<string> = [];

            if (self._expressions && self._expressions.length) {
                self._expressions.map(function(qe: QueryExpression) {
                    if (qe.fieldName === '_text') {
                        if (qe.wholeWord) {
                            result.push('"' + qe.value + '"');
                        } else {
                            result.push(qe.value.toString());
                        }
                    } else {
                        if (qe.from !== undefined && qe.from !== null) {
                            result.push(qe.fieldName + '>' + qe.from);
                        }
                        if (qe.to !== undefined && qe.to !== null) {
                            result.push(qe.fieldName + '<' + qe.to);
                        }
                        if (qe.value !== undefined && qe.value !== null) {
                            if (qe.wholeWord) {
                                result.push(qe.fieldName + '="' + qe.value + '"');
                            } else {
                                result.push(qe.fieldName + '=' + qe.value);
                            }
                        }
                    }
                })
            }

            return result.join(' ');
        }
    }
}

export = tct
import DataProviding = require('../../common/ts/data-providing');
declare module TubeUsage {
    var tubeUsageModel: {
        name: {
            type: string;
            maxLength: number;
            required: boolean;
        };
        note: {
            type: string;
            maxLength: number;
        };
        unit: {
            type: string;
            required: boolean;
        };
        mode: {
            type: string;
            required: boolean;
        };
        va: {
            type: string;
            min: number;
            max: number;
        };
        iazero: {
            type: string;
            min: number;
            max: number;
        };
        vinpp: {
            type: string;
            min: number;
            max: number;
        };
        load: {
            type: string;
            min: number;
            max: number;
        };
        traces: {
            type: string;
        };
        zoom: {
            type: string;
        };
        series: {
            type: string;
        };
    };
    class TubeUsage extends DataProviding.Dataset {
        constructor(usageInfo: TubeUsageData);
        protected oncreate(): void;
        name: DataProviding.Field;
        note: DataProviding.Field;
        unit: DataProviding.Field;
        mode: DataProviding.Field;
        va: DataProviding.Field;
        iazero: DataProviding.Field;
        vinpp: DataProviding.Field;
        load: DataProviding.Field;
        traces: DataProviding.Field;
        zoom: DataProviding.Field;
        series: DataProviding.Field;
        effectiveLoad: number;
    }
}
export = TubeUsage;

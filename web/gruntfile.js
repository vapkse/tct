'use strict';

module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-ts');
    grunt.loadNpmTasks('grunt-contrib-less');

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Automatically load required Grunt tasks
    require('jit-grunt')(grunt);

    // Configurable paths for the application
    var path = require('path');
    var config = require('./config');

    // Define the configuration for all the tasks
    grunt.initConfig({
        // Express config
        express: {
            options: {
                script: path.resolve(config.path.server, 'app.js')
            },
            dev: {
                options: {
                    node_env: 'dev',
                    debug: config.dev.debugPort
                }
            },
            prod: {
                options: {
                    node_env: 'prod',
                }
            },
            test: {
                options: {
                    node_env: 'test'
                }
            }
        },

        less: {
            options: {
                paths: [path.resolve(config.path.app, 'less')],
                strictMath: false
            },
            files: {
                expand: true,
                cwd: path.resolve(config.path.app, 'less'),
                src: ['*.less'],
                dest: path.resolve(config.path.app, 'css'),
                ext: '.css',
            }
        },

        // Typescript compiler
        ts: {
            server: {
                src: [path.resolve(config.path.server, '{,*/}{,*/}*.ts'),
                    '!' + path.resolve(config.path.server, '{,*/}{,*/}*.d.ts'),
                    path.resolve(config.path.server, 'typings/tsd.d.ts'),
                    path.resolve(config.path.server, 'common/tsd.d.ts')],
                options: {
                    module: "commonjs",
                    target: 'es5',
                    sourceMap: true,
                    noImplicitAny: true,
                    removeComments: true,
                    experimentalDecorators: true,
                    preserveConstEnums: true,
                    declaration: true
                }
            },
            client: {
                src: [path.resolve(config.path.app, 'ts/{,*/}{,*/}*.ts'),
                    '!' + path.resolve(config.path.app, 'ts/{,*/}{,*/}*.d.ts'),
                    path.resolve(config.path.app, 'typings/tsd.d.ts'),
                    path.resolve(config.path.app, 'common/tsd.d.ts')],
                options: {
                    module: "amd",
                    target: 'es5',
                    sourceMap: false,
                    noImplicitAny: true,
                    experimentalDecorators: true,
                    removeComments: true,
                    preserveConstEnums: true,
                    declaration: true,
                }
            },
            commoncli: {
                src: [path.resolve(config.path.common, '{,*/}{,*/}*.ts'),
                    '!' + path.resolve(config.path.common, '{,*/}{,*/}*.d.ts'),
                    path.resolve(config.path.common, 'typings/tsd.d.ts')],
                dest: path.resolve(config.path.app, 'common'),
                options: {
                    module: "amd",
                    target: 'es5',
                    sourceMap: false,
                    noImplicitAny: true,
                    experimentalDecorators: true,
                    removeComments: false,
                    preserveConstEnums: true,
                    declaration: true,
                }
            },
            commonserv: {
                src: [path.resolve(config.path.common, '{,*/}{,*/}*.ts'),
                    '!' + path.resolve(config.path.common, '{,*/}{,*/}*.d.ts'),
                    path.resolve(config.path.common, 'typings/tsd.d.ts')],
                dest: path.resolve(config.path.server, 'common'),
                options: {
                    module: "commonjs",
                    target: 'es5',
                    sourceMap: false,
                    noImplicitAny: true,
                    experimentalDecorators: true,
                    removeComments: false,
                    preserveConstEnums: true,
                    declaration: true,
                }
            }
        },

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            options: {
                livereload: config.dev.livereload
            },
            less: {
                files: [path.resolve(config.path.app, 'less/{,*/}{,*/}*.less')],
                tasks: ['less'],
            },
            tscli: {
                files: [path.resolve(config.path.app, 'ts/{,*/}{,*/}*.ts'),
                    '!' + path.resolve(config.path.app, 'ts/{,*/}{,*/}*.d.ts'),
                    path.resolve(config.path.app, 'typings/tsd.d.ts'),
                    path.resolve(config.path.app, 'common/tsd.d.ts')],
                tasks: ['ts:client'],
            },
            tsserv: {
                files: [path.resolve(config.path.server, '{,*/}{,*/}*.ts'),
                    '!' + path.resolve(config.path.server, '{,*/}{,*/}*.d.ts'),
                    path.resolve(config.path.server, 'typings/tsd.d.ts'),
                    path.resolve(config.path.server, 'common/tsd.d.ts')],
                tasks: ['ts:server'],
            },
            tscommon: {
                files: [path.resolve(config.path.common, 'ts/{,*/}{,*/}*.ts'),
                    '!' + path.resolve(config.path.common, 'ts/{,*/}{,*/}*.d.ts'),
                    path.resolve(config.path.common, 'typings/tsd.d.ts')],
                tasks: ['ts:commoncli', 'ts:commonserv', 'ts:server', 'ts:client'],
            },
            js: {
                files: [path.resolve(config.path.server, '{,*/}{,*/}*.js')],
                tasks: ['newer:jshint:all'],
            },
            jsTest: {
                files: [path.resolve(config.path.test, 'spec/{,*/}*.js')],
                tasks: ['newer:jshint:test', 'karma']
            },
            css: {
                files: [path.resolve(config.path.app, 'css/{,*/}*.css')],
                tasks: ['newer:copy:css', 'autoprefixer']
            },
            server: {
                files: ['gruntfile.js', path.resolve(config.path.server, '{,*/}*.js')],
                tasks: ['express:dev'],
                options: {
                    spawn: false
                }
            },
            livereload: {
                options: {
                    livereload: config.dev.livereload,
                },
                files: [
                    path.resolve(config.path.app, '*.html'),
                    path.resolve(config.path.app, 'widgets/{,*/}*.html'),
                    path.resolve(config.path.app, 'images/*.{png,jpg,jpeg,gif,webp,svg}')
                ]
            }
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: {
                src: [
                    'gruntfile.js',
                    path.resolve(config.path.server, '{,*/}*.js')
                ]
            },
            test: {
                options: {
                    jshintrc: path.resolve(config.path.test, '.jshintrc')
                },
                src: [path.resolve(config.path.test, 'spec/{,*/}*.js')]
            }
        },

        // Empties folders to start fresh
        clean: {
            server: config.path.temp,
            dependencies: ['bower_components', 'node_modules']
        },

        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            },
            server: {
                options: {
                    map: true,
                },
                files: [{
                    expand: true,
                    cwd: path.resolve(config.path.temp, 'css/'),
                    src: '{,*/}*.css',
                    dest: path.resolve(config.path.temp, 'css/')
                }]
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            css: {
                expand: true,
                cwd: path.resolve(config.path.app, 'css'),
                dest: path.resolve(config.path.temp, 'css'),
                src: '{,*/}*.css'
            }
        },

        // Run some tasks in parallel to speed up the build process
        concurrent: {
            server: [
                'copy:css'
            ],
            test: [
                'copy:css'
            ]
        },

        // Test settings
        karma: {
            unit: {
                configFile: path.resolve(config.path.test, 'karma.conf.js'),
                singleRun: true
            }
        }
    });

    grunt.registerTask('dev', 'Compile then start an express web server for dev', function() {
        grunt.task.run([
            //'clean:server',
            //'concurrent:server',
            //'autoprefixer:server',
            'express:dev',
            'watch'
        ]);
    });

    grunt.registerTask('prod', 'Compile then start an express web server for prod', function() {
        grunt.task.run([
            //'clean:server',
            'express:prod',
            'watch'
        ]);
    });

    grunt.registerTask('compile', 'Compile typescripts', function() {
        grunt.task.run([
            'ts:commonserv',
            'ts:commoncli',
            'ts:server',
            'ts:client',
        ]);
    });

    grunt.registerTask('hint', 'Run jshint', function() {
        grunt.task.run([
            'newer:jshint:all'
        ]);
    });

    grunt.registerTask('test', [
        'clean:server',
        'concurrent:test',
        'autoprefixer:server',
        'express:test',
        'karma'
    ]);
};

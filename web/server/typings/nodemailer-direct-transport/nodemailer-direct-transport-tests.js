var directTransport = require('nodemailer-direct-transport');
var opts = {
    name: "harry"
};
var transport = directTransport(opts);
var mailOptions = {
    from: 'Fred Foo ✔ <foo@blurdybloop.com>',
    to: 'bar@blurdybloop.com, baz@blurdybloop.com',
    subject: 'Hello ✔',
    text: 'Hello world ✔',
    html: '<b>Hello world ✔</b>'
};
transport.send(mailOptions, function (error, info) {
});
//# sourceMappingURL=nodemailer-direct-transport-tests.js.map
var smtpTransport = require('nodemailer-smtp-transport');
var opts = {
    host: "localhost",
    port: 25
};
var transport = smtpTransport(opts);
var mailOptions = {
    from: 'Fred Foo ✔ <foo@blurdybloop.com>',
    to: 'bar@blurdybloop.com, baz@blurdybloop.com',
    subject: 'Hello ✔',
    text: 'Hello world ✔',
    html: '<b>Hello world ✔</b>'
};
transport.send(mailOptions, function (error, info) {
});
//# sourceMappingURL=nodemailer-smtp-transport-tests.js.map
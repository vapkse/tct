var passport = require('passport');
var twitter = require('passport-twitter');
var User = {
    findOrCreate: function (id, provider, callback) {
        callback(null, { username: 'james' });
    }
};
passport.use(new twitter.Strategy({
    consumerKey: process.env.PASSPORT_TWITTER_CONSUMER_KEY,
    consumerSecret: process.env.PASSPORT_TWITTER_CONSUMER_SECRET,
    callbackURL: process.env.PASSPORT_TWITTER_CALLBACK_URL
}, function (accessToken, refreshToken, profile, done) {
    User.findOrCreate(profile.id, profile.provider, function (err, user) {
        if (err) {
            return done(err);
        }
        done(null, user);
    });
}));
//# sourceMappingURL=passport-twitter-tests.js.map
var passport = require('passport');
var facebook = require('passport-facebook-token');
var User = {
    findOrCreate: function (id, provider, callback) {
        callback(null, { username: 'ray' });
    }
};
var options = {
    clientID: process.env.PASSPORT_FACEBOOK_CLIENT_ID,
    clientSecret: process.env.PASSPORT_FACEBOOK_CLIENT_SECRET
};
function verify(accessToken, refreshToken, profile, done) {
    User.findOrCreate(profile.id, profile.provider, function (err, user) {
        if (err) {
            return done(err);
        }
        done(null, user);
    });
}
passport.use(new facebook.Strategy(options, verify));
//# sourceMappingURL=passport-facebook-token-tests.js.map
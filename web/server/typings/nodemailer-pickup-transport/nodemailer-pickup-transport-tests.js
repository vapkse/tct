var nodemailer = require('nodemailer');
var pickupTransport = require('nodemailer-pickup-transport');
var opts = {
    directory: 'C:\\inetpub\\mailroot\\Pickup'
};
var transport = pickupTransport(opts);
var transporter = nodemailer.createTransport(transport);
var mailOptions = {
    from: 'Fred Foo ✔ <foo@blurdybloop.com>',
    to: 'bar@blurdybloop.com, baz@blurdybloop.com',
    subject: 'Hello ✔',
    text: 'Hello world ✔',
    html: '<b>Hello world ✔</b>'
};
transporter.sendMail(mailOptions, function (error, info) {
});
//# sourceMappingURL=nodemailer-pickup-transport-tests.js.map
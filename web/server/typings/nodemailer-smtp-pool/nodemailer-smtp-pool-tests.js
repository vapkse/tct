var smtpPool = require('nodemailer-smtp-pool');
var opts = {
    maxConnections: 5,
    maxMessages: 10
};
var transport = smtpPool(opts);
var mailOptions = {
    from: 'Fred Foo ✔ <foo@blurdybloop.com>',
    to: 'bar@blurdybloop.com, baz@blurdybloop.com',
    subject: 'Hello ✔',
    text: 'Hello world ✔',
    html: '<b>Hello world ✔</b>'
};
transport.send(mailOptions, function (error, info) {
});
//# sourceMappingURL=nodemailer-smtp-pool-tests.js.map
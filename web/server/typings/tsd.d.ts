/// <reference path="express/express.d.ts"/>
/// <reference path="express-session/express-session.d.ts"/>
/// <reference path="cookie-parser/cookie-parser.d.ts"/>
/// <reference path="cheerio/cheerio.d.ts"/>
/// <reference path="archiver/archiver.d.ts"/>
/// <reference path="passport/passport.d.ts"/>
/// <reference path="split/split.d.ts"/>
/// <reference path="passport-twitter/passport-twitter.d.ts"/>
/// <reference path="passport-facebook/passport-facebook.d.ts"/>
/// <reference path="passport-google-oauth/passport-google-oauth.d.ts"/>
/// <reference path="body-parser/body-parser.d.ts"/>
/// <reference path="bcryptjs/bcryptjs.d.ts" />
/// <reference path="node-uuid/node-uuid.d.ts" />
/// <reference path="connect-flash/connect-flash.d.ts" />
/// <reference path="socket.io/socket.io.d.ts" />
/// <reference path="gruntjs/gruntjs.d.ts" />
/// <reference path="nodemailer/nodemailer.d.ts" />
/// <reference path="ua-parser-js/ua-parser-js.d.ts" />
/// <reference path="../../common/typings/tsd.d.ts" />

interface FileCache {
    modifiedTime: number,
    content: any
}

interface SocketInfo {
    io: SocketIO.Socket,
    event: string
}

interface SocketInfos {
    [socketid: string]: SocketInfo
}

interface FileVersion {
    fileName: string,
    version: {
        major: number,
        minor: number
    }
}

declare module Express {
    interface Request {
        socket: any
    }
}

var passport = require('passport');
var google = require('passport-google-oauth');
var User = {
    findOrCreate: function (id, provider, callback) {
        callback(null, { username: 'james' });
    }
};
passport.use(new google.OAuthStrategy({
    consumerKey: process.env.GOOGLE_CONSUMER_KEY,
    consumerSecret: process.env.GOOGLE_CONSUMER_SECRET,
    callbackURL: process.env.PASSPORT_GOOGLE_CALLBACK_URL
}, function (accessToken, refreshToken, profile, done) {
    User.findOrCreate(profile.id, profile.provider, function (err, user) {
        if (err) {
            return done(err);
        }
        done(null, user);
    });
}));
passport.use(new google.OAuth2Strategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.PASSPORT_GOOGLE_CALLBACK_URL
}, function (accessToken, refreshToken, profile, done) {
    User.findOrCreate(profile.id, profile.provider, function (err, user) {
        if (err) {
            return done(err);
        }
        done(null, user);
    });
}));
//# sourceMappingURL=passport-google-oauth-tests.js.map
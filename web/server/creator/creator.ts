'use strict';

import express = require('express');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');
import config = require('../../config');
import logger = require('../utils/logger');

var app = express();

app.set('views', config.path.views);

app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.get('/', function(req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        user: req.user,
        errors: req.flash('error'),
        warnings: req.flash('warning'),
        infos: req.flash('info')
    }

    logger.log(req, 'grey', 'Route', 'Navigate to graph creator');
    res.render('creator.html', options);
});

export = app;

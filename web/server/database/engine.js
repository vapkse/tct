'use strict';
var cfg = require('../../config');
var versionsModule = require('./modules/versions');
var tubePinsModule = require('./modules/tube-pins');
var tubeBasesModule = require('./modules/tube-bases');
var tubeTypesModule = require('./modules/tube-types');
var tubesModule = require('./modules/tubes');
var translationsModule = require('./modules/translations');
var users = require('./modules/users');
var news = require('./modules/news');
var DBEngine = (function () {
    function DBEngine() {
    }
    Object.defineProperty(DBEngine, "news", {
        get: function () {
            if (!DBEngine.db) {
                throw 'Database not initialized.';
            }
            return new news.News(DBEngine.db);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DBEngine, "users", {
        get: function () {
            if (!DBEngine.db) {
                throw 'Database not initialized.';
            }
            return new users.Users(DBEngine.db);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DBEngine, "tubePins", {
        get: function () {
            if (!DBEngine.db) {
                throw 'Database not initialized.';
            }
            return new tubePinsModule.TubePins(DBEngine.db);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DBEngine, "tubeBases", {
        get: function () {
            if (!DBEngine.db) {
                throw 'Database not initialized.';
            }
            return new tubeBasesModule.TubeBases(DBEngine.db);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DBEngine, "tubeTypes", {
        get: function () {
            if (!DBEngine.db) {
                throw 'Database not initialized.';
            }
            return new tubeTypesModule.TubeTypes(DBEngine.db);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DBEngine, "tubes", {
        get: function () {
            if (!DBEngine.db) {
                throw 'Database not initialized.';
            }
            return new tubesModule.Tubes(DBEngine.db);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DBEngine, "translations", {
        get: function () {
            if (!DBEngine.db) {
                throw 'Database not initialized.';
            }
            return new translationsModule.Translations(DBEngine.db);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DBEngine, "versions", {
        get: function () {
            if (!DBEngine.db) {
                throw 'Database not initialized.';
            }
            return new versionsModule.Versions(DBEngine.db);
        },
        enumerable: true,
        configurable: true
    });
    DBEngine.getDB = function () {
        if (!DBEngine.staticdb) {
            if (cfg.db.engine === 'mongodb') {
                var mcfg = cfg.db.mongo;
                DBEngine.engine = require('mongodb');
                DBEngine.staticdb = new DBEngine.engine.Db(mcfg.db, new DBEngine.engine.Server(mcfg.host, mcfg.port, mcfg.opts), {
                    native_parser: false,
                    safe: true
                });
            }
            else {
                var tcfg = cfg.db.tingo;
                DBEngine.engine = require("tingodb")(tcfg.opts);
                DBEngine.staticdb = new DBEngine.engine.Db(tcfg.path, {});
            }
        }
        return DBEngine.staticdb;
    };
    DBEngine.getObjectID = function () {
        return DBEngine.engine.ObjectID;
    };
    DBEngine.compressDB = function (colname, cbdone) {
        DBEngine.db.collection(colname, { strict: true }, function (err, col) {
            if (err) {
                cbdone(err);
                return;
            }
            col.compactCollection(function (err) {
                cbdone(err);
            });
        });
    };
    DBEngine.initDB = function (cbdone) {
        var initNews = function () {
            DBEngine.news.initCollection(function (err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                cbdone(DBEngine.db);
            });
        };
        var initVersions = function () {
            DBEngine.versions.initCollection(function (err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                initNews();
            });
        };
        var initTubes = function () {
            DBEngine.tubes.initCollection(function (err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                initVersions();
            });
        };
        var initTypes = function () {
            DBEngine.tubeTypes.initCollection(function (err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                initTubes();
            });
        };
        var initBases = function () {
            DBEngine.tubeBases.initCollection(function (err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                initTypes();
            });
        };
        var initPinouts = function () {
            DBEngine.tubePins.initCollection(function (err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                initBases();
            });
        };
        var initUsers = function () {
            DBEngine.users.initCollection(function (err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                initPinouts();
            });
        };
        DBEngine.getDB().open(function (err, db) {
            if (err) {
                cbdone(null, err);
                return;
            }
            DBEngine.db = db;
            initUsers();
        });
    };
    return DBEngine;
})();
module.exports = DBEngine;
//# sourceMappingURL=engine.js.map
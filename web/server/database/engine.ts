'use strict';

import cfg = require('../../config');
import versionsModule = require('./modules/versions');
import tubePinsModule = require('./modules/tube-pins');
import tubeBasesModule = require('./modules/tube-bases');
import tubeTypesModule = require('./modules/tube-types');
import tubesModule = require('./modules/tubes');
import translationsModule = require('./modules/translations');
import users = require('./modules/users');
import news = require('./modules/news');

class DBEngine {
    private static staticdb: dbengine.Db;
    private static db: dbengine.Db;
    private static engine: any;

    private static getDB = function() {
        if (!DBEngine.staticdb) {
            if (cfg.db.engine === 'mongodb') {
                var mcfg = cfg.db.mongo;
                DBEngine.engine = require('mongodb');
                DBEngine.staticdb = new DBEngine.engine.Db(mcfg.db, new DBEngine.engine.Server(mcfg.host, mcfg.port, mcfg.opts), {
                    native_parser: false,
                    safe: true
                });
            } else {
                var tcfg = cfg.db.tingo;
                DBEngine.engine = require("tingodb")(tcfg.opts);
                DBEngine.staticdb = new DBEngine.engine.Db(tcfg.path, {});
            }
        }

        return DBEngine.staticdb;
    }

    static getObjectID = function() {
        return DBEngine.engine.ObjectID;
    }

    static compressDB = function(colname: string, cbdone: (err?: Error) => void) {
        DBEngine.db.collection(colname, { strict: true }, function(err, col) {
            if (err) {
                cbdone(err);
                return;
            }

            col.compactCollection(function(err) {
                cbdone(err);
            })
        })
    }

    static initDB = function(cbdone: (db: dbengine.Db, err?: Error) => void) {
        var initNews = function() {
            DBEngine.news.initCollection(function(err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                cbdone(DBEngine.db);
            })
        }
        
        var initVersions = function() {
            DBEngine.versions.initCollection(function(err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                initNews();
            })
        }

        var initTubes = function() {
            DBEngine.tubes.initCollection(function(err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                initVersions();
            })
        }

        var initTypes = function() {
            DBEngine.tubeTypes.initCollection(function(err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                initTubes();
            })
        }

        var initBases = function() {
            DBEngine.tubeBases.initCollection(function(err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                initTypes();
            })
        }

        var initPinouts = function() {
            DBEngine.tubePins.initCollection(function(err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                initBases();
            })
        }

        var initUsers = function() {
            DBEngine.users.initCollection(function(err) {
                if (err) {
                    cbdone(null, err);
                    return;
                }
                initPinouts();
            })
        }
        
        // Open DB
        DBEngine.getDB().open(function(err, db) {
            if (err) {
                cbdone(null, err);
                return;
            }
            DBEngine.db = db;
            initUsers()
        })
    }
    
    // Specialized modules
    static get news() {
        if (!DBEngine.db) {
            throw 'Database not initialized.'
        }
        return new news.News(DBEngine.db);
    }

    static get users() {
        if (!DBEngine.db) {
            throw 'Database not initialized.'
        }
        return new users.Users(DBEngine.db);
    }

    static get tubePins() {
        if (!DBEngine.db) {
            throw 'Database not initialized.'
        }
        return new tubePinsModule.TubePins(DBEngine.db);
    }

    static get tubeBases() {
        if (!DBEngine.db) {
            throw 'Database not initialized.'
        }
        return new tubeBasesModule.TubeBases(DBEngine.db);
    }

    static get tubeTypes() {
        if (!DBEngine.db) {
            throw 'Database not initialized.'
        }
        return new tubeTypesModule.TubeTypes(DBEngine.db);
    }

    static get tubes() {
        if (!DBEngine.db) {
            throw 'Database not initialized.'
        }
        return new tubesModule.Tubes(DBEngine.db);
    }

    static get translations() {
        if (!DBEngine.db) {
            throw 'Database not initialized.'
        }
        return new translationsModule.Translations(DBEngine.db);
    }
    
    static get versions() {
        if (!DBEngine.db) {
            throw 'Database not initialized.'
        }
        return new versionsModule.Versions(DBEngine.db);
    }
}

export = DBEngine; 

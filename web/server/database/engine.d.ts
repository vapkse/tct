import versionsModule = require('./modules/versions');
import tubePinsModule = require('./modules/tube-pins');
import tubeBasesModule = require('./modules/tube-bases');
import tubeTypesModule = require('./modules/tube-types');
import tubesModule = require('./modules/tubes');
import translationsModule = require('./modules/translations');
import users = require('./modules/users');
import news = require('./modules/news');
declare class DBEngine {
    private static staticdb;
    private static db;
    private static engine;
    private static getDB;
    static getObjectID: () => any;
    static compressDB: (colname: string, cbdone: (err?: Error) => void) => void;
    static initDB: (cbdone: (db: dbengine.Db, err?: Error) => void) => void;
    static news: news.News;
    static users: users.Users;
    static tubePins: tubePinsModule.TubePins;
    static tubeBases: tubeBasesModule.TubeBases;
    static tubeTypes: tubeTypesModule.TubeTypes;
    static tubes: tubesModule.Tubes;
    static translations: translationsModule.Translations;
    static versions: versionsModule.Versions;
}
export = DBEngine;

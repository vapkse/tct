declare module dbmodules {
    class News {
        private db;
        private colname;
        constructor(db: dbengine.Db);
        addNews(news: NewsData, done: (err: Error, news?: NewsData) => void): void;
        updateNews(news: NewsData, done: (err: Error, news?: NewsData) => void): void;
        deleteNews(id: string, done: (err?: Error) => void): void;
        getLatestNews(done: (err: Error, news?: Array<NewsData>) => void): void;
        initCollection(done: (err?: Error) => void): void;
    }
}
export = dbmodules;

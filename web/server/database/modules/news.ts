'use strict';

import cfg = require('../../../config');
import tubesModule = require('./tubes');
import News = require('../../common/ts/news'); // Namespace only

module dbmodules {
    export class News {
        private db: dbengine.Db;
        private colname = 'news.json';

        constructor(db: dbengine.Db) {
            var self = <News>this;
            self.db = db;
        }

        public addNews(news: NewsData, done: (err: Error, news?: NewsData) => void) {
            var self = <News>this;

            // Open collection
            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                var save = function() {
                    col.insert(news, function(err: Error, result: NewsData) {
                        done(err, result);
                    })
                }

                // Delete previous news with the same text
                col.find({ text: news.text }).toArray(function(err: Error, old: Array<NewsData>) {
                    if (err) {
                        done(err);
                        return;
                    }

                    var toDelete = {
                        $or: [] as Array<any>
                    }

                    var sparams = JSON.stringify(news.params);
                    old.forEach(function(value) {
                        if (JSON.stringify(value.params) === sparams) {
                            toDelete.$or.push({ '_id': value._id });
                        }
                    })

                    if (toDelete.$or.length === 0) {
                        save();
                    } else {
                        col.remove(toDelete, function(err, result) {
                            save();
                        })
                    }
                })
            })
        }

        public updateNews(news: NewsData, done: (err: Error, news?: NewsData) => void) {
            var self = <News>this;

            if (!news) {
                done({
                    name: 'msg-newsismand',
                    message: 'News is mandatory.'
                });
                return;
            }

            if (!news._id) {
                done({
                    name: 'msg-newsidismand',
                    message: 'News ID is mandatory.'
                });
                return;
            }

            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                col.findOne({ '_id': news._id }, function(err: Error, dbn: NewsData) {
                    if (err) {
                        done(err);
                        return;
                    }

                    if (!news) {
                        done({
                            name: 'msg-newsnotfound',
                            message: 'News not found.'
                        });
                        return;
                    }

                    // Merge news
                    var extend = require('extend');
                    delete news._id;
                    var merged = extend(true, {}, dbn, news) as NewsData;

                    // Update news
                    col.save(merged, function(err, result) {
                        done(err, merged);
                    })
                })
            })
        }

        public deleteNews(id: string, done: (err?: Error) => void) {
            var self = <News>this;

            if (!id) {
                done({
                    name: 'msg-newsidismand',
                    message: 'News ID is mandatory.'
                });
                return;
            }

            // Open collection
            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                }

                // find user        
                col.findOne({ '_id': id }, function(err: Error, news: NewsData) {
                    if (err) {
                        done(err);
                    } else if (!news) {
                        done();
                    } else {
                        col.remove({ '_id': id }, function(err, result) {
                            done(err);
                        })
                    }
                })
            })
        }

        public getLatestNews(done: (err: Error, news?: Array<NewsData>) => void) {
            var self = <News>this;

            // Open collection
            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                col.find().toArray(function(err: Error, news: Array<NewsData>) {
                    news = news && news.sort(function(a, b) {
                        var da = new Date(a.date);
                        var db = new Date(b.date);
                        return da < db ? 1 : -1;
                    })
                    done(null, news);
                })
            })
        }

        public initCollection(done: (err?: Error) => void) {
            var self = <News>this;

            self.db.collection(self.colname, { strict: true }, function(err, collection) {
                if (err) {
                    // Create collection
                    try {
                        self.db.createCollection(self.colname, function(err, col) {
                            if (err) {
                                done(err);
                            } else {
                                var tubes = new tubesModule.Tubes(self.db);
                                tubes.getTubesCount(function(err, tubesCount) {
                                    // insert default entry
                                    var news: NewsData = {
                                        date: new Date().toJSON(),
                                        text: 'Database created, ' + String(tubesCount) + ' tubes avalaible.',
                                        uid: 'news0',
                                    }
                                    col.insert(news, function(err, col) {
                                        done(err);
                                    })
                                })
                            }
                        })
                    } catch (e) {
                        done(e);
                    }
                } else {
                    done(err);
                }
            })
        }
    }
}

export = dbmodules;
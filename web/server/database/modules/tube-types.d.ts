declare module dbmodules {
    class TubeTypes {
        private db;
        private colname;
        constructor(db: dbengine.Db);
        getTypes(selector: Object, done: (err: Error, result?: Array<any>) => void): void;
        initCollection(done: (err?: Error) => void): void;
    }
}
export = dbmodules;

'use strict';
var cfg = require('../../../config');
var fs = require('fs');
var path = require('path');
var dbmodules;
(function (dbmodules) {
    var Versions = (function () {
        function Versions(db) {
            this.colname = 'versions.json';
            var self = this;
            self.db = db;
        }
        Versions.prototype.updateVersion = function (filename, major, minor, done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                var version = {
                    fileName: filename,
                    version: {
                        major: major,
                        minor: minor
                    }
                };
                col.update({ fileName: filename }, version, function (err, result) {
                    done(err, result);
                });
            });
        };
        Versions.prototype.getVersion = function (selector, done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.find(selector).toArray(function (err, result) {
                    done(err, result);
                });
            });
        };
        Versions.prototype.getVersions = function (done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.find().toArray(function (err, result) {
                    done(err, result);
                });
            });
        };
        Versions.prototype.initCollection = function (done) {
            var self = this;
            var next = function (col) {
                var dataPath = path.resolve(cfg.path.data);
                fs.readdir(dataPath, function (err, files) {
                    if (err) {
                        done(err);
                        return;
                    }
                    col.find().toArray(function (err, versionsData) {
                        if (err) {
                            done(err);
                            return;
                        }
                        var versions = {};
                        versionsData.map(function (ver) { return versions[ver.fileName] = ver; });
                        var checkFile = function (index) {
                            if (index >= files.length) {
                                done();
                                return;
                            }
                            var filename = files[index];
                            if (versions[filename]) {
                                checkFile(index + 1);
                            }
                            else {
                                var fileVer = {
                                    fileName: filename,
                                    version: {
                                        major: 1,
                                        minor: 0
                                    }
                                };
                                if (/.*\-tubes\.json$/.test(filename) && versions['tubes.json']) {
                                    fileVer.version = versions['tubes.json'].version;
                                }
                                col.insert(fileVer, function (err, col) {
                                    if (err) {
                                        done(err);
                                        return;
                                    }
                                    checkFile(index + 1);
                                });
                            }
                        };
                        checkFile(0);
                    });
                });
            };
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    self.db.createCollection(self.colname, function (err, newcol) {
                        if (err) {
                            done(err);
                            return;
                        }
                        next(newcol);
                    });
                }
                else {
                    next(col);
                }
            });
        };
        return Versions;
    })();
    dbmodules.Versions = Versions;
})(dbmodules || (dbmodules = {}));
module.exports = dbmodules;
//# sourceMappingURL=versions.js.map
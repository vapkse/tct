declare module dbmodules {
    class TubeBases {
        private db;
        private colname;
        constructor(db: dbengine.Db);
        getBases(selector: Object, done: (err: Error, result?: any) => void): void;
        initCollection(done: (err?: Error) => void): void;
    }
}
export = dbmodules;

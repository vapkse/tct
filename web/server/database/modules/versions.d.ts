declare module dbmodules {
    class Versions {
        private db;
        private colname;
        constructor(db: dbengine.Db);
        updateVersion(filename: string, major: number, minor: number, done: (err: Error, result?: any) => void): void;
        getVersion(selector: Object, done: (err: Error, result?: any) => void): void;
        getVersions(done: (err: Error, result?: any) => void): void;
        initCollection(done: (err?: Error) => void): void;
    }
}
export = dbmodules;

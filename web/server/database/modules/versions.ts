'use strict';

import cfg = require('../../../config');
import fs = require('fs');
import path = require('path');

module dbmodules {
    export class Versions {
        private db: dbengine.Db;
        private colname = 'versions.json';

        constructor(db: dbengine.Db) {
            var self = <Versions>this;
            self.db = db;
        }

        public updateVersion(filename: string, major: number, minor: number, done: (err: Error, result?: any) => void) {
            var self = <Versions>this;

            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                var version = {
                    fileName: filename,
                    version: {
                        major: major,
                        minor: minor
                    }
                }
                    
                col.update({ fileName: filename }, version, function(err, result) {
                    done(err, result);
                })
            })
        }

        public getVersion(selector: Object, done: (err: Error, result?: any) => void) {
            var self = <Versions>this;

            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.find(selector).toArray(function(err, result) {
                    done(err, result);
                })
            })
        }

        public getVersions(done: (err: Error, result?: any) => void) {
            var self = <Versions>this;

            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.find().toArray(function(err, result) {
                    done(err, result);
                })
            })
        }

        public initCollection(done: (err?: Error) => void) {
            var self = <Versions>this;

            var next = function(col: dbengine.Collection) {
                var dataPath = path.resolve(cfg.path.data);
                fs.readdir(dataPath, function(err, files) {
                    if (err) {
                        done(err);
                        return;
                    }

                    // Get all file versions
                    col.find().toArray(function(err: Error, versionsData: Array<FileVersion>) {
                        if (err) {
                            done(err);
                            return;
                        }

                        var versions: {
                            [filename: string]: FileVersion
                        } = {};
                        versionsData.map(ver => versions[ver.fileName] = ver);

                        var checkFile = function(index: number) {
                            if (index >= files.length) {
                                done();
                                return;
                            }
                            var filename = files[index];
                            if (versions[filename]) {
                                checkFile(index + 1);
                            } else {
                                var fileVer = {
                                    fileName: filename,
                                    version: {
                                        major: 1,
                                        minor: 0
                                    }
                                } as FileVersion

                                if (/.*\-tubes\.json$/.test(filename) && versions['tubes.json']) {
                                    // User tube file has the same version of the tube file
                                    fileVer.version = versions['tubes.json'].version;
                                }

                                col.insert(fileVer, function(err, col) {
                                    if (err) {
                                        done(err);
                                        return;
                                    }
                                    checkFile(index + 1);
                                })
                            }
                        }
                        checkFile(0);
                    })
                })
            }

            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    // Create collection
                    self.db.createCollection(self.colname, function(err, newcol) {
                        if (err) {
                            done(err);
                            return;
                        }
                        next(newcol);
                    })
                } else {
                    next(col);
                }
            })
        }
    }
}

export = dbmodules;
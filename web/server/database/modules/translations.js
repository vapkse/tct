'use strict';
var cfg = require('../../../config');
var fs = require('fs');
var path = require('path');
var cacheProvider = require('../../utils/cache');
var cache = cacheProvider.Cache;
var extend = require('extend');
var dbmodules;
(function (dbmodules) {
    var Translations = (function () {
        function Translations(db) {
            this.colname = 'translations.json';
            var self = this;
            self.db = db;
        }
        Translations.prototype.addProposal = function (proposal, done) {
            var self = this;
            var colname = 'i18n_proposal.json';
            if (!proposal.original || !proposal.proposal || !proposal.locale || !proposal.userName) {
                done({
                    name: 'wrongparams',
                    message: 'Wrong Parameters.'
                });
                return;
            }
            self.db.collection(colname, { strict: false }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.insert(proposal, function (err, docs) {
                    done(err);
                });
            });
        };
        Translations.prototype.updateTranslations = function (locale, translations, done) {
            var self = this;
            var colname = 'i18n_' + locale + '.json';
            if (translations.length === 0) {
                done({
                    name: 'wrongparams',
                    message: 'Wrong Parameters.'
                });
                return;
            }
            self.db.collection(colname, { strict: false }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                var ids = [];
                translations.map(function (t) { return ids.push(t._id); });
                col.remove({
                    '_id': {
                        $in: ids
                    }
                }, function (e, docs) {
                    col.insert(translations, function (err, docs) {
                        cache.clear(locale);
                        done(err);
                    });
                });
            });
        };
        Translations.prototype.editTranslations = function (editedRecord, sl, newid, done) {
            var self = this;
            var updateTables = function () {
                fs.readdir(path.resolve(cfg.path.data), function (err, items) {
                    if (err) {
                        done(err);
                    }
                    else {
                        var selector = { '_id': editedRecord._id };
                        var updateTable = function (index) {
                            if (index >= items.length) {
                                cache.clear(sl);
                                done(null, editedRecord);
                                return;
                            }
                            var colname = items[index];
                            var match = /i18n\_(.*)\.json$/.exec(colname);
                            if (match && match[1] !== 'proposal') {
                                self.db.collection(colname, { strict: false }, function (err, col) {
                                    if (err) {
                                        done(err);
                                        return;
                                    }
                                    col.findOne(selector, function (err, dbr) {
                                        if (err) {
                                            done(err);
                                        }
                                        else if (!dbr) {
                                            updateTable(index + 1);
                                        }
                                        else {
                                            var update = false;
                                            if (match[1] === 'en') {
                                                if (editedRecord.value && editedRecord.value !== dbr.value) {
                                                    dbr.value = editedRecord.value;
                                                    update = true;
                                                }
                                            }
                                            else if (match[1] === sl && editedRecord.selected && editedRecord.selected !== dbr.value) {
                                                dbr.value = editedRecord.selected;
                                                dbr.isBest = true;
                                                update = true;
                                            }
                                            if (newid) {
                                                dbr._id = newid;
                                                editedRecord._id = newid;
                                                col.remove(selector, function (err, result) {
                                                    if (err) {
                                                        done(err);
                                                    }
                                                    else {
                                                        col.insert(dbr, function (err) {
                                                            if (err) {
                                                                done(err);
                                                            }
                                                            else {
                                                                updateTable(index + 1);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                            else if (update) {
                                                col.update(selector, dbr, function (err) {
                                                    if (err) {
                                                        done(err);
                                                    }
                                                    else {
                                                        updateTable(index + 1);
                                                    }
                                                });
                                            }
                                            else {
                                                updateTable(index + 1);
                                            }
                                        }
                                    });
                                });
                            }
                            else {
                                updateTable(index + 1);
                            }
                        };
                        updateTable(0);
                    }
                });
            };
            if (newid) {
                self.db.collection('i18n_en.json', { strict: false }, function (err, col) {
                    if (err) {
                        done(err);
                        return;
                    }
                    col.findOne({ '_id': newid }, function (err, dbr) {
                        if (err) {
                            done(err);
                        }
                        else if (!dbr) {
                            updateTables();
                        }
                        else {
                            done({
                                name: 'newidexists',
                                message: 'New id already exists.'
                            });
                        }
                    });
                });
            }
            else {
                updateTables();
            }
        };
        Translations.prototype.deleteTranslations = function (ids, done) {
            var self = this;
            fs.readdir(path.resolve(cfg.path.data), function (err, items) {
                if (err) {
                    done(err);
                }
                else {
                    var selector = {
                        '_id': {
                            $in: ids
                        }
                    };
                    var deleteOnTable = function (index) {
                        if (index >= items.length) {
                            done(null);
                            return;
                        }
                        var colname = items[index];
                        if (/i18n\_(?!proposal).*\.json$/.test(colname)) {
                            self.db.collection(colname, { strict: false }, function (err, col) {
                                if (err) {
                                    done(err);
                                    return;
                                }
                                col.remove(selector, function (err, result) {
                                    if (err) {
                                        done(err);
                                    }
                                    else {
                                        deleteOnTable(index + 1);
                                    }
                                });
                            });
                        }
                        else {
                            deleteOnTable(index + 1);
                        }
                    };
                    deleteOnTable(0);
                }
            });
        };
        Translations.prototype.getTranslations = function (locale, done) {
            var self = this;
            var colname = 'i18n_' + locale + '.json';
            self.db.collection(colname, { strict: false }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.find().toArray(function (err, result) {
                    done(err, result);
                });
            });
        };
        Translations.prototype.getProposals = function (done) {
            var self = this;
            var colname = 'i18n_proposal.json';
            self.db.collection(colname, { strict: false }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.find().toArray(function (err, result) {
                    done(err, result);
                });
            });
        };
        Translations.prototype.deleteProposals = function (ids, done) {
            var self = this;
            var colname = 'i18n_proposal.json';
            self.db.collection(colname, { strict: false }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                var selector = {
                    '_id': {
                        $in: ids
                    }
                };
                col.remove(selector, function (err, result) {
                    done(err);
                });
            });
        };
        Translations.prototype.mergeProposal = function (proposal, done) {
            var self = this;
            var selector = { '_id': proposal.uid };
            var colname = 'i18n_' + proposal.locale + '.json';
            self.db.collection(colname, { strict: false }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.findOne(selector, function (err, dbr) {
                    if (err) {
                        done(err);
                    }
                    else if (!dbr) {
                        done({
                            name: 'notfound',
                            message: 'Not Found.'
                        });
                    }
                    else {
                        dbr.value = proposal.proposal;
                        dbr.isBest = true;
                        col.update(selector, dbr, function (err) {
                            if (err) {
                                done(err);
                            }
                            else {
                                self.db.collection('i18n_proposal.json', { strict: false }, function (err, pcol) {
                                    if (err) {
                                        done(err);
                                        return;
                                    }
                                    cache.clear(proposal.locale);
                                    pcol.remove({ '_id': proposal._id }, function (err, result) {
                                        done(err, proposal);
                                    });
                                });
                            }
                        });
                    }
                });
            });
        };
        return Translations;
    })();
    dbmodules.Translations = Translations;
})(dbmodules || (dbmodules = {}));
module.exports = dbmodules;
//# sourceMappingURL=translations.js.map
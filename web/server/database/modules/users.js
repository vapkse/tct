'use strict';
var cfg = require('../../../config');
var bcrypt = require('bcryptjs');
var dbmodules;
(function (dbmodules) {
    var Users = (function () {
        function Users(db) {
            this.colname = 'users.json';
            var self = this;
            self.db = db;
        }
        Users.prototype.findUserInfos = function (selector, done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.findOne(selector, function (err, dbu) {
                    done(err, dbu);
                });
            });
        };
        Users.prototype.getAllUsers = function (done) {
            var self = this;
            self.findUsers(null, function (err, users) {
                done(err, users);
            });
        };
        Users.prototype.findUsers = function (selector, done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                }
                col.find(selector).toArray(function (err, users) {
                    done(null, users);
                });
            });
        };
        Users.prototype.unlockLocalAccountFromToken = function (token, done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                }
                col.findOne({ validationToken: token }, function (err, dbu) {
                    if (err) {
                        done(err);
                    }
                    else if (!dbu) {
                        done({
                            name: 'msg-tokennotfound',
                            message: 'No user found with this token. Please try again.'
                        });
                    }
                    else {
                        var now = (new Date()).getTime();
                        if (now - dbu.validationTime > 900000) {
                            done({
                                name: 'msg-tokenexpired',
                                message: 'Your token has expired. Please try again.'
                            });
                        }
                        else {
                            dbu.validationTime = 0;
                            dbu.blocked = false;
                            dbu.failAttempts = 0;
                            col.save(dbu, function (err, result) {
                                done(err, dbu);
                            });
                        }
                    }
                });
            });
        };
        Users.prototype.changePasswordFromToken = function (token, password, done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                }
                col.findOne({ validationToken: token }, function (err, dbu) {
                    if (err) {
                        done(err);
                    }
                    else if (!dbu) {
                        done({
                            name: 'msg-tokennotfound',
                            message: 'No user found with this token. Please try again.'
                        });
                    }
                    else {
                        var now = (new Date()).getTime();
                        if (now - dbu.validationTime > 900000) {
                            done({
                                name: 'msg-tokenexpired',
                                message: 'Your token has expired. Please try again.'
                            });
                        }
                        else {
                            bcrypt.genSalt(10, function (err, salt) {
                                if (err) {
                                    done(err);
                                    return;
                                }
                                bcrypt.hash(password, salt, function (err, encrypted) {
                                    if (err) {
                                        done(err);
                                        return;
                                    }
                                    dbu.validationTime = 0;
                                    dbu.blocked = false;
                                    dbu.failAttempts = 0;
                                    dbu._password = encrypted;
                                    col.save(dbu, function (err, result) {
                                        done(err, dbu);
                                    });
                                });
                            });
                        }
                    }
                });
            });
        };
        Users.prototype.cancelResetPassword = function (token, done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                }
                var selector = { validationToken: token };
                col.findOne(selector, function (err, dbu) {
                    if (err) {
                        done(err);
                    }
                    else if (!dbu) {
                        done();
                    }
                    else {
                        dbu.validationTime = 0;
                        col.save(dbu, function (err, result) {
                            done(err, dbu);
                        });
                    }
                });
            });
        };
        Users.prototype.getResetPasswordToken = function (email, done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                }
                var selector = {
                    provider: 'local',
                    email: email.toLowerCase()
                };
                col.findOne(selector, function (err, dbu) {
                    if (err) {
                        done(err);
                    }
                    else if (!dbu) {
                        done({
                            name: 'msg-nouser',
                            message: 'User not found.'
                        });
                    }
                    else {
                        dbu.validationTime = (new Date()).getTime();
                        dbu.validationToken = bcrypt.genSaltSync(45) + 'rp';
                        col.save(dbu, function (err, result) {
                            done(err, dbu);
                        });
                    }
                });
            });
        };
        Users.prototype.validateLocalUserInvitation = function (token, done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                }
                col.findOne({ validationToken: token }, function (err, dbu) {
                    if (err) {
                        done(err);
                    }
                    else if (!dbu) {
                        done({
                            name: 'msg-tokennotfound',
                            message: 'No user found with this token. Please try again.'
                        });
                    }
                    else {
                        var now = (new Date()).getTime();
                        if (now - dbu.validationTime > 260000000) {
                            done({
                                name: 'msg-tokenexpired',
                                message: 'Your token has expired. Please try again.'
                            });
                        }
                        else {
                            dbu.validationTime = 0;
                            dbu.validated = true;
                            col.save(dbu, function (err, result) {
                                done(err, dbu);
                            });
                        }
                    }
                });
            });
        };
        Users.prototype.declineLocalUserInvitation = function (token, done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                }
                var selector = { validationToken: token };
                col.findOne(selector, function (err, dbu) {
                    if (err) {
                        done(err);
                    }
                    else if (!dbu) {
                        done();
                    }
                    else {
                        if (dbu.validated) {
                            done({
                                name: 'msg-nodelvalid',
                                message: 'Your token has expired. Please ask an administrator if you want to close your account.'
                            });
                        }
                        else {
                            col.remove(selector, function (err, result) {
                                done(err);
                            });
                        }
                    }
                });
            });
        };
        Users.prototype.checkLocalUser = function (user, done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                }
                var selector = {
                    provider: 'local',
                    $or: [
                        { userName: user.userName && user.userName.toLowerCase() },
                        { email: user.email && user.email.toLowerCase() }
                    ]
                };
                col.findOne(selector, function (err, dbu) {
                    if (err) {
                        done(err);
                    }
                    else if (dbu) {
                        done({
                            name: 'msg-useroremailexists',
                            message: 'User or email already exists.'
                        });
                    }
                    else {
                        done(null);
                    }
                });
            });
        };
        Users.prototype.addUser = function (user, password, done) {
            var self = this;
            user.email = user.email && user.email.toLowerCase();
            user.userName = user.userName && user.userName.toLowerCase();
            self.checkLocalUser(user, function (err) {
                if (err) {
                    done(err);
                    return;
                }
                self.db.collection(self.colname, { strict: true }, function (err, col) {
                    bcrypt.genSalt(10, function (err, salt) {
                        if (err) {
                            done(err);
                            return;
                        }
                        bcrypt.hash(password, salt, function (err, encrypted) {
                            if (err) {
                                done(err);
                                return;
                            }
                            user._password = encrypted;
                            col.insert(user, function (err, result) {
                                done(err, result[0]);
                            });
                        });
                    });
                });
            });
        };
        Users.prototype.findLocalUser = function (emailOrUserName, password, done) {
            var self = this;
            if (!emailOrUserName || (!emailOrUserName.userName && !emailOrUserName.email)) {
                done({
                    name: 'msg-useremailmissing',
                    message: 'User or email are mandatory.'
                });
                return;
            }
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                var selector = {
                    provider: 'local',
                    $or: [
                        { userName: emailOrUserName.userName && emailOrUserName.userName.toLowerCase() },
                        { email: emailOrUserName.email && emailOrUserName.email.toLowerCase() }
                    ]
                };
                col.findOne(selector, function (err, dbu) {
                    if (!dbu) {
                        done({
                            name: 'msg-wrongpwd',
                            message: 'No user or wrong password.'
                        });
                        return;
                    }
                    if (!bcrypt.compareSync(password, dbu._password)) {
                        if (dbu.blocked) {
                            done({
                                name: 'msg-wrongpwd',
                                message: 'No user or wrong password.'
                            });
                            return;
                        }
                        var failAttempts = dbu.failAttempts | 0;
                        dbu.failAttempts = ++failAttempts;
                        if (failAttempts > cfg.maxFailLoginAttempts) {
                            dbu.blocked = true;
                            dbu.validationToken = bcrypt.genSaltSync(45) + 'ul';
                            dbu.validationTime = (new Date()).getTime();
                        }
                        col.save(dbu, function (err, result) {
                            done({
                                name: 'msg-wrongpwd',
                                message: 'No user or wrong password.'
                            }, dbu, failAttempts, dbu.blocked);
                        });
                        return;
                    }
                    if (dbu.blocked) {
                        done({
                            name: 'msg-blockacc',
                            message: 'Your account is blocked.'
                        });
                        return;
                    }
                    if (!dbu.validated) {
                        done({
                            name: 'msg-notvaluser',
                            message: 'User is not validated, please check your mailbox and follow the link from the sended mail.'
                        });
                        return;
                    }
                    var failAttempts = dbu.failAttempts;
                    if (failAttempts > 0) {
                        dbu.failAttempts = 0;
                        col.save(dbu, function (err, result) {
                            done(err, dbu, failAttempts);
                        });
                    }
                    else {
                        done(err, dbu);
                    }
                });
            });
        };
        Users.prototype.findOrCreateUser = function (user, done) {
            var self = this;
            if (!user) {
                done({
                    name: 'msg-userismand',
                    message: 'User is mandatory.'
                });
                return;
            }
            var dbuser = {
                _id: user._id,
                userName: user.userName && user.userName.toLowerCase(),
                email: user.email && user.email.toLowerCase(),
                displayName: user.displayName,
                role: user.role,
                validated: true,
                provider: user.provider,
                lastLogin: user.lastLogin,
                lastIP: user.lastIP,
            };
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.findOne({ '_id': dbuser._id }, function (err, dbu) {
                    if (err) {
                        done(err);
                        return;
                    }
                    if (!dbu) {
                        if (!user.token) {
                            done({
                                name: 'msg-tokenmissing',
                                message: 'Token is mandatory.'
                            }, user);
                            return;
                        }
                        col.insert(dbuser, function (err, result) {
                            done(err, user);
                        });
                        return;
                    }
                    else {
                        if (dbu.blocked) {
                            done({
                                name: 'msg-blockacc',
                                message: 'Your account is blocked.'
                            });
                            return;
                        }
                    }
                    dbuser._password = dbu._password;
                    col.save(dbuser, function (err, result) {
                        if (err) {
                            console.log("Fail to save user infos.");
                        }
                    });
                    done(null, dbu);
                });
            });
        };
        Users.prototype.deleteUser = function (id, done) {
            var self = this;
            if (!id) {
                done({
                    name: 'msg-useridismand',
                    message: 'User ID is mandatory.'
                });
                return;
            }
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                }
                col.findOne({ '_id': id }, function (err, dbu) {
                    if (err) {
                        done(err);
                    }
                    else if (!dbu) {
                        done();
                    }
                    else {
                        col.remove({ '_id': id }, function (err, result) {
                            done(err);
                        });
                    }
                });
            });
        };
        Users.prototype.updateUser = function (user, done) {
            var self = this;
            if (!user) {
                done({
                    name: 'msg-userismand',
                    message: 'User is mandatory.'
                });
                return;
            }
            if (!user._id) {
                done({
                    name: 'msg-useridismand',
                    message: 'User ID is mandatory.'
                });
                return;
            }
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.findOne({ '_id': user._id }, function (err, dbu) {
                    if (err) {
                        done(err);
                        return;
                    }
                    if (!dbu) {
                        done({
                            name: 'msg-usernotfound',
                            message: 'User not found.'
                        });
                        return;
                    }
                    var extend = require('extend');
                    delete user._id;
                    var merged = extend(true, {}, dbu, user);
                    col.save(merged, function (err, result) {
                        done(err, merged);
                    });
                });
            });
        };
        Users.prototype.initCollection = function (done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, collection) {
                if (err) {
                    try {
                        self.db.createCollection(self.colname, function (err, col) {
                            if (err) {
                                done(err);
                            }
                            else {
                                var user = {
                                    _id: 'local-vapkse',
                                    displayName: 'Admin',
                                    provider: 'local',
                                    userName: 'vapkse',
                                    email: 'tubecurvetracer@gmail.com',
                                    _password: '$2a$04$jiOnW.Wby.mNdBuYGOQm2OZofg7qKwxTcJhSo63Hg.pUIpXSqL1ju',
                                    validated: true,
                                    validationToken: 'lhfhsks8ehkjwhesad',
                                    role: 'admin'
                                };
                                col.insert(user, function (err, col) {
                                    done(err);
                                });
                            }
                        });
                    }
                    catch (e) {
                        done(e);
                    }
                }
                else {
                    done(err);
                }
            });
        };
        return Users;
    })();
    dbmodules.Users = Users;
})(dbmodules || (dbmodules = {}));
module.exports = dbmodules;
//# sourceMappingURL=users.js.map
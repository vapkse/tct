declare module dbmodules {
    class TubePins {
        private db;
        private colname;
        constructor(db: dbengine.Db);
        getPinouts(selector: Object, done: (err: Error, result?: any) => void): void;
        initCollection(done: (err?: Error) => void): void;
    }
}
export = dbmodules;

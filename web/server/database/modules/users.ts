'use strict';

import cfg = require('../../../config');
import fs = require('fs');
import path = require('path');
import bcrypt = require('bcryptjs');
import User = require('../../common/ts/user'); // Namespace only

module dbmodules {
    export class Users {
        private db: dbengine.Db;
        private colname = 'users.json';

        constructor(db: dbengine.Db) {
            var self = <Users>this;
            self.db = db;
        }

        public findUserInfos(selector: any, done: (err: Error, user?: User.DbUser) => void) {
            var self = <Users>this;

            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                col.findOne(selector, function(err: Error, dbu: User.DbUser) {
                    done(err, dbu);           
                })
            })
        }
        
        public getAllUsers(done: (err: Error, users?: Array<User.DbUser>) => void) {
            var self = <Users>this;
            self.findUsers(null, function(err, users) {
                done(err, users);                
            })
        }
        
        public findUsers(selector: any, done: (err: Error, users?: Array<User.DbUser>) => void) {
            var self = <Users>this;

            // Open collection
            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                }

                col.find(selector).toArray(function(err: Error, users: Array<User.DbUser>) {
                    done(null, users);
                })
            })
        }
        
        public unlockLocalAccountFromToken(token: string, done: (err?: Error, dbu?: User.DbUser) => void) {
            var self = <Users>this;

            // Open collection
            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                }

                // Find user                
                col.findOne({ validationToken: token }, function(err: Error, dbu: User.DbUser) {
                    if (err) {
                        done(err);
                    } else if (!dbu) {
                        done({
                            name: 'msg-tokennotfound',
                            message: 'No user found with this token. Please try again.'
                        })
                    } else {
                        var now = (new Date()).getTime();
                        if (now - dbu.validationTime > 900000) {
                            done({
                                name: 'msg-tokenexpired',
                                message: 'Your token has expired. Please try again.'
                            })
                        } else {
                            // Update user in db
                            dbu.validationTime = 0;
                            dbu.blocked = false;
                            dbu.failAttempts = 0;

                            col.save(dbu, function(err, result) {
                                done(err, dbu);
                            })
                        }
                    }
                })
            })
        }

        public changePasswordFromToken(token: string, password: string, done: (err?: Error, dbu?: User.DbUser) => void) {
            var self = <Users>this;

            // Open collection
            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                }

                // Find user                
                col.findOne({ validationToken: token }, function(err: Error, dbu: User.DbUser) {
                    if (err) {
                        done(err);
                    } else if (!dbu) {
                        done({
                            name: 'msg-tokennotfound',
                            message: 'No user found with this token. Please try again.'
                        })
                    } else {
                        var now = (new Date()).getTime();
                        if (now - dbu.validationTime > 900000) {
                            done({
                                name: 'msg-tokenexpired',
                                message: 'Your token has expired. Please try again.'
                            })
                        } else {
                            // Crypt password                        
                            bcrypt.genSalt(10, function(err: Error, salt: string) {
                                if (err) {
                                    done(err);
                                    return;
                                }
                                bcrypt.hash(password, salt, function(err: Error, encrypted: string) {
                                    if (err) {
                                        done(err);
                                        return;
                                    }
                                    // Update user in db
                                    dbu.validationTime = 0;
                                    dbu.blocked = false;
                                    dbu.failAttempts = 0;
                                    dbu._password = encrypted;

                                    col.save(dbu, function(err, result) {
                                        done(err, dbu);
                                    })
                                })
                            })
                        }
                    }
                })
            })
        }

        public cancelResetPassword(token: string, done: (err?: Error, dbu?: User.DbUser) => void) {
            var self = <Users>this;

            // Open collection
            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                }

                // find user        
                var selector = { validationToken: token };
                col.findOne(selector, function(err: Error, dbu: User.DbUser) {
                    if (err) {
                        done(err);
                    } else if (!dbu) {
                        done();
                    } else {
                        dbu.validationTime = 0;
                        col.save(dbu, function(err, result) {
                            done(err, dbu);
                        })
                    }
                })
            })
        }

        public getResetPasswordToken(email: string, done: (err: Error, dbu?: User.DbUser) => void) {
            var self = <Users>this;
            
            // Open collection
            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                }

                var selector = {
                    provider: 'local',
                    email: email.toLowerCase()
                }
                
                // Find user                
                col.findOne(selector, function(err: Error, dbu: User.DbUser) {
                    if (err) {
                        done(err);
                    } else if (!dbu) {
                        done({
                            name: 'msg-nouser',
                            message: 'User not found.'
                        })
                    } else {
                        // Create a token for password reset and save
                        dbu.validationTime = (new Date()).getTime();
                        dbu.validationToken = bcrypt.genSaltSync(45) + 'rp';

                        col.save(dbu, function(err, result) {
                            done(err, dbu);
                        })
                    }
                })
            })
        }

        public validateLocalUserInvitation(token: string, done: (err: Error, dbu?: User.DbUser) => void) {
            var self = <Users>this;

            // Open collection
            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                }

                // Find user                
                col.findOne({ validationToken: token }, function(err: Error, dbu: User.DbUser) {
                    if (err) {
                        done(err);
                    } else if (!dbu) {
                        done({
                            name: 'msg-tokennotfound',
                            message: 'No user found with this token. Please try again.'
                        })
                    } else {
                        var now = (new Date()).getTime();
                        if (now - dbu.validationTime > 260000000) {
                            done({
                                name: 'msg-tokenexpired',
                                message: 'Your token has expired. Please try again.'
                            })
                        } else {
                            // Update user in db
                            dbu.validationTime = 0;
                            dbu.validated = true;

                            col.save(dbu, function(err, result) {
                                done(err, dbu);
                            })
                        }
                    }
                })
            })
        }

        public declineLocalUserInvitation(token: string, done: (err?: Error) => void) {
            var self = <Users>this;

            // Open collection
            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                }

                // find user        
                var selector = { validationToken: token };
                col.findOne(selector, function(err: Error, dbu: User.DbUser) {
                    if (err) {
                        done(err);
                    } else if (!dbu) {
                        done();
                    } else {
                        if (dbu.validated) {
                            done({
                                name: 'msg-nodelvalid',
                                message: 'Your token has expired. Please ask an administrator if you want to close your account.'
                            })
                        } else {
                            col.remove(selector, function(err, result) {
                                done(err);
                            })
                        }
                    }
                })
            })
        }

        public checkLocalUser(user: User.UserEmail, done: (err: Error) => void) {
            var self = <Users>this;

            // Open collection
            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                }

                var selector = {
                    provider: 'local',
                    $or: [
                        { userName: user.userName && user.userName.toLowerCase() },
                        { email: user.email && user.email.toLowerCase() }
                    ]
                }

                // Find user                
                col.findOne(selector, function(err: Error, dbu: User.DbUser) {
                    if (err) {
                        done(err);
                    } else if (dbu) {
                        done({
                            name: 'msg-useroremailexists',
                            message: 'User or email already exists.'
                        })
                    } else {
                        done(null);
                    }
                })
            })
        }

        public addUser(user: User.User, password: string, done: (err: Error, user?: User.DbUser) => void) {
            var self = <Users>this;
            
            // User and email are not case sensititive 
            user.email = user.email && user.email.toLowerCase();
            user.userName = user.userName && user.userName.toLowerCase();

            self.checkLocalUser(user, function(err) {
                if (err) {
                    done(err);
                    return;
                }
                // Open collection
                self.db.collection(self.colname, { strict: true }, function(err, col) {
                    // Crypt password                        
                    bcrypt.genSalt(10, function(err: Error, salt: string) {
                        if (err) {
                            done(err);
                            return;
                        }
                        bcrypt.hash(password, salt, function(err: Error, encrypted: string) {
                            if (err) {
                                done(err);
                                return;
                            }
                            // insert user
                            user._password = encrypted;
                            col.insert(user, function(err, result) {
                                done(err, result[0]);
                            })
                        })
                    })
                })
            })
        }

        public findLocalUser(emailOrUserName: User.UserEmail, password: string, done: (err: Error, user?: User.DbUser, failAttempts?: number, accountBlocked?: boolean) => void) {
            var self = <Users>this;

            if (!emailOrUserName || (!emailOrUserName.userName && !emailOrUserName.email)) {
                done({
                    name: 'msg-useremailmissing',
                    message: 'User or email are mandatory.'
                });
                return;
            }

            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                var selector = {
                    provider: 'local',
                    $or: [
                        { userName: emailOrUserName.userName && emailOrUserName.userName.toLowerCase() },
                        { email: emailOrUserName.email && emailOrUserName.email.toLowerCase() }
                    ]
                }

                col.findOne(selector, function(err: Error, dbu: User.DbUser) {
                    // if there is no user, create them or return if local account
                    if (!dbu) {
                        done({
                            name: 'msg-wrongpwd',
                            message: 'No user or wrong password.'
                        });
                        return;
                    }

                    if (!bcrypt.compareSync(password, dbu._password)) {
                        if (dbu.blocked) {
                            done({
                                name: 'msg-wrongpwd',
                                message: 'No user or wrong password.'
                            });
                            return;
                        }

                        //  Write a fail attempt
                        var failAttempts = dbu.failAttempts | 0;
                        dbu.failAttempts = ++failAttempts;
                        if (failAttempts > cfg.maxFailLoginAttempts) {
                            dbu.blocked = true;
                            dbu.validationToken = bcrypt.genSaltSync(45) + 'ul';
                            dbu.validationTime = (new Date()).getTime();
                        }
                        col.save(dbu, function(err, result) {
                            done({
                                name: 'msg-wrongpwd',
                                message: 'No user or wrong password.'
                            }, dbu, failAttempts, dbu.blocked);
                        })
                        return;
                    }

                    if (dbu.blocked) {
                        done({
                            name: 'msg-blockacc',
                            message: 'Your account is blocked.'
                        });
                        return;
                    }

                    if (!dbu.validated) {
                        done({
                            name: 'msg-notvaluser',
                            message: 'User is not validated, please check your mailbox and follow the link from the sended mail.'
                        });
                        return;
                    }
                    
                    // Reset fail attempt
                    var failAttempts = dbu.failAttempts;
                    if (failAttempts > 0) {
                        dbu.failAttempts = 0;
                        col.save(dbu, function(err, result) {
                            // Return fail attempts
                            done(err, dbu, failAttempts);
                        })
                    } else {
                        done(err, dbu);
                    }
                })
            })
        }

        public findOrCreateUser(user: User.User, done: (err: Error, user?: User.DbUser) => void) {
            var self = <Users>this;

            if (!user) {
                done({
                    name: 'msg-userismand',
                    message: 'User is mandatory.'
                });
                return;
            }

            var dbuser: User.DbUser = {
                _id: user._id,
                userName: user.userName && user.userName.toLowerCase(),
                email: user.email && user.email.toLowerCase(),
                displayName: user.displayName,
                role: user.role,
                validated: true,
                provider: user.provider,
                lastLogin: user.lastLogin,
                lastIP: user.lastIP,                                
            }

            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                col.findOne({ '_id': dbuser._id }, function(err: Error, dbu: User.DbUser) {
                    if (err) {
                        done(err);
                        return;
                    }

                    if (!dbu) {
                        if (!user.token) {
                            done({
                                name: 'msg-tokenmissing',
                                message: 'Token is mandatory.'
                            }, user);
                            return;
                        }
                        
                        // if there is no user, create them
                        col.insert(dbuser, function(err, result) {
                            done(err, user);
                        })
                        return;
                    } else {
                        if (dbu.blocked) {
                            done({
                                name: 'msg-blockacc',
                                message: 'Your account is blocked.'
                            });
                            return;
                        }
                    }
                    
                    // Update user
                    dbuser._password = dbu._password;
                    col.save(dbuser, function(err, result) {
                        if (err) {
                            console.log("Fail to save user infos.");
                        }
                    })
                    
                    // Success
                    done(null, dbu);
                })
            })
        }
        
        public deleteUser(id: string, done: (err?: Error) => void) {
            var self = <Users>this;

            if (!id) {
                done({
                    name: 'msg-useridismand',
                    message: 'User ID is mandatory.'
                });
                return;
            }     
            
            // Open collection
            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                }

                // find user        
                col.findOne({ '_id': id }, function(err: Error, dbu: User.DbUser) {
                    if (err) {
                        done(err);
                    } else if (!dbu) {
                        done();
                    } else {
                        col.remove({ '_id': id }, function(err, result) {
                            done(err);
                        })
                    }
                })
            })                      
        }
        
        public updateUser(user: User.User, done: (err: Error, user?: User.DbUser) => void) {
            var self = <Users>this;

            if (!user) {
                done({
                    name: 'msg-userismand',
                    message: 'User is mandatory.'
                });
                return;
            }

            if (!user._id) {
                done({
                    name: 'msg-useridismand',
                    message: 'User ID is mandatory.'
                });
                return;
            }

            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                col.findOne({ '_id': user._id }, function(err: Error, dbu: User.DbUser) {
                    if (err) {
                        done(err);
                        return;
                    }

                    if (!dbu) {
                        done({
                            name: 'msg-usernotfound',
                            message: 'User not found.'
                        });
                        return;
                    }
                    
                    // Merge user
                    var extend = require('extend');
                    delete user._id;
                    var merged = extend(true, {}, dbu, user) as User.User;                    
                    
                    // Update user
                    col.save(merged, function(err, result) {
                        done(err, merged);
                    })                    
                })
            })
        }        
        
        public initCollection(done: (err?: Error) => void) {
            var self = <Users>this;

            self.db.collection(self.colname, { strict: true }, function(err, collection) {
                if (err) {
                    // Create collection
                    try {
                        self.db.createCollection(self.colname, function(err, col) {
                            if (err) {
                                done(err);
                            } else {
                                // insert default user
                                var user: User.User = {
                                    _id: 'local-vapkse',
                                    displayName: 'Admin',
                                    provider: 'local',
                                    userName: 'vapkse',
                                    email: 'tubecurvetracer@gmail.com',
                                    _password: '$2a$04$jiOnW.Wby.mNdBuYGOQm2OZofg7qKwxTcJhSo63Hg.pUIpXSqL1ju',
                                    validated: true,
                                    validationToken: 'lhfhsks8ehkjwhesad',
                                    role: 'admin'
                                }
                                col.insert(user, function(err, col) {
                                    done(err);
                                })
                            }
                        })
                    } catch (e) {
                        done(e);
                    }
                } else {
                    done(err);
                }
            })
        }
    }
}

export = dbmodules;
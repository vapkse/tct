'use strict';

import cfg = require('../../../config');
import fs = require('fs');
import path = require('path');

module dbmodules {
    export class TubePins {
        private db: dbengine.Db;
        private colname = 'tubes-pins.json';

        constructor(db: dbengine.Db) {
            var self = <TubePins>this;
            self.db = db;
        }

        public getPinouts(selector: Object, done: (err: Error, result?: any) => void) {
            var self = <TubePins>this;

            var openCollection = function() {
                self.db.collection(self.colname, { strict: true }, function(err, col) {
                    if (err) {
                        done(err);
                        return;
                    }
                    col.find(selector).toArray(function(err, result) {
                        done(err, result);
                    })
                })
            }

            openCollection();
        }

        public initCollection(done: (err?: Error) => void) {
            var self = <TubePins>this;

            self.db.collection(self.colname, { strict: true }, function(err) {
                if (err) {
                    // Create collection
                    var filename = path.resolve(cfg.db.importPath, self.colname);
                    var data = fs.readFile(filename, function(err, data) {
                        if (err) {
                            done(err);
                            return;
                        }
                        try {
                            var json = JSON.parse(data.toString());
                            self.db.createCollection(self.colname, function(err, col) {
                                if (err) {
                                    done(err);
                                    return;
                                }
                                col.insert(json, function(err, col) {
                                    if (err) {
                                        done(err);
                                        return;
                                    }
                                    done();
                                })
                            })
                        } catch (e) {
                            done(e);
                        }
                    });
                } else {
                    done();
                }
            });
        }
    }
}

export = dbmodules;
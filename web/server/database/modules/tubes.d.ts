import User = require('../../common/ts/user');
declare module dbmodules {
    class Tubes {
        private db;
        private colname;
        constructor(db: dbengine.Db);
        private getUserColName;
        saveGlobalTube(data: TubeData, user: User.DbUser, done: (err: Error, result?: TubeSearchResult) => void): void;
        saveTube(tubeData: TubeData, user: User.User, done: (err: Error, result?: TubeSearchResult) => void): void;
        private getLookups(tubeData, done);
        deleteGlobalTube(id: number, done: (err: Error, result?: TubeData) => void): void;
        deleteTube(id: number, userid: string, done: (err: Error, result?: TubeData) => void): void;
        newTube(done: (err: Error, result?: TubeSearchResult) => void): void;
        getTube(selector: Object, userid: string, forViewer: boolean, done: (err: Error, result?: TubeSearchResult, isGlobalTube?: boolean) => void): void;
        renameDocumentInCollection(colname: string, srcName: string, dstName: string, done: (err: Error, result?: Array<TubeData>) => void): void;
        getTubeDataFromCollection(colname: string, done: (err: Error, result?: Array<TubeData>) => void): void;
        getTubesCount(done: (err: Error, result?: number) => void): void;
        searchTubes(expression: string, user: User.DbUser, done: (err: Error, result?: Array<any>, users?: {
            [id: string]: User.UserEmail;
        }) => void): void;
        createTextIndex(done: (err: Error, result?: Array<any>) => void): void;
        private createDataIndexes(t);
        initCollection(done: (err?: Error) => void): void;
    }
}
export = dbmodules;

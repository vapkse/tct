import User = require('../../common/ts/user');
declare module dbmodules {
    class Users {
        private db;
        private colname;
        constructor(db: dbengine.Db);
        findUserInfos(selector: any, done: (err: Error, user?: User.DbUser) => void): void;
        getAllUsers(done: (err: Error, users?: Array<User.DbUser>) => void): void;
        findUsers(selector: any, done: (err: Error, users?: Array<User.DbUser>) => void): void;
        unlockLocalAccountFromToken(token: string, done: (err?: Error, dbu?: User.DbUser) => void): void;
        changePasswordFromToken(token: string, password: string, done: (err?: Error, dbu?: User.DbUser) => void): void;
        cancelResetPassword(token: string, done: (err?: Error, dbu?: User.DbUser) => void): void;
        getResetPasswordToken(email: string, done: (err: Error, dbu?: User.DbUser) => void): void;
        validateLocalUserInvitation(token: string, done: (err: Error, dbu?: User.DbUser) => void): void;
        declineLocalUserInvitation(token: string, done: (err?: Error) => void): void;
        checkLocalUser(user: User.UserEmail, done: (err: Error) => void): void;
        addUser(user: User.User, password: string, done: (err: Error, user?: User.DbUser) => void): void;
        findLocalUser(emailOrUserName: User.UserEmail, password: string, done: (err: Error, user?: User.DbUser, failAttempts?: number, accountBlocked?: boolean) => void): void;
        findOrCreateUser(user: User.User, done: (err: Error, user?: User.DbUser) => void): void;
        deleteUser(id: string, done: (err?: Error) => void): void;
        updateUser(user: User.User, done: (err: Error, user?: User.DbUser) => void): void;
        initCollection(done: (err?: Error) => void): void;
    }
}
export = dbmodules;

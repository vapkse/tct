'use strict';
var cfg = require('../../../config');
var fs = require('fs');
var path = require('path');
var tubeBasesModule = require('./tube-bases');
var tubeTypesModule = require('./tube-types');
var tubePinoutsModule = require('./tube-pins');
var tct = require('../../common/ts/tube');
var search = require('../../common/ts/search-params');
var usersModule = require('./users');
var dbmodules;
(function (dbmodules) {
    var Tubes = (function () {
        function Tubes(db) {
            this.colname = 'tubes.json';
            this.getUserColName = function (userid) {
                var self = this;
                return userid + '-' + self.colname;
            };
            var self = this;
            self.db = db;
        }
        Tubes.prototype.saveGlobalTube = function (data, user, done) {
            var self = this;
            if (!user || user.role !== 'admin') {
                done({
                    name: 'notlogged',
                    message: 'You must be logged in to use this service.'
                });
                return;
            }
            var validateDatas = function (tubeData, done) {
                self.getLookups(tubeData, function (err, result) {
                    if (err) {
                        done(err);
                        return;
                    }
                    var tube = new tct.Tube(result);
                    var verrors = tube.validate();
                    if (verrors.length) {
                        done(verrors[0]);
                        return;
                    }
                    done(null, result);
                });
            };
            validateDatas(data, function (err, result) {
                if (err) {
                    done(err);
                    return;
                }
                self.db.collection(self.colname, { strict: true }, function (err, collection) {
                    if (err) {
                        done(err);
                        return;
                    }
                    collection.findOne({ name: new RegExp('^' + result.tubeData.name + '$', 'i') }, function (err, originalData) {
                        if (err) {
                            done(err);
                            return;
                        }
                        self.createDataIndexes(result.tubeData);
                        if (originalData) {
                            var originalId = originalData._id.id || originalData._id;
                            if (result.tubeData._id !== originalId) {
                                done({
                                    name: 'invalidtubeid',
                                    message: 'Invalid tube id.'
                                });
                                return;
                            }
                            collection.save(result.tubeData, function (err, r) {
                                self.getLookups(result.tubeData, function (err, saved) {
                                    done(err, saved);
                                });
                            });
                        }
                        else {
                            if (result.tubeData._id) {
                                done({
                                    name: 'invalidtubeid',
                                    message: 'Invalid tube id.'
                                });
                                return;
                            }
                            delete result.tubeData._id;
                            collection.insert(result.tubeData, function (err, gtube) {
                                self.getLookups(gtube[0], function (err, saved) {
                                    done(err, saved);
                                });
                            });
                        }
                    });
                });
            });
        };
        Tubes.prototype.saveTube = function (tubeData, user, done) {
            var self = this;
            var userid = user && user._id;
            if (!userid) {
                done({
                    name: 'notlogged',
                    message: 'You must be logged in to use this service.'
                });
                return;
            }
            var updateTube = function (result) {
                self.db.collection(self.getUserColName(userid), { strict: false }, function (err, personalCol) {
                    if (err) {
                        done(err);
                        return;
                    }
                    delete result.tubeData._userId;
                    self.createDataIndexes(result.tubeData);
                    personalCol.save(result.tubeData, function (err, r) {
                        self.getLookups(result.tubeData, function (err, saved) {
                            done(err, saved);
                        });
                    });
                });
            };
            var insertTube = function (tubeInfos) {
                var regex = new RegExp('^' + tubeInfos.tubeData.name + '$', 'i');
                self.getTube({ name: regex }, userid, false, function (err, result, isGlobalTube) {
                    if (err) {
                        done(err);
                        return;
                    }
                    if (result && result.tubeData && !(isGlobalTube && user && user.role === 'admin')) {
                        done({
                            name: 'name-exists',
                            message: 'A tube with this name already exists.',
                            type: 'error',
                            fieldName: 'name',
                            tooltip: 'true'
                        });
                        return;
                    }
                    self.createDataIndexes(tubeInfos.tubeData);
                    delete tubeInfos.tubeData._id;
                    self.db.collection(self.getUserColName(userid), { strict: false }, function (err, userCol) {
                        if (err) {
                            done(err);
                            return;
                        }
                        userCol.insert(tubeInfos.tubeData, function (err, userTube) {
                            if (err) {
                                done(err);
                                return;
                            }
                            self.getLookups(userTube[0], function (err, result) {
                                done(err, result);
                            });
                        });
                    });
                });
            };
            var validateDatas = function (tubeData, done) {
                self.getLookups(tubeData, function (err, result) {
                    if (err) {
                        done(err);
                        return;
                    }
                    var tube = new tct.Tube(result);
                    var verrors = tube.validate();
                    if (verrors.length) {
                        done(verrors[0]);
                        return;
                    }
                    done(null, result);
                });
            };
            validateDatas(tubeData, function (err, result) {
                if (err) {
                    done(err);
                    return;
                }
                if (tubeData._id) {
                    updateTube(result);
                }
                else {
                    insertTube(result);
                }
            });
        };
        Tubes.prototype.getLookups = function (tubeData, done) {
            var self = this;
            var returnValue = {
                tubeData: tubeData
            };
            var getType = function () {
                if (returnValue.tubeData.type) {
                    var typesModule = new tubeTypesModule.TubeTypes(self.db);
                    typesModule.getTypes({ _id: tubeData.type }, function (err, result) {
                        if (err) {
                            done(err);
                        }
                        else {
                            returnValue.type = result.length && result[0];
                            done(null, returnValue);
                        }
                    });
                }
                else {
                    done(null, returnValue);
                }
            };
            var getBase = function () {
                if (returnValue.tubeData.base) {
                    var basesModule = new tubeBasesModule.TubeBases(self.db);
                    basesModule.getBases({ _id: tubeData.base }, function (err, result) {
                        if (err) {
                            done(err);
                        }
                        else {
                            returnValue.base = result[0];
                            getType();
                        }
                    });
                }
                else {
                    getType();
                }
            };
            var getPinout = function () {
                if (returnValue.tubeData.pinout) {
                    var pinsModule = new tubePinoutsModule.TubePins(self.db);
                    pinsModule.getPinouts({ _id: tubeData.pinout }, function (err, result) {
                        if (err) {
                            done(err);
                        }
                        else {
                            returnValue.pinout = result[0];
                            getBase();
                        }
                    });
                }
                else {
                    getBase();
                }
            };
            getPinout();
        };
        Tubes.prototype.deleteGlobalTube = function (id, done) {
            var self = this;
            if (!id || isNaN(id)) {
                done({
                    name: 'invalid_id',
                    message: 'Invalid id.'
                });
                return;
            }
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                var selector = { _id: id };
                col.findOne(selector, function (err, tubeData) {
                    if (err) {
                        done(err);
                        return;
                    }
                    if (!tubeData) {
                        done({
                            name: 'not_found',
                            message: 'Not found.'
                        });
                        return;
                    }
                    col.remove(selector, function (err, result) {
                        done(err, tubeData);
                    });
                });
            });
        };
        Tubes.prototype.deleteTube = function (id, userid, done) {
            var self = this;
            if (!userid) {
                done({
                    name: 'notlogged',
                    message: 'You must be logged in to use this service.'
                });
                return;
            }
            if (!id || isNaN(id)) {
                done({
                    name: 'invalid_id',
                    message: 'Invalid id.'
                });
                return;
            }
            self.db.collection(self.getUserColName(userid), { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                var selector = { _id: id };
                col.findOne(selector, function (err, tubeData) {
                    if (err) {
                        done(err);
                        return;
                    }
                    if (!tubeData) {
                        done({
                            name: 'not_found',
                            message: 'Not found.'
                        });
                        return;
                    }
                    col.remove(selector, function (err, result) {
                        done(err, tubeData);
                    });
                });
            });
        };
        Tubes.prototype.newTube = function (done) {
            var self = this;
            self.getLookups({ _id: 0 }, function (err, result) {
                done(err, result);
            });
        };
        Tubes.prototype.getTube = function (selector, userid, forViewer, done) {
            var self = this;
            if (userid && typeof userid !== 'string') {
                done({
                    name: 'invaliduser',
                    message: 'Invalid User ID.'
                });
                return;
            }
            var getUserInfos = function (tubeResult, done) {
                new usersModule.Users(self.db).findUserInfos({ _id: userid }, function (err, user) {
                    if (err) {
                        done(err);
                        return;
                    }
                    var userResult = tubeResult;
                    userResult.userInfos = {
                        _id: user._id,
                        userName: user.userName,
                        email: user.email
                    };
                    done(null, userResult);
                });
            };
            var complete = function (tubeData, isGlobalTube) {
                if (!tubeData) {
                    done(null, null);
                    return;
                }
                self.getLookups(tubeData, function (err, result) {
                    if (err) {
                        done(err);
                    }
                    else {
                        if (!forViewer) {
                            done(err, result, isGlobalTube);
                        }
                        else if (userid) {
                            getUserInfos(result, function (err, result3) {
                                done(err, result3, isGlobalTube);
                            });
                        }
                        else {
                            done(err, result, isGlobalTube);
                        }
                    }
                });
            };
            var findUserTube = function (selector, cb) {
                if (!userid) {
                    cb(null);
                    return;
                }
                self.db.collection(self.getUserColName(userid), { strict: false }, function (err, col) {
                    if (err) {
                        cb(err);
                        return;
                    }
                    col.findOne(selector, function (err, tubeData) {
                        if (err) {
                            cb(err);
                            return;
                        }
                        cb(null, tubeData);
                    });
                });
            };
            var findGlobalTube = function (selector, cb) {
                self.db.collection(self.colname, { strict: true }, function (err, col) {
                    if (err) {
                        done(err);
                        return;
                    }
                    col.findOne(selector, function (err, tubeData) {
                        if (err) {
                            done(err);
                        }
                        else {
                            cb(null, tubeData);
                        }
                    });
                });
            };
            if (userid) {
                findUserTube(selector, function (err, tubeData) {
                    if (err) {
                        done(err);
                    }
                    else if (tubeData) {
                        complete(tubeData, false);
                    }
                    else {
                        findGlobalTube(selector, function (err, tubeData) {
                            if (err) {
                                done(err);
                            }
                            else {
                                complete(tubeData, true);
                            }
                        });
                    }
                });
            }
            else {
                findGlobalTube(selector, function (err, tubeData) {
                    if (err) {
                        done(err);
                    }
                    else {
                        complete(tubeData, true);
                    }
                });
            }
        };
        Tubes.prototype.renameDocumentInCollection = function (colname, srcName, dstName, done) {
            var self = this;
            self.db.collection(colname, { strict: false }, function (err, col) {
                if (err) {
                    done(err, []);
                    return;
                }
                col.find({ 'documents.filename': srcName }, { '_tiar.documents.filename': 0 }).toArray(function (err, tubes) {
                    if (err) {
                        done(err);
                        return;
                    }
                    if (!tubes || tubes.length === 0) {
                        done({
                            name: 'msg-linknotfound',
                            message: 'Link not found.'
                        });
                        return;
                    }
                    var updateDoc = function (index) {
                        if (index >= tubes.length) {
                            done(null);
                            return;
                        }
                        var tube = tubes[index];
                        var found = false;
                        if (tube.documents.length) {
                            for (var i = 0; i < tube.documents.length; i++) {
                                if (tube.documents[i].filename === srcName) {
                                    found = true;
                                    tube.documents[i].filename = dstName;
                                }
                            }
                        }
                        if (!found) {
                            done({
                                name: 'msg-linknotfound',
                                message: 'Link not found.'
                            });
                        }
                        else {
                            col.update({ _id: tube._id }, tube, function (err) {
                                if (err) {
                                    done(err);
                                }
                                else {
                                    updateDoc(index + 1);
                                }
                            });
                        }
                    };
                    updateDoc(0);
                });
            });
        };
        Tubes.prototype.getTubeDataFromCollection = function (colname, done) {
            var self = this;
            self.db.collection(colname, { strict: false }, function (err, col) {
                if (err) {
                    done(err, []);
                    return;
                }
                col.find().toArray(function (err, qresult) {
                    done(err, qresult);
                });
            });
        };
        Tubes.prototype.getTubesCount = function (done) {
            var self = this;
            self.db.collection(self.colname, { strict: false }, function (err, col) {
                if (err) {
                    done(err, 0);
                    return;
                }
                col.find().toArray(function (err, qresult) {
                    done(err, qresult.length);
                });
            });
        };
        Tubes.prototype.searchTubes = function (expression, user, done) {
            var self = this;
            var allUsersSearch = user && user.role === 'admin';
            var sort = false;
            var users = {};
            var findUserTubes = function (qsel, fields, usr, cb) {
                var userid = usr && usr._id;
                if (!userid) {
                    cb(null, []);
                    return;
                }
                self.db.collection(self.getUserColName(userid), { strict: false }, function (err, col) {
                    if (err) {
                        cb(err, []);
                        return;
                    }
                    col.find(qsel, fields).toArray(function (err, qresult) {
                        if (err) {
                            cb(err, []);
                            return;
                        }
                        qresult.map(function (td) { return td._userId = userid; });
                        cb(null, qresult);
                    });
                });
            };
            var addTubesInGlobalCollection = function (qsel, fields, excludeList, result) {
                self.db.collection(self.colname, { strict: true }, function (err, col) {
                    if (err) {
                        done(err);
                        return;
                    }
                    col.find(qsel, fields, function (err, cursor) {
                        if (err) {
                            done(err);
                            return;
                        }
                        cursor.each(function (err, item) {
                            if (err) {
                                done(err);
                                return;
                            }
                            if (!item) {
                                if (sort || allUsersSearch) {
                                    result.sort(function (a, b) {
                                        var ir;
                                        if (!a._text && !b._text) {
                                            ir = 0;
                                        }
                                        else if (a._text && !b._text) {
                                            ir = -1;
                                        }
                                        else if (!a._text && b._text) {
                                            ir = 1;
                                        }
                                        else {
                                            ir = a._text.indexOf(expression) - b._text.indexOf(expression);
                                        }
                                        if (ir === 0) {
                                            ir = a.name.localeCompare(b.name);
                                        }
                                        return ir;
                                    });
                                }
                                done(null, result, users);
                                return;
                            }
                            if (!excludeList || !excludeList[item.name]) {
                                result.push(item);
                            }
                        });
                    });
                });
            };
            search.TubeSearch.FromQueryExpression(expression, function (err, tubeSearch) {
                if (err) {
                    done(err);
                    return;
                }
                tubeSearch.getFieldExpressions(function (err, fieldExprs, fields, userExprs, allUserSearch) {
                    if (err) {
                        done(err);
                        return;
                    }
                    var qsel;
                    if (fieldExprs.length === 0 && !userExprs && !allUserSearch) {
                        done(null, []);
                        return;
                    }
                    else if (fieldExprs.length === 0) {
                        qsel = undefined;
                    }
                    else {
                        qsel = { $and: fieldExprs };
                    }
                    var findTubes = function (qsel, qusers) {
                        if (!qusers || qusers.length === 0) {
                            findUserTubes(qsel, fields, user, function (err, result) {
                                if (err) {
                                    done(err);
                                    return;
                                }
                                findUserTubes(null, null, user, function (err, excludeList) {
                                    if (err) {
                                        done(err);
                                        return;
                                    }
                                    var list = {};
                                    excludeList.map(function (td) { return list[td.name] = td; });
                                    addTubesInGlobalCollection(qsel, fields, list, result);
                                });
                            });
                        }
                        else {
                            var result = [];
                            var addUser = function (index) {
                                if (index >= qusers.length) {
                                    done(null, result, users);
                                    ;
                                    return;
                                }
                                var u = qusers[index];
                                findUserTubes(qsel, fields, u, function (err, qres) {
                                    if (err) {
                                        done(err);
                                        return;
                                    }
                                    if (qres) {
                                        qres.map(function (tdu) { return result.push(tdu); });
                                    }
                                    users[u._id] = {
                                        userName: u.userName,
                                        email: u.email
                                    };
                                    addUser(++index);
                                });
                            };
                            addUser(0);
                        }
                    };
                    if (allUserSearch) {
                        new usersModule.Users(self.db).getAllUsers(function (err, allusers) {
                            if (err) {
                                done(err);
                                return;
                            }
                            findTubes(qsel, allusers);
                        });
                    }
                    else if (userExprs) {
                        new usersModule.Users(self.db).findUsers(userExprs, function (err, usrs) {
                            if (err) {
                                done(err);
                                return;
                            }
                            findTubes(qsel, usrs);
                        });
                    }
                    else {
                        findTubes(qsel);
                    }
                });
            });
        };
        Tubes.prototype.createTextIndex = function (done) {
            var self = this;
            var openCollection = function () {
                self.db.collection(self.colname, { strict: true }, function (err, col) {
                    if (err) {
                        done(err);
                        return;
                    }
                    col.createIndex({ "_text": 1 }, {}, function (e, result) {
                        done(e);
                    });
                });
            };
            openCollection();
        };
        Tubes.prototype.createDataIndexes = function (t) {
            var searchIndex = [];
            searchIndex.push(t.name);
            if (t.type) {
                searchIndex.push('ttype_' + t.type);
            }
            if (t.base) {
                searchIndex.push('tbase_' + t.base);
            }
            if (t.pinout) {
                searchIndex.push('tpins_' + t.pinout);
            }
            if (t.notes) {
                searchIndex.push(t.notes);
            }
            t._text = searchIndex.join(' ');
        };
        Tubes.prototype.initCollection = function (done) {
            var self = this;
            var typesModule = new tubeTypesModule.TubeTypes(self.db);
            var basesModule = new tubeBasesModule.TubeBases(self.db);
            var pinsModule = new tubePinoutsModule.TubePins(self.db);
            self.db.collection(self.colname, { strict: true }, function (err, collection) {
                if (err) {
                    var filename = path.resolve(cfg.db.importPath, self.colname);
                    var data = fs.readFile(filename, function (err, data) {
                        if (err) {
                            done(err);
                            return;
                        }
                        try {
                            var json = JSON.parse(data.toString());
                            self.db.createCollection(self.colname, function (err, col) {
                                if (err) {
                                    done(err);
                                    return;
                                }
                                var tubes = json;
                                var insertTube = function (index) {
                                    if (index >= tubes.length) {
                                        done();
                                        return;
                                    }
                                    var otube = tubes[index];
                                    var tbase;
                                    var ttype;
                                    var tpins;
                                    var c = {};
                                    var insertTubeInDB = function () {
                                        var unit = {
                                            vamax: otube.vamax,
                                            vg2max: otube.vg2max,
                                            vhknmax: otube.vhkmax,
                                            vhkpmax: otube.vhkmax,
                                            pamax: otube.pamax,
                                            pg2max: otube.pg2max,
                                            ikmax: otube.ikmax,
                                            cgk: parseFloat(otube.cgk),
                                            cak: parseFloat(otube.cak),
                                            cga: parseFloat(otube.cga),
                                        };
                                        var units = [];
                                        if (!ttype || !ttype.cfg || ttype.cfg.length === 0) {
                                            units = [unit];
                                        }
                                        else if (unit.pg2max || unit.vg2max) {
                                            units = [];
                                            for (var t = 0; t < ttype.cfg.length; t++) {
                                                var config = ttype.cfg[t];
                                                if (config.pins.length > 3) {
                                                    units.push(unit);
                                                    break;
                                                }
                                                units.push({});
                                            }
                                        }
                                        else if (unit.cgk || unit.cga) {
                                            units = [];
                                            for (var t = 0; t < ttype.cfg.length; t++) {
                                                var config = ttype.cfg[t];
                                                if (config.pins.length > 2) {
                                                    units.push(unit);
                                                    break;
                                                }
                                                units.push({});
                                            }
                                        }
                                        else {
                                            units = [unit];
                                        }
                                        var tubeData = {
                                            _id: undefined,
                                            type: ttype && ttype._id,
                                            base: tbase && tbase._id,
                                            pinout: tpins && tpins._id,
                                            name: otube.name,
                                            vh1: otube.vh,
                                            ih1: otube.ih,
                                            units: units,
                                            notes: otube.notes
                                        };
                                        self.createDataIndexes(tubeData);
                                        col.insert(tubeData, function (err, col) {
                                            if (err) {
                                                done(err);
                                                return;
                                            }
                                            insertTube(index + 1);
                                        });
                                    };
                                    var searchTubePinout = function () {
                                        if (otube.pinout) {
                                            var regex = new RegExp(['^', otube.pinout, '$'].join(''), 'i');
                                            pinsModule.getPinouts({ name: regex }, function (err, result) {
                                                if (err) {
                                                    done(err);
                                                    return;
                                                }
                                                if (result.length === 0) {
                                                    done({
                                                        message: 'Pinout ' + otube.pinout + ' not found, please correct the tubes.json file.',
                                                        name: 'notfound'
                                                    });
                                                    return;
                                                }
                                                tpins = result[0];
                                                insertTubeInDB();
                                            });
                                        }
                                        else {
                                            insertTubeInDB();
                                        }
                                    };
                                    var searchTubeBase = function () {
                                        if (otube.base) {
                                            var regex = new RegExp(["^", otube.base, "$"].join(""), "i");
                                            basesModule.getBases({ name: regex }, function (err, result) {
                                                if (err) {
                                                    done(err);
                                                    return;
                                                }
                                                if (result.length === 0) {
                                                    done({
                                                        message: 'base ' + otube.base + ' not found, please correct the tubes.json file.',
                                                        name: 'notfound'
                                                    });
                                                    return;
                                                }
                                                tbase = result[0];
                                                searchTubePinout();
                                            });
                                        }
                                        else {
                                            searchTubePinout();
                                        }
                                    };
                                    if (otube.type) {
                                        var s = otube.type.replace('(', '\\(').replace(')', '\\)');
                                        var regex = new RegExp(["^", s, "$"].join(""), "i");
                                        typesModule.getTypes({ text: regex }, function (err, result) {
                                            if (err) {
                                                done(err);
                                                return;
                                            }
                                            if (result.length === 0) {
                                                done({
                                                    message: 'type ' + otube.type + ' not found, please correct the tubes.json file.',
                                                    name: 'notfound'
                                                });
                                                return;
                                            }
                                            ttype = result[0];
                                            searchTubeBase();
                                        });
                                    }
                                    else {
                                        searchTubeBase();
                                    }
                                };
                                insertTube(0);
                            });
                        }
                        catch (e) {
                            done(e);
                        }
                    });
                }
                else {
                    done();
                }
            });
        };
        return Tubes;
    })();
    dbmodules.Tubes = Tubes;
})(dbmodules || (dbmodules = {}));
module.exports = dbmodules;
//# sourceMappingURL=tubes.js.map
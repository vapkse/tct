'use strict';
var tubesModule = require('./tubes');
var dbmodules;
(function (dbmodules) {
    var News = (function () {
        function News(db) {
            this.colname = 'news.json';
            var self = this;
            self.db = db;
        }
        News.prototype.addNews = function (news, done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                var save = function () {
                    col.insert(news, function (err, result) {
                        done(err, result);
                    });
                };
                col.find({ text: news.text }).toArray(function (err, old) {
                    if (err) {
                        done(err);
                        return;
                    }
                    var toDelete = {
                        $or: []
                    };
                    var sparams = JSON.stringify(news.params);
                    old.forEach(function (value) {
                        if (JSON.stringify(value.params) === sparams) {
                            toDelete.$or.push({ '_id': value._id });
                        }
                    });
                    if (toDelete.$or.length === 0) {
                        save();
                    }
                    else {
                        col.remove(toDelete, function (err, result) {
                            save();
                        });
                    }
                });
            });
        };
        News.prototype.updateNews = function (news, done) {
            var self = this;
            if (!news) {
                done({
                    name: 'msg-newsismand',
                    message: 'News is mandatory.'
                });
                return;
            }
            if (!news._id) {
                done({
                    name: 'msg-newsidismand',
                    message: 'News ID is mandatory.'
                });
                return;
            }
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.findOne({ '_id': news._id }, function (err, dbn) {
                    if (err) {
                        done(err);
                        return;
                    }
                    if (!news) {
                        done({
                            name: 'msg-newsnotfound',
                            message: 'News not found.'
                        });
                        return;
                    }
                    var extend = require('extend');
                    delete news._id;
                    var merged = extend(true, {}, dbn, news);
                    col.save(merged, function (err, result) {
                        done(err, merged);
                    });
                });
            });
        };
        News.prototype.deleteNews = function (id, done) {
            var self = this;
            if (!id) {
                done({
                    name: 'msg-newsidismand',
                    message: 'News ID is mandatory.'
                });
                return;
            }
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                }
                col.findOne({ '_id': id }, function (err, news) {
                    if (err) {
                        done(err);
                    }
                    else if (!news) {
                        done();
                    }
                    else {
                        col.remove({ '_id': id }, function (err, result) {
                            done(err);
                        });
                    }
                });
            });
        };
        News.prototype.getLatestNews = function (done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.find().toArray(function (err, news) {
                    news = news && news.sort(function (a, b) {
                        var da = new Date(a.date);
                        var db = new Date(b.date);
                        return da < db ? 1 : -1;
                    });
                    done(null, news);
                });
            });
        };
        News.prototype.initCollection = function (done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err, collection) {
                if (err) {
                    try {
                        self.db.createCollection(self.colname, function (err, col) {
                            if (err) {
                                done(err);
                            }
                            else {
                                var tubes = new tubesModule.Tubes(self.db);
                                tubes.getTubesCount(function (err, tubesCount) {
                                    var news = {
                                        date: new Date().toJSON(),
                                        text: 'Database created, ' + String(tubesCount) + ' tubes avalaible.',
                                        uid: 'news0',
                                    };
                                    col.insert(news, function (err, col) {
                                        done(err);
                                    });
                                });
                            }
                        });
                    }
                    catch (e) {
                        done(e);
                    }
                }
                else {
                    done(err);
                }
            });
        };
        return News;
    })();
    dbmodules.News = News;
})(dbmodules || (dbmodules = {}));
module.exports = dbmodules;
//# sourceMappingURL=news.js.map
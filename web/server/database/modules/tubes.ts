'use strict';

import express = require('express');
import cfg = require('../../../config');
import fs = require('fs');
import path = require('path');
import tubeBasesModule = require('./tube-bases');
import tubeTypesModule = require('./tube-types');
import tubePinoutsModule = require('./tube-pins');
import tct = require('../../common/ts/tube');
import search = require('../../common/ts/search-params');
import User = require('../../common/ts/user');
import usersModule = require('./users');

module dbmodules {
    export class Tubes {
        private db: dbengine.Db;
        private colname = 'tubes.json';

        constructor(db: dbengine.Db) {
            var self = <Tubes>this;
            self.db = db;
        }

        private getUserColName = function(userid: string) {
            var self = <Tubes>this;
            return userid + '-' + self.colname
        }

        // ====================================================================
        //
        // Save global tube method
        //
        // ====================================================================        
        public saveGlobalTube(data: TubeData, user: User.DbUser, done: (err: Error, result?: TubeSearchResult) => void) {
            var self = <Tubes>this;

            if (!user || user.role !== 'admin') {
                done({
                    name: 'notlogged',
                    message: 'You must be logged in to use this service.'
                });
                return;
            }

            var validateDatas = function(tubeData: TubeData, done: (err: Error, result?: TubeSearchResult) => void) {
                self.getLookups(tubeData, function(err, result) {
                    if (err) {
                        done(err);
                        return;
                    }

                    // Check dataset validation
                    // Use common tube object for validation
                    var tube = new tct.Tube(result);
                    var verrors = tube.validate();
                    if (verrors.length) {
                        // Dataset validation was already done client side, just doing again to protect the database, but no need to return all the errors
                        done(verrors[0])
                        return;
                    }

                    done(null, result);
                });
            }

            validateDatas(data, function(err, result) {
                if (err) {
                    done(err);
                    return;
                }

                self.db.collection(self.colname, { strict: true }, function(err, collection) {
                    if (err) {
                        done(err);
                        return;
                    }

                    // Update, search original on global collection by name
                    collection.findOne({ name: new RegExp('^' + result.tubeData.name + '$', 'i') }, function(err: Error, originalData: any) {
                        if (err) {
                            done(err);
                            return;
                        }

                        // Create indexation
                        self.createDataIndexes(result.tubeData);

                        if (originalData) {
                            var originalId = originalData._id.id || originalData._id;
                            if (result.tubeData._id !== originalId) {
                                done({
                                    name: 'invalidtubeid',
                                    message: 'Invalid tube id.'
                                });
                                return;
                            }

                            // Update datas                                                                
                            collection.save(result.tubeData, function(err, r) {
                                self.getLookups(result.tubeData, function(err, saved) {
                                    done(err, saved);
                                })
                            })
                        } else {
                            if (result.tubeData._id) {
                                done({
                                    name: 'invalidtubeid',
                                    message: 'Invalid tube id.'
                                });
                                return;
                            }

                            // Tube not found, add a new one, ensure id is not 0
                            delete result.tubeData._id;

                            // Insert datas
                            collection.insert(result.tubeData, function(err, gtube) {
                                self.getLookups(gtube[0], function(err, saved) {
                                    done(err, saved);
                                })
                            })
                        }
                    })
                })
            })
        }

        // ====================================================================
        //
        // Save tube method
        //
        // ====================================================================        
        public saveTube(tubeData: TubeData, user: User.User, done: (err: Error, result?: TubeSearchResult) => void) {
            var self = <Tubes>this;
            var userid = user && user._id;

            if (!userid) {
                done({
                    name: 'notlogged',
                    message: 'You must be logged in to use this service.'
                });
                return;
            }

            var updateTube = function(result: TubeSearchResult) {
                // Open collection for personal edition
                self.db.collection(self.getUserColName(userid), { strict: false }, function(err, personalCol) {
                    if (err) {
                        done(err);
                        return;
                    }

                    delete result.tubeData._userId;

                    // Create indexation
                    self.createDataIndexes(result.tubeData);

                    // Save datas                                                                
                    personalCol.save(result.tubeData, function(err, r) {
                        self.getLookups(result.tubeData, function(err, saved) {
                            done(err, saved);
                        })
                    })
                })
            }


            var insertTube = function(tubeInfos: TubeSearchResult) {
                // New, save to personal collection                                  
                // Check if the name already exists
                var regex = new RegExp('^' + tubeInfos.tubeData.name + '$', 'i');
                self.getTube({ name: regex }, userid, false, function(err, result, isGlobalTube) {
                    if (err) {
                        done(err);
                        return;
                    }

                    // An administartor can duplicate a tube on his local database if the tube don't exists
                    if (result && result.tubeData && !(isGlobalTube && user && user.role === 'admin')) {
                        // Tube already exists
                        done({
                            name: 'name-exists',
                            message: 'A tube with this name already exists.',
                            type: 'error',
                            fieldName: 'name',
                            tooltip: 'true'
                        } as validation.Message)
                        return;
                    }

                    // Create indexation
                    self.createDataIndexes(tubeInfos.tubeData);

                    // Ensure new id
                    delete tubeInfos.tubeData._id;

                    // Save global tube
                    // Open private collection
                    self.db.collection(self.getUserColName(userid), { strict: false }, function(err, userCol) {
                        if (err) {
                            done(err);
                            return;
                        }

                        // Insert in the user collection
                        userCol.insert(tubeInfos.tubeData, function(err, userTube) {
                            if (err) {
                                done(err);
                                return;
                            }

                            self.getLookups(userTube[0], function(err, result) {
                                done(err, result);
                            })
                        })
                    })
                })
            }

            var validateDatas = function(tubeData: TubeData, done: (err: Error, result?: TubeSearchResult) => void) {
                self.getLookups(tubeData, function(err, result) {
                    if (err) {
                        done(err);
                        return;
                    }

                    // Check dataset validation
                    // Use common tube object for validation
                    var tube = new tct.Tube(result);
                    var verrors = tube.validate();
                    if (verrors.length) {
                        // Dataset validation was already done client side, just doing again to protect the database, but no need to return all the errors
                        done(verrors[0])
                        return;
                    }

                    done(null, result);
                });
            }

            validateDatas(tubeData, function(err, result) {
                if (err) {
                    done(err);
                    return;
                }

                if (tubeData._id) {
                    updateTube(result);
                } else {
                    insertTube(result);
                }
            });
        }

        private getLookups(tubeData: TubeData, done: (err: Error, result?: TubeSearchResult) => void) {
            var self = <Tubes>this;

            var returnValue: TubeSearchResult = {
                tubeData: tubeData
            }

            var getType = function() {
                if (returnValue.tubeData.type) {
                    var typesModule = new tubeTypesModule.TubeTypes(self.db);
                    typesModule.getTypes({ _id: tubeData.type }, function(err, result) {
                        if (err) {
                            done(err);
                        } else {
                            returnValue.type = result.length && result[0];
                            done(null, returnValue);
                        }
                    })
                } else {
                    done(null, returnValue);
                }
            }

            var getBase = function() {
                if (returnValue.tubeData.base) {
                    var basesModule = new tubeBasesModule.TubeBases(self.db);
                    basesModule.getBases({ _id: tubeData.base }, function(err, result) {
                        if (err) {
                            done(err);
                        } else {
                            returnValue.base = result[0];
                            getType();
                        }
                    })
                } else {
                    getType();
                }
            }

            var getPinout = function() {
                if (returnValue.tubeData.pinout) {
                    var pinsModule = new tubePinoutsModule.TubePins(self.db);
                    pinsModule.getPinouts({ _id: tubeData.pinout }, function(err, result) {
                        if (err) {
                            done(err);
                        } else {
                            returnValue.pinout = result[0];
                            getBase();
                        }
                    })
                } else {
                    getBase();
                }
            }

            getPinout();
        }

        // ====================================================================
        //
        // Delete global tube methode
        //
        // ====================================================================        
        public deleteGlobalTube(id: number, done: (err: Error, result?: TubeData) => void) {
            var self = <Tubes>this;

            if (!id || isNaN(id)) {
                done({
                    name: 'invalid_id',
                    message: 'Invalid id.'
                });
                return;
            }

            self.db.collection(self.colname, { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                var selector = { _id: id };
                col.findOne(selector, function(err: Error, tubeData: TubeData) {
                    if (err) {
                        done(err);
                        return;
                    }
                    if (!tubeData) {
                        done({
                            name: 'not_found',
                            message: 'Not found.'
                        });
                        return;
                    }

                    col.remove(selector, function(err, result) {
                        done(err, tubeData);
                    })
                })
            })
        }
        
        // ====================================================================
        //
        // Delete tube methode
        //
        // ====================================================================        
        public deleteTube(id: number, userid: string, done: (err: Error, result?: TubeData) => void) {
            var self = <Tubes>this;

            if (!userid) {
                done({
                    name: 'notlogged',
                    message: 'You must be logged in to use this service.'
                });
                return;
            }

            if (!id || isNaN(id)) {
                done({
                    name: 'invalid_id',
                    message: 'Invalid id.'
                });
                return;
            }

            self.db.collection(self.getUserColName(userid), { strict: true }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                var selector = { _id: id };
                col.findOne(selector, function(err: Error, tubeData: TubeData) {
                    if (err) {
                        done(err);
                        return;
                    }
                    if (!tubeData) {
                        done({
                            name: 'not_found',
                            message: 'Not found.'
                        });
                        return;
                    }

                    col.remove(selector, function(err, result) {
                        done(err, tubeData);
                    })
                })
            })
        }

        // ====================================================================
        //
        // New tube methode
        //
        // ====================================================================        
        public newTube(done: (err: Error, result?: TubeSearchResult) => void) {
            var self = <Tubes>this;

            self.getLookups({ _id: 0 }, function(err, result) {
                done(err, result);
            })
        }

        // ====================================================================
        //
        // Get tube methode
        //
        // ====================================================================        
        public getTube(selector: Object, userid: string, forViewer: boolean, done: (err: Error, result?: TubeSearchResult, isGlobalTube?: boolean) => void) {
            var self = <Tubes>this;

            if (userid && typeof userid !== 'string') {
                done({
                    name: 'invaliduser',
                    message: 'Invalid User ID.'
                })
                return;
            }

            var getUserInfos = function(tubeResult: TubeSearchResult, done: (err: Error, result?: UserTubeSearchResult) => void) {
                // Search tubes for all users    
                new usersModule.Users(self.db).findUserInfos({ _id: userid }, function(err, user) {
                    if (err) {
                        done(err)
                        return;
                    }

                    var userResult = tubeResult as UserTubeSearchResult;
                    userResult.userInfos = {
                        _id: user._id,
                        userName: user.userName,
                        email: user.email
                    }
                    done(null, userResult);
                })
            }

            var complete = function(tubeData: TubeData, isGlobalTube: boolean) {
                if (!tubeData) {
                    // Not found
                    done(null, null);
                    return;
                }
                self.getLookups(tubeData, function(err, result) {
                    if (err) {
                        done(err);
                    } else {
                        if (!forViewer) {
                            done(err, result, isGlobalTube);
                        } else if (userid) {
                            getUserInfos(result, function(err, result3) {
                                done(err, result3, isGlobalTube);
                            })
                        } else {
                            done(err, result, isGlobalTube);
                        }
                    }
                })
            }

            var findUserTube = function(selector: Object, cb: (err: Error, result?: TubeData) => void) {
                if (!userid) {
                    cb(null);
                    return;
                }

                self.db.collection(self.getUserColName(userid), { strict: false }, function(err, col) {
                    if (err) {
                        cb(err);
                        return;
                    }

                    col.findOne(selector, function(err, tubeData) {
                        if (err) {
                            cb(err);
                            return;
                        }

                        cb(null, tubeData);
                    })
                })
            }

            var findGlobalTube = function(selector: Object, cb: (err: Error, result?: TubeData) => void) {
                self.db.collection(self.colname, { strict: true }, function(err, col) {
                    if (err) {
                        done(err);
                        return;
                    }
                    col.findOne(selector, function(err, tubeData) {
                        if (err) {
                            done(err);
                        } else {
                            cb(null, tubeData);
                        }
                    })
                })
            }

            if (userid) {
                findUserTube(selector, function(err, tubeData) {
                    if (err) {
                        done(err);
                    } else if (tubeData) {
                        complete(tubeData, false);
                    } else {
                        findGlobalTube(selector, function(err, tubeData) {
                            if (err) {
                                done(err);
                            } else {
                                complete(tubeData, true);
                            }
                        })
                    }
                })
            } else {
                findGlobalTube(selector, function(err, tubeData) {
                    if (err) {
                        done(err);
                    } else {
                        complete(tubeData, true);
                    }
                })
            }
        }

        // ====================================================================
        //
        // renameDocumentInCollection tube method
        //
        // ====================================================================        
        public renameDocumentInCollection(colname: string, srcName: string, dstName: string, done: (err: Error, result?: Array<TubeData>) => void) {
            var self = <Tubes>this;

            self.db.collection(colname, { strict: false }, function(err, col) {
                if (err) {
                    done(err, []);
                    return;
                }
                
                col.find({ 'documents.filename': srcName }, { '_tiar.documents.filename': 0 }).toArray(function(err: Error, tubes: Array<TubeData>) {
                    if (err) {
                        done(err);
                        return;
                    }
                    
                    if (!tubes || tubes.length === 0) {
                        done({
                            name: 'msg-linknotfound',
                            message: 'Link not found.'
                        });
                        return;
                    }

                    var updateDoc = function(index: number) {
                        if (index >= tubes.length) {
                            done(null);
                            return;
                        }

                        var tube = tubes[index];
                        var found = false;
                        if (tube.documents.length) {
                            for (var i = 0; i < tube.documents.length; i++) {
                                if (tube.documents[i].filename === srcName) {
                                    found = true;
                                    tube.documents[i].filename = dstName;
                                }
                            }
                        }
                        if (!found) {
                            done({
                                name: 'msg-linknotfound',
                                message: 'Link not found.'
                            });
                        } else {
                            col.update({ _id: tube._id }, tube, function(err) {
                                if (err) {
                                    done(err);
                                } else {
                                    updateDoc(index + 1);
                                }
                            })
                        }
                    }
                    updateDoc(0);
                })
            })
        }

        // ====================================================================
        //
        // getTubeDataFromCollection tube method
        //
        // ====================================================================        
        public getTubeDataFromCollection(colname: string, done: (err: Error, result?: Array<TubeData>) => void) {
            var self = <Tubes>this;

            self.db.collection(colname, { strict: false }, function(err, col) {
                if (err) {
                    done(err, []);
                    return;
                }

                col.find().toArray(function(err: Error, qresult: Array<TubeData>) {
                    done(err, qresult);
                })
            })
        }

        // ====================================================================
        //
        // GetTubesCount tube method
        //
        // ====================================================================        
        public getTubesCount(done: (err: Error, result?: number) => void) {
            var self = <Tubes>this;

            self.db.collection(self.colname, { strict: false }, function(err, col) {
                if (err) {
                    done(err, 0);
                    return;
                }

                col.find().toArray(function(err: Error, qresult: Array<TubeData>) {
                    done(err, qresult.length);
                })
            })
        }

        // ====================================================================
        //
        // Search tube method
        //
        // ====================================================================        
        public searchTubes(expression: string, user: User.DbUser, done: (err: Error, result?: Array<any>, users?: { [id: string]: User.UserEmail }) => void) {
            var self = <Tubes>this;
            var allUsersSearch = user && user.role === 'admin';
            var sort = false;
            var users: { [id: string]: User.UserEmail } = {};

            var findUserTubes = function(qsel: search.FieldExpression | search.OperatorExpression, fields: { [field: string]: number }, usr: User.DbUser, cb: (err: Error, result: Array<TubeData>) => void) {
                var userid = usr && usr._id;

                if (!userid) {
                    cb(null, []);
                    return;
                }

                self.db.collection(self.getUserColName(userid), { strict: false }, function(err, col) {
                    if (err) {
                        cb(err, []);
                        return;
                    }

                    col.find(qsel, fields).toArray(function(err: Error, qresult: Array<TubeData>) {
                        if (err) {
                            cb(err, []);
                            return;
                        }

                        qresult.map(td => td._userId = userid);
                        cb(null, qresult);
                    })
                })
            }

            var addTubesInGlobalCollection = function(qsel: search.FieldExpression | search.OperatorExpression, fields: { [field: string]: number }, excludeList: { [name: string]: TubeData }, result: Array<TubeData>) {
                // Search tube in global collection
                self.db.collection(self.colname, { strict: true }, function(err, col) {
                    if (err) {
                        done(err);
                        return;
                    }

                    col.find(qsel, fields, function(err, cursor) {
                        if (err) {
                            done(err);
                            return;
                        }

                        cursor.each(function(err: Error, item: TubeData) {
                            if (err) {
                                done(err);
                                return;
                            }

                            if (!item) {
                                if (sort || allUsersSearch) {
                                    result.sort(function(a, b) {
                                        var ir: number;
                                        if (!a._text && !b._text) {
                                            ir = 0;
                                        } else if (a._text && !b._text) {
                                            ir = -1;
                                        } else if (!a._text && b._text) {
                                            ir = 1;
                                        } else {
                                            ir = a._text.indexOf(expression) - b._text.indexOf(expression);
                                        }

                                        if (ir === 0) {
                                            ir = a.name.localeCompare(b.name);
                                        }
                                        return ir;
                                    });
                                }
                                done(null, result, users);
                                return;
                            }

                            // Add only the tube not in the user table
                            if (!excludeList || !excludeList[item.name]) {
                                result.push(item);
                            }
                        })
                    })
                })
            }

            search.TubeSearch.FromQueryExpression(expression, function(err, tubeSearch) {
                if (err) {
                    done(err);
                    return;
                }

                tubeSearch.getFieldExpressions(function(err: Error, fieldExprs: Array<search.FieldExpression>, fields: { [field: string]: number }, userExprs: search.OperatorExpression, allUserSearch: boolean) {
                    if (err) {
                        done(err)
                        return;
                    }

                    var qsel: search.OperatorExpression;
                    if (fieldExprs.length === 0 && !userExprs && !allUserSearch) {
                        done(null, []);
                        return;
                    } else if (fieldExprs.length === 0) {
                        // All tubes search
                        qsel = undefined;
                    } else {
                        // Construct query expression
                        qsel = { $and: fieldExprs };
                    }

                    var findTubes = function(qsel: search.OperatorExpression, qusers?: Array<User.User>) {
                        if (!qusers || qusers.length === 0) {
                            // Search tubes in private collection
                            findUserTubes(qsel, fields, user, function(err, result) {
                                if (err) {
                                    done(err)
                                    return;
                                }
                                // Get the list of all user tubes for the exclusion of global list
                                findUserTubes(null, null, user, function(err, excludeList) {
                                    if (err) {
                                        done(err)
                                        return;
                                    }

                                    var list: { [name: string]: TubeData } = {};
                                    excludeList.map(td => list[td.name] = td);
                                    addTubesInGlobalCollection(qsel, fields, list, result);
                                })
                            })
                        } else {
                            // Search specific users tubes
                            var result = [] as Array<TubeData>;
                            var addUser = function(index: number) {
                                if (index >= qusers.length) {
                                    // No more users
                                    // All user tubes are added, add also all global tubes 
                                    done(null, result, users);;
                                    return;
                                }
                                // Search tubes for the current user
                                var u = qusers[index];
                                findUserTubes(qsel, fields, u, function(err, qres) {
                                    if (err) {
                                        done(err)
                                        return;
                                    }
                                    // Add user tubes to the collection
                                    if (qres) {
                                        qres.map(tdu => result.push(tdu));
                                    }
                                    // Add user to the userlist
                                    users[u._id] = {
                                        userName: u.userName,
                                        email: u.email
                                    } as User.UserEmail;
                                    // Next
                                    addUser(++index);
                                })
                            }
                            addUser(0);
                        }
                    }

                    if (allUserSearch) {
                        // All users search
                        new usersModule.Users(self.db).getAllUsers(function(err, allusers) {
                            if (err) {
                                done(err)
                                return;
                            }

                            findTubes(qsel, allusers);
                        })
                    } else if (userExprs) {
                        // Specific user search
                        new usersModule.Users(self.db).findUsers(userExprs, function(err, usrs) {
                            if (err) {
                                done(err)
                                return;
                            }

                            findTubes(qsel, usrs);
                        })
                    } else {
                        findTubes(qsel);
                    }
                })
            })
        }

        // ====================================================================
        //
        // Create and import tube methodes
        //
        // ====================================================================        
        public createTextIndex(done: (err: Error, result?: Array<any>) => void) {
            var self = <Tubes>this;

            var openCollection = function() {
                self.db.collection(self.colname, { strict: true }, function(err, col) {
                    if (err) {
                        done(err);
                        return;
                    }
                    col.createIndex({ "_text": 1 }, {}, function(e: Error, result: any) {
                        done(e);
                    });
                })
            }

            openCollection();
        }

        private createDataIndexes(t: TubeData) {
            var searchIndex: Array<string> = [];
            searchIndex.push(t.name);
            if (t.type) {
                searchIndex.push('ttype_' + t.type);
            }
            if (t.base) {
                searchIndex.push('tbase_' + t.base);
            }
            if (t.pinout) {
                searchIndex.push('tpins_' + t.pinout);
            }
            if (t.notes) {
                searchIndex.push(t.notes);
            }
            t._text = searchIndex.join(' ');
        }

        public initCollection(done: (err?: Error) => void) {
            var self = <Tubes>this;
            var typesModule = new tubeTypesModule.TubeTypes(self.db);
            var basesModule = new tubeBasesModule.TubeBases(self.db);
            var pinsModule = new tubePinoutsModule.TubePins(self.db);

            self.db.collection(self.colname, { strict: true }, function(err, collection) {
                if (err) {
                    // Create collection
                    var filename = path.resolve(cfg.db.importPath, self.colname);
                    var data = fs.readFile(filename, function(err, data) {
                        if (err) {
                            done(err);
                            return;
                        }
                        try {
                            var json = JSON.parse(data.toString());
                            self.db.createCollection(self.colname, function(err, col) {
                                if (err) {
                                    done(err);
                                    return;
                                }
                                var tubes = <Array<any>>json;
                                var insertTube = function(index: number) {
                                    if (index >= tubes.length) {
                                        done();
                                        return;
                                    }

                                    var otube = tubes[index];
                                    var tbase: TubeBase;
                                    var ttype: TubeType;
                                    var tpins: TubePinout;
                                    var c: dbengine.MongoCollectionOptions = {}

                                    var insertTubeInDB = function() {
                                        var unit: TubeUnitData = {
                                            vamax: otube.vamax,
                                            vg2max: otube.vg2max,
                                            vhknmax: otube.vhkmax,
                                            vhkpmax: otube.vhkmax,
                                            pamax: otube.pamax,
                                            pg2max: otube.pg2max,
                                            ikmax: otube.ikmax,
                                            cgk: parseFloat(otube.cgk),
                                            cak: parseFloat(otube.cak),
                                            cga: parseFloat(otube.cga),
                                        }

                                        // Search best unit
                                        var units: Array<TubeUnitData> = [];
                                        if (!ttype || !ttype.cfg || ttype.cfg.length === 0) {
                                            // First one
                                            units = [unit];
                                        } else if (unit.pg2max || unit.vg2max) {
                                            // Heptode, Pentode or tetrode             
                                            units = [];
                                            for (var t = 0; t < ttype.cfg.length; t++) {
                                                var config = ttype.cfg[t];
                                                if (config.pins.length > 3) {
                                                    units.push(unit);
                                                    break;
                                                }
                                                units.push({});
                                            }
                                        } else if (unit.cgk || unit.cga) {
                                            // Triode
                                            units = [];
                                            for (var t = 0; t < ttype.cfg.length; t++) {
                                                var config = ttype.cfg[t];
                                                if (config.pins.length > 2) {
                                                    units.push(unit);
                                                    break;
                                                }
                                                units.push({});
                                            }
                                        } else {
                                            // First one
                                            units = [unit];
                                        }

                                        var tubeData: TubeData = {
                                            _id: undefined,
                                            type: ttype && ttype._id,
                                            base: tbase && tbase._id,
                                            pinout: tpins && tpins._id,
                                            name: otube.name,
                                            vh1: otube.vh,
                                            ih1: otube.ih,
                                            units: units,
                                            notes: otube.notes
                                        }

                                        self.createDataIndexes(tubeData);

                                        col.insert(tubeData, function(err, col) {
                                            if (err) {
                                                done(err);
                                                return;
                                            }
                                            insertTube(index + 1);
                                        })
                                    }

                                    // Search for pinouts
                                    var searchTubePinout = function() {
                                        if (otube.pinout) {
                                            var regex = new RegExp(['^', otube.pinout, '$'].join(''), 'i');
                                            pinsModule.getPinouts({ name: regex }, function(err, result) {
                                                if (err) {
                                                    done(err);
                                                    return;
                                                }

                                                if (result.length === 0) {
                                                    done({
                                                        message: 'Pinout ' + otube.pinout + ' not found, please correct the tubes.json file.',
                                                        name: 'notfound'
                                                    });
                                                    return;
                                                }

                                                tpins = result[0] as TubePinout;
                                                insertTubeInDB();
                                            })
                                        } else {
                                            insertTubeInDB();
                                        }
                                    }

                                    // Search fo base
                                    var searchTubeBase = function() {
                                        if (otube.base) {
                                            var regex = new RegExp(["^", otube.base, "$"].join(""), "i");
                                            basesModule.getBases({ name: regex }, function(err, result) {
                                                if (err) {
                                                    done(err);
                                                    return;
                                                }

                                                if (result.length === 0) {
                                                    done({
                                                        message: 'base ' + otube.base + ' not found, please correct the tubes.json file.',
                                                        name: 'notfound'
                                                    });
                                                    return;
                                                }

                                                tbase = result[0] as TubeBase;
                                                searchTubePinout();
                                            })
                                        } else {
                                            searchTubePinout();
                                        }
                                    }

                                    // Search for type 
                                    if (otube.type) {
                                        var s = otube.type.replace('(', '\\(').replace(')', '\\)');
                                        var regex = new RegExp(["^", s, "$"].join(""), "i");
                                        typesModule.getTypes({ text: regex }, function(err, result) {
                                            if (err) {
                                                done(err);
                                                return;
                                            }

                                            if (result.length === 0) {
                                                done({
                                                    message: 'type ' + otube.type + ' not found, please correct the tubes.json file.',
                                                    name: 'notfound'
                                                });
                                                return;
                                            }

                                            ttype = result[0] as TubeType;
                                            searchTubeBase();
                                        })
                                    } else {
                                        searchTubeBase();
                                    }
                                }
                                insertTube(0);
                            })
                        } catch (e) {
                            done(e);
                        }
                    });
                } else {
                    done();
                }
            });
        }
    }
}

export = dbmodules;
'use strict';

import cfg = require('../../../config');
import fs = require('fs');
import path = require('path');
import cacheProvider = require('../../utils/cache');

var cache = cacheProvider.Cache;
var extend = require('extend');

module dbmodules {
    export class Translations {
        private db: dbengine.Db;
        private colname = 'translations.json';

        constructor(db: dbengine.Db) {
            var self = <Translations>this;
            self.db = db;
        }

        public addProposal(proposal: TranslationProposal, done: (err: Error) => void) {
            var self = <Translations>this;
            var colname = 'i18n_proposal.json';

            if (!proposal.original || !proposal.proposal || !proposal.locale || !proposal.userName) {
                done({
                    name: 'wrongparams',
                    message: 'Wrong Parameters.'
                })
                return;
            }

            self.db.collection(colname, { strict: false }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                col.insert(proposal, function(err, docs) {
                    done(err);
                });
            })
        }

        public updateTranslations(locale: string, translations: Array<TranslationData>, done: (err: Error) => void) {
            var self = <Translations>this;
            var colname = 'i18n_' + locale + '.json';

            if (translations.length === 0) {
                done({
                    name: 'wrongparams',
                    message: 'Wrong Parameters.'
                })
                return;
            }

            self.db.collection(colname, { strict: false }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                var ids = [] as any;
                translations.map(t => ids.push(t._id));
                col.remove({
                    '_id': {
                        $in: ids
                    }
                }, function(e, docs) {
                    col.insert(translations, function(err, docs) {
                        // Clear memory caches for edited locale
                        cache.clear(locale);

                        done(err);
                    });
                })
            })
        }

        public editTranslations(editedRecord: TranslationRecord, sl: string, newid: string, done: (err: Error, tr?: TranslationRecord) => void) {
            var self = <Translations>this;

            var updateTables = function() {
                // !!Mongodb compatibility!!
                fs.readdir(path.resolve(cfg.path.data), function(err, items) {
                    if (err) {
                        done(err);
                    } else {
                        var selector = { '_id': editedRecord._id };

                        var updateTable = function(index: number) {
                            if (index >= items.length) {
                                // Clear memory caches for edited locale
                                cache.clear(sl);
                                                                
                                // finished
                                done(null, editedRecord);
                                return;
                            }

                            var colname = items[index];
                            var match = /i18n\_(.*)\.json$/.exec(colname);
                            if (match && match[1] !== 'proposal') {
                                self.db.collection(colname, { strict: false }, function(err, col) {
                                    if (err) {
                                        done(err);
                                        return;
                                    }

                                    col.findOne(selector, function(err: Error, dbr: TranslationRecord) {
                                        if (err) {
                                            done(err);
                                        } else if (!dbr) {
                                            updateTable(index + 1);
                                        } else {
                                            var update = false;
                                            if (match[1] === 'en') {
                                                if (editedRecord.value && editedRecord.value !== dbr.value) {
                                                    dbr.value = editedRecord.value;
                                                    update = true;
                                                }
                                            } else if (match[1] === sl && editedRecord.selected && editedRecord.selected !== dbr.value) {
                                                dbr.value = editedRecord.selected;
                                                dbr.isBest = true;
                                                update = true;
                                            }

                                            if (newid) {
                                                dbr._id = newid;
                                                editedRecord._id = newid;
                                                col.remove(selector, function(err: Error, result: number) {
                                                    if (err) {
                                                        done(err);
                                                    } else {
                                                        col.insert(dbr, function(err: Error) {
                                                            if (err) {
                                                                done(err);
                                                            } else {
                                                                updateTable(index + 1);
                                                            }
                                                        })
                                                    }
                                                })
                                            } else if (update) {
                                                col.update(selector, dbr, function(err: Error) {
                                                    if (err) {
                                                        done(err);
                                                    } else {
                                                        updateTable(index + 1);
                                                    }
                                                })
                                            } else {
                                                updateTable(index + 1);
                                            }
                                        }
                                    })
                                })
                            } else {
                                updateTable(index + 1);
                            }
                        }
                        updateTable(0);
                    }
                })
            }

            if (newid) {
                // Check if id already exists
                self.db.collection('i18n_en.json', { strict: false }, function(err, col) {
                    if (err) {
                        done(err);
                        return;
                    }

                    col.findOne({ '_id': newid }, function(err: Error, dbr: TranslationRecord) {
                        if (err) {
                            done(err);
                        } else if (!dbr) {
                            updateTables();
                        } else {
                            done({
                                name: 'newidexists',
                                message: 'New id already exists.'
                            })
                        }
                    })
                })
            } else {
                updateTables();
            }
        }

        public deleteTranslations(ids: Array<string>, done: (err: Error) => void) {
            var self = <Translations>this;

            // !!Mongodb compatibility!!
            fs.readdir(path.resolve(cfg.path.data), function(err, items) {
                if (err) {
                    done(err);
                } else {
                    var selector = {
                        '_id': {
                            $in: ids
                        }
                    };

                    var deleteOnTable = function(index: number) {
                        if (index >= items.length) {
                            // finished
                            done(null);
                            return;
                        }

                        var colname = items[index];
                        if (/i18n\_(?!proposal).*\.json$/.test(colname)) {
                            self.db.collection(colname, { strict: false }, function(err, col) {
                                if (err) {
                                    done(err);
                                    return;
                                }
                                col.remove(selector, function(err, result) {
                                    if (err) {
                                        done(err);
                                    } else {
                                        deleteOnTable(index + 1);
                                    }
                                })
                            })
                        } else {
                            deleteOnTable(index + 1);
                        }
                    }
                    deleteOnTable(0);
                }
            })
        }

        public getTranslations(locale: string, done: (err: Error, result?: Array<TranslationData>) => void) {
            var self = <Translations>this;
            var colname = 'i18n_' + locale + '.json';

            self.db.collection(colname, { strict: false }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.find().toArray(function(err, result) {
                    done(err, result);
                })
            })
        }

        public getProposals(done: (err: Error, done?: Array<TranslationProposal>) => void) {
            var self = <Translations>this;
            var colname = 'i18n_proposal.json';

            self.db.collection(colname, { strict: false }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }
                col.find().toArray(function(err, result) {
                    done(err, result);
                })
            })
        }

        public deleteProposals(ids: Array<string>, done: (err: Error) => void) {
            var self = <Translations>this;
            var colname = 'i18n_proposal.json';

            self.db.collection(colname, { strict: false }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                var selector = {
                    '_id': {
                        $in: ids
                    }
                };

                col.remove(selector, function(err, result) {
                    done(err);
                })
            })
        }

        public mergeProposal(proposal: ProposalData, done: (err: Error, tr?: ProposalData) => void) {
            var self = <Translations>this;

            var selector = { '_id': proposal.uid };
            var colname = 'i18n_' + proposal.locale + '.json';
            self.db.collection(colname, { strict: false }, function(err, col) {
                if (err) {
                    done(err);
                    return;
                }

                col.findOne(selector, function(err: Error, dbr: TranslationRecord) {
                    if (err) {
                        done(err);
                    } else if (!dbr) {
                        done({
                            name: 'notfound',
                            message: 'Not Found.'
                        })
                    } else {
                        // Update current record
                        dbr.value = proposal.proposal;
                        dbr.isBest = true;
                        col.update(selector, dbr, function(err: Error) {
                            if (err) {
                                done(err);
                            } else {
                                // Remove original from proposal table
                                self.db.collection('i18n_proposal.json', { strict: false }, function(err, pcol) {
                                    if (err) {
                                        done(err);
                                        return;
                                    }
                                    
                                    // Clear caches for locale
                                    cache.clear(proposal.locale);
                                    
                                    // Remove merged proposal for the proposal table                                    
                                    pcol.remove({ '_id': proposal._id }, function(err, result){
                                        done(err, proposal);                                    
                                    })
                                })
                            }
                        })
                    }
                })
            })
        }
    }
}

export = dbmodules;
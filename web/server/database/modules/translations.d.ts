declare module dbmodules {
    class Translations {
        private db;
        private colname;
        constructor(db: dbengine.Db);
        addProposal(proposal: TranslationProposal, done: (err: Error) => void): void;
        updateTranslations(locale: string, translations: Array<TranslationData>, done: (err: Error) => void): void;
        editTranslations(editedRecord: TranslationRecord, sl: string, newid: string, done: (err: Error, tr?: TranslationRecord) => void): void;
        deleteTranslations(ids: Array<string>, done: (err: Error) => void): void;
        getTranslations(locale: string, done: (err: Error, result?: Array<TranslationData>) => void): void;
        getProposals(done: (err: Error, done?: Array<TranslationProposal>) => void): void;
        deleteProposals(ids: Array<string>, done: (err: Error) => void): void;
        mergeProposal(proposal: ProposalData, done: (err: Error, tr?: ProposalData) => void): void;
    }
}
export = dbmodules;

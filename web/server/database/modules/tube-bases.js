'use strict';
var cfg = require('../../../config');
var fs = require('fs');
var path = require('path');
var dbmodules;
(function (dbmodules) {
    var TubeBases = (function () {
        function TubeBases(db) {
            this.colname = 'tubes-bases.json';
            var self = this;
            self.db = db;
        }
        TubeBases.prototype.getBases = function (selector, done) {
            var self = this;
            var openCollection = function () {
                self.db.collection(self.colname, { strict: true }, function (err, col) {
                    if (err) {
                        done(err);
                        return;
                    }
                    col.find(selector).toArray(function (err, result) {
                        done(err, result);
                    });
                });
            };
            openCollection();
        };
        TubeBases.prototype.initCollection = function (done) {
            var self = this;
            self.db.collection(self.colname, { strict: true }, function (err) {
                if (err) {
                    var filename = path.resolve(cfg.db.importPath, self.colname);
                    var data = fs.readFile(filename, function (err, data) {
                        if (err) {
                            done(err);
                            return;
                        }
                        try {
                            var json = JSON.parse(data.toString());
                            self.db.createCollection(self.colname, function (err, col) {
                                if (err) {
                                    done(err);
                                    return;
                                }
                                col.insert(json, function (err, col) {
                                    if (err) {
                                        done(err);
                                        return;
                                    }
                                    done();
                                });
                            });
                        }
                        catch (e) {
                            done(e);
                        }
                    });
                }
                else {
                    done();
                }
            });
        };
        return TubeBases;
    })();
    dbmodules.TubeBases = TubeBases;
})(dbmodules || (dbmodules = {}));
module.exports = dbmodules;
//# sourceMappingURL=tube-bases.js.map
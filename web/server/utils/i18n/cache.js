'use strict';
var dbengine = require('../../database/engine');
var logger = require('../logger');
var cacheProvider = require('../../utils/cache');
var memoryCache = cacheProvider.Cache;
var TranslationCache = (function () {
    function TranslationCache() {
    }
    TranslationCache.get = function (locale, cb) {
        var done = function (err, cache) {
            if (!cache) {
                cache = {
                    translations: {},
                    locale: locale
                };
            }
            cb(err, cache);
        };
        if (!locale) {
            done();
            return;
        }
        var cache = memoryCache.get('translations_' + locale);
        if (cache) {
            done(null, cache);
        }
        else {
            logger.log('System', 'grey', 'TranslCache', 'No cache for ' + locale + '. Load from DB.');
            dbengine.translations.getTranslations(locale, function (err, result) {
                if (err || !result || result.length === 0) {
                    if (err) {
                        logger.logError('System', 'TranslCache', err);
                    }
                    else {
                        logger.log('System', 'grey', 'TranslCache', 'No cache in DB for ' + locale);
                    }
                    done(err, null);
                }
                else {
                    cache = {
                        locale: locale,
                        translations: {}
                    };
                    memoryCache.set(cache, 'translations_' + locale);
                    result.forEach(function (t) {
                        if (locale === 'en') {
                            cache.translations[t._id] = t.value;
                            TranslationCache.versions[t._id] = t.version;
                        }
                        else if (t.version === TranslationCache.versions[t._id]) {
                            cache.translations[t._id] = t.value;
                        }
                    });
                    logger.log('System', 'green', 'TranslCache', 'Cache for ' + locale + ' restored from DB.');
                    done(null, cache);
                }
            });
        }
    };
    TranslationCache.set = function (locale, translations) {
        var cache = memoryCache.get('translations_' + locale);
        if (!cache) {
            cache = {
                translations: {},
                locale: locale
            };
            memoryCache.set(cache, 'translations_' + locale);
        }
        var updateData = [];
        Object.keys(translations).forEach(function (uid) {
            if (!cache.translations[uid]) {
                cache.translations[uid] = translations[uid];
                TranslationCache.versions[uid] = 1;
                updateData.push({
                    _id: uid,
                    value: translations[uid],
                    version: 1
                });
            }
            else if (locale === 'en' && cache.translations[uid] !== translations[uid]) {
                cache.translations[uid] = translations[uid];
                TranslationCache.versions[uid] = TranslationCache.versions[uid] + 1;
                updateData.push({
                    _id: uid,
                    value: translations[uid],
                    version: TranslationCache.versions[uid]
                });
            }
        });
        if (updateData.length > 0) {
            logger.log('System', 'yellow', 'TranslCache', 'updating translations to DB for ' + locale);
            updateData.map(function (t) { return logger.log('System', 'yellow', 'TranslCache', 'updating ' + t._id + ' -> ' + t.value); });
            dbengine.translations.updateTranslations(locale, updateData, function (err) {
                if (err) {
                    logger.logError('System', 'TranslCache', err);
                }
            });
        }
    };
    TranslationCache.versions = {};
    return TranslationCache;
})();
module.exports = TranslationCache;
//# sourceMappingURL=cache.js.map
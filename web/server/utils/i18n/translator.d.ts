declare class engine {
    private static credentials;
    private static accessTokenCache;
    private static getToken;
    private static languagesCache;
    static getLanguages(locale: string, callback: (languages: translator.RequestResult) => void): void;
    static replaceParameters: (text: string, translationsParams: {
        [id: string]: any;
    }) => string;
    static getTranslations(requestParam: translator.TranslationsParams, callback: (trans: translator.RequestResult) => void): void;
}
export = engine;

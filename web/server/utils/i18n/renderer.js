'use strict';
var translatorEngine = require('./translator');
var sessionProvider = require('../session-provider');
var utils = require('../../common/ts/encoder');
var encoder = utils.Encoder;
var renderer = (function () {
    function renderer() {
    }
    renderer.renderjson = function (opts, json, cb) {
        var sessionId = opts.sessionID;
        var locale = opts.query && opts.query.lng;
        if (!locale) {
            locale = sessionProvider.States.state(sessionId).locale || 'en';
        }
        sessionProvider.States.state(sessionId).locale = locale;
        var nocache = opts.nocache;
        var addObjectToTranslations = function (object, indexToId, translation) {
            for (var msgid in object) {
                translation[msgid] = object[msgid];
            }
        };
        var addArrayToTranslations = function (array, indexToId, translation) {
            for (var i = 0; i < array.length; i++) {
                var text = array[i];
                var msgid = encoder.crc(text).toString();
                indexToId.push(msgid);
                translation[msgid] = text;
            }
        };
        var translation = {};
        var indexToId = [];
        function getTexts(json, translation) {
            if (json instanceof Array) {
                for (var i = 0; i < json.length; i++) {
                    getTexts(json[i], translation);
                }
            }
            else {
                var text;
                var uid;
                for (var name in json) {
                    if (json[name] instanceof Object || json[name] instanceof Array) {
                        if (name === 'texts') {
                            if (json[name] instanceof Array) {
                                addArrayToTranslations(json[name], indexToId, translation);
                            }
                            else {
                                addObjectToTranslations(json[name], indexToId, translation);
                            }
                        }
                        else {
                            getTexts(json[name], translation);
                        }
                    }
                    else if (name === 'message' && json['name']) {
                        text = json[name];
                        uid = json['name'];
                    }
                    else if (name === 'text') {
                        text = json[name];
                    }
                    else if (name === 'uid') {
                        uid = json[name];
                    }
                }
                if (text) {
                    var msgid = uid || encoder.crc(text).toString();
                    indexToId.push(msgid);
                    translation[msgid] = text;
                }
            }
        }
        getTexts(json, translation);
        var addTranslationsToObject = function (object, indexToId, currentIndex, translation) {
            for (var msgid in object) {
                object[msgid] = translation[msgid];
            }
        };
        var addTranslationsToArray = function (array, indexToId, currentIndex, translation) {
            for (var i = 0; i < array.length; i++) {
                var msgid = indexToId[currentIndex++];
                array[i] = translation[msgid];
            }
        };
        function setTexts(json, indexToId, translation) {
            if (json instanceof Array) {
                for (var i = 0; i < json.length; i++) {
                    setTexts(json[i], indexToId, translation);
                }
            }
            else {
                var text;
                for (var name in json) {
                    if (json[name] instanceof Object || json[name] instanceof Array) {
                        if (name === 'texts') {
                            if (json[name] instanceof Array) {
                                addArrayToTranslations(json[name], indexToId, translation);
                            }
                            else {
                                addObjectToTranslations(json[name], indexToId, translation);
                            }
                        }
                        else {
                            setTexts(json[name], indexToId, translation);
                        }
                    }
                    else if (name === 'message' && json['name']) {
                        var uid = indexToId.shift();
                        json['uid'] = uid;
                        if (json.params) {
                            json[name] = translatorEngine.replaceParameters(translation[uid], json.params);
                        }
                        else {
                            json[name] = translation[uid];
                        }
                    }
                    else if (name === 'text') {
                        var uid = indexToId.shift();
                        if (json.params) {
                            json[name] = translatorEngine.replaceParameters(translation[uid], json.params);
                        }
                        else {
                            json[name] = translation[uid];
                        }
                        if (!json['uid']) {
                            json['uid'] = uid;
                        }
                    }
                }
            }
        }
        var requestParam = {
            to: locale,
            translations: translation,
            nocache: nocache
        };
        translatorEngine.getTranslations(requestParam, function (result) {
            if (!result.error) {
                setTexts(json, indexToId, result.translations);
            }
            cb(json);
        });
    };
    renderer.renderhtml = function (container, $, locale, pageId, sessionId, cb) {
        var renderLanguages = function () {
            var translatorControl = $('#translator,[language-select]', container);
            if (translatorControl.length) {
                translatorControl.append('<select style="display: none"></select>');
                var select = $('select', translatorControl);
                translatorEngine.getLanguages(locale, function (result) {
                    if (result.error) {
                        translatorControl.addClass('hidden');
                    }
                    else {
                        var options = '';
                        var json = result.languages.languages;
                        for (var i = 0; i < json.length; i++) {
                            options += '<option value="' + json[i].id + '">' + json[i].text + '</option>';
                        }
                        select.html(options);
                    }
                    cb();
                });
                return;
            }
            cb();
        };
        var renderTranslations = function () {
            var requestParam = {
                to: locale,
                translations: {},
                nocache: false
            };
            var somethingToTranslate = false;
            var translatable = $('[lang],lang,[tooltip]', container);
            if (translatable.length) {
                somethingToTranslate = true;
                translatable.each(function (index, element) {
                    var $element = $(element);
                    var text = $element.is('[tooltip]') ? $element.attr('tooltip') : $element.text();
                    if (text) {
                        var id = $element.attr('uid');
                        if (!id) {
                            id = $element.attr('id');
                            if (id) {
                                id = pageId + '-' + id;
                            }
                        }
                        if (id) {
                            requestParam.translations[id] = encoder.htmlEncode(text);
                        }
                    }
                });
            }
            if (!somethingToTranslate) {
                renderLanguages();
                return;
            }
            translatorEngine.getTranslations(requestParam, function (result) {
                if (!result.error) {
                    container.attr('locale', locale);
                    if (result.translations) {
                        translatable.each(function (index, element) {
                            var $element = $(element);
                            var uid = $element.attr('uid');
                            var id = uid || pageId + '-' + $element.attr('id');
                            if (result.translations[id]) {
                                var text = encoder.htmlDecode(result.translations[id]);
                                if ($element.is('[tooltip]')) {
                                    $element.attr('tooltip', text);
                                }
                                else {
                                    $element.text(text);
                                }
                            }
                        });
                    }
                }
                renderLanguages();
            });
        }();
    };
    return renderer;
})();
module.exports = renderer;
//# sourceMappingURL=renderer.js.map
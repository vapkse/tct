declare class TranslationCache {
    private static versions;
    static get(locale: string, cb: (err: Error, cache: translator.LocaleTranslations) => void): void;
    static set(locale: string, translations: translator.Translation): void;
}
export = TranslationCache;

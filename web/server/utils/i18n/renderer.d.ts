declare class renderer {
    static renderjson(opts: any, json: any, cb: (json: any) => void): void;
    static renderhtml(container: Cheerio, $: CheerioStatic, locale: string, pageId: string, sessionId: string, cb: Function): void;
}
export = renderer;

'use strict';

import http = require('http');
import https = require('https');
import logger = require('../logger');
import translationCache = require('./cache');

class engine {
    private static credentials: translator.Credentials = {
        clientId: 'tct',                                              /* Client ID from the registered app */
        clientSecret: 'yQaNCSc4KDGs1lhvbSOLLve2fQqFl9RUJMlqf+7/prc='  /* Client Secret from the registered app */
    }

    // Cache for token during expire time
    private static accessTokenCache: translator.AccessToken;
    private static getToken = function(creds: translator.Credentials, callback: (token: translator.AccessToken) => void) {
        if (engine.accessTokenCache) {
            callback(engine.accessTokenCache);
            return;
        }

        var admOptions: https.RequestOptions = {
            hostname: 'datamarket.accesscontrol.windows.net',
            port: 443,
            path: '/v2/OAuth2-13',
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        };

        logger.log('System', 'grey', 'Translator', 'Sending request for languages token');
        var req = https.request(admOptions, function(res) {
            res.setEncoding('utf8');
            var data = '';

            res.on('data', function(d: string) {
                data += d;
            });

            res.on('error', function(err: Error) {
                callback({ error: err });
            });

            res.on('end', function() {
                var token: translator.AccessToken = JSON.parse(data);
                engine.accessTokenCache = token;

                logger.log('System', 'grey', 'Translator', 'Languages token request done');
                callback(token);

                if (token.expires_in) {
                    setTimeout(function() {
                        logger.log('System', 'grey', 'Translator', 'Languages token expired');
                        engine.accessTokenCache = null;
                    }, parseInt(token.expires_in) * 1000)
                }
            });
        });

        req.end('grant_type=client_credentials&client_id=' +
            encodeURIComponent(creds.clientId) + '&client_secret=' +
            encodeURIComponent(creds.clientSecret) + '&scope=http://api.microsofttranslator.com');
    }

    private static languagesCache: translator.Languages = {};
    public static getLanguages(locale: string, callback: (languages: translator.RequestResult) => void) {
        if (engine.languagesCache[locale]) {
            callback({
                locale: locale,
                languages: engine.languagesCache[locale]
            });
            return;
        }

        engine.getToken(engine.credentials, function(token: translator.AccessToken) {
            if (token.error) {
                var msg = (token.error_description || (token.error.message && token.error.message.toString()) || token.error);
                logger.log('System', 'red', 'Translator', msg);
                callback({
                    locale: locale,
                    error: new Error(msg)
                });
                return;
            }

            var options = {
                hostname: 'api.microsofttranslator.com',
                port: 80,
                path: '/v2/Ajax.svc/GetLanguagesForSpeak?appId=' + encodeURIComponent('Bearer ' + token.access_token),
                method: 'GET'
            };

            logger.log('System', 'grey', 'Translator', 'Sending request for spoken languages');
            var req = http.request(options, function(res: http.ServerResponse) {
                res.setEncoding('utf8');

                var data = '';
                res.on('data', function(d: string) {
                    data += d;
                });

                res.on('error', function(err: Error) {
                    logger.logError('System', 'Translator', err);
                    callback({
                        locale: locale,
                        error: err
                    });
                    return;
                });

                res.on('end', function() {
                    // Strip out JSONP header and quotes
                    var codeResult = data.substring(1);
                    var languageCodes = JSON.parse(codeResult);

                    if (languageCodes.error) {
                        logger.logError('System', 'Translator', languageCodes.error);
                        callback({
                            locale: locale,
                            error: new Error(languageCodes.error)
                        });
                        return;
                    }

                    options.path = '/v2/Ajax.svc/GetLanguageNames?appId=' + encodeURIComponent('Bearer ' + token.access_token) +
                        '&languageCodes=' + encodeURIComponent(codeResult) + '&locale=' + encodeURIComponent(locale);

                    logger.log('System', 'grey', 'Translator', 'Sending request for spoken languages names');
                    var req = http.request(options, function(res: http.ServerResponse) {
                        res.setEncoding('utf8');

                        var data2 = '';
                        res.on('data', function(d: string) {
                            data2 += d;
                        });

                        res.on('error', function(err: Error) {
                            logger.logError('System', 'Translator', err);
                            callback({
                                locale: locale,
                                error: err
                            });
                            return;
                        });

                        res.on('end', function() {
                            // Strip out JSONP header and quotes
                            var result = data2.substring(3, data2.length - 2).split('","');
                            if (result.length <= 1) {
                                var msg3 = (result.length === 1 ? result[0] : 'unknown');
                                logger.log('System', 'red', 'Translator', msg3);
                                callback({
                                    locale: locale,
                                    error: new Error(msg3)
                                });
                                return;
                            }

                            var lngs: Array<translator.Language> = [];
                            for (var i = 0; i < result.length; i++) {
                                var id = languageCodes[i];
                                lngs.push({
                                    id: id,
                                    text: result[i]
                                });
                            }

                            var localLanguages: translator.LocaleLanguages = {
                                languages: lngs,
                                locale: locale
                            }

                            engine.languagesCache[locale] = localLanguages;
                            callback({
                                locale: locale,
                                languages: localLanguages
                            });
                            return;
                        });
                    });

                    req.end();
                });
            });

            req.end();
        });
    }

    // param can be { 0: name }     
    public static replaceParameters = function(text: string, translationsParams: { [id: string]: any }) {
        if (translationsParams) {
            var getParam = function(paramIndex: number) {
                var param = translationsParams[paramIndex];
                if (param !== '\n') {
                    return ' ' + param + ' ';
                } else {
                    return param || '';
                }
            }

            var re = /([ ]?)\\([0-9]*)([ ]?)/gm;
            var matches = re.exec(text);
            var output: Array<string> = [];
            var start = 0;
            while (matches) {
                var end = matches.index;
                if (end > start) {
                    output.push(text.substring(start, end))
                }

                // Remove one space before and step \\
                start = end + matches[1].length + 1;
                if (matches[2]) {
                    var iparam = parseInt(matches[2]);
                    if (!isNaN(iparam)) {
                        // Replace param
                        output.push(getParam(iparam));
                        // Step replacement value
                        start += matches[2].length;
                    }
                }

                // Remove one space after
                start += matches[3].length;

                var matches = re.exec(text);
            }

            if (output.length) {
                output.push(text.substring(start))
                return output.join('');
            } else {                
                return text;
            }
        }
    }

    public static getTranslations(requestParam: translator.TranslationsParams, callback: (trans: translator.RequestResult) => void) {
        if (!requestParam.from) {
            requestParam.from = 'en';
        } else if (requestParam.from !== 'en') {
            requestParam.nocache = true;
        }

        var isNativeLocale = requestParam.from === 'en' && requestParam.to === 'en';

        var result: translator.RequestResult = {
            locale: requestParam.to,
            translations: {}
        };

        var translate = function(originales: translator.LocaleTranslations, cache: translator.LocaleTranslations) {
            var toTranslate: translator.Translation = {};
            var originalTranslation = originales.translations;
            var texts: Array<string>;
            if (originales.locale === 'en') {
                var originalToUpdate = {} as translator.Translation;
                for (var id in requestParam.translations) {
                    var original = originalTranslation[id];
                    if (!original) {
                        original = requestParam.translations[id];
                        if (original) {
                            originalToUpdate[id] = original;
                        } else {
                            logger.log('System', 'red', 'Translator', 'No original translation for ' + id);
                            continue;
                        }
                    } else if (requestParam.translations[id]) {
                        // Check duplication of id with a different text
                        if (original !== requestParam.translations[id]) {
                            logger.log('System', 'red', 'Translator', 'Different original translation for ' + id + ': ' + original + ' - ' + requestParam.translations[id]);
                            originalToUpdate[id] = requestParam.translations[id];
                        }
                    }

                    if (isNativeLocale) {
                        result.translations[id] = original;
                    } else {
                        // Check for cached translation
                        if (cache && cache.translations[id]) {
                            result.translations[id] = cache.translations[id];
                        } else {
                            // No cache or not cacheable
                            toTranslate[id] = original;
                        }
                    }
                }

                // Store original in DB
                translationCache.set('en', originalToUpdate);
            } else {
                toTranslate = requestParam.translations;
            }

            texts = Object.keys(toTranslate).map(key => toTranslate[key]);
            if (texts.length === 0) {
                // Nothing to translate 
                callback(result);
                return;
            }

            engine.getToken(engine.credentials, function(token: translator.AccessToken) {
                if (token.error) {
                    var msg = (token.error_description || (token.error.message && token.error.message.toString()) || token.error);
                    logger.log('System', 'red', 'Translator', msg);
                    callback({
                        locale: requestParam.to,
                        error: new Error(msg)
                    });
                    return;
                }

                var detectOptions = {
                    hostname: 'api.microsofttranslator.com',
                    port: 80,
                    path: '/v2/Ajax.svc/TranslateArray?appId=' + encodeURIComponent('Bearer ' + token.access_token) +
                    '&texts=' + encodeURIComponent(JSON.stringify(texts)) +
                    '&from=' + encodeURIComponent(requestParam.from) +
                    '&to=' + encodeURIComponent(requestParam.to),
                    method: 'GET'
                };

                logger.log('System', 'yellow', 'Translator', 'Sending request for translation. \\0 words to translate in \\1', String(texts.length), requestParam.to);

                var req = http.request(detectOptions, function(res: http.ServerResponse) {
                    res.setEncoding('utf8');

                    var data = '';
                    res.on('data', function(d: string) {
                        data += d;
                    });

                    res.on('error', function(err: Error) {
                        logger.logError('System', 'Translator', err);
                        callback({
                            locale: requestParam.to,
                            error: err
                        });
                        return;
                    });

                    res.on('end', function() {
                        // Strip out JSONP header and quotes
                        var r = JSON.parse(data.substring(1));
                        if (r.error || r.length !== texts.length) {
                            var error = r.error ? r.error : 'wrong received size';
                            logger.log('System', 'red', 'Translator', error);
                            callback({
                                locale: requestParam.to,
                                error: new Error(error)
                            });
                            return;
                        }

                        var translationsToUpdate = {} as translator.Translation;
                        var ids = Object.keys(toTranslate);
                        for (var i = 0; i < texts.length; i++) {
                            var text = r[i].TranslatedText;
                            var id = ids[i];
                            result.translations[id] = translationsToUpdate[id] = text;
                        }

                        if (!isNativeLocale && requestParam.nocache !== true) {
                            translationCache.set(requestParam.to, translationsToUpdate);
                        }

                        logger.log('System', 'green', 'Translator', 'Translations done');
                        callback(result);
                    });
                });

                req.end();
            });
        }

        var getLocaleTranslations = function(done: (err?: Error, cache?: translator.LocaleTranslations) => void) {
            if (isNativeLocale || requestParam.nocache === true) {
                done();
            } else {
                translationCache.get(requestParam.to, function(err, cache) {
                    done(err, cache);
                })
            }
        }

        // getOriginalTranslations
        if (requestParam.from === 'en') {
            translationCache.get('en', function(err, originales) {
                getLocaleTranslations(function(err, cache) {
                    translate(originales, cache);
                });
            })
        } else {
            translate({
                locale: requestParam.from,
                translations: requestParam.translations
            }, null);
        }
    }
}

export = engine;

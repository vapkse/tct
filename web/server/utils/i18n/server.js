'use strict';
var translatorEngine = require('./translator');
var express = require('express');
var sessionProvider = require('../session-provider');
var dbengine = require('../../database/engine');
var app = express();
app.post('/', function (req, res) {
    var sessionId = req.session.id;
    if (req.query.r === 'translate') {
        var requestParams = req.body;
        if (requestParams.to === 'current') {
            requestParams.to = sessionProvider.States.state(sessionId).locale || 'en';
        }
        translatorEngine.getTranslations(requestParams, function (transResult) {
            if (transResult.error) {
                res.status(500).send(transResult.error.message);
                return;
            }
            sessionProvider.States.state(sessionId).locale = requestParams.to;
            res.send(transResult);
        });
    }
    else if (req.query.r === 'change') {
        var changeParams = req.body;
        translatorEngine.getLanguages(changeParams.to, function (languagesResult) {
            if (languagesResult.error) {
                res.status(500).send(languagesResult.error.message);
                return;
            }
            translatorEngine.getTranslations(changeParams, function (transResult) {
                if (transResult.error) {
                    res.status(500).send(transResult.error.message);
                    return;
                }
                sessionProvider.States.state(sessionId).locale = changeParams.to;
                transResult.languages = languagesResult.languages;
                res.send(transResult);
            });
        });
    }
    else if (req.query.r === 'propose' && req.user) {
        var options = {
            query: req.query,
            sessionID: req.session.id,
            user: req.user,
        };
        var proposeParams = req.body;
        proposeParams.userName = (options.user.userName || options.user.displayName || options.user.email);
        dbengine.translations.addProposal(proposeParams, function (error) {
            if (error) {
                res.status(500).send(error.message);
            }
            else {
                res.status(200).end();
            }
        });
    }
    else {
        res.status(500).send({
            name: 'wrongparams',
            message: 'Wrong Parameters.'
        });
    }
});
module.exports = app;
//# sourceMappingURL=server.js.map
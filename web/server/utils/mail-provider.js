'use strict';
var nodemailer = require('nodemailer');
var translatorEngine = require('./i18n/translator');
var mail;
(function (mail) {
    var Provider = (function () {
        function Provider() {
        }
        Provider.send = function (mailInfos, cb) {
            var translations = {};
            translations[mailInfos.object.uid] = mailInfos.object.text;
            translations[mailInfos.body.uid] = mailInfos.body.text;
            var translationRequest = {
                to: mailInfos.locale,
                nocache: false,
                translations: translations,
            };
            translatorEngine.getTranslations(translationRequest, function (translated) {
                if (translated.error) {
                    cb(translated.error);
                    return;
                }
                var body = translatorEngine.replaceParameters(translated.translations[mailInfos.body.uid], mailInfos.bodyParams);
                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: mailInfos.user,
                        pass: mailInfos.password
                    }
                }, {
                    from: mailInfos.from || mailInfos.user,
                });
                transporter.sendMail({
                    to: mailInfos.dest,
                    subject: translated.translations[mailInfos.object.uid],
                    text: body,
                }, function (error) {
                    cb(error);
                });
            });
        };
        return Provider;
    })();
    mail.Provider = Provider;
})(mail || (mail = {}));
module.exports = mail;
//# sourceMappingURL=mail-provider.js.map
'use strict';

module session {    
    export class State {
        private _id: string;
        
        // Session properties
        // Spoken language
        public locale: string;
        // User decide to ignore the old browser advise
        public ignoreBrowserVersion: boolean;
        
        constructor (id: string) {
            var self = this as State;
            self._id = id;    
        }
        
        public get id() {
            return this._id;
        }
    }
    
    interface states {
        [id: string]: State
    }
    
    export class States {
        private static s: states = {};
    
        public static state(id: string) {
            var state = States.s[id];
            if (!state){
                state = States.s[id] = new State(id);
            }   
            return state;
        }
    }
}

export = session;
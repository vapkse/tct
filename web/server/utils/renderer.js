'use strict';
var cheerio = require('cheerio');
var fs = require('fs');
var ps = require('path');
var translationRenderer = require('./i18n/renderer');
var validationRenderer = require('./validation/renderer');
var cacheProvider = require('./cache');
var sessionProvider = require('./session-provider');
var logger = require('../utils/logger');
var cache = cacheProvider.Cache;
var renderer = (function () {
    function renderer() {
    }
    renderer.cpOptsInData = function (data, opts) {
        renderer.OPTS.forEach(function (p) {
            if (typeof data[p] !== 'undefined') {
                opts[p] = data[p];
            }
        });
    };
    renderer.render = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i - 0] = arguments[_i];
        }
        var args = Array.prototype.slice.call(arguments);
        var path = args.shift();
        var cb = args.pop();
        var data = args.shift() || {};
        var opts = args.pop() || {};
        try {
            if (arguments.length === 3) {
                renderer.cpOptsInData(data, opts);
            }
            var sessionId = opts.sessionID;
            var locale = data.query && data.query.lng;
            if (!locale) {
                locale = sessionProvider.States.state(sessionId).locale || 'en';
            }
            sessionProvider.States.state(sessionId).locale = locale;
            var wait = data.query && data.query.wait;
            var _$;
            var content;
            var $ = function () {
                if (!_$) {
                    _$ = cheerio.load(content);
                }
                return _$;
            };
            var renderDone = function () {
                if (content) {
                    var r = _$ ? _$.html() : content;
                    content = null;
                    if (!wait) {
                        return cb(null, r);
                    }
                    else {
                        setTimeout(function () {
                            return cb(null, r);
                        }, wait);
                    }
                }
            };
            var removeNonAdminHTML = function () {
                if (content && (!opts.user || opts.user.role !== 'admin')) {
                    $()('[admin-only]').remove();
                }
                renderDone();
            };
            var renderUser = function () {
                if (opts.user) {
                    $()('head').append('<script id="user" type="application/json">' + JSON.stringify(opts.user) + '</script>');
                }
                removeNonAdminHTML();
            };
            var renderValidation = function () {
                if (opts.message && opts.message.length) {
                    validationRenderer.render($(), opts, locale, function () {
                        renderUser();
                    });
                }
                else {
                    renderUser();
                }
            };
            var cacheKey = path;
            var type = ps.extname(path);
            if (type === '.html' || type === '.json') {
                cacheKey += '_' + locale;
            }
            var stats = fs.statSync(path);
            var modifiedTime = stats && stats.mtime && stats.mtime.getTime();
            content = cache.get(cacheKey, modifiedTime);
            if (content) {
                logger.log(opts, 'grey', 'Render', 'Restoring file from cache \\0', cacheKey);
                if (type === '.html') {
                    renderValidation();
                }
                else {
                    renderDone();
                }
            }
            else {
                logger.log(opts, 'white', 'Render', 'Pre processing for file \\0', cacheKey);
                fs.readFile(path, function (err, data) {
                    if (err) {
                        logger.logError(opts, 'Render', err);
                        return cb(err);
                    }
                    content = data.toString();
                    if (type === '.html') {
                        var container = $()('[translate]');
                        if (container.length > 0) {
                            var pageId = container.attr('translate');
                            translationRenderer.renderhtml(container, $(), locale, pageId, sessionId, function () {
                                cache.set($().html(), cacheKey, modifiedTime);
                                renderValidation();
                            });
                        }
                        else {
                            cache.set(content, cacheKey, modifiedTime);
                            renderValidation();
                        }
                    }
                    else if (type === '.json') {
                        var json = JSON.parse(content);
                        translationRenderer.renderjson(opts, json, function (j) {
                            content = j;
                            cache.set(content, cacheKey, modifiedTime);
                            renderDone();
                        });
                    }
                    else {
                        cache.set(content, cacheKey, modifiedTime);
                        renderDone();
                    }
                    setTimeout(function () {
                        removeNonAdminHTML();
                    }, renderer.renderTimeout);
                });
            }
        }
        catch (err) {
            return cb(err);
        }
    };
    renderer.OPTS = ['sessionID', 'cache', 'filename', 'delimiter', 'scope', 'context', 'debug', 'compileDebug',
        'client', '_with', 'rmWhitespace', 'locale', 'message', 'user'];
    renderer.renderTimeout = 20000;
    return renderer;
})();
module.exports = renderer;
//# sourceMappingURL=renderer.js.map
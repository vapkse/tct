'use strict';

import nodemailer = require('nodemailer');
import translatorEngine = require('./i18n/translator');

module mail {
    export interface MailInfos {
        locale: string,
        user: string,
        password: string,
        from?: string,
        dest: string,
        object: {
            uid: string,
            text: string
        },
        body: {
            uid: string,
            text: string
        },
        bodyParams?: { [index: string]: string }
    }
    
    export class Provider {
        public static send(mailInfos: MailInfos, cb: (error: Error) => void) {
            var translations: translator.Translation = {};
            translations[mailInfos.object.uid] = mailInfos.object.text;
            translations[mailInfos.body.uid] = mailInfos.body.text;

            var translationRequest: translator.TranslationsParams = {
                to: mailInfos.locale,
                nocache: false,
                translations: translations,
            }

            translatorEngine.getTranslations(translationRequest, function(translated) {
                if (translated.error) {
                    cb(translated.error);
                    return;
                }

                var body = translatorEngine.replaceParameters(translated.translations[mailInfos.body.uid], mailInfos.bodyParams);    
                
                // Send confirmation email                
                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: mailInfos.user,
                        pass: mailInfos.password
                    }
                }, {
                        // default values for sendMail method
                        from: mailInfos.from || mailInfos.user,
                    });
                transporter.sendMail({
                    to: mailInfos.dest,
                    subject: translated.translations[mailInfos.object.uid],
                    text: body,
                }, function(error: Error) {
                    cb(error);
                });
            })
        }
    }
}

export = mail;
'use strict';

import cheerio = require('cheerio');
import fs = require('fs');
import ps = require('path');
import translationRenderer = require('./i18n/renderer');
import translatorEngine = require('./i18n/translator');
import validationRenderer = require('./validation/renderer');
import cacheProvider = require('./cache');
import sessionProvider = require('./session-provider');
import logger = require('../utils/logger');

var cache = cacheProvider.Cache;

class renderer {
    private static OPTS = ['sessionID', 'cache', 'filename', 'delimiter', 'scope', 'context', 'debug', 'compileDebug',
        'client', '_with', 'rmWhitespace', 'locale', 'message', 'user']

    private static cpOptsInData(data: any, opts: any) {
        renderer.OPTS.forEach(function(p) {
            if (typeof data[p] !== 'undefined') {
                opts[p] = data[p];
            }
        });
    }
    
    private static renderTimeout = 20000;

    public static render(...params: any[]) {
        var args = Array.prototype.slice.call(arguments)
        var path = args.shift()
        var cb = args.pop()
        var data = args.shift() || {}
        var opts = args.pop() || {}

        try {
            if (arguments.length === 3) {
                renderer.cpOptsInData(data, opts);
            }

            // Get locale
            var sessionId = opts.sessionID;
            var locale = data.query && data.query.lng;
            if (!locale) {
                locale = sessionProvider.States.state(sessionId).locale  || 'en';                               
            }	
            sessionProvider.States.state(sessionId).locale = locale;                            
                
            // Check wait for test
            var wait = data.query && data.query.wait; 
            
            // Cheerio can be udefull to process te html
            var _$: CheerioStatic;
            var content: string;
            var $ = function() {
                if (!_$) {
                    _$ = cheerio.load(content);
                }
                return _$;
            }

            var renderDone = function() {
                if (content) {
                    var r = _$ ? _$.html() : content;
                    content = null; // For the timeout
                    if (!wait){
                        return cb(null, r);
                    } else {
                        setTimeout(function() { 
                            return cb(null, r);
                        }, wait);                        
                    }
                }
            }
            
            var removeNonAdminHTML = function() {
                if (content && (!opts.user || opts.user.role !== 'admin')){
                    $()('[admin-only]').remove();
                }
                renderDone();
            }
            
            var renderUser = function(){
                // Check if a user is connected
                if (opts.user){
                    // Add user profile
                    $()('head').append('<script id="user" type="application/json">' + JSON.stringify(opts.user) + '</script>');
                }                
                removeNonAdminHTML();   
            }
            
            var renderValidation = function() {         
                // Check for validation rendering
                if (opts.message && opts.message.length) {
                    // Need validations, call the validation renderer
                    validationRenderer.render($(), opts, locale, function() {
                        renderUser();
                    });
                } else {
                    renderUser();
                }
            }

            // Check in cache
            var cacheKey = path;
            var type = ps.extname(path);
            if (type === '.html' || type === '.json') {
                cacheKey += '_' + locale
            }
            var stats = fs.statSync(path);
            var modifiedTime = stats && stats.mtime && stats.mtime.getTime();
            content = cache.get(cacheKey, modifiedTime);
            if (content) {
                logger.log(opts, 'grey', 'Render', 'Restoring file from cache \\0', cacheKey);
                if (type === '.html') {
                    renderValidation();
                } else {
                    renderDone();
                }
            } else {
                logger.log(opts, 'white', 'Render', 'Pre processing for file \\0', cacheKey);
                fs.readFile(path, function(err, data) {
                    if (err) {
                        logger.logError(opts, 'Render', err);
                        return cb(err);
                    }

                    content = data.toString();
                    if (type === '.html') {                        
                        // Translate content to put a clean content translated in cache                        
                        var container = $()('[translate]');
                        if (container.length > 0) {
                            // Need translations, call the translation renderer
                            var pageId = container.attr('translate');
                            translationRenderer.renderhtml(container, $(), locale, pageId, sessionId, function() {
                                cache.set($().html(), cacheKey, modifiedTime);
                                renderValidation();
                            });
                        } else {
                            cache.set(content, cacheKey, modifiedTime);
                            renderValidation();
                        }
                    } else if (type === '.json') {
                        var json = JSON.parse(content);
                        translationRenderer.renderjson(opts, json, function(j) {
                            content = j;
                            cache.set(content, cacheKey, modifiedTime);
                            renderDone();
                        })
                    } else {
                        cache.set(content, cacheKey, modifiedTime);
                        renderDone();
                    }

                    setTimeout(function() {
                        removeNonAdminHTML();
                    }, renderer.renderTimeout);
                });
            }
        }
        catch (err) {
            return cb(err);
        }
    }
}

export = renderer;
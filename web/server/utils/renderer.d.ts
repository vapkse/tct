declare class renderer {
    private static OPTS;
    private static cpOptsInData(data, opts);
    private static renderTimeout;
    static render(...params: any[]): any;
}
export = renderer;

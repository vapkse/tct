'use strict';
var session;
(function (session) {
    var State = (function () {
        function State(id) {
            var self = this;
            self._id = id;
        }
        Object.defineProperty(State.prototype, "id", {
            get: function () {
                return this._id;
            },
            enumerable: true,
            configurable: true
        });
        return State;
    })();
    session.State = State;
    var States = (function () {
        function States() {
        }
        States.state = function (id) {
            var state = States.s[id];
            if (!state) {
                state = States.s[id] = new State(id);
            }
            return state;
        };
        States.s = {};
        return States;
    })();
    session.States = States;
})(session || (session = {}));
module.exports = session;
//# sourceMappingURL=session-provider.js.map
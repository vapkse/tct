"use strict";
var grunt = require('grunt');
var Logger = (function () {
    function Logger() {
    }
    Logger.start = function (env) {
        var colors = ['white', 'black', 'grey', 'blue', 'cyan', 'green', 'magenta', 'red', 'yellow', 'rainbow'];
        colors.forEach(function (color) {
            grunt.log.writeln(('Starting express in ' + color + ' mode => ' + env)[color]);
        });
        grunt.log.writeln('');
    };
    Logger.log = function (opts, color, namespace, text) {
        var params = [];
        for (var _i = 4; _i < arguments.length; _i++) {
            params[_i - 4] = arguments[_i];
        }
        var username;
        if (typeof opts === 'string') {
            username = opts;
        }
        else if (opts) {
            var user = opts.user;
            username = user && (user.userName || user.email);
            if (!username) {
                username = opts.session ? opts.session.id : opts.sessionID;
            }
        }
        else {
            username = 'System';
        }
        if (username && username.length > 29) {
            username = username.substr(0, 27) + '..';
        }
        var req = opts && opts.res && opts.res.req;
        var ip = req ? req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress : '';
        var paddy = function (text, n, c) {
            if (!text) {
                text = '';
            }
            if (text.length >= n) {
                return text;
            }
            return text + new Array(n - text.length).join(c);
        };
        var now = new Date();
        var day = String(now.getDate());
        var month = String(now.getMonth() + 1);
        if (day.length === 1) {
            day = '0' + day;
        }
        if (month.length === 1) {
            month = '0' + month;
        }
        var textToLog = [];
        textToLog.push(day + '-' + month + ' ' + now.toLocaleTimeString());
        textToLog.push(paddy(username, 30, ' '));
        textToLog.push(paddy(ip.substr(7), 16, ' '));
        textToLog.push(paddy(namespace, 16, ' '));
        textToLog.push(Logger.replaceParameters(text, params));
        var s = textToLog.join('  ');
        grunt.log.writeln(s[color]);
    };
    Logger.logError = function (opts, namespace, error) {
        if (!error) {
            return;
        }
        if (error.type === 'info') {
            Logger.log(opts, 'blue', namespace, 'Info: ' + error.message);
        }
        else if (error.type === 'warning') {
            Logger.log(opts, 'yellow', namespace, 'Warning: ' + error.message);
        }
        else {
            Logger.log(opts, 'red', namespace, 'Error: ' + error.message);
        }
    };
    Logger.logErrors = function (opts, namespace, verrors) {
        for (var v = 0; v < verrors.length; v++) {
            Logger.logError(opts, namespace, verrors[v]);
        }
    };
    Logger.replaceParameters = function (text, params) {
        if (params && params.length) {
            var re = /\\([0-9]*)/gm;
            var matches = re.exec(text);
            var output = [];
            var start = 0;
            while (matches) {
                var end = matches.index;
                if (end > start) {
                    output.push(text.substring(start, end));
                }
                start = end + 1;
                if (matches[1]) {
                    var iparam = parseInt(matches[1]);
                    if (!isNaN(iparam)) {
                        output.push(params[iparam]);
                        start += matches[1].length;
                    }
                }
                matches = re.exec(text);
            }
            if (output.length) {
                output.push(text.substring(start));
                return output.join('');
            }
        }
        return text;
    };
    return Logger;
})();
module.exports = Logger;
//# sourceMappingURL=logger.js.map
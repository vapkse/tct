'use strict';
var cacheProvider;
(function (cacheProvider) {
    var Cache = (function () {
        function Cache() {
        }
        Cache.get = function (cacheKey, time) {
            var cache = Cache.cache[cacheKey];
            if (cache && (cache.modifiedTime === time)) {
                return cache.content;
            }
            return null;
        };
        Cache.set = function (content, cacheKey, time) {
            Cache.cache[cacheKey] = {
                content: content,
                modifiedTime: time
            };
        };
        Cache.clear = function (locale) {
            if (!locale) {
                Cache.cache = {};
                return;
            }
            var re = new RegExp('_' + locale + '$', 'i');
            Object.keys(Cache.cache).forEach(function (key) {
                if (re.test(key)) {
                    Cache.cache[key] = undefined;
                }
            });
        };
        Cache.cache = {};
        return Cache;
    })();
    cacheProvider.Cache = Cache;
})(cacheProvider || (cacheProvider = {}));
module.exports = cacheProvider;
//# sourceMappingURL=cache.js.map
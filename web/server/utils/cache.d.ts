declare module cacheProvider {
    interface cache {
        [key: string]: any;
    }
    class Cache {
        private static cache;
        static get(cacheKey: string, time?: number): any;
        static set(content: any, cacheKey: string, time?: number): void;
        static clear(locale?: string): void;
    }
}
export = cacheProvider;

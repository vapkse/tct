'use strict';

module cacheProvider {
    export interface cache {
        [key: string]: any
    }

    export class Cache {
        private static cache: cache = {};

        public static get(cacheKey: string, time?: number) {
            var cache = Cache.cache[cacheKey];
            if (cache && (cache.modifiedTime === time)) {
                return cache.content;
            }

            return null;
        }

        public static set(content: any, cacheKey: string, time?: number) {
            Cache.cache[cacheKey] = {
                content: content,
                modifiedTime: time
            }
        }

        public static clear(locale?: string) {          
            if (!locale) {
                Cache.cache = {};
                return;
            }  
            
            var re = new RegExp('_' + locale + '$', 'i');
            Object.keys(Cache.cache).forEach(function(key) {
                if (re.test(key)) {
                    Cache.cache[key] = undefined;
                }
            })
        }
    }
}

export = cacheProvider;
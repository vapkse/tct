declare class Logger {
    static start(env: string): void;
    private static replaceParameters;
    static log(opts: any, color: string, namespace: string, text: string, ...params: string[]): void;
    static logError(opts: any, namespace: string, error: validation.Message): void;
    static logErrors(opts: any, namespace: string, verrors: Array<validation.Message>): void;
}
export = Logger;

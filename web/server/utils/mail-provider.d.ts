declare module mail {
    interface MailInfos {
        locale: string;
        user: string;
        password: string;
        from?: string;
        dest: string;
        object: {
            uid: string;
            text: string;
        };
        body: {
            uid: string;
            text: string;
        };
        bodyParams?: {
            [index: string]: string;
        };
    }
    class Provider {
        static send(mailInfos: MailInfos, cb: (error: Error) => void): void;
    }
}
export = mail;

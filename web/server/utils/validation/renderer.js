var translationRenderer = require('../i18n/renderer');
var utils = require('../../common/ts/encoder');
var encoder = utils.Encoder;
var renderer = (function () {
    function renderer() {
    }
    renderer.render = function ($, options, locale, cb) {
        var message = options.message;
        if (!message || (message instanceof Array && message.length === 0)) {
            cb();
            return;
        }
        var normalizeEntry = function (message, type) {
            var all = [];
            if (typeof message === 'string') {
                var uid = encoder.crc(message).toString();
                all.push({
                    message: message,
                    name: uid,
                    type: type
                });
            }
            else if (message instanceof Array) {
                for (var i = 0; i < message.length; i++) {
                    var msg = message[i];
                    if (typeof msg === 'string') {
                        var uid = encoder.crc(msg).toString();
                        all.push({
                            message: msg,
                            name: uid,
                            type: type
                        });
                    }
                    else {
                        if (!msg.name) {
                            msg.name = encoder.crc(msg.message).toString();
                        }
                        if (!msg.type) {
                            msg.type = type;
                        }
                        all.push(msg);
                    }
                }
            }
            else {
                var vmsg = message;
                if (!vmsg.name) {
                    vmsg.name = encoder.crc(vmsg.message).toString();
                }
                if (!vmsg.type) {
                    vmsg.type = type;
                }
                all.push(vmsg);
            }
            return all;
        };
        var all = normalizeEntry(message, 'error');
        translationRenderer.renderjson(options, all, function (translated) {
            $('head').append('<script id="errors" type="application/json">' + JSON.stringify(translated) + '</script>');
            cb();
        });
    };
    return renderer;
})();
module.exports = renderer;
//# sourceMappingURL=renderer.js.map
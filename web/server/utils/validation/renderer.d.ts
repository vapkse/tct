declare class renderer {
    static render($: CheerioStatic, options: {
        message: Array<string> | Array<validation.Message> | string | validation.Message;
    }, locale: string, cb: () => void): void;
}
export = renderer;

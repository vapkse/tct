import translatorEngine = require('../i18n/translator');
import translationRenderer = require('../i18n/renderer');
import utils = require('../../common/ts/encoder');

var encoder = utils.Encoder;

class renderer {
    public static render($: CheerioStatic,
        options: {message: Array<string> | Array<validation.Message> | string | validation.Message},
        locale: string, cb: () => void) {

        var message = options.message;
        if (!message || (message instanceof Array && message.length === 0)) {
            // nothing to do
            cb();
            return;
        }

        var normalizeEntry = function(message: Array<string> | Array<validation.Message> | string | validation.Message, type: string) {
            var all: Array<validation.Message> = [];
            if (typeof message === 'string') {
                var uid = encoder.crc(message).toString();
                all.push({
                    message: message,
                    name: uid,
                    type: type
                });
            } else if (message instanceof Array) {
                for (var i = 0; i < message.length; i++) {
                    var msg = message[i];
                    if (typeof msg === 'string') {
                        var uid = encoder.crc(msg).toString();
                        all.push({
                            message: msg,
                            name: uid,
                            type: type
                        });
                    } else {
                        if (!msg.name) {
                            msg.name = encoder.crc(msg.message).toString();
                        }
                        if (!msg.type) {
                            msg.type = type;
                        }
                        all.push(msg);
                    }
                }
            } else {
                var vmsg = <validation.Message>message;
                if (!vmsg.name) {
                    vmsg.name = encoder.crc(vmsg.message).toString();
                }
                if (!vmsg.type) {
                    vmsg.type = type;
                }
                all.push(vmsg);
            }

            return all;
        }

        var all = normalizeEntry(message, 'error');
        translationRenderer.renderjson(options, all, function(translated: any) {
            $('head').append('<script id="errors" type="application/json">' + JSON.stringify(translated) + '</script>');
            cb();
        });
    }
}

export = renderer;
declare module session {
    class State {
        private _id;
        locale: string;
        ignoreBrowserVersion: boolean;
        constructor(id: string);
        id: string;
    }
    class States {
        private static s;
        static state(id: string): State;
    }
}
export = session;

'use strict';

import express = require('express');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');
import config = require('../../config');
import fs = require('fs');
import path = require('path');
import translationrenderer = require('../utils/i18n/renderer');
import dbengine = require('../database/engine');
import logger = require('../utils/logger');
import uuid = require('node-uuid');

var app = express();

app.set('views', config.path.views);

app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.get('/', function(req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        user: req.user,
        errors: req.flash('error'),
        warnings: req.flash('warning'),
        infos: req.flash('info')
    }

    logger.log(req, 'grey', 'Model', 'Navigate to model');
    res.render('model.html', options);
});

app.post('/', function(req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
    }

    var returnError = function(error: Error) {
        translationrenderer.renderjson(options, error, function(json) {
            logger.logError(req, 'Model', json);
            res.status(500).send(JSON.stringify(json));
        })
    }

    if (req.query.id) {
        logger.log(req, 'cyan', 'Model', 'Modeling tube ' + req.url);
        dbengine.tubes.getTube({ _id: req.query.id }, req.query.userid || req.user._id, false, function(err, result) {
            if (err) {
                returnError(err);
            } else if (!result || !result.tubeData) {
                returnError({
                    name: 'tubenotfound',
                    message: 'Tube not found in the database.'
                });
            } else {
                translationrenderer.renderjson(options, result, function(tube) {
                    res.json(tube);
                })
            }
        })

    } else {
        // Wrong parameters
        returnError({
            name: 'wrongparams',
            message: 'Wrong Parameters.'
        });
    }
})

export = app;

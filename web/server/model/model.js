'use strict';
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('../../config');
var translationrenderer = require('../utils/i18n/renderer');
var dbengine = require('../database/engine');
var logger = require('../utils/logger');
var app = express();
app.set('views', config.path.views);
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.get('/', function (req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        user: req.user,
        errors: req.flash('error'),
        warnings: req.flash('warning'),
        infos: req.flash('info')
    };
    logger.log(req, 'grey', 'Model', 'Navigate to model');
    res.render('model.html', options);
});
app.post('/', function (req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
    };
    var returnError = function (error) {
        translationrenderer.renderjson(options, error, function (json) {
            logger.logError(req, 'Model', json);
            res.status(500).send(JSON.stringify(json));
        });
    };
    if (req.query.id) {
        logger.log(req, 'cyan', 'Model', 'Modeling tube ' + req.url);
        dbengine.tubes.getTube({ _id: req.query.id }, req.query.userid || req.user._id, false, function (err, result) {
            if (err) {
                returnError(err);
            }
            else if (!result || !result.tubeData) {
                returnError({
                    name: 'tubenotfound',
                    message: 'Tube not found in the database.'
                });
            }
            else {
                translationrenderer.renderjson(options, result, function (tube) {
                    res.json(tube);
                });
            }
        });
    }
    else {
        returnError({
            name: 'wrongparams',
            message: 'Wrong Parameters.'
        });
    }
});
module.exports = app;
//# sourceMappingURL=model.js.map
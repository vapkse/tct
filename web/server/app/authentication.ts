'use strict';

import config = require('../../config');
import Passport = require('passport');
import Express = require('express');
import dbengine = require('../database/engine');

import sessionProvider = require('../utils/session-provider');
import mail = require('../utils/mail-provider');
import bcrypt = require('bcryptjs');
import User = require('../../common/ts/user'); // Namespace only
import logger = require('../utils/logger');
import mailProviderValidator = require('./mail-blacklist-validator');

// load all the things we need
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var GitHubStrategy = require('passport-github').Strategy;

module.exports = function(passport: Passport.Passport, env: string) {
    var authConfig = (<any>config).auth[env];

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    // used to deserialize the user
    passport.deserializeUser(function(user, done) {
        done(null, user);
    });

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'user_login',
        passwordField: 'user_password',
        passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    }, function(req: Express.Request, email: string, password: string, done: Function) {
        // asynchronous
        process.nextTick(function() {
            var provider = 'local';

            var userEmail = {
                email: email,
                userName: email
            }

            // Search user in db
            logger.log(req, 'grey', 'Local login', 'User \\0 try to connect', email, password);
            dbengine.users.findLocalUser(userEmail, password, function(err: Error, dbu: User.DbUser, failAttempts: number, accountBlocked: boolean) {
                if (err) {
                    var next = function() {
                        var validationMessages: Array<validation.Message> = [];
                        validationMessages.push({
                            fieldName: '#user_login',
                            message: err.message,
                            tooltip: true,
                            type: 'error',
                            name: err.name
                        })
                        validationMessages.push({
                            fieldName: '#user_password',
                            message: err.message,
                            tooltip: true,
                            type: 'error',
                            name: err.name
                        })

                        logger.log(req, 'red', 'Local login', 'User \\0 and password \\1 rejected', email, password);
                        return done(null, null, req.flash('message', validationMessages), req.flash('provider', provider));
                    }

                    if (accountBlocked) {
                        // Send mail for blocked account
                        // Send confirmation mail
                        var locale = sessionProvider.States.state(req.session.id).locale || 'en';
                        var mailInfos: mail.MailInfos = {
                            locale: locale,
                            dest: dbu.email,
                            user: 'tubecurvetracer@gmail.com',
                            password: 'zestilegbcnrdzda',
                            object: {
                                uid: 'accountlocked',
                                text: 'Your account is locked'
                            },
                            body: {
                                uid: 'lockedmailbody',
                                text: 'Hello \\0 \\0 Because you have reached the limit of \\1 connexions with a wrong password, your account has been locked. \\0 You can click on the following link to unlock your account. \\0 \\2 \\0 \\0 \\0 If you forget your password, or want to change it, please visit the following link. \\0 \\3 \\0 \\0 Best Regards \\0 \\4'
                            },
                            bodyParams: {
                                0: '\n',
                                1: String(config.maxFailLoginAttempts),
                                2: authConfig.local.unlockUrl + dbu.validationToken + '&lng=' + locale,
                                3: authConfig.local.changePwdUrl + dbu.validationToken + '&lng=' + locale,
                                4: 'tct Team'
                            }
                        }

                        logger.log(req, 'grey', 'Local login', 'Account \\0 locked, send an email.', email);
                        mail.Provider.send(mailInfos, function(err: Error) {
                            if (err) {
                                logger.logError(req, 'Local login', err);
                            }

                            next();
                        })
                    } else {
                        next();
                    }
                } else {
                    logger.log(req, 'green', 'Local login', 'User \\0 connected', email);

                    dbu.lastLogin = (new Date()).getTime();
                    dbu.lastIP = req ? req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || (req.connection as any).socket.remoteAddress : '';
                    dbengine.users.findOrCreateUser(dbu, function(err) {
                        if (err) {
                            logger.logError(req, 'Local login', err);
                        }
                    })

                    if (failAttempts) {
                        var failAttemptsMessage: validation.Message = {
                            message: 'Login successfull. \\0 attempt connections have failed before.',
                            type: 'warning',
                            name: 'msg-loginattemtps',
                            params: { 0: failAttempts }
                        }
                        return done(null, dbu, req.flash('message', [failAttemptsMessage]), req.flash('provider', provider));
                    } else {
                        return done(null, dbu);
                    }
                }
            })
        });
    }));

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'user_email',
        passwordField: 'user_password',
        passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    }, function(req: Express.Request, email: string, password: string, done: Function) {
        // Get username from request
        var username = req.body.user_username;

        // asynchronous
        process.nextTick(function() {
            var provider = 'local';

            var userEmail = {
                email: email,
                userName: username
            }

            var fail = function(verrors: Array<validation.Message>) {
                logger.logErrors(req, 'Local signup', verrors);
                return done(null, null, req.flash('message', verrors));
            }

            // Check mail provider 
            mailProviderValidator.validate(email, function(mailerr: Error) {
                if (mailerr) {
                    return fail([{
                        fieldName: '#user_email',
                        message: mailerr.message,
                        tooltip: true,
                        type: 'error',
                        name: mailerr.name
                    }]);
                }

                // Validate received datas with the common validator
                var userValidator = new User.Validator();
                userValidator.validateSignup(userEmail, password, password, function(verrors) {
                    if (verrors && verrors.length) {
                        // Normally already validated client side (same validator)                    
                        return fail(verrors);
                    }

                    dbengine.users.checkLocalUser(userEmail, function(err) {
                        if (err) {
                            return fail([{
                                fieldName: '#user_email',
                                message: err.message,
                                tooltip: true,
                                type: 'error',
                                name: err.name
                            }])
                        }

                        var user: User.User = {
                            _id: provider + '-' + email,
                            displayName: username,
                            provider: provider,
                            email: email,
                            userName: username,
                            validated: false,
                            validationToken: bcrypt.genSaltSync(45) + 'av',
                            validationTime: (new Date()).getTime()
                        }

                        // Send confirmation mail
                        var locale = sessionProvider.States.state(req.session.id).locale || 'en';
                        var mailInfos: mail.MailInfos = {
                            locale: locale,
                            dest: user.email,
                            user: 'tubecurvetracer@gmail.com',
                            password: 'zestilegbcnrdzda',
                            object: {
                                uid: 'newacccreation',
                                text: 'New account creation'
                            },
                            body: {
                                uid: 'confmailbody',
                                text: 'Hello \\0 \\0 An account has been created for you at \\1 . \\0 Please visit the following url to activate your account. \\0 \\2 \\0 \\0 \\0 If the account was created in error, you can visit the following link to decline this invitation. \\0 \\3 \\0 \\0 Best Regards \\0 \\4'
                            },
                            bodyParams: {
                                0: '\n',
                                1: 'tube curve tracer',
                                2: authConfig.local.validationUrl + user.validationToken + '&lng=' + locale,
                                3: authConfig.local.declineUrl + user.validationToken + '&lng=' + locale,
                                4: 'tct Team'
                            }
                        }

                        // Create and add new user
                        mail.Provider.send(mailInfos, function(err: Error) {
                            if (err) {
                                return fail([{
                                    fieldName: '#user_email',
                                    message: err.message,
                                    tooltip: true,
                                    type: 'error',
                                    name: err.name
                                }]);
                            }

                            logger.log(req, 'grey', 'Local signup', 'Adding user \\0 to db', email);
                            dbengine.users.addUser(user, password, function(err) {
                                if (err) {
                                    return fail([{
                                        fieldName: '#user_username',
                                        message: err.message,
                                        tooltip: true,
                                        type: 'error',
                                        name: err.name
                                    }]);
                                }

                                // Because the logn must be confirmed by email, the login always fail by this way
                                return fail([{
                                    message: 'A confirmation email has been sent. Please check your mail and click on the link on the sent email to validate the account creation.',
                                    type: 'info',
                                    name: 'msg-emailsent'
                                }]);
                            })
                        });
                    })
                })
            })
        });
    }));

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({
        clientID: authConfig.google.id,
        clientSecret: authConfig.google.secret,
        callbackURL: authConfig.google.redirect,
        passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    }, function(req: Express.Request, token: string, refreshToken: string, profile: Passport.Profile, done: Function) {
        // asynchronous
        process.nextTick(function() {
            var user: User.User;
            var provider = 'google';
            var userName = (profile.emails.length && profile.emails[0].value) || profile.displayName;

            logger.log(req, 'white', 'Login ' + provider, 'Connecting user \\0', userName);
            if (req.user) {
                if (req.user.provider !== provider) {
                    var validationMessage: validation.Message = {
                        message: 'You are already logged, please logout after login again.',
                        type: 'error',
                        name: 'msg-alreadylogged'
                    }

                    logger.logError(req, 'Login ' + provider, validationMessage);
                    return done(null, null, req.flash('message', [validationMessage]), req.flash('provider', provider));
                }

                user = req.user;
            } else {
                // Create standard user from google profile
                user = {
                    _id: provider + '-' + profile.id,
                    provider: provider,
                    validated: true
                }
            }

            user.token = token;
            user.displayName = profile.displayName;
            user.userName = profile.username;
            user.email = profile.emails && profile.emails.length && profile.emails[0].value;
            user.photo = profile.photos && profile.photos.length && profile.photos[0].value;
            user.lastLogin = (new Date()).getTime();
            user.lastIP = req ? req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || (req.connection as any).socket.remoteAddress : '';

            // Search, add or update user in db
            logger.log(req, 'grey', 'Login ' + provider, 'Searching \\0 in db', userName);
            dbengine.users.findOrCreateUser(user, function(err: Error, dbu: User.DbUser) {
                if (err) {
                    logger.log(req, 'red', 'Login ' + provider, 'User \\0 in error \\1', userName, err.message);
                    return done(null, null, req.flash('message', [err.message]));
                }

                logger.log(req, 'green', 'Login ' + provider, 'User \\0 connected', userName);
                user.role = dbu.role;
                return done(null, user);
            })
        });
    }));

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({
        clientID: authConfig.facebook.id,
        clientSecret: authConfig.facebook.secret,
        callbackURL: authConfig.facebook.redirect,
        passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)    
    }, function(req: Express.Request, token: string, refreshToken: string, profile: Passport.Profile, done: Function) {
        // asynchronous
        process.nextTick(function() {
            var user: User.User;
            var provider = 'facebook';
            var userName = (profile.name.givenName + profile.name.familyName) || profile.displayName;

            logger.log(req, 'white', 'Login ' + provider, 'Connecting user \\0', userName);
            if (req.user) {
                if (req.user.provider !== provider) {
                    var validationMessage: validation.Message = {
                        message: 'You are already logged, please logout after login again.',
                        type: 'error',
                        name: 'msg-alreadylogged'
                    }

                    logger.logError(req, 'Login ' + provider, validationMessage);
                    return done(null, null, req.flash('message', [validationMessage]), req.flash('provider', provider));
                }

                user = req.user;
            } else {
                // Create standard user from google profile
                user = {
                    _id: provider + '-' + profile.id,
                    provider: provider,
                    validated: true
                }
            }

            user.token = token;
            user.displayName = profile.displayName;
            user.userName = profile.username;
            user.email = profile.emails && profile.emails.length && profile.emails[0].value;
            user.lastLogin = (new Date()).getTime();
            user.lastIP = req ? req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || (req.connection as any).socket.remoteAddress : '';

            // Search, add or update user in db
            logger.log(req, 'grey', 'Login ' + provider, 'Searching \\0 in db', userName);
            dbengine.users.findOrCreateUser(user, function(err: Error, dbu: User.DbUser) {
                if (err) {
                    logger.log(req, 'red', 'Login ' + provider, 'User \\0 in error \\1', userName, err.message);
                    return done(null, null, req.flash('message', [err.message]));
                }

                logger.log(req, 'green', 'Login ' + provider, 'User \\0 connected', userName);
                user.role = dbu.role;
                return done(null, user);
            })
        });
    }));

    // =========================================================================
    // TWITTER =================================================================
    // =========================================================================
    passport.use(new TwitterStrategy({
        consumerKey: authConfig.twitter.consumerKey,
        consumerSecret: authConfig.twitter.consumerSecret,
        callbackURL: authConfig.twitter.redirect,
        passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    }, function(req: Express.Request, token: string, refreshToken: string, profile: Passport.Profile, done: Function) {
        // asynchronous
        process.nextTick(function() {
            // check if the user is already logged in
            var user: User.User;
            var provider = 'tweeter';
            var userName = (user && user.userName) || req.session.id;

            logger.log(req, 'white', 'Login ' + provider, 'Connecting user \\0', userName);
            if (req.user) {
                if (req.user.provider !== provider) {
                    var validationMessage: validation.Message = {
                        message: 'You are already logged, please logout after login again.',
                        type: 'error',
                        name: 'msg-alreadylogged'
                    }

                    logger.logError(req, 'Login ' + provider, validationMessage);
                    return done(null, null, req.flash('message', [validationMessage]), req.flash('provider', provider));
                }

                user = req.user;
            } else {
                // Create standard user from google profile
                user = {
                    _id: provider + '-' + profile.id,
                    provider: provider,
                    validated: true
                }
            }

            user.token = token;
            user.displayName = profile.displayName;
            user.userName = profile.username;
            user.photo = profile.photos && profile.photos.length && profile.photos[0].value;
            user.email = profile.emails && profile.emails.length && profile.emails[0].value;
            user.lastLogin = (new Date()).getTime();
            user.lastIP = req ? req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || (req.connection as any).socket.remoteAddress : '';

            // Search, add or update user in db
            logger.log(req, 'grey', 'Login ' + provider, 'Searching \\0 in db', userName);
            dbengine.users.findOrCreateUser(user, function(err: Error, dbu: User.DbUser) {
                if (err) {
                    logger.log(req, 'red', 'Login ' + provider, 'User \\0 in error \\1', userName, err.message);
                    return done(null, null, req.flash('message', [err.message]), req.flash('provider', provider));
                }

                logger.log(req, 'green', 'Login ' + provider, 'User \\0 connected', userName);
                user.role = dbu.role;
                return done(null, user);
            })
        });
    }));

    // =========================================================================
    // Github ============================================================
    // =========================================================================
    passport.use(new GitHubStrategy({
        clientID: authConfig.github.id,
        clientSecret: authConfig.github.secret,
        callbackURL: authConfig.github.redirect,
        passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    }, function(req: Express.Request, token: string, refreshToken: string, profile: Passport.Profile, done: Function) {
        // asynchronous
        process.nextTick(function() {
            var user: User.User;
            var provider = 'github';
            var userName = (user && user.userName) || req.session.id;

            logger.log(req, 'white', 'Login ' + provider, 'Connecting user \\0', userName);
            if (req.user) {
                if (req.user.provider !== provider) {
                    var validationMessage: validation.Message = {
                        message: 'You are already logged, please logout after login again.',
                        type: 'error',
                        name: 'msg-alreadylogged'
                    }

                    logger.logError(req, 'Login ' + provider, validationMessage);
                    return done(null, null, req.flash('message', [validationMessage]), req.flash('provider', provider));
                }

                user = req.user;
            } else {
                // Create standard user from google profile
                user = {
                    _id: provider + '-' + profile.id,
                    provider: provider,
                    validated: true
                }
            }

            user.token = token;
            user.displayName = profile.displayName;
            user.userName = profile.username;
            user.email = profile.emails && profile.emails.length && profile.emails[0].value;
            user.lastLogin = (new Date()).getTime();
            user.lastIP = req ? req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || (req.connection as any).socket.remoteAddress : '';

            // Search, add or update user in db
            logger.log(req, 'grey', 'Login ' + provider, 'Searching \\0 in db', userName);
            dbengine.users.findOrCreateUser(user, function(err: Error, dbu: User.DbUser) {
                if (err) {
                    logger.log(req, 'red', 'Login ' + provider, 'User \\0 in error \\1', userName, err.message);
                    return done(null, null, req.flash('message', [err.message]), req.flash('provider', provider));
                }

                logger.log(req, 'green', 'Login ' + provider, 'User \\0 connected', userName);
                user.role = dbu.role;
                return done(null, user);
            })
        });
    }));

}

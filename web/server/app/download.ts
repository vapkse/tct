'use strict';

import path = require('path');
import fs = require('fs');
import express = require('express');
import config = require('../../config');
import logger = require('../utils/logger');
import translationrenderer = require('../utils/i18n/renderer');
import renderer = require('../utils/renderer');

var app = express();

/* Download request
Params:
    url: Download filename
    isnew: True if the file is new and not saved (temporary directory)
*/
app.get('/', function(req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        message: req.flash('message')
    }

    var fail = function(err: Error) {
        logger.logError(req, 'Download', err);
        translationrenderer.renderjson(options, err, function(json) {
            res.status(500).send(json.message);
        })
    }

    logger.log(req, 'grey', 'Download', 'Downloading ' + req.url);
    var filename = req.query.url;
    if (!filename) {
        fail({
            message: 'No file specified.',
            name: 'msg-nofilesepcified'
        });
        return;
    }

    var filePath: string;
    if (req.query.isnew) {
        var userDir = req.user.userName.replace(/[\/:*?"<>|]/gi, '_').toLowerCase();
        filePath = path.resolve(config.path.upload, userDir, filename);
    } else {
        filePath = path.resolve(config.path.documents, filename);
    }

    fs.exists(filePath, function(exists) {
        // Get file extension
        var fileInfo = /(^.+)(\.[^.]+)$/.exec(filename);
        //var fileBase = fileInfo ? fileInfo[1] : filename;
        var ext = fileInfo.length > 1 && fileInfo[2].toLowerCase();

        var sendContent = function(contentType: string) {
            // Return blob file content
            fs.readFile(filePath, function(err, data) {
                if (err) {
                    fail(err);
                    return;
                }

                res.writeHead(200, {
                    'Content-Type': contentType,
                    'Content-Disposition': 'attachment; filename=' + filename,
                    'Content-Length': data.length
                });
                res.end(data);
            })
        }

        var setContent = function() {
            switch (ext) {
                case 'tds':
                    // Return JSON content
                    sendContent('application/json');
                    break;

                case 'pdf':
                    sendContent('application/pdf');
                    break;

                case 'jpeg':
                case 'jpg':
                    sendContent('image/jpeg');
                    break;

                case 'png':
                    sendContent('image/png');
                    break;

                case 'tiff':
                case 'tif':
                    sendContent('image/tiff');
                    break;

                case 'bmp':
                    sendContent('image/bitmap');
                    break;

                default:
                    sendContent('application/octet-stream');
                    break;
            }
        }

        if (!exists) {
            fail({
                message: 'File not found: ' + filename,
                name: 'msg-filenotfound'
            });
        } else {
            setContent();
        }
    })
})


export = app;
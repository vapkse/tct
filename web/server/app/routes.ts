'use strict';

// For typescript
import Passport = require('passport');
import Express = require('express');
import User = require('../../common/ts/user'); // Namespace only
import DataProviding = require('../../common/ts/data-providing');

// Real import
import config = require('../../config');
import express = require('express');
import translationrenderer = require('../utils/i18n/renderer');
import search = require('./search');
import download = require('./download');
import admin = require('../admin/admin');
import translate = require('../utils/i18n/server');
import editor = require('../editor/editor');
import model = require('../model/model');
import merge = require('../merge/merge');
import viewer = require('../viewer/viewer');
import usages = require('../usages/usages');
import documents = require('../documents/documents');
import creator = require('../creator/creator');
import api = require('../api/api');
import dbengine = require('../database/engine');
import logger = require('../utils/logger');
import resetPassword = require('./reset-password');
import sessionProvider = require('../utils/session-provider');
import tube = require('../../common/ts/tube');
import tubeUnit = require('../../common/ts/tube-unit');
import path = require('path');
import fs = require('fs');
var uaparser = require('ua-parser-js');

var trafficAnalysis = {} as {
    [ip: string]: {
        temporarilyBanned?: number,
        browserUpdateRequest?: number,
        lastNotFound?: number,
        notFoundRequest?: number,
    }
}

module.exports = function(app: Express.Application, passport: Passport.Passport) {
    // Ensure authentication for privates destinations
    var ensureAuthenticated = function(req: Express.Request, res: Express.Response, next: Function) {
        if (req.isAuthenticated()) {
            var user = req.user as User.User;
            if (user.blocked) {
                res.redirect('/logout');
            } else if (!user.validated) {
                var validationMessage: validation.Message = {
                    message: 'Your account is not been validated, please validate your account from the sent mail.',
                    type: 'warning',
                    name: 'msg-accountnotvalid'
                }
                req.flash('message', validationMessage);
                res.redirect('/');
            } else {
                return next();
            }
        } else {
            res.redirect('/login?rl=' + encodeURIComponent(req.originalUrl));
        }
    }

    function ensureLatestBrowser(req: Express.Request, res: Express.Response, next: Function) {
        var sessionState = sessionProvider.States.state(req.session.id);

        if (req.query.browser === 'true') {
            sessionState.ignoreBrowserVersion = true;
        }

        if (sessionState.ignoreBrowserVersion) {
            next();
            return;
        }

        var parser = new uaparser();
        parser.setUA(req.headers['user-agent']);
        var browser = parser.getBrowser();
        var fullBrowserVersion = browser.version;
        var browserVersion = fullBrowserVersion && fullBrowserVersion.split('.', 1).toString();
        var browserVersionNumber = browserVersion ? Number(browserVersion) : 0;
        var clientAnalysis = trafficAnalysis[req.ip];

        var redirect = function() {
            if (!clientAnalysis) {
                trafficAnalysis[req.ip] = clientAnalysis = {};
            }
            clientAnalysis.browserUpdateRequest = clientAnalysis.browserUpdateRequest ? clientAnalysis.browserUpdateRequest + 1 : 1;
            if (clientAnalysis.browserUpdateRequest >= 5) {
                clientAnalysis.temporarilyBanned = (new Date()).getTime() + 3600000;
            }
            logger.log(req, 'yellow', 'Route', 'Browser detected ' + browser.name + ' ' + browser.version);
            res.redirect('/browserupdate');
        }

        if (browser.name === 'IE' && browserVersionNumber <= 9) {
            redirect();
        } else if (browser.name === 'Firefox' && browserVersionNumber <= 30) {
            redirect();
        } else if (browser.name === 'Chrome' && browserVersionNumber <= 30) {
            redirect();
        } else if (browser.name === 'Canary' && browserVersionNumber <= 35) {
            redirect();
        } else if (browser.name === 'Safari' && browserVersionNumber <= 5) {
            redirect();
        } else if (browser.name === 'Opera' && browserVersionNumber <= 25) {
            redirect();
        } else {
            logger.log(req, 'blue', 'Route', 'Browser detected ' + browser.name + ' ' + browser.version);
            if (clientAnalysis) {
                delete clientAnalysis.browserUpdateRequest;
            }
            return next();
        }
    }

    var ensureAdmin = function(req: Express.Request, res: Express.Response, next: Function) {
        if (req.isAuthenticated() && req.user.role === 'admin') {
            return next();
        }
        res.redirect('/login?rl=' + encodeURIComponent(req.originalUrl));
    }

    app.use(function(req, res, next) {
        var clientAnalysis = trafficAnalysis[req.ip];
        if (clientAnalysis && clientAnalysis.temporarilyBanned) {
            if (clientAnalysis.temporarilyBanned > (new Date()).getTime()) {
                logger.log(req, 'cyan', 'Route', 'Forbidden send for ' + req.ip);
                res.status(403).end('Temporarily Banned');
                return;
            }
            delete clientAnalysis.temporarilyBanned;
        }
        next();
    })

    // Public queries server
    app.get('/', ensureLatestBrowser, function(req, res) {
        var options = {
            query: req.query,
            sessionID: req.session.id,
            user: req.user,
            message: req.flash('message')
        }

        logger.log(req, 'grey', 'Route', 'Navigate to home');
        res.render('home.html', options);
    });

    app.get('/browserupdate', function(req, res) {
        var options = {
            query: req.query,
            sessionID: req.session.id,
            user: req.user,
        }

        logger.log(req, 'grey', 'Route', 'Navigate to notsupported');
        res.render('notsupported.html', options);
    })

    app.get('/login', function(req, res) {
        var options = {
            query: req.query,
            sessionID: req.session.id,
            user: req.user,
            message: req.flash('message'),
            provider: req.flash('provider')
        }
        if (options.message && options.message.length) {
            translationrenderer.renderjson(options, { 'messages': options.message }, function(json: any) {
                if (options.provider && options.provider.length && options.provider[0] === 'local') {
                    res.json(json);
                } else {
                    logger.log(req, 'grey', 'Route', 'Navigate to login')
                    res.render('login.html', options);
                }
            })
        }
        else {
            logger.log(req, 'grey', 'Route', 'Navigate to login')
            res.render('login.html', options);
        }
    });

    // Global resource server
    app.get('/resource', function(req, res) {
        var options = {
            query: req.query,
            sessionID: req.session.id,
            user: req.user,
            message: req.flash('message')
        }
        if (req.query.img) {
            // SVG
            fs.exists(path.resolve(config.path.views, req.query.img), function(exists) {
                if (!exists) {
                    res.status(200).end();
                } else {
                    res.render(req.query.img, options);
                }
            })
        } else if (req.query.j) {
            // Json not translated
            fs.readFile(path.resolve(config.path.views, req.query.j), function(err: Error, data: any) {
                if (err) {
                    logger.logError(req, 'Route', err);
                    res.status(500).send(err.message);
                } else {
                    var json = JSON.parse(data.toString());
                    res.json(json);
                }
            });

        } else if (req.query.r) {
            // File resource
            res.render(req.query.r, options);

        } else if (req.query.c) {
            // DB ressource
            logger.log(req, 'grey', 'Route', 'Loading ressource ' + req.query.c);
            var returnResult = function(err: Error, result: any) {
                if (err) {
                    logger.logError(req, 'Route', err);
                    res.status(500).send(err.message);
                } else {
                    logger.log(req, 'grey', 'Route', 'Ressource loaded ' + req.query.c);
                    res.json(result);
                }
            }

            var selector = req.query.q;
            switch (req.query.c) {
                case 'tpins':
                    dbengine.tubePins.getPinouts(selector, returnResult);
                    break;
                case 'tbases':
                    dbengine.tubeBases.getBases(selector, returnResult);
                    break;
                case 'ttypes':
                    dbengine.tubeTypes.getTypes(selector, function(err: Error, result: any) {
                        if (err) {
                            res.status(500).send(err.message);
                        } else {
                            translationrenderer.renderjson(options, result, function(json: any) {
                                res.json(json);
                            })
                        }
                    });
                    break;
            }
        }
    })

    // Search server
    app.use('/search', search);

    // Download server
    app.use('/download', download);

    // Admin server
    app.use('/admin', ensureAdmin, admin);

    // Global translator server
    app.use('/translator', translate);

    // Graph creator server
    app.use('/creator', ensureAuthenticated, creator);

    // Tube data editor server
    app.use('/editor', ensureAuthenticated, editor);

    // Modeling server
    app.use('/model', ensureAuthenticated, model);

    // Merge server for global db
    app.use('/merge', ensureAdmin, merge);

    // Tube data viewer server
    app.use('/viewer', viewer);

    // Tube usage examples server
    app.use('/usages', ensureAuthenticated, usages);

    // Documents server
    app.use('/document', ensureAuthenticated, documents);

    app.post('/delete', function(req, res) {
        var entity = req.query.e;
        if (entity === 'tube') {
            var tubeid = parseInt(req.query.id);
            var userid: string;
            if (req.query.userid) {
                if (req.query.userid !== req.user._id && req.user.role !== 'admin') {
                    // Delete foreign entities, only for admin
                    var usererr = {
                        name: 'deletenotallowed',
                        message: 'You are not allowed to delete entities of another user.'
                    }
                    logger.logError({
                        query: req.query,
                        sessionID: req.session.id,
                        user: req.user,
                    }, 'Editor', usererr);
                    res.status(500).send(usererr.message);
                    return;
                }
                userid = req.query.userid;
            } else {
                userid = req.user._id;
            }

            dbengine.tubes.deleteTube(tubeid, userid, function(err: Error, tubeData: TubeData) {
                if (err) {
                    res.status(500).send(err.message);
                } else {
                    res.json(tubeData);
                }
            });

        } else {
            var err: Error = {
                message: 'Invalid parameters for delete request: ' + req.originalUrl,
                name: 'invalid-del-params'
            }
            logger.logError(req, 'Route', err);
            res.status(500).send(err.message);
        }
    })

    // WebAPI server
    app.use('/api', api);

    // Other apps redirection
    app.get('/cv', function(req, res) {
        var options = {
            query: req.query,
            sessionID: req.session.id,
        }
        if (req.query.lng) {
            logger.log(req, 'magenta', 'Route', 'Navigate to multilangual cv');
            res.render('cv/index-mult.html', options);
        } else {
            logger.log(req, 'magenta', 'Route', 'Navigate to cv');
            res.redirect('cv/index.html');
        }
    });

    // Logout query
    app.get('/logout', function(req, res) {
        logger.log(req, 'grey', 'Route', 'Logout')
        req.logout();
        res.redirect('/');
    });

    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/login',
        failureRedirect: '/login',
        failureFlash: true // allow flash messages
    }));

    // Public queries server
    app.get('/signup', function(req, res) {
        var options = {
            query: req.query,
            sessionID: req.session.id,
            user: req.user,
            message: req.flash('message') as Array<validation.Message>,
        }

        if (options.message && options.message.length) {
            translationrenderer.renderjson(options, { 'messages': options.message }, function(json: any) {
                res.json(json);
            })
        }
        else {
            logger.log(req, 'grey', 'Route', 'Navigate to signup');
            res.render('login.html', options);
        }
    });

    // process the signup form
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/signup',
        failureRedirect: '/signup',
        failureFlash: true // allow flash messages
    }));

    // Register account from mail 
    app.get('/register', function(req, res) {
        var token = req.query.id;
        var lang = req.query.lng;

        logger.log(req, 'grey', 'Route', 'Validating account ' + token);
        dbengine.users.validateLocalUserInvitation(token, function(err, dbu) {
            var redirect = function(url: string) {
                if (lang) {
                    url += '?lng=' + lang;
                }
                res.redirect(url);
            }

            if (err) {
                logger.logError(req, 'Route', err);
                var ve: validation.Message = {
                    message: err.message,
                    type: 'error',
                    name: err.name
                }

                req.flash('message', [ve]);
                redirect('/');
            } else {
                logger.log(req, 'green', 'Route', 'Account validated ' + dbu.userName || dbu.email);
                var vm: validation.Message = {
                    name: 'msg-successvalidated',
                    type: 'info',
                    message: 'Your account is validated, you can login now.'
                }

                req.flash('message', [vm]);
                redirect('/login');
            }
        })
    });

    // Reject account from mail 
    app.get('/decline', function(req, res) {
        var token = req.query.id;
        var lang = req.query.lng;
        var vm: validation.Message;

        logger.log(req, 'grey', 'Route', 'Canceling account ' + token);
        dbengine.users.declineLocalUserInvitation(token, function(err) {
            var redirect = function(url: string) {
                if (lang) {
                    url += '?lng=' + lang;
                }
                res.redirect(url);
            }

            if (err) {
                logger.logError(req, 'Route', err);
                vm = {
                    message: err.message,
                    type: 'error',
                    name: err.name
                }
            } else {
                logger.log(req, 'grey', 'Route', 'Account creation canceled ' + token);
                vm = {
                    name: 'msg-successdecline',
                    type: 'info',
                    message: 'Your account is successfully deleted.'
                }
            }

            req.flash('message', [vm]);
            redirect('/');
        })
    });

    // Unlock account from mail 
    app.get('/unlock', function(req, res) {
        var token = req.query.id;
        var lang = req.query.lng;

        logger.log(req, 'grey', 'Route', 'Unlocking account ' + token);
        dbengine.users.unlockLocalAccountFromToken(token, function(err, dbu) {
            var redirect = function(url: string) {
                if (lang) {
                    url += '?lng=' + lang;
                }
                res.redirect(url);
            }

            if (err) {
                logger.logError(req, 'Route', err);
                var ve: validation.Message = {
                    message: err.message,
                    type: 'error',
                    name: err.name
                }

                req.flash('message', [ve]);
                redirect('/');
            } else {
                logger.log(req, 'green', 'Route', 'Account unocked ' + dbu.userName || dbu.email);
                var vm: validation.Message = {
                    name: 'msg-successunlocked',
                    type: 'info',
                    message: 'Your account is unlocked, you can login now.'
                }

                req.flash('message', [vm]);
                redirect('/login');
            }
        })
    });

    app.use('/pwdreset', resetPassword);

    // send to google to do the authentication
    app.get('/auth/google', function(req, res, next) {
        req.session.returnTo = req.query.rl || '/';
        next();
    }, passport.authenticate('google', { scope: ['profile', 'email'] }));

    // the callback after google has authenticated the user
    app.get('/auth/google/callback',
        passport.authenticate('google', {
            successReturnToOrRedirect: '/',
            failureRedirect: '/login',
            failureFlash: true // allow flash messages
        })
    );

    // send to facebook to do the authentication
    app.get('/auth/facebook', function(req, res, next) {
        req.session.returnTo = req.query.rl || '/';
        next();
    }, passport.authenticate('facebook', { scope: 'email' }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successReturnToOrRedirect: '/',
            failureRedirect: '/login',
            failureFlash: true // allow flash messages
        })
    );

    // send to twitter to do the authentication
    app.get('/auth/twitter', function(req, res, next) {
        req.session.returnTo = req.query.rl || '/';
        next();
    }, passport.authenticate('twitter', { scope: 'email' }));

    // handle the callback after twitter has authenticated the user
    app.get('/auth/twitter/callback',
        passport.authenticate('twitter', {
            successReturnToOrRedirect: '/',
            failureRedirect: '/login',
            failureFlash: true // allow flash messages
        })
    );

    // send to github to do the authentication
    app.get('/auth/github', function(req, res, next) {
        req.session.returnTo = req.query.rl || '/';
        next();
    }, passport.authenticate('github'));

    // handle the callback after twitter has authenticated the user
    app.get('/auth/github/callback',
        passport.authenticate('github', {
            successReturnToOrRedirect: '/',
            failureRedirect: '/login',
            failureFlash: true // allow flash messages
        })
    );

    app.use(express.static(config.path.app));

    // Last route 404
    app.use(function(req, res, next) {
        var err = new Error('Not Found');
        err.name = '404';

        var clientAnalysis = trafficAnalysis[req.ip];
        if (!clientAnalysis) {
            trafficAnalysis[req.ip] = clientAnalysis = {};
        }
        var now = (new Date()).getTime();
        if (now - clientAnalysis.lastNotFound < 1000) {
            clientAnalysis.notFoundRequest = clientAnalysis.notFoundRequest ? clientAnalysis.notFoundRequest + 1 : 1;
            if (clientAnalysis.notFoundRequest >= 5) {
                clientAnalysis.temporarilyBanned = now + 3600000;
            }
        } else {
            delete clientAnalysis.notFoundRequest;
        }        
        clientAnalysis.lastNotFound  = now;

        next(err);
    });

    // Error handling
    function logErrors(err: any, req: Express.Request, res: Express.Response, next: Function) {
        if (err.name === '404') {
            logger.log(req, 'yellow', 'Route', '404 for url ' + req.originalUrl);

            var options = {
                query: req.query,
                sessionID: req.session.id,
                user: req.user
            }

            logger.log(req, 'grey', 'Route', 'Navigate to notfound');
            res.render('notfound.html', options);
            return;
        }

        logger.log(req, 'red', 'Route', 'Express error ' + err.stack);
        next(err);
    }

    function clientErrorHandler(err: any, req: Express.Request, res: Express.Response, next: Function) {
        if (req.xhr) {
            res.status(500).send('Server Error');
        } else {
            next(err);
        }
    }

    function errorHandler(err: any, req: Express.Request, res: Express.Response, next: Function) {
        res.status(500);

        res.render('error', { error: err });
        /*jshint unused: false */
        var n = next;
    }

    app.use(logErrors);
    app.use(clientErrorHandler);
    app.use(errorHandler);
}
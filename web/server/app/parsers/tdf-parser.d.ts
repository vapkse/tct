declare var parser: {
    parse: (file: NodeJS.ReadableStream, filename: string, callback: Function) => void;
};
export = parser;

'use strict';
var parser = {
    parse: function (file, filename, callback) {
        var getValue = function (value) {
            return value === '.' ? 0 : parseFloat(value);
        };
        var measures = [];
        var addCurve = function (curve) {
            curve.p.sort(function (p1, p2) {
                return p1.va - p2.va;
            });
            measures.push(curve);
        };
        file.on('data', function (data) {
            var points = data.toString().split('\n');
            var lastvg1;
            var curve;
            for (var i = 0; i < points.length; i++) {
                var values = points[i].split(' ');
                var vg1 = getValue(values[2]);
                if (vg1 !== lastvg1) {
                    lastvg1 = vg1;
                    if (curve && curve.p.length) {
                        addCurve(curve);
                    }
                    curve = {
                        vg1: vg1,
                        p: []
                    };
                }
                else {
                    curve.p.push({
                        va: getValue(values[0]),
                        ik: getValue(values[1]),
                        vg1: vg1,
                        ig2: 0,
                        ig1: 0,
                        vg2: 0
                    });
                }
            }
            if (curve && curve.p.length) {
                addCurve(curve);
            }
        });
        file.on('end', function () {
            measures.sort(function (c1, c2) {
                return c2.vg1 - c1.vg1;
            });
            callback(measures);
        });
    }
};
module.exports = parser;
//# sourceMappingURL=tdf-parser.js.map
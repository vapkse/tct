'use strict';

import path = require('path');
import cacheProvider = require('../utils/cache');
import express = require('express');
import dbengine = require('../database/engine');
import translationrenderer = require('../utils/i18n/renderer');
import renderer = require('../utils/renderer');
import config = require('../../config');
import logger = require('../utils/logger');
import User = require('../../common/ts/user');

var cache = cacheProvider.Cache;
var app = express();

/* Search request
Params:
    q: Search query
    reset: Ignore cache and search in DB again 
    tmpl<template url>: ask for result template
    lb: Request for bases lookup
    lt: Request for types lookup
    lp: Request for pinouts lookup
    p: Pages to return
    max: Number of maximums entities in result (default: 50)    
*/
app.get('/', function(req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        message: req.flash('message')
    }

    var fail = function(err: Error) {
        logger.logError(req, 'Search', err);
        translationrenderer.renderjson(options, err, function(json) {
            res.status(500).send(json.message);
        })
    }

    var result: TubesSearchResult = {};
    var resultTemplate: string;
    var nocache = req.query.reset !== undefined;

    logger.log(req, 'grey', 'Search', 'Searching for ' + req.url);
    var returnRightPage = function(searchResult: TubesSearchResult) {
        searchResult.template = resultTemplate;
        var max = parseInt(req.query.max);
        if (isNaN(max)) {
            max = 50;
        }
        var page = parseInt(req.query.p);
        if (!isNaN(page)) {
            // page is 1 based
            page--;
        }
        if (isNaN(page) || page < 0) {
            page = 0;
        }
        var index = page * max;
        searchResult.pageCount = Math.ceil(searchResult.tubes.length / max);
        searchResult.page = page + 1;
        searchResult.tubes = searchResult.tubes.slice(index, index + max);
        res.json(searchResult);
    }

    var tubesSearch = function() {
        if (!req.query.q) {
            res.json(result);
            return;
        }

        // Check search in cache
        var cacheKey = options.sessionID + '_' + req.query.q;
        if (!nocache) {
            var cached = cache.get(cacheKey) as Array<TubeData>;
            if (cached) {
                result.tubes = cached;
                returnRightPage(result);
                logger.log(req, 'grey', 'Search', 'Search done from cache');
                return;
            }
        }

        dbengine.tubes.searchTubes(req.query.q, req.user, function(err: Error, json?: Array<any>, users?: { [id: string]: User.UserEmail }) {
            if (err) {
                fail(err);
                return;
            } else {
                result.tubes = json;
                result.users = users;
                translationrenderer.renderjson(options, result, function(translated: any) {
                    // Store result in cache
                    cache.set(translated.tubes, cacheKey)  
                    // Define paging  
                    logger.log(req, 'white', 'Search', 'Search done from db');
                    returnRightPage(translated);
                })
            }
        });
    }

    var typesSearch = function() {
        if (req.query.lt !== undefined) {
            nocache = true;
            dbengine.tubeTypes.getTypes(null, function(err: Error, json: any) {
                if (err) {
                    logger.log(req, 'red', 'Search', 'Get types error' + err.message);
                    result.typesError = err.message;
                } else {
                    result.types = json;
                }
                tubesSearch();
            })
        } else {
            tubesSearch();
        }
    }

    var pinoutsSearch = function() {
        if (req.query.lp !== undefined) {
            nocache = true;
            dbengine.tubePins.getPinouts(null, function(err: Error, json: any) {
                if (err) {
                    logger.log(req, 'red', 'Search', 'Get pinouts error' + err.message);
                    result.pinoutsError = err.message;
                } else {
                    result.pinouts = json;
                }
                typesSearch();
            })
        } else {
            typesSearch();
        }
    }

    var basesSearch = function() {
        if (req.query.lb !== undefined) {
            nocache = true;
            dbengine.tubeBases.getBases(null, function(err: Error, json: any) {
                if (err) {
                    logger.log(req, 'red', 'Search', 'Get bases error' + err.message);
                    result.basesError = err.message;
                } else {
                    result.bases = json;
                }
                pinoutsSearch();
            })
        } else {
            pinoutsSearch();
        }
    }

    if (req.query.q === 'news') {
        dbengine.news.getLatestNews(function(err, n) {
            if (err) {
                fail(err);
                return;
            } else if (!n || n.length === 0) {
                fail({
                    name: 'nonews',
                    message: 'No news'
                })
                return;
            }
            var news = n.slice(0, 50);
            translationrenderer.renderjson(options, news, function(json: any) {
                result = {
                    page: 1,
                    pageCount: 1,
                    news: json as Array<NewsData>
                } as NewsSearchResult;
                res.json(result);
            })
        })
    } else {   
        // Check if template must be send 
        if (req.query.tmpl !== undefined) {
            nocache = true;
            var template = path.resolve(config.path.views, req.query.tmpl)
            renderer.render(template, options, function(err: Error, html: string) {
                if (err) {
                    fail(err);
                    return;
                } else {
                    resultTemplate = html;
                    // Check if lookup must be send with the result                
                    basesSearch();
                }
            })
        } else {
            // Check if lookup must be send with the result                
            basesSearch();
        }
    }
})


export = app;
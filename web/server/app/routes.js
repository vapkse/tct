'use strict';
var config = require('../../config');
var express = require('express');
var translationrenderer = require('../utils/i18n/renderer');
var search = require('./search');
var download = require('./download');
var admin = require('../admin/admin');
var translate = require('../utils/i18n/server');
var editor = require('../editor/editor');
var model = require('../model/model');
var merge = require('../merge/merge');
var viewer = require('../viewer/viewer');
var usages = require('../usages/usages');
var documents = require('../documents/documents');
var creator = require('../creator/creator');
var api = require('../api/api');
var dbengine = require('../database/engine');
var logger = require('../utils/logger');
var resetPassword = require('./reset-password');
var sessionProvider = require('../utils/session-provider');
var path = require('path');
var fs = require('fs');
var uaparser = require('ua-parser-js');
var trafficAnalysis = {};
module.exports = function (app, passport) {
    var ensureAuthenticated = function (req, res, next) {
        if (req.isAuthenticated()) {
            var user = req.user;
            if (user.blocked) {
                res.redirect('/logout');
            }
            else if (!user.validated) {
                var validationMessage = {
                    message: 'Your account is not been validated, please validate your account from the sent mail.',
                    type: 'warning',
                    name: 'msg-accountnotvalid'
                };
                req.flash('message', validationMessage);
                res.redirect('/');
            }
            else {
                return next();
            }
        }
        else {
            res.redirect('/login?rl=' + encodeURIComponent(req.originalUrl));
        }
    };
    function ensureLatestBrowser(req, res, next) {
        var sessionState = sessionProvider.States.state(req.session.id);
        if (req.query.browser === 'true') {
            sessionState.ignoreBrowserVersion = true;
        }
        if (sessionState.ignoreBrowserVersion) {
            next();
            return;
        }
        var parser = new uaparser();
        parser.setUA(req.headers['user-agent']);
        var browser = parser.getBrowser();
        var fullBrowserVersion = browser.version;
        var browserVersion = fullBrowserVersion && fullBrowserVersion.split('.', 1).toString();
        var browserVersionNumber = browserVersion ? Number(browserVersion) : 0;
        var clientAnalysis = trafficAnalysis[req.ip];
        var redirect = function () {
            if (!clientAnalysis) {
                trafficAnalysis[req.ip] = clientAnalysis = {};
            }
            clientAnalysis.browserUpdateRequest = clientAnalysis.browserUpdateRequest ? clientAnalysis.browserUpdateRequest + 1 : 1;
            if (clientAnalysis.browserUpdateRequest >= 5) {
                clientAnalysis.temporarilyBanned = (new Date()).getTime() + 3600000;
            }
            logger.log(req, 'yellow', 'Route', 'Browser detected ' + browser.name + ' ' + browser.version);
            res.redirect('/browserupdate');
        };
        if (browser.name === 'IE' && browserVersionNumber <= 9) {
            redirect();
        }
        else if (browser.name === 'Firefox' && browserVersionNumber <= 30) {
            redirect();
        }
        else if (browser.name === 'Chrome' && browserVersionNumber <= 30) {
            redirect();
        }
        else if (browser.name === 'Canary' && browserVersionNumber <= 35) {
            redirect();
        }
        else if (browser.name === 'Safari' && browserVersionNumber <= 5) {
            redirect();
        }
        else if (browser.name === 'Opera' && browserVersionNumber <= 25) {
            redirect();
        }
        else {
            logger.log(req, 'blue', 'Route', 'Browser detected ' + browser.name + ' ' + browser.version);
            if (clientAnalysis) {
                delete clientAnalysis.browserUpdateRequest;
            }
            return next();
        }
    }
    var ensureAdmin = function (req, res, next) {
        if (req.isAuthenticated() && req.user.role === 'admin') {
            return next();
        }
        res.redirect('/login?rl=' + encodeURIComponent(req.originalUrl));
    };
    app.use(function (req, res, next) {
        var clientAnalysis = trafficAnalysis[req.ip];
        if (clientAnalysis && clientAnalysis.temporarilyBanned) {
            if (clientAnalysis.temporarilyBanned > (new Date()).getTime()) {
                logger.log(req, 'cyan', 'Route', 'Forbidden send for ' + req.ip);
                res.status(403).end('Temporarily Banned');
                return;
            }
            delete clientAnalysis.temporarilyBanned;
        }
        next();
    });
    app.get('/', ensureLatestBrowser, function (req, res) {
        var options = {
            query: req.query,
            sessionID: req.session.id,
            user: req.user,
            message: req.flash('message')
        };
        logger.log(req, 'grey', 'Route', 'Navigate to home');
        res.render('home.html', options);
    });
    app.get('/browserupdate', function (req, res) {
        var options = {
            query: req.query,
            sessionID: req.session.id,
            user: req.user,
        };
        logger.log(req, 'grey', 'Route', 'Navigate to notsupported');
        res.render('notsupported.html', options);
    });
    app.get('/login', function (req, res) {
        var options = {
            query: req.query,
            sessionID: req.session.id,
            user: req.user,
            message: req.flash('message'),
            provider: req.flash('provider')
        };
        if (options.message && options.message.length) {
            translationrenderer.renderjson(options, { 'messages': options.message }, function (json) {
                if (options.provider && options.provider.length && options.provider[0] === 'local') {
                    res.json(json);
                }
                else {
                    logger.log(req, 'grey', 'Route', 'Navigate to login');
                    res.render('login.html', options);
                }
            });
        }
        else {
            logger.log(req, 'grey', 'Route', 'Navigate to login');
            res.render('login.html', options);
        }
    });
    app.get('/resource', function (req, res) {
        var options = {
            query: req.query,
            sessionID: req.session.id,
            user: req.user,
            message: req.flash('message')
        };
        if (req.query.img) {
            fs.exists(path.resolve(config.path.views, req.query.img), function (exists) {
                if (!exists) {
                    res.status(200).end();
                }
                else {
                    res.render(req.query.img, options);
                }
            });
        }
        else if (req.query.j) {
            fs.readFile(path.resolve(config.path.views, req.query.j), function (err, data) {
                if (err) {
                    logger.logError(req, 'Route', err);
                    res.status(500).send(err.message);
                }
                else {
                    var json = JSON.parse(data.toString());
                    res.json(json);
                }
            });
        }
        else if (req.query.r) {
            res.render(req.query.r, options);
        }
        else if (req.query.c) {
            logger.log(req, 'grey', 'Route', 'Loading ressource ' + req.query.c);
            var returnResult = function (err, result) {
                if (err) {
                    logger.logError(req, 'Route', err);
                    res.status(500).send(err.message);
                }
                else {
                    logger.log(req, 'grey', 'Route', 'Ressource loaded ' + req.query.c);
                    res.json(result);
                }
            };
            var selector = req.query.q;
            switch (req.query.c) {
                case 'tpins':
                    dbengine.tubePins.getPinouts(selector, returnResult);
                    break;
                case 'tbases':
                    dbengine.tubeBases.getBases(selector, returnResult);
                    break;
                case 'ttypes':
                    dbengine.tubeTypes.getTypes(selector, function (err, result) {
                        if (err) {
                            res.status(500).send(err.message);
                        }
                        else {
                            translationrenderer.renderjson(options, result, function (json) {
                                res.json(json);
                            });
                        }
                    });
                    break;
            }
        }
    });
    app.use('/search', search);
    app.use('/download', download);
    app.use('/admin', ensureAdmin, admin);
    app.use('/translator', translate);
    app.use('/creator', ensureAuthenticated, creator);
    app.use('/editor', ensureAuthenticated, editor);
    app.use('/model', ensureAuthenticated, model);
    app.use('/merge', ensureAdmin, merge);
    app.use('/viewer', viewer);
    app.use('/usages', ensureAuthenticated, usages);
    app.use('/document', ensureAuthenticated, documents);
    app.post('/delete', function (req, res) {
        var entity = req.query.e;
        if (entity === 'tube') {
            var tubeid = parseInt(req.query.id);
            var userid;
            if (req.query.userid) {
                if (req.query.userid !== req.user._id && req.user.role !== 'admin') {
                    var usererr = {
                        name: 'deletenotallowed',
                        message: 'You are not allowed to delete entities of another user.'
                    };
                    logger.logError({
                        query: req.query,
                        sessionID: req.session.id,
                        user: req.user,
                    }, 'Editor', usererr);
                    res.status(500).send(usererr.message);
                    return;
                }
                userid = req.query.userid;
            }
            else {
                userid = req.user._id;
            }
            dbengine.tubes.deleteTube(tubeid, userid, function (err, tubeData) {
                if (err) {
                    res.status(500).send(err.message);
                }
                else {
                    res.json(tubeData);
                }
            });
        }
        else {
            var err = {
                message: 'Invalid parameters for delete request: ' + req.originalUrl,
                name: 'invalid-del-params'
            };
            logger.logError(req, 'Route', err);
            res.status(500).send(err.message);
        }
    });
    app.use('/api', api);
    app.get('/cv', function (req, res) {
        var options = {
            query: req.query,
            sessionID: req.session.id,
        };
        if (req.query.lng) {
            logger.log(req, 'magenta', 'Route', 'Navigate to multilangual cv');
            res.render('cv/index-mult.html', options);
        }
        else {
            logger.log(req, 'magenta', 'Route', 'Navigate to cv');
            res.redirect('cv/index.html');
        }
    });
    app.get('/logout', function (req, res) {
        logger.log(req, 'grey', 'Route', 'Logout');
        req.logout();
        res.redirect('/');
    });
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/login',
        failureRedirect: '/login',
        failureFlash: true
    }));
    app.get('/signup', function (req, res) {
        var options = {
            query: req.query,
            sessionID: req.session.id,
            user: req.user,
            message: req.flash('message'),
        };
        if (options.message && options.message.length) {
            translationrenderer.renderjson(options, { 'messages': options.message }, function (json) {
                res.json(json);
            });
        }
        else {
            logger.log(req, 'grey', 'Route', 'Navigate to signup');
            res.render('login.html', options);
        }
    });
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/signup',
        failureRedirect: '/signup',
        failureFlash: true
    }));
    app.get('/register', function (req, res) {
        var token = req.query.id;
        var lang = req.query.lng;
        logger.log(req, 'grey', 'Route', 'Validating account ' + token);
        dbengine.users.validateLocalUserInvitation(token, function (err, dbu) {
            var redirect = function (url) {
                if (lang) {
                    url += '?lng=' + lang;
                }
                res.redirect(url);
            };
            if (err) {
                logger.logError(req, 'Route', err);
                var ve = {
                    message: err.message,
                    type: 'error',
                    name: err.name
                };
                req.flash('message', [ve]);
                redirect('/');
            }
            else {
                logger.log(req, 'green', 'Route', 'Account validated ' + dbu.userName || dbu.email);
                var vm = {
                    name: 'msg-successvalidated',
                    type: 'info',
                    message: 'Your account is validated, you can login now.'
                };
                req.flash('message', [vm]);
                redirect('/login');
            }
        });
    });
    app.get('/decline', function (req, res) {
        var token = req.query.id;
        var lang = req.query.lng;
        var vm;
        logger.log(req, 'grey', 'Route', 'Canceling account ' + token);
        dbengine.users.declineLocalUserInvitation(token, function (err) {
            var redirect = function (url) {
                if (lang) {
                    url += '?lng=' + lang;
                }
                res.redirect(url);
            };
            if (err) {
                logger.logError(req, 'Route', err);
                vm = {
                    message: err.message,
                    type: 'error',
                    name: err.name
                };
            }
            else {
                logger.log(req, 'grey', 'Route', 'Account creation canceled ' + token);
                vm = {
                    name: 'msg-successdecline',
                    type: 'info',
                    message: 'Your account is successfully deleted.'
                };
            }
            req.flash('message', [vm]);
            redirect('/');
        });
    });
    app.get('/unlock', function (req, res) {
        var token = req.query.id;
        var lang = req.query.lng;
        logger.log(req, 'grey', 'Route', 'Unlocking account ' + token);
        dbengine.users.unlockLocalAccountFromToken(token, function (err, dbu) {
            var redirect = function (url) {
                if (lang) {
                    url += '?lng=' + lang;
                }
                res.redirect(url);
            };
            if (err) {
                logger.logError(req, 'Route', err);
                var ve = {
                    message: err.message,
                    type: 'error',
                    name: err.name
                };
                req.flash('message', [ve]);
                redirect('/');
            }
            else {
                logger.log(req, 'green', 'Route', 'Account unocked ' + dbu.userName || dbu.email);
                var vm = {
                    name: 'msg-successunlocked',
                    type: 'info',
                    message: 'Your account is unlocked, you can login now.'
                };
                req.flash('message', [vm]);
                redirect('/login');
            }
        });
    });
    app.use('/pwdreset', resetPassword);
    app.get('/auth/google', function (req, res, next) {
        req.session.returnTo = req.query.rl || '/';
        next();
    }, passport.authenticate('google', { scope: ['profile', 'email'] }));
    app.get('/auth/google/callback', passport.authenticate('google', {
        successReturnToOrRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true
    }));
    app.get('/auth/facebook', function (req, res, next) {
        req.session.returnTo = req.query.rl || '/';
        next();
    }, passport.authenticate('facebook', { scope: 'email' }));
    app.get('/auth/facebook/callback', passport.authenticate('facebook', {
        successReturnToOrRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true
    }));
    app.get('/auth/twitter', function (req, res, next) {
        req.session.returnTo = req.query.rl || '/';
        next();
    }, passport.authenticate('twitter', { scope: 'email' }));
    app.get('/auth/twitter/callback', passport.authenticate('twitter', {
        successReturnToOrRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true
    }));
    app.get('/auth/github', function (req, res, next) {
        req.session.returnTo = req.query.rl || '/';
        next();
    }, passport.authenticate('github'));
    app.get('/auth/github/callback', passport.authenticate('github', {
        successReturnToOrRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true
    }));
    app.use(express.static(config.path.app));
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.name = '404';
        var clientAnalysis = trafficAnalysis[req.ip];
        if (!clientAnalysis) {
            trafficAnalysis[req.ip] = clientAnalysis = {};
        }
        var now = (new Date()).getTime();
        if (now - clientAnalysis.lastNotFound < 1000) {
            clientAnalysis.notFoundRequest = clientAnalysis.notFoundRequest ? clientAnalysis.notFoundRequest + 1 : 1;
            if (clientAnalysis.notFoundRequest >= 5) {
                clientAnalysis.temporarilyBanned = now + 3600000;
            }
        }
        else {
            delete clientAnalysis.notFoundRequest;
        }
        clientAnalysis.lastNotFound = now;
        next(err);
    });
    function logErrors(err, req, res, next) {
        if (err.name === '404') {
            logger.log(req, 'yellow', 'Route', '404 for url ' + req.originalUrl);
            var options = {
                query: req.query,
                sessionID: req.session.id,
                user: req.user
            };
            logger.log(req, 'grey', 'Route', 'Navigate to notfound');
            res.render('notfound.html', options);
            return;
        }
        logger.log(req, 'red', 'Route', 'Express error ' + err.stack);
        next(err);
    }
    function clientErrorHandler(err, req, res, next) {
        if (req.xhr) {
            res.status(500).send('Server Error');
        }
        else {
            next(err);
        }
    }
    function errorHandler(err, req, res, next) {
        res.status(500);
        res.render('error', { error: err });
        var n = next;
    }
    app.use(logErrors);
    app.use(clientErrorHandler);
    app.use(errorHandler);
};
//# sourceMappingURL=routes.js.map
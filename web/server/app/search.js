'use strict';
var path = require('path');
var cacheProvider = require('../utils/cache');
var express = require('express');
var dbengine = require('../database/engine');
var translationrenderer = require('../utils/i18n/renderer');
var renderer = require('../utils/renderer');
var config = require('../../config');
var logger = require('../utils/logger');
var cache = cacheProvider.Cache;
var app = express();
app.get('/', function (req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        message: req.flash('message')
    };
    var fail = function (err) {
        logger.logError(req, 'Search', err);
        translationrenderer.renderjson(options, err, function (json) {
            res.status(500).send(json.message);
        });
    };
    var result = {};
    var resultTemplate;
    var nocache = req.query.reset !== undefined;
    logger.log(req, 'grey', 'Search', 'Searching for ' + req.url);
    var returnRightPage = function (searchResult) {
        searchResult.template = resultTemplate;
        var max = parseInt(req.query.max);
        if (isNaN(max)) {
            max = 50;
        }
        var page = parseInt(req.query.p);
        if (!isNaN(page)) {
            page--;
        }
        if (isNaN(page) || page < 0) {
            page = 0;
        }
        var index = page * max;
        searchResult.pageCount = Math.ceil(searchResult.tubes.length / max);
        searchResult.page = page + 1;
        searchResult.tubes = searchResult.tubes.slice(index, index + max);
        res.json(searchResult);
    };
    var tubesSearch = function () {
        if (!req.query.q) {
            res.json(result);
            return;
        }
        var cacheKey = options.sessionID + '_' + req.query.q;
        if (!nocache) {
            var cached = cache.get(cacheKey);
            if (cached) {
                result.tubes = cached;
                returnRightPage(result);
                logger.log(req, 'grey', 'Search', 'Search done from cache');
                return;
            }
        }
        dbengine.tubes.searchTubes(req.query.q, req.user, function (err, json, users) {
            if (err) {
                fail(err);
                return;
            }
            else {
                result.tubes = json;
                result.users = users;
                translationrenderer.renderjson(options, result, function (translated) {
                    cache.set(translated.tubes, cacheKey);
                    logger.log(req, 'white', 'Search', 'Search done from db');
                    returnRightPage(translated);
                });
            }
        });
    };
    var typesSearch = function () {
        if (req.query.lt !== undefined) {
            nocache = true;
            dbengine.tubeTypes.getTypes(null, function (err, json) {
                if (err) {
                    logger.log(req, 'red', 'Search', 'Get types error' + err.message);
                    result.typesError = err.message;
                }
                else {
                    result.types = json;
                }
                tubesSearch();
            });
        }
        else {
            tubesSearch();
        }
    };
    var pinoutsSearch = function () {
        if (req.query.lp !== undefined) {
            nocache = true;
            dbengine.tubePins.getPinouts(null, function (err, json) {
                if (err) {
                    logger.log(req, 'red', 'Search', 'Get pinouts error' + err.message);
                    result.pinoutsError = err.message;
                }
                else {
                    result.pinouts = json;
                }
                typesSearch();
            });
        }
        else {
            typesSearch();
        }
    };
    var basesSearch = function () {
        if (req.query.lb !== undefined) {
            nocache = true;
            dbengine.tubeBases.getBases(null, function (err, json) {
                if (err) {
                    logger.log(req, 'red', 'Search', 'Get bases error' + err.message);
                    result.basesError = err.message;
                }
                else {
                    result.bases = json;
                }
                pinoutsSearch();
            });
        }
        else {
            pinoutsSearch();
        }
    };
    if (req.query.q === 'news') {
        dbengine.news.getLatestNews(function (err, n) {
            if (err) {
                fail(err);
                return;
            }
            else if (!n || n.length === 0) {
                fail({
                    name: 'nonews',
                    message: 'No news'
                });
                return;
            }
            var news = n.slice(0, 50);
            translationrenderer.renderjson(options, news, function (json) {
                result = {
                    page: 1,
                    pageCount: 1,
                    news: json
                };
                res.json(result);
            });
        });
    }
    else {
        if (req.query.tmpl !== undefined) {
            nocache = true;
            var template = path.resolve(config.path.views, req.query.tmpl);
            renderer.render(template, options, function (err, html) {
                if (err) {
                    fail(err);
                    return;
                }
                else {
                    resultTemplate = html;
                    basesSearch();
                }
            });
        }
        else {
            basesSearch();
        }
    }
});
module.exports = app;
//# sourceMappingURL=search.js.map
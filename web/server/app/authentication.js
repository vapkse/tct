'use strict';
var config = require('../../config');
var dbengine = require('../database/engine');
var sessionProvider = require('../utils/session-provider');
var mail = require('../utils/mail-provider');
var bcrypt = require('bcryptjs');
var User = require('../../common/ts/user');
var logger = require('../utils/logger');
var mailProviderValidator = require('./mail-blacklist-validator');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var GitHubStrategy = require('passport-github').Strategy;
module.exports = function (passport, env) {
    var authConfig = config.auth[env];
    passport.serializeUser(function (user, done) {
        done(null, user);
    });
    passport.deserializeUser(function (user, done) {
        done(null, user);
    });
    passport.use('local-login', new LocalStrategy({
        usernameField: 'user_login',
        passwordField: 'user_password',
        passReqToCallback: true
    }, function (req, email, password, done) {
        process.nextTick(function () {
            var provider = 'local';
            var userEmail = {
                email: email,
                userName: email
            };
            logger.log(req, 'grey', 'Local login', 'User \\0 try to connect', email, password);
            dbengine.users.findLocalUser(userEmail, password, function (err, dbu, failAttempts, accountBlocked) {
                if (err) {
                    var next = function () {
                        var validationMessages = [];
                        validationMessages.push({
                            fieldName: '#user_login',
                            message: err.message,
                            tooltip: true,
                            type: 'error',
                            name: err.name
                        });
                        validationMessages.push({
                            fieldName: '#user_password',
                            message: err.message,
                            tooltip: true,
                            type: 'error',
                            name: err.name
                        });
                        logger.log(req, 'red', 'Local login', 'User \\0 and password \\1 rejected', email, password);
                        return done(null, null, req.flash('message', validationMessages), req.flash('provider', provider));
                    };
                    if (accountBlocked) {
                        var locale = sessionProvider.States.state(req.session.id).locale || 'en';
                        var mailInfos = {
                            locale: locale,
                            dest: dbu.email,
                            user: 'tubecurvetracer@gmail.com',
                            password: 'zestilegbcnrdzda',
                            object: {
                                uid: 'accountlocked',
                                text: 'Your account is locked'
                            },
                            body: {
                                uid: 'lockedmailbody',
                                text: 'Hello \\0 \\0 Because you have reached the limit of \\1 connexions with a wrong password, your account has been locked. \\0 You can click on the following link to unlock your account. \\0 \\2 \\0 \\0 \\0 If you forget your password, or want to change it, please visit the following link. \\0 \\3 \\0 \\0 Best Regards \\0 \\4'
                            },
                            bodyParams: {
                                0: '\n',
                                1: String(config.maxFailLoginAttempts),
                                2: authConfig.local.unlockUrl + dbu.validationToken + '&lng=' + locale,
                                3: authConfig.local.changePwdUrl + dbu.validationToken + '&lng=' + locale,
                                4: 'tct Team'
                            }
                        };
                        logger.log(req, 'grey', 'Local login', 'Account \\0 locked, send an email.', email);
                        mail.Provider.send(mailInfos, function (err) {
                            if (err) {
                                logger.logError(req, 'Local login', err);
                            }
                            next();
                        });
                    }
                    else {
                        next();
                    }
                }
                else {
                    logger.log(req, 'green', 'Local login', 'User \\0 connected', email);
                    dbu.lastLogin = (new Date()).getTime();
                    dbu.lastIP = req ? req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress : '';
                    dbengine.users.findOrCreateUser(dbu, function (err) {
                        if (err) {
                            logger.logError(req, 'Local login', err);
                        }
                    });
                    if (failAttempts) {
                        var failAttemptsMessage = {
                            message: 'Login successfull. \\0 attempt connections have failed before.',
                            type: 'warning',
                            name: 'msg-loginattemtps',
                            params: { 0: failAttempts }
                        };
                        return done(null, dbu, req.flash('message', [failAttemptsMessage]), req.flash('provider', provider));
                    }
                    else {
                        return done(null, dbu);
                    }
                }
            });
        });
    }));
    passport.use('local-signup', new LocalStrategy({
        usernameField: 'user_email',
        passwordField: 'user_password',
        passReqToCallback: true
    }, function (req, email, password, done) {
        var username = req.body.user_username;
        process.nextTick(function () {
            var provider = 'local';
            var userEmail = {
                email: email,
                userName: username
            };
            var fail = function (verrors) {
                logger.logErrors(req, 'Local signup', verrors);
                return done(null, null, req.flash('message', verrors));
            };
            mailProviderValidator.validate(email, function (mailerr) {
                if (mailerr) {
                    return fail([{
                            fieldName: '#user_email',
                            message: mailerr.message,
                            tooltip: true,
                            type: 'error',
                            name: mailerr.name
                        }]);
                }
                var userValidator = new User.Validator();
                userValidator.validateSignup(userEmail, password, password, function (verrors) {
                    if (verrors && verrors.length) {
                        return fail(verrors);
                    }
                    dbengine.users.checkLocalUser(userEmail, function (err) {
                        if (err) {
                            return fail([{
                                    fieldName: '#user_email',
                                    message: err.message,
                                    tooltip: true,
                                    type: 'error',
                                    name: err.name
                                }]);
                        }
                        var user = {
                            _id: provider + '-' + email,
                            displayName: username,
                            provider: provider,
                            email: email,
                            userName: username,
                            validated: false,
                            validationToken: bcrypt.genSaltSync(45) + 'av',
                            validationTime: (new Date()).getTime()
                        };
                        var locale = sessionProvider.States.state(req.session.id).locale || 'en';
                        var mailInfos = {
                            locale: locale,
                            dest: user.email,
                            user: 'tubecurvetracer@gmail.com',
                            password: 'zestilegbcnrdzda',
                            object: {
                                uid: 'newacccreation',
                                text: 'New account creation'
                            },
                            body: {
                                uid: 'confmailbody',
                                text: 'Hello \\0 \\0 An account has been created for you at \\1 . \\0 Please visit the following url to activate your account. \\0 \\2 \\0 \\0 \\0 If the account was created in error, you can visit the following link to decline this invitation. \\0 \\3 \\0 \\0 Best Regards \\0 \\4'
                            },
                            bodyParams: {
                                0: '\n',
                                1: 'tube curve tracer',
                                2: authConfig.local.validationUrl + user.validationToken + '&lng=' + locale,
                                3: authConfig.local.declineUrl + user.validationToken + '&lng=' + locale,
                                4: 'tct Team'
                            }
                        };
                        mail.Provider.send(mailInfos, function (err) {
                            if (err) {
                                return fail([{
                                        fieldName: '#user_email',
                                        message: err.message,
                                        tooltip: true,
                                        type: 'error',
                                        name: err.name
                                    }]);
                            }
                            logger.log(req, 'grey', 'Local signup', 'Adding user \\0 to db', email);
                            dbengine.users.addUser(user, password, function (err) {
                                if (err) {
                                    return fail([{
                                            fieldName: '#user_username',
                                            message: err.message,
                                            tooltip: true,
                                            type: 'error',
                                            name: err.name
                                        }]);
                                }
                                return fail([{
                                        message: 'A confirmation email has been sent. Please check your mail and click on the link on the sent email to validate the account creation.',
                                        type: 'info',
                                        name: 'msg-emailsent'
                                    }]);
                            });
                        });
                    });
                });
            });
        });
    }));
    passport.use(new GoogleStrategy({
        clientID: authConfig.google.id,
        clientSecret: authConfig.google.secret,
        callbackURL: authConfig.google.redirect,
        passReqToCallback: true
    }, function (req, token, refreshToken, profile, done) {
        process.nextTick(function () {
            var user;
            var provider = 'google';
            var userName = (profile.emails.length && profile.emails[0].value) || profile.displayName;
            logger.log(req, 'white', 'Login ' + provider, 'Connecting user \\0', userName);
            if (req.user) {
                if (req.user.provider !== provider) {
                    var validationMessage = {
                        message: 'You are already logged, please logout after login again.',
                        type: 'error',
                        name: 'msg-alreadylogged'
                    };
                    logger.logError(req, 'Login ' + provider, validationMessage);
                    return done(null, null, req.flash('message', [validationMessage]), req.flash('provider', provider));
                }
                user = req.user;
            }
            else {
                user = {
                    _id: provider + '-' + profile.id,
                    provider: provider,
                    validated: true
                };
            }
            user.token = token;
            user.displayName = profile.displayName;
            user.userName = profile.username;
            user.email = profile.emails && profile.emails.length && profile.emails[0].value;
            user.photo = profile.photos && profile.photos.length && profile.photos[0].value;
            user.lastLogin = (new Date()).getTime();
            user.lastIP = req ? req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress : '';
            logger.log(req, 'grey', 'Login ' + provider, 'Searching \\0 in db', userName);
            dbengine.users.findOrCreateUser(user, function (err, dbu) {
                if (err) {
                    logger.log(req, 'red', 'Login ' + provider, 'User \\0 in error \\1', userName, err.message);
                    return done(null, null, req.flash('message', [err.message]));
                }
                logger.log(req, 'green', 'Login ' + provider, 'User \\0 connected', userName);
                user.role = dbu.role;
                return done(null, user);
            });
        });
    }));
    passport.use(new FacebookStrategy({
        clientID: authConfig.facebook.id,
        clientSecret: authConfig.facebook.secret,
        callbackURL: authConfig.facebook.redirect,
        passReqToCallback: true
    }, function (req, token, refreshToken, profile, done) {
        process.nextTick(function () {
            var user;
            var provider = 'facebook';
            var userName = (profile.name.givenName + profile.name.familyName) || profile.displayName;
            logger.log(req, 'white', 'Login ' + provider, 'Connecting user \\0', userName);
            if (req.user) {
                if (req.user.provider !== provider) {
                    var validationMessage = {
                        message: 'You are already logged, please logout after login again.',
                        type: 'error',
                        name: 'msg-alreadylogged'
                    };
                    logger.logError(req, 'Login ' + provider, validationMessage);
                    return done(null, null, req.flash('message', [validationMessage]), req.flash('provider', provider));
                }
                user = req.user;
            }
            else {
                user = {
                    _id: provider + '-' + profile.id,
                    provider: provider,
                    validated: true
                };
            }
            user.token = token;
            user.displayName = profile.displayName;
            user.userName = profile.username;
            user.email = profile.emails && profile.emails.length && profile.emails[0].value;
            user.lastLogin = (new Date()).getTime();
            user.lastIP = req ? req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress : '';
            logger.log(req, 'grey', 'Login ' + provider, 'Searching \\0 in db', userName);
            dbengine.users.findOrCreateUser(user, function (err, dbu) {
                if (err) {
                    logger.log(req, 'red', 'Login ' + provider, 'User \\0 in error \\1', userName, err.message);
                    return done(null, null, req.flash('message', [err.message]));
                }
                logger.log(req, 'green', 'Login ' + provider, 'User \\0 connected', userName);
                user.role = dbu.role;
                return done(null, user);
            });
        });
    }));
    passport.use(new TwitterStrategy({
        consumerKey: authConfig.twitter.consumerKey,
        consumerSecret: authConfig.twitter.consumerSecret,
        callbackURL: authConfig.twitter.redirect,
        passReqToCallback: true
    }, function (req, token, refreshToken, profile, done) {
        process.nextTick(function () {
            var user;
            var provider = 'tweeter';
            var userName = (user && user.userName) || req.session.id;
            logger.log(req, 'white', 'Login ' + provider, 'Connecting user \\0', userName);
            if (req.user) {
                if (req.user.provider !== provider) {
                    var validationMessage = {
                        message: 'You are already logged, please logout after login again.',
                        type: 'error',
                        name: 'msg-alreadylogged'
                    };
                    logger.logError(req, 'Login ' + provider, validationMessage);
                    return done(null, null, req.flash('message', [validationMessage]), req.flash('provider', provider));
                }
                user = req.user;
            }
            else {
                user = {
                    _id: provider + '-' + profile.id,
                    provider: provider,
                    validated: true
                };
            }
            user.token = token;
            user.displayName = profile.displayName;
            user.userName = profile.username;
            user.photo = profile.photos && profile.photos.length && profile.photos[0].value;
            user.email = profile.emails && profile.emails.length && profile.emails[0].value;
            user.lastLogin = (new Date()).getTime();
            user.lastIP = req ? req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress : '';
            logger.log(req, 'grey', 'Login ' + provider, 'Searching \\0 in db', userName);
            dbengine.users.findOrCreateUser(user, function (err, dbu) {
                if (err) {
                    logger.log(req, 'red', 'Login ' + provider, 'User \\0 in error \\1', userName, err.message);
                    return done(null, null, req.flash('message', [err.message]), req.flash('provider', provider));
                }
                logger.log(req, 'green', 'Login ' + provider, 'User \\0 connected', userName);
                user.role = dbu.role;
                return done(null, user);
            });
        });
    }));
    passport.use(new GitHubStrategy({
        clientID: authConfig.github.id,
        clientSecret: authConfig.github.secret,
        callbackURL: authConfig.github.redirect,
        passReqToCallback: true
    }, function (req, token, refreshToken, profile, done) {
        process.nextTick(function () {
            var user;
            var provider = 'github';
            var userName = (user && user.userName) || req.session.id;
            logger.log(req, 'white', 'Login ' + provider, 'Connecting user \\0', userName);
            if (req.user) {
                if (req.user.provider !== provider) {
                    var validationMessage = {
                        message: 'You are already logged, please logout after login again.',
                        type: 'error',
                        name: 'msg-alreadylogged'
                    };
                    logger.logError(req, 'Login ' + provider, validationMessage);
                    return done(null, null, req.flash('message', [validationMessage]), req.flash('provider', provider));
                }
                user = req.user;
            }
            else {
                user = {
                    _id: provider + '-' + profile.id,
                    provider: provider,
                    validated: true
                };
            }
            user.token = token;
            user.displayName = profile.displayName;
            user.userName = profile.username;
            user.email = profile.emails && profile.emails.length && profile.emails[0].value;
            user.lastLogin = (new Date()).getTime();
            user.lastIP = req ? req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress : '';
            logger.log(req, 'grey', 'Login ' + provider, 'Searching \\0 in db', userName);
            dbengine.users.findOrCreateUser(user, function (err, dbu) {
                if (err) {
                    logger.log(req, 'red', 'Login ' + provider, 'User \\0 in error \\1', userName, err.message);
                    return done(null, null, req.flash('message', [err.message]), req.flash('provider', provider));
                }
                logger.log(req, 'green', 'Login ' + provider, 'User \\0 connected', userName);
                user.role = dbu.role;
                return done(null, user);
            });
        });
    }));
};
//# sourceMappingURL=authentication.js.map
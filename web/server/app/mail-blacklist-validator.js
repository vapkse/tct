'use strict';
var config = require('../../config');
var path = require('path');
var fs = require('fs');
var split = require('split');
var logger = require('../utils/logger');
var list = [];
setTimeout(function () {
    logger.log('System', 'yellow', 'mailValidator', 'Read mail blacklist');
    fs.createReadStream(path.resolve(config.path.json, 'disposable-email-providers-domains', 'disposable-email-provider-domains'))
        .pipe(split())
        .on('data', function (line) {
        list.push(line);
    }).on('error', function (error) {
        logger.logError('System', 'mailValidator', error);
    }).on('close', function () {
        logger.log('System', 'green', 'mailValidator', 'Mail blacklist parsed, \\0 entries found.', String(list.length));
    });
}, 5000);
var mailValidator = {
    validate: function (email, callback) {
        var match = /^.*@(.*)$/.exec(email);
        if (!match || match.length < 2) {
            callback({
                name: 'err-invalidmail',
                message: 'Invalid mail.'
            });
            logger.log('System', 'red', 'mailValidator', 'Invalid email \\0 .', email);
            return;
        }
        var re = new RegExp('^' + match[1] + '$', 'i');
        for (var i = 0; i < list.length; i++) {
            if (re.test(list[i])) {
                callback({
                    name: 'err-invalidprovider',
                    message: 'Disposal providers are not allowed for this service. Please give another email.'
                });
                logger.log('System', 'red', 'mailValidator', 'Invalid email provider \\0 .', email);
                return;
            }
        }
        callback();
    }
};
module.exports = mailValidator;
//# sourceMappingURL=mail-blacklist-validator.js.map
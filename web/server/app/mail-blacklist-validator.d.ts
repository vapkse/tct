declare var mailValidator: {
    validate: (email: string, callback: (err?: Error) => void) => void;
};
export = mailValidator;

'use strict';

import path = require('path');
import express = require('express');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');
import dbengine = require('../database/engine');
import translationrenderer = require('../utils/i18n/renderer');
import renderer = require('../utils/renderer');
import cfg = require('../../config');
import logger = require('../utils/logger');
import userns = require('../../common/ts/user');
import sessionProvider = require('../utils/session-provider');
import mail = require('../utils/mail-provider');

var app = express();

// Config
var extend = require('extend');
/*jshint -W069 */
var env = (<any>app.settings)['env'] || 'prod';
var config = extend({}, cfg, (<any>cfg)[env]);
var authConfig = (<any>config).auth[env];

app.set('views', config.path.views);

app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

// Reset password
app.get('/', function(req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        user: req.user,
        message: req.flash('message')
    }

    if (req.query.id) {
        var token = req.query.id;

        // Change password request from mail
        if (req.query.cancel) {
            // Cancel request
            var vm: validation.Message;
            logger.log(req, 'grey', 'ResetPassword', 'Canceling reset password');
            dbengine.users.cancelResetPassword(token, function(err) {
                if (err) {
                    logger.logError(req, 'ResetPassword', err);
                    vm = {
                        message: err.message,
                        type: 'error',
                        name: err.name
                    }
                } else {
                    logger.log(req, 'grey', 'ResetPassword', 'Reset password canceled');
                    vm = {
                        name: 'msg-successreset',
                        type: 'info',
                        message: 'Your request is successfully canceled.'
                    }
                }

                req.flash('message', [vm]);
                res.redirect('/');
            })
        } else {
            logger.log(req, 'grey', 'ResetPassword', 'Navigate to changepwd');
            res.render('login.html', options);
        }
    } else {
        // Ask form
        logger.log(req, 'grey', 'ResetPassword', 'Navigate to reset password');
        res.render('login.html', options);
    }
})

app.post('/', function(req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        user: req.user,
    }

    var fail = function(verrors: Array<validation.Message>) {
        logger.logErrors(req, 'ResetPassword', verrors);
        translationrenderer.renderjson(options, { 'messages': verrors }, function(json: any) {
            res.json(json);
        })
    }

    var validator = new userns.Validator();

    if (req.query.id) {
        // Password change
        var password = req.body.user_password;
        // Valid password
        logger.log(req, 'yellow', 'ResetPassword', 'Changing password');
        validator.validatePasswordChange(password, password, function(verrors) {
            if (verrors && verrors.length) {
                fail(verrors);
            } else {
                dbengine.users.changePasswordFromToken(req.query.id, password, function(err: Error) {
                    if (err) {
                        return fail([{
                            fieldName: '#user_password',
                            message: err.message,
                            tooltip: true,
                            type: 'error',
                            name: err.name
                        }]);
                    }

                    // Because the login can be done at this time, we always fail 
                    return fail([{
                        message: 'Your password is changed successfully.',
                        type: 'info',
                        name: 'msg-changepwdsuccess'
                    }]);
                })
            }
        })

    } else { 
        // Password reset request
        var email = req.body.user_email;

        // Valid email
        logger.log(req, 'cyan', 'ResetPassword', 'Changing password request');
        validator.validatePasswordReset(email, function(verrors) {
            if (verrors && verrors.length) {
                fail(verrors);
            } else {
                // Check in db if user exists and get a token in case of
                dbengine.users.getResetPasswordToken(email, function(err: Error, user: userns.DbUser) {
                    if (err) {
                        return fail([{
                            fieldName: '#user_email',
                            message: err.message,
                            tooltip: true,
                            type: 'error',
                            name: err.name
                        }]);
                    }
                
                    // Send email for password reset
                    var locale = sessionProvider.States.state(req.session.id).locale || 'en';
                    var mailInfos: mail.MailInfos = {
                        locale: locale,
                        dest: email,
                        user: 'tubecurvetracer@gmail.com',
                        password: 'zestilegbcnrdzda',
                        object: {
                            uid: 'resetpwd',
                            text: 'Reset password'
                        },
                        body: {
                            uid: 'resetmailbody',
                            text: 'Hello \\0 \\0 A password change is requested. \\0 Please visit the following url to reset your password. \\0 \\2 \\0 \\0 \\0 If the change was requested in error, you can visit the following link to cancel this request. \\0 \\3 \\0 \\0 Best Regards \\0 \\4'
                        },
                        bodyParams: {
                            0: '\n',
                            1: 'tube curve tracer',
                            2: authConfig.local.changePwdUrl + user.validationToken + '&lng=' + locale,
                            3: authConfig.local.changePwdUrl + user.validationToken + '&lng=' + locale + '&cancel=true',
                            4: 'tct Team'
                        }
                    }

                    // Create and add new user
                    mail.Provider.send(mailInfos, function(err: Error) {
                        if (err) {
                            return fail([{
                                fieldName: '#user_email',
                                message: err.message,
                                tooltip: true,
                                type: 'error',
                                name: err.name
                            }]);
                        }

                        // Because the login can be done at this time, we always fail 
                        return fail([{
                            message: 'A confirmation email has been sent. Please check your mail and click on the link to validate the password reset request.',
                            type: 'info',
                            name: 'msg-resetemailsent'
                        }]);
                    });
                })
            }
        })
    }
});

export = app;
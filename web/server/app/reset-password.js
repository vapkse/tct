'use strict';
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var dbengine = require('../database/engine');
var translationrenderer = require('../utils/i18n/renderer');
var cfg = require('../../config');
var logger = require('../utils/logger');
var userns = require('../../common/ts/user');
var sessionProvider = require('../utils/session-provider');
var mail = require('../utils/mail-provider');
var app = express();
var extend = require('extend');
var env = app.settings['env'] || 'prod';
var config = extend({}, cfg, cfg[env]);
var authConfig = config.auth[env];
app.set('views', config.path.views);
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.get('/', function (req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        user: req.user,
        message: req.flash('message')
    };
    if (req.query.id) {
        var token = req.query.id;
        if (req.query.cancel) {
            var vm;
            logger.log(req, 'grey', 'ResetPassword', 'Canceling reset password');
            dbengine.users.cancelResetPassword(token, function (err) {
                if (err) {
                    logger.logError(req, 'ResetPassword', err);
                    vm = {
                        message: err.message,
                        type: 'error',
                        name: err.name
                    };
                }
                else {
                    logger.log(req, 'grey', 'ResetPassword', 'Reset password canceled');
                    vm = {
                        name: 'msg-successreset',
                        type: 'info',
                        message: 'Your request is successfully canceled.'
                    };
                }
                req.flash('message', [vm]);
                res.redirect('/');
            });
        }
        else {
            logger.log(req, 'grey', 'ResetPassword', 'Navigate to changepwd');
            res.render('login.html', options);
        }
    }
    else {
        logger.log(req, 'grey', 'ResetPassword', 'Navigate to reset password');
        res.render('login.html', options);
    }
});
app.post('/', function (req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        user: req.user,
    };
    var fail = function (verrors) {
        logger.logErrors(req, 'ResetPassword', verrors);
        translationrenderer.renderjson(options, { 'messages': verrors }, function (json) {
            res.json(json);
        });
    };
    var validator = new userns.Validator();
    if (req.query.id) {
        var password = req.body.user_password;
        logger.log(req, 'yellow', 'ResetPassword', 'Changing password');
        validator.validatePasswordChange(password, password, function (verrors) {
            if (verrors && verrors.length) {
                fail(verrors);
            }
            else {
                dbengine.users.changePasswordFromToken(req.query.id, password, function (err) {
                    if (err) {
                        return fail([{
                                fieldName: '#user_password',
                                message: err.message,
                                tooltip: true,
                                type: 'error',
                                name: err.name
                            }]);
                    }
                    return fail([{
                            message: 'Your password is changed successfully.',
                            type: 'info',
                            name: 'msg-changepwdsuccess'
                        }]);
                });
            }
        });
    }
    else {
        var email = req.body.user_email;
        logger.log(req, 'cyan', 'ResetPassword', 'Changing password request');
        validator.validatePasswordReset(email, function (verrors) {
            if (verrors && verrors.length) {
                fail(verrors);
            }
            else {
                dbengine.users.getResetPasswordToken(email, function (err, user) {
                    if (err) {
                        return fail([{
                                fieldName: '#user_email',
                                message: err.message,
                                tooltip: true,
                                type: 'error',
                                name: err.name
                            }]);
                    }
                    var locale = sessionProvider.States.state(req.session.id).locale || 'en';
                    var mailInfos = {
                        locale: locale,
                        dest: email,
                        user: 'tubecurvetracer@gmail.com',
                        password: 'zestilegbcnrdzda',
                        object: {
                            uid: 'resetpwd',
                            text: 'Reset password'
                        },
                        body: {
                            uid: 'resetmailbody',
                            text: 'Hello \\0 \\0 A password change is requested. \\0 Please visit the following url to reset your password. \\0 \\2 \\0 \\0 \\0 If the change was requested in error, you can visit the following link to cancel this request. \\0 \\3 \\0 \\0 Best Regards \\0 \\4'
                        },
                        bodyParams: {
                            0: '\n',
                            1: 'tube curve tracer',
                            2: authConfig.local.changePwdUrl + user.validationToken + '&lng=' + locale,
                            3: authConfig.local.changePwdUrl + user.validationToken + '&lng=' + locale + '&cancel=true',
                            4: 'tct Team'
                        }
                    };
                    mail.Provider.send(mailInfos, function (err) {
                        if (err) {
                            return fail([{
                                    fieldName: '#user_email',
                                    message: err.message,
                                    tooltip: true,
                                    type: 'error',
                                    name: err.name
                                }]);
                        }
                        return fail([{
                                message: 'A confirmation email has been sent. Please check your mail and click on the link to validate the password reset request.',
                                type: 'info',
                                name: 'msg-resetemailsent'
                            }]);
                    });
                });
            }
        });
    }
});
module.exports = app;
//# sourceMappingURL=reset-password.js.map
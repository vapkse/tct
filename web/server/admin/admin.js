'use strict';
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var path = require('path');
var fs = require('fs');
var dbengine = require('../database/engine');
var config = require('../../config');
var logger = require('../utils/logger');
var translationrenderer = require('../utils/i18n/renderer');
var translatorEngine = require('../utils/i18n/translator');
var app = express();
var documentsCache;
app.set('views', config.path.views);
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.get('/', function (req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        user: req.user,
        errors: req.flash('error'),
        warnings: req.flash('warning'),
        infos: req.flash('info')
    };
    logger.log(req, 'grey', 'Route', 'Navigate to admin');
    res.render('admin.html', options);
});
app.post('/', function (req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
    };
    var returnError = function (error) {
        translationrenderer.renderjson(options, error, function (json) {
            logger.logError(req, 'Editor', json);
            res.status(500).send(JSON.stringify(json));
        });
    };
    var getFileSize = function (filename) {
        var stats = fs.statSync(filename);
        var fileSizeInBytes = stats.size;
        return fileSizeInBytes;
    };
    var getDocumentsList = function (cb) {
        fs.readdir(path.resolve(config.path.documents), function (err, items) {
            if (err) {
                cb(err);
            }
            else {
                var docs = [];
                var dict = {};
                items.forEach(function (f) {
                    var d = {
                        name: f,
                        size: getFileSize(path.resolve(config.path.documents, f)),
                        linked: false,
                        linkedTo: [],
                        missing: false,
                    };
                    docs.push(d);
                    dict[d.name.toLowerCase()] = d;
                });
                fs.readdir(path.resolve(config.path.data), function (err, items) {
                    if (err) {
                        cb(err);
                    }
                    else {
                        var checkTable = function (index) {
                            if (index >= items.length) {
                                cb(null, docs);
                                return;
                            }
                            var tableName = items[index];
                            if (/.*tubes\.json$/.test(tableName)) {
                                dbengine.tubes.getTubeDataFromCollection(tableName, function (err, tubesData) {
                                    if (err) {
                                        cb(err);
                                        return;
                                    }
                                    tubesData.forEach(function (tubeData) {
                                        if (tubeData.documents) {
                                            tubeData.documents.forEach(function (doc) {
                                                var key = doc.filename.toLowerCase();
                                                var d = dict[key];
                                                if (!d) {
                                                    logger.log(req, 'red', 'Admin', 'Missing documents linked in table ' + tableName + ' : ' + doc.filename);
                                                    docs.push({
                                                        name: doc.filename,
                                                        size: 0,
                                                        linked: true,
                                                        linkedTo: [tableName],
                                                        missing: true,
                                                    });
                                                }
                                                else {
                                                    d.linked = true;
                                                    d.linkedTo.push(tableName);
                                                }
                                            });
                                        }
                                    });
                                    checkTable(index + 1);
                                });
                            }
                            else {
                                checkTable(index + 1);
                            }
                        };
                        checkTable(0);
                    }
                });
            }
        });
    };
    var findDocumentInfos = function (name, cb) {
        var next = function (docs) {
            if (docs.length > 1) {
                cb({
                    name: 'msg-unknownerror',
                    message: 'Unknown error.'
                });
                return;
            }
            if (docs.length === 0) {
                cb({
                    name: 'msg-filenotfound',
                    message: 'File not found.'
                });
                return;
            }
            cb(null, docs[0]);
        };
        if (documentsCache) {
            next(documentsCache.filter(function (d) { return d.name === name; }));
        }
        else {
            getDocumentsList(function (err, docs) {
                if (err) {
                    cb(err);
                }
                else {
                    documentsCache = docs;
                    next(documentsCache.filter(function (d) { return d.name === name; }));
                }
            });
        }
    };
    if (req.query.d === 'users') {
        logger.log(req, 'yellow', 'Admin', 'Get users list');
        dbengine.users.getAllUsers(function (err, users) {
            if (err) {
                returnError(err);
            }
            else {
                res.json({
                    data: users
                });
            }
        });
    }
    else if (req.query.d === 'user-role') {
        logger.log(req, 'yellow', 'Admin', 'Changing role of ' + req.query.id + ' to ' + req.query.r);
        dbengine.users.updateUser({
            _id: req.query.id,
            role: req.query.r
        }, function (err, user) {
            if (err) {
                returnError(err);
            }
            else {
                res.json(user);
            }
        });
    }
    else if (req.query.d === 'user-block' || req.query.d === 'user-unblock') {
        var block = req.query.d === 'user-block';
        logger.log(req, 'yellow', 'Admin', (block ? 'Block' : 'Unblock') + ' account ' + req.query.id);
        dbengine.users.updateUser({
            _id: req.query.id,
            blocked: block
        }, function (err, user) {
            if (err) {
                returnError(err);
            }
            else {
                res.json(user);
            }
        });
    }
    else if (req.query.d === 'user-validate') {
        logger.log(req, 'yellow', 'Admin', 'Validate account ' + req.query.id);
        dbengine.users.updateUser({
            _id: req.query.id,
            validated: true
        }, function (err, user) {
            if (err) {
                returnError(err);
            }
            else {
                res.json(user);
            }
        });
    }
    else if (req.query.d === 'user-delete') {
        logger.log(req, 'yellow', 'Admin', 'Deleting account ' + req.query.id);
        dbengine.users.deleteUser(req.query.id, function (err) {
            if (err) {
                returnError(err);
            }
            else {
                logger.log(req, 'green', 'Admin', 'User deleted successfully');
                res.status(200).end();
            }
        });
    }
    else if (req.query.d === 'tables') {
        logger.log(req, 'yellow', 'Admin', 'Get tables list');
        dbengine.versions.getVersions(function (err, versions) {
            if (err) {
                logger.logError('System', 'Admin', err);
            }
            var vers = {};
            versions.map(function (v) { return vers[v.fileName] = v; });
            fs.readdir(path.resolve(config.path.data), function (err, items) {
                if (err) {
                    returnError(err);
                }
                else {
                    var tables = [];
                    items.map(function (f) { return tables.push({
                        name: f,
                        size: getFileSize(path.resolve(config.path.data, f)),
                        version: {
                            major: (vers[f] && vers[f].version.major) || 1,
                            minor: (vers[f] && vers[f].version.minor) || 0
                        }
                    }); });
                    res.json({
                        data: tables
                    });
                }
            });
        });
    }
    else if (req.query.d === 'table-compress') {
        var colname = req.query.name;
        logger.log(req, 'yellow', 'Admin', 'Compressing database ' + colname);
        dbengine.compressDB(colname, function (err) {
            if (err) {
                returnError(err);
            }
            else {
                logger.log(req, 'green', 'Admin', 'Database compressed successfully');
                res.json({
                    name: colname,
                    size: getFileSize(path.resolve(config.path.data, colname))
                });
            }
        });
    }
    else if (req.query.d === 'news') {
        logger.log(req, 'yellow', 'Admin', 'Get news list');
        dbengine.news.getLatestNews(function (err, news) {
            if (err) {
                returnError(err);
            }
            else {
                res.json({
                    data: news
                });
            }
        });
    }
    else if (req.query.d === 'news-delete') {
        logger.log(req, 'yellow', 'Admin', 'Deleting news ' + req.query.id);
        dbengine.news.deleteNews(req.query.id, function (err) {
            if (err) {
                returnError(err);
            }
            else {
                logger.log(req, 'green', 'Admin', 'News deleted successfully');
                res.status(200).end();
            }
        });
    }
    else if (req.query.d === 'news-edit') {
        if (!req.query.q) {
            returnError({
                name: 'msg-missingparam',
                message: 'Missing parameter'
            });
            return;
        }
        logger.log(req, 'yellow', 'Admin', 'Saving news ' + req.query.q);
        var news = JSON.parse(req.query.q);
        if (!news.date) {
            returnError({
                type: 'error',
                name: 'msg-mand',
                message: 'The field \\0 is mandatory.',
                fieldName: 'date',
                params: { 0: 'date' }
            });
            return;
        }
        if (!news.text || !news.text.trim()) {
            returnError({
                type: 'error',
                name: 'msg-mand',
                message: 'The field \\0 is mandatory.',
                fieldName: 'text',
                params: { 0: 'text' }
            });
            return;
        }
        if (!news._id) {
            dbengine.news.addNews(news, function (err, result) {
                if (err) {
                    returnError(err);
                }
                else {
                    logger.log(req, 'green', 'Admin', 'News saved successfully');
                    res.json(result);
                }
            });
        }
        else {
            dbengine.news.updateNews(news, function (err, result) {
                if (err) {
                    returnError(err);
                }
                else {
                    logger.log(req, 'green', 'Admin', 'News saved successfully');
                    res.json(result);
                }
            });
        }
    }
    else if (req.query.d === 'translations') {
        var selected = req.query.sl;
        var current = req.query.cl;
        logger.log(req, 'yellow', 'Admin', 'Get translations list for en');
        var result = {};
        var done = function () {
            var r = [];
            Object.keys(result).map(function (key) { return r.push(result[key]); });
            res.json({
                data: r
            });
        };
        var getCurrent = function () {
            if (current && selected && current !== selected) {
                var translations = {};
                Object.keys(result).forEach(function (key) {
                    if (result[key].selected) {
                        translations[key] = result[key].selected;
                    }
                });
                var requestParam = {
                    from: selected,
                    to: current,
                    translations: translations,
                    nocache: true
                };
                translatorEngine.getTranslations(requestParam, function (trans) {
                    if (trans.translations) {
                        Object.keys(result).map(function (key) { return result[key].current = trans.translations[key]; });
                    }
                    done();
                });
            }
            else {
                done();
            }
        };
        var getSelected = function () {
            if (selected && selected !== 'en') {
                dbengine.translations.getTranslations(selected, function (err, tdatas) {
                    if (err) {
                        returnError(err);
                    }
                    else {
                        tdatas.forEach(function (element) {
                            var tr = result[element._id];
                            if (!tr) {
                                tr = result[element._id] = {
                                    _id: element._id,
                                    value: '',
                                    current: ''
                                };
                            }
                            tr.selected = element.value;
                            tr.isBest = element.isBest;
                            if (element.version) {
                                tr.version = element.version;
                            }
                        });
                        getCurrent();
                    }
                });
            }
            else {
                getCurrent();
            }
        };
        dbengine.translations.getTranslations('en', function (err, tdatas) {
            if (err) {
                returnError(err);
            }
            else {
                tdatas.forEach(function (element) {
                    result[element._id] = {
                        _id: element._id,
                        value: element.value,
                        version: element.version,
                        selected: '',
                        current: ''
                    };
                });
                getSelected();
            }
        });
    }
    else if (req.query.d === 'trans-delete') {
        logger.log(req, 'yellow', 'Admin', 'Deleting translations ' + req.query.ids);
        if (!req.query.ids) {
            returnError({
                name: 'msg-missingparam',
                message: 'Missing parameter'
            });
            return;
        }
        var ids = JSON.parse(req.query.ids);
        dbengine.translations.deleteTranslations(ids, function (err) {
            if (err) {
                returnError(err);
            }
            else {
                res.json(ids);
            }
        });
    }
    else if (req.query.d === 'trans-edit') {
        logger.log(req, 'yellow', 'Admin', 'Editing translations ' + req.query.q + ' sl:' + req.query.sl + ' newid:' + req.query.newid);
        if (!req.query.q || !req.query.sl) {
            returnError({
                name: 'msg-missingparam',
                message: 'Missing parameter'
            });
            return;
        }
        var editedRecord = JSON.parse(req.query.q);
        dbengine.translations.editTranslations(editedRecord, req.query.sl, req.query.newid, function (err, tr) {
            if (err) {
                returnError(err);
            }
            else {
                res.json(tr);
            }
        });
    }
    else if (req.query.d === 'besttrans') {
        var current2 = req.query.cl;
        logger.log(req, 'yellow', 'Admin', 'Get Proposal list');
        var proposals = [];
        dbengine.translations.getProposals(function (err, pdatas) {
            if (err) {
                returnError(err);
            }
            else {
                proposals = pdatas;
                if (current2) {
                    var translateCurrent = function (index) {
                        if (index >= proposals.length) {
                            res.json({
                                data: proposals
                            });
                            return;
                        }
                        var proposal = proposals[index];
                        if (!proposal.proposal || proposal.locale === current2) {
                            translateCurrent(index + 1);
                            return;
                        }
                        var requestParam = {
                            from: proposal.locale,
                            to: current2,
                            translations: {},
                            nocache: true
                        };
                        requestParam.translations[proposal.uid] = proposal.proposal;
                        translatorEngine.getTranslations(requestParam, function (trans) {
                            if (trans.translations) {
                                proposal.current = trans.translations[proposal.uid];
                            }
                            translateCurrent(index + 1);
                        });
                    };
                    translateCurrent(0);
                }
                else {
                    res.json({
                        data: proposals
                    });
                }
            }
        });
    }
    else if (req.query.d === 'besttrans-delete') {
        logger.log(req, 'yellow', 'Admin', 'Deleting proposal ' + req.query.ids);
        if (!req.query.ids) {
            returnError({
                name: 'msg-missingparam',
                message: 'Missing parameter'
            });
            return;
        }
        var ids2 = JSON.parse(req.query.ids);
        dbengine.translations.deleteProposals(ids2, function (err) {
            if (err) {
                returnError(err);
            }
            else {
                res.json(ids2);
            }
        });
    }
    else if (req.query.d === 'besttrans-merge') {
        logger.log(req, 'yellow', 'Admin', 'Merging proposal ' + req.query.q);
        if (!req.query.q) {
            returnError({
                name: 'msg-missingparam',
                message: 'Missing parameter'
            });
            return;
        }
        var proposalData = JSON.parse(req.query.q);
        dbengine.translations.mergeProposal(proposalData, function (err, pod) {
            if (err) {
                returnError(err);
            }
            else {
                res.json(pod);
            }
        });
    }
    else if (req.query.d === 'docs') {
        logger.log(req, 'yellow', 'Admin', 'Get documents list');
        getDocumentsList(function (err, docs) {
            if (err) {
                returnError(err);
            }
            else {
                documentsCache = docs;
                res.json({
                    data: docs
                });
            }
        });
    }
    else if (req.query.d === 'docs-delete') {
        var name = req.query.name;
        if (!name) {
            returnError({
                name: 'msg-missingparam',
                message: 'Missing parameter'
            });
            return;
        }
        logger.log(req, 'yellow', 'Admin', 'Deleting document ' + name);
        findDocumentInfos(name, function (err, doc) {
            if (err) {
                returnError(err);
                return;
            }
            if (doc.linked) {
                returnError({
                    name: 'msg-linkeddoc',
                    message: 'The document is linked.'
                });
                return;
            }
            fs.unlink(path.resolve(config.path.documents, doc.name), function (err) {
                if (err) {
                    returnError(err);
                }
                else {
                    res.json(doc);
                }
            });
        });
    }
    else if (req.query.d === 'docs-rename') {
        var srcName = req.query.src;
        var dstName = req.query.dst;
        if (!srcName || !dstName) {
            returnError({
                name: 'msg-missingparam',
                message: 'Missing parameter'
            });
            return;
        }
        if (srcName === dstName) {
            res.status(200).end();
            return;
        }
        if (/[\/:*?"<>|]/gi.test(dstName)) {
            returnError({
                name: 'msg-invalidchar',
                message: 'Invalid character in destination name.'
            });
            return;
        }
        var srcPath = path.resolve(config.path.documents, srcName);
        var dstPath = path.resolve(config.path.documents, dstName);
        if (fs.existsSync(dstPath)) {
            returnError({
                name: 'msg-fileexists',
                message: 'Destination file already exists.'
            });
            return;
        }
        logger.log(req, 'yellow', 'Admin', 'Renaming document ' + srcName + ' to ' + dstName);
        findDocumentInfos(srcName, function (err, doc) {
            if (err) {
                returnError(err);
                return;
            }
            logger.log(req, 'grey', 'Admin', 'Reading source document.');
            fs.readFile(srcPath, 'utf8', function (err, data) {
                if (err) {
                    returnError(err);
                    return;
                }
                logger.log(req, 'grey', 'Admin', 'Writing destination document.');
                fs.writeFile(dstPath, data, function (err) {
                    if (err) {
                        returnError(err);
                        return;
                    }
                    var deleteOldDocument = function () {
                        logger.log(req, 'grey', 'Admin', 'Deleting source document.');
                        fs.unlink(srcPath, function (err) {
                            if (err) {
                                returnError(err);
                                return;
                            }
                            doc.name = dstName;
                            logger.log(req, 'green', 'Admin', 'Document renamed successfully.');
                            res.json(doc);
                        });
                    };
                    var renameDocument = function (index) {
                        if (index >= doc.linkedTo.length) {
                            deleteOldDocument();
                            return;
                        }
                        var tableName = doc.linkedTo[index];
                        logger.log(req, 'grey', 'Admin', 'Renaming document in table ' + tableName);
                        dbengine.tubes.renameDocumentInCollection(tableName, srcName, dstName, function (err) {
                            if (err) {
                                returnError(err);
                                return;
                            }
                            renameDocument(index + 1);
                        });
                    };
                    renameDocument(0);
                });
            });
        });
    }
    else if (req.query.d === 'tubes') {
        logger.log(req, 'yellow', 'Admin', 'Get tubes list');
        dbengine.tubes.getTubeDataFromCollection('tubes.json', function (err, result) {
            if (err) {
                returnError(err);
            }
            else {
                res.json({
                    data: result
                });
            }
        });
    }
    else if (req.query.d === 'tube-delete') {
        logger.log(req, 'yellow', 'Admin', 'Deleting tube ' + req.query.id);
        dbengine.tubes.deleteGlobalTube(req.query.id, function (err) {
            if (err) {
                returnError(err);
            }
            else {
                logger.log(req, 'green', 'Admin', 'Tube deleted successfully');
                res.status(200).end();
            }
        });
    }
    else {
        returnError({
            name: 'wrongparams',
            message: 'Wrong Parameters.'
        });
    }
});
module.exports = app;
//# sourceMappingURL=admin.js.map
﻿'use strict';

import express = require('express');
import path = require('path');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');
import http = require('http');
import cfg = require('../config');
import passport = require('passport');
import bcrypt = require('bcryptjs');
import session = require('express-session');
import renderer = require('./utils/renderer');
import flash = require('connect-flash');
import dbengine = require('./database/engine');
import bg = require('./background/bgProcess');
import documents = require('./documents/documents');
import logger = require('./utils/logger');
import socketio = require('socket.io');
import Net = require('net');

// Express
var app = express();
var favicon = require('serve-favicon');

// Config
var extend = require('extend');
/*jshint -W069 */
var env = (<any>app.settings)['env'] || 'prod';
var config = extend({}, cfg, (<any>cfg)[env]);

logger.start(env);

// For testing
/*var sleep = function(time: number) {
    var stop = new Date().getTime();
    while (new Date().getTime() < stop + time) {
    }
}*/

// view engine setup
app.set('port', config.listenPort);
app.set('views', config.path.views);
app.engine('html', renderer.render);
app.engine('json', renderer.render);
app.engine('svg', renderer.render);

app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(flash());

app.use(favicon(path.resolve(cfg.path.base, 'favicon.ico')));

app.use(session({
    secret: 'i2l2odfv45ewmWyQwLLi2foe2addsnwwdwm3Eyws3oSnKP',
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: false
    }
}));

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

require('./app/authentication')(passport, env);

require('./app/routes')(app, passport);
    
// Database
logger.log('System', 'cyan', 'Application', 'Initializing database');
dbengine.initDB(function(db, err) {
    if (err) {
        logger.logError('System', 'Application', err);
        throw err;
    }
    logger.log('System', 'green', 'Application', 'Database ready');
    var port = app.get('port') || config[env].listenPort || 80;
    var httpServer = http.createServer(app).listen(port, function() {
        process.title = 'tct Server';
        logger.log('System', 'green', 'Application', 'tct server listening on port \\0', port);

        // Start Background Process
        new bg.bgProcess(db);
    });
    
    /*httpServer.on('connection', function(socket: Net.Socket) {
        logger.log('System', 'white', 'Application', 'Server, client connected \\0', socket.remoteAddress);    
    })*/
    
    var socketInfos: SocketInfos = {};

    socketio.listen(httpServer).sockets.on('connection', function(socket) {
        socketInfos[socket.client.id] = {
            io: socket,
            event: socket.handshake.query.event
        } as SocketInfo;
        logger.log('System', 'white', 'Application', 'Socket io connected \\0', socket.client.id);
        socket.on('disconnect', function() {
            logger.log('System', 'white', 'Application', 'Socket io disconnected \\0', socket.client.id);
            delete socketInfos[socket.client.id];
        });
    })
    
    // Set io express app need for
    documents.locals.io = socketInfos;
});

export = app;

'use strict';

import express = require('express');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');
import config = require('../../config');
import fs = require('fs');
import path = require('path');
import dbengine = require('../database/engine');
import logger = require('../utils/logger');

var app = express();

app.set('views', config.path.views);

app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.get('/', function(req, res) {
    logger.log(req, 'grey', 'Api', 'Navigate to webapi test page')
    res.render('webapi/webapi.html');
})

app.post('/', function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    var user: any;

    var executeFunction = function() {
        var fn = req.query.f;
        if (fn) {
            logger.log(req, 'grey', 'Api', 'Webapi request ' + fn);
        }

        if (fn === 'gettube') {
            var tubename = req.query.name;
            if (!tubename) {
                var err: Error = {
                    name: 'gettubeparam',
                    message: 'Invalid parameters for gettube methode. Name is mandatory.'
                }
                logger.logError(req, 'Api', err);
                res.status(500).send(err.message);
            } else {
                dbengine.tubes.getTube({ name: new RegExp('^' + tubename + '$', 'i') }, user && user._id, true, function(error: Error, tubeInfos: TubeSearchResult) {
                    if (error) {
                        res.status(500).send(error.message);
                    } else if (!tubeInfos) {
                        res.status(500).send('Tube with this name not found.');
                    } else {
                        delete tubeInfos.tubeData._text;
                        res.jsonp(tubeInfos);
                    }
                });
            }

        } else {
            var err2: Error = {
                name: 'invalidapi',
                message: 'Invalid api'
            }
            logger.logError(req, 'Api', err2);
            res.status(500).send(err2.message);
        }
    }

    if (req.query.user) {
        var selector = {
            $or: [
                { userName: req.query.user.toLowerCase() },
                { email: req.query.user.toLowerCase() }
            ]
        }
        dbengine.users.findUserInfos(selector, function(uerr, u) {
            user = u;
            executeFunction();
        })
    } else {
        executeFunction();
    }
})

export = app;

'use strict';
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('../../config');
var dbengine = require('../database/engine');
var logger = require('../utils/logger');
var app = express();
app.set('views', config.path.views);
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.get('/', function (req, res) {
    logger.log(req, 'grey', 'Api', 'Navigate to webapi test page');
    res.render('webapi/webapi.html');
});
app.post('/', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    var user;
    var executeFunction = function () {
        var fn = req.query.f;
        if (fn) {
            logger.log(req, 'grey', 'Api', 'Webapi request ' + fn);
        }
        if (fn === 'gettube') {
            var tubename = req.query.name;
            if (!tubename) {
                var err = {
                    name: 'gettubeparam',
                    message: 'Invalid parameters for gettube methode. Name is mandatory.'
                };
                logger.logError(req, 'Api', err);
                res.status(500).send(err.message);
            }
            else {
                dbengine.tubes.getTube({ name: new RegExp('^' + tubename + '$', 'i') }, user && user._id, true, function (error, tubeInfos) {
                    if (error) {
                        res.status(500).send(error.message);
                    }
                    else if (!tubeInfos) {
                        res.status(500).send('Tube with this name not found.');
                    }
                    else {
                        delete tubeInfos.tubeData._text;
                        res.jsonp(tubeInfos);
                    }
                });
            }
        }
        else {
            var err2 = {
                name: 'invalidapi',
                message: 'Invalid api'
            };
            logger.logError(req, 'Api', err2);
            res.status(500).send(err2.message);
        }
    };
    if (req.query.user) {
        var selector = {
            $or: [
                { userName: req.query.user.toLowerCase() },
                { email: req.query.user.toLowerCase() }
            ]
        };
        dbengine.users.findUserInfos(selector, function (uerr, u) {
            user = u;
            executeFunction();
        });
    }
    else {
        executeFunction();
    }
});
module.exports = app;
//# sourceMappingURL=api.js.map
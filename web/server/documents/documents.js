'use strict';
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('../../config');
var fs = require('fs');
var path = require('path');
var translationrenderer = require('../utils/i18n/renderer');
var dbengine = require('../database/engine');
var logger = require('../utils/logger');
var uuid = require('node-uuid');
var tdfparser = require('../app/parsers/tdf-parser');
var Busboy = require('busboy');
var app = express();
app.set('views', config.path.views);
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
var socketInfos = function () {
    return (app.locals.io);
};
app.post('/', function (req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        user: req.user,
    };
    var returnError = function (error) {
        translationrenderer.renderjson(options, error, function (json) {
            logger.logError(req, 'Documents', json);
            res.status(500).send(JSON.stringify(json));
        });
    };
    var searchFreeFilename = function (targetDir, destName, deleteIfExists, cb) {
        var fileInfo;
        var fileBase;
        var ext;
        var index = 0;
        var checkFile = function (filename) {
            var file = path.resolve(targetDir, filename);
            fs.exists(file, function (exists) {
                if (!exists) {
                    cb(filename);
                    return;
                }
                if (deleteIfExists) {
                    fs.unlink(file, function (err) {
                        if (!err) {
                            cb(filename);
                            return;
                        }
                        else {
                            logger.logError(req, 'Documents', err);
                            deleteIfExists = false;
                            checkFile(filename);
                        }
                    });
                }
                else {
                    if (!fileInfo) {
                        fileInfo = /(^.+)(\.[^.]+)$/.exec(destName);
                        fileBase = fileInfo ? fileInfo[1] : destName;
                        ext = fileInfo ? fileInfo[2] : '';
                    }
                    checkFile(fileBase + ' (' + (++index) + ')' + ext);
                }
            });
        };
        checkFile(destName);
    };
    var saveFile = function (dir, file, filename, cb) {
        fs.exists(config.path.upload, function (exists) {
            if (!exists) {
                fs.mkdirSync(config.path.upload);
            }
            var uploadDir = path.join(config.path.upload, dir);
            fs.exists(uploadDir, function (exists) {
                if (!exists) {
                    fs.mkdirSync(uploadDir);
                }
                var fileInfo = /(^.+\.)([^.]+)$/.exec(filename);
                if (fileInfo && fileInfo.length > 2 && fileInfo[2] === 'tdf') {
                    tdfparser.parse(file, filename, function (datas) {
                        filename = fileInfo[1] + 'tds';
                        searchFreeFilename(uploadDir, filename, true, function (uploadFile) {
                            var uploadResult = {
                                filename: uploadFile,
                                tubeGraph: {
                                    c: datas,
                                    name: uploadFile,
                                    raw: true
                                },
                            };
                            fs.writeFile(path.resolve(uploadDir, uploadFile), JSON.stringify(uploadResult), function (err) {
                                if (err) {
                                    logger.logError(req, 'Documents', err);
                                }
                                else {
                                    logger.log(req, 'magenta', 'Documents', 'File \\0 uploaded.', uploadFile);
                                }
                                uploadResult.error = err;
                                cb(uploadResult);
                            });
                        });
                    });
                }
                else {
                    searchFreeFilename(uploadDir, filename, true, function (uploadFile) {
                        var fstream = fs.createWriteStream(path.resolve(uploadDir, uploadFile));
                        file.pipe(fstream);
                        fstream.on('close', function () {
                            logger.log(req, 'magenta', 'Documents', 'File \\0 uploaded.', uploadFile);
                            cb({
                                filename: uploadFile
                            });
                        });
                    });
                }
            });
        });
    };
    var saveTube = function (uploadResult, cb) {
        var saveTubeTimeout;
        if (saveTubeTimeout) {
            clearTimeout(saveTubeTimeout);
        }
        var saveNow = function () {
            var tubeData = uploadResult.tubeResult.tubeData;
            logger.log(req, 'yellow', 'Documents', 'Saving tube ' + tubeData._id);
            dbengine.tubes.saveTube(tubeData, req.user, function (err) {
                if (err) {
                    returnError(err);
                }
                else {
                    logger.log(req, 'rainbow', 'Documents', 'Tube saved');
                    translationrenderer.renderjson(options, uploadResult, function (json) {
                        if (cb) {
                            cb(json);
                        }
                        else {
                            var sis = socketInfos();
                            Object.keys(sis).forEach(function (socketId) {
                                var s = sis[socketId];
                                var userName = (req.user.userName || req.user.email);
                                if (s.event === 'documentAttached#' + userName) {
                                    s.io.emit('documentAttached', json);
                                }
                            });
                        }
                        uploadResult.files = [];
                    });
                }
            });
        };
        if (cb) {
            saveNow();
        }
        else {
            saveTubeTimeout = setTimeout(function () {
                saveTubeTimeout = undefined;
                saveNow();
            }, 500);
        }
    };
    var attachFile = function (uploadResult, file, description, cb) {
        var tubeData = uploadResult.tubeResult.tubeData;
        var moveFile = function (from, to, nameForLog) {
            try {
                fs.renameSync(from, to);
                logger.log(req, 'rainbow', 'Documents', 'File added for tube ' + tubeData.name + ': ' + nameForLog);
            }
            catch (err) {
                logger.logError(req, 'Documents', err);
            }
        };
        fs.exists(config.path.documents, function (exists) {
            if (!exists) {
                fs.mkdirSync(config.path.documents);
            }
            var destName = file.filename;
            var fileInfo = /(^.+)(\.[^.]+)$/.exec(destName);
            var fileBase = fileInfo ? fileInfo[1] : file.filename;
            var ext = fileInfo ? fileInfo[2] : '';
            var userName = (req.user.userName || req.user.email);
            var targetDir = userName.replace(/[\/:*?"<>|]/gi, '_').toLowerCase();
            var uploadFile = path.resolve(config.path.upload, targetDir, destName);
            if (fs.existsSync(uploadFile)) {
                var index = 0;
                while (fs.existsSync(path.resolve(config.path.documents, destName))) {
                    if (req.query.override === "true") {
                        var destPath = path.resolve(config.path.documents, destName);
                        fs.unlink(destPath, function (err) {
                            if (err) {
                                returnError(err);
                                return;
                            }
                            moveFile(uploadFile, destPath, destName);
                            uploadResult.files.push(file);
                            cb(uploadResult);
                        });
                        return;
                    }
                    destName = fileBase + ' (' + (++index) + ')' + ext;
                }
                file.filename = destName;
            }
            if (!tubeData.documents) {
                tubeData.documents = [];
            }
            var descr = description || fileBase;
            var d = 0;
            if (tubeData.documents.some(function (doc) { return doc.description === descr; })) {
                descr = fileBase + ' (' + (++d) + ')';
            }
            var destFile = path.resolve(config.path.documents, file.filename);
            moveFile(uploadFile, destFile, file.filename);
            var tubeDoc = {
                _id: uuid.v1(),
                filename: file.filename,
                description: descr
            };
            tubeData.documents.push(tubeDoc);
            uploadResult.files.push(file);
            saveTube(uploadResult, cb);
        });
    };
    var validateTubeGraph = function (tubeResult, tubeGraph) {
        var ve = [];
        var numberOfUnits = Math.min(!tubeResult.type || tubeResult.type.sym ? 1 : tubeResult.type.cfg.length);
        var currentConfig;
        if (numberOfUnits > 1) {
            if (isNaN(tubeGraph.unit)) {
                ve.push({
                    name: 'err-unitmand',
                    message: 'Unit is mandatory.',
                    tooltip: true,
                    type: 'error',
                    fieldName: 'unit'
                });
            }
            currentConfig = tubeResult.type.cfg[tubeGraph.unit];
        }
        else if (tubeResult.type.cfg.length) {
            currentConfig = tubeResult.type.cfg[0];
        }
        if (currentConfig && currentConfig.pins.indexOf('g2') >= 0 && !tubeGraph.triode) {
            if (isNaN(tubeGraph.vg2)) {
                ve.push({
                    name: 'err-vg2mand',
                    message: 'Grid2 voltage is mandatory.',
                    tooltip: true,
                    type: 'error',
                    fieldName: 'vg2-value'
                });
            }
            else if (tubeGraph.vg2 < 0 || tubeGraph.vg2 > 9999) {
                ve.push({
                    name: 'err-vg2',
                    message: 'Grid2 voltage is out of range.',
                    tooltip: true,
                    type: 'error',
                    fieldName: 'vg2-value'
                });
            }
        }
        if (!tubeGraph.c || tubeGraph.c.length === 0) {
            ve.push({
                name: 'err-noserie',
                message: 'At least one curve must be defined before continuing.',
                tooltip: false,
                type: 'error',
            });
        }
        return ve;
    };
    if (req.query.d === 'attachgraph') {
        var tubeGraph = req.body;
        var tubeid = req.query.id;
        if (!tubeid || !tubeGraph) {
            returnError({
                name: 'wrongparams',
                message: 'Wrong Parameters.'
            });
            return;
        }
        logger.log(req, 'yellow', 'Documents', 'Saving graph: ' + req.body.name);
        logger.log(req, 'yellow', 'Documents', 'Saving graph body: ' + JSON.stringify(req.body));
        dbengine.tubes.getTube({ _id: tubeid }, req.query.userid || req.user._id, false, function (err, tubeResult) {
            if (err) {
                returnError(err);
            }
            else if (!tubeResult || !tubeResult.tubeData) {
                returnError({
                    name: 'Tubenotfound',
                    message: 'Tube not found'
                });
            }
            else {
                var verrors = validateTubeGraph(tubeResult, tubeGraph);
                if (verrors.length) {
                    returnError(verrors[0]);
                }
                else {
                    var userName = (req.user.userName || req.user.email);
                    var dir = userName.replace(/[\/:*?"<>|]/gi, '_').toLowerCase();
                    var uploadDir = path.join(config.path.upload, dir);
                    fs.exists(uploadDir, function (exists) {
                        if (!exists) {
                            fs.mkdirSync(uploadDir);
                        }
                        var destname = tubeGraph.name.replace(/[\/:*?"<>|]/gi, '_').toLowerCase() + '.tds';
                        searchFreeFilename(uploadDir, destname, true, function (filename) {
                            var file = {
                                filename: filename,
                                tubeGraph: tubeGraph
                            };
                            fs.writeFile(path.resolve(uploadDir, filename), JSON.stringify(file), function (err) {
                                if (err) {
                                    logger.logError(req, 'Documents', err);
                                    returnError(err);
                                }
                                else {
                                    logger.log(req, 'magenta', 'Documents', 'File \\0 created.', filename);
                                    attachFile({ tubeResult: tubeResult, files: [] }, file, tubeGraph.name, function (result) {
                                        res.json(result.files[0]);
                                    });
                                }
                            });
                        });
                    });
                }
            }
        });
    }
    else if (req.query.d === 'upload') {
        var busboy = new Busboy({ headers: req.headers });
        busboy.on('file', function (fieldname, file, filename) {
            logger.log(req, 'cyan', 'Documents', 'uploading ' + filename);
            var userName = (req.user.userName || req.user.email);
            var dir = userName.replace(/[\/:*?"<>|]/gi, '_').toLowerCase();
            saveFile(dir, file, filename, function (result) {
                translationrenderer.renderjson(options, result, function (json) {
                    var sis = socketInfos();
                    Object.keys(sis).forEach(function (socketId) {
                        var s = sis[socketId];
                        if (s.event === 'uploadResult#' + req.user.userName) {
                            s.io.emit('uploadResult', json);
                        }
                    });
                });
            });
        });
        busboy.on('finish', function () {
            logger.log(req, 'cyan', 'Documents', 'uploading done');
            res.status(200).end();
        });
        req.pipe(busboy);
    }
    else if (req.query.d === 'attach') {
        var tubeId = req.query.id;
        if (!tubeId) {
            returnError({
                name: 'wrongparams',
                message: 'Wrong Parameters.'
            });
            return;
        }
        var uploadResult = {
            files: []
        };
        dbengine.tubes.getTube({ _id: tubeId }, req.query.userid || req.user._id, false, function (err, tubeResult) {
            if (err) {
                returnError(err);
            }
            else if (!tubeResult || !tubeResult.tubeData) {
                returnError({
                    name: 'Tubenotfound',
                    message: 'Tube not found'
                });
            }
            else {
                uploadResult.tubeResult = tubeResult;
                var busboy2 = new Busboy({ headers: req.headers });
                busboy2.on('finish', function () {
                    logger.log(req, 'cyan', 'Documents', 'uploading done');
                    res.status(200).end();
                });
                busboy2.on('file', function (fieldname, file, filename) {
                    logger.log(req, 'cyan', 'Documents', 'uploading ' + filename);
                    var userName = (req.user.userName || req.user.email);
                    var dir = userName.replace(/[\/:*?"<>|]/gi, '_').toLowerCase();
                    saveFile(dir, file, filename, function (file) {
                        attachFile(uploadResult, file);
                    });
                });
                req.pipe(busboy2);
            }
        });
    }
});
module.exports = app;
//# sourceMappingURL=documents.js.map
'use strict';

import express = require('express');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');
import config = require('../../config');
import fs = require('fs');
import path = require('path');
import session = require('express-session');
import translationrenderer = require('../utils/i18n/renderer');
import tube = require('../common/ts/tube');
import dbengine = require('../database/engine');
import logger = require('../utils/logger');
import uuid = require('node-uuid');
import socketio = require('socket.io');

// Parsers
import tdfparser = require('../app/parsers/tdf-parser');

var Busboy = require('busboy');

var app = express();

app.set('views', config.path.views);

app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

var socketInfos = function(): SocketInfos {
    return <SocketInfos>(app.locals.io);
}

app.post('/', function(req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        user: req.user,
    }

    var returnError = function(error: Error) {
        translationrenderer.renderjson(options, error, function(json) {
            logger.logError(req, 'Documents', json);
            res.status(500).send(JSON.stringify(json));
        })
    }

    var searchFreeFilename = function(targetDir: string, destName: string, deleteIfExists: boolean, cb: (filename: string) => void) {
        var fileInfo: RegExpExecArray;
        var fileBase: string;
        var ext: string;
        var index = 0;

        var checkFile = function(filename: string) {
            var file = path.resolve(targetDir, filename)
            fs.exists(file, function(exists) {
                if (!exists) {
                    // Filename is free, go
                    cb(filename);
                    return;
                }

                if (deleteIfExists) {
                    // Try to delete the file
                    fs.unlink(file, function(err: NodeJS.ErrnoException){
                        if (!err) {
                            // File is deleted, go
                            cb(filename);
                            return;                            
                        } else {
                            // Can't delete the target file, try again with a new filename
                            logger.logError(req, 'Documents', err);  
                            deleteIfExists = false;
                            checkFile(filename);
                        }                        
                    });
                } else {
                    // We need a new filename, infos are required
                    if (!fileInfo) {
                        fileInfo = /(^.+)(\.[^.]+)$/.exec(destName);
                        fileBase = fileInfo ? fileInfo[1] : destName;
                        ext = fileInfo ? fileInfo[2] : '';
                    }
                    // Check if the new filename is available
                    checkFile(fileBase + ' (' + (++index) + ')' + ext);
                }
            })
        }

        checkFile(destName);
    }

    var saveFile = function(dir: string, file: NodeJS.ReadableStream, filename: string, cb: (result: UploadResultFile) => void) {
        fs.exists(config.path.upload, function(exists) {
            if (!exists) {
                fs.mkdirSync(config.path.upload);
            }

            var uploadDir = path.join(config.path.upload, dir);
            fs.exists(uploadDir, function(exists) {
                if (!exists) {
                    fs.mkdirSync(uploadDir);
                }

                var fileInfo = /(^.+\.)([^.]+)$/.exec(filename);
                if (fileInfo && fileInfo.length > 2 && fileInfo[2] === 'tdf') {
                    tdfparser.parse(file, filename, function(datas: Array<TubeCurve>) {
                        // New extension for the parsed file
                        filename = fileInfo[1] + 'tds';
                        searchFreeFilename(uploadDir, filename, true, function(uploadFile) {    
                            var uploadResult = {
                                filename: uploadFile,
                                tubeGraph: {
                                    c: datas,
                                    name: uploadFile,
                                    raw: true
                                },
                            } as UploadResultFile;
                    
                            // Save file 
                            fs.writeFile(path.resolve(uploadDir ,uploadFile), JSON.stringify(uploadResult), function(err) {
                                if (err) {
                                    logger.logError(req, 'Documents', err);
                                } else {
                                    logger.log(req, 'magenta', 'Documents', 'File \\0 uploaded.', uploadFile);
                                }
                                uploadResult.error = err;
                                cb(uploadResult);
                            })
                        })
                    });
                } else {
                    //Path where image will be uploaded
                    searchFreeFilename(uploadDir, filename, true, function(uploadFile) {
                        var fstream = fs.createWriteStream(path.resolve(uploadDir ,uploadFile));
                        file.pipe(fstream);
                        fstream.on('close', function() {
                            logger.log(req, 'magenta', 'Documents', 'File \\0 uploaded.', uploadFile);
                            cb({
                                filename: uploadFile
                            });
                        })
                    });
                }
            })
        })
    }

    var saveTube = function(uploadResult: UploadResult, cb: (result: UploadResult) => void) {
        var saveTubeTimeout: NodeJS.Timer;

        if (saveTubeTimeout) {
            clearTimeout(saveTubeTimeout);
        }

        var saveNow = function() {
            var tubeData = uploadResult.tubeResult.tubeData;
            logger.log(req, 'yellow', 'Documents', 'Saving tube ' + tubeData._id);
            dbengine.tubes.saveTube(tubeData, req.user, function(err) {
                if (err) {
                    returnError(err);
                } else {
                    logger.log(req, 'rainbow', 'Documents', 'Tube saved');
                    // Notify by socket
                    translationrenderer.renderjson(options, uploadResult, function(json) {
                        if (cb) {
                            cb(json);
                        } else {
                            // Notify all clients registered to the event by push                                       
                            var sis = socketInfos();
                            Object.keys(sis).forEach(function(socketId: string) {
                                var s = sis[socketId] as SocketInfo;
                                var userName = (req.user.userName || req.user.email);
                                if (s.event === 'documentAttached#' + userName) {
                                    s.io.emit('documentAttached', json);
                                }
                            })
                        }
                        uploadResult.files = [];
                    })
                }
            })
        }

        if (cb) {
            saveNow();
        } else {
            // Delayed save, return is with push
            saveTubeTimeout = setTimeout(function() {
                saveTubeTimeout = undefined;
                saveNow();
            }, 500);
        }
    }

    var attachFile = function(uploadResult: UploadResult, file: UploadResultFile, description?: string, cb?: (result: UploadResult) => void) {
        var tubeData = uploadResult.tubeResult.tubeData;

        // Move function
        var moveFile = function(from: string, to: string, nameForLog: string) {
            try {
                fs.renameSync(from, to);
                logger.log(req, 'rainbow', 'Documents', 'File added for tube ' + tubeData.name + ': ' + nameForLog);
            } catch (err) {
                logger.logError(req, 'Documents', err);
            }
        }
  
        // Check if document directory exists                    
        fs.exists(config.path.documents, function(exists) {
            if (!exists) {
                fs.mkdirSync(config.path.documents);
            }

            var destName = file.filename;
            var fileInfo = /(^.+)(\.[^.]+)$/.exec(destName);
            var fileBase = fileInfo ? fileInfo[1] : file.filename;
            var ext = fileInfo ? fileInfo[2] : '';
            var userName = (req.user.userName || req.user.email);
            var targetDir = userName.replace(/[\/:*?"<>|]/gi, '_').toLowerCase();
            var uploadFile = path.resolve(config.path.upload, targetDir, destName);

            if (fs.existsSync(uploadFile)) {                
                // Search for a non existing name in document folder                   
                var index = 0;
                while (fs.existsSync(path.resolve(config.path.documents, destName))) {
                    // Override the existing file if a override qery string keyword is specified
                    if (req.query.override === "true") {
                        // Just replace the file, the tube is not modified because linked to the file
                        var destPath = path.resolve(config.path.documents, destName);
                        /*jshint -W083 */ 
                        fs.unlink(destPath, function(err: NodeJS.ErrnoException) {
                            if (err) {
                                returnError(err);
                                return;
                            }
                            moveFile(   uploadFile, destPath, destName);
                            uploadResult.files.push(file);
                            cb(uploadResult);                            
                        })
                        return;
                    }
                    
                    // Search a free file name
                    destName = fileBase + ' (' + (++index) + ')' + ext;
                }
                file.filename = destName;
            }

            if (!tubeData.documents) {
                tubeData.documents = [];
            }
                                    
            // Search a free description based on the filename
            var descr = description || fileBase;
            var d = 0;
            if (tubeData.documents.some(doc => doc.description === descr)) {
                descr = fileBase + ' (' + (++d) + ')';
            }

            // Save files in document directory
            var destFile = path.resolve(config.path.documents, file.filename);
            moveFile(uploadFile, destFile, file.filename);

            // Add unique id for the document
            var tubeDoc = {
                _id: uuid.v1(),
                filename: file.filename,
                description: descr
            } as TubeDoc;
            tubeData.documents.push(tubeDoc);

            uploadResult.files.push(file);
            saveTube(uploadResult, cb);
        })
    }

    var validateTubeGraph = function(tubeResult: TubeSearchResult, tubeGraph: TubeGraph) {
        var ve: Array<validation.Message> = [];
        
        // Check if the unit has a G2
        var numberOfUnits = Math.min(!tubeResult.type || tubeResult.type.sym ? 1 : tubeResult.type.cfg.length);
        var currentConfig: TubePins
        if (numberOfUnits > 1) {
            // Check the selected unit
            if (isNaN(tubeGraph.unit)) {
                ve.push({
                    name: 'err-unitmand',
                    message: 'Unit is mandatory.',
                    tooltip: true,
                    type: 'error',
                    fieldName: 'unit'
                });
            }
            currentConfig = tubeResult.type.cfg[tubeGraph.unit];
        } else if (tubeResult.type.cfg.length) {
            currentConfig = tubeResult.type.cfg[0];
        }

        if (currentConfig && currentConfig.pins.indexOf('g2') >= 0 && !tubeGraph.triode) {
            if (isNaN(tubeGraph.vg2)) {
                ve.push({
                    name: 'err-vg2mand',
                    message: 'Grid2 voltage is mandatory.',
                    tooltip: true,
                    type: 'error',
                    fieldName: 'vg2-value'
                });
            } else if (tubeGraph.vg2 < 0 || tubeGraph.vg2 > 9999) {
                ve.push({
                    name: 'err-vg2',
                    message: 'Grid2 voltage is out of range.',
                    tooltip: true,
                    type: 'error',
                    fieldName: 'vg2-value'
                });
            }
        }

        if (!tubeGraph.c || tubeGraph.c.length === 0) {
            ve.push({
                name: 'err-noserie',
                message: 'At least one curve must be defined before continuing.',
                tooltip: false,
                type: 'error',
            });
        }

        return ve;
    }

    if (req.query.d === 'attachgraph') {
        // Attach a created graph structure to the specified tube id
        var tubeGraph = req.body as TubeGraph;

        // Get the tube with the id
        var tubeid = req.query.id;
        if (!tubeid || !tubeGraph) {
            // Wrong parameters
            returnError({
                name: 'wrongparams',
                message: 'Wrong Parameters.'
            });
            return;
        }

        logger.log(req, 'yellow', 'Documents', 'Saving graph: ' + req.body.name);
        logger.log(req, 'yellow', 'Documents', 'Saving graph body: ' + JSON.stringify(req.body));
        dbengine.tubes.getTube({ _id: tubeid }, req.query.userid || req.user._id, false, function(err, tubeResult) {
            if (err) {
                returnError(err);
            } else if (!tubeResult || !tubeResult.tubeData) {
                returnError({
                    name: 'Tubenotfound',
                    message: 'Tube not found'
                })
            } else {
                var verrors = validateTubeGraph(tubeResult, tubeGraph);
                if (verrors.length) {
                    returnError(verrors[0]);
                } else {                      
                    // Save file to upload directory
                    var userName = (req.user.userName || req.user.email);
                    var dir = userName.replace(/[\/:*?"<>|]/gi, '_').toLowerCase();
                    var uploadDir = path.join(config.path.upload, dir);
                    fs.exists(uploadDir, function(exists) {
                        if (!exists) {
                            fs.mkdirSync(uploadDir);
                        }
                    
                        // Create a compatible filename from the description                    
                        var destname = tubeGraph.name.replace(/[\/:*?"<>|]/gi, '_').toLowerCase() + '.tds';
                        searchFreeFilename(uploadDir, destname, true, function(filename) {
                            var file: UploadResultFile = {
                                filename: filename,
                                tubeGraph: tubeGraph
                            };
            
                            // Save file 
                            fs.writeFile(path.resolve(uploadDir, filename), JSON.stringify(file), function(err) {
                                if (err) {
                                    logger.logError(req, 'Documents', err);
                                    returnError(err);
                                } else {
                                    logger.log(req, 'magenta', 'Documents', 'File \\0 created.', filename);
                                    attachFile({ tubeResult: tubeResult, files: [] }, file, tubeGraph.name, function(result: UploadResult) {
                                        res.json(result.files[0]);
                                    });
                                }
                            })
                        });
                    })
                }
            }
        })
    } else if (req.query.d === 'upload') {
        // Upload files, save to the temporary upload directory and return the filenames
        var busboy = new Busboy({ headers: req.headers });
        busboy.on('file', function(fieldname: string, file: NodeJS.ReadableStream, filename: string) {
            logger.log(req, 'cyan', 'Documents', 'uploading ' + filename);
            var userName = (req.user.userName || req.user.email);
            var dir = userName.replace(/[\/:*?"<>|]/gi, '_').toLowerCase();
            saveFile(dir, file, filename, function(result: UploadResultFile) {
                // Notify by socket
                translationrenderer.renderjson(options, result, function(json) {
                    // Notify only the related client
                    var sis = socketInfos();
                    Object.keys(sis).forEach(function(socketId: string) {
                        var s = sis[socketId] as SocketInfo;
                        if (s.event === 'uploadResult#' + req.user.userName) {
                            s.io.emit('uploadResult', json);
                        }
                    })
                })
            });
        })
        busboy.on('finish', function() {
            logger.log(req, 'cyan', 'Documents', 'uploading done');
            res.status(200).end();
        });
        req.pipe(busboy);

    } else if (req.query.d === 'attach') {
        // Attach a uploaded file or multiple files to the specified tube id
        // Get the tube with the id
        var tubeId = req.query.id;
        if (!tubeId) {
            // Wrong parameters
            returnError({
                name: 'wrongparams',
                message: 'Wrong Parameters.'
            });
            return;
        }

        var uploadResult = {
            files: []
        } as UploadResult;

        dbengine.tubes.getTube({ _id: tubeId }, req.query.userid || req.user._id, false, function(err, tubeResult) {
            if (err) {
                returnError(err);
            } else if (!tubeResult || !tubeResult.tubeData) {
                returnError({
                    name: 'Tubenotfound',
                    message: 'Tube not found'
                })
            } else {
                uploadResult.tubeResult = tubeResult;
                var busboy2 = new Busboy({ headers: req.headers });
                busboy2.on('finish', function() {
                    logger.log(req, 'cyan', 'Documents', 'uploading done');
                    res.status(200).end();
                });
                busboy2.on('file', function(fieldname: string, file: NodeJS.ReadableStream, filename: string) {
                    logger.log(req, 'cyan', 'Documents', 'uploading ' + filename);
                    var userName = (req.user.userName || req.user.email);
                    var dir = userName.replace(/[\/:*?"<>|]/gi, '_').toLowerCase();
                    saveFile(dir, file, filename, function(file: UploadResultFile) {
                        attachFile(uploadResult, file);
                    })
                });
                req.pipe(busboy2);
            }
        })
    }
});

export = app;

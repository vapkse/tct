'use strict';

import express = require('express');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');
import config = require('../../config');
import fs = require('fs');
import path = require('path');
import session = require('express-session');
import translationrenderer = require('../utils/i18n/renderer');
import tube = require('../common/ts/tube');
import dbengine = require('../database/engine');
import logger = require('../utils/logger');
import encoder = require('../common/ts/encoder');
import uuid = require('node-uuid');

var app = express();

app.set('views', config.path.views);

app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.get('/', function(req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        user: req.user,
        errors: req.flash('error'),
        warnings: req.flash('warning'),
        infos: req.flash('info')
    }

    logger.log(req, 'grey', 'Route', 'Navigate to viewer');
    res.render('viewer.html', options);
});

app.post('/', function(req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
    }

    var returnError = function(error: Error) {
        translationrenderer.renderjson(options, error, function(json) {
            logger.logError(req, 'Viewer', json);
            res.status(500).send(json.message);
        })
    }

    if (req.query.id) {
        // View tube
        logger.log(req, 'cyan', 'Viewer', 'Viewing tube ' + req.url);
        var userid = req.query.userid || req.user && req.user._id;
        dbengine.tubes.getTube({ _id: req.query.id }, userid , true, function(err, result) {
            if (err) {
                returnError(err);
            } else if (!result || !result.tubeData) {
                returnError({
                    name: 'tubenotfound',
                    message: 'Tube not found in the dtabase.'
                });
            } else {               
                translationrenderer.renderjson(options, result, function(tube) {
                    res.json(tube);
                })
            }
        })
    } else {
        // Wrong parameters
        returnError({
            name: 'wrongparams',
            message: 'Wrong Parameters.'
        });
    }
})

export = app;

'use strict';
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('../../config');
var translationrenderer = require('../utils/i18n/renderer');
var dbengine = require('../database/engine');
var logger = require('../utils/logger');
var app = express();
app.set('views', config.path.views);
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.get('/', function (req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        user: req.user,
        errors: req.flash('error'),
        warnings: req.flash('warning'),
        infos: req.flash('info')
    };
    logger.log(req, 'grey', 'Route', 'Navigate to viewer');
    res.render('viewer.html', options);
});
app.post('/', function (req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
    };
    var returnError = function (error) {
        translationrenderer.renderjson(options, error, function (json) {
            logger.logError(req, 'Viewer', json);
            res.status(500).send(json.message);
        });
    };
    if (req.query.id) {
        logger.log(req, 'cyan', 'Viewer', 'Viewing tube ' + req.url);
        var userid = req.query.userid || req.user && req.user._id;
        dbengine.tubes.getTube({ _id: req.query.id }, userid, true, function (err, result) {
            if (err) {
                returnError(err);
            }
            else if (!result || !result.tubeData) {
                returnError({
                    name: 'tubenotfound',
                    message: 'Tube not found in the dtabase.'
                });
            }
            else {
                translationrenderer.renderjson(options, result, function (tube) {
                    res.json(tube);
                });
            }
        });
    }
    else {
        returnError({
            name: 'wrongparams',
            message: 'Wrong Parameters.'
        });
    }
});
module.exports = app;
//# sourceMappingURL=viewer.js.map
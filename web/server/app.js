'use strict';
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var cfg = require('../config');
var passport = require('passport');
var session = require('express-session');
var renderer = require('./utils/renderer');
var flash = require('connect-flash');
var dbengine = require('./database/engine');
var bg = require('./background/bgProcess');
var documents = require('./documents/documents');
var logger = require('./utils/logger');
var socketio = require('socket.io');
var app = express();
var favicon = require('serve-favicon');
var extend = require('extend');
var env = app.settings['env'] || 'prod';
var config = extend({}, cfg, cfg[env]);
logger.start(env);
app.set('port', config.listenPort);
app.set('views', config.path.views);
app.engine('html', renderer.render);
app.engine('json', renderer.render);
app.engine('svg', renderer.render);
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(flash());
app.use(favicon(path.resolve(cfg.path.base, 'favicon.ico')));
app.use(session({
    secret: 'i2l2odfv45ewmWyQwLLi2foe2addsnwwdwm3Eyws3oSnKP',
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: false
    }
}));
app.use(passport.initialize());
app.use(passport.session());
require('./app/authentication')(passport, env);
require('./app/routes')(app, passport);
logger.log('System', 'cyan', 'Application', 'Initializing database');
dbengine.initDB(function (db, err) {
    if (err) {
        logger.logError('System', 'Application', err);
        throw err;
    }
    logger.log('System', 'green', 'Application', 'Database ready');
    var port = app.get('port') || config[env].listenPort || 80;
    var httpServer = http.createServer(app).listen(port, function () {
        process.title = 'tct Server';
        logger.log('System', 'green', 'Application', 'tct server listening on port \\0', port);
        new bg.bgProcess(db);
    });
    var socketInfos = {};
    socketio.listen(httpServer).sockets.on('connection', function (socket) {
        socketInfos[socket.client.id] = {
            io: socket,
            event: socket.handshake.query.event
        };
        logger.log('System', 'white', 'Application', 'Socket io connected \\0', socket.client.id);
        socket.on('disconnect', function () {
            logger.log('System', 'white', 'Application', 'Socket io disconnected \\0', socket.client.id);
            delete socketInfos[socket.client.id];
        });
    });
    documents.locals.io = socketInfos;
});
module.exports = app;
//# sourceMappingURL=app.js.map
'use strict';

import express = require('express');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');
import config = require('../../config');
import translationrenderer = require('../utils/i18n/renderer');
import dbengine = require('../database/engine');
import logger = require('../utils/logger');

var app = express();

app.set('views', config.path.views);

app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.post('/', function(req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
    }

    var returnError = function(error: Error) {
        translationrenderer.renderjson(options, error, function(json) {
            logger.logError(req, 'Merge', json);
            res.status(500).send(JSON.stringify(json));
        })
    }

    if (req.query.userid && req.query.d === 'save') {
        // Save tube in global db
        logger.log(req, 'yellow', 'Merge', 'Saving tube ' + req.url);
        var tubeData = JSON.parse(req.query.j) as TubeData;

        var isNew = !tubeData._id;

        dbengine.tubes.saveGlobalTube(tubeData, req.user, function(err, result) {
            if (err) {
                returnError(err);
            } else {
                translationrenderer.renderjson(options, result, function(json) {
                    logger.log(req, 'rainbow', 'Merge', 'Tube saved');

                    var news: NewsData;
                    if (isNew) {
                        news = {
                            text: 'New tube added \\0',
                            uid: 'news-tubeadded',
                        }
                    } else {
                        news = {
                            text: 'Tube \\0 updated.',
                            uid: 'news-tubeupdated',
                        }
                    }

                    var href = '/?q=name%3D%22' + result.tubeData.name + '%22';
                    news.params = {
                        0: '<a href="' + href + '">' + result.tubeData.name + '</a>'
                    }
                    news.date = (new Date()).toJSON();

                    // Add news in db
                    dbengine.news.addNews(news, function(err: Error) {
                        if (err) {
                            logger.logError(options, 'Merge', err)
                        }
                    })

                    res.json(json);
                });
            }
        })
    } else if (req.query.userid) {
        // Get user from username
        logger.log(req, 'cyan', 'Merge', 'Merging tube ' + req.url);
        dbengine.users.findUserInfos({ _id: req.query.userid.toString() }, function(err, user) {
            if (err) {
                returnError(err);
            } else if (!user) {
                returnError({
                    name: 'usernotfound',
                    message: 'User not found in the database.'
                });
            } else {
                // Find user tube 
                dbengine.tubes.getTube({ _id: req.query.id }, user._id, false, function(err, userTube) {
                    if (err) {
                        returnError(err);
                    } else if (!userTube || !userTube.tubeData) {
                        returnError({
                            name: 'utubenotfound',
                            message: 'User tube not found in the database.'
                        });
                    } else {
                        // Load global tube with the same name
                        dbengine.tubes.getTube({ name: new RegExp('^' + userTube.tubeData.name + '$', 'i') }, null, false, function(err, globalTube) {
                            if (err) {
                                returnError(err);
                            } else if (globalTube && globalTube.tubeData) {
                                // Extend global and user tube
                                var extend = require('extend');
                                var merged = {} as TubeSearchResult;
                                merged.base = userTube.base || globalTube.base;
                                merged.pinout = userTube.pinout || globalTube.pinout;
                                merged.type = userTube.type || globalTube.type;
                                merged.tubeData = extend({}, false, globalTube.tubeData, userTube.tubeData);

                                // Merge units
                                merged.tubeData.units = [];
                                for (var i = 0; i < merged.type.cfg.length; i++) {
                                    var src = userTube.tubeData.units[i] || {};
                                    var tgt = globalTube.tubeData.units[i] || {};
                                    merged.tubeData.units[i] = extend({}, false, tgt, src);
                                }

                                // Merge documents
                                var docs = {} as { [id: string]: TubeDoc };
                                if (globalTube.tubeData.documents) {
                                    /*jshint -W093 */
                                    globalTube.tubeData.documents.map(d => docs[d._id] = d);
                                }
                                if (userTube.tubeData.documents) {
                                    /*jshint -W093 */
                                    userTube.tubeData.documents.map(d => docs[d._id] = d);
                                }
                                merged.tubeData.documents = [];
                                Object.keys(docs).map(id => merged.tubeData.documents.push(docs[id]));

                                // Merge usages exemples
                                var ue = {} as { [id: string]: TubeUsageData };
                                if (globalTube.tubeData.usages) {
                                    /*jshint -W093 */
                                    globalTube.tubeData.usages.map(u => ue[u._id] = u);
                                }
                                if (userTube.tubeData.usages) {
                                    /*jshint -W093 */
                                    userTube.tubeData.usages.map(u => ue[u._id] = u);
                                }
                                merged.tubeData.usages = [];
                                Object.keys(ue).map(id => merged.tubeData.usages.push(ue[id]));

                                merged.merged = true;
                                merged.tubeData._id = globalTube.tubeData._id;
                                translationrenderer.renderjson(options, merged, function(tube) {
                                    res.json(tube);
                                })
                            } else {
                                userTube.merged = true;
                                userTube.tubeData._id = undefined;
                                translationrenderer.renderjson(options, userTube, function(tube) {
                                    res.json(tube);
                                })
                            }
                        })
                    }
                })
            }
        })
    } else {
        // Wrong parameters
        returnError({
            name: 'wrongparams',
            message: 'Wrong Parameters.'
        });
    }
})

export = app;

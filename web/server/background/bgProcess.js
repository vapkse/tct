'use strict';
var fs = require('fs');
var path = require('path');
var config = require('../../config');
var logger = require('../utils/logger');
var dbengine = require('../database/engine');
var archiver = require('archiver');
var bgProcess = (function () {
    function bgProcess(d) {
        var self = this;
        self.db = d;
        logger.log('System', 'magenta', 'BgProcess', 'Background Processes Started');
        dbengine.versions.getVersions(function (err, versions) {
            if (err) {
                logger.logError('System', 'BgProcess', err);
            }
            var checkFile = function (index) {
                if (index >= versions.length) {
                    return;
                }
                var version = versions[index];
                if (version.version.major === 1 && version.version.minor === 1 && /.*[\-]*tubes\.json$/.test(version.fileName)) {
                    logger.log('System', 'red', 'BgProcess', 'Version of ' + version.fileName + ' is ' + version.version.major + '.' + version.version.minor);
                    checkFile(index + 1);
                }
                else {
                    logger.log('System', 'green', 'BgProcess', 'Version of ' + version.fileName + ' is ' + version.version.major + '.' + version.version.minor);
                    checkFile(index + 1);
                }
            };
            checkFile(0);
        });
        var backupDatabase = function () {
            logger.log('System', 'yellow', 'BgProcess', 'Backup Database');
            var backupDir = path.resolve(config.path.backup);
            fs.exists(backupDir, function (exists) {
                if (!exists) {
                    fs.mkdirSync(backupDir);
                }
                var now = new Date();
                var twoChar = function (value) {
                    return value < 10 ? '0' + String(value) : String(value);
                };
                var backupFileName = String(now.getFullYear()) + twoChar(now.getMonth() + 1) + twoChar(now.getDate()) + '_' + twoChar(now.getHours()) + twoChar(now.getMinutes()) + twoChar(now.getSeconds()) + '_backup.zip';
                var output = fs.createWriteStream(path.resolve(backupDir, backupFileName));
                var archive = archiver('zip');
                output.on('close', function () {
                    logger.log('System', 'green', 'BgProcess', 'Document backup zipped: ' + archive.pointer() + ' total bytes');
                });
                archive.on('error', function (err) {
                    logger.logError('System', 'BgProcess', err);
                });
                archive.pipe(output);
                logger.log('System', 'grey', 'BgProcess', 'Backup Documents');
                archive.bulk([
                    { expand: true, cwd: path.resolve(config.path.documents), src: ['**'], dest: 'documents' }
                ]);
                logger.log('System', 'grey', 'BgProcess', 'Backup Database');
                archive.bulk([
                    { expand: true, cwd: path.resolve(config.path.data), src: ['**'], dest: 'database' }
                ]);
                archive.finalize();
            });
        };
        setInterval(function () {
            backupDatabase();
        }, 86400000);
        setTimeout(function () {
            backupDatabase();
        }, 10000);
    }
    return bgProcess;
})();
exports.bgProcess = bgProcess;
//# sourceMappingURL=bgProcess.js.map
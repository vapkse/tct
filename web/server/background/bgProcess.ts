'use strict';

import fs = require('fs');
import path = require('path');
import config = require('../../config');
import encoder = require('../common/ts/encoder');
import logger = require('../utils/logger');
import dbengine = require('../database/engine');
import uuid = require('node-uuid');
import archiver = require('archiver');

export class bgProcess {
    private db: dbengine.Db;

    constructor(d: dbengine.Db) {
        /*jshint validthis: true */
        var self = <bgProcess>this;
        self.db = d;

        logger.log('System', 'magenta', 'BgProcess', 'Background Processes Started');

        // *********************************************************************
        // Database versioning
        // *********************************************************************
        dbengine.versions.getVersions(function(err: Error, versions: Array<FileVersion>) {
            if (err) {
                logger.logError('System', 'BgProcess', err);
            }

            var checkFile = function(index: number) {
                if (index >= versions.length) {
                    return;
                }
                var version = versions[index];
                if (version.version.major === 1 && version.version.minor === 1 && /.*[\-]*tubes\.json$/.test(version.fileName)) {
                    logger.log('System', 'red', 'BgProcess', 'Version of ' + version.fileName + ' is ' + version.version.major + '.' + version.version.minor);
                    /*updateUsages(version.fileName, function(err) {
                        if (err) {
                            logger.logError('System', 'BgProcess', err);
                            return;
                        }
                        dbengine.versions.updateVersion(version.fileName, 1, 2, function(err) {
                            if (err) {
                                logger.logError('System', 'BgProcess', err);
                                return;
                            }
                            version.version.minor = 2;
                            checkFile(index);
                        })
                    })*/
                    checkFile(index + 1);
                } else {
                    logger.log('System', 'green', 'BgProcess', 'Version of ' + version.fileName + ' is ' + version.version.major + '.' + version.version.minor);
                    checkFile(index + 1);
                }
            }
            checkFile(0);
        })

        // *********************************************************************
        // Backup Database
        // *********************************************************************
        var backupDatabase = function() {
            logger.log('System', 'yellow', 'BgProcess', 'Backup Database');

            var backupDir = path.resolve(config.path.backup);
            fs.exists(backupDir, function(exists) {
                if (!exists) {
                    fs.mkdirSync(backupDir);
                }

                var now = new Date();
                var twoChar = function(value: number) {
                    return value < 10 ? '0' + String(value) : String(value);
                }
                var backupFileName = String(now.getFullYear()) + twoChar(now.getMonth() + 1) + twoChar(now.getDate()) + '_' + twoChar(now.getHours()) + twoChar(now.getMinutes()) + twoChar(now.getSeconds()) + '_backup.zip'
                var output = fs.createWriteStream(path.resolve(backupDir, backupFileName));
                var archive = archiver('zip');

                output.on('close', function() {
                    logger.log('System', 'green', 'BgProcess', 'Document backup zipped: ' + archive.pointer() + ' total bytes');
                });

                archive.on('error', function(err: Error) {
                    logger.logError('System', 'BgProcess', err);
                });

                archive.pipe(output);
                logger.log('System', 'grey', 'BgProcess', 'Backup Documents');
                archive.bulk([
                    { expand: true, cwd: path.resolve(config.path.documents), src: ['**'], dest: 'documents' }
                ]);
                logger.log('System', 'grey', 'BgProcess', 'Backup Database');
                archive.bulk([
                    { expand: true, cwd: path.resolve(config.path.data), src: ['**'], dest: 'database' }
                ]);
                archive.finalize();
            });
        }

        // Backup db every days and after 30 minuts
        setInterval(function() {
            backupDatabase();
        }, 86400000);
        setTimeout(function() {
            backupDatabase();
        }, 10000) // 1800000)

        // *********************************************************************
        // Database updates
        // *********************************************************************
        /*var updateUsages = function(colname: string, cb: (err?: Error) => void) {
            logger.log('System', 'yellow', 'BgProcess', 'Updating usage version of ' + colname + ' to version 1.2');
            // Open collection
            db.collection(colname, { strict: true }, function(err, collection) {
                if (err) {
                    cb(err);
                    return;
                }

                // Update, search original on global collection
                collection.find().toArray(function(err: Error, tubeDatas: Array<TubeData>) {
                    if (err) {
                        cb(err);
                    } else if (!tubeDatas || tubeDatas.length === 0) {
                        cb();
                    } else {
                        var updateTube = function(tubeIndex: number) {
                            if (tubeIndex >= tubeDatas.length) {
                                cb();
                                return;
                            }

                            var tubeData = tubeDatas[tubeIndex];
                            if (tubeData.usages && tubeData.usages.length) {
                                var update = false;
                                tubeData.usages.forEach(function(usage) {
                                    if (!usage._id) {
                                        usage._id = uuid.v1();
                                        update = true;
                                    }
                                    if (usage.traces) {
                                        usage.traces = usage.traces.length ? usage.traces[0] : undefined;
                                        update = true;
                                    }
                                })

                                if (update) {
                                    logger.log('System', 'white', 'BgProcess', 'Updating tube ' + tubeData._id + ' ' + tubeData.name);
                                    collection.update({ _id: tubeData._id }, tubeData, function(err) {
                                        if (err) {
                                            cb(err);
                                            return;
                                        }
                                        logger.log('System', 'green', 'BgProcess', 'Updated tube ' + tubeData._id + ' ' + tubeData.name);
                                        updateTube(tubeIndex + 1);
                                    })
                                } else {
                                    updateTube(tubeIndex + 1);
                                }

                            } else {
                                updateTube(tubeIndex + 1);
                            }
                        }
                        updateTube(0);
                    }
                })
            })
        }*/

        // Temporary function to sort the tdf files
        /*var sortTdf = function() {
            logger.log('System', 'magenta', 'BgProcess', 'Satrting tds files sort.');
            // Check if document directory exists    
            var folder = path.resolve(cfg.path.documents);
            fs.exists(folder, function(exists) {
                if (!exists) {
                    return;
                }
                fs.readdir(folder, function(err, files) {
                    if (err) {
                        logger.logError(null, 'BgProcess', err)
                    }
                    var softFile = function(fileIndex: number) {
                        if (fileIndex >= files.length) {
                            logger.log('System', 'magenta', 'BgProcess', 'Finished tds files sort.');
                            return;
                        }
                        var filename = path.resolve(cfg.path.documents, files[fileIndex]);
                        var fileInfo = /(^.+\.)([^.]+)$/.exec(files[fileIndex]);
                        if (fileInfo && fileInfo.length > 2 && fileInfo[2] === 'tds') {
                            fs.readFile(filename, function(err, data) {
                                if (err) {
                                    logger.logError(null, 'BgProcess', err)
                                    softFile(++fileIndex);
                                } else {
                                    var json = JSON.parse(data.toString()) as UploadResultFile;
                                    json.tubeGraph.c.sort(function(c1, c2) {
                                        return c2.vg1 - c1.vg1;
                                    })
                                    json.tubeGraph.c.forEach(function(c) {
                                        c.p.sort(function(p1, p2) {
                                            return p1.va - p2.va;
                                        })
                                    })
                                    fs.writeFile(filename, JSON.stringify(json), function(err) {
                                        if (err) {
                                            logger.logError(null, 'BgProcess', err)
                                        } else {
                                            logger.log(null, 'green', 'BgProcess', 'File ' + filename + ' sorted successfully.');
                                        }
                                        softFile(++fileIndex);
                                    });
                                }
                            })
                        } else {
                            softFile(++fileIndex);
                        }
                    };
                    softFile(0);
                });
            })
        };
        setTimeout(sortTdf, 10000)*/
    }
}


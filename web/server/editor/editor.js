'use strict';
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('../../config');
var fs = require('fs');
var path = require('path');
var translationrenderer = require('../utils/i18n/renderer');
var dbengine = require('../database/engine');
var logger = require('../utils/logger');
var uuid = require('node-uuid');
var app = express();
app.set('views', config.path.views);
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.get('/', function (req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
        user: req.user,
        errors: req.flash('error'),
        warnings: req.flash('warning'),
        infos: req.flash('info')
    };
    logger.log(req, 'grey', 'Editor', 'Navigate to editor');
    res.render('editor.html', options);
});
app.post('/', function (req, res) {
    var options = {
        query: req.query,
        sessionID: req.session.id,
    };
    var returnError = function (error) {
        translationrenderer.renderjson(options, error, function (json) {
            logger.logError(req, 'Editor', json);
            res.status(500).send(JSON.stringify(json));
        });
    };
    if (req.query.d === 'saveusage') {
        logger.log(req, 'yellow', 'Editor', 'Saving usage example ' + req.url);
        var tubeUsageData = JSON.parse(req.query.j);
        dbengine.tubes.getTube({ _id: req.query.id }, req.query.userid || req.user._id, false, function (err, searchResult) {
            if (err) {
                returnError(err);
            }
            else if (!searchResult || !searchResult.tubeData) {
                returnError({
                    name: 'Tubenotfound',
                    message: 'Tube not found'
                });
            }
            else {
                if (req.query.userid !== req.user.id) {
                    tubeUsageData._id = undefined;
                }
                var tubeData = searchResult.tubeData;
                if (!req.query.usageid || req.query.usageid === 'new') {
                    if (!tubeData.usages) {
                        tubeData.usages = [];
                    }
                    tubeUsageData._id = uuid.v1();
                    tubeData.usages.push(tubeUsageData);
                }
                else {
                    tubeUsageData._id = req.query.usageid;
                    var usageIndex = -1;
                    for (var i = 0; i < tubeData.usages.length; i++) {
                        if (tubeData.usages[i]._id === tubeUsageData._id) {
                            usageIndex = i;
                            break;
                        }
                    }
                    if (usageIndex === -1) {
                        returnError({
                            name: 'usagenotfound',
                            message: 'Usage example not found.'
                        });
                        return;
                    }
                    tubeData.usages[usageIndex] = tubeUsageData;
                }
                dbengine.tubes.saveTube(tubeData, req.user, function (err, result) {
                    if (err) {
                        returnError(err);
                    }
                    else {
                        translationrenderer.renderjson(options, result, function (json) {
                            logger.log(req, 'rainbow', 'Editor', 'Tube saved');
                            json.usageId = tubeUsageData._id;
                            res.json(json);
                        });
                    }
                });
            }
        });
    }
    else if (req.query.d === 'save') {
        logger.log(req, 'yellow', 'Editor', 'Saving tube ' + req.url);
        var tubeData = JSON.parse(req.query.j);
        if (req.query.userid && req.query.userid !== req.user._id) {
            delete tubeData._id;
        }
        var moveFile = function (from, to, name) {
            try {
                fs.renameSync(from, to);
                logger.log(req, 'rainbow', 'Editor', 'File added for tube ' + tubeData.name + ': ' + name);
            }
            catch (err) {
                logger.logError(req, 'Editor', err);
            }
        };
        fs.exists(config.path.documents, function (exists) {
            if (!exists) {
                fs.mkdirSync(config.path.documents);
            }
            var files = {};
            for (var f = 0; f < tubeData.documents.length; f++) {
                var file = tubeData.documents[f];
                files[file.filename] = file;
                var userName = (req.user.userName || req.user.email);
                var targetDir = userName.replace(/[\/:*?"<>|]/gi, '_').toLowerCase();
                var uploadFile = path.resolve(config.path.upload, targetDir, file.filename);
                if (fs.existsSync(uploadFile)) {
                    var destName = file.filename;
                    var fileInfo = /(^.+)(\.[^.]+)$/.exec(destName);
                    var index = 0;
                    while (fs.existsSync(path.resolve(config.path.documents, destName))) {
                        destName = fileInfo[1] + ' (' + (++index) + ')' + (fileInfo.length > 2 ? fileInfo[2] : '');
                    }
                    var destFile = path.resolve(config.path.documents, destName);
                    moveFile(uploadFile, destFile, destName);
                    file._id = uuid.v1();
                }
            }
            dbengine.tubes.saveTube(tubeData, req.user, function (err, result) {
                if (err) {
                    returnError(err);
                }
                else {
                    translationrenderer.renderjson(options, result, function (json) {
                        logger.log(req, 'rainbow', 'Editor', 'Tube saved');
                        res.json(json);
                    });
                }
            });
        });
    }
    else if (req.query.id) {
        if (req.query.id === 'new') {
            logger.log(req, 'cyan', 'Editor', 'New tube ' + req.url);
            dbengine.tubes.newTube(function (err, result) {
                if (err) {
                    returnError(err);
                }
                else {
                    translationrenderer.renderjson(options, result, function (tube) {
                        res.json(tube);
                    });
                }
            });
        }
        else {
            logger.log(req, 'cyan', 'Editor', 'Editing tube ' + req.url);
            dbengine.tubes.getTube({ _id: req.query.id }, req.query.userid || req.user._id, false, function (err, result) {
                if (err) {
                    returnError(err);
                }
                else if (!result || !result.tubeData) {
                    returnError({
                        name: 'tubenotfound',
                        message: 'Tube not found in the database.'
                    });
                }
                else {
                    translationrenderer.renderjson(options, result, function (tube) {
                        res.json(tube);
                    });
                }
            });
        }
    }
    else {
        returnError({
            name: 'wrongparams',
            message: 'Wrong Parameters.'
        });
    }
});
module.exports = app;
//# sourceMappingURL=editor.js.map
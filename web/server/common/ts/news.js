'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
// ************ IMPORTANT ***********************
// The only pass compatible with client and server directories structure
var DataProviding = require('../../common/ts/data-providing');
var News;
(function (News_1) {
    News_1.newsModel = {
        _id: {
            type: "string"
        },
        date: {
            type: "date"
        },
        text: {
            type: "string"
        },
        uid: {
            type: "string"
        },
        params: {
            type: "Object"
        },
    };
    var News = (function (_super) {
        __extends(News, _super);
        function News(newsData) {
            var self = this;
            _super.call(this, newsData);
        }
        News.prototype.oncreate = function () {
            var self = this;
            var tubeFile = self._datas;
            // Create fields from model
            self.createFields(News_1.newsModel);
            // Fill standard values from model
            self.fillValues(News_1.newsModel, tubeFile);
            // Call base to initialize original values
            _super.prototype.oncreate.call(this);
        };
        Object.defineProperty(News.prototype, "date", {
            get: function () {
                return this._fields['date'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(News.prototype, "text", {
            get: function () {
                return this._fields['text'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(News.prototype, "uid", {
            get: function () {
                return this._fields['uid'];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(News.prototype, "params", {
            get: function () {
                return this._fields['params'];
            },
            enumerable: true,
            configurable: true
        });
        return News;
    })(DataProviding.Dataset);
    News_1.News = News;
})(News || (News = {}));
module.exports = News;

declare module user {
    interface UserEmail {
        userName?: string;
        email?: string;
    }
    interface DbUser extends UserEmail {
        _id: string;
        _password?: string;
        displayName?: string;
        validated?: boolean;
        blocked?: boolean;
        role?: string;
        validationToken?: string;
        validationTime?: number;
        provider?: string;
        failAttempts?: number;
        lastLogin?: number;
        lastIP?: string;
    }
    interface User extends DbUser {
        token?: string;
        photo?: string;
    }
    class Validator {
        private validatePassword(password, password2, cb);
        private validateEmail(email, cb);
        validatePasswordChange(password: string, password2: string, cb: (verrors: Array<validation.Message>) => void): void;
        validatePasswordReset(email: string, cb: (verrors: Array<validation.Message>) => void): void;
        validateSignup(userEmail: UserEmail, password: string, password2: string, cb: (verrors: Array<validation.Message>) => void): void;
        validateLogin(login: string, password: string, cb: (verrors: Array<validation.Message>) => void): void;
    }
}
export = user;

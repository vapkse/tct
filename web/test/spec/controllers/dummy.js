'use strict';

describe('Controller: DummyCtrl', function () {

  // load the controller's module
  beforeEach(module('angularTestApp'));

  var DummyCtrl;
  var scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DummyCtrl = $controller('DummyCtrl', {
      $scope: scope
    });
  }));

  it('should return 0 when multiplying by 0', function () {
    expect(scope.multiply(0, 2)).toEqual(0);
  });

  it('should return Infinity when divising by 0', function() {
    expect(DummyCtrl.division(3, 0)).toEqual(Number.POSITIVE_INFINITY);
  });

  it('should return 3 when adding 1 + 2', function() {
    // function add not testable
  });
});

var path = require('path');
var base = __dirname;
var config = {
    maxFailLoginAttempts: 30,
    path: {
        base: base,
        data: path.join(base, 'data'),
        backup: path.join(base, 'backup'),
        views: path.join(base, 'public'),
        app: path.join(base, 'public'),
        server: path.join(base, 'server'),
        common: path.join(base, 'common'),
        test: path.join(base, 'test'),
        temp: path.join(base, '.tmp'),
        json: path.join(base, 'json'),
        upload: path.join(base, 'uploads'),
        documents: path.join(base, 'documents')
    },
    dev: {
        listenPort: 8080,
        livereload: 38080,
        debugPort: 5860
    },
    prod: {
        listenPort: 80,
        livereload: false
    },
    db: {
        engine: 'tingodb',
        importPath: path.join(base, 'json'),
        mongo: {
            host: '127.0.0.1',
            port: 27017,
            db: 'data',
            opts: {
                auto_reconnect: true,
                safe: true
            }
        },
        tingo: {
            path: path.join(base, 'data'),
            opts: {}
        }
    },
    auth: {
        prod: {
            google: {
                id: '766706097107-om36qtkqlh24ppheq8ia8kovksuabhvj.apps.googleusercontent.com',
                secret: 'kWWbfoZ2RSO66qNgq7A3vaAz',
                redirect: 'http://vapkse.internet-box.ch/auth/google/callback'
            },
            facebook: {
                id: '1703445216567316',
                secret: '0d9890e415be8e4d436404b2c6ed2d82',
                redirect: 'http://vapkse.internet-box.ch/auth/facebook/callback'
            },
            twitter: {
                consumerKey: 'ujtfkOQxFXIZefM5NWIHUCeVa',
                consumerSecret: 'W5nEhbJB2HhSK7ePkF6bRO3P4PSt77jr1xLBTJghQfgB3OpuZs',
                redirect: 'http://vapkse.internet-box.ch/auth/twitter/callback'
            },
            github: {
                id: 'f096682e2358fe84e985',
                secret: 'abb123c2596be162b674ab97f00ced2fee2e3231',
                redirect: 'http://vapkse.internet-box.ch/auth/github/callback'
            },
            local: {
                validationUrl: 'http://vapkse.internet-box.ch/register?id=',
                declineUrl: 'http://vapkse.internet-box.ch/decline?id=',
                changePwdUrl: 'http://vapkse.internet-box.ch/pwdreset?id=',
                unlockUrl: 'http://vapkse.internet-box.ch/unlock?id='
            }
        },
        dev: {
            google: {
                id: '766706097107-om36qtkqlh24ppheq8ia8kovksuabhvj.apps.googleusercontent.com',
                secret: 'kWWbfoZ2RSO66qNgq7A3vaAz',
                redirect: 'http://127.0.0.1:8080/auth/google/callback'
            },
            facebook: {
                id: '1703445216567316',
                secret: '0d9890e415be8e4d436404b2c6ed2d82',
                redirect: 'http://127.0.0.1:8080/auth/facebook/callback'
            },
            twitter: {
                consumerKey: 'IqxfpLSV9bH46TsJtCeWZmcrE',
                consumerSecret: '9DCRA8N0eD6tCvJvrNCAp0ZeQMlkzFCl7iHclF3JV046g9Oj74',
                redirect: 'http://127.0.0.1:8080/auth/twitter/callback'
            },
            github: {
                id: 'e2ffad8884c981367c60',
                secret: 'b93ffe5ed4f6b655dc5eee050f7e42890158938a',
                redirect: 'http://127.0.0.1:8080/auth/github/callback'
            },
            local: {
                validationUrl: 'http://127.0.0.1:8080/register?id=',
                declineUrl: 'http://127.0.0.1:8080/decline?id=',
                changePwdUrl: 'http://127.0.0.1:8080/pwdreset?id=',
                unlockUrl: 'http://127.0.0.1:8080/unlock?id='
            }
        }
    }
};
module.exports = config;
//# sourceMappingURL=config.js.map
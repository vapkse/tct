declare module tct {
    interface OperatorExpression {
        [andOr: string]: Array<FieldExpression>;
    }
    interface FieldExpression {
        [fieldName: string]: RegExp | number | OperatorExpression | FieldExpression;
    }
    interface QueryExpression {
        fieldName: string;
        value?: string | number;
        wholeWord: boolean;
        from?: number;
        to?: number;
    }
    class TubeSearch {
        private _expressions;
        private _isAdvancedExpression;
        private static _searchDataModel;
        constructor();
        queryExpressions: QueryExpression[];
        static searchDataModel: {
            [name: string]: DataProviding.Field;
        };
        static FromSearchExpressions(expressions: Array<QueryExpression>): TubeSearch;
        static FromQueryExpression(expression: string, done: (err: Error, tubeSearch?: TubeSearch) => void): void;
        isAdvancedExpression: boolean;
        private addFieldExpression;
        private static getStringFieldValue;
        getFieldExpressions: (done: (err: Error, result?: FieldExpression[], fields?: {
            [field: string]: number;
        }, users?: OperatorExpression, allUserSearch?: boolean) => void) => void;
        toString: () => string;
    }
}
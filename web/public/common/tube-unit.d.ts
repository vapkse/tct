/// <reference path="./data-providing.d.ts" />

declare module TubeUnit {
    class TubeUnit extends DataProviding.Dataset {
        constructor(unitInfo: TubeUnitData);
        protected oncreate(): void;
        vamax: DataProviding.Field;
        vamaxp: DataProviding.Field;
        pamax: DataProviding.Field;
        vg2max: DataProviding.Field;
        vg2maxp: DataProviding.Field;
        ig2max: DataProviding.Field;
        ig2maxp: DataProviding.Field;
        pg2max: DataProviding.Field;
        vhknmax: DataProviding.Field;
        vhkpmax: DataProviding.Field;
        ikmax: DataProviding.Field;
        ikmaxp: DataProviding.Field;
        vg1nmax: DataProviding.Field;
        vg1nmaxp: DataProviding.Field;
        ig1max: DataProviding.Field;
        ig1maxp: DataProviding.Field;
        cgk: DataProviding.Field;
        cak: DataProviding.Field;
        cga: DataProviding.Field;
    }
}
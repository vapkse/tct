declare module utils {
    class Encoder {
        // Convert HTML entities into numerical entities
        HTML2Numerical(s: string): string;
        // Convert Numerical entities into HTML entities
        NumericalToHTML(s: string): string;
        // HTML Decode numerical and HTML entities back to original values
        htmlDecode(s: string): string;
        // encode an input string into either numerical or HTML entities    
        htmlEncode(s: string, dbl?: boolean): string;
        // Encodes the basic 4 characters used to malform HTML in XSS hacks
        XSSEncode(s: string, en: boolean): string;
        // returns true if a string contains html or numerical encoded entities
        hasEncoded(s: string): boolean;
        // will remove any unicode characters
        stripUnicode(s: string): string;
        // corrects any double encoded &amp; entities e.g &amp;amp;
        correctEncoding(s: string): string;
        swapArrayVals(s: string, arr1: Array<string>, arr2: Array<string>): string;
        // Return the position of this instance in an Array
        inArray(item: string, arr: Array<string>): number;
        // Return a checksum number for this instance
        crc(message: string): number;
    }
}

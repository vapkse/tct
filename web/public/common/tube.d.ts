/// <reference path="./data-providing.d.ts" />
/// <reference path="./tube-unit.d.ts" />

declare module tct {
    class Document extends DataProviding.Dataset {
        private _dataModel;
        constructor(dataModel: any, tubeDoc: TubeDoc);
        protected oncreate(): void;
        filename: DataProviding.Field;
        description: DataProviding.Field;
    }
    class Tube extends DataProviding.Dataset {
        constructor(tubeInfos: TubeSearchResult);
        protected oncreate(): void;
        createArrayFieldSubset(name: string, json?: any): DataProviding.Dataset;
        _id: number;
        name: DataProviding.Field;
        vh1: DataProviding.Field;
        ih1: DataProviding.Field;
        vh2: DataProviding.Field;
        ih2: DataProviding.Field;
        dh: DataProviding.Field;
        notes: DataProviding.Field;
        type: DataProviding.Field;
        base: DataProviding.Field;
        pinout: DataProviding.Field;
        units: Array<TubeUnit.TubeUnit>;
        documents: DataProviding.Field;
        usages: DataProviding.Field;
        getDocumentById: (docId: string) => Document;
        toJson(): any;
    }
}
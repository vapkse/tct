define(["require", "exports"], function (require, exports) {
    'use strict';
    var DataProviding;
    (function (DataProviding) {
        var Dataset = (function () {
            function Dataset(data) {
                this._fields = {};
                this.resetOriginalValues = function () {
                    var self = this;
                    self._isNew = self._id === 0;
                    for (var name in self._fields) {
                        var f = self._fields[name];
                        switch (f.type) {
                            case 'string':
                            case 'number':
                            case 'date':
                            case 'boolean':
                                f._originalValue = f.value;
                                break;
                            case 'objectid':
                                var valuefield = f.valueField || '_id';
                                f._originalValue = f.value && f.value[valuefield];
                                break;
                            case 'array':
                                f._originalValue = f.value ? f.value.length : undefined;
                                break;
                            case 'object':
                                if (f.value) {
                                    if (f.value.resetOriginalValues) {
                                        f.value.resetOriginalValues();
                                    }
                                    else {
                                        f._originalValue = {};
                                        Object.keys(f.value).map(function (n) { return f._originalValue[n] = f.value[n]; });
                                    }
                                }
                                else {
                                    f._originalValue = f.value;
                                }
                                break;
                        }
                    }
                };
                this.getField = function (name) {
                    var self = this;
                    return self._fields[name];
                };
                this.isModified = function () {
                    var self = this;
                    if (self._isNew) {
                        // New item
                        return true;
                    }
                    for (var name in self._fields) {
                        var f = self._fields[name];
                        if (!self.compareFieldValues(f)) {
                            return true;
                        }
                    }
                    return false;
                };
                this.validate = function () {
                    var self = this;
                    var verrors = [];
                    self.validateSet(verrors);
                    return verrors;
                };
                this.validateField = function (field, name) {
                    var self = this;
                    var verrors = [];
                    self.validateFieldSet(verrors, field, name);
                    return verrors;
                };
                var self = this;
                self._datas = data;
                self.oncreate();
            }
            Object.defineProperty(Dataset.prototype, "_id", {
                get: function () {
                    var self = this;
                    return self._datas._id;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Dataset.prototype, "isNew", {
                get: function () {
                    var self = this;
                    return self._isNew;
                },
                set: function (value) {
                    var self = this;
                    self._isNew = value;
                    self._datas._id = undefined;
                },
                enumerable: true,
                configurable: true
            });
            Dataset.prototype.oncreate = function () {
                var self = this;
                self.resetOriginalValues();
            };
            Dataset.prototype.createFields = function (model) {
                var self = this;
                // Create generic fields from model
                for (var name in model) {
                    var m = model[name];
                    var f = {};
                    Object.keys(m).map(function (n) { return f[n] = m[n]; });
                    self._fields[name] = f;
                }
                return model;
            };
            Dataset.prototype.fillValues = function (model, json) {
                var self = this;
                if (!model) {
                    throw 'undefined model';
                }
                Object.keys(model).map(function (name) {
                    var f = self._fields[name];
                    // Check field type
                    switch (f.type) {
                        case 'string':
                        case 'number':
                        case 'date':
                        case 'boolean':
                            f.value = json[name];
                            break;
                        case 'array':
                            f.value = [];
                            break;
                        case 'object':
                            f.value = json[name] || {};
                            break;
                    }
                });
            };
            Dataset.prototype.createArrayFieldSubset = function (name, json) {
                throw 'Create subset method must be overrided and return a new dataset of the sepcified name.';
            };
            Dataset.prototype.isDirtyField = function (field) {
                var self = this;
                return !self.compareFieldValues(field);
            };
            Dataset.prototype.compareFieldValues = function (field) {
                var self = this;
                var current = field.value;
                var original = field._originalValue;
                if (current === null) {
                    current = undefined;
                }
                if (original === null) {
                    original = undefined;
                }
                if (current === undefined && original === undefined) {
                    return true;
                }
                else if ((current === undefined && original !== undefined) || (original === undefined && current !== undefined)) {
                    return false;
                }
                switch (field.type) {
                    case 'objectid':
                        var valuefield = field.valueField || '_id';
                        return current[valuefield] === original;
                    case 'array':
                        if (current.length != original) {
                            return false;
                        }
                        else {
                            for (var i = 0; i < current.length; i++) {
                                if (current[i].isModified) {
                                    if (current[i].isModified()) {
                                        return false;
                                    }
                                }
                            }
                        }
                        return true;
                    case 'object':
                        if (current.isModified) {
                            return !current.isModified();
                        }
                        else {
                            for (var name in current) {
                                if (current[name] !== original[name]) {
                                    return false;
                                }
                            }
                            return true;
                        }
                    default:
                        return current === original;
                }
            };
            Dataset.prototype.toJson = function () {
                var self = this;
                var result = {
                    _id: self._id
                };
                for (var name in self._fields) {
                    if (self._fields[name].value !== undefined) {
                        var f = self._fields[name];
                        var value;
                        switch (f.type) {
                            case 'objectid':
                                var valuefield = f.valueField || '_id';
                                value = f.value[valuefield];
                                break;
                            case 'array':
                                value = [];
                                f.value.map(function (v) {
                                    if (v.toJson) {
                                        value.push(v.toJson());
                                    }
                                    else {
                                        value.push(v);
                                    }
                                });
                                break;
                            case 'object':
                                value = f.value.toJson ? f.value.toJson() : f.value;
                                break;
                            default:
                                value = f.value;
                                break;
                        }
                        result[name] = value;
                    }
                }
                return result;
            };
            Dataset.prototype.validateSet = function (verrors, dataIndex) {
                var self = this;
                for (var name in self._fields) {
                    var f = self._fields[name];
                    switch (f.type) {
                        case 'array':
                            var array = f.value;
                            if (f.required && (!array || array.length === 0)) {
                                verrors.push({
                                    type: 'error',
                                    name: 'msg-mand',
                                    message: 'The field \\0 is mandatory.',
                                    fieldName: name,
                                    params: { 0: name }
                                });
                            }
                            if (array) {
                                for (var v = 0; v < array.length; v++) {
                                    if (array[v].validateSet) {
                                        array[v].validateSet(verrors, v);
                                    }
                                }
                                ;
                            }
                            break;
                        case 'object':
                            self.validateFieldSet(verrors, f, name);
                            var subset = f.value;
                            if (subset && subset.validateFieldSet) {
                                subset.validateSet(verrors, dataIndex);
                            }
                            break;
                        default:
                            self.validateFieldSet(verrors, f, name, dataIndex);
                            break;
                    }
                }
            };
            Dataset.prototype.validateFieldSet = function (verrors, field, name, dataIndex) {
                var self = this;
                var value = field.value;
                if (value === undefined || value === null || value === '' || (value === 0 && field.type === 'objectid')) {
                    if (field.required) {
                        verrors.push({
                            type: 'error',
                            name: 'msg-mand',
                            message: 'The field \\0 is mandatory.',
                            fieldName: name,
                            tooltip: true,
                            dataIndex: dataIndex,
                            params: { 0: name }
                        });
                    }
                }
                else {
                    if (field.maxLength && value && value.toString().length > field.maxLength) {
                        verrors.push({
                            type: 'error',
                            name: 'msg-maxlength',
                            message: 'The length of the field \\0 exceeds the maximum allowed of \\1',
                            fieldName: name,
                            tooltip: true,
                            dataIndex: dataIndex,
                            params: {
                                0: name,
                                1: field.maxLength
                            }
                        });
                    }
                    if (field.min !== undefined || field.max !== undefined) {
                        var num = parseFloat(value);
                        if (isNaN(num)) {
                            verrors.push({
                                type: 'error',
                                name: 'msg-wrongtype',
                                message: 'Invalid value type for the field \\0',
                                fieldName: name,
                                tooltip: true,
                                dataIndex: dataIndex,
                                params: {
                                    0: name
                                }
                            });
                        }
                        else if (field.max !== undefined && num > field.max) {
                            verrors.push({
                                type: 'error',
                                name: 'msg-maxerr',
                                message: 'The value of the field \\0 exceeds the maximum allowed of \\1',
                                fieldName: name,
                                tooltip: true,
                                dataIndex: dataIndex,
                                params: {
                                    0: name,
                                    1: field.max
                                }
                            });
                        }
                        else if (field.min !== undefined && num < field.min) {
                            verrors.push({
                                type: 'error',
                                name: 'msg-minerr',
                                message: 'The value of the field \\0 is less than the minimum value allowed of \\1',
                                fieldName: name,
                                tooltip: true,
                                dataIndex: dataIndex,
                                params: {
                                    0: name,
                                    1: field.min
                                }
                            });
                        }
                    }
                }
            };
            return Dataset;
        })();
        DataProviding.Dataset = Dataset;
    })(DataProviding || (DataProviding = {}));
    return DataProviding;
});

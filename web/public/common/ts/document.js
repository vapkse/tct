var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", '../../common/ts/data-providing'], function (require, exports, DataProviding) {
    'use strict';
    var Document;
    (function (Document_1) {
        Document_1.documentModel = {
            _id: {
                type: "string"
            },
            filename: {
                type: "string",
                maxLength: 255,
                required: true
            },
            description: {
                type: "string",
                maxLength: 255,
                required: true
            }
        };
        var Document = (function (_super) {
            __extends(Document, _super);
            function Document(tubeDoc) {
                var self = this;
                _super.call(this, tubeDoc);
            }
            Document.prototype.oncreate = function () {
                var self = this;
                var tubeFile = self._datas;
                // Create fields from model
                self.createFields(Document_1.documentModel);
                // Fill standard values from model
                self.fillValues(Document_1.documentModel, tubeFile);
                // Call base to initialize original values
                _super.prototype.oncreate.call(this);
            };
            Object.defineProperty(Document.prototype, "filename", {
                get: function () {
                    return this._fields['filename'];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Document.prototype, "description", {
                get: function () {
                    return this._fields['description'];
                },
                enumerable: true,
                configurable: true
            });
            return Document;
        })(DataProviding.Dataset);
        Document_1.Document = Document;
    })(Document || (Document = {}));
    return Document;
});

define(["require", "exports"], function (require, exports) {
    var user;
    (function (user) {
        var Validator = (function () {
            function Validator() {
            }
            Validator.prototype.validatePassword = function (password, password2, cb) {
                var verrors = [];
                if (!password) {
                    verrors.push({
                        type: 'error',
                        name: 'msg-pwdmand',
                        message: 'Password is mandatory.',
                        tooltip: true,
                        fieldCss: 'error',
                        fieldName: '#user_password'
                    });
                    verrors.push({
                        type: 'error',
                        name: 'msg-pwdmand',
                        message: 'Password is mandatory.',
                        tooltip: true,
                        fieldCss: 'error',
                        fieldName: '#pwd2'
                    });
                }
                else if (password !== password2) {
                    verrors.push({
                        type: 'error',
                        name: 'msg-pwddosntmatch',
                        message: 'The passwords doesnt match.',
                        tooltip: true,
                        fieldCss: 'error',
                        fieldName: '#user_password'
                    });
                    verrors.push({
                        type: 'error',
                        name: 'msg-pwddosntmatch',
                        message: 'The passwords doesnt match.',
                        tooltip: true,
                        fieldCss: 'error',
                        fieldName: '#pwd2'
                    });
                }
                else if (password.length < 5) {
                    var uid = '';
                    verrors.push({
                        type: 'error',
                        name: 'msg-pwdtooshort',
                        message: 'The password must be at least 5 characters.',
                        tooltip: true,
                        fieldCss: 'error',
                        fieldName: '#user_password'
                    });
                    verrors.push({
                        type: 'error',
                        name: 'msg-pwdtooshort',
                        message: 'The password must be at least 5 characters.',
                        tooltip: true,
                        fieldCss: 'error',
                        fieldName: '#pwd2'
                    });
                }
                cb(verrors);
            };
            Validator.prototype.validateEmail = function (email, cb) {
                var verrors = [];
                if (!email) {
                    verrors.push({
                        type: 'error',
                        name: 'msg-emailmand',
                        message: 'Email is mandatory.',
                        tooltip: true,
                        fieldCss: 'error',
                        fieldName: '#user_email'
                    });
                }
                else {
                    // validate mail
                    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (!re.test(email)) {
                        verrors.push({
                            type: 'error',
                            name: 'msg-invemail',
                            message: 'Invalid email.',
                            tooltip: true,
                            fieldCss: 'error',
                            fieldName: '#user_email',
                        });
                    }
                }
                cb(verrors);
            };
            Validator.prototype.validatePasswordChange = function (password, password2, cb) {
                var self = this;
                self.validatePassword(password, password2, function (verrors) {
                    cb(verrors);
                });
            };
            Validator.prototype.validatePasswordReset = function (email, cb) {
                var self = this;
                self.validateEmail(email, function (verrors) {
                    cb(verrors);
                });
            };
            Validator.prototype.validateSignup = function (userEmail, password, password2, cb) {
                var self = this;
                var verrors = [];
                if (!userEmail.userName) {
                    verrors.push({
                        type: 'error',
                        name: 'msg-usermand',
                        message: 'User is mandatory.',
                        tooltip: true,
                        fieldCss: 'error',
                        fieldName: '#user_username'
                    });
                }
                else if (userEmail.userName.length < 5) {
                    verrors.push({
                        type: 'error',
                        name: 'msg-usernametooshort',
                        message: 'The user name must be at least 5 characters.',
                        tooltip: true,
                        fieldCss: 'error',
                        fieldName: '#user_username'
                    });
                }
                self.validateEmail(userEmail.email, function (vee) {
                    vee.map(function (v) { return verrors.push(v); });
                    self.validatePassword(password, password2, function (vep) {
                        vep.map(function (v) { return verrors.push(v); });
                        cb(verrors);
                    });
                });
            };
            Validator.prototype.validateLogin = function (login, password, cb) {
                var verrors = [];
                if (!login) {
                    verrors.push({
                        type: 'error',
                        name: 'msg-logmand',
                        message: 'Login is mandatory.',
                        tooltip: true,
                        fieldCss: 'error',
                        fieldName: '#user_login'
                    });
                }
                if (!password) {
                    var uid = "";
                    verrors.push({
                        type: 'error',
                        name: 'msg-pwdmand',
                        message: 'Password is mandatory.',
                        tooltip: true,
                        fieldCss: 'error',
                        fieldName: '#user_password'
                    });
                }
                cb(verrors);
            };
            return Validator;
        })();
        user.Validator = Validator;
    })(user || (user = {}));
    return user;
});

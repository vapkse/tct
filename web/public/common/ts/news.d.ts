import DataProviding = require('../../common/ts/data-providing');
declare module News {
    var newsModel: {
        _id: {
            type: string;
        };
        date: {
            type: string;
        };
        text: {
            type: string;
        };
        uid: {
            type: string;
        };
        params: {
            type: string;
        };
    };
    class News extends DataProviding.Dataset {
        constructor(newsData: NewsData);
        protected oncreate(): void;
        date: DataProviding.Field;
        text: DataProviding.Field;
        uid: DataProviding.Field;
        params: DataProviding.Field;
    }
}
export = News;

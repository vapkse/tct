declare module utils {
    class Encoder {
        private static EncodeType;
        private static isEmpty(val);
        private static arr1;
        private static arr2;
        static HTML2Numerical(s: string): string;
        static NumericalToHTML(s: string): string;
        private static numEncode(s);
        static htmlDecode(s: string): string;
        static htmlEncode(s: string, dbl?: boolean): string;
        static XSSEncode(s: string, en: boolean): string;
        static hasEncoded(s: string): boolean;
        static stripUnicode(s: string): string;
        static correctEncoding(s: string): string;
        static swapArrayVals(s: string, arr1: Array<string>, arr2: Array<string>): string;
        static inArray(item: string, arr: Array<string>): number;
        static crc(message: string): number;
    }
}
export = utils;

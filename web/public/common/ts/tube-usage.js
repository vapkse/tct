var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", '../../common/ts/data-providing'], function (require, exports, DataProviding) {
    'use strict';
    var TubeUsage;
    (function (TubeUsage_1) {
        TubeUsage_1.tubeUsageModel = {
            name: {
                type: "string",
                maxLength: 255,
                required: true
            },
            note: {
                type: "string",
                maxLength: 10000
            },
            unit: {
                type: "number",
                required: true
            },
            mode: {
                type: "string",
                required: true
            },
            va: {
                type: "number",
                min: 0,
                max: 9999
            },
            iazero: {
                type: "number",
                min: 0,
                max: 9999
            },
            vinpp: {
                type: "number",
                min: -999,
                max: 999
            },
            load: {
                type: "number",
                min: 0,
                max: 9999999
            },
            /*iamax: {
                type: "number",
                min: 0,
                max: 9999
            },
            vg2: {
                type: "number",
                min: 0,
                max: 9999
            },
            ig2zero: {
                type: "number",
                min: 0,
                max: 9999
            },
            ig2max: {
                type: "number",
                min: 0,
                max: 9999
            },
            vg1: {
                type: "number",
                min: -999,
                max: 999
            },
            ig1zero: {
                type: "number",
                min: 0,
                max: 9999
            },
            ig1max: {
                type: "number",
                min: 0,
                max: 9999
            },
            pout: {
                type: "number",
                min: 0,
                max: 9999
            },
            gain: {
                type: "number",
                min: 0,
                max: 9999
            },*/
            traces: {
                type: "string"
            },
            zoom: {
                type: "object"
            },
            series: {
                type: "object"
            }
        };
        var TubeUsage = (function (_super) {
            __extends(TubeUsage, _super);
            function TubeUsage(usageInfo) {
                _super.call(this, usageInfo);
            }
            TubeUsage.prototype.oncreate = function () {
                var self = this;
                var usageInfo = self._datas;
                // Create fields from model
                self.createFields(TubeUsage_1.tubeUsageModel);
                // Fill standard values from model
                self.fillValues(TubeUsage_1.tubeUsageModel, usageInfo);
                // Call base to initialize original values
                _super.prototype.oncreate.call(this);
            };
            Object.defineProperty(TubeUsage.prototype, "name", {
                get: function () {
                    return this._fields['name'];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TubeUsage.prototype, "note", {
                get: function () {
                    return this._fields['note'];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TubeUsage.prototype, "unit", {
                get: function () {
                    return this._fields['unit'];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TubeUsage.prototype, "mode", {
                get: function () {
                    return this._fields['mode'];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TubeUsage.prototype, "va", {
                get: function () {
                    return this._fields['va'];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TubeUsage.prototype, "iazero", {
                get: function () {
                    return this._fields['iazero'];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TubeUsage.prototype, "vinpp", {
                get: function () {
                    return this._fields['vinpp'];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TubeUsage.prototype, "load", {
                get: function () {
                    return this._fields['load'];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TubeUsage.prototype, "traces", {
                /*public get iamax(): DataProviding.Field {
                    return this._fields['iamax'];
                }
                public get vg2(): DataProviding.Field {
                    return this._fields['vg2'];
                }
                public get ig2zero(): DataProviding.Field {
                    return this._fields['ig2zero'];
                }
                public get ig2max(): DataProviding.Field {
                    return this._fields['ig2max'];
                }
                public get vg1(): DataProviding.Field {
                    return this._fields['vg1'];
                }
                public get ig1zero(): DataProviding.Field {
                    return this._fields['ig1zero'];
                }
                public get ig1max(): DataProviding.Field {
                    return this._fields['ig1max'];
                }
                public get pout(): DataProviding.Field {
                    return this._fields['pout'];
                }
                public get gain(): DataProviding.Field {
                    return this._fields['gain'];
                }*/
                get: function () {
                    return this._fields['traces'];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TubeUsage.prototype, "zoom", {
                get: function () {
                    return this._fields['zoom'];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TubeUsage.prototype, "series", {
                get: function () {
                    return this._fields['series'];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TubeUsage.prototype, "effectiveLoad", {
                get: function () {
                    var self = this;
                    // Calc new load param
                    switch (self.mode.value) {
                        case 'se':
                            return self.load.value;
                        case 'pp':
                            return self.load.value / 2;
                        case '2pp':
                            return self.load.value;
                        case '3pp':
                            return self.load.value * 3 / 2;
                        case '4pp':
                            return self.load.value * 2;
                        case '5pp':
                            return self.load.value * 5 / 2;
                        case '6pp':
                            return self.load.value * 3;
                    }
                },
                set: function (load) {
                    var self = this;
                    // Calc new load param
                    switch (self.mode.value) {
                        case 'se':
                            self.load.value = load;
                            break;
                        case 'pp':
                            self.load.value = load * 2;
                            break;
                        case '2pp':
                            self.load.value = load;
                            break;
                        case '3pp':
                            self.load.value = load * 2 / 3;
                            break;
                        case '4pp':
                            self.load.value = load / 2;
                            break;
                        case '5pp':
                            self.load.value = load * 2 / 5;
                            break;
                        case '6pp':
                            self.load.value = load / 3;
                            break;
                    }
                },
                enumerable: true,
                configurable: true
            });
            return TubeUsage;
        })(DataProviding.Dataset);
        TubeUsage_1.TubeUsage = TubeUsage;
    })(TubeUsage || (TubeUsage = {}));
    return TubeUsage;
});

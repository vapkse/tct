define(["require", "exports", '../../common/ts/tube-unit', '../../common/ts/tube'], function (require, exports, TubeUnit, Tube) {
    'use strict';
    var tct;
    (function (tct) {
        var TubeSearch = (function () {
            function TubeSearch() {
                this._expressions = [];
                this._isAdvancedExpression = false;
                this.addFieldExpression = function (result, qe, fields) {
                    var self = this;
                    var getField = function (name) {
                        var dataModel = TubeSearch.searchDataModel;
                        var field = dataModel[name];
                        if (field && field.isUnit) {
                            fields['_tiar.units.' + name] = 0;
                            name = 'units.' + name;
                        }
                        return {
                            type: !field ? 'string' : field.type,
                            name: name
                        };
                    };
                    // No field or text field, search anyway
                    var fieldExpr = {};
                    // Search if a filed with this name exist in the data model
                    var fieldInfos = getField(qe.fieldName);
                    var value;
                    if (qe.value !== undefined) {
                        if (fieldInfos.type === 'string') {
                            value = TubeSearch.getStringFieldValue(qe);
                        }
                        else {
                            value = qe.value;
                        }
                    }
                    else if (qe.from !== undefined || qe.to !== undefined) {
                        value = {};
                        if (qe.from !== undefined) {
                            value.$gte = qe.from;
                        }
                        if (qe.to !== undefined) {
                            value.$lte = qe.to;
                        }
                    }
                    if (value !== undefined) {
                        fieldExpr[fieldInfos.name] = value;
                        if (!result[fieldInfos.name]) {
                            result[fieldInfos.name] = fieldExpr;
                        }
                        else {
                            var opex = result[fieldInfos.name];
                            if (opex['$or']) {
                                opex['$or'].push(fieldExpr);
                            }
                            else {
                                var prevExpr = result[fieldInfos.name];
                                opex = result[fieldInfos.name] = {
                                    $or: [
                                        prevExpr,
                                        fieldExpr
                                    ]
                                };
                            }
                        }
                    }
                    else {
                        throw 'invalid expression';
                    }
                };
                this.getFieldExpressions = function (done) {
                    var self = this;
                    var result = {};
                    var fields = {};
                    var quser = [];
                    var allUsers = false;
                    try {
                        self._expressions.map(function (qe) {
                            if (qe.fieldName.match(/^user$/i)) {
                                if (!allUsers) {
                                    var userName = qe.value.toString();
                                    allUsers = userName.match(/^all$/i) !== null;
                                    if (allUsers) {
                                        quser = [];
                                    }
                                    else {
                                        quser.push(qe);
                                    }
                                }
                            }
                            else {
                                self.addFieldExpression(result, qe, fields);
                            }
                        });
                        // Map object to and array
                        var arr = [];
                        Object.keys(result).map(function (name) { return arr.push(result[name]); });
                        // Map users to query expression
                        var users;
                        if (quser.length) {
                            var feexps = [];
                            users = {
                                $or: feexps
                            };
                            var getUserFieldValue = function (qe) {
                                if (qe.wholeWord) {
                                    return new RegExp('^' + qe.value + '$', 'i');
                                }
                                else {
                                    return new RegExp(qe.value.toString(), 'i');
                                }
                            };
                            quser.forEach(function (qe) {
                                feexps.push({ email: getUserFieldValue(qe) });
                                feexps.push({ userName: getUserFieldValue(qe) });
                            });
                        }
                        done(null, arr, fields, users, allUsers);
                    }
                    catch (e) {
                        done({
                            name: 'unmanaged_error',
                            message: e.toString()
                        });
                    }
                };
                this.toString = function () {
                    var self = this;
                    var result = [];
                    if (self._expressions && self._expressions.length) {
                        self._expressions.map(function (qe) {
                            if (qe.fieldName === '_text') {
                                if (qe.wholeWord) {
                                    result.push('"' + qe.value + '"');
                                }
                                else {
                                    result.push(qe.value.toString());
                                }
                            }
                            else {
                                if (qe.from !== undefined && qe.from !== null) {
                                    result.push(qe.fieldName + '>' + qe.from);
                                }
                                if (qe.to !== undefined && qe.to !== null) {
                                    result.push(qe.fieldName + '<' + qe.to);
                                }
                                if (qe.value !== undefined && qe.value !== null) {
                                    if (qe.wholeWord) {
                                        result.push(qe.fieldName + '="' + qe.value + '"');
                                    }
                                    else {
                                        result.push(qe.fieldName + '=' + qe.value);
                                    }
                                }
                            }
                        });
                    }
                    return result.join(' ');
                };
                var self = this;
            }
            Object.defineProperty(TubeSearch.prototype, "queryExpressions", {
                get: function () {
                    var self = this;
                    return self._expressions;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TubeSearch, "searchDataModel", {
                get: function () {
                    if (!TubeSearch._searchDataModel) {
                        TubeSearch._searchDataModel = {};
                        Object.keys(Tube.tubeModel).forEach(function (name) {
                            TubeSearch._searchDataModel[name] = Tube.tubeModel[name];
                        });
                        Object.keys(TubeUnit.tubeUnitModel).forEach(function (name) {
                            var field = TubeUnit.tubeUnitModel[name];
                            field.isUnit = true;
                            TubeSearch._searchDataModel[name] = field;
                        });
                    }
                    return TubeSearch._searchDataModel;
                },
                enumerable: true,
                configurable: true
            });
            TubeSearch.FromSearchExpressions = function (expressions) {
                var instance = new TubeSearch();
                instance._expressions = expressions;
                return instance;
            };
            // Parse expression to a fieldExpr array and retirn the class when available
            TubeSearch.FromQueryExpression = function (expression, done) {
                var self = new TubeSearch();
                try {
                    // Split by expressions ignore quotes               
                    var exps = expression.match(/(?:[^\s=<>"]+|"[^"]*"|[=<>\s])?/g);
                    var getExpression = function (index, cb) {
                        // Add expression to the class collection and return index
                        var addExpression = function (qexp) {
                            self._expressions.push(qexp);
                            cb(index);
                        };
                        var name = exps[index];
                        if (++index >= exps.length || exps[index] === '' || exps[index] === ' ') {
                            var wholeWord = name.match(/"([^"]*)"/);
                            addExpression({
                                fieldName: '_text',
                                value: wholeWord ? wholeWord[1] : name,
                                wholeWord: wholeWord !== null
                            });
                        }
                        else {
                            if (!exps[index].match('[=<>]')) {
                                done({
                                    name: 'invalid_expr',
                                    message: 'Invalid search parameters.'
                                });
                                return;
                            }
                            var operator = exps[index];
                            if (++index >= exps.length || exps[index] === '' || exps[index] === ' ') {
                                done({
                                    name: 'invalid_expr',
                                    message: 'Invalid search parameters.'
                                });
                                return;
                            }
                            var value = exps[index];
                            ++index;
                            self._isAdvancedExpression = true;
                            if (name.match(/^user$/i)) {
                                var wholeWord = value.match(/"([^"]*)"/);
                                addExpression({
                                    fieldName: name,
                                    value: wholeWord ? wholeWord[1] : value,
                                    wholeWord: wholeWord !== null
                                });
                            }
                            else {
                                // Search if a field with this name exist in the data model
                                var field = TubeSearch.searchDataModel[name];
                                if (!field || field.type === 'string') {
                                    var wholeWord = value.match(/"([^"]*)"/);
                                    addExpression({
                                        fieldName: name,
                                        value: wholeWord ? wholeWord[1] : value,
                                        wholeWord: wholeWord !== null
                                    });
                                }
                                else if (field.type === 'number') {
                                    var fval = parseFloat(value);
                                    if (isNaN(fval)) {
                                        done({
                                            name: 'invalid_value',
                                            message: 'Invalid value parameter: ' + value
                                        });
                                        return;
                                    }
                                    var fillValue = function (q) {
                                        if (operator === '<') {
                                            q.to = fval;
                                        }
                                        else if (operator === '>') {
                                            q.from = fval;
                                        }
                                        else {
                                            q.value = fval;
                                        }
                                    };
                                    // Search an existing expression
                                    var qe;
                                    for (var i = 0; i < self._expressions.length; i++) {
                                        var q = self._expressions[i];
                                        if (q.fieldName === name) {
                                            fillValue(q);
                                            cb(index);
                                            return;
                                        }
                                    }
                                    qe = {
                                        fieldName: name,
                                        wholeWord: false,
                                    };
                                    fillValue(qe);
                                    addExpression(qe);
                                }
                                else {
                                    // lookups, value is an object id
                                    addExpression({
                                        fieldName: name,
                                        value: parseInt(value),
                                        wholeWord: false
                                    });
                                }
                            }
                        }
                    };
                    var next = function (iexp) {
                        if (++iexp >= exps.length) {
                            done(null, self);
                        }
                        else {
                            getExpression(iexp, next);
                        }
                    };
                    getExpression(0, next);
                }
                catch (e) {
                    done({
                        name: 'unmanaged_error',
                        message: e.toString()
                    });
                }
            };
            Object.defineProperty(TubeSearch.prototype, "isAdvancedExpression", {
                get: function () {
                    var self = this;
                    return self._isAdvancedExpression;
                },
                enumerable: true,
                configurable: true
            });
            TubeSearch.getStringFieldValue = function (qe) {
                if (qe.wholeWord) {
                    return new RegExp('(^|\\s)' + qe.value + '($|\\s)', 'i');
                }
                else {
                    return new RegExp(qe.value.toString(), 'i');
                }
            };
            return TubeSearch;
        })();
        tct.TubeSearch = TubeSearch;
    })(tct || (tct = {}));
    return tct;
});

declare module DataProviding {
    interface Field {
        _originalValue?: any;
        value: any;
        type: string;
        required?: boolean;
        min?: any;
        max?: any;
        maxLength?: number;
        valueField?: string;
        textField?: string;
        lookupurl?: string;
    }
    class Dataset {
        protected _isNew: boolean;
        protected _datas: any;
        protected _fields: {
            [index: string]: Field;
        };
        constructor(data: any);
        _id: any;
        isNew: boolean;
        protected oncreate(): void;
        protected createFields(model: {
            [name: string]: Field;
        }): {
            [name: string]: Field;
        };
        protected fillValues(model: {
            [name: string]: Field;
        }, json: any): void;
        resetOriginalValues: () => void;
        createArrayFieldSubset(name: string, json?: any): Dataset;
        getField: (name: string) => Field;
        isModified: () => boolean;
        isDirtyField(field: Field): boolean;
        protected compareFieldValues(field: Field): boolean;
        toJson(): any;
        validate: () => validation.Message[];
        validateField: (field: Field, name?: string) => validation.Message[];
        protected validateSet: (verrors: validation.Message[], dataIndex?: number) => void;
        protected validateFieldSet: (verrors: validation.Message[], field: Field, name: string, dataIndex?: number) => void;
    }
}

declare module DataTables {
    export interface Settings {
        responsive?: any
    }
    
    export interface ColumnSettings{
        responsivePriority?: number
    }
    
    export interface DataTableExt {
        oStdClasses: {
            sFilter: string,
            sFilterInput: string,
            sFooterTH: string,
            sHeaderTH: string,
            sInfo: string,
            sJUIFooter: string,
            sJUIHeader: string,
            sLength: string,
            sLengthSelect: string,
            sNoFooter: string,
            sPageButton: string,
            sPageButtonActive: string,
            sPageButtonDisabled: string,
            sPaging: string,
            sProcessing: string,
            sRowEmpty: string,
            sScrollBody: string,
            sScrollFoot: string,
            sScrollFootInner: string,
            sScrollHead: string,
            sScrollHeadInner: string,
            sScrollWrapper: string,
            sSortAsc: string,
            sSortColumn: string,
            sSortDesc: string,
            sSortIcon: string,
            sSortJUI: string,
            sSortJUIAsc: string,
            sSortJUIAscAllowed: string,
            sSortJUIDesc: string,
            sSortJUIDescAllowed: string,
            sSortJUIWrapper: string,
            sSortable: string,
            sSortableAsc: string,
            sSortableDesc: string,
            sSortableNone: string,
            sStripeEven: string,
            sStripeOdd: string,
            sTable: string,
            sWrapper: string,
        }
    }
}

interface JQuery {
    dataTableExt: DataTables.DataTableExt;
}
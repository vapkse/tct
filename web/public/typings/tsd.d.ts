/// <reference path="jquery/jquery.d.ts" />
/// <reference path="jqueryui/jqueryui.d.ts" />
/// <reference path="jquery.dataTables/jquery.dataTables.d.ts" />
/// <reference path="jquery.dataTables-extension/jquery.dataTables-extension.d.ts" />
/// <reference path="highcharts/highstock.d.ts" />
/// <reference path="highcharts/highcharts.d.ts" />
/// <reference path="highcharts-extension/highcharts-extension.d.ts" />
/// <reference path="interactjs/interact.d.ts" />
/// <reference path="google.analytics/ga.d.ts" />
/// <reference path="metro/metro.d.ts" />
/// <reference path="requirejs/require.d.ts" />
/// <reference path="socket.io-client/socket.io-client.d.ts" />
/// <reference path="microsoft/microsoft-translator.d.ts" />
/// <reference path="theme.d.ts" />
/// <reference path="../../common/typings/tsd.d.ts" />

interface JQueryStatic {
	io: SocketIOClientStatic
}

interface JQueryMouseEventObject {
    buttons: number,
}

interface Window{
    XMLHttpRequest: XMLHttpRequest
}

interface Event{
    deltaX: number,
    deltaY: number,
    layerX: number,
    layerY: number    
}

interface Math {
    sign: (n: number) => number;
}
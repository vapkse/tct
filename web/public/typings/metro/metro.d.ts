declare module metro {
    interface DialogOptions {
        overlay?: boolean,
        overlayClickClose?: boolean,
        overlayColor?: string,
        type?: string,
        background?: string,
        color?: string,
        hide?: boolean,
        width?: number,
        height?: number,
        show?: boolean,
        closeButton?: boolean,
        windowsStyle?: boolean
    }

    interface WizardButton {
        show?: boolean,
        title?: string,
        cls?: string,
        group?: string
    }

    interface WizardButtons {
        cancel?: boolean | WizardButton,
        help?: boolean | WizardButton,
        prior?: boolean | WizardButton,
        next?: boolean | WizardButton,
        finish?: boolean | WizardButton
    }

    interface WizardOptions {
        stepper?: boolean,              // Default: true. Show, hide stepper subcomponent 
        stepperType?: string,           // Define a type of stepper: square, cycle, diamond 
        stepperClickable?: boolean,     // Default: false. Change wizard step by stepper item click 
        startPage?: number,             // Determines which page to start the wizard 
        finishStep?: number,            // Determines which page to finish the wizard and activate finish button 
        locale?: string,                // Default: en. Determines locale (language) for wizard buttons 
        buttons?: WizardButtons         // When a declarative definition of parameters parameter type of the object is specified in JSON format 
        onCancel?: Function,
        onPrior?: Function,
        onNext?: Function,
        onFinish?: Function,
        onPage?: Function,
        onStepClick?: Function
    }

    interface CharmOptions {
        position: string
    }

    interface AccordionOptions extends JQueryUI.AccordionOptions {
        onFrameOpen: (frame: JQuery) => boolean;
    }

    interface Stepper {
        _positioningSteps: () => void,
        stepTo: (step: number) => void,
    }

    interface StepperOptions {
        steps?: number,
        start?: number,
        type?: string,
        clickable?: boolean,
        onStep?: (index: number, step: number) => void,
        onStepClick?: (index: number, step: number) => void,
    }
}

interface JQueryStatic {
    StartScreen(): void,
}

interface JQuery {
    wizard(): JQueryUI.Widget;
    wizard(method: string): JQueryUI.Widget;
    wizard(options: metro.WizardOptions): JQueryUI.Widget;
    charm(): JQueryUI.Widget;
    charm(method: string): JQueryUI.Widget;
    charm(options: metro.CharmOptions): JQueryUI.Widget;
    accordion(options: metro.AccordionOptions): JQuery;
    stepper(options?: metro.StepperOptions): JQuery;
}


interface HighchartsChartObject {
    showResetZoom: () => void,
    downloadXLS: () => void,
    downloadCSV: () => void,
}

interface HighchartsAxisObject {
    dataMin?: number,
    dataMax?: number,
    options?: HighchartsAxisOptions
}

interface HighchartsAxisEvent {
    userMin?: number,
    userMax?: number
}

interface HighchartsIndividualSeriesOptions {
    dragMinX?: number,
    dragMaxX?: number,
    dragMinY?: number,
    dragMaxY?: number,
    vg1?: number,
    umax?: number,
    imax?: number,
    color?: string,
}

interface HighchartsSeriesObject {
    index?: number,
    points?: Array<HighchartsPointObject>,
    graph?: {
        element: SVGPathElement
    },
    group?: {
        element: SVGPathElement
    },
    markerGroup?: {
        element: SVGGElement
    }
}

interface HighchartsPointObject {
    plotX?: number,
    plotY?: number,
    pathLength?: number,
    graphic?: {
        element: SVGPathElement
    }
}



'use strict';

module DataProviding {
    export interface DataFormEvent extends JQueryEventObject {
        value: Dataset;
    }

    export interface DataFormOptions {

    }

    export class DataForm {
        public options: DataFormOptions = {};
        protected _dataset: Dataset;
        public element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject, data?: DataFieldEventData) => boolean;
        protected _create = function() {
            var self: DataForm = this;
            self.refresh();
            setTimeout(function() {
                self.onload();
            }, 0)
        };

        public setOptions = function(value: DataFormOptions) {
            var self: DataForm = this;
            self.options = value;
            self.refresh();
            return self;
        }

        protected onchange = function(data: DataFieldEventData): boolean {
            var self: DataForm = this;
            var e = jQuery.Event('change');
            data.dataForm = self.element;
            return self._trigger('change', e, data);
        }

        protected onfocus = function(data: DataFieldEventData): boolean {
            var self: DataForm = this;
            var e = jQuery.Event('focus');
            data.dataForm = self.element;
            return self._trigger('focus', e, data);
        }
        
        protected onlookuprequired = function(data: DataFieldRequiredEventData): boolean {
            var self: DataForm = this;
            var e = jQuery.Event('lookuprequired');
            data.dataForm = self.element;
            return self._trigger('lookuprequired', e, data);
        }

        protected onload = function() {
            var self: DataForm = this;
            var evt = <DataFormEvent>jQuery.Event("load");
            evt.value = self._dataset;
            self._trigger('load', evt);
        }

        public dataset = function(dataset?: Dataset) {
            var self: DataForm = this;
            if (dataset !== undefined) {
                self._dataset = dataset;
                self.refresh();
            }
            return self._dataset;
        }

        public refresh = function() {
            var self: DataForm = this;
            var containers = self.element.find('[data-container]:not([data-form] [data-container] [data-container])');
            containers.each(function(index: number, element: Element) {
                var $element = $(element);
                var isCreated = $element.is(':data-providing-datacontainer');
                if (isCreated) {
                    $element.datacontainer('dataset', self._dataset);
                } else {
                    $element.datacontainer({}).on('datacontainerchange', function(e: JQueryEventObject, data: DataFieldEventData) {
                        return self.onchange(data);
                    }).on('datacontainerlookuprequired', function(e: JQueryEventObject, data: DataFieldRequiredEventData) {
                        return self.onlookuprequired(data);
                    }).on('datacontainerfocus', function(e: JQueryEventObject, data: DataFieldRequiredEventData) {
                        return self.onfocus(data);
                    }).datacontainer('dataset', self._dataset);
                }
            })
            return self;
        }
    }
}

interface JQuery {
    dataform(methodName: 'refresh'): JQuery,
    dataform(optionLiteral: string, options: DataProviding.Dataset): DataProviding.Dataset;
    dataform(methodName: 'dataset', options: DataProviding.Dataset): DataProviding.Dataset,
    dataform(optionLiteral: string, options: DataProviding.DataFormOptions): JQuery;
    dataform(methodName: 'setOptions', options: DataProviding.DataFormOptions): JQuery,
    dataform(options: DataProviding.DataFormOptions): JQuery,
}

$.widget("data-providing.dataform", new DataProviding.DataForm());
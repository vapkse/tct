'use strict';
var DataProviding;
(function (DataProviding) {
    var DataContainer = (function () {
        function DataContainer() {
            this._create = function () {
            };
            this.onfocus = function (data) {
                var self = this;
                var e = jQuery.Event("focus");
                data.container = self.element;
                return self._trigger('focus', e, data);
            };
            this.onchange = function (data) {
                var self = this;
                var e = jQuery.Event("change");
                data.container = self.element;
                data.path.unshift(self.element.attr('data-container'));
                return self._trigger('change', e, data);
            };
            this.onlookuprequired = function (data) {
                var self = this;
                var e = jQuery.Event("lookuprequired");
                data.container = self.element;
                data.path.unshift(self.element.attr('data-container'));
                return self._trigger('lookuprequired', e, data);
            };
            this.dataset = function (dataset) {
                var self = this;
                if (dataset !== undefined) {
                    self._dataset = dataset;
                    self.refresh();
                }
                return self._dataset;
            };
            this.refresh = function () {
                var self = this;
                var element = self.element;
                var containerName = element.attr('data-container');
                var bindFields = function (ds, container) {
                    var bindable = container.find('[data-field]:not([data-container="' + containerName + '"] [data-container] [data-field])');
                    bindable.each(function (index, element) {
                        var $element = $(element);
                        var fieldname = $element.attr('data-field');
                        try {
                            var bindingControl = $element.attr('data-bind');
                            if (bindingControl) {
                                var isCreated = $element.is(':data-providing-' + bindingControl);
                                if (!isCreated) {
                                    $element[bindingControl]({}).on(bindingControl + 'change', function (e, data) {
                                        return self.onchange(data);
                                    }).on(bindingControl + 'lookuprequired', function (e, data) {
                                        return self.onlookuprequired(data);
                                    }).on(bindingControl + 'focus', function (e) {
                                        var element = $(e.target);
                                        var data = {
                                            fieldName: $element.attr('data-field'),
                                            element: element
                                        };
                                        return self.onfocus(data);
                                    });
                                }
                                var field = ds[fieldname];
                                if (field) {
                                    $element[bindingControl]('field', field);
                                }
                                else {
                                    $element[bindingControl]('field', { value: null });
                                }
                            }
                        }
                        catch (e) {
                            var msg = 'Fail to bind field ' + fieldname + ': ' + e.toString();
                            console.log(msg);
                        }
                    });
                    var containers = self.element.find('[data-container]:not([data-container="' + containerName + '"] [data-container] [data-container])');
                    containers.each(function (index, element) {
                        var $element = $(element);
                        var isCreated = $element.is(':data-providing-datacontainer');
                        if (isCreated) {
                            $element.datacontainer('dataset', ds);
                        }
                        else {
                            $element.datacontainer({}).on('datacontainerchange', function (e, data) {
                                return self.onchange(data);
                            }).on('datacontainerlookuprequired', function (e, data) {
                                return self.onlookuprequired(data);
                            }).on('datacontainerfocus', function (e, data) {
                                return self.onfocus(data);
                            }).datacontainer('dataset', ds);
                        }
                    });
                };
                if (containerName) {
                    var field = self._dataset.getField(containerName);
                    if (field.type === 'array') {
                        if (field.value === undefined) {
                            return null;
                        }
                        if (field.value instanceof Array) {
                            var indexedBindable = element.is('[data-index]') ? element : element.find('[data-index]:not([data-container="' + containerName + '"] [data-container] [data-index])');
                            indexedBindable.hide();
                            if (field.value.length === 0) {
                                return null;
                            }
                            var template = element.find('[data-template="' + containerName + '"]').html();
                            for (var d = 0; d < field.value.length; d++) {
                                var subset = field.value[d];
                                var $childElement = indexedBindable.filter('[data-index="' + d + '"]');
                                if ($childElement.length === 0 && template) {
                                    $childElement = $(template).appendTo(element).attr('data-index', d);
                                }
                                if ($childElement.length) {
                                    bindFields(subset, $childElement.show());
                                }
                            }
                        }
                        else {
                            throw "Wrong data type for array model.";
                        }
                    }
                    else {
                        bindFields(field.value, element);
                    }
                }
                else {
                    bindFields(self._dataset, element);
                }
                return self;
            };
        }
        return DataContainer;
    })();
    DataProviding.DataContainer = DataContainer;
})(DataProviding || (DataProviding = {}));
$.widget("data-providing.datacontainer", new DataProviding.DataContainer());

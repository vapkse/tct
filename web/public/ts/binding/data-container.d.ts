declare module DataProviding {
    interface DataFieldEventData {
        field?: Field;
        path?: Array<string>;
        fieldName: string;
        container?: JQuery;
        dataForm?: JQuery;
        element: JQuery;
    }
    interface DataFieldRequiredEventData extends DataFieldEventData {
        callback(options: any): void;
    }
    interface DataContainerOptions {
    }
    class DataContainer {
        protected element: JQuery;
        protected _dataset: Dataset;
        options: DataContainerOptions;
        protected _trigger: (eventName: string, event: JQueryEventObject, data: DataFieldEventData) => boolean;
        protected _create: () => void;
        protected onfocus: (data: DataFieldEventData) => boolean;
        protected onchange: (data: DataFieldEventData) => boolean;
        protected onlookuprequired: (data: DataFieldRequiredEventData) => boolean;
        dataset: (dataset?: Dataset) => Dataset;
        refresh: () => DataContainer;
    }
}
interface JQuery {
    datacontainer(methodName: 'refresh'): JQuery;
    datacontainer(optionLiteral: string, options: DataProviding.Dataset): DataProviding.Dataset;
    datacontainer(methodName: 'dataset', options: DataProviding.Dataset): DataProviding.Dataset;
    datacontainer(optionLiteral: string, options: DataProviding.DataContainerOptions): JQuery;
    datacontainer(methodName: 'setOptions', options: DataProviding.DataContainerOptions): JQuery;
    datacontainer(options: DataProviding.DataContainerOptions): JQuery;
    on(eventType: string, handler: (eventObject: JQueryEventObject, data: DataProviding.DataFieldEventData) => any): JQuery;
}

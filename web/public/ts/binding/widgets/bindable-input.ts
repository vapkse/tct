'use strict';

module DataProviding {
    export class BindableInput extends modern.TextInput {
        private _field: Field;

        public field = function(field?: Field): Field {
            var self: BindableInput = this;
            var element = self.element;

            if (field !== undefined) {
                self._field = field;

                if (field.value !== self.value()) {
                    self.value(field.value);
                }
            }

            return self._field;
        };
       
         protected onchange = function() {
            var self: BindableInput = this;
            var e = jQuery.Event('change');
            var fieldName = self.element.attr('data-field');
            var field = self.field();
            field.value = self.value();
            var data: DataFieldEventData = {
                field: field,
                path: [fieldName],
                fieldName: fieldName,
                element: self.element,
            }
            return this._trigger('change', e, data);
        }
    }
}

$.widget("data-providing.datainput", $.modern.textinput, new DataProviding.BindableInput());

interface JQuery {
    datainput(): JQuery,
    datainput(optionLiteral: 'field', field?: DataProviding.Field): DataProviding.Field;
    datainput(optionLiteral: string, field?: DataProviding.Field): DataProviding.Field;
}
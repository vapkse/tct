'use strict';

module DataProviding {
    export class BindableNumeric extends modern.NumericInput {
        private _field: Field;

        public field = function(field?: Field): Field {
            var self: BindableNumeric = this;

            if (field !== undefined) {
                self._field = field;
                var value = field.value;
                if (field.value !== self.value()) {                    
                    self.value(value !== undefined ? value : null);
                }
            } 

            return self._field;
        };
        
        protected onchange = function(): boolean {
            var self: BindableNumeric = this;
            var e = jQuery.Event('change');
            var fieldName = self.element.attr('data-field');
            var field = self.field();
            field.value = self.value();
            var data: DataFieldEventData = {
                field: field,
                path: [fieldName],
                fieldName: fieldName,
                element: self.element,
            }
            self.ensureEmptyClass();
            return this._trigger('change', e, data);
        }
    }
}

$.widget("data-providing.datanumeric", $.modern.numericinput, new DataProviding.BindableNumeric());

interface JQuery {
    datanumeric(): JQuery,
    datanumeric(options: modern.NumericInputOptions): JQuery,
    datanumeric(optionLiteral: 'field', field?: DataProviding.Field): DataProviding.Field;
    datanumeric(optionLiteral: string, field?: DataProviding.Field): DataProviding.Field;
}
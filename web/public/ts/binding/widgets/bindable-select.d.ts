declare module DataProviding {
    class BindableSelect extends modern.Chosen2 {
        private _field;
        field: (field?: Field) => Field;
        protected valueField(): string;
        protected textField(): string;
        protected onlookuprequired: (e: modern.ChosenRequiredEvent) => boolean;
        protected onchange: () => boolean;
    }
}
interface JQuery {
    dataselect(): JQuery;
    dataselect(options: modern.ChosenOptions): JQuery;
    dataselect(optionLiteral: string, field?: DataProviding.Field): DataProviding.Field;
    dataselect(optionLiteral: 'field', field?: DataProviding.Field): DataProviding.Field;
    dataselect(optionLiteral: 'selectedItem', item: any): any;
}

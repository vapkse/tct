'use strict';
var DataProviding;
(function (DataProviding) {
    var BindableRadio = (function () {
        function BindableRadio() {
            this._create = function () {
                var self = this;
                self._radioButtons = this.element.find('[data-value]').removeClass(self._selectedClass).on('click', function (e) {
                    self._radioButtons.removeClass(self._selectedClass);
                    var value = $(e.target).closest('[data-value]').addClass(self._selectedClass).attr('data-value');
                    if (self._field.type === 'number') {
                        self._field.value = parseInt(value);
                    }
                    else if (self._field.type === 'string') {
                        self._field.value = value;
                    }
                    else {
                        throw 'Datatype not provided.';
                    }
                    self.onfocus();
                    self.onchange();
                });
            };
            this.field = function (field) {
                var self = this;
                self._selectedClass = self.element.attr('selected-class') || 'selected';
                if (field !== undefined) {
                    self._field = field;
                    if (field && field.value !== undefined && field.value !== null && field.value !== '') {
                        self._radioButtons.filter('[data-value=' + field.value + ']').addClass(self._selectedClass);
                    }
                }
                return self._field;
            };
            this.onfocus = function () {
                var self = this;
                var e = jQuery.Event('focus');
                var fieldName = self.element.attr('data-field');
                var field = self.field();
                var data = {
                    field: field,
                    path: [fieldName],
                    fieldName: fieldName,
                    element: self.element,
                };
                return this._trigger('focus', e, data);
            };
            this.onchange = function () {
                var self = this;
                var e = jQuery.Event('change');
                var fieldName = self.element.attr('data-field');
                var field = self.field();
                var data = {
                    field: field,
                    path: [fieldName],
                    fieldName: fieldName,
                    element: self.element,
                };
                return this._trigger('change', e, data);
            };
        }
        return BindableRadio;
    })();
    DataProviding.BindableRadio = BindableRadio;
})(DataProviding || (DataProviding = {}));
$.widget("data-providing.dataradio", new DataProviding.BindableRadio());

'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var DataProviding;
(function (DataProviding) {
    var BindableNumeric = (function (_super) {
        __extends(BindableNumeric, _super);
        function BindableNumeric() {
            _super.apply(this, arguments);
            this.field = function (field) {
                var self = this;
                if (field !== undefined) {
                    self._field = field;
                    var value = field.value;
                    if (field.value !== self.value()) {
                        self.value(value !== undefined ? value : null);
                    }
                }
                return self._field;
            };
            this.onchange = function () {
                var self = this;
                var e = jQuery.Event('change');
                var fieldName = self.element.attr('data-field');
                var field = self.field();
                field.value = self.value();
                var data = {
                    field: field,
                    path: [fieldName],
                    fieldName: fieldName,
                    element: self.element,
                };
                self.ensureEmptyClass();
                return this._trigger('change', e, data);
            };
        }
        return BindableNumeric;
    })(modern.NumericInput);
    DataProviding.BindableNumeric = BindableNumeric;
})(DataProviding || (DataProviding = {}));
$.widget("data-providing.datanumeric", $.modern.numericinput, new DataProviding.BindableNumeric());

'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var DataProviding;
(function (DataProviding) {
    var BindableInput = (function (_super) {
        __extends(BindableInput, _super);
        function BindableInput() {
            _super.apply(this, arguments);
            this.field = function (field) {
                var self = this;
                var element = self.element;
                if (field !== undefined) {
                    self._field = field;
                    if (field.value !== self.value()) {
                        self.value(field.value);
                    }
                }
                return self._field;
            };
            this.onchange = function () {
                var self = this;
                var e = jQuery.Event('change');
                var fieldName = self.element.attr('data-field');
                var field = self.field();
                field.value = self.value();
                var data = {
                    field: field,
                    path: [fieldName],
                    fieldName: fieldName,
                    element: self.element,
                };
                return this._trigger('change', e, data);
            };
        }
        return BindableInput;
    })(modern.TextInput);
    DataProviding.BindableInput = BindableInput;
})(DataProviding || (DataProviding = {}));
$.widget("data-providing.datainput", $.modern.textinput, new DataProviding.BindableInput());

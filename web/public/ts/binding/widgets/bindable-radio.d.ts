declare module DataProviding {
    class BindableRadio {
        private _field;
        private _radioButtons;
        private _selectedClass;
        private element;
        protected _create: () => void;
        field: (field?: Field) => Field;
        onfocus: () => any;
        onchange: () => any;
    }
}
interface JQuery {
    dataradio(): JQuery;
    dataradio(optionLiteral: 'field', field?: DataProviding.Field): DataProviding.Field;
    dataradio(optionLiteral: string, field?: DataProviding.Field): DataProviding.Field;
}

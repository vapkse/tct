'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var DataProviding;
(function (DataProviding) {
    var BindableSelect = (function (_super) {
        __extends(BindableSelect, _super);
        function BindableSelect() {
            _super.apply(this, arguments);
            this.field = function (field) {
                var self = this;
                if (field !== undefined) {
                    self._field = field;
                    self.selectedItem(field.value);
                    return field;
                }
                return self._field;
            };
            this.onlookuprequired = function (e) {
                var self = this;
                var evt = jQuery.Event('lookuprequired');
                var fieldName = self.element.attr('data-field');
                var field = self.field();
                var data = {
                    field: field,
                    path: [fieldName],
                    fieldName: fieldName,
                    callback: e.callback,
                    element: self.element
                };
                return this._trigger('lookuprequired', evt, data);
            };
            this.onchange = function () {
                var self = this;
                var e = jQuery.Event('change');
                var fieldName = self.element.attr('data-field');
                var field = self.field();
                field.value = self.selectedItem();
                var data = {
                    field: self.field(),
                    path: [fieldName],
                    fieldName: fieldName,
                    element: self.element,
                };
                return this._trigger('change', e, data);
            };
        }
        BindableSelect.prototype.valueField = function () {
            var self = this;
            return self._field && self._field.valueField || _super.prototype.valueField.call(this);
        };
        BindableSelect.prototype.textField = function () {
            var self = this;
            return self._field && self._field.textField || _super.prototype.textField.call(this);
        };
        return BindableSelect;
    })(modern.Chosen2);
    DataProviding.BindableSelect = BindableSelect;
})(DataProviding || (DataProviding = {}));
$.widget("data-providing.dataselect", $.modern.chosen2, new DataProviding.BindableSelect());

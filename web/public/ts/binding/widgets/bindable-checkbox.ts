'use strict';

module DataProviding {
    export class BindableCheckbox extends modern.Checkbox {
        private _field: Field;

        public field = function(field?: Field): Field {
            var self: BindableCheckbox = this;

            if (field !== undefined) {
                self._field = field;

                if (field.value !== self.value()) {
                    self.value(field.value);
                }
            }
            
            return self._field;
        };
        
        public onchange = function() {
            var self: BindableCheckbox = this;
            var e = jQuery.Event('change');
            var fieldName = self.element.attr('data-field');
            var field = self.field();
            field.value = self.value();
            var data: DataFieldEventData = {
                field: field,
                path: [fieldName],
                fieldName: fieldName,
                element: self.element,
            }
            return this._trigger('change', e, data);
        }
    }
}

$.widget("data-providing.datacheckbox", $.modern.checkbox, new DataProviding.BindableCheckbox());

interface JQuery {
    datacheckbox(): JQuery,
    datacheckbox(options: modern.CheckboxOptions): JQuery,
    datacheckbox(optionLiteral: 'field', field?: DataProviding.Field): DataProviding.Field;
    datacheckbox(optionLiteral: string, field?: DataProviding.Field): DataProviding.Field;
}
'use strict';

module DataProviding {
    export class BindableSelect extends modern.Chosen2 {
        private _field: Field;

        public field = function(field?: Field): Field {
            var self: BindableSelect = this;

            // Find selected value in field item        
            if (field !== undefined) {
                self._field = field;
                self.selectedItem(field.value);
                return field;
            }

            return self._field;
        };

        protected valueField() {
            var self: BindableSelect = this;
            return self._field && self._field.valueField || super.valueField();
        }

        protected textField() {
            var self: BindableSelect = this;
            return self._field && self._field.textField || super.textField();
        }
        
        protected onlookuprequired = function(e: modern.ChosenRequiredEvent): boolean {
            var self: BindableSelect = this;
            var evt = jQuery.Event('lookuprequired');
            var fieldName = self.element.attr('data-field');
            var field = self.field();
            var data: DataFieldRequiredEventData = {
                field: field,
                path: [fieldName],
                fieldName: fieldName,
                callback: e.callback,
                element: self.element
            }
            return this._trigger('lookuprequired', evt, data);
        }
        
        protected onchange = function(): boolean {
            var self: BindableSelect = this;
            var e = jQuery.Event('change');
            var fieldName = self.element.attr('data-field');
            var field = self.field();
            field.value = self.selectedItem();
            var data: DataFieldEventData = {
                field: self.field(),
                path: [fieldName],
                fieldName: fieldName,
                element: self.element,
            }
            return this._trigger('change', e, data);
        }
    }
}

$.widget("data-providing.dataselect", $.modern.chosen2, new DataProviding.BindableSelect());

interface JQuery {
    dataselect(): JQuery,
    dataselect(options: modern.ChosenOptions): JQuery,
    dataselect(optionLiteral: string, field?: DataProviding.Field): DataProviding.Field;
    dataselect(optionLiteral: 'field', field?: DataProviding.Field): DataProviding.Field;
    dataselect(optionLiteral: 'selectedItem', item: any): any;
}
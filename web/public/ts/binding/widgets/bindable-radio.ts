'use strict';

module DataProviding {
    export class BindableRadio {
        private _field: Field;
        private _radioButtons: JQuery;
        private _selectedClass: string;
        private element: JQuery;

        protected _create = function() {
            var self: BindableRadio = this;

            self._radioButtons = this.element.find('[data-value]').removeClass(self._selectedClass).on('click', function(e: JQueryEventObject) {
                self._radioButtons.removeClass(self._selectedClass);
                var value = $(e.target).closest('[data-value]').addClass(self._selectedClass).attr('data-value');
                if (self._field.type === 'number') {
                    self._field.value = parseInt(value);
                } else if (self._field.type === 'string') {
                    self._field.value = value;
                } else {
                    throw 'Datatype not provided.'
                }
                self.onfocus();
                self.onchange();
            });
        };

        public field = function(field?: Field): Field {
            var self: BindableRadio = this;

            self._selectedClass = self.element.attr('selected-class') || 'selected';

            if (field !== undefined) {
                self._field = field;
                if (field && field.value !== undefined && field.value !== null && field.value !== '') {
                    self._radioButtons.filter('[data-value=' + field.value + ']').addClass(self._selectedClass);
                }
            }

            return self._field;
        };

        public onfocus = function() {
            var self: BindableRadio = this;
            var e = jQuery.Event('focus');
            var fieldName = self.element.attr('data-field');
            var field = self.field();
            var data: DataFieldEventData = {
                field: field,
                path: [fieldName],
                fieldName: fieldName,
                element: self.element,
            }

            return this._trigger('focus', e, data);
        }

        public onchange = function() {
            var self: BindableRadio = this;
            var e = jQuery.Event('change');
            var fieldName = self.element.attr('data-field');
            var field = self.field();
            var data: DataFieldEventData = {
                field: field,
                path: [fieldName],
                fieldName: fieldName,
                element: self.element,
            }

            return this._trigger('change', e, data);
        }
    }
}

$.widget("data-providing.dataradio", new DataProviding.BindableRadio());

interface JQuery {
    dataradio(): JQuery,
    dataradio(optionLiteral: 'field', field?: DataProviding.Field): DataProviding.Field;
    dataradio(optionLiteral: string, field?: DataProviding.Field): DataProviding.Field;
}
declare module DataProviding {
    class BindableInput extends modern.TextInput {
        private _field;
        field: (field?: Field) => Field;
        protected onchange: () => any;
    }
}
interface JQuery {
    datainput(): JQuery;
    datainput(optionLiteral: 'field', field?: DataProviding.Field): DataProviding.Field;
    datainput(optionLiteral: string, field?: DataProviding.Field): DataProviding.Field;
}

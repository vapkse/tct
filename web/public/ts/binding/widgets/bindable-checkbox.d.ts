declare module DataProviding {
    class BindableCheckbox extends modern.Checkbox {
        private _field;
        field: (field?: Field) => Field;
        onchange: () => any;
    }
}
interface JQuery {
    datacheckbox(): JQuery;
    datacheckbox(options: modern.CheckboxOptions): JQuery;
    datacheckbox(optionLiteral: 'field', field?: DataProviding.Field): DataProviding.Field;
    datacheckbox(optionLiteral: string, field?: DataProviding.Field): DataProviding.Field;
}

declare module DataProviding {
    class BindableNumeric extends modern.NumericInput {
        private _field;
        field: (field?: Field) => Field;
        protected onchange: () => boolean;
    }
}
interface JQuery {
    datanumeric(): JQuery;
    datanumeric(options: modern.NumericInputOptions): JQuery;
    datanumeric(optionLiteral: 'field', field?: DataProviding.Field): DataProviding.Field;
    datanumeric(optionLiteral: string, field?: DataProviding.Field): DataProviding.Field;
}

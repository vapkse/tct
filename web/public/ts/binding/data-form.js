'use strict';
var DataProviding;
(function (DataProviding) {
    var DataForm = (function () {
        function DataForm() {
            this.options = {};
            this._create = function () {
                var self = this;
                self.refresh();
                setTimeout(function () {
                    self.onload();
                }, 0);
            };
            this.setOptions = function (value) {
                var self = this;
                self.options = value;
                self.refresh();
                return self;
            };
            this.onchange = function (data) {
                var self = this;
                var e = jQuery.Event('change');
                data.dataForm = self.element;
                return self._trigger('change', e, data);
            };
            this.onfocus = function (data) {
                var self = this;
                var e = jQuery.Event('focus');
                data.dataForm = self.element;
                return self._trigger('focus', e, data);
            };
            this.onlookuprequired = function (data) {
                var self = this;
                var e = jQuery.Event('lookuprequired');
                data.dataForm = self.element;
                return self._trigger('lookuprequired', e, data);
            };
            this.onload = function () {
                var self = this;
                var evt = jQuery.Event("load");
                evt.value = self._dataset;
                self._trigger('load', evt);
            };
            this.dataset = function (dataset) {
                var self = this;
                if (dataset !== undefined) {
                    self._dataset = dataset;
                    self.refresh();
                }
                return self._dataset;
            };
            this.refresh = function () {
                var self = this;
                var containers = self.element.find('[data-container]:not([data-form] [data-container] [data-container])');
                containers.each(function (index, element) {
                    var $element = $(element);
                    var isCreated = $element.is(':data-providing-datacontainer');
                    if (isCreated) {
                        $element.datacontainer('dataset', self._dataset);
                    }
                    else {
                        $element.datacontainer({}).on('datacontainerchange', function (e, data) {
                            return self.onchange(data);
                        }).on('datacontainerlookuprequired', function (e, data) {
                            return self.onlookuprequired(data);
                        }).on('datacontainerfocus', function (e, data) {
                            return self.onfocus(data);
                        }).datacontainer('dataset', self._dataset);
                    }
                });
                return self;
            };
        }
        return DataForm;
    })();
    DataProviding.DataForm = DataForm;
})(DataProviding || (DataProviding = {}));
$.widget("data-providing.dataform", new DataProviding.DataForm());

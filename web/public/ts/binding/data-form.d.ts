declare module DataProviding {
    interface DataFormEvent extends JQueryEventObject {
        value: Dataset;
    }
    interface DataFormOptions {
    }
    class DataForm {
        options: DataFormOptions;
        protected _dataset: Dataset;
        element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject, data?: DataFieldEventData) => boolean;
        protected _create: () => void;
        setOptions: (value: DataFormOptions) => DataForm;
        protected onchange: (data: DataFieldEventData) => boolean;
        protected onfocus: (data: DataFieldEventData) => boolean;
        protected onlookuprequired: (data: DataFieldRequiredEventData) => boolean;
        protected onload: () => void;
        dataset: (dataset?: Dataset) => Dataset;
        refresh: () => DataForm;
    }
}
interface JQuery {
    dataform(methodName: 'refresh'): JQuery;
    dataform(optionLiteral: string, options: DataProviding.Dataset): DataProviding.Dataset;
    dataform(methodName: 'dataset', options: DataProviding.Dataset): DataProviding.Dataset;
    dataform(optionLiteral: string, options: DataProviding.DataFormOptions): JQuery;
    dataform(methodName: 'setOptions', options: DataProviding.DataFormOptions): JQuery;
    dataform(options: DataProviding.DataFormOptions): JQuery;
}

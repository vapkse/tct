'use strict';

module DataProviding {
    export interface DataFieldEventData {
        field?: Field,
        path?: Array<string>,
        fieldName: string,
        container?: JQuery;
        dataForm?: JQuery;
        element: JQuery;
    }

    export interface DataFieldRequiredEventData extends DataFieldEventData {
        callback(options: any): void;
    }

    export interface DataContainerOptions {
    }

    export class DataContainer {
        protected element: JQuery;
        protected _dataset: Dataset;
        public options: DataContainerOptions;
        protected _trigger: (eventName: string, event: JQueryEventObject, data: DataFieldEventData) => boolean;
        protected _create = function() {

        };

        protected onfocus = function(data: DataFieldEventData): boolean {
            var self: DataContainer = this;
            var e = jQuery.Event("focus");
            data.container = self.element;
            return self._trigger('focus', e, data);
        }

        protected onchange = function(data: DataFieldEventData): boolean {
            var self: DataContainer = this;
            var e = jQuery.Event("change");
            data.container = self.element;
            data.path.unshift(self.element.attr('data-container'))
            return self._trigger('change', e, data);
        }

        protected onlookuprequired = function(data: DataFieldRequiredEventData): boolean {
            var self: DataContainer = this;
            var e = jQuery.Event("lookuprequired");
            data.container = self.element;
            data.path.unshift(self.element.attr('data-container'))
            return self._trigger('lookuprequired', e, data);
        }

        public dataset = function(dataset?: Dataset) {
            var self: DataContainer = this;
            if (dataset !== undefined) {
                self._dataset = dataset;
                self.refresh();
            }
            return self._dataset;
        }

        public refresh = function() {
            var self: DataContainer = this;
            var element = self.element
            var containerName = element.attr('data-container');

            var bindFields = function(ds: any, container: JQuery) {
                var bindable = container.find('[data-field]:not([data-container="' + containerName + '"] [data-container] [data-field])');
                bindable.each(function(index: number, element: Element) {
                    var $element = $(element)
                    var fieldname = $element.attr('data-field');
                    try {
                        var bindingControl = $element.attr('data-bind');
                        if (bindingControl) {
                            var isCreated = $element.is(':data-providing-' + bindingControl);
                            if (!isCreated) {
                                $element[bindingControl]({}).on(bindingControl + 'change', function(e: JQueryEventObject, data: DataFieldRequiredEventData) {
                                    return self.onchange(data);
                                }).on(bindingControl + 'lookuprequired', function(e: JQueryEventObject, data: DataFieldRequiredEventData) {
                                    return self.onlookuprequired(data);
                                }).on(bindingControl + 'focus', function(e: JQueryEventObject) {
                                    var element = $(e.target);
                                    var data = {
                                        fieldName: $element.attr('data-field'),
                                        element: element
                                    }
                                    return self.onfocus(data);
                                });
                            }
                            var field = ds[fieldname];
                            if (field) {
                                $element[bindingControl]('field', field);
                            } else {
                                $element[bindingControl]('field', { value: null });
                            }
                        }
                    } catch (e) {
                        var msg = 'Fail to bind field ' + fieldname + ': ' + e.toString();
                        console.log(msg);
                    }
                })

                // Recursives containers
                var containers = self.element.find('[data-container]:not([data-container="' + containerName + '"] [data-container] [data-container])');
                containers.each(function(index: number, element: Element) {
                    var $element = $(element)
                    var isCreated = $element.is(':data-providing-datacontainer');
                    if (isCreated) {
                        $element.datacontainer('dataset', ds);
                    } else {
                        $element.datacontainer({}).on('datacontainerchange', function(e: JQueryEventObject, data: DataFieldEventData) {
                            return self.onchange(data);
                        }).on('datacontainerlookuprequired', function(e: JQueryEventObject, data: DataFieldRequiredEventData) {
                            return self.onlookuprequired(data);
                        }).on('datacontainerfocus', function(e: JQueryEventObject, data: DataFieldRequiredEventData) {
                            return self.onfocus(data);
                        }).datacontainer('dataset', ds);
                    }
                })
            }
                
            // Get the right dataset to bind
            if (containerName) {
                var field = self._dataset.getField(containerName);
                if (field.type === 'array') {
                    if (field.value === undefined) {
                        // Only hide all indexed containers, nothing to bind  
                        return null;
                    }
                    if (field.value instanceof Array) {
                        // Get indexed containers
                        var indexedBindable = element.is('[data-index]') ? element : element.find('[data-index]:not([data-container="' + containerName + '"] [data-container] [data-index])');
                        indexedBindable.hide();
                        if (field.value.length === 0) {
                            // Only hide all indexed containers, nothing to bind  
                            return null;
                        }
                
                        // Check if a template for the repeater exists in the container
                        var template = element.find('[data-template="' + containerName + '"]').html(); 
                
                        // Bind the repeater            
                        for (var d = 0; d < field.value.length; d++) {
                            var subset = field.value[d];
                            var $childElement = indexedBindable.filter('[data-index="' + d + '"]');
                            if ($childElement.length === 0 && template) {
                                // If not enough elements on the container and if a template exists, clone it
                                $childElement = $(template).appendTo(element).attr('data-index', d);
                            }
                            if ($childElement.length) {
                                bindFields(subset, $childElement.show());
                            }
                        }
                    } else {
                        throw "Wrong data type for array model."
                    }
                } else {
                    // Object
                    bindFields(field.value, element);
                }
            } else {
                // Dataset
                bindFields(self._dataset, element);
            }

            return self;
        }
    }
}

interface JQuery {
    datacontainer(methodName: 'refresh'): JQuery,
    datacontainer(optionLiteral: string, options: DataProviding.Dataset): DataProviding.Dataset;
    datacontainer(methodName: 'dataset', options: DataProviding.Dataset): DataProviding.Dataset,
    datacontainer(optionLiteral: string, options: DataProviding.DataContainerOptions): JQuery;
    datacontainer(methodName: 'setOptions', options: DataProviding.DataContainerOptions): JQuery,
    datacontainer(options: DataProviding.DataContainerOptions): JQuery,
    on(eventType: string, handler: (eventObject: JQueryEventObject, data: DataProviding.DataFieldEventData) => any): JQuery;
}

$.widget("data-providing.datacontainer", new DataProviding.DataContainer());
declare class Translator {
    replaceParameter: (text: string, params: {
        [key: number]: string;
    }) => string;
    replaceParameters: (messages: translator.Message[]) => void;
    translateMessages(messages: Array<translator.Message>, cb: (translated: Array<translator.Message>) => void): void;
}
interface TranslatorStatic {
    translateMessages(messages: Array<translator.Message>, cb: (translated: Array<translator.Message>) => void): void;
    replaceParameters(messages: Array<translator.Message>): void;
    replaceParameter(text: string, params: {
        [key: number]: string;
    }): string;
}
interface JQueryStatic {
    translator: TranslatorStatic;
}

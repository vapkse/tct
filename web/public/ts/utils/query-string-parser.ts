'option: strict';

$(function() {
    location.query = function() {
        var queryString = location.search.substring(1).split("&").reduce(function(result: IUrlParams, value: string) {
            var parts = value.split('=');
            if (parts[0]) {
                result[decodeURIComponent(parts[0])] = decodeURIComponent(parts[1]);
            }
            return result;
        }, <IUrlParams>{});

        return queryString;
    }
});

interface IUrlParams {
    [index: string]: string;
    lang?: string;
}

interface Location {
	query(): IUrlParams,
}

declare class PageState {
    private _currentState;
    replaceOrAddState(state: any, urlParams?: IUrlParams): void;
    replaceUrlParams(values: {
        [name: string]: string;
    }): void;
    replaceUrlParam(name: string, value: string): void;
    private replace(search, name, value);
    replaceUrl(search: string): void;
}
interface PageStateStatic {
    replaceOrAddState(state: any, search?: IUrlParams): void;
    replaceUrlParam(name: string, value: string): void;
    replaceUrlParams(values: {
        [name: string]: string;
    }): void;
    replaceUrl(search: string): void;
}
interface JQueryStatic {
    pageState: PageStateStatic;
}

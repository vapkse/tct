'option: strict';
var Translator = (function () {
    function Translator() {
        this.replaceParameter = function (text, params) {
            var re = /([ ]?)\\([0-9]*)([ ]?)/gm;
            var matches = re.exec(text);
            var output = [];
            var start = 0;
            while (matches) {
                var end = matches.index;
                if (end > start) {
                    output.push(text.substring(start, end));
                }
                start = end + matches[1].length + 1;
                if (matches[2]) {
                    var iparam = parseInt(matches[2]);
                    if (!isNaN(iparam)) {
                        output.push(' ');
                        output.push(params[iparam]);
                        output.push(' ');
                        start += matches[2].length;
                    }
                }
                start += matches[3].length;
                var matches = re.exec(text);
            }
            if (output.length) {
                output.push(text.substring(start));
                return output.join('');
            }
            else {
                return text;
            }
        };
        this.replaceParameters = function (messages) {
            var self = this;
            for (var i = 0; i < messages.length; i++) {
                messages[i].message = self.replaceParameter(messages[i].message, messages[i].params);
            }
        };
    }
    Translator.prototype.translateMessages = function (messages, cb) {
        var self = this;
        var texts = {};
        messages.map(function (message) { return texts[message.name] = message.message; });
        var translationsParam = {
            to: 'current',
            translations: texts,
            nocache: true
        };
        var options = {
            url: '/translator?r=translate',
            type: 'POST',
            timeout: 20000,
            contentType: 'application/json',
            data: JSON.stringify(translationsParam),
            success: function (result) {
                messages.map(function (message) { return message.message = result.translations[message.name]; });
                self.replaceParameters(messages);
                cb(messages);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
                self.replaceParameters(messages);
                cb(messages);
            }
        };
        var request = $.ajax(options);
    };
    return Translator;
})();
(function ($) {
    $.translator = new Translator();
})(jQuery);

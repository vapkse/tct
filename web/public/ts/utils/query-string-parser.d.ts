interface IUrlParams {
    [index: string]: string;
    lang?: string;
}
interface Location {
    query(): IUrlParams;
}

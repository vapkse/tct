'option: strict';
var PageState = (function () {
    function PageState() {
    }
    PageState.prototype.replaceOrAddState = function (state, urlParams) {
        var self = this;
        var sb;
        var search;
        if (!history.pushState) {
            return;
        }
        if (urlParams) {
            sb = [];
            var params = $.extend({}, location.query(), urlParams);
            for (var name in params) {
                if (params[name] === null) {
                    continue;
                }
                if (sb.length) {
                    sb.push('&');
                }
                else {
                    sb.push('?');
                }
                sb.push(name);
                sb.push('=');
                sb.push(params[name]);
            }
            if (sb.length) {
                search = sb.join('');
            }
        }
        if (search && search !== location.search) {
            history.pushState({ 'j': state }, document.title, location.pathname + search);
        }
        else {
            history.replaceState({ 'j': state }, document.title, location.pathname + location.search);
        }
        self._currentState = state;
    };
    PageState.prototype.replaceUrlParams = function (values) {
        var self = this;
        var search = location.search;
        for (var name in values) {
            search = self.replace(search, name, values[name]);
        }
        history.pushState({ 'j': self._currentState }, document.title, location.pathname + search);
    };
    PageState.prototype.replaceUrlParam = function (name, value) {
        var self = this;
        if (!history.pushState) {
            return;
        }
        var search = self.replace(location.search, name, value);
        history.pushState({ 'j': self._currentState }, document.title, location.pathname + search);
    };
    PageState.prototype.replace = function (search, name, value) {
        var s = search.indexOf(name);
        if (s >= 0) {
            var qs = [];
            var q = search.indexOf('=', s) + 1;
            var e = search.indexOf('&', q);
            var val = e > 0 ? search.substr(q, e - q) : search.substr(q);
            if (val !== value) {
                qs.push(search.substr(0, s));
                qs.push(name);
                qs.push('=');
                qs.push(value);
                if (e > 0) {
                    qs.push(search.substr(e));
                }
                search = qs.join('');
            }
        }
        else {
            search += (search ? '&' : '?') + name + '=' + value;
        }
        return search;
    };
    PageState.prototype.replaceUrl = function (search) {
        var self = this;
        if (!history.pushState) {
            return;
        }
        history.pushState({ 'j': self._currentState }, document.title, location.pathname + search);
    };
    return PageState;
})();
(function ($) {
    $.pageState = new PageState();
})(jQuery);

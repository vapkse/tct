'option: strict';

class PageState {
    private _currentState: any;

    public replaceOrAddState(state: any, urlParams?: IUrlParams) {
        var self = <PageState>this;
        var sb: Array<string>;
        var search: string;

        if (!history.pushState) {
            return;
        }

        if (urlParams) {            
            // Merge url params with current parameters
            sb = [];
            var params = $.extend({}, location.query(), urlParams)
            for (var name in params) {
                if (params[name] === null) {
                    // Reset param
                    continue;
                }

                if (sb.length) {
                    sb.push('&');
                } else {
                    sb.push('?');
                }
                sb.push(name);
                sb.push('=');
                sb.push(params[name]);
            }
            if (sb.length) {
                search = sb.join('');
            }
        }

        if (search && search !== location.search) {
            history.pushState({ 'j': state }, document.title, location.pathname + search);
        } else {
            history.replaceState({ 'j': state }, document.title, location.pathname + location.search);
        }
        self._currentState = state;
    }

    public replaceUrlParams(values: { [name: string]: string }) {
        var self = <PageState>this;
        var search = location.search;

        for (var name in values) {
            search = self.replace(search, name, values[name]);
        }

        history.pushState({ 'j': self._currentState }, document.title, location.pathname + search);
    }

    public replaceUrlParam(name: string, value: string) {
        var self = <PageState>this;

        if (!history.pushState) {
            return;
        }

        var search = self.replace(location.search, name, value);

        history.pushState({ 'j': self._currentState }, document.title, location.pathname + search);
    }

    private replace(search: string, name: string, value: string) {
        var s = search.indexOf(name);
        if (s >= 0) {
            var qs: Array<string> = [];
            var q = search.indexOf('=', s) + 1;
            var e = search.indexOf('&', q);
            var val = e > 0 ? search.substr(q, e - q) : search.substr(q);
            if (val !== value) {
                qs.push(search.substr(0, s));
                qs.push(name);
                qs.push('=');
                qs.push(value);
                if (e > 0) {
                    qs.push(search.substr(e));
                }
                search = qs.join('');
            }
        } else {
            search += (search ? '&' : '?') + name + '=' + value;
        }

        return search;
    }

    public replaceUrl(search: string) {
        var self = <PageState>this;

        if (!history.pushState) {
            return;
        }

        history.pushState({ 'j': self._currentState }, document.title, location.pathname + search);
    }
}

(function($: JQueryStatic) {
    $.pageState = new PageState();
})(jQuery);

interface PageStateStatic {
    replaceOrAddState(state: any, search?: IUrlParams): void,
    replaceUrlParam(name: string, value: string): void,
    replaceUrlParams(values: { [name: string]: string }): void,
    replaceUrl(search: string): void,
}

interface JQueryStatic {
    pageState: PageStateStatic;
}

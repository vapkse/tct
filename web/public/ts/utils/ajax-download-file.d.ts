declare module utils {
    interface AjaxDownloadFileOptions {
        url: string;
        type?: string;
        headers?: {
            [key: string]: string;
        };
        data?: any;
        dataType?: string;
        async?: boolean;
    }
    class AjaxDownloadFile {
        private _onSuccessFinish;
        private _onErrorFinish;
        static download(options: AjaxDownloadFileOptions): AjaxDownloadFile;
        onSuccessFinish(fn: (status: number, statusText: string, res: {
            [key: string]: any;
        }, headers: string) => void): AjaxDownloadFile;
        onErrorFinish(fn: (status: number, statusText: string, res: {
            [key: string]: any;
        }, headers: string) => void): AjaxDownloadFile;
    }
}
interface HTMLAnchorElement {
    download: string;
}
interface Window {
    webkitURL: URL;
}

interface BrowserVersion {
    browser: string;
    version: number;
}
interface Window {
    browserVersion(): BrowserVersion;
    mobilecheck(): boolean;
    opera: string;
}

'option: strict';

class Translator {
    public replaceParameter = function(text: string, params: { [key: number]: string }) {
        var re = /([ ]?)\\([0-9]*)([ ]?)/gm;
        var matches = re.exec(text);
        var output: Array<string> = [];
        var start = 0;
        while (matches) {
            var end = matches.index;
            if (end > start) {
                output.push(text.substring(start, end))
            }

            // Remove one space before and step \\
            start = end + matches[1].length + 1;
            if (matches[2]) {
                var iparam = parseInt(matches[2]);
                if (!isNaN(iparam)) {
                    // Replace param
                    output.push(' ');
                    output.push(params[iparam]);
                    output.push(' ');
                    // Step replacement value
                    start += matches[2].length;
                }
            }

            // Remove one space after
            start += matches[3].length;

            var matches = re.exec(text);
        }

        if (output.length) {
            output.push(text.substring(start))
            return output.join('');
        } else {
            return text;
        }
    }

    public replaceParameters = function(messages: Array<translator.Message>) {
        var self = this as Translator;
        for (var i = 0; i < messages.length; i++) {
            messages[i].message = self.replaceParameter(messages[i].message, messages[i].params)
        }
    }

    public translateMessages(messages: Array<translator.Message>, cb: (translated: Array<translator.Message>) => void) {
        var self = this as Translator;
        var texts: translator.Translation = {}

        messages.map(message => texts[message.name] = message.message);

        var translationsParam: translator.TranslationsParams = {
            to: 'current',
            translations: texts,
            nocache: true
        }

        var options = {
            url: '/translator?r=translate',
            type: 'POST',
            timeout: 20000,
            contentType: 'application/json',
            data: JSON.stringify(translationsParam),
            success: function(result: translator.RequestResult) {
                messages.map(message => message.message = result.translations[message.name]);
                self.replaceParameters(messages);
                cb(messages);
            },
            error: function(jqXHR: JQueryXHR, textStatus: string, errorThrown: string) {
                console.error(errorThrown);
                self.replaceParameters(messages);
                cb(messages);
            }
        }
        var request = $.ajax(options);
    }
}

(function($: JQueryStatic) {
    $.translator = new Translator();
})(jQuery);

interface TranslatorStatic {
    translateMessages(messages: Array<translator.Message>, cb: (translated: Array<translator.Message>) => void): void,
    replaceParameters(messages: Array<translator.Message>): void,
    replaceParameter(text: string, params: { [key: number]: string }): string
}

interface JQueryStatic {
    translator: TranslatorStatic;
}

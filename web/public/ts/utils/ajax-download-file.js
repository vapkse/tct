var utils;
(function (utils) {
    var AjaxDownloadFile = (function () {
        function AjaxDownloadFile() {
        }
        AjaxDownloadFile.download = function (options) {
            var self = new AjaxDownloadFile();
            var url = options.url || window.location.href;
            var type = options.type || 'GET';
            var dataType = options.dataType || 'text';
            var data = options.data || null;
            var async = options.async || true;
            var headers = options.headers || { 'Content-Type': 'application/json; charset=UTF-8' };
            var xhr = new XMLHttpRequest();
            xhr = new XMLHttpRequest();
            xhr.addEventListener('load', function () {
                var res = {};
                var success = xhr.status >= 200 && xhr.status < 300 || xhr.status === 304;
                if (success) {
                    res[dataType] = xhr.response;
                    self._onSuccessFinish(xhr.status, xhr.statusText, res, xhr.getAllResponseHeaders());
                }
                else {
                    res['text'] = xhr.statusText;
                    self._onErrorFinish(xhr.status, xhr.statusText, res, xhr.getAllResponseHeaders());
                }
            });
            xhr.open(type, url, async);
            xhr.responseType = dataType;
            for (var key in headers) {
                if (headers.hasOwnProperty(key)) {
                    xhr.setRequestHeader(key, headers[key]);
                }
            }
            xhr.send(data);
            return self;
        };
        AjaxDownloadFile.prototype.onSuccessFinish = function (fn) {
            var self = this;
            self._onSuccessFinish = fn;
            return self;
        };
        AjaxDownloadFile.prototype.onErrorFinish = function (fn) {
            var self = this;
            self._onErrorFinish = fn;
            return self;
        };
        return AjaxDownloadFile;
    })();
    utils.AjaxDownloadFile = AjaxDownloadFile;
})(utils || (utils = {}));

var utils;
(function (utils) {
    var Color = (function () {
        function Color(r, g, b, a) {
            var self = this;
            self.r = r;
            self.g = g;
            self.b = b;
            self.a = a;
        }
        Color.fromRgbaString = function (rgba) {
            var color = new Color();
            if (!rgba) {
                return color;
            }
            var colors = rgba.match(/\d+/g);
            if (colors.length < 3) {
                return color;
            }
            try {
                color.r = parseInt(colors[0]);
                color.g = parseInt(colors[1]);
                color.b = parseInt(colors[2]);
                if (colors.length > 3) {
                    color.a = parseInt(colors[3]);
                }
                return color;
            }
            catch (e) {
                return new Color();
            }
        };
        Color.prototype.isEmpty = function () {
            var self = this;
            return self.r === undefined || self.g === undefined || self.b === undefined;
        };
        Object.defineProperty(Color.prototype, "red", {
            get: function () {
                var self = this;
                return self.r;
            },
            set: function (value) {
                var self = this;
                if (isNaN(value) || value < 0 || value > 255) {
                    throw 'Invalid range.';
                }
                self.r = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Color.prototype, "green", {
            get: function () {
                var self = this;
                return self.g;
            },
            set: function (value) {
                var self = this;
                if (isNaN(value) || value < 0 || value > 255) {
                    throw 'Invalid range.';
                }
                self.g = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Color.prototype, "blue", {
            get: function () {
                var self = this;
                return self.b;
            },
            set: function (value) {
                var self = this;
                if (isNaN(value) || value < 0 || value > 255) {
                    throw 'Invalid range.';
                }
                self.b = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Color.prototype, "alpha", {
            get: function () {
                var self = this;
                return self.a;
            },
            set: function (value) {
                var self = this;
                if (isNaN(value) || value < 0 || value > 255) {
                    throw 'Invalid range.';
                }
                self.a = value;
            },
            enumerable: true,
            configurable: true
        });
        return Color;
    })();
    utils.Color = Color;
})(utils || (utils = {}));

'option: strict';

$(function() {
    require(['../../common/ts/encoder'], function(utils: any) {
        var encoder = utils.Encoder as utils.Encoder;
        String.prototype.HTML2Numerical = function(){
            return encoder.HTML2Numerical(this);            
        };
        String.prototype.NumericalToHTML = function(){
            return encoder.NumericalToHTML(this);            
        };
        String.prototype.XSSEncode = function(en: boolean){
            return encoder.XSSEncode(this,en);            
        };
        String.prototype.crc = function(){
            return encoder.crc(this);            
        };
        String.prototype.hasEncoded = function(){
            return encoder.hasEncoded(this);            
        };
        String.prototype.stripUnicode = function(){
            return encoder.stripUnicode(this);            
        };
        String.prototype.correctEncoding = function(){
            return encoder.correctEncoding(this);            
        };
        String.prototype.htmlEncode = function(dbl?: boolean){
            return encoder.htmlEncode(this, dbl);            
        };
        String.prototype.htmlDecode = function(){
            return encoder.htmlDecode(this);            
        };
        String.prototype.inArray = function(arr: Array<string>){
            return encoder.inArray(this, arr);            
        };
    })
});

interface String {
    // Convert HTML entities into numerical entities
    HTML2Numerical(): string,
    // Convert Numerical entities into HTML entities
    NumericalToHTML(): string,
    // Encodes the basic 4 characters used to malform HTML in XSS hacks
    XSSEncode(en: boolean): string
    // Return a checksum number for this instance
    crc(): number,
    // returns true if a string contains html or numerical encoded entities
    hasEncoded(): boolean,
    // will remove any unicode characters
    stripUnicode(): string,    
    // corrects any double encoded &amp; entities e.g &amp;amp;
    correctEncoding(): string,
    // encode an input string into either numerical or HTML entities    
    htmlEncode(dbl?: boolean): string,    
    // HTML Decode numerical and HTML entities back to original values
    htmlDecode(): string,    
    // Return the position of this instance in an Array
    inArray(arr: Array<string>): number, 
}

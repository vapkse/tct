'option: strict';
$(function () {
    require(['../../common/ts/encoder'], function (utils) {
        var encoder = utils.Encoder;
        String.prototype.HTML2Numerical = function () {
            return encoder.HTML2Numerical(this);
        };
        String.prototype.NumericalToHTML = function () {
            return encoder.NumericalToHTML(this);
        };
        String.prototype.XSSEncode = function (en) {
            return encoder.XSSEncode(this, en);
        };
        String.prototype.crc = function () {
            return encoder.crc(this);
        };
        String.prototype.hasEncoded = function () {
            return encoder.hasEncoded(this);
        };
        String.prototype.stripUnicode = function () {
            return encoder.stripUnicode(this);
        };
        String.prototype.correctEncoding = function () {
            return encoder.correctEncoding(this);
        };
        String.prototype.htmlEncode = function (dbl) {
            return encoder.htmlEncode(this, dbl);
        };
        String.prototype.htmlDecode = function () {
            return encoder.htmlDecode(this);
        };
        String.prototype.inArray = function (arr) {
            return encoder.inArray(this, arr);
        };
    });
});

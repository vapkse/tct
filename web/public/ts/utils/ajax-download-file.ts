module utils {
    export interface AjaxDownloadFileOptions {
        url: string,
        type?: string,
        headers?: { [key: string]: string },
        data?: any,
        dataType?: string,
        async?: boolean
    }

    export class AjaxDownloadFile {
        private _onSuccessFinish: (status: number, statusText: string, res: { [key: string]: any }, headers: string) => void;
        private _onErrorFinish: (status: number, statusText: string, res: { [key: string]: any }, headers: string) => void;

        public static download(options: AjaxDownloadFileOptions) {
            var self = new AjaxDownloadFile() as AjaxDownloadFile;

            var url = options.url || window.location.href;
            var type = options.type || 'GET';
            var dataType = options.dataType || 'text';
            var data = options.data || null;
            var async = options.async || true;
            var headers = options.headers || { 'Content-Type': 'application/json; charset=UTF-8' };

            var xhr = new XMLHttpRequest();
            xhr = new XMLHttpRequest();
            xhr.addEventListener('load', function() {
                var res: {
                    [key: string]: any
                } = {};
                var success = xhr.status >= 200 && xhr.status < 300 || xhr.status === 304;

                if (success) {
                    res[dataType] = xhr.response;
                    self._onSuccessFinish(xhr.status, xhr.statusText, res, xhr.getAllResponseHeaders());
                } else {
                    res['text'] = xhr.statusText;
                    self._onErrorFinish(xhr.status, xhr.statusText, res, xhr.getAllResponseHeaders());
                }
            });


            xhr.open(type, url, async);
            xhr.responseType = dataType;

            for (var key in headers) {
                if (headers.hasOwnProperty(key)) {
                    xhr.setRequestHeader(key, headers[key]);
                }
            }

            xhr.send(data);

            return self;
        }

        public onSuccessFinish(fn: (status: number, statusText: string, res: { [key: string]: any }, headers: string) => void) {
            var self = this as AjaxDownloadFile;
            self._onSuccessFinish = fn;
            return self;
        }

        public onErrorFinish(fn: (status: number, statusText: string, res: { [key: string]: any }, headers: string) => void) {
            var self = this as AjaxDownloadFile;
            self._onErrorFinish = fn;
            return self;
        }
    }
}

interface HTMLAnchorElement {
    download: string
}

interface Window {
    webkitURL: URL
}
var utils;
(function (utils) {
    var Colors = (function () {
        function Colors() {
        }
        Colors.highchartsColors = function () {
            return [
                '#FFAA1D',
                '#FFF700',
                '#FF3855',
                '#299617',
                '#A7F432',
                '#2243B6',
                '#FD3A4A',
                '#5DADEC',
                '#5946B2',
                '#9C51B6',
                '#A83731',
                '#AF6E4D',
                '#BFAFB2',
                '#FF5470',
                '#FFDB00',
                '#FF7A00',
                '#FDFF00',
                '#87FF2A',
                '#0048BA',
                '#FF007C',
                '#E936A7',
                '#C46210',
                '#2E5894',
                '#9C2542',
                '#BF4F51',
                '#A57164',
                '#58427C',
                '#4A646C',
                '#85754E',
                '#319177',
                '#0A7E8C',
                '#FB4D46',
                '#9C7C38',
                '#8D4E85',
                '#FA5B3D',
                '#8FD400',
                '#D98695',
                '#757575',
                '#0081AB'
            ];
        };
        return Colors;
    })();
    utils.Colors = Colors;
})(utils || (utils = {}));

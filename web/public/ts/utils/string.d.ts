interface String {
    HTML2Numerical(): string;
    NumericalToHTML(): string;
    XSSEncode(en: boolean): string;
    crc(): number;
    hasEncoded(): boolean;
    stripUnicode(): string;
    correctEncoding(): string;
    htmlEncode(dbl?: boolean): string;
    htmlDecode(): string;
    inArray(arr: Array<string>): number;
}

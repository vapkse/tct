declare module utils {
    class Color {
        private r;
        private g;
        private b;
        private a;
        constructor(r?: number, g?: number, b?: number, a?: number);
        static fromRgbaString(rgba: string): Color;
        isEmpty(): boolean;
        red: number;
        green: number;
        blue: number;
        alpha: number;
    }
}

module utils {
    export class Color{
        private r: number;
        private g: number;
        private b: number;
        private a: number;
        
        constructor(r?: number, g?: number, b?: number, a?: number) {
            var self = this as Color;
            self.r = r;       
            self.g = g;       
            self.b = b;       
            self.a = a;       
        }
        
        public static fromRgbaString(rgba: string) {
            var color = new Color();

            if (!rgba) {
                return color;
            }

            var colors = rgba.match(/\d+/g);
            if (colors.length < 3) {
                return color;
            }            
            
            try {
                color.r = parseInt(colors[0]);
                color.g = parseInt(colors[1]);
                color.b = parseInt(colors[2]);
                if (colors.length > 3) {
                    color.a = parseInt(colors[3]);
                }
                return color;
            } catch (e) {
                return new Color();
            }
        }
        
        public isEmpty() {
            var self = this as Color;
            return self.r === undefined || self.g === undefined || self.b === undefined;
        }
        
        public get red() {
            var self = this as Color;
            return self.r;
        }

        public set red(value: number) {
            var self = this as Color;
            if (isNaN(value) || value < 0 || value > 255) {
                throw 'Invalid range.';
            }
            self.r = value;
        }

        public get green() {
            var self = this as Color;
            return self.g;
        }

        public set green(value: number) {
            var self = this as Color;
            if (isNaN(value) || value < 0 || value > 255) {
                throw 'Invalid range.';
            }
            self.g = value;
        }

        public get blue() {
            var self = this as Color;
            return self.b;
        }

        public set blue(value: number) {
            var self = this as Color;
            if (isNaN(value) || value < 0 || value > 255) {
                throw 'Invalid range.';
            }
            self.b = value;
        }

        public get alpha() {
            var self = this as Color;
            return self.a;
        }

        public set alpha(value: number) {
            var self = this as Color;
            if (isNaN(value) || value < 0 || value > 255) {
                throw 'Invalid range.';
            }
            self.a = value;
        }
    }
}
'option: strict';
$(function () {
    location.query = function () {
        var queryString = location.search.substring(1).split("&").reduce(function (result, value) {
            var parts = value.split('=');
            if (parts[0]) {
                result[decodeURIComponent(parts[0])] = decodeURIComponent(parts[1]);
            }
            return result;
        }, {});
        return queryString;
    };
});

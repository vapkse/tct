'use strict';

// TODO Export to .inc ltspice file

interface HashBases {
    [id: number]: TubeBase
}

interface HashTypes {
    [id: number]: TubeType
}

interface HashPins {
    [id: number]: TubePinout
}

module tct.home {
    // Get search parser module from common directory
    require(['../../common/ts/search-params'], function(searchParamsModule: any) {
        (function($: JQueryStatic) {
            $.StartScreen = function() {
                var settingsPanel: JQuery;
                var hashBases: HashBases;
                var hashTypes: HashTypes;
                var hashPins: HashPins;
                var resultTemplate: string;
                var searchParams: TubeSearch;
                var docSelector: JQuery;
                var usageSelector: JQuery;
                var resultArea: JQuery;
                var users: { [id: string]: user.UserEmail };
                var tubeDatas: { [key: string]: TubeData } = {};
                var translations = $('translations');

                var translatorControl: modern.TranslatorControl = $('#translator').translatorControl().on('translatorcontrolchange', function(e: modern.TranslatorControlEvent) {
                    hashTypes = undefined;
                    resultTemplate = undefined;
                }).data('modern-translatorControl');

                var container = $('#container');
                var transArea = $('#trans-area');
                var messagerControl: modern.Messager = $('#messager').messager().data('modern-messager');
                container.tooltipmanager().data('modern-tooltipmanager');

                var body = $('body');
                var pager = body.find('#pager-area');
                var $searchArea = body.find('#search-area');
                var $advancedArea = body.find('#advanced-area');
                var $errorFlag = body.find('.input-state-error');
                var fieldCriterionsControl: SearchCriterions;
                var simpleSearch = $searchArea.find('[search]').textinput()

                var uploader = $('form#upload');
                var uploadElement = uploader.find(':file');
                uploadElement.on('change', function() {
                    var input = $(this);
                    var files = input.prop('files');
                    if (files.length) {
                        $.waitPanel.show();
                        var formElement: HTMLFormElement = <HTMLFormElement>(uploader[0]);
                        var formData = new FormData(formElement);
                        var dataid = uploadElement.attr('tubeid');
                        var userid = uploadElement.attr('userid');
                        var url = '/document?d=attach&id=' + dataid;
                        if (userid) {
                            url += '&userid=' + userid;
                        }
                        $.ajax({
                            url: url,
                            type: 'POST',
                            timeout: 1800000,
                            // Form data
                            data: formData,					
                            //Options to tell jQuery not to process data or worry about content-type.
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(result: UploadResult) {
                                $.waitPanel.hide();
                                input.val('');
                            },
                            error: function(jqXHR: JQueryXHR, textStatus: string, errorThrown: string) {
                                $.waitPanel.hide();
                                messagerControl.show('error', (jqXHR && (jqXHR.responseText || jqXHR.statusText)) || errorThrown);
                                input.val('');
                            }
                        });
                    }
                });

                var showAdvancedSearch = function() {
                    var readyToShow = function() {
                        $searchArea.animate({ opacity: 0 }, 250, function() {
                            $searchArea.hide();
                            $advancedArea.show().css('opacity', 0).animate({ opacity: 1 }, 250);
                        })
                    }

                    if (!fieldCriterionsControl) {
                        // No criterions control, initialize the first time
                        // look ups are required for advanced search
                        var ajaxRequest = $.ajax({
                            url: '/search?lt&lb&lp',
                            dataType: 'json',
                            type: 'get',
                            timeout: 120000,
                            success: function(json) {
                                if (!json || json.error) {
                                    var msg = json ? json.error : 'Get lookups error.';
                                    messagerControl.show('error', msg);
                                } else {
                                    var result = json as TubesSearchResult;

                                    if (result.basesError) {
                                        messagerControl.show('error', result.basesError);
                                    } else if (result.bases) {
                                        hashBases = {};
                                        result.bases.map(b => hashBases[b._id] = b);
                                    }

                                    if (result.pinoutsError) {
                                        messagerControl.show('error', result.pinoutsError);
                                    } else if (result.pinouts) {
                                        hashPins = {};
                                        result.pinouts.map(p => hashPins[p._id] = p);
                                    }

                                    if (result.typesError) {
                                        messagerControl.show('error', result.typesError);
                                    } else if (result.types) {
                                        hashTypes = {};
                                        result.types.map(t => hashTypes[t._id] = t);
                                    }
                                    // Instanciate parse with current simple search string
                                    var el = $advancedArea.find('#criterions');
                                    el.searchCriterions({
                                        modelFields: searchParamsModule.TubeSearch.searchDataModel,
                                        expressions: searchParams,
                                        bases: result.bases,
                                        types: result.types,
                                        pinouts: result.pinouts,
                                        user: user
                                    }).bind('searchcriterionschange', function(e: searchCriterionsEvent) {
                                        // Attention, this event happens a lot
                                        replaceSearchParams(null, searchParamsModule.TubeSearch.FromSearchExpressions(e.expressions));
                                        if ($advancedArea.find('[field-criteria]').length === 0) {
                                            showSimpleSearch();
                                        }
                                    }).bind('searchcriterionsready', function(e: JQueryEventObject) {
                                        readyToShow();
                                    }).bind('searchcriterionssearch', function(e: JQueryEventObject) {
                                        $('[search-btn]').focus();
                                        setTimeout(function() {
                                            search(null, true);
                                        }, 100)
                                    });

                                    fieldCriterionsControl = el.searchCriterions().data('tct-searchCriterions');
                                }
                            },
                            error: function(e) {
                                messagerControl.show('error', e);
                            }
                        })
                    } else {
                        fieldCriterionsControl.setExpressions(searchParams);
                        readyToShow();
                    }
                }

                var showSimpleSearch = function() {
                    var qr = searchParams ? searchParams.toString() : '';
                    var text = (qr !== 'news') ? qr : '';
                    simpleSearch.textinput('value', text);
                    $advancedArea.animate({ opacity: 0 }, 250, function() {
                        $advancedArea.hide();
                        $searchArea.show().css('opacity', 0).animate({ opacity: 1 }, 250);
                    })
                }

                $advancedArea.find('#addnewfield').on('click', function() {
                    if (fieldCriterionsControl) {
                        fieldCriterionsControl.addNew();
                    }
                })

                $searchArea.find('.advanced').on('click', function() {
                    showAdvancedSearch();
                });

                $searchArea.find('.latest').on('click', function() {
                    // latest news search
                    $.pageState.replaceUrl('?q=news');
                    loadSearchInputFromUrl();
                    search(1, true);
                });

                $advancedArea.find('.simple').on('click', function() {
                    showSimpleSearch();
                });

                var deleteDlgCont = $('#light-theme[delete-dialog]');
                deleteDlgCont.find('[cancel]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[delete-dialog]').data('dialog').close();
                });
                deleteDlgCont.find('[validate]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[delete-dialog]').data('dialog').validate();
                });

                resultArea = body.find('#result-area').on('click', function(e: JQueryEventObject) {
                    var $dataClick = $(e.target).closest('[data-click]');
                    if ($dataClick.is('.disabled')) {
                        return;
                    }

                    var tubeResult = $(e.target).closest('[data-id]');
                    var tubeid = parseInt(tubeResult.attr('data-id'));
                    var userid = tubeResult.attr('user-id');
                    var tubeKey = tubeid + (userid || '')
                    var func = $dataClick.attr('data-click');
                    if (func) {
                        switch (func) {
                            case 'view':
                                var qstring: Array<string> = [];
                                qstring.push('id=' + tubeid);
                                qstring.push('rl=' + encodeURIComponent(location.search));
                                if (userid) {
                                    qstring.push('userid=' + encodeURIComponent(userid));
                                }
                                location.href = '/viewer?' + qstring.join('&');
                                break;

                            case 'unit':
                                var unitIndex = parseInt($(e.target).closest('[u-index]').attr('u-index'));
                                var tubeResultData = getResultData(tubeDatas[tubeKey], unitIndex);
                                tubeResult.tubeResult('setOptions', {
                                    datas: tubeResultData,
                                    id: tubeid,
                                });
                                break;

                            case 'docs':
                                var selectorOptions: DocSelectorOptions = {
                                    tubeData: tubeDatas[tubeKey],
                                    title: translations.find('#seltitle').prop('innerHTML'),
                                    mode: 'viewer',
                                    showUsages: true,
                                    userid: userid
                                };

                                if (!docSelector) {
                                    requirejs(['docSelector'], function() {
                                        docSelector = $('[doc-selector]');
                                        docSelector.docSelector(selectorOptions).bind('docselectorcreated', function() {
                                            docSelector.docSelector('show');
                                        });
                                    })
                                } else {
                                    docSelector.docSelector('setOptions', selectorOptions);
                                }
                                break;

                            case 'menu':
                                requirejs(['dropdown'], function(ns: any) {
                                    var dropDown = new ns.DropDown();

                                    dropDown.show({
                                        template: $('#tubemenu').html(),
                                        backdrop: true,
                                        backdropCancel: true,
                                        backdropId: 'dmenu-backdrop',
                                        modalId: 'dmenu-body',
                                        position: {
                                            of: $dataClick,
                                            within: resultArea,
                                            my: 'right top',
                                            at: 'right top'
                                        },
                                        cancelOnEsc: true
                                    }).modalBody.attr('deletable', user && userid && (userid === user._id || user.role === 'admin'))
                                        .attr('mergeable', userid && user && user.role === 'admin')
                                        .attr('ismobile', window.mobilecheck())
                                        .on('click', function(e: JQueryEventObject) {
                                            // Bind menu
                                            var $dataClick = $(e.target).closest('[data-click]');
                                            if ($dataClick.is('.disabled')) {
                                                return;
                                            }

                                            var func = $dataClick.attr('data-click');
                                            if (func) {
                                                switch (func) {
                                                    case 'merge':
                                                        location.href = '/editor?unit=0&id=' + tubeid + '&merge=' + encodeURIComponent(userid) + '&rl=' + encodeURIComponent(location.search);
                                                        break;

                                                    case 'create':
                                                        var sb = [] as Array<string>;
                                                        sb.push('/creator?id=');
                                                        sb.push(String(tubeid));
                                                        if (userid) {
                                                            sb.push('&userid=');
                                                            sb.push(encodeURIComponent(userid));
                                                        }
                                                        sb.push('&rl=');
                                                        sb.push(encodeURIComponent(location.search));
                                                        location.href = sb.join('');
                                                        break;

                                                    case 'edit':
                                                        var sb = [] as Array<string>;
                                                        sb.push('/editor?unit=0&id=');
                                                        sb.push(String(tubeid));
                                                        if (userid) {
                                                            sb.push('&userid=');
                                                            sb.push(encodeURIComponent(userid));
                                                        }
                                                        sb.push('&rl=');
                                                        sb.push(encodeURIComponent(location.search));
                                                        location.href = sb.join('');
                                                        break;

                                                    case 'upload':
                                                        dropDown.close();
                                                        if (user) {
                                                            uploadElement.attr('tubeid', tubeid).attr('userid', userid).click();
                                                        } else {
                                                            location.href = '/login?rl=' + encodeURIComponent(location.pathname + location.search);
                                                        }
                                                        break;

                                                    case 'usage':
                                                        var sb = [] as Array<string>;
                                                        sb.push('/usages?usageid=new&id=');
                                                        sb.push(String(tubeid));
                                                        if (userid) {
                                                            sb.push('&userid=');
                                                            sb.push(encodeURIComponent(userid));
                                                        }
                                                        sb.push('&rl=');
                                                        sb.push(encodeURIComponent(location.search));
                                                        location.href = sb.join('');
                                                        break;

                                                    case 'delete':
                                                        // Confirm    
                                                        dropDown.close();
                                                        var dialog = deleteDlgCont.data('dialog');
                                                        dialog.validate = function() {
                                                            var qstring: Array<string> = ['e=tube'];
                                                            qstring.push('id=' + tubeid);
                                                            qstring.push('userid=' + encodeURIComponent(userid));
                                                            // Post request
                                                            $.waitPanel.show();
                                                            var ajaxRequest = $.ajax({
                                                                url: '/delete?' + qstring.join('&'),
                                                                dataType: 'json',
                                                                type: 'post',
                                                                timeout: 60000,
                                                                success: function(json) {
                                                                    if (json && json.error) {
                                                                        messagerControl.show('error', json.error);
                                                                    } else {
                                                                        // Remove search result
                                                                        setTimeout(function() {
                                                                            tubeResult.css('overflow', 'hidden');
                                                                            tubeResult.animate({ height: 0, opacity: 0, marginTop: 0 }, 300, function() {
                                                                                tubeResult.remove();
                                                                            })
                                                                        }, 1);
                                                                    }
                                                                    dialog.close();
                                                                    $.waitPanel.hide();
                                                                },
                                                                error: function(e) {
                                                                    messagerControl.show('error', e);
                                                                    dialog.close();
                                                                    $.waitPanel.hide();
                                                                },
                                                            });
                                                        }

                                                        deleteDlgCont.css('opacity', 0);
                                                        dialog.open();
                                                        deleteDlgCont.animate({ opacity: 1 }, 500);
                                                        break;

                                                }
                                            }
                                        });
                                });
                                break;
                        }
                    }
                });
                    
                // Bind buttons
                var header = body.find('header');
                header.find('[settings-panel]').on('click', function() {
                    // Instianciate settings panel if the button is pressed for the first time
                    if (!settingsPanel) {
                        requirejs(['settingsPanel'], function(settingsPanel: JQuery) {
                            settingsPanel = $('#settings-panel');
                            var settings = new tct.Settings(localStorage);
                            settingsPanel.settingsPanel(settings).bind('settingspanelchange', function(e: tct.SettingsPanelEvent, data: tct.SettingsPanelOptions) {
                                if (data.colorSettings.scheme) {
                                    body.attr('id', 'scheme-' + data.colorSettings.scheme);
                                }
                                if (data.colorSettings.theme) {
                                    body.attr('theme', data.colorSettings.theme);
                                }
                            });
                            settingsPanel.settingsPanel('toogle');
                        })
                    } else {
                        settingsPanel.settingsPanel('toogle');
                    }
                });
                    
                // New tube
                header.find('[new-tube]').on('click', function() {
                    location.href = '/editor?id=new&rl=' + encodeURIComponent(location.search);
                })
                    
                // User infos panel
                var huser = $('#user').html();
                var user: user.User = huser && JSON.parse(huser);
                if (user && user.role === 'admin') {
                    $('#admin').show();
                }
                var userInfosPanel = header.find('#user-infos-panel').userInfos({
                    user: user
                }).bind('userinfoslogin', function() {
                    location.href = '/login?rl=' + encodeURIComponent(location.pathname + location.search);
                }).bind('userinfoscreated', function() {
                    transArea.animate({ opacity: 1 }, 500);
                });

                var getResultData = function(tubeInfos: TubeData, unitIndex: number) {
                    var baseId = tubeInfos.base;
                    var base = baseId && hashBases && hashBases[baseId];

                    var pinoutId = tubeInfos.pinout;
                    var pinout = pinoutId && hashPins && hashPins[pinoutId];

                    var descr = '';
                    var type: TubeType;
                    var typeuid = '';
                    var cfg: TubePins;
                    var typeId = tubeInfos.type;
                    if (typeId) {
                        type = hashTypes[typeId];
                        if (type) {
                            descr = type.text;
                            typeuid = type.uid;
                            cfg = type.cfg[unitIndex];
                        }
                    }

                    var unit = tubeInfos.units && tubeInfos.units[unitIndex] || [] as TubeUnitData;
                    var docs = tubeInfos.documents || [];
                    var usages = tubeInfos.usages || [];

                    var tubeResultData: TubeResultData = {
                        name: tubeInfos.name,
                        type: descr,
                        typeuid: typeuid,
                        base: base ? base.name : '-',
                        pinout: pinout ? pinout.name : '-',
                        vh1: tubeInfos.vh1 ? tubeInfos.vh1 + 'V' : '-',
                        ih1: tubeInfos.ih1 && tubeInfos.ih1 + 'A',
                        h2tt: tubeInfos.vh2 && (tubeInfos.vh1 + 'V ' + tubeInfos.ih1 + 'A  or  ' + tubeInfos.vh2 + 'V ' + tubeInfos.ih1 + 'A'),
                        docsCount: docs.length + usages.length,
                        usagesCount: tubeInfos.usages && tubeInfos.usages.length,
                        unitsCount: tubeInfos.units && tubeInfos.units.length,
                        unitIndex: unitIndex,
                        userId: tubeInfos._userId,
                        userEmail: tubeInfos._userId && users && users[tubeInfos._userId]
                    }

                    var pins = cfg && cfg.pins;
                    if (pins && pins.indexOf('a') >= 0) {
                        if (unit.vamax || unit.pamax) {
                            tubeResultData.vamax = unit.vamax ? unit.vamax + 'V' : '';
                            tubeResultData.pamax = unit.pamax ? unit.pamax + 'W' : '';
                        } else {
                            tubeResultData.vamax = '-';
                            tubeResultData.pamax = '';
                        }
                    } else {
                        tubeResultData.vamax = undefined;
                        tubeResultData.pamax = undefined;
                    }

                    if (pins && pins.indexOf('g2') >= 0) {
                        if (unit.vg2max || unit.pg2max) {
                            tubeResultData.vg2max = unit.vg2max ? unit.vg2max + 'V' : '';
                            tubeResultData.pg2max = unit.pg2max ? unit.pg2max + 'W' : '';
                        } else {
                            tubeResultData.vg2max = '-';
                            tubeResultData.pg2max = '';
                        }
                    } else {
                        tubeResultData.vg2max = undefined;
                        tubeResultData.pg2max = undefined;
                    }

                    return tubeResultData;
                }

                var loadTubesResult = function(json: TubesSearchResult, done: Function) {
                    var index = 0;
                    var resultEntities = resultArea.children('.tube-result');

                    if (!json) {
                        resultEntities.hide();
                        return;
                    }

                    tubeDatas = {};
                    require(['tubeResult'], function() {
                        var loadTube = function(t: number, cb: Function) {
                            if (!json.tubes || t >= json.tubes.length) {
                                cb();
                                return;
                            }

                            var tubeInfos = json.tubes[t];
                            var key = tubeInfos._id + (tubeInfos._userId || '');
                            tubeDatas[key] = tubeInfos;
                            var tubeResultData = getResultData(tubeInfos, 0);

                            if (index >= resultEntities.length) {
                                $('<div></div>').appendTo(resultArea).tubeResult({
                                    datas: tubeResultData,
                                    template: resultTemplate,
                                    id: tubeInfos._id,
                                });
                            } else {
                                $(resultEntities[index]).tubeResult('setOptions', {
                                    datas: tubeResultData,
                                    id: tubeInfos._id,
                                }).show();
                            }

                            index++;
                            /*setTimeout(function() {
                                loadTube(t + 1, cb);
                            }, 1);*/
                            loadTube(t + 1, cb);
                        }

                        loadTube(0, function() {
                            for (var i = index; i < resultEntities.length; i++) {
                                $(resultEntities[i]).hide();
                            }

                            done();
                        })
                    })
                }

                var loadNewsResult = function(json: NewsSearchResult, done: Function) {
                    resultArea.children('.news-result').remove();

                    if (!json || !json.news || !json.news.length) {
                        done();
                        return;
                    }

                    var stb = ['<div class="news-result"><div class="newsHeader" lang uid="latestnews">Latest News</div>'];

                    json.news.forEach(function(news) {
                        var date = news.date && new Date(news.date);
                        stb.push('<div class="row"><span class="date">');
                        stb.push(date.toLocaleString());
                        stb.push('</span>');
                        stb.push('<span class="news" lang uid="');
                        stb.push(news.uid);
                        if (news.params) {
                            stb.push('" tpar="');
                            stb.push(encodeURIComponent(JSON.stringify(news.params)));
                        }
                        stb.push('">');
                        stb.push(news.text);
                        stb.push('</span></div>');
                    })

                    stb.push('</div>');
                    $(stb.join('')).appendTo(resultArea).find('a');
                    done();
                }

                var loadSearchResults = function(json?: SearchResult, scrollto?: JQuery) {
                    loadNewsResult(json as NewsSearchResult, function() {
                        loadTubesResult(json as TubesSearchResult, function() {
                            loadPager(json);

                            if (scrollto) {
                                $(document).scrollTop(scrollto.position().top);
                            }
                        })
                    })
                }

                var loadPager = function(json?: SearchResult) {
                    pager.empty();
                    if (!json) {
                        return;
                    }

                    var page = json.page;
                    if (json.pageCount > 1) {
                        var html = [] as Array<string>;
                        var buttonClass = 'button no-border bg-transparent fg-theme-over fg-light';
                        var from: number;
                        if (page > 7) {
                            from = page - 3;
                        } else {
                            from = 1;
                        }

                        var to = Math.min(from + 7, json.pageCount);
                        var from = Math.max(1, to - 7);
                        if (from > 1) {
                            html.push('<span class="');
                            html.push(buttonClass);
                            html.push('" data-page="1"><span class="mif-chevron-thin-left"></span></span>');
                        }

                        for (var i = from; i <= to; i++) {
                            html.push('<span class="');
                            html.push(buttonClass);
                            html.push('" data-page="');
                            html.push(String(i));
                            html.push('">');
                            html.push(String(i));
                            html.push('</span>');
                        }

                        if (to < json.pageCount) {
                            html.push('<span class="');
                            html.push(buttonClass);
                            html.push('" data-page="');
                            html.push(String(json.pageCount));
                            html.push('"><span class="mif-chevron-thin-right"></span></span>');
                        }

                        pager.append(html.join(''));

                        pager.find('[data-page="' + page + '"]').addClass('current fg-theme disabled');
                        pager.find('[data-page]:not(.current)').on('click', function(e: JQueryEventObject) {
                            var page = parseInt($(e.target).closest('[data-page]').attr('data-page'));
                            if (isNaN(page) || page <= 0) {
                                page = 1;
                            }
                            search(page, false, pager);
                        })
                    }
                }

                var searchTimeout: number;
                var search = function(page: number, reset: boolean, scrollto?: JQuery) {
                    if (searchTimeout) {
                        clearTimeout(searchTimeout);
                    }
                    searchTimeout = setTimeout(function() {
                        if (searchParams) {
                            var searchText = searchParams.toString();
                            $.waitPanel.show();

                            var query = [] as Array<string>;
                            query.push('/search?q=');

                            var encodedSearchUrlParams = encodeURIComponent(searchText);
                            query.push(encodedSearchUrlParams);

                            if (page && !isNaN(page)) {
                                query.push('&p=');
                                query.push(String(page));
                            }

                            var lq = location.query();
                            if (lq['max']) {
                                query.push('&max=');
                                query.push(lq['max']);
                            } else if (window.mobilecheck()) {
                                query.push('&max=20');
                            } else {
                                query.push('&max=50');
                            }
                                
                            // Ask for lookup if not already here
                            if (!hashBases) {
                                query.push('&lb');
                            }
                            if (!hashTypes) {
                                query.push('&lt');
                            }
                            if (!hashPins) {
                                query.push('&lp');
                            }
                            if (!resultTemplate) {
                                query.push('&tmpl=widgets%2Ftube-result.html');
                            }
                            if (reset) {
                                query.push('&reset');
                            }

                            var ajaxRequest = $.ajax({
                                url: query.join(''),
                                dataType: 'json',
                                type: 'get',
                                timeout: 120000,
                                success: function(json) {
                                    if (!json) {
                                        body.removeClass('params-error');
                                        loadSearchResults();
                                    } else if (json && json.error) {
                                        body.addClass('params-error');
                                        $errorFlag.attr('tooltip', json.error).removeAttr('uid');
                                        messagerControl.show('error', json.error);
                                    } else {
                                        body.removeClass('params-error');
                                        var result = json as TubesSearchResult;

                                        if (result.template) {
                                            resultTemplate = result.template;
                                        }

                                        if (result.basesError) {
                                            messagerControl.show('error', result.basesError);
                                        } else if (result.bases) {
                                            hashBases = {};
                                            result.bases.map(b => hashBases[b._id] = b);
                                        }

                                        if (result.pinoutsError) {
                                            messagerControl.show('error', result.pinoutsError);
                                        } else if (result.pinouts) {
                                            hashPins = {};
                                            result.pinouts.map(p => hashPins[p._id] = p);
                                        }

                                        if (result.typesError) {
                                            messagerControl.show('error', result.typesError);
                                        } else if (result.types) {
                                            hashTypes = {};
                                            result.types.map(t => hashTypes[t._id] = t);
                                        }

                                        if (json.users) {
                                            users = json.users;
                                        }

                                        loadSearchResults(json, scrollto);
                                                                                                                        
                                        // Update url
                                        var state: SearchState = {
                                            tubes: result.tubes,
                                            page: result.page,
                                            pageCount: result.pageCount,
                                            scrollPos: $(document).scrollTop()
                                        }
                                        var searchState: IUrlParams = { q: encodedSearchUrlParams };
                                        if (page) {
                                            searchState['p'] = String(page);
                                        } else {
                                            searchState['p'] = null;
                                        }
                                        $.pageState.replaceOrAddState(state, searchState)
                                    }
                                    $.waitPanel.hide();
                                },
                                error: function(e) {
                                    if (e.statusText !== 'canceled') {
                                        body.addClass('params-error');
                                        $errorFlag.attr('tooltip', e.responseText || e.statusText).removeAttr('uid');
                                        messagerControl.show('error', e);
                                    }
                                    $.waitPanel.hide();
                                },
                            });
                        }
                    }, 50);
                }

                var replaceSearchParams = function(err: Error, ts: TubeSearch) {
                    // Manage parsing error
                    if (err) {
                        var translation: translator.Translation = {};
                        translation[err.name] = err.message;
                        translatorControl.translate(translation, function(error, translated) {
                            body.addClass('params-error');
                            $errorFlag.attr('tooltip', translated[err.name]).attr('uid', err.name);
                        })
                    } else {
                        body.removeClass('params-error');
                    }
                    searchParams = ts;
                }   
                    
                // Search binding
                $('[search-btn]').on('click', function() {
                    search(null, true);
                });
                simpleSearch.find('input').on('keypress', (e: JQueryKeyEventObject) => {
                    if (e.keyCode === 13) {
                        search(null, true);
                    }
                });
                simpleSearch.bind('textinputchange', function(e: modern.TextInputEvent) {
                    var value = e.value.trim();
                    searchParamsModule.TubeSearch.FromQueryExpression(value, function(err: Error, ts: TubeSearch) {
                        replaceSearchParams(err, ts);
                    })
                })

                var loadSearchInputFromUrl = function() {
                    var query = location.query();
                    if (query['q']) {
                        searchParamsModule.TubeSearch.FromQueryExpression(query['q'], function(err: Error, ts: TubeSearch) {
                            replaceSearchParams(err, ts);

                            if ($advancedArea.is(':visible')) {
                                // Show advanced search
                                showAdvancedSearch();
                            } else {
                                // Show simple search
                                showSimpleSearch();
                            }
                        })
                    }
                }

                var loadFromUrl = function() {
                    var query = location.query();
                    var queryExp = query['q'] || 'news';
                    searchParamsModule.TubeSearch.FromQueryExpression(queryExp, function(err: Error, ts: TubeSearch) {
                        if (!ts) {
                            ts = searchParamsModule.TubeSearch.FromSearchExpressions([{
                                fieldName: '_text',
                                wholeWord: true,
                                value: queryExp
                            } as QueryExpression]);
                        }

                        replaceSearchParams(err, ts);

                        // Show simple search
                        showSimpleSearch();

                        var page = parseInt(query['p']);
                        search(page, true);
                    })
                }

                var registerForPush = function() {
                    if (!user) {
                        return;
                    }          
                                           
                    // Socket io, register to upload result event
                    var io = $.io.connect({
                        query: {
                            event: 'documentAttached#' + user.userName
                        }
                    });
                    io.on('documentAttached', function(result: UploadResult) {
                        result.files.forEach(function(file) {
                            if (file.error) {
                                messagerControl.show('error', file.error);
                            } else {
                                var fileName = file.filename;
                                var msg: modern.MessagerMessage = {
                                    type: 'info',
                                    name: 'msg-fileuploaded',
                                    message: 'File \\0 Uploaded',
                                    autoCloseDelay: 6,
                                    params: { 0: fileName }
                                }

                                $.translator.translateMessages([msg], function(translated: Array<translator.Message>) {
                                    messagerControl.showMessages(translated);
                                });
                            }
                        });

                        // Get the right result area
                        var tubeData = result.tubeResult.tubeData;
                        var tubeResult = resultArea.find('[data-id="' + tubeData._id + '"]');
                        // Refresh stored collection for the incons functionalities
                        var unitIndex = parseInt(tubeResult.find('[u-index]').attr('u-index'));
                        var key = tubeData._id + (tubeData._userId || '');
                        tubeDatas[key] = tubeData;
                        // Refresh the result area
                        var tubeResultData = getResultData(tubeData, unitIndex);
                        tubeResult.tubeResult('setOptions', {
                            datas: tubeResultData,
                            id: tubeData._id,
                        });
                    });
                }

                $(window).on('popstate', function(e) {
                    var event = <PopStateEvent>e.originalEvent;
                    if (event && event.state && event.state.j) {
                        var state = event.state.j as SearchState;
                        loadSearchResults(state);
                        loadSearchInputFromUrl();
                        setTimeout(function() {
                            $(document).scrollTop(state.scrollPos);
                        }, 1);
                    } else {
                        // Reload datas from url
                        loadFromUrl();
                    }
                });

                setTimeout(function() {
                    simpleSearch.textinput('focus');
                }, 200);

                loadFromUrl();
                registerForPush();
            };
        })(jQuery);

        $(function() {
            if (location.hostname === '127.0.0.1') {
                var browserVersion = window.browserVersion();
                if (browserVersion.version > 9) {
                    $('head').append('<script type="text/javascript" src="http://127.0.0.1:38080/livereload.js"><\/script>');
                }
            }

            $.StartScreen();
        });
    })
}

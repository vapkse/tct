'use strict';
var tct;
(function (tct) {
    var home;
    (function (home) {
        require(['../../common/ts/search-params'], function (searchParamsModule) {
            (function ($) {
                $.StartScreen = function () {
                    var settingsPanel;
                    var hashBases;
                    var hashTypes;
                    var hashPins;
                    var resultTemplate;
                    var searchParams;
                    var docSelector;
                    var usageSelector;
                    var resultArea;
                    var users;
                    var tubeDatas = {};
                    var translations = $('translations');
                    var translatorControl = $('#translator').translatorControl().on('translatorcontrolchange', function (e) {
                        hashTypes = undefined;
                        resultTemplate = undefined;
                    }).data('modern-translatorControl');
                    var container = $('#container');
                    var transArea = $('#trans-area');
                    var messagerControl = $('#messager').messager().data('modern-messager');
                    container.tooltipmanager().data('modern-tooltipmanager');
                    var body = $('body');
                    var pager = body.find('#pager-area');
                    var $searchArea = body.find('#search-area');
                    var $advancedArea = body.find('#advanced-area');
                    var $errorFlag = body.find('.input-state-error');
                    var fieldCriterionsControl;
                    var simpleSearch = $searchArea.find('[search]').textinput();
                    var uploader = $('form#upload');
                    var uploadElement = uploader.find(':file');
                    uploadElement.on('change', function () {
                        var input = $(this);
                        var files = input.prop('files');
                        if (files.length) {
                            $.waitPanel.show();
                            var formElement = (uploader[0]);
                            var formData = new FormData(formElement);
                            var dataid = uploadElement.attr('tubeid');
                            var userid = uploadElement.attr('userid');
                            var url = '/document?d=attach&id=' + dataid;
                            if (userid) {
                                url += '&userid=' + userid;
                            }
                            $.ajax({
                                url: url,
                                type: 'POST',
                                timeout: 1800000,
                                data: formData,
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function (result) {
                                    $.waitPanel.hide();
                                    input.val('');
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    $.waitPanel.hide();
                                    messagerControl.show('error', (jqXHR && (jqXHR.responseText || jqXHR.statusText)) || errorThrown);
                                    input.val('');
                                }
                            });
                        }
                    });
                    var showAdvancedSearch = function () {
                        var readyToShow = function () {
                            $searchArea.animate({ opacity: 0 }, 250, function () {
                                $searchArea.hide();
                                $advancedArea.show().css('opacity', 0).animate({ opacity: 1 }, 250);
                            });
                        };
                        if (!fieldCriterionsControl) {
                            var ajaxRequest = $.ajax({
                                url: '/search?lt&lb&lp',
                                dataType: 'json',
                                type: 'get',
                                timeout: 120000,
                                success: function (json) {
                                    if (!json || json.error) {
                                        var msg = json ? json.error : 'Get lookups error.';
                                        messagerControl.show('error', msg);
                                    }
                                    else {
                                        var result = json;
                                        if (result.basesError) {
                                            messagerControl.show('error', result.basesError);
                                        }
                                        else if (result.bases) {
                                            hashBases = {};
                                            result.bases.map(function (b) { return hashBases[b._id] = b; });
                                        }
                                        if (result.pinoutsError) {
                                            messagerControl.show('error', result.pinoutsError);
                                        }
                                        else if (result.pinouts) {
                                            hashPins = {};
                                            result.pinouts.map(function (p) { return hashPins[p._id] = p; });
                                        }
                                        if (result.typesError) {
                                            messagerControl.show('error', result.typesError);
                                        }
                                        else if (result.types) {
                                            hashTypes = {};
                                            result.types.map(function (t) { return hashTypes[t._id] = t; });
                                        }
                                        var el = $advancedArea.find('#criterions');
                                        el.searchCriterions({
                                            modelFields: searchParamsModule.TubeSearch.searchDataModel,
                                            expressions: searchParams,
                                            bases: result.bases,
                                            types: result.types,
                                            pinouts: result.pinouts,
                                            user: user
                                        }).bind('searchcriterionschange', function (e) {
                                            replaceSearchParams(null, searchParamsModule.TubeSearch.FromSearchExpressions(e.expressions));
                                            if ($advancedArea.find('[field-criteria]').length === 0) {
                                                showSimpleSearch();
                                            }
                                        }).bind('searchcriterionsready', function (e) {
                                            readyToShow();
                                        }).bind('searchcriterionssearch', function (e) {
                                            $('[search-btn]').focus();
                                            setTimeout(function () {
                                                search(null, true);
                                            }, 100);
                                        });
                                        fieldCriterionsControl = el.searchCriterions().data('tct-searchCriterions');
                                    }
                                },
                                error: function (e) {
                                    messagerControl.show('error', e);
                                }
                            });
                        }
                        else {
                            fieldCriterionsControl.setExpressions(searchParams);
                            readyToShow();
                        }
                    };
                    var showSimpleSearch = function () {
                        var qr = searchParams ? searchParams.toString() : '';
                        var text = (qr !== 'news') ? qr : '';
                        simpleSearch.textinput('value', text);
                        $advancedArea.animate({ opacity: 0 }, 250, function () {
                            $advancedArea.hide();
                            $searchArea.show().css('opacity', 0).animate({ opacity: 1 }, 250);
                        });
                    };
                    $advancedArea.find('#addnewfield').on('click', function () {
                        if (fieldCriterionsControl) {
                            fieldCriterionsControl.addNew();
                        }
                    });
                    $searchArea.find('.advanced').on('click', function () {
                        showAdvancedSearch();
                    });
                    $searchArea.find('.latest').on('click', function () {
                        $.pageState.replaceUrl('?q=news');
                        loadSearchInputFromUrl();
                        search(1, true);
                    });
                    $advancedArea.find('.simple').on('click', function () {
                        showSimpleSearch();
                    });
                    var deleteDlgCont = $('#light-theme[delete-dialog]');
                    deleteDlgCont.find('[cancel]').on('click', function (e) {
                        $(e.target).closest('[delete-dialog]').data('dialog').close();
                    });
                    deleteDlgCont.find('[validate]').on('click', function (e) {
                        $(e.target).closest('[delete-dialog]').data('dialog').validate();
                    });
                    resultArea = body.find('#result-area').on('click', function (e) {
                        var $dataClick = $(e.target).closest('[data-click]');
                        if ($dataClick.is('.disabled')) {
                            return;
                        }
                        var tubeResult = $(e.target).closest('[data-id]');
                        var tubeid = parseInt(tubeResult.attr('data-id'));
                        var userid = tubeResult.attr('user-id');
                        var tubeKey = tubeid + (userid || '');
                        var func = $dataClick.attr('data-click');
                        if (func) {
                            switch (func) {
                                case 'view':
                                    var qstring = [];
                                    qstring.push('id=' + tubeid);
                                    qstring.push('rl=' + encodeURIComponent(location.search));
                                    if (userid) {
                                        qstring.push('userid=' + encodeURIComponent(userid));
                                    }
                                    location.href = '/viewer?' + qstring.join('&');
                                    break;
                                case 'unit':
                                    var unitIndex = parseInt($(e.target).closest('[u-index]').attr('u-index'));
                                    var tubeResultData = getResultData(tubeDatas[tubeKey], unitIndex);
                                    tubeResult.tubeResult('setOptions', {
                                        datas: tubeResultData,
                                        id: tubeid,
                                    });
                                    break;
                                case 'docs':
                                    var selectorOptions = {
                                        tubeData: tubeDatas[tubeKey],
                                        title: translations.find('#seltitle').prop('innerHTML'),
                                        mode: 'viewer',
                                        showUsages: true,
                                        userid: userid
                                    };
                                    if (!docSelector) {
                                        requirejs(['docSelector'], function () {
                                            docSelector = $('[doc-selector]');
                                            docSelector.docSelector(selectorOptions).bind('docselectorcreated', function () {
                                                docSelector.docSelector('show');
                                            });
                                        });
                                    }
                                    else {
                                        docSelector.docSelector('setOptions', selectorOptions);
                                    }
                                    break;
                                case 'menu':
                                    requirejs(['dropdown'], function (ns) {
                                        var dropDown = new ns.DropDown();
                                        dropDown.show({
                                            template: $('#tubemenu').html(),
                                            backdrop: true,
                                            backdropCancel: true,
                                            backdropId: 'dmenu-backdrop',
                                            modalId: 'dmenu-body',
                                            position: {
                                                of: $dataClick,
                                                within: resultArea,
                                                my: 'right top',
                                                at: 'right top'
                                            },
                                            cancelOnEsc: true
                                        }).modalBody.attr('deletable', user && userid && (userid === user._id || user.role === 'admin'))
                                            .attr('mergeable', userid && user && user.role === 'admin')
                                            .attr('ismobile', window.mobilecheck())
                                            .on('click', function (e) {
                                            var $dataClick = $(e.target).closest('[data-click]');
                                            if ($dataClick.is('.disabled')) {
                                                return;
                                            }
                                            var func = $dataClick.attr('data-click');
                                            if (func) {
                                                switch (func) {
                                                    case 'merge':
                                                        location.href = '/editor?unit=0&id=' + tubeid + '&merge=' + encodeURIComponent(userid) + '&rl=' + encodeURIComponent(location.search);
                                                        break;
                                                    case 'create':
                                                        var sb = [];
                                                        sb.push('/creator?id=');
                                                        sb.push(String(tubeid));
                                                        if (userid) {
                                                            sb.push('&userid=');
                                                            sb.push(encodeURIComponent(userid));
                                                        }
                                                        sb.push('&rl=');
                                                        sb.push(encodeURIComponent(location.search));
                                                        location.href = sb.join('');
                                                        break;
                                                    case 'edit':
                                                        var sb = [];
                                                        sb.push('/editor?unit=0&id=');
                                                        sb.push(String(tubeid));
                                                        if (userid) {
                                                            sb.push('&userid=');
                                                            sb.push(encodeURIComponent(userid));
                                                        }
                                                        sb.push('&rl=');
                                                        sb.push(encodeURIComponent(location.search));
                                                        location.href = sb.join('');
                                                        break;
                                                    case 'upload':
                                                        dropDown.close();
                                                        if (user) {
                                                            uploadElement.attr('tubeid', tubeid).attr('userid', userid).click();
                                                        }
                                                        else {
                                                            location.href = '/login?rl=' + encodeURIComponent(location.pathname + location.search);
                                                        }
                                                        break;
                                                    case 'usage':
                                                        var sb = [];
                                                        sb.push('/usages?usageid=new&id=');
                                                        sb.push(String(tubeid));
                                                        if (userid) {
                                                            sb.push('&userid=');
                                                            sb.push(encodeURIComponent(userid));
                                                        }
                                                        sb.push('&rl=');
                                                        sb.push(encodeURIComponent(location.search));
                                                        location.href = sb.join('');
                                                        break;
                                                    case 'delete':
                                                        dropDown.close();
                                                        var dialog = deleteDlgCont.data('dialog');
                                                        dialog.validate = function () {
                                                            var qstring = ['e=tube'];
                                                            qstring.push('id=' + tubeid);
                                                            qstring.push('userid=' + encodeURIComponent(userid));
                                                            $.waitPanel.show();
                                                            var ajaxRequest = $.ajax({
                                                                url: '/delete?' + qstring.join('&'),
                                                                dataType: 'json',
                                                                type: 'post',
                                                                timeout: 60000,
                                                                success: function (json) {
                                                                    if (json && json.error) {
                                                                        messagerControl.show('error', json.error);
                                                                    }
                                                                    else {
                                                                        setTimeout(function () {
                                                                            tubeResult.css('overflow', 'hidden');
                                                                            tubeResult.animate({ height: 0, opacity: 0, marginTop: 0 }, 300, function () {
                                                                                tubeResult.remove();
                                                                            });
                                                                        }, 1);
                                                                    }
                                                                    dialog.close();
                                                                    $.waitPanel.hide();
                                                                },
                                                                error: function (e) {
                                                                    messagerControl.show('error', e);
                                                                    dialog.close();
                                                                    $.waitPanel.hide();
                                                                },
                                                            });
                                                        };
                                                        deleteDlgCont.css('opacity', 0);
                                                        dialog.open();
                                                        deleteDlgCont.animate({ opacity: 1 }, 500);
                                                        break;
                                                }
                                            }
                                        });
                                    });
                                    break;
                            }
                        }
                    });
                    var header = body.find('header');
                    header.find('[settings-panel]').on('click', function () {
                        if (!settingsPanel) {
                            requirejs(['settingsPanel'], function (settingsPanel) {
                                settingsPanel = $('#settings-panel');
                                var settings = new tct.Settings(localStorage);
                                settingsPanel.settingsPanel(settings).bind('settingspanelchange', function (e, data) {
                                    if (data.colorSettings.scheme) {
                                        body.attr('id', 'scheme-' + data.colorSettings.scheme);
                                    }
                                    if (data.colorSettings.theme) {
                                        body.attr('theme', data.colorSettings.theme);
                                    }
                                });
                                settingsPanel.settingsPanel('toogle');
                            });
                        }
                        else {
                            settingsPanel.settingsPanel('toogle');
                        }
                    });
                    header.find('[new-tube]').on('click', function () {
                        location.href = '/editor?id=new&rl=' + encodeURIComponent(location.search);
                    });
                    var huser = $('#user').html();
                    var user = huser && JSON.parse(huser);
                    if (user && user.role === 'admin') {
                        $('#admin').show();
                    }
                    var userInfosPanel = header.find('#user-infos-panel').userInfos({
                        user: user
                    }).bind('userinfoslogin', function () {
                        location.href = '/login?rl=' + encodeURIComponent(location.pathname + location.search);
                    }).bind('userinfoscreated', function () {
                        transArea.animate({ opacity: 1 }, 500);
                    });
                    var getResultData = function (tubeInfos, unitIndex) {
                        var baseId = tubeInfos.base;
                        var base = baseId && hashBases && hashBases[baseId];
                        var pinoutId = tubeInfos.pinout;
                        var pinout = pinoutId && hashPins && hashPins[pinoutId];
                        var descr = '';
                        var type;
                        var typeuid = '';
                        var cfg;
                        var typeId = tubeInfos.type;
                        if (typeId) {
                            type = hashTypes[typeId];
                            if (type) {
                                descr = type.text;
                                typeuid = type.uid;
                                cfg = type.cfg[unitIndex];
                            }
                        }
                        var unit = tubeInfos.units && tubeInfos.units[unitIndex] || [];
                        var docs = tubeInfos.documents || [];
                        var usages = tubeInfos.usages || [];
                        var tubeResultData = {
                            name: tubeInfos.name,
                            type: descr,
                            typeuid: typeuid,
                            base: base ? base.name : '-',
                            pinout: pinout ? pinout.name : '-',
                            vh1: tubeInfos.vh1 ? tubeInfos.vh1 + 'V' : '-',
                            ih1: tubeInfos.ih1 && tubeInfos.ih1 + 'A',
                            h2tt: tubeInfos.vh2 && (tubeInfos.vh1 + 'V ' + tubeInfos.ih1 + 'A  or  ' + tubeInfos.vh2 + 'V ' + tubeInfos.ih1 + 'A'),
                            docsCount: docs.length + usages.length,
                            usagesCount: tubeInfos.usages && tubeInfos.usages.length,
                            unitsCount: tubeInfos.units && tubeInfos.units.length,
                            unitIndex: unitIndex,
                            userId: tubeInfos._userId,
                            userEmail: tubeInfos._userId && users && users[tubeInfos._userId]
                        };
                        var pins = cfg && cfg.pins;
                        if (pins && pins.indexOf('a') >= 0) {
                            if (unit.vamax || unit.pamax) {
                                tubeResultData.vamax = unit.vamax ? unit.vamax + 'V' : '';
                                tubeResultData.pamax = unit.pamax ? unit.pamax + 'W' : '';
                            }
                            else {
                                tubeResultData.vamax = '-';
                                tubeResultData.pamax = '';
                            }
                        }
                        else {
                            tubeResultData.vamax = undefined;
                            tubeResultData.pamax = undefined;
                        }
                        if (pins && pins.indexOf('g2') >= 0) {
                            if (unit.vg2max || unit.pg2max) {
                                tubeResultData.vg2max = unit.vg2max ? unit.vg2max + 'V' : '';
                                tubeResultData.pg2max = unit.pg2max ? unit.pg2max + 'W' : '';
                            }
                            else {
                                tubeResultData.vg2max = '-';
                                tubeResultData.pg2max = '';
                            }
                        }
                        else {
                            tubeResultData.vg2max = undefined;
                            tubeResultData.pg2max = undefined;
                        }
                        return tubeResultData;
                    };
                    var loadTubesResult = function (json, done) {
                        var index = 0;
                        var resultEntities = resultArea.children('.tube-result');
                        if (!json) {
                            resultEntities.hide();
                            return;
                        }
                        tubeDatas = {};
                        require(['tubeResult'], function () {
                            var loadTube = function (t, cb) {
                                if (!json.tubes || t >= json.tubes.length) {
                                    cb();
                                    return;
                                }
                                var tubeInfos = json.tubes[t];
                                var key = tubeInfos._id + (tubeInfos._userId || '');
                                tubeDatas[key] = tubeInfos;
                                var tubeResultData = getResultData(tubeInfos, 0);
                                if (index >= resultEntities.length) {
                                    $('<div></div>').appendTo(resultArea).tubeResult({
                                        datas: tubeResultData,
                                        template: resultTemplate,
                                        id: tubeInfos._id,
                                    });
                                }
                                else {
                                    $(resultEntities[index]).tubeResult('setOptions', {
                                        datas: tubeResultData,
                                        id: tubeInfos._id,
                                    }).show();
                                }
                                index++;
                                loadTube(t + 1, cb);
                            };
                            loadTube(0, function () {
                                for (var i = index; i < resultEntities.length; i++) {
                                    $(resultEntities[i]).hide();
                                }
                                done();
                            });
                        });
                    };
                    var loadNewsResult = function (json, done) {
                        resultArea.children('.news-result').remove();
                        if (!json || !json.news || !json.news.length) {
                            done();
                            return;
                        }
                        var stb = ['<div class="news-result"><div class="newsHeader" lang uid="latestnews">Latest News</div>'];
                        json.news.forEach(function (news) {
                            var date = news.date && new Date(news.date);
                            stb.push('<div class="row"><span class="date">');
                            stb.push(date.toLocaleString());
                            stb.push('</span>');
                            stb.push('<span class="news" lang uid="');
                            stb.push(news.uid);
                            if (news.params) {
                                stb.push('" tpar="');
                                stb.push(encodeURIComponent(JSON.stringify(news.params)));
                            }
                            stb.push('">');
                            stb.push(news.text);
                            stb.push('</span></div>');
                        });
                        stb.push('</div>');
                        $(stb.join('')).appendTo(resultArea).find('a');
                        done();
                    };
                    var loadSearchResults = function (json, scrollto) {
                        loadNewsResult(json, function () {
                            loadTubesResult(json, function () {
                                loadPager(json);
                                if (scrollto) {
                                    $(document).scrollTop(scrollto.position().top);
                                }
                            });
                        });
                    };
                    var loadPager = function (json) {
                        pager.empty();
                        if (!json) {
                            return;
                        }
                        var page = json.page;
                        if (json.pageCount > 1) {
                            var html = [];
                            var buttonClass = 'button no-border bg-transparent fg-theme-over fg-light';
                            var from;
                            if (page > 7) {
                                from = page - 3;
                            }
                            else {
                                from = 1;
                            }
                            var to = Math.min(from + 7, json.pageCount);
                            var from = Math.max(1, to - 7);
                            if (from > 1) {
                                html.push('<span class="');
                                html.push(buttonClass);
                                html.push('" data-page="1"><span class="mif-chevron-thin-left"></span></span>');
                            }
                            for (var i = from; i <= to; i++) {
                                html.push('<span class="');
                                html.push(buttonClass);
                                html.push('" data-page="');
                                html.push(String(i));
                                html.push('">');
                                html.push(String(i));
                                html.push('</span>');
                            }
                            if (to < json.pageCount) {
                                html.push('<span class="');
                                html.push(buttonClass);
                                html.push('" data-page="');
                                html.push(String(json.pageCount));
                                html.push('"><span class="mif-chevron-thin-right"></span></span>');
                            }
                            pager.append(html.join(''));
                            pager.find('[data-page="' + page + '"]').addClass('current fg-theme disabled');
                            pager.find('[data-page]:not(.current)').on('click', function (e) {
                                var page = parseInt($(e.target).closest('[data-page]').attr('data-page'));
                                if (isNaN(page) || page <= 0) {
                                    page = 1;
                                }
                                search(page, false, pager);
                            });
                        }
                    };
                    var searchTimeout;
                    var search = function (page, reset, scrollto) {
                        if (searchTimeout) {
                            clearTimeout(searchTimeout);
                        }
                        searchTimeout = setTimeout(function () {
                            if (searchParams) {
                                var searchText = searchParams.toString();
                                $.waitPanel.show();
                                var query = [];
                                query.push('/search?q=');
                                var encodedSearchUrlParams = encodeURIComponent(searchText);
                                query.push(encodedSearchUrlParams);
                                if (page && !isNaN(page)) {
                                    query.push('&p=');
                                    query.push(String(page));
                                }
                                var lq = location.query();
                                if (lq['max']) {
                                    query.push('&max=');
                                    query.push(lq['max']);
                                }
                                else if (window.mobilecheck()) {
                                    query.push('&max=20');
                                }
                                else {
                                    query.push('&max=50');
                                }
                                if (!hashBases) {
                                    query.push('&lb');
                                }
                                if (!hashTypes) {
                                    query.push('&lt');
                                }
                                if (!hashPins) {
                                    query.push('&lp');
                                }
                                if (!resultTemplate) {
                                    query.push('&tmpl=widgets%2Ftube-result.html');
                                }
                                if (reset) {
                                    query.push('&reset');
                                }
                                var ajaxRequest = $.ajax({
                                    url: query.join(''),
                                    dataType: 'json',
                                    type: 'get',
                                    timeout: 120000,
                                    success: function (json) {
                                        if (!json) {
                                            body.removeClass('params-error');
                                            loadSearchResults();
                                        }
                                        else if (json && json.error) {
                                            body.addClass('params-error');
                                            $errorFlag.attr('tooltip', json.error).removeAttr('uid');
                                            messagerControl.show('error', json.error);
                                        }
                                        else {
                                            body.removeClass('params-error');
                                            var result = json;
                                            if (result.template) {
                                                resultTemplate = result.template;
                                            }
                                            if (result.basesError) {
                                                messagerControl.show('error', result.basesError);
                                            }
                                            else if (result.bases) {
                                                hashBases = {};
                                                result.bases.map(function (b) { return hashBases[b._id] = b; });
                                            }
                                            if (result.pinoutsError) {
                                                messagerControl.show('error', result.pinoutsError);
                                            }
                                            else if (result.pinouts) {
                                                hashPins = {};
                                                result.pinouts.map(function (p) { return hashPins[p._id] = p; });
                                            }
                                            if (result.typesError) {
                                                messagerControl.show('error', result.typesError);
                                            }
                                            else if (result.types) {
                                                hashTypes = {};
                                                result.types.map(function (t) { return hashTypes[t._id] = t; });
                                            }
                                            if (json.users) {
                                                users = json.users;
                                            }
                                            loadSearchResults(json, scrollto);
                                            var state = {
                                                tubes: result.tubes,
                                                page: result.page,
                                                pageCount: result.pageCount,
                                                scrollPos: $(document).scrollTop()
                                            };
                                            var searchState = { q: encodedSearchUrlParams };
                                            if (page) {
                                                searchState['p'] = String(page);
                                            }
                                            else {
                                                searchState['p'] = null;
                                            }
                                            $.pageState.replaceOrAddState(state, searchState);
                                        }
                                        $.waitPanel.hide();
                                    },
                                    error: function (e) {
                                        if (e.statusText !== 'canceled') {
                                            body.addClass('params-error');
                                            $errorFlag.attr('tooltip', e.responseText || e.statusText).removeAttr('uid');
                                            messagerControl.show('error', e);
                                        }
                                        $.waitPanel.hide();
                                    },
                                });
                            }
                        }, 50);
                    };
                    var replaceSearchParams = function (err, ts) {
                        if (err) {
                            var translation = {};
                            translation[err.name] = err.message;
                            translatorControl.translate(translation, function (error, translated) {
                                body.addClass('params-error');
                                $errorFlag.attr('tooltip', translated[err.name]).attr('uid', err.name);
                            });
                        }
                        else {
                            body.removeClass('params-error');
                        }
                        searchParams = ts;
                    };
                    $('[search-btn]').on('click', function () {
                        search(null, true);
                    });
                    simpleSearch.find('input').on('keypress', function (e) {
                        if (e.keyCode === 13) {
                            search(null, true);
                        }
                    });
                    simpleSearch.bind('textinputchange', function (e) {
                        var value = e.value.trim();
                        searchParamsModule.TubeSearch.FromQueryExpression(value, function (err, ts) {
                            replaceSearchParams(err, ts);
                        });
                    });
                    var loadSearchInputFromUrl = function () {
                        var query = location.query();
                        if (query['q']) {
                            searchParamsModule.TubeSearch.FromQueryExpression(query['q'], function (err, ts) {
                                replaceSearchParams(err, ts);
                                if ($advancedArea.is(':visible')) {
                                    showAdvancedSearch();
                                }
                                else {
                                    showSimpleSearch();
                                }
                            });
                        }
                    };
                    var loadFromUrl = function () {
                        var query = location.query();
                        var queryExp = query['q'] || 'news';
                        searchParamsModule.TubeSearch.FromQueryExpression(queryExp, function (err, ts) {
                            if (!ts) {
                                ts = searchParamsModule.TubeSearch.FromSearchExpressions([{
                                        fieldName: '_text',
                                        wholeWord: true,
                                        value: queryExp
                                    }]);
                            }
                            replaceSearchParams(err, ts);
                            showSimpleSearch();
                            var page = parseInt(query['p']);
                            search(page, true);
                        });
                    };
                    var registerForPush = function () {
                        if (!user) {
                            return;
                        }
                        var io = $.io.connect({
                            query: {
                                event: 'documentAttached#' + user.userName
                            }
                        });
                        io.on('documentAttached', function (result) {
                            result.files.forEach(function (file) {
                                if (file.error) {
                                    messagerControl.show('error', file.error);
                                }
                                else {
                                    var fileName = file.filename;
                                    var msg = {
                                        type: 'info',
                                        name: 'msg-fileuploaded',
                                        message: 'File \\0 Uploaded',
                                        autoCloseDelay: 6,
                                        params: { 0: fileName }
                                    };
                                    $.translator.translateMessages([msg], function (translated) {
                                        messagerControl.showMessages(translated);
                                    });
                                }
                            });
                            var tubeData = result.tubeResult.tubeData;
                            var tubeResult = resultArea.find('[data-id="' + tubeData._id + '"]');
                            var unitIndex = parseInt(tubeResult.find('[u-index]').attr('u-index'));
                            var key = tubeData._id + (tubeData._userId || '');
                            tubeDatas[key] = tubeData;
                            var tubeResultData = getResultData(tubeData, unitIndex);
                            tubeResult.tubeResult('setOptions', {
                                datas: tubeResultData,
                                id: tubeData._id,
                            });
                        });
                    };
                    $(window).on('popstate', function (e) {
                        var event = e.originalEvent;
                        if (event && event.state && event.state.j) {
                            var state = event.state.j;
                            loadSearchResults(state);
                            loadSearchInputFromUrl();
                            setTimeout(function () {
                                $(document).scrollTop(state.scrollPos);
                            }, 1);
                        }
                        else {
                            loadFromUrl();
                        }
                    });
                    setTimeout(function () {
                        simpleSearch.textinput('focus');
                    }, 200);
                    loadFromUrl();
                    registerForPush();
                };
            })(jQuery);
            $(function () {
                if (location.hostname === '127.0.0.1') {
                    var browserVersion = window.browserVersion();
                    if (browserVersion.version > 9) {
                        $('head').append('<script type="text/javascript" src="http://127.0.0.1:38080/livereload.js"><\/script>');
                    }
                }
                $.StartScreen();
            });
        });
    })(home = tct.home || (tct.home = {}));
})(tct || (tct = {}));

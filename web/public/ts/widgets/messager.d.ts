declare module modern {
    interface MessagerMessage extends translator.Message {
        autoCloseDelay?: number;
        onclose?: () => void;
    }
    class Messager {
        element: JQuery;
        private currentMsgId;
        _create: () => void;
        private fieldSelector;
        showMessages: (msgs: MessagerMessage[], hidePrevious?: boolean) => any;
        hideMessages: (msgs?: translator.Message[]) => Messager;
        hideMessage: (msg: translator.Message | string) => Messager;
        private removeMessageElement;
        private parseMessages;
        showMessage: (msg: MessagerMessage) => void;
        show: (type: string, name?: any, message?: any, autoCloseDelay?: number, onclose?: () => void) => Messager;
        private showInternal;
        hide: (id?: string) => void;
    }
}
interface JQuery {
    messager(): JQuery;
    messager(optionLiteral: string): JQuery;
    messager(methodName: 'hide'): JQuery;
    messager(optionLiteral: string): modern.Messager;
    messager(optionLiteral: string, msgs: Array<translator.Message>): modern.Messager;
    messager(methodName: 'showMessages', msgs: Array<translator.Message>): modern.Messager;
    messager(methodName: 'hideMessages?', msgs: Array<translator.Message>): modern.Messager;
    messager(optionLiteral: string, msg: translator.Message): modern.Messager;
    messager(methodName: 'showMessage', msg: translator.Message): modern.Messager;
    messager(methodName: 'hideMessage', msg: translator.Message): modern.Messager;
    messager(optionLiteral: string, type: string, msgUniqueId?: string, message?: string, autoCloseDelay?: number): modern.Messager;
    messager(methodName: 'show', type: string, msgUniqueId?: string, message?: string, autoCloseDelay?: number): modern.Messager;
}

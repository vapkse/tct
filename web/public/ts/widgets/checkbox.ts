'use strict';

module modern {
    export interface CheckboxOptions {
        value?: boolean
    }

    export interface CheckboxEvent extends JQueryEventObject {
        value: boolean;
    }

    export class Checkbox {
        private _value: boolean;
        protected input: JQuery;
        protected element: JQuery;
        public options: CheckboxOptions;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _create = function() {
            var self: Checkbox = this;
            var element = this.element;
            self.input = element.find('input').on('change', function(e: JQueryEventObject) {
                self.value(!self._value);
                self.onchange();
            }).on('focus', function(e: JQueryEventObject) {
                $(e.target).siblings('.mif-checkmark').addClass('bo-theme fg-theme').removeClass('bo-lighter bo-lighter-over');
            }).on('blur', function(e: JQueryEventObject) {
                $(e.target).siblings('.mif-checkmark').removeClass('bo-theme fg-theme').addClass('bo-lighter bo-lighter-over');
            });
            self._value = !(!self.input.attr('checked'));
            if (self.options && self.options.value !== undefined) {
                self.value(self.options.value);
            }
            self.oncreated();
        };

        protected oncreated = function() {
            // For overrides
        }

        protected onchange = function() {
            var self: Checkbox = this;
            var e = <CheckboxEvent>jQuery.Event("change");
            e.value = self._value;
            self._trigger('change', e);
        }

        public value = function(value?: boolean) {
            var self: Checkbox = this;
            if (value !== undefined && value != self._value) {
                self._value = value;
                self.format();
            }

            return self._value;
        }

        public format = function() {
            var self: Checkbox = this;
            if (self._value !== undefined && self._value !== null && self._value) {
                self.input.attr('checked', 'checked');
            } else {
                self.input.removeAttr('checked');
            }

            return self;
        };
    }
}

$.widget("modern.checkbox", new modern.Checkbox())

interface JQuery {
    checkbox(): JQuery,
    checkbox(optionLiteral: string): JQuery,
    checkbox(optionLiteral: string): boolean,
    checkbox(methodName: 'format'): JQuery,
    checkbox(methodName: 'value'): boolean,
    checkbox(optionLiteral: string, value: boolean): boolean,
    checkbox(methodName: 'value', value: boolean): boolean,
    checkbox(options: modern.CheckboxOptions): JQuery,
}

interface JQueryStatic {
    modern: JQuery;
}
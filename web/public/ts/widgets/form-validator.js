'use strict';
var modern;
(function (modern) {
    var FormValidator = (function () {
        function FormValidator() {
            this.validationErrors = [];
            this._create = function () {
                var self = this;
                self.container = self.options.container || self.element.closest('[validate]');
                self.messages = self.element.find('messages');
                self.oncreated();
            };
            this.oncreated = function () {
                var self = this;
                setTimeout(function () {
                    var herrors = $('#errors').html();
                    var errors = herrors && JSON.parse(herrors);
                    if (errors && errors.length) {
                        self.showValidationErrors(errors);
                        errors.remove();
                    }
                }, 100);
            };
            this.getValidationMessage = function (uid) {
                var self = this;
                return self.messages.find('[uid="' + uid + '"]').html();
            };
            this.showValidationErrors = function (verrors) {
                var self = this;
                if (verrors && verrors.length) {
                    for (var ve = 0; ve < verrors.length; ve++) {
                        var verror = verrors[ve];
                        var fieldName = verror.fieldName;
                        if (fieldName) {
                            var fieldsCss = verror.fieldCss || 'error';
                            var field = self.container.find(self.fieldSelector(fieldName, verror.dataIndex)).addClass(fieldsCss);
                            if (verror.tooltip !== false) {
                                var text = verror.message;
                                var ft = field.find('.input-state-error');
                                if (verror.params) {
                                    text = $.translator.replaceParameter(text, verror.params);
                                    ft.attr('tpar', encodeURIComponent(JSON.stringify(verror.params)));
                                }
                                ft.attr('tooltip', text).attr('uid', verror.name).show();
                            }
                        }
                    }
                    self.validationErrors = verrors;
                }
                return self;
            };
            this.showValidationsError = function (verror) {
                var self = this;
                return self.showValidationErrors([verror]);
            };
            this.hideValidationError = function (filedName) {
                var self = this;
                var verrors = self.validationErrors;
                if (verrors && verrors.length) {
                    for (var ve = 0; ve < verrors.length; ve++) {
                        if (verrors[ve].fieldName === filedName) {
                            self.hideValidationMessage(verrors[ve]);
                            verrors.slice(ve, 1);
                        }
                    }
                }
                return self;
            };
            this.hideValidationMessage = function (verror) {
                var self = this;
                var fieldName = verror.fieldName;
                if (fieldName) {
                    self.removeClassNames(fieldName, verror.fieldCss, verror.dataIndex);
                }
                return self;
            };
            this.removeClassNames = function (filedName, className, dataIndex) {
                var self = this;
                className = className || 'error info warning';
                var field = self.container.find(self.fieldSelector(filedName, dataIndex)).removeClass(className);
                if (field.length) {
                    field.filter('.input-state-error').removeAttr('tooltip').removeAttr('uid').hide();
                }
            };
            this.hideValidationErrors = function () {
                var self = this;
                var verrors = self.validationErrors;
                if (verrors && verrors.length) {
                    for (var ve = 0; ve < verrors.length; ve++) {
                        self.hideValidationMessage(verrors[ve]);
                    }
                    self.validationErrors = null;
                }
                return self;
            };
            this.fieldSelector = function (field, dataIndex) {
                var re = new RegExp('^[a-z0-9]', 'i');
                if (re.test(field)) {
                    if (dataIndex !== undefined) {
                        return '[data-index="' + dataIndex + '"] [for="' + field + '"],[data-index="' + dataIndex + '"] [data-field="' + field + '"],[data-index="' + dataIndex + '"] #' + field + ',[data-index="' + dataIndex + '"] [data-field="' + field + '"] .input-state-error,[data-index="' + dataIndex + '"] #' + field + ' .input-state-error';
                    }
                    return '[for="' + field + '"],[data-field="' + field + '"],#' + field + ',[data-field="' + field + '"] .input-state-error,#' + field + ' .input-state-error';
                }
                else {
                    return field;
                }
            };
        }
        return FormValidator;
    })();
    modern.FormValidator = FormValidator;
})(modern || (modern = {}));
$.widget("modern.formvalidator", new modern.FormValidator());

'use strict';

module modern {
    export interface TranslatorControlEvent extends JQueryEventObject {
        value: string
    }

    export class TranslatorControl extends Chosen2 {
        private staticTranslations: translator.RequestResult;
        private container: JQuery;
        private messagerControl: modern.Messager;

        protected oncreated = function() {
            var self = this as TranslatorControl;
            var overTranslatable: JQuery;
            var dialogContainer: JQuery;
            var dialogInput: JQuery;
            var huser = $('#user').html();
            var proposal = {} as TranslationProposal;
            
            self.element.attr('id', 'translator');
            self.container = self.element.closest('[translate]').on('mousemove', function(e) {
                if (overTranslatable) {
                    overTranslatable.css('cursor', 'default');
                    overTranslatable = undefined;
                }

                // Mousemove for best translation feature
                if (huser && e.altKey && e.ctrlKey) {
                    var translatable = $(e.target).closest('[lang],lang,[tooltip]');
                    if (translatable.length) {
                        overTranslatable = translatable.css('cursor', 'pointer');
                    }
                }
            }).on('mousedown', function(e) {
                if (overTranslatable) {
                    // Propose new translation
                    var pageId = overTranslatable.closest('[translate]').attr('translate');
                    var id = overTranslatable.attr('uid');
                    if (!id) {
                        id = overTranslatable.attr('id');
                        if (id) {
                            id = pageId + '-' + id;
                        }
                    }
                    if (id) {
                        proposal.uid = id;
                        proposal.original = overTranslatable.is('[tooltip]') ? overTranslatable.attr('tooltip') : overTranslatable.text();
                        if (proposal.original) {
                            proposal.locale = overTranslatable.parents('[locale]').last().attr('locale');
                            var showDialog = function() {
                                var dialog = dialogContainer.data('dialog');
                                dialog.open();
                            }
                            if (dialogContainer) {
                                dialogInput.val(proposal.original);
                                showDialog();
                            } else {
                                $.get('/resource?r=widgets%2Fbest-translation.html').done(function(html) {
                                    dialogContainer = $(html).appendTo('body');
                                    dialogInput = dialogContainer.find('.input-control input').val(proposal.original)
                                    var validate = dialogContainer.find('[validate]');
                                    dialogInput.on('keypress', function(e) {
                                        if (e.keyCode === 13) {
                                            validate.click();
                                        }
                                    });
                                    validate.on('click', function(e) {
                                        var newText = dialogInput.val()
                                        if (newText && newText !== proposal.original) {
                                            proposal.proposal = newText;
                                            var options = {
                                                url: '/translator?r=propose',
                                                type: 'POST',
                                                contentType: 'application/json',
                                                timeout: 30000,
                                                data: JSON.stringify(proposal),
                                                success: function(result: translator.RequestResult) {
                                                    dialogContainer.data('dialog').close();
                                                    var message = {
                                                        autoCloseDelay: 15,
                                                        name: 'proposalsent',
                                                        message: 'Your proposal was sent to the administrator.',
                                                        type: 'info'
                                                    } as MessagerMessage
                                                    $.translator.translateMessages([message], function(translated) {
                                                        if (self.getMessagerControl()) {
                                                            self.messagerControl.showMessages(translated);
                                                        }
                                                    })
                                                },
                                                error: function(jqXHR: JQueryXHR, textStatus: string, errorThrown: string) {
                                                    console.error(errorThrown);
                                                    dialogContainer.data('dialog').close();
                                                    if (self.getMessagerControl()) {
                                                        self.messagerControl.show('error', (jqXHR && (jqXHR.responseText || jqXHR.statusText)) || errorThrown);
                                                    }
                                                }
                                            }

                                            $.ajax(options);
                                        } else {
                                            dialogContainer.data('dialog').close();
                                        }
                                    })
                                    dialogContainer.find('[cancel]').on('click', function(e) {
                                        dialogContainer.data('dialog').close();
                                    })
                                    setTimeout(function() {
                                        showDialog();
                                    }, 1);
                                })
                            }
                        }
                    }
                }
            });

            if (self.container.length === 0) {
                return;
            }

            self.container.each(function(index: number, element: Element) {
                var $element = $(element);
                var pageId = $element.attr('translate')
                var $staticTranslations = $element.find('translations lang');
                self.staticTranslations = {
                    locale: $element.attr('locale'),
                    translations: {}
                };
                if ($staticTranslations.length > 0) {
                    $staticTranslations.each(function(index: number, element: Element) {
                        var $lang = $(element);
                        var id = $lang.attr('id');
                        var text = $lang.text();
                        self.staticTranslations.translations[pageId + '-' + id] = text;
                    })
                }
            })

            self.setOptions({
                selectedValue: self.container.attr('locale'),
                valueField: 'id',
            })
        }

        private getMessagerControl() {
            var self = this as TranslatorControl;
            if (!self.messagerControl) {
                self.messagerControl = $('#notify').messager().data('modern-messager');
            }
            return self.messagerControl;
        }

        protected onchange = function() {
            var self = this as TranslatorControl;
            self.changeLocale(self.selectedValue());
            var e = <TranslatorControlEvent>jQuery.Event("change");
            e.value = self.selectedValue();
            self._trigger('change', e);
        }

        public changeLocale = function(loc: string) {
            var self = this as TranslatorControl;
            var translatables: Array<JQuery> = [];

            // Change translator content
            var translationsParam: translator.TranslationsParams = {
                to: loc,
                translations: {},
                nocache: false
            }

            var conts: JQuery = $('[translate]');
            for (var c = 0; c < conts.length; c++) {
                var container = $(conts[c]);
                var pageId = container.attr('translate');
                var translatable = container.find('[lang]:not([translate="' + pageId + '"] [translate] [lang]),lang:not([translate="' + pageId + '"] [translate] lang),[tooltip]:not([translate="' + pageId + '"] [translate] [tooltip])');
                translatables.push(translatable);
                translatable.each(function(index: number, element: Element) {
                    var $element = $(element);
                    var id = $element.attr('uid');
                    if (!id) {
                        id = $element.attr('id');
                        if (id) {
                            id = pageId + '-' + id;
                        }
                    }
                    if (id) {
                        // No need, originals are stored server side at the first rendering of the page
                        translationsParam.translations[id] = '';
                    }
                })
            }

            var loadTexts = function(containers: JQuery, result: translator.RequestResult) {
                if (!result.translations) {
                    return;
                }

                for (var c = 0; c < containers.length; c++) {
                    var container = $(containers[c]);
                    var pageId = container.attr('translate');
                    var translatable = translatables[c];
                    translatable.each(function(index: number, element: Element) {
                        var $element = $(element);
                        var id = $element.attr('uid');
                        if (!id) {
                            id = $element.attr('id');
                            if (id) {
                                id = pageId + '-' + id;
                            }
                        }
                        if (id && result.translations[id]) {
                            var text = result.translations[id].htmlDecode();
                            var tpar = $element.attr('tpar');
                            if (tpar) {
                                text = $.translator.replaceParameter(text, JSON.parse(decodeURIComponent(tpar)));
                            }
                            if ($element.is('[tooltip]')) {
                                $element.attr('tooltip', text)
                            } else {
                                $element.html(text);
                            }
                        }
                    })
                    container.attr('locale', result.locale);
                }
            }

            var loadLanguages = function(result: translator.RequestResult) {
                if (result.error) {
                    self.element.hide();
                }

                self.setOptions({
                    selectedValue: result.languages.locale,
                    valueField: 'id',
                    items: result.languages.languages
                })
            }

            var options = {
                url: '/translator?r=change',
                type: 'POST',
                contentType: 'application/json',
                timeout: 60000,
                data: JSON.stringify(translationsParam),
                success: function(result: translator.RequestResult) {
                    if (result.error) {
                        console.error(result.error.message);
                        return;
                    }

                    loadTexts(conts, result);
                    loadLanguages(result);

                    // Update static translations (and more...)
                    self.staticTranslations = result;
                },
                error: function(jqXHR: JQueryXHR, textStatus: string, errorThrown: string) {
                    console.error(errorThrown);
                    self.selectedValue(container.attr('locale') || 'en');
                }
            }
            $.ajax(options);
        }

        public translate = function(translations: translator.Translation, cb: (err: Error, translated: translator.Translation) => void) {
            var self = this as TranslatorControl;

            // Change translator content
            var translationsParam: translator.TranslationsParams = {
                to: self.container.attr('locale'),
                translations: translations,
                nocache: false
            }

            $.ajax({
                url: '/translator?r=change',
                type: 'POST',
                contentType: 'application/json',
                timeout: 60000,
                data: JSON.stringify(translationsParam),
                success: function(result: translator.RequestResult) {
                    if (result.error) {
                        cb(result.error, translations);
                        return;
                    }

                    cb(null, result.translations);
                },
                error: function(jqXHR: JQueryXHR, textStatus: string, errorThrown: string) {
                    cb({
                        name: '',
                        message: errorThrown
                    }, translations);
                }
            });
        }

        public translations() {
            return this.staticTranslations;
        }

        // Return the translation with the specified id stored in the html page inside the translations panel
        public translation(pageid: string, id: string): string {
            return (this.staticTranslations && this.staticTranslations.translations && this.staticTranslations.translations[pageid + '-' + id]) || '???';
        }
    }
}

$.widget("modern.translatorControl", $.modern.chosen2, new modern.TranslatorControl());

interface JQuery {
    translatorControl(): JQuery;
    translatorControl(optionLiteral: string): string,
    translatorControl(methodName: 'selectedValue'): string,
    translatorControl(optionLiteral: string, value: string): string,
    translatorControl(methodName: 'selectedValue', value: string): string,
}


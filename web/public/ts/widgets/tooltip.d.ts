declare module modern {
    interface TooltipOptions {
        parent?: JQuery;
        within?: JQuery;
        backdrop?: boolean;
        backdropClass?: string;
        backdropOpacity?: number;
        backdropOverClose?: boolean;
        template?: string;
        templateUrl?: string;
        templateControl?: string;
        templateControlOptions?: any;
        toolTipClass?: string;
        position?: JQueryUI.JQueryPositionOptions;
    }
    class Tooltip {
        private $;
        options: TooltipOptions;
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _create: () => void;
        _dispose: () => Tooltip;
        hide: () => Tooltip;
        show: () => Tooltip;
    }
}
interface JQuery {
    tooltip2(): JQuery;
    tooltip2(optionLiteral: string): JQuery;
    tooltip2(methodName: 'hide'): JQuery;
    tooltip2(methodName: 'show'): JQuery;
    tooltip2(options: modern.TooltipOptions): JQuery;
}

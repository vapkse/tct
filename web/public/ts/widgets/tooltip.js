var modern;
(function (modern) {
    var Tooltip = (function () {
        function Tooltip() {
            this._create = function () {
                var self = this;
                self.$ = {
                    body: $('body')
                };
            };
            this._dispose = function () {
                var self = this;
                return self.hide();
            };
            this.hide = function () {
                var self = this;
                if (!self.$.modalBody) {
                    return;
                }
                if (self.$.modalBackdrop) {
                    self.$.modalBackdrop.animate({ opacity: 0 }, 200, function () {
                        self.$.modalBackdrop.remove();
                        delete self.$.modalBackdrop;
                    });
                }
                self.$.modalBody.animate({ opacity: 0 }, 200, function () {
                    self.$.modalBody.remove();
                    delete self.$.modalBody;
                });
                return self;
            };
            this.show = function () {
                var self = this;
                var options = self.options;
                var backdropHideTimer;
                if (options.backdrop) {
                    self.$.modalBackdrop = $('<div class="tooltip-backdrop ' + (options.backdropClass || '') + '"></div>').appendTo($('body')).css('opacity', 0);
                }
                var html = [];
                html.push('<div class="tooltip-body bg-theme-selected fg-theme-selected"><span class="');
                if (!options.templateControl) {
                    html.push('selected');
                }
                if (options.toolTipClass) {
                    html.push(' ' + options.toolTipClass);
                }
                if (options.template) {
                    html.push(' tooltip-text');
                }
                html.push('">');
                if (options.template) {
                    html.push(options.template);
                }
                html.push('</span></div>');
                self.$.modalBody = $(html.join('')).css('opacity', 0).appendTo(options.parent || self.$.body);
                var showTooltip = function () {
                    if (options.position) {
                        if (options.position.within === undefined) {
                            options.position.within = self.$.body;
                        }
                        if (!options.position.of) {
                            options.position.of = parent;
                        }
                        if (!options.position.my) {
                            options.position.my = "left bottom";
                        }
                        if (!options.position.at) {
                            options.position.at = "left top";
                        }
                        if (!options.position.collision) {
                            options.position.collision = "flip";
                        }
                        self.$.modalBody.position(self.options.position).animate({ opacity: 1 }, 500);
                    }
                    else {
                        self.$.modalBody.animate({ opacity: 1 }, 200);
                    }
                    if (self.$.modalBackdrop) {
                        var backdropOpacity = self.options.backdropOpacity;
                        if (!backdropOpacity || isNaN(backdropOpacity)) {
                            backdropOpacity = 0;
                        }
                        self.$.modalBackdrop.animate({ opacity: backdropOpacity }, 1000);
                        if (options.backdrop) {
                            if (options.backdropOverClose) {
                                self.$.modalBackdrop.hover(function (e) {
                                    if (backdropHideTimer) {
                                        clearTimeout(backdropHideTimer);
                                        backdropHideTimer = null;
                                    }
                                    backdropHideTimer = setTimeout(function () {
                                        self.hide();
                                    }, 50);
                                }, function (e) {
                                    if (backdropHideTimer) {
                                        clearTimeout(backdropHideTimer);
                                        backdropHideTimer = null;
                                    }
                                });
                            }
                        }
                    }
                };
                if (options.templateUrl) {
                    $.get(options.templateUrl, function (template) {
                        $(template).appendTo(self.$.modalBody.children('span'));
                        showTooltip();
                    });
                }
                else if (options.templateControl) {
                    $('<div></div').appendTo(self.$.modalBody.children('span'))[String(options.templateControl)](options.templateControlOptions).bind(options.templateControl.toLowerCase() + 'created', function (e) {
                        showTooltip();
                    });
                }
                else {
                    showTooltip();
                }
                return self;
            };
        }
        return Tooltip;
    })();
    modern.Tooltip = Tooltip;
})(modern || (modern = {}));
$.widget("modern.tooltip2", new modern.Tooltip());

'use strict';
var modern;
(function (modern) {
    var TooltipManagerOptions = (function () {
        function TooltipManagerOptions() {
        }
        return TooltipManagerOptions;
    })();
    modern.TooltipManagerOptions = TooltipManagerOptions;
    var TooltipManager = (function () {
        function TooltipManager() {
            this.options = new TooltipManagerOptions();
            this._create = function () {
                if (window.mobilecheck()) {
                    return;
                }
                var self = this;
                var currentHoverred;
                var currentHoverredTooltip;
                var selectTimerHandle;
                var showTimerHandle;
                var hideTimerHandle;
                var cancelTooltip = function () {
                    currentHoverredTooltip = null;
                    if (showTimerHandle) {
                        clearTimeout(showTimerHandle);
                        showTimerHandle = 0;
                    }
                    if (hideTimerHandle) {
                        clearTimeout(hideTimerHandle);
                        hideTimerHandle = 0;
                    }
                    if (self._currentTooltip) {
                        self.onhidetooltip();
                    }
                };
                var hideTooltipDelayed = function () {
                    if (hideTimerHandle) {
                        clearTimeout(hideTimerHandle);
                        hideTimerHandle = 0;
                    }
                    if (self._currentTooltip) {
                        hideTimerHandle = setTimeout(function () {
                            hideTimerHandle = 0;
                            self.onhidetooltip();
                        }, 300);
                    }
                };
                var onmouseup = function (e) {
                    var tooltipElement = $(e.target).closest('.tooltip-body,.tooltip-backdrop,[tooltip],[tooltip-control]');
                    if (tooltipElement.is('.tooltip-body')) {
                        return;
                    }
                    if (tooltipElement.is('.tooltip-backdrop,[tooltip]')) {
                        cancelTooltip();
                    }
                };
                var onmousemove = function (e) {
                    self._currentEvent = e;
                    if (currentHoverred && $(e.target).is(currentHoverred)) {
                        return;
                    }
                    currentHoverred = $(e.target);
                    if (selectTimerHandle) {
                        clearTimeout(selectTimerHandle);
                    }
                    selectTimerHandle = setTimeout(function () {
                        selectTimerHandle = 0;
                        var tooltipElement = $(e.target).closest('.tooltip-body,[tooltip],[tooltip-control]');
                        if (tooltipElement.is('.tooltip-body')) {
                            return;
                        }
                        if (tooltipElement.length === 0) {
                            tooltipElement = null;
                        }
                        if ((!currentHoverredTooltip && !tooltipElement) || (currentHoverredTooltip && tooltipElement && tooltipElement.is(currentHoverredTooltip))) {
                            return;
                        }
                        hideTooltipDelayed();
                        if (showTimerHandle) {
                            clearTimeout(showTimerHandle);
                            showTimerHandle = 0;
                        }
                        currentHoverredTooltip = tooltipElement;
                        if (currentHoverredTooltip) {
                            showTimerHandle = setTimeout(function () {
                                showTimerHandle = 0;
                                self._currentTooltip = currentHoverredTooltip;
                                if (currentHoverredTooltip) {
                                    self.onshowtooltip();
                                }
                            }, 500);
                        }
                    }, 50);
                };
                var eventRegistered;
                var registerEvent = function () {
                    currentHoverred = self.element.on('mousemove', onmousemove).on('mouseup', onmouseup);
                    currentHoverredTooltip = null;
                    eventRegistered = true;
                };
                $(document).hover(function (e) {
                    if (!eventRegistered) {
                        registerEvent();
                    }
                }, function (e) {
                    if (eventRegistered) {
                        self.element.off('mousemove', onmousemove).off('mouseup', onmouseup);
                        cancelTooltip();
                        eventRegistered = false;
                    }
                });
                registerEvent();
            };
            this.onshowtooltip = function () {
                var self = this;
                var targetEvent = $(self._currentEvent.target);
                var targetOffset = targetEvent.offset();
                var offset = self._currentTooltip.offset();
                var body = $('body');
                var x = self._currentEvent.offsetX + targetOffset.left - offset.left + 20;
                var y = self._currentEvent.offsetY + targetOffset.top - offset.top - 20;
                var options = {
                    parent: self._currentTooltip.parent(),
                    position: {
                        my: 'left bottom',
                        at: 'left+' + x + ' top+' + y,
                        of: self._currentTooltip,
                        collision: 'flipfit'
                    }
                };
                if (self.options.withinElement !== undefined) {
                    options.position.within = self.options.withinElement;
                }
                else {
                    options.position.within = self.element;
                }
                if (self._currentTooltip.is('[tooltip-control]')) {
                    options.templateControl = self._currentTooltip.attr('tooltip-control');
                    options.toolTipClass = 'bg-transparent';
                    options.backdrop = self._currentTooltip.attr('tooltip-backdrop') !== undefined;
                    options.backdropOpacity = parseFloat(self._currentTooltip.attr('backdrop-opacity'));
                    options.backdropClass = 'bg-darker';
                    options.backdropOverClose = true;
                }
                else {
                    options.template = self._currentTooltip.attr('tooltip');
                }
                self._currentTooltip.tooltip2(options);
                self._currentTooltip.tooltip2('show');
                var e = jQuery.Event('show');
                e.tooltip = self._currentTooltip;
                self._trigger('show', e);
            };
            this.onhidetooltip = function () {
                var self = this;
                if (!self._currentTooltip || !self._currentTooltip.tooltip2('instance')) {
                    return;
                }
                self._currentTooltip.tooltip2('hide');
                var e = jQuery.Event('hide');
                e.tooltip = self._currentTooltip;
                self._currentTooltip = null;
                self._trigger('hide', e);
            };
        }
        return TooltipManager;
    })();
    modern.TooltipManager = TooltipManager;
})(modern || (modern = {}));
$.widget("modern.tooltipmanager", new modern.TooltipManager());

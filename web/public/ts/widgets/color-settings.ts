'use strict';

module modern {
    export class ColorSettingsOptions {
        theme: string;  // Class name
        scheme: string;   // Class name
    }

    export interface ColorSettingsEvent extends JQueryEventObject {
        theme?: string;  // Class name
        scheme?: string;   // Class name    
    }

    export class ColorSettings {
        public options = new ColorSettingsOptions();
        private _themeButtons: JQuery;
        private _schemeRadioButtons: JQuery;
        protected _create = function() {
            var self = this;
            var options = <ColorSettingsOptions>this.options;

            var animate = function() {
                var body = $('body').addClass('color-changing').on('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd',
                    function(e) {
                        body.removeClass('color-changing');
                    });
            } 
            
            // Resource need to be renderized, so the call must be like this
            $.get('/resource?r=widgets%2Fcolor-settings.html').done(function(html) {
                self.element.append(html).attr('id', 'color-settings');

                self._schemeRadioButtons = self.element.find('[data-scheme] input').on('change', function(e: JQueryEventObject) {
                    var event: ColorSettingsEvent = e;
                    options.scheme = $(e.target).closest('[data-scheme]').attr('data-scheme');
                    event.theme = options.theme;
                    event.scheme = options.scheme;
                    self._trigger('change', event);
                });

                var schemeButtons = self.element.find('[data-scheme]').hover(function(e: JQueryEventObject) {
                    var event: ColorSettingsEvent = e;
                    animate();
                    event.theme = options.theme;
                    event.scheme = $(e.target).closest('[data-scheme]').attr('data-scheme');
                    self._trigger('enter', event);
                }, function(e: JQueryEventObject) {
                    animate();
                    self._trigger('leave', e);
                });

                self._themeButtons = self.element.find('[data-theme]').on('click', function(e: JQueryEventObject) {
                    var event: ColorSettingsEvent = e;
                    options.theme = $(e.target).closest('[data-theme]').attr('data-theme');
                    event.theme = options.theme;
                    event.scheme = options.scheme;
                    self.refresh();
                    self._trigger('change', event);
                }).hover(function(e: JQueryEventObject) {
                    var event: ColorSettingsEvent = e;
                    animate();
                    event.scheme = options.scheme;
                    event.theme = $(e.target).closest('[data-theme]').attr('data-theme');
                    self._trigger('enter', event);
                }, function(e: JQueryEventObject) {
                    animate();
                    self._trigger('leave', e);
                });

                self.refresh();
            });
        }

        public refresh = function() {
            var options = <ColorSettingsOptions>this.options;

            if (options.theme) {
                this._themeButtons.removeClass('selected');
                this._themeButtons.filter('[data-theme="' + options.theme + '"]').addClass('selected');
            }
            if (options.scheme) {
                this._schemeRadioButtons.filter('[data-scheme] [value="' + options.scheme + '"]').attr('checked', 'checked');
            }
        }
    }
}

$.widget("modern.colorSettings", new modern.ColorSettings());

interface JQuery {
    colorSettings(): modern.ColorSettings;
    colorSettings(method: string): modern.ColorSettings;
    colorSettings(options: modern.ColorSettingsOptions): JQuery;
}
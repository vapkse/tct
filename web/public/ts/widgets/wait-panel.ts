'option: strict';

class WaitPanel {
    public show() {
        var body = $('body').addClass('loading');
        var panel = body.find('#waiting-panel').show();
        if (panel.length === 0) {
            var cont = $('<div id="waiting-panel" style="display: none;">').appendTo(body);
            $.get('/resource?r=widgets%2F/wait-panel.html').done(function(html) {    
                cont.append(html);
                if ($('body').is('.loading')) {
                    cont.show();                 
                } else {
                    cont.hide();                                     
                }
            })
        }
    }

    public hide() {
        var panel = $('body').removeClass('loading').find('#waiting-panel').hide();
    }
}

(function($: JQueryStatic) {
    $.waitPanel = new WaitPanel();
})(jQuery);

interface WaitPanelStatic {
    show(): void,
    hide(): void,
}

interface JQueryStatic {
	waitPanel: WaitPanelStatic;
}

'use strict';
var modern;
(function (modern) {
    var TextInput = (function () {
        function TextInput() {
            this.options = {};
            this._create = function () {
                var self = this;
                var element = self.element;
                var options = self.options;
                self.input = element.find("input");
                if (self.input.length === 0) {
                    self.input = element.find("textarea");
                    if (self.input.length > 0) {
                        var fitTextarea = function (textarea, width) {
                            textarea.css({
                                "resize": 'none',
                                "overflow-y": 'hidden'
                            });
                            if (width) {
                                textarea[0].style.width = width + 'px';
                            }
                            textarea[0].style.height = '0';
                            var adjust = textarea[0].scrollHeight;
                            if (width) {
                                textarea[0].style.width = '';
                            }
                            textarea[0].style.height = adjust + 'px';
                        };
                        $(window).on('beforeprint', function (e) {
                            fitTextarea(self.input, 600);
                        }).on('afterprint', function (e) {
                            fitTextarea(self.input);
                        });
                    }
                }
                if (options.value) {
                    self.value(options.value);
                }
                else {
                    element.addClass('empty');
                }
                self.input.on("blur", function (e) {
                    self.onblur();
                }).on("focus", function () {
                    self.onfocus();
                }).on('change', function () {
                    self.onchange();
                });
                var foucusOnInput = function () {
                    setTimeout(function () {
                        self.input.focus();
                    }, 0);
                };
                self.element.find('.unitholder').on('mouseup', function () { foucusOnInput(); });
                self.placeholder = self.element.find('.placeholder').on('mouseup', function () { foucusOnInput(); });
                self.element.find('.informer').on('mouseup', function () { foucusOnInput(); });
                element.data('textinput', this);
                self.oncreated();
            };
            this.oncreated = function () {
            };
            this.onblur = function () {
                var self = this;
                self.ensureEmptyClass();
                self.input.removeClass('bo-theme focus');
                var e = jQuery.Event('blur');
                self._trigger('blur', e);
            };
            this.onfocus = function () {
                var self = this;
                if (!self.input.is('[readonly]')) {
                    self.input.addClass('bo-theme focus');
                }
                var e = jQuery.Event('focus');
                self._trigger('focus', e);
            };
            this.onchange = function () {
                var self = this;
                var e = jQuery.Event("change");
                e.value = self.value();
                self.ensureEmptyClass();
                self._trigger('change', e);
            };
            this.isEmpty = function () {
                var self = this;
                var val = self.value();
                return val === undefined || val === null || val === '';
            };
            this.ensureEmptyClass = function () {
                var self = this;
                var val = self.value();
                if (self.isEmpty()) {
                    self.element.addClass('empty');
                    if (self.placeholder) {
                        self.placeholder.css({ display: "block" });
                    }
                }
                else {
                    self.element.removeClass('empty');
                    if (self.placeholder) {
                        self.placeholder.css({ display: "none" });
                    }
                }
            };
            this.focus = function () {
                var self = this;
                self.input.focus();
                return self.element;
            };
            this.value = function (value) {
                var self = this;
                var val = self.input.val();
                if (value !== undefined && val !== value) {
                    if (value === null) {
                        self.input.empty();
                    }
                    else {
                        self.input.val(value);
                    }
                    self.ensureEmptyClass();
                    return value;
                }
                return val;
            };
        }
        return TextInput;
    })();
    modern.TextInput = TextInput;
})(modern || (modern = {}));
$.widget("modern.textinput", new modern.TextInput());

'use strict';

module modern {
    export class TooltipManagerOptions {
        withinElement: JQuery
    }

    export interface TooltipManagerEvent extends JQueryEventObject {
        tooltip: JQuery;
    }

    export class TooltipManager {
        public options = new TooltipManagerOptions();
        protected element: JQuery;
        private _currentTooltip: JQuery;
        private _currentEvent: JQueryMouseEventObject;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _create = function() {
            if (window.mobilecheck()) {
                return;
            }

            var self: TooltipManager = this;
            var currentHoverred: JQuery;
            var currentHoverredTooltip: JQuery;
            var selectTimerHandle: number;
            var showTimerHandle: number;
            var hideTimerHandle: number;

            var cancelTooltip = function() {
                currentHoverredTooltip = null;

                if (showTimerHandle) {
                    clearTimeout(showTimerHandle);
                    showTimerHandle = 0;
                }

                if (hideTimerHandle) {
                    clearTimeout(hideTimerHandle);
                    hideTimerHandle = 0;
                }

                if (self._currentTooltip) {
                    self.onhidetooltip();
                }
            }

            var hideTooltipDelayed = function() {
                if (hideTimerHandle) {
                    clearTimeout(hideTimerHandle);
                    hideTimerHandle = 0;
                }

                if (self._currentTooltip) {
                    hideTimerHandle = setTimeout(function() {                        
                        hideTimerHandle = 0;
                        self.onhidetooltip();
                    }, 300);
                }
            }

            var onmouseup = function(e: JQueryEventObject) {
                var tooltipElement = $(e.target).closest('.tooltip-body,.tooltip-backdrop,[tooltip],[tooltip-control]');
                if (tooltipElement.is('.tooltip-body')) {
                    // On the tooltip, nothing to do
                    return;
                }

                if (tooltipElement.is('.tooltip-backdrop,[tooltip]')) {
                    cancelTooltip();
                }
            }

            var onmousemove = function(e: JQueryEventObject) {
                self._currentEvent = e;

                if (currentHoverred && $(e.target).is(currentHoverred)) {
                    // Same element
                    return;
                }
                
                // Current hovered changed
                currentHoverred = $(e.target);

                // Cancel current select process
                if (selectTimerHandle) {
                    clearTimeout(selectTimerHandle);
                }
                
                // New select process                
                selectTimerHandle = setTimeout(function() {
                    selectTimerHandle = 0;
                    
                    // Check if a tooltip is avalaible on one of the parents
                    var tooltipElement = $(e.target).closest('.tooltip-body,[tooltip],[tooltip-control]');
                    if (tooltipElement.is('.tooltip-body')) {
                        // On the tooltip, nothing to do
                        return;
                    }

                    if (tooltipElement.length === 0) {
                        // No tooltip available
                        tooltipElement = null;
                    }

                    if ((!currentHoverredTooltip && !tooltipElement) || (currentHoverredTooltip && tooltipElement && tooltipElement.is(currentHoverredTooltip))) {
                        // Same tooltip available
                        return;
                    }
                    
                    // Current tooltip is different
                    // hide previous tooltip
                    hideTooltipDelayed();

                    // Cancel current show process
                    if (showTimerHandle) {
                        clearTimeout(showTimerHandle);
                        showTimerHandle = 0;
                    }

                    currentHoverredTooltip = tooltipElement;

                    if (currentHoverredTooltip) {
                        showTimerHandle = setTimeout(function() {
                            showTimerHandle = 0;
                            self._currentTooltip = currentHoverredTooltip;
                            if (currentHoverredTooltip) {
                                self.onshowtooltip();
                            }
                        }, 500);
                    }
                }, 50);

            }

            var eventRegistered: boolean
            var registerEvent = function() {
                currentHoverred = self.element.on('mousemove', onmousemove).on('mouseup', onmouseup);
                currentHoverredTooltip = null;
                eventRegistered = true;
            }

            // Register container events             
            $(document).hover(function(e: JQueryEventObject) {
                if (!eventRegistered) {
                    registerEvent();
                }
            }, function(e: JQueryEventObject) {
                if (eventRegistered) {
                    self.element.off('mousemove', onmousemove).off('mouseup', onmouseup);
                    cancelTooltip();
                    eventRegistered = false;
                }
            });

            registerEvent();
        };

        protected onshowtooltip = function() {
            var self: TooltipManager = this;

            var targetEvent = $(self._currentEvent.target);
            var targetOffset = targetEvent.offset();
            var offset = self._currentTooltip.offset();
            var body = $('body');
            var x = self._currentEvent.offsetX + targetOffset.left - offset.left + 20;
            var y = self._currentEvent.offsetY + targetOffset.top - offset.top - 20;

            var options = <TooltipOptions>{
                parent: self._currentTooltip.parent(),
                position: {
                    my: 'left bottom',
                    at: 'left+' + x + ' top+' + y,
                    of: self._currentTooltip,
                    collision: 'flipfit'
                }
            }

            if (self.options.withinElement !== undefined) {
                options.position.within = self.options.withinElement;
            } else {
                options.position.within = self.element;
            }

            if (self._currentTooltip.is('[tooltip-control]')) {
                options.templateControl = self._currentTooltip.attr('tooltip-control');
                options.toolTipClass = 'bg-transparent';
                options.backdrop = self._currentTooltip.attr('tooltip-backdrop') !== undefined;
                options.backdropOpacity = parseFloat(self._currentTooltip.attr('backdrop-opacity'));
                options.backdropClass = 'bg-darker';
                options.backdropOverClose = true;
            } else {
                options.template = self._currentTooltip.attr('tooltip');
            }

            self._currentTooltip.tooltip2(options);
            self._currentTooltip.tooltip2('show');

            var e = <TooltipManagerEvent>jQuery.Event('show');
            e.tooltip = self._currentTooltip;
            self._trigger('show', e);

            // console.log('Show ' + x + ' ' + y);
        }

        protected onhidetooltip = function() {
            var self: TooltipManager = this;
            if (!self._currentTooltip || !self._currentTooltip.tooltip2('instance')) {
                return;
            }
                        
            self._currentTooltip.tooltip2('hide');

            var e = <TooltipManagerEvent>jQuery.Event('hide');
            e.tooltip = self._currentTooltip;
            self._currentTooltip = null;
            self._trigger('hide', e);

            //console.log('Hide ' + (new Date()).getTime());
        }
    }
}

$.widget("modern.tooltipmanager", new modern.TooltipManager());

interface JQuery {
    tooltipmanager(): JQuery,
    tooltipmanager(options: modern.TooltipManagerOptions): JQuery,
}

'use strict';
var modern;
(function (modern) {
    var Checkbox = (function () {
        function Checkbox() {
            this._create = function () {
                var self = this;
                var element = this.element;
                self.input = element.find('input').on('change', function (e) {
                    self.value(!self._value);
                    self.onchange();
                }).on('focus', function (e) {
                    $(e.target).siblings('.mif-checkmark').addClass('bo-theme fg-theme').removeClass('bo-lighter bo-lighter-over');
                }).on('blur', function (e) {
                    $(e.target).siblings('.mif-checkmark').removeClass('bo-theme fg-theme').addClass('bo-lighter bo-lighter-over');
                });
                self._value = !(!self.input.attr('checked'));
                if (self.options && self.options.value !== undefined) {
                    self.value(self.options.value);
                }
                self.oncreated();
            };
            this.oncreated = function () {
            };
            this.onchange = function () {
                var self = this;
                var e = jQuery.Event("change");
                e.value = self._value;
                self._trigger('change', e);
            };
            this.value = function (value) {
                var self = this;
                if (value !== undefined && value != self._value) {
                    self._value = value;
                    self.format();
                }
                return self._value;
            };
            this.format = function () {
                var self = this;
                if (self._value !== undefined && self._value !== null && self._value) {
                    self.input.attr('checked', 'checked');
                }
                else {
                    self.input.removeAttr('checked');
                }
                return self;
            };
        }
        return Checkbox;
    })();
    modern.Checkbox = Checkbox;
})(modern || (modern = {}));
$.widget("modern.checkbox", new modern.Checkbox());

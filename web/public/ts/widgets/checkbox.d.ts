declare module modern {
    interface CheckboxOptions {
        value?: boolean;
    }
    interface CheckboxEvent extends JQueryEventObject {
        value: boolean;
    }
    class Checkbox {
        private _value;
        protected input: JQuery;
        protected element: JQuery;
        options: CheckboxOptions;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _create: () => void;
        protected oncreated: () => void;
        protected onchange: () => void;
        value: (value?: boolean) => boolean;
        format: () => Checkbox;
    }
}
interface JQuery {
    checkbox(): JQuery;
    checkbox(optionLiteral: string): JQuery;
    checkbox(optionLiteral: string): boolean;
    checkbox(methodName: 'format'): JQuery;
    checkbox(methodName: 'value'): boolean;
    checkbox(optionLiteral: string, value: boolean): boolean;
    checkbox(methodName: 'value', value: boolean): boolean;
    checkbox(options: modern.CheckboxOptions): JQuery;
}
interface JQueryStatic {
    modern: JQuery;
}

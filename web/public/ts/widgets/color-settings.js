'use strict';
var modern;
(function (modern) {
    var ColorSettingsOptions = (function () {
        function ColorSettingsOptions() {
        }
        return ColorSettingsOptions;
    })();
    modern.ColorSettingsOptions = ColorSettingsOptions;
    var ColorSettings = (function () {
        function ColorSettings() {
            this.options = new ColorSettingsOptions();
            this._create = function () {
                var self = this;
                var options = this.options;
                var animate = function () {
                    var body = $('body').addClass('color-changing').on('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function (e) {
                        body.removeClass('color-changing');
                    });
                };
                $.get('/resource?r=widgets%2Fcolor-settings.html').done(function (html) {
                    self.element.append(html).attr('id', 'color-settings');
                    self._schemeRadioButtons = self.element.find('[data-scheme] input').on('change', function (e) {
                        var event = e;
                        options.scheme = $(e.target).closest('[data-scheme]').attr('data-scheme');
                        event.theme = options.theme;
                        event.scheme = options.scheme;
                        self._trigger('change', event);
                    });
                    var schemeButtons = self.element.find('[data-scheme]').hover(function (e) {
                        var event = e;
                        animate();
                        event.theme = options.theme;
                        event.scheme = $(e.target).closest('[data-scheme]').attr('data-scheme');
                        self._trigger('enter', event);
                    }, function (e) {
                        animate();
                        self._trigger('leave', e);
                    });
                    self._themeButtons = self.element.find('[data-theme]').on('click', function (e) {
                        var event = e;
                        options.theme = $(e.target).closest('[data-theme]').attr('data-theme');
                        event.theme = options.theme;
                        event.scheme = options.scheme;
                        self.refresh();
                        self._trigger('change', event);
                    }).hover(function (e) {
                        var event = e;
                        animate();
                        event.scheme = options.scheme;
                        event.theme = $(e.target).closest('[data-theme]').attr('data-theme');
                        self._trigger('enter', event);
                    }, function (e) {
                        animate();
                        self._trigger('leave', e);
                    });
                    self.refresh();
                });
            };
            this.refresh = function () {
                var options = this.options;
                if (options.theme) {
                    this._themeButtons.removeClass('selected');
                    this._themeButtons.filter('[data-theme="' + options.theme + '"]').addClass('selected');
                }
                if (options.scheme) {
                    this._schemeRadioButtons.filter('[data-scheme] [value="' + options.scheme + '"]').attr('checked', 'checked');
                }
            };
        }
        return ColorSettings;
    })();
    modern.ColorSettings = ColorSettings;
})(modern || (modern = {}));
$.widget("modern.colorSettings", new modern.ColorSettings());

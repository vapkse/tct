declare module modern {
    class TooltipManagerOptions {
        withinElement: JQuery;
    }
    interface TooltipManagerEvent extends JQueryEventObject {
        tooltip: JQuery;
    }
    class TooltipManager {
        options: TooltipManagerOptions;
        protected element: JQuery;
        private _currentTooltip;
        private _currentEvent;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _create: () => void;
        protected onshowtooltip: () => void;
        protected onhidetooltip: () => void;
    }
}
interface JQuery {
    tooltipmanager(): JQuery;
    tooltipmanager(options: modern.TooltipManagerOptions): JQuery;
}

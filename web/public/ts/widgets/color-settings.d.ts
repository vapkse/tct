declare module modern {
    class ColorSettingsOptions {
        theme: string;
        scheme: string;
    }
    interface ColorSettingsEvent extends JQueryEventObject {
        theme?: string;
        scheme?: string;
    }
    class ColorSettings {
        options: ColorSettingsOptions;
        private _themeButtons;
        private _schemeRadioButtons;
        protected _create: () => void;
        refresh: () => void;
    }
}
interface JQuery {
    colorSettings(): modern.ColorSettings;
    colorSettings(method: string): modern.ColorSettings;
    colorSettings(options: modern.ColorSettingsOptions): JQuery;
}

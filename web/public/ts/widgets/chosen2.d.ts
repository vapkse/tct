declare module modern {
    class ChosenDefaultOptions {
        allow_single_deselect: boolean;
        disable_search: boolean;
        disable_search_threshold: number;
        enable_split_word_search: boolean;
        inherit_select_classes: boolean;
        max_selected_options: number;
        no_results_text: boolean;
        placeholder_text_multiple: string;
        placeholder_text_single: string;
        search_contains: boolean;
        single_backstroke_delete: boolean;
        width: string;
        display_disabled_options: boolean;
        display_selected_options: boolean;
        include_group_label_in_selected: boolean;
        items: any;
        selectedValue: string;
        valueField: string;
        textField: string;
        sortField: string;
        itemsUrl: string;
    }
    interface ChosenOptions {
        allow_single_deselect?: boolean;
        disable_search?: boolean;
        disable_search_threshold?: number;
        enable_split_word_search?: boolean;
        inherit_select_classes?: boolean;
        max_selected_options?: number;
        no_results_text?: string;
        placeholder_text_multiple?: string;
        placeholder_text_single?: string;
        search_contains?: boolean;
        single_backstroke_delete?: boolean;
        width?: number | string;
        display_disabled_options?: boolean;
        display_selected_options?: boolean;
        include_group_label_in_selected?: boolean;
        items?: any;
        selectedValue?: string;
        valueField?: string;
        textField?: string;
        sortField?: string;
        itemsUrl?: string;
    }
    interface ChosenEvent extends JQueryEventObject {
        value: string;
    }
    interface ChosenRequiredEvent extends JQueryEventObject {
        callback(options: ChosenOptions): void;
    }
    class Chosen2 {
        protected itemsLocale: string;
        protected element: JQuery;
        protected select: JQuery;
        protected label: JQuery;
        options: ChosenOptions;
        protected _trigger: (eventName: string, event: JQueryEventObject) => boolean;
        protected _create: () => void;
        protected onfocus: () => void;
        protected onblur: () => void;
        protected oncreated: () => void;
        protected onlookuprequired: (e: ChosenRequiredEvent) => ChosenOptions;
        protected onchange: () => void;
        private getOptionsHtml(items);
        setOptions(value: ChosenOptions): ChosenOptions;
        private copyAttributes(selectedValue);
        protected valueField(): string;
        protected textField(): string;
        selectedItem: (item?: any) => any;
        clearItems(): JQuery;
        selectedValue(value?: string): string;
    }
}
interface JQuery {
    chosen(): JQuery;
    chosen(options: modern.ChosenOptions): JQuery;
    chosen2(): JQuery;
    chosen2(optionLiteral: string): JQuery;
    chosen2(methodName: 'format'): JQuery;
    chosen2(optionLiteral: string): string;
    chosen2(methodName: 'selectedValue'): string;
    chosen2(optionLiteral: string, value: string): string;
    chosen2(methodName: 'selectedValue', value: string): string;
    chosen2(optionLiteral: string, value: modern.ChosenOptions): modern.ChosenOptions;
    chosen2(methodName: 'setOptions', value: modern.ChosenOptions): modern.ChosenOptions;
    chosen2(options: modern.ChosenOptions): JQuery;
    chosen2(methodName: 'clearItems'): JQuery;
}

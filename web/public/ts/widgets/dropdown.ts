﻿"use strict";

module modern {
    export interface DropDownOptions {
        parent: JQuery,
        template: string | JQuery,
        backdrop: boolean,
        backdropId: string,
        backdropCancel: boolean,
        modalId: string,
        position: JQueryUI.JQueryPositionOptions,
        cancelOnEsc: boolean,
        closeOnEnter: boolean,
        onclosed(): void,
        oncancel(isBackdrop: boolean): boolean
    }

    export class DropDown {
        protected $: {
            template?: JQuery,
            modalBody?: JQuery,
            modal?: JQuery,
            backdrop?: JQuery
        } = {};

        protected options: DropDownOptions;

        protected dispose = function() {
            var self = this as DropDown;
            self.close();
        }

        private cancel = function(isBackdrop?: boolean) {
            var self = this as DropDown;
            if (self.options.oncancel && self.options.oncancel(isBackdrop) === false) {
                return;
            }
            self.close();
        }

        public get modalBody() {
            var self = this as DropDown;
            return self.$.modalBody;
        }

        public close = function() {
            var self = this as DropDown;
            if (!self.$.modalBody) {
                return;
            }

            /*if (self._templateControl) {
                self._templateControl[self.options.templateControl]('dispose');
            }*/

            $(document).off('keydown', self.onKeyDown);

            self.$.modalBody.animate({ opacity: 0 }, 200, function() {
                if (self.$.backdrop) {
                    self.$.backdrop.remove();
                }
                else {
                    self.$.modalBody.remove();
                }

                delete self.$.modalBody;
                delete self.$.backdrop;
                if (self.options.onclosed) {
                    self.options.onclosed();
                }
            });
        }

        public show = function(options: DropDownOptions) {
            var self = this as DropDown;
            self.options = options;

            if (!options.parent) {
                options.parent = $('body');
            }

            var backdropHtml: Array<string> = [];
            if (options.backdrop !== false) {
                backdropHtml.push('<div class="backdrop');
                if (options.backdropId) {
                    backdropHtml.push('" id="' + options.backdropId);
                }
                backdropHtml.push('"></div>');
                self.$.backdrop = $(backdropHtml.join('')).appendTo(options.parent);

                if (options.backdropCancel) {
                    self.$.backdrop.click(function(e: JQueryEventObject) {
                        if ($(e.target).closest('.modal-body').is(self.$.modalBody)) {
                            return true;
                        }
                        self.cancel(true);
                    });
                }

                options.parent.find('[modal-disabled] form').attr('disabled', 'disabled');
            }

            var modalHtml: Array<string> = [];
            modalHtml.push('<div class="modal-body');
            if (options.modalId) {
                modalHtml.push('" id="' + options.modalId);
            }
            modalHtml.push('">');
            
            // Simple string template
            var isTemplateUrl = false;
            if (typeof options.template === 'string') {
                var urlRegex = /(https?:\/\/[^\s]+)/g;
                isTemplateUrl = urlRegex.test(options.template as string);
                if (!isTemplateUrl) {
                    modalHtml.push(options.template as string);
                }
            }

            modalHtml.push("</div>");

            self.$.modalBody = $(modalHtml.join('')).appendTo(options.backdrop !== false ? self.$.backdrop : options.parent).css('opacity', 0);

            if (isTemplateUrl) {
                // HTML file
                $.get(options.template as string, function(template) {
                    $(template).appendTo(self.$.modalBody);
                });
            } else if (options.template instanceof jQuery) {
                var $template = options.template as JQuery;
                $template.appendTo(self.$.modalBody);        
            }

            var animate = function() {
                self.$.modalBody.animate({ opacity: 1 }, 300);
            }

            if (options.position) {
                if (!options.position.within) {
                    options.position.within = options.parent;
                }
                if (!options.position.of) {
                    options.position.of = options.parent;
                }
                if (!options.position.my) {
                    options.position.my = "center center";
                }
                if (!options.position.at) {
                    options.position.at = "center center";
                }
                if (!options.position.collision) {
                    options.position.collision = "flip flip";
                }

                setTimeout(function() {
                    self.$.modalBody.position(options.position);
                    animate();
                }, 5);
            } else {
                animate();
            }

            $(document).on('keydown', self, self.onKeyDown);
            return self;
        }

        protected onKeyDown = function(e: JQueryKeyEventObject) {
            var self = this as DropDown;
            if (e.keyCode === 9 && e.data.options.backdrop !== false) {
                return false;
            }

            if (e.keyCode === 13 && e.data.options.closeOnEnter) {
                setTimeout(function() {
                    e.data.close();
                }, 100);
            }

            if (e.keyCode === 27 && e.data.options.cancelOnEsc) {
                e.data.cancel(false);
            }
        }
    }
}

export =modern;


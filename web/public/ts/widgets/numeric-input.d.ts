declare module modern {
    interface NumericInputOptions {
        thousandsSeparator?: string;
        decimalSeparator?: string;
        decimalCount?: number;
        value?: number;
    }
    interface NumericInputEvent extends JQueryEventObject {
        value: number;
    }
    class NumericInput extends TextInput {
        protected oncreated: () => void;
        private _createInputNumeric;
        private decimalCount;
        protected onchange: () => void;
        value: (value?: number) => number;
    }
}
interface JQuery {
    numericinput(): JQuery;
    numericinput(optionLiteral: string): JQuery;
    numericinput(optionLiteral: string): number;
    numericinput(methodName: 'format'): JQuery;
    numericinput(methodName: 'value'): number;
    numericinput(optionLiteral: string, value: number): number;
    numericinput(methodName: 'value', value: number): number;
    numericinput(options: modern.NumericInputOptions): JQuery;
}

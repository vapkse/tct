'use strict';

module modern {
    export interface FormValidatorOptions {
        container?: JQuery;
    }

    export class FormValidator {
        protected container: JQuery;
        private messages: JQuery;
        protected validationErrors: Array<validation.Message> = [];
        public element: JQuery;
        protected options: FormValidatorOptions;
        public _create = function() {
            var self = <FormValidator>this;
            self.container = self.options.container || self.element.closest('[validate]');
            self.messages = self.element.find('messages');
            self.oncreated();
        };

        protected oncreated = function() {
            var self = <FormValidator>this;

            // Check injected erros
            setTimeout(function() {
                var herrors = $('#errors').html();
                var errors = herrors && JSON.parse(herrors);
                if (errors && errors.length) {
                    self.showValidationErrors(errors);
                    errors.remove();
                }
            }, 100);
        }

        public getValidationMessage = function(uid: string) {
            var self = <FormValidator>this;
            return self.messages.find('[uid="' + uid + '"]').html();
        }

        public showValidationErrors = function(verrors: Array<validation.Message>) {
            var self = <FormValidator>this;
            if (verrors && verrors.length) {
                for (var ve = 0; ve < verrors.length; ve++) {
                    var verror = verrors[ve];
                    var fieldName = verror.fieldName;
                    if (fieldName) {
                        var fieldsCss = verror.fieldCss || 'error';
                        var field = self.container.find(self.fieldSelector(fieldName, verror.dataIndex)).addClass(fieldsCss);
                        if (verror.tooltip !== false) {
                            var text = verror.message;
                            var ft = field.find('.input-state-error');
                            if (verror.params){
                                text = $.translator.replaceParameter(text, verror.params);   
                                ft.attr('tpar', encodeURIComponent(JSON.stringify(verror.params)));
                            }
                            ft.attr('tooltip', text).attr('uid', verror.name).show();
                        }
                    }
                }
                self.validationErrors = verrors;
            }
            return self;
        }

        public showValidationsError = function(verror: validation.Message) {
            var self = <FormValidator>this;
            return self.showValidationErrors([verror])
        }

        public hideValidationError = function(filedName: string) {
            var self = <FormValidator>this;
            var verrors = self.validationErrors;
            if (verrors && verrors.length) {
                for (var ve = 0; ve < verrors.length; ve++) {
                    if (verrors[ve].fieldName === filedName) {
                        self.hideValidationMessage(verrors[ve]);
                        verrors.slice(ve, 1)
                    }
                }
            }
            return self;
        }

        private hideValidationMessage = function(verror: validation.Message) {
            var self = <FormValidator>this;
            var fieldName = verror.fieldName;
            if (fieldName) {
                self.removeClassNames(fieldName, verror.fieldCss, verror.dataIndex);
            }
            return self;
        }

        private removeClassNames = function(filedName: string, className?: string, dataIndex?: number) {
            var self = <FormValidator>this;
            className = className || 'error info warning';
            var field = self.container.find(self.fieldSelector(filedName, dataIndex)).removeClass(className);
            if (field.length) {
                field.filter('.input-state-error').removeAttr('tooltip').removeAttr('uid').hide();
            }
        }

        public hideValidationErrors = function() {
            var self = <FormValidator>this;
            var verrors = self.validationErrors;
            if (verrors && verrors.length) {
                for (var ve = 0; ve < verrors.length; ve++) {
                    self.hideValidationMessage(verrors[ve]);
                }
                self.validationErrors = null;
            }
            return self;
        }

        protected fieldSelector = function(field: string, dataIndex?: number) {
            var re = new RegExp('^[a-z0-9]', 'i');
            if (re.test(field)) {
                if (dataIndex !== undefined) {
                    return '[data-index="' + dataIndex + '"] [for="' + field + '"],[data-index="' + dataIndex + '"] [data-field="' + field + '"],[data-index="' + dataIndex + '"] #' + field + ',[data-index="' + dataIndex + '"] [data-field="' + field + '"] .input-state-error,[data-index="' + dataIndex + '"] #' + field + ' .input-state-error';
                }
                return '[for="' + field + '"],[data-field="' + field + '"],#' + field + ',[data-field="' + field + '"] .input-state-error,#' + field + ' .input-state-error';
            } else {
                return field;
            }
        }
    }
}

$.widget("modern.formvalidator", new modern.FormValidator());

interface JQuery {
    formvalidator(): JQuery;
    formvalidator(options: modern.FormValidatorOptions): JQuery;
    formvalidator(optionLiteral: string): JQuery,
    formvalidator(methodName: 'hideValidationError', fieldName: string): JQuery,
    formvalidator(methodName: 'hideValidationErrors'): JQuery,
    formvalidator(optionLiteral: string, value: string): JQuery,
    formvalidator(methodName: 'getValidationMessage', uid: string): JQuery,
    formvalidator(optionLiteral: string, verrors: Array<validation.Message>): JQuery,
    formvalidator(methodName: 'showValidationErrors', verrors: Array<validation.Message>): JQuery,
    formvalidator(optionLiteral: string, verror: validation.Message): JQuery,
    formvalidator(methodName: 'showValidationError', verror: validation.Message): JQuery,
}
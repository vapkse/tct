declare module modern {
    interface TranslatorControlEvent extends JQueryEventObject {
        value: string;
    }
    class TranslatorControl extends Chosen2 {
        private staticTranslations;
        private container;
        private messagerControl;
        protected oncreated: () => void;
        private getMessagerControl();
        protected onchange: () => void;
        changeLocale: (loc: string) => void;
        translate: (translations: translator.Translation, cb: (err: Error, translated: translator.Translation) => void) => void;
        translations(): translator.RequestResult;
        translation(pageid: string, id: string): string;
    }
}
interface JQuery {
    translatorControl(): JQuery;
    translatorControl(optionLiteral: string): string;
    translatorControl(methodName: 'selectedValue'): string;
    translatorControl(optionLiteral: string, value: string): string;
    translatorControl(methodName: 'selectedValue', value: string): string;
}

'use strict';
var modern;
(function (modern) {
    var Messager = (function () {
        function Messager() {
            this.currentMsgId = 0;
            this._create = function () {
                var self = this;
                self.element.attr('data-role', 'charm').attr('id', 'notify').charm({
                    position: 'top'
                });
                self.element.children().hide();
                setTimeout(function () {
                    var herrors = $('#errors').html();
                    var errors = herrors && JSON.parse(herrors);
                    if (errors && errors.length) {
                        self.showMessages(errors);
                    }
                }, 100);
            };
            this.fieldSelector = function (field) {
                return '[for="' + field + '"],#' + field;
            };
            this.showMessages = function (msgs, hidePrevious) {
                var self = this;
                var displayed = {};
                if (hidePrevious) {
                    self.element.children('.message').remove();
                }
                if (!msgs) {
                    msgs = self.parseMessages();
                    if (msgs.length === 0) {
                        return this;
                    }
                }
                for (var m = 0; m < msgs.length; m++) {
                    var msg = msgs[m];
                    if (!displayed[msg.message]) {
                        self.showMessage(msg);
                        displayed[msg.message] = true;
                    }
                }
                return this;
            };
            this.hideMessages = function (msgs) {
                var self = this;
                if (!msgs) {
                    var $message = self.element.children('.message').remove();
                    self.element.charm('close');
                }
                else {
                    for (var m = 0; m < msgs.length; m++) {
                        var $message = self.element.children('.message').find('.label:contains(' + msgs[m].message + ')').closest('.message');
                        self.removeMessageElement($message);
                    }
                }
                return self;
            };
            this.hideMessage = function (msg) {
                var self = this;
                var $message;
                if (typeof msg === 'string') {
                    $message = self.element.children('.message[data-field="' + msg + '"]');
                }
                else {
                    $message = self.element.children('.message').find('.label:contains(' + msg.message + ')').closest('.message');
                }
                self.removeMessageElement($message);
                return self;
            };
            this.removeMessageElement = function (msgElement) {
                var self = this;
                msgElement.remove();
                if (self.element.children('.message').length === 0) {
                    self.element.charm('close');
                }
            };
            this.parseMessages = function (element) {
                var self = this;
                var messages = [];
                var $messages = (element || self.element.children('messages')).children();
                $messages.each(function (index, elem) {
                    var $msg = $(elem);
                    var f = $msg.attr('f');
                    messages.push({
                        name: $msg.attr('uid'),
                        type: $msg.attr('t'),
                        autoCloseDelay: parseInt($msg.attr('d')),
                        message: $msg.text()
                    });
                });
                self.element.children('messages').empty();
                return messages;
            };
            this.showMessage = function (msg) {
                var self = this;
                var $templates = self.showInternal(msg.type, msg.name, msg.message, msg.autoCloseDelay, msg.onclose);
                var ve = msg;
                if (ve.fieldName) {
                    $templates.attr('data-field', ve.fieldName);
                }
                if (ve.params) {
                    $templates.find('.label').attr('tpar', encodeURIComponent(JSON.stringify(ve.params)));
                }
            };
            this.show = function (type, name, message, autoCloseDelay, onclose) {
                var self = this;
                self.showInternal(type, name, message, autoCloseDelay, onclose);
                return self;
            };
            this.showInternal = function (type, name, message, autoCloseDelay, onclose) {
                var self = this;
                if (!message) {
                    message = name;
                    name = null;
                }
                var text;
                if (typeof message === 'string') {
                    text = message;
                }
                else {
                    if (message.error && message.error instanceof Function) {
                        message = message.error();
                    }
                    text = message.responseText || message.statusText || message.message || message.toString();
                }
                if (/^[\[|\{](\s|.*|\w)*[\]|\}]$/.test(text)) {
                    var error = JSON.parse(text);
                    text = error.message;
                }
                var $templates = self.element.find('templates').children();
                var template = $templates.filter('[' + type + ']');
                if (template.length === 0) {
                    template = $templates.filter('.' + type);
                }
                if (template.length === 0) {
                    template = $templates.filter('.default');
                }
                else {
                    template = $(template[0]);
                }
                if (template.length > 0) {
                    var msgElement = template.clone().addClass('message');
                    var label = msgElement.find('.label') || msgElement;
                    if (name) {
                        label.attr('uid', name).attr('lang', '');
                    }
                    if (text) {
                        label.html(text);
                    }
                    msgElement.appendTo(self.element);
                    setTimeout(function () {
                        self.element.charm('open');
                    }, 100);
                    var messageCloser = function (msgElement, cb) {
                        return function (e) {
                            self.removeMessageElement(msgElement);
                            if (cb) {
                                cb();
                            }
                        };
                    };
                    if (autoCloseDelay) {
                        setTimeout(messageCloser(msgElement, onclose), autoCloseDelay * 1000);
                    }
                    msgElement.find('.button').on('click', messageCloser(msgElement, onclose));
                }
                return msgElement;
            };
            this.hide = function (id) {
                var self = this;
                self.element.charm('close');
            };
        }
        return Messager;
    })();
    modern.Messager = Messager;
})(modern || (modern = {}));
$.widget("modern.messager", new modern.Messager());

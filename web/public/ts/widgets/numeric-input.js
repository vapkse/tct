'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var modern;
(function (modern) {
    var NumericInput = (function (_super) {
        __extends(NumericInput, _super);
        function NumericInput() {
            _super.apply(this, arguments);
            this.oncreated = function () {
                var element = this.element;
                if (element.hasClass('numeric')) {
                    this._createInputNumeric();
                }
                element.data('numericinput', this);
            };
            this._createInputNumeric = function () {
                var self = this;
                var element = self.element.addClass('empty');
                var options = self.options;
                self.decimalCount = options.decimalCount || parseInt(element.attr("decimal-max"));
                if (isNaN(self.decimalCount)) {
                    self.decimalCount = 0;
                }
                var step = 1 / Math.pow(10, self.decimalCount);
                if (self.decimalCount > 0) {
                    self.input.attr('pattern', '[0-9]+([\.,][0-9]+)?').attr('step', step);
                }
                self.input.attr('formnovalidate', '');
                if (options.value !== undefined && options.value !== null) {
                    self.value(options.value);
                }
                var increase = function (sign, ctrlKey, shiftKey) {
                    var value = self.value();
                    if (ctrlKey && shiftKey) {
                        value += sign * step * 1000;
                    }
                    else if (ctrlKey) {
                        value += sign * step * 100;
                    }
                    else if (shiftKey) {
                        value += sign * step * 10;
                    }
                    else {
                        value += sign * step;
                    }
                    self.value(value);
                    self.onchange();
                };
                self.input.on('input', function () {
                    self.onchange();
                }).on('mousewheel', function (e) {
                    if (!self.input.is(':focus')) {
                        return true;
                    }
                    var wheelCount = e.originalEvent && (e.originalEvent.deltaY || e.originalEvent.deltaX);
                    var sign = wheelCount > 0 ? 1 : (wheelCount < 0 ? -1 : 0);
                    increase(sign, e.ctrlKey, e.shiftKey);
                    return false;
                }).on('keydown', function (e) {
                    if (!self.input.is(':focus')) {
                        return true;
                    }
                    if (e.keyCode === 40) {
                        increase(-1, e.ctrlKey, e.shiftKey);
                        return false;
                    }
                    else if (e.keyCode === 38) {
                        increase(+1, e.ctrlKey, e.shiftKey);
                        return false;
                    }
                });
            };
            this.onchange = function () {
                var self = this;
                var e = jQuery.Event("change");
                e.value = self.value();
                self.ensureEmptyClass();
                self._trigger('change', e);
            };
            this.value = function (value) {
                var self = this;
                var v = self.decimalCount ? parseFloat(self.input.prop('value')) : parseInt(self.input.prop('value'));
                if (value !== undefined) {
                    if (value === null || String(value) === '') {
                        self.input.prop('value', '');
                    }
                    else if (!isNaN(value)) {
                        var places = Math.pow(10, self.decimalCount || 0);
                        self.input.prop('value', Math.round(value * places) / places);
                    }
                    else {
                        self.input.prop('value', value);
                    }
                    v = value;
                    self.ensureEmptyClass();
                }
                return !isNaN(v) ? v : null;
            };
        }
        return NumericInput;
    })(modern.TextInput);
    modern.NumericInput = NumericInput;
})(modern || (modern = {}));
$.widget("modern.numericinput", $.modern.textinput, new modern.NumericInput());

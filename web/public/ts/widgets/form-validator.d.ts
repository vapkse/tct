declare module modern {
    interface FormValidatorOptions {
        container?: JQuery;
    }
    class FormValidator {
        protected container: JQuery;
        private messages;
        protected validationErrors: Array<validation.Message>;
        element: JQuery;
        protected options: FormValidatorOptions;
        _create: () => void;
        protected oncreated: () => void;
        getValidationMessage: (uid: string) => string;
        showValidationErrors: (verrors: validation.Message[]) => FormValidator;
        showValidationsError: (verror: validation.Message) => FormValidator;
        hideValidationError: (filedName: string) => FormValidator;
        private hideValidationMessage;
        private removeClassNames;
        hideValidationErrors: () => FormValidator;
        protected fieldSelector: (field: string, dataIndex?: number) => string;
    }
}
interface JQuery {
    formvalidator(): JQuery;
    formvalidator(options: modern.FormValidatorOptions): JQuery;
    formvalidator(optionLiteral: string): JQuery;
    formvalidator(methodName: 'hideValidationError', fieldName: string): JQuery;
    formvalidator(methodName: 'hideValidationErrors'): JQuery;
    formvalidator(optionLiteral: string, value: string): JQuery;
    formvalidator(methodName: 'getValidationMessage', uid: string): JQuery;
    formvalidator(optionLiteral: string, verrors: Array<validation.Message>): JQuery;
    formvalidator(methodName: 'showValidationErrors', verrors: Array<validation.Message>): JQuery;
    formvalidator(optionLiteral: string, verror: validation.Message): JQuery;
    formvalidator(methodName: 'showValidationError', verror: validation.Message): JQuery;
}

declare class WaitPanel {
    show(): void;
    hide(): void;
}
interface WaitPanelStatic {
    show(): void;
    hide(): void;
}
interface JQueryStatic {
    waitPanel: WaitPanelStatic;
}

'use strict';

module modern {
    export interface NumericInputOptions {
        thousandsSeparator?: string;
        decimalSeparator?: string;
        decimalCount?: number;
        value?: number;
    }

    export interface NumericInputEvent extends JQueryEventObject {
        value: number
    }

    export class NumericInput extends TextInput {
        protected oncreated = function() {
            var element = this.element
            if (element.hasClass('numeric')) {
                this._createInputNumeric();
            }
            element.data('numericinput', this);
        };

        private _createInputNumeric = function() {
            var self: NumericInput = this;
            var element = self.element.addClass('empty');
            var options = <NumericInputOptions>self.options;

            self.decimalCount = options.decimalCount || parseInt(element.attr("decimal-max"));

            if (isNaN(self.decimalCount)) {
                self.decimalCount = 0;
            }

            var step = 1 / Math.pow(10, self.decimalCount);

            if (self.decimalCount > 0) {
                self.input.attr('pattern', '[0-9]+([\.,][0-9]+)?').attr('step', step);
            }

            self.input.attr('formnovalidate', '');

            if (options.value !== undefined && options.value !== null) {
                self.value(options.value);
            }

            var increase = function(sign: number, ctrlKey: boolean, shiftKey: boolean) {
                var value = self.value();
                if (ctrlKey && shiftKey) {
                    value += sign * step * 1000;
                } else if (ctrlKey) {
                    value += sign * step * 100;
                } else if (shiftKey) {
                    value += sign * step * 10;
                } else {
                    value += sign * step;
                }
                self.value(value);
                self.onchange();
            }

            self.input.on('input', function() {
                self.onchange();
            }).on('mousewheel', function(e: JQueryMouseEventObject) {
                if (!self.input.is(':focus')) {
                    return true;
                }
                var wheelCount = e.originalEvent && (e.originalEvent.deltaY || e.originalEvent.deltaX);
                var sign = wheelCount > 0 ? 1 : (wheelCount < 0 ? -1 : 0);
                increase(sign, e.ctrlKey, e.shiftKey);
                return false;
            }).on('keydown', function(e: JQueryKeyEventObject){                
                if (!self.input.is(':focus')) {
                    return true;
                }
                if (e.keyCode === 40) {
                    // keydown
                    increase(-1, e.ctrlKey, e.shiftKey);
                    return false;
                } else if (e.keyCode === 38) {
                    // keyup
                    increase(+1, e.ctrlKey, e.shiftKey);
                    return false;
                }
            });
        };

        private decimalCount: number;

        protected onchange = function() {
            var self: NumericInput = this;
            var e = <NumericInputEvent>jQuery.Event("change");
            e.value = self.value();
            self.ensureEmptyClass();
            self._trigger('change', e);
        }

        public value = function(value?: number) {
            var self: NumericInput = this;
            var v = self.decimalCount ? parseFloat(self.input.prop('value')) : parseInt(self.input.prop('value'));
            if (value !== undefined) {
                if (value === null || String(value) === '') {
                    self.input.prop('value', '');
                } else if (!isNaN(value)) {
                    var places = Math.pow(10, self.decimalCount || 0);
                    self.input.prop('value', Math.round(value * places) / places);
                } else {
                    self.input.prop('value', value);
                }
                v = value;
                self.ensureEmptyClass();
            }

            return !isNaN(v) ? v : null;
        }
    }
}

$.widget("modern.numericinput", $.modern.textinput, new modern.NumericInput());

interface JQuery {
    numericinput(): JQuery,
    numericinput(optionLiteral: string): JQuery,
    numericinput(optionLiteral: string): number,
    numericinput(methodName: 'format'): JQuery,
    numericinput(methodName: 'value'): number,
    numericinput(optionLiteral: string, value: number): number,
    numericinput(methodName: 'value', value: number): number,
    numericinput(options: modern.NumericInputOptions): JQuery,
}

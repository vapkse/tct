declare module modern {
    interface DropDownOptions {
        parent: JQuery;
        template: string | JQuery;
        backdrop: boolean;
        backdropId: string;
        backdropCancel: boolean;
        modalId: string;
        position: JQueryUI.JQueryPositionOptions;
        cancelOnEsc: boolean;
        closeOnEnter: boolean;
        onclosed(): void;
        oncancel(isBackdrop: boolean): boolean;
    }
    class DropDown {
        protected $: {
            template?: JQuery;
            modalBody?: JQuery;
            modal?: JQuery;
            backdrop?: JQuery;
        };
        protected options: DropDownOptions;
        protected dispose: () => void;
        private cancel;
        modalBody: JQuery;
        close: () => void;
        show: (options: DropDownOptions) => DropDown;
        protected onKeyDown: (e: JQueryKeyEventObject) => boolean;
    }
}
export = modern;

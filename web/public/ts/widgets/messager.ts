'use strict';

module modern {
    export interface MessagerMessage extends translator.Message {
        autoCloseDelay?: number,
        onclose?: () => void
    }

    export class Messager {
        public element: JQuery;
        private currentMsgId = 0;
        public _create = function() {
            var self = <Messager>this;

            self.element.attr('data-role', 'charm').attr('id', 'notify').charm({
                position: 'top'
            });

            // Hide template
            self.element.children().hide();
            
            // Check for inbounds messages
            setTimeout(function() {
                var herrors = $('#errors').html();
                var errors = herrors && JSON.parse(herrors);
                if (errors && errors.length) {
                    self.showMessages(errors);
                }
            }, 100);
        };

        private fieldSelector = function(field: string) {
            //return '[for="'+field+'"],#'+field+',['+field+'],.'+field;
            return '[for="' + field + '"],#' + field;
        }

        public showMessages = function(msgs: Array<MessagerMessage>, hidePrevious?: boolean) {
            var self = <Messager>this;
            var displayed: { [msg: string]: boolean } = {};

            if (hidePrevious) {
                self.element.children('.message').remove();
            }

            if (!msgs) {
                msgs = self.parseMessages();
                if (msgs.length === 0) {
                    return this;
                }
            }

            for (var m = 0; m < msgs.length; m++) {
                var msg = msgs[m];
                if (!displayed[msg.message]) {
                    self.showMessage(msg);
                    displayed[msg.message] = true;
                }
            }

            return this;
        }

        public hideMessages = function(msgs?: Array<translator.Message>) {
            var self = <Messager>this;
            if (!msgs) {
                // Remove all
                var $message = self.element.children('.message').remove();
                self.element.charm('close');
            } else {
                for (var m = 0; m < msgs.length; m++) {
                    var $message = self.element.children('.message').find('.label:contains(' + msgs[m].message + ')').closest('.message');
                    self.removeMessageElement($message);
                }
            }
            return self;
        }

        public hideMessage = function(msg: translator.Message | string) {
            var self = <Messager>this;

            var $message: JQuery;
            if (typeof msg === 'string') {
                $message = self.element.children('.message[data-field="' + msg + '"]');
            } else {
                $message = self.element.children('.message').find('.label:contains(' + msg.message + ')').closest('.message');
            }

            self.removeMessageElement($message);
            return self;
        }

        private removeMessageElement = function(msgElement: JQuery) {
            var self = <Messager>this;
            msgElement.remove();
            if (self.element.children('.message').length === 0) {
                self.element.charm('close');
            }
        }

        private parseMessages = function(element?: JQuery): Array<MessagerMessage> {
            var self = <Messager>this;
            var messages: Array<MessagerMessage> = [];
            var $messages = (element || self.element.children('messages')).children();
            $messages.each(function(index: number, elem: Element) {
                var $msg = $(elem);
                var f = $msg.attr('f')
                messages.push({
                    name: $msg.attr('uid'),
                    type: $msg.attr('t'), // template class name
                    autoCloseDelay: parseInt($msg.attr('d')),
                    message: $msg.text()
                });
            });
            self.element.children('messages').empty();
            return messages;
        }

        public showMessage = function(msg: MessagerMessage) {
            var self = <Messager>this;
            var $templates = self.showInternal(msg.type, msg.name, msg.message, msg.autoCloseDelay, msg.onclose);
            var ve = msg as validation.Message;
            if (ve.fieldName) {
                $templates.attr('data-field', ve.fieldName);
            }
            if (ve.params) {
                $templates.find('.label').attr('tpar', encodeURIComponent(JSON.stringify(ve.params)))
            }
        }

        public show = function(type: string, name?: any, message?: any, autoCloseDelay?: number, onclose?: () => void) {
            var self = <Messager>this;
            self.showInternal(type, name, message, autoCloseDelay, onclose);
            return self;
        }

        private showInternal = function(type: string, name?: any, message?: any, autoCloseDelay?: number, onclose?: () => void) {
            var self = <Messager>this;
            if (!message) {
                message = name;
                name = null;
            }

            var text: string;
            if (typeof message === 'string') {
                text = message;
            } else {
                if (message.error && message.error instanceof Function) {
                    message = message.error();
                }
                text = message.responseText || message.statusText || message.message || message.toString();
            }

            if (/^[\[|\{](\s|.*|\w)*[\]|\}]$/.test(text)) {
                var error = JSON.parse(text);
                text = error.message;
            }

            var $templates = self.element.find('templates').children();
            // Search a template of default
            var template = $templates.filter('[' + type + ']');
            if (template.length === 0) {
                template = $templates.filter('.' + type);
            }
            if (template.length === 0) {
                template = $templates.filter('.default');
            } else {
                template = $(template[0]);
            }
            if (template.length > 0) {
                var msgElement = template.clone().addClass('message');
                var label = msgElement.find('.label') || msgElement;
                if (name) {
                    label.attr('uid', name).attr('lang', '');
                }
                if (text) {
                    label.html(text);
                }

                msgElement.appendTo(self.element);
                setTimeout(function() {
                    self.element.charm('open');
                }, 100)

                var messageCloser = function(msgElement: JQuery, cb: () => void) {
                    return function(e: JQueryEventObject) {
                        self.removeMessageElement(msgElement);
                        if (cb) {
                            cb();
                        }
                    }
                }

                if (autoCloseDelay) {
                    setTimeout(messageCloser(msgElement, onclose), autoCloseDelay * 1000);
                }

                msgElement.find('.button').on('click', messageCloser(msgElement, onclose));
            }

            return msgElement;
        }

        public hide = function(id?: string) {
            var self = <Messager>this;
            self.element.charm('close');
        }
    }
}

$.widget("modern.messager", new modern.Messager());

interface JQuery {
    messager(): JQuery;
    messager(optionLiteral: string): JQuery,
    messager(methodName: 'hide'): JQuery,
    messager(optionLiteral: string): modern.Messager,
    messager(optionLiteral: string, msgs: Array<translator.Message>): modern.Messager,
    messager(methodName: 'showMessages', msgs: Array<translator.Message>): modern.Messager,
    messager(methodName: 'hideMessages?', msgs: Array<translator.Message>): modern.Messager,
    messager(optionLiteral: string, msg: translator.Message): modern.Messager,
    messager(methodName: 'showMessage', msg: translator.Message): modern.Messager,
    messager(methodName: 'hideMessage', msg: translator.Message): modern.Messager,
    messager(optionLiteral: string, type: string, msgUniqueId?: string, message?: string, autoCloseDelay?: number): modern.Messager,
    messager(methodName: 'show', type: string, msgUniqueId?: string, message?: string, autoCloseDelay?: number): modern.Messager,
}
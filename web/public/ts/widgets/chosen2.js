'use strict';
var modern;
(function (modern) {
    var ChosenDefaultOptions = (function () {
        function ChosenDefaultOptions() {
            this.allow_single_deselect = false;
            this.disable_search = false;
            this.disable_search_threshold = 10;
            this.enable_split_word_search = true;
            this.inherit_select_classes = false;
            this.max_selected_options = 0;
            this.no_results_text = true;
            this.placeholder_text_multiple = ' ';
            this.placeholder_text_single = ' ';
            this.search_contains = true;
            this.single_backstroke_delete = true;
            this.width = "100%";
            this.display_disabled_options = false;
            this.display_selected_options = false;
            this.include_group_label_in_selected = false;
        }
        return ChosenDefaultOptions;
    })();
    modern.ChosenDefaultOptions = ChosenDefaultOptions;
    var Chosen2 = (function () {
        function Chosen2() {
            this._create = function () {
                var self = this;
                var chosenContainer;
                var preloader;
                if (self.element.is('select')) {
                    self.select = self.element;
                    preloader = self.element.siblings('.input-preloader').hide();
                }
                else {
                    self.select = self.element.find('select');
                    preloader = self.element.find('.input-preloader').hide();
                }
                var drop;
                self.select.addClass('empty bg-darker fg-lighter');
                self.select.siblings('.placeholder').on('mouseup', function () {
                    setTimeout(function () {
                        self.select.trigger('chosen:open');
                        setTimeout(function () {
                            self.select.trigger('chosen:activate');
                        }, 0);
                    }, 0);
                });
                self.select.on('chosen:ready', function (e) {
                    chosenContainer = self.select.siblings('.chosen-container');
                    chosenContainer.addClass('bo-theme').find('.chosen-single').addClass('fg-lighter');
                    chosenContainer.find("input").first().on('focus', function () {
                        self.onfocus();
                    }).on('blur', function (e) {
                        self.onblur();
                    });
                    chosenContainer.find('.chosen-choices').addClass('bg-darker fg-lighter').addClass('fg-theme-highlight');
                    drop = chosenContainer.find('.chosen-drop').addClass('bo-theme bg-theme-highlight fg-theme-highlight');
                    self.label = chosenContainer.find('.chosen-single span');
                    if (preloader && preloader.length) {
                        preloader.appendTo(drop);
                    }
                });
                var loadDatasOnDemand = function () {
                    var currentLocale = self.element.closest('[locale]').attr('locale') || 'en';
                    if (self.itemsLocale === currentLocale) {
                        return;
                    }
                    var e = jQuery.Event("lookuprequired");
                    e.callback = function (options) {
                        if (options) {
                            self.setOptions(options);
                            self.itemsLocale = currentLocale;
                        }
                        if (chosenContainer) {
                            preloader.hide();
                            chosenContainer.removeClass('loading');
                        }
                    };
                    if (self.onlookuprequired(e)) {
                        if (self.itemsLocale) {
                            self.itemsLocale = currentLocale;
                            self.select.trigger('chosen:updated');
                            if (self.selectedItem()) {
                                self.onchange();
                            }
                            return;
                        }
                        var itemsUrl = (self.options && self.options.itemsUrl) || self.element.attr('items-url');
                        if (itemsUrl) {
                            if (chosenContainer) {
                                chosenContainer.addClass('loading');
                                preloader.show();
                            }
                            $.get(itemsUrl).done(function (json) {
                                self.setOptions({
                                    items: json
                                });
                                self.itemsLocale = currentLocale;
                                if (chosenContainer) {
                                    preloader.hide();
                                    chosenContainer.removeClass('loading');
                                }
                            }).fail(function (e) {
                                var messagerControl = $('#messager').messager().data('modern-messager');
                                if (messagerControl) {
                                    messagerControl.show('error', e);
                                }
                            });
                        }
                    }
                    else {
                        if (chosenContainer) {
                            chosenContainer.addClass('loading');
                            preloader.show();
                        }
                    }
                };
                self.select.on('chosen:showing_dropdown', function (e) {
                    loadDatasOnDemand();
                }).change(function (e) {
                    self.copyAttributes(self.select.prop('value'));
                    self.onchange();
                });
                if (self.options) {
                    if (self.options.items) {
                        self.select.html(self.getOptionsHtml(self.options.items));
                    }
                    if (self.options.selectedValue) {
                        self.select.prop('value', self.options.selectedValue);
                    }
                }
                self.copyAttributes(self.select.prop('value'));
                var options = $.extend(new ChosenDefaultOptions(), self.options);
                var chosen = self.select.chosen(options).data('chosen');
                if (!chosen) {
                    setTimeout(function () {
                        loadDatasOnDemand();
                        self.select.show();
                    }, 100);
                }
                self.oncreated();
            };
            this.onfocus = function () {
                var self = this;
                self.element.addClass('bo-theme');
                var e = jQuery.Event('focus');
                self._trigger('focus', e);
            };
            this.onblur = function () {
                var self = this;
                self.element.removeClass('bo-theme');
                var e = jQuery.Event('blur');
                self._trigger('blur', e);
            };
            this.oncreated = function () {
            };
            this.onlookuprequired = function (e) {
                var self = this;
                return self._trigger('lookuprequired', e);
            };
            this.onchange = function () {
                var self = this;
                var e = jQuery.Event("change");
                e.value = self.selectedValue();
                self._trigger('change', e);
            };
            this.selectedItem = function (item) {
                var self = this;
                var valueField = self.valueField();
                if (item !== undefined) {
                    var currentValue = self.selectedValue();
                    var fieldValue = item && item[valueField] && item[valueField].toString();
                    if (fieldValue !== currentValue) {
                        if (self.options && self.options.items) {
                            self.selectedValue(fieldValue);
                        }
                        else {
                            var textField = self.textField();
                            var options = {
                                allow_single_deselect: true,
                                selectedValue: fieldValue,
                                valueField: valueField,
                                textField: textField
                            };
                            if (item) {
                                options.items = [item];
                            }
                            self.setOptions(options);
                        }
                    }
                    return item;
                }
                var selectedValue = self.selectedValue();
                if (selectedValue) {
                    var items = self.options.items;
                    if (items instanceof Array) {
                        for (var i = 0; i < items.length; i++) {
                            var value = items[i][valueField] && items[i][valueField].toString();
                            if (value === selectedValue) {
                                return items[i];
                            }
                        }
                    }
                }
                return null;
            };
        }
        Chosen2.prototype.getOptionsHtml = function (items) {
            var self = this;
            var options = [];
            var valueField = self.valueField();
            var textField = self.textField();
            var sort = self.element.attr('data-sort') || self.options.sortField;
            var addEmptyItem = self.element.attr('allow-deselect') || self.options.allow_single_deselect;
            if (addEmptyItem) {
                options.push('<option value="">&nbsp;</option>');
            }
            if (items instanceof Array) {
                var arr = items;
                if (sort) {
                    arr = arr.sort(function (a, b) {
                        if (typeof b[sort] === 'string') {
                            return a[sort].localeCompare(b[sort]);
                        }
                        else if (typeof b[sort] === 'number') {
                            return parseFloat(b[sort]) - parseFloat(a[sort]);
                        }
                    });
                }
                for (var i = 0; i < arr.length; i++) {
                    var uid = arr[i].uid;
                    options.push('<option value="');
                    options.push(arr[i][valueField]);
                    options.push('"');
                    if (uid) {
                        if (textField === 'text') {
                            options.push(' lang');
                        }
                        options.push(' uid="');
                        options.push(uid);
                        options.push('"');
                    }
                    options.push('>');
                    options.push(arr[i][textField]);
                    options.push('</option>');
                }
            }
            else {
                if (sort) {
                    throw "data-sort is only available for array as items";
                }
                for (var name in items) {
                    var id = valueField ? items[name][valueField] : name;
                    var text = textField ? items[name][textField] : items[name];
                    var uid = items[name].uid;
                    options.push('<option value="');
                    options.push(id);
                    options.push('"');
                    if (uid) {
                        if (items[name].text) {
                            options.push(' lang');
                        }
                        options.push(' uid="');
                        options.push(uid);
                        options.push('"');
                    }
                    options.push('>');
                    options.push(text);
                    options.push('</option>');
                }
            }
            return options.join('');
        };
        Chosen2.prototype.setOptions = function (value) {
            var self = this;
            if (value !== undefined) {
                var selected = value.selectedValue || self.select.prop('value');
                self.options = $.extend(new ChosenDefaultOptions(), value);
                if (value.items) {
                    self.select.html(self.getOptionsHtml(value.items));
                }
                if (selected) {
                    self.select.prop('value', selected);
                    self.copyAttributes(selected);
                }
                else {
                    self.select.prop('value', '');
                }
                self.select.trigger('chosen:updated');
            }
            return self.options;
        };
        Chosen2.prototype.copyAttributes = function (selectedValue) {
            var self = this;
            if (selectedValue) {
                if (self.label) {
                    var selectedOption = self.select.find('[value="' + selectedValue + '"]');
                    var attributes = selectedOption.prop('attributes');
                    if (attributes) {
                        for (var a = 0; a < attributes.length; a++) {
                            var att = attributes[a];
                            if (att.name !== 'value' && att.name !== 'class') {
                                self.label.attr(att.name, att.value);
                            }
                        }
                    }
                }
                self.select.removeClass('empty');
            }
            else {
                self.select.addClass('empty');
            }
        };
        Chosen2.prototype.valueField = function () {
            var self = this;
            return self.element.attr('value-field') || (self.options && self.options.valueField) || 'value';
        };
        Chosen2.prototype.textField = function () {
            var self = this;
            return self.element.attr('value-text') || (self.options && self.options.textField) || 'text';
        };
        Chosen2.prototype.clearItems = function () {
            var self = this;
            self.itemsLocale = null;
            self.options.items = [];
            self.select.html('');
            self.selectedValue(null);
            return self.element;
        };
        Chosen2.prototype.selectedValue = function (value) {
            var self = this;
            if (value !== undefined) {
                self.select.prop('value', value).trigger('chosen:updated');
                self.copyAttributes(value);
            }
            return self.select.prop('value');
        };
        return Chosen2;
    })();
    modern.Chosen2 = Chosen2;
})(modern || (modern = {}));
$.widget("modern.chosen2", new modern.Chosen2());

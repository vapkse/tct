'use strict';

module modern {
    export interface TextInputOptions {
        value?: any;
    }

    export interface TextInputEvent extends JQueryEventObject {
        value: any
    }

    export class TextInput {
        protected input: JQuery;
        protected element: JQuery;
        private placeholder: JQuery;
        public options: TextInputOptions = {};
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _create = function() {
            var self: TextInput = this;
            var element = self.element;
            var options = self.options;
            self.input = element.find("input");

            if (self.input.length === 0) {
                self.input = element.find("textarea");
                if (self.input.length > 0) {
                    // Fit textareas
                    var fitTextarea = function(textarea: JQuery, width?: number) {
                        textarea.css({
                            "resize": 'none',
                            "overflow-y": 'hidden'
                        });

                        if (width) {
                            textarea[0].style.width = width + 'px';
                        }
                        textarea[0].style.height = '0';
                        var adjust = textarea[0].scrollHeight;
                        if (width) {
                            textarea[0].style.width = '';
                        }
                        textarea[0].style.height = adjust + 'px';
                    }
                    $(window).on('beforeprint', function(e) {
                        fitTextarea(self.input, 600);
                    }).on('afterprint', function(e) {
                        fitTextarea(self.input);
                    });
                }
            }

            if (options.value) {
                self.value(options.value);
            } else {
                element.addClass('empty');
            }

            self.input.on("blur", function(e) {
                self.onblur();
            }).on("focus", function() {
                self.onfocus();
            }).on('change', function() {
                self.onchange();
            });

            var foucusOnInput = function() {
                setTimeout(function() {
                    self.input.focus();
                }, 0);
            }

            self.element.find('.unitholder').on('mouseup', function() { foucusOnInput(); });
            self.placeholder = self.element.find('.placeholder').on('mouseup', function() { foucusOnInput(); });
            self.element.find('.informer').on('mouseup', function() { foucusOnInput(); });

            element.data('textinput', this);
            self.oncreated();
        };

        protected oncreated = function() {
            // For overrides
        }

        protected onblur = function() {
            var self: TextInput = this;
            self.ensureEmptyClass();
            self.input.removeClass('bo-theme focus');
            var e = jQuery.Event('blur');
            self._trigger('blur', e);
        }

        protected onfocus = function() {
            var self: TextInput = this;
            if (!self.input.is('[readonly]')) {
                self.input.addClass('bo-theme focus');
            }
            var e = jQuery.Event('focus');
            self._trigger('focus', e);
        }

        protected onchange = function() {
            var self: TextInput = this;
            var e = <TextInputEvent>jQuery.Event("change");
            e.value = self.value();
            self.ensureEmptyClass();
            self._trigger('change', e);
        }

        protected isEmpty = function() {
            var self: TextInput = this;
            var val = self.value();
            return val === undefined || val === null || val === '';
        }

        protected ensureEmptyClass = function() {
            var self: TextInput = this;
            var val = self.value();
            if (self.isEmpty()) {
                self.element.addClass('empty');
                if (self.placeholder) {
                    self.placeholder.css({ display: "block" });
                }
            } else {
                self.element.removeClass('empty');
                if (self.placeholder) {
                    self.placeholder.css({ display: "none" });
                }
            }
        }

        public focus = function() {
            var self: TextInput = this;
            self.input.focus();
            return self.element;
        }

        public value = function(value?: any) {
            var self: TextInput = this;
            var val = self.input.val();
            if (value !== undefined && val !== value) {
                if (value === null) {
                    self.input.empty();
                } else {
                    self.input.val(value);
                }
                self.ensureEmptyClass();
                return value;
            }

            return val;
        }
    }
}

$.widget("modern.textinput", new modern.TextInput());

interface JQuery {
    textinput(): JQuery,
    textinput(optionLiteral: string): JQuery,
    textinput(optionLiteral: any): any,
    textinput(methodName: 'value'): any,
    textinput(optionLiteral: string, value: string): string,
    textinput(methodName: 'value', value: string): string,
    textinput(methodName: 'focus'): JQuery,
    textinput(options: modern.TextInputOptions): JQuery,
}

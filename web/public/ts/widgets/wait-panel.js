'option: strict';
var WaitPanel = (function () {
    function WaitPanel() {
    }
    WaitPanel.prototype.show = function () {
        var body = $('body').addClass('loading');
        var panel = body.find('#waiting-panel').show();
        if (panel.length === 0) {
            var cont = $('<div id="waiting-panel" style="display: none;">').appendTo(body);
            $.get('/resource?r=widgets%2F/wait-panel.html').done(function (html) {
                cont.append(html);
                if ($('body').is('.loading')) {
                    cont.show();
                }
                else {
                    cont.hide();
                }
            });
        }
    };
    WaitPanel.prototype.hide = function () {
        var panel = $('body').removeClass('loading').find('#waiting-panel').hide();
    };
    return WaitPanel;
})();
(function ($) {
    $.waitPanel = new WaitPanel();
})(jQuery);

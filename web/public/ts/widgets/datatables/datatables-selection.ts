'use strict';

module datatables {
    export interface SelectionEvent extends JQueryEventObject {
        value: boolean;
        table: JQuery;
    }

    export class Selection {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _hasSelected: boolean;
        protected _table: JQuery;
        protected _currentTR: JQuery;

        protected _create = function() {
            var self: Selection = this;
            var element = this.element;

            self._hasSelected = false;
            self._table = element.is('table') ? element : element.find('table');
            var selectRange = function(from: JQuery, to: JQuery) {
                var start = from.index();
                var current = to.addClass('selected');
                if (start < current.index()) {
                    current.prevUntil(from, 'tbody tr').addClass('selected');
                } else {
                    current.nextUntil(from, 'tbody tr').addClass('selected');
                }
                self._hasSelected = true;
                self.onchange();          
            }

            self._table.on('mousedown', 'tbody tr', function(e) {
                if (e.shiftKey && self._currentTR && !self._currentTR.is(this)) {
                    selectRange(self._currentTR, $(this));
                }
                else if (!e.ctrlKey) {
                    self._table.find('tbody tr.selected').removeClass('selected');
                    self._currentTR = $(this).addClass('selected');
                    self._hasSelected = true;
                    self.onchange();
                }
            }).on('mousemove', 'tbody tr', function(e) {
                if (e.buttons === 1 && self._currentTR && !self._currentTR.is(this)) {
                    selectRange(self._currentTR, $(this));
                }

            }).on('mouseup', 'tbody tr', function(e) {
                if (e.ctrlKey) {
                    $(this).toggleClass('selected');
                    if (self._table.find('tbody tr.selected').length) {
                        self._hasSelected = true;
                    } else {
                        self._hasSelected = false;
                    }
                    self.onchange();
                }
            });
        }

        protected onchange() {
            var self: Selection = this;
            var e = <SelectionEvent>jQuery.Event('change');
            e.value = self._hasSelected;
            e.table = self._currentTR.closest('table');
            self._trigger('change', e);
        }
    }
}

$.widget("datatables.selection", new datatables.Selection())

interface JQuery {
    selection(): JQuery,
}

interface JQueryStatic {
    datatables: JQuery;
}
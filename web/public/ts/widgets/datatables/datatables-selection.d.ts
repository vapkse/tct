declare module datatables {
    interface SelectionEvent extends JQueryEventObject {
        value: boolean;
        table: JQuery;
    }
    class Selection {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _hasSelected: boolean;
        protected _table: JQuery;
        protected _currentTR: JQuery;
        protected _create: () => void;
        protected onchange(): void;
    }
}
interface JQuery {
    selection(): JQuery;
}
interface JQueryStatic {
    datatables: JQuery;
}

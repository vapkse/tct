'use strict';
var datatables;
(function (datatables) {
    var Selection = (function () {
        function Selection() {
            this._create = function () {
                var self = this;
                var element = this.element;
                self._hasSelected = false;
                self._table = element.is('table') ? element : element.find('table');
                var selectRange = function (from, to) {
                    var start = from.index();
                    var current = to.addClass('selected');
                    if (start < current.index()) {
                        current.prevUntil(from, 'tbody tr').addClass('selected');
                    }
                    else {
                        current.nextUntil(from, 'tbody tr').addClass('selected');
                    }
                    self._hasSelected = true;
                    self.onchange();
                };
                self._table.on('mousedown', 'tbody tr', function (e) {
                    if (e.shiftKey && self._currentTR && !self._currentTR.is(this)) {
                        selectRange(self._currentTR, $(this));
                    }
                    else if (!e.ctrlKey) {
                        self._table.find('tbody tr.selected').removeClass('selected');
                        self._currentTR = $(this).addClass('selected');
                        self._hasSelected = true;
                        self.onchange();
                    }
                }).on('mousemove', 'tbody tr', function (e) {
                    if (e.buttons === 1 && self._currentTR && !self._currentTR.is(this)) {
                        selectRange(self._currentTR, $(this));
                    }
                }).on('mouseup', 'tbody tr', function (e) {
                    if (e.ctrlKey) {
                        $(this).toggleClass('selected');
                        if (self._table.find('tbody tr.selected').length) {
                            self._hasSelected = true;
                        }
                        else {
                            self._hasSelected = false;
                        }
                        self.onchange();
                    }
                });
            };
        }
        Selection.prototype.onchange = function () {
            var self = this;
            var e = jQuery.Event('change');
            e.value = self._hasSelected;
            e.table = self._currentTR.closest('table');
            self._trigger('change', e);
        };
        return Selection;
    })();
    datatables.Selection = Selection;
})(datatables || (datatables = {}));
$.widget("datatables.selection", new datatables.Selection());

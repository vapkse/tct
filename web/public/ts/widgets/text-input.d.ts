declare module modern {
    interface TextInputOptions {
        value?: any;
    }
    interface TextInputEvent extends JQueryEventObject {
        value: any;
    }
    class TextInput {
        protected input: JQuery;
        protected element: JQuery;
        private placeholder;
        options: TextInputOptions;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _create: () => void;
        protected oncreated: () => void;
        protected onblur: () => void;
        protected onfocus: () => void;
        protected onchange: () => void;
        protected isEmpty: () => boolean;
        protected ensureEmptyClass: () => void;
        focus: () => JQuery;
        value: (value?: any) => any;
    }
}
interface JQuery {
    textinput(): JQuery;
    textinput(optionLiteral: string): JQuery;
    textinput(optionLiteral: any): any;
    textinput(methodName: 'value'): any;
    textinput(optionLiteral: string, value: string): string;
    textinput(methodName: 'value', value: string): string;
    textinput(methodName: 'focus'): JQuery;
    textinput(options: modern.TextInputOptions): JQuery;
}

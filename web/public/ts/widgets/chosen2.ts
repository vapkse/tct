'use strict';

module modern {
    export class ChosenDefaultOptions {
        allow_single_deselect = false;
        disable_search = false;
        disable_search_threshold = 10;
        enable_split_word_search = true;
        inherit_select_classes = false;
        max_selected_options = 0;
        no_results_text = true;
        placeholder_text_multiple = ' ';
        placeholder_text_single = ' ';
        search_contains = true;
        single_backstroke_delete = true;
        width = "100%";
        display_disabled_options = false;
        display_selected_options = false;
        include_group_label_in_selected = false;
        items: any;
        selectedValue: string;
        valueField: string;
        textField: string;
        sortField: string;
        itemsUrl: string;
    }

    export interface ChosenOptions {
        allow_single_deselect?: boolean;
        disable_search?: boolean;
        disable_search_threshold?: number;
        enable_split_word_search?: boolean;
        inherit_select_classes?: boolean;
        max_selected_options?: number;
        no_results_text?: string;
        placeholder_text_multiple?: string;
        placeholder_text_single?: string;
        search_contains?: boolean;
        single_backstroke_delete?: boolean;
        width?: number | string;
        display_disabled_options?: boolean;
        display_selected_options?: boolean;
        include_group_label_in_selected?: boolean;
        items?: any;
        selectedValue?: string;
        valueField?: string,
        textField?: string,
        sortField?: string,
        itemsUrl?: string,
    }

    export interface ChosenEvent extends JQueryEventObject {
        value: string // Selected value
    }

    export interface ChosenRequiredEvent extends JQueryEventObject {
        callback(options: ChosenOptions): void;
    }

    export class Chosen2 {
        protected itemsLocale: string; // Locale for the loaded items
        protected element: JQuery;
        protected select: JQuery;
        protected label: JQuery;
        public options: ChosenOptions;
        protected _trigger: (eventName: string, event: JQueryEventObject) => boolean;
        protected _create = function() {
            var self = <Chosen2>this;
            var chosenContainer: JQuery;
            var preloader: JQuery;

            if (self.element.is('select')) {
                self.select = self.element;
                preloader = self.element.siblings('.input-preloader').hide();
            } else {
                self.select = self.element.find('select');
                preloader = self.element.find('.input-preloader').hide();
            }

            var drop: JQuery;

            self.select.addClass('empty bg-darker fg-lighter');

            self.select.siblings('.placeholder').on('mouseup', function() {
                setTimeout(function() {
                    self.select.trigger('chosen:open');
                    setTimeout(function() {
                        self.select.trigger('chosen:activate');
                    }, 0);
                }, 0);
            });

            self.select.on('chosen:ready', function(e: JQueryEventObject) {
                // Themes
                chosenContainer = self.select.siblings('.chosen-container');
                chosenContainer.addClass('bo-theme').find('.chosen-single').addClass('fg-lighter');
                chosenContainer.find("input").first().on('focus', function() {
                    self.onfocus();
                }).on('blur', function(e) {
                    self.onblur();
                });
                chosenContainer.find('.chosen-choices').addClass('bg-darker fg-lighter').addClass('fg-theme-highlight');
                drop = chosenContainer.find('.chosen-drop').addClass('bo-theme bg-theme-highlight fg-theme-highlight');
            
                // Preloader for dropdown
                self.label = chosenContainer.find('.chosen-single span');
                if (preloader && preloader.length) {
                    preloader.appendTo(drop);
                }
            });

            var loadDatasOnDemand = function() {
                var currentLocale = self.element.closest('[locale]').attr('locale') || 'en';
                if (self.itemsLocale === currentLocale) {
                    // Already loaded for the current locale                                
                    return;
                }

                var e = <ChosenRequiredEvent>jQuery.Event("lookuprequired");
                e.callback = function(options: ChosenOptions) {
                    if (options) {
                        self.setOptions(options)
                        self.itemsLocale = currentLocale;
                    }

                    if (chosenContainer) {
                        preloader.hide()
                        chosenContainer.removeClass('loading');
                    }
                };

                if (self.onlookuprequired(e)) {
                    if (self.itemsLocale) {
                        // just reload from select   
                        self.itemsLocale = currentLocale;
                        self.select.trigger('chosen:updated');
                        if (self.selectedItem()){
                            self.onchange();
                        }                                                
                        return;
                    }

                    var itemsUrl = (self.options && self.options.itemsUrl) || self.element.attr('items-url');
                    if (itemsUrl) {
                        if (chosenContainer) {
                            chosenContainer.addClass('loading');
                            preloader.show();
                        }

                        $.get(itemsUrl).done(function(json) {
                            self.setOptions({
                                items: json
                            })
                            self.itemsLocale = currentLocale;
                            if (chosenContainer) {
                                preloader.hide()
                                chosenContainer.removeClass('loading');
                            }

                        }).fail(function(e) {
                            var messagerControl: Messager = $('#messager').messager().data('modern-messager');
                            if (messagerControl) {
                                messagerControl.show('error', e);
                            }
                        });
                    }
                } else {
                    // Waiting for callback
                    if (chosenContainer) {
                        chosenContainer.addClass('loading');
                        preloader.show();
                    }
                }
            }

            self.select.on('chosen:showing_dropdown', function(e: JQueryEventObject) {
                loadDatasOnDemand();
            }).change(function(e: JQueryEventObject) {
                self.copyAttributes(self.select.prop('value'));
                self.onchange();
            });

            if (self.options) {
                if (self.options.items) {
                    self.select.html(self.getOptionsHtml(self.options.items));
                }
                if (self.options.selectedValue) {
                    self.select.prop('value', self.options.selectedValue);
                }
            }

            self.copyAttributes(self.select.prop('value'));

            var options = $.extend(new ChosenDefaultOptions(), self.options)
            var chosen = self.select.chosen(options).data('chosen');
            if (!chosen) {
                // Mobile unsupported
                setTimeout(function() {
                    loadDatasOnDemand();
                    self.select.show();
                }, 100);
            }
            self.oncreated();
        };

        protected onfocus = function() {
            var self: Chosen2 = this;
            self.element.addClass('bo-theme');
            var e = jQuery.Event('focus');
            self._trigger('focus', e);
        }

        protected onblur = function() {
            var self: Chosen2 = this;
            self.element.removeClass('bo-theme');
            var e = jQuery.Event('blur');
            self._trigger('blur', e);
        }
        
        protected oncreated = function() {
            // For overrides
        }

        protected onlookuprequired = function(e: ChosenRequiredEvent): ChosenOptions {
            var self: Chosen2 = this;
            return self._trigger('lookuprequired', e);
        }

        protected onchange = function() {
            var self: Chosen2 = this;
            var e = <ChosenEvent>jQuery.Event("change");
            e.value = self.selectedValue();
            self._trigger('change', e);
        }

        private getOptionsHtml(items: any) {
            var self: Chosen2 = this;
            var options: Array<string> = [];
            var valueField = self.valueField();
            var textField = self.textField();
            var sort = self.element.attr('data-sort') || self.options.sortField;
            var addEmptyItem = self.element.attr('allow-deselect') || self.options.allow_single_deselect;
            if (addEmptyItem) {
                options.push('<option value="">&nbsp;</option>');
            }

            if (items instanceof Array) {
                var arr = <Array<any>>items;
                if (sort) {
                    arr = arr.sort(function(a: any, b: any) {
                        if (typeof b[sort] === 'string') {
                            return a[sort].localeCompare(b[sort]);
                        } else if (typeof b[sort] === 'number') {
                            return parseFloat(b[sort]) - parseFloat(a[sort]);
                        }
                    })
                }

                for (var i = 0; i < arr.length; i++) {
                    var uid = arr[i].uid;
                    options.push('<option value="');
                    options.push(arr[i][valueField]);
                    options.push('"');
                    // Check for unique id and add to the options
                    if (uid) {
                        // Check if translatable entry
                        if (textField === 'text') {
                            options.push(' lang');
                        }
                        options.push(' uid="');
                        options.push(uid);
                        options.push('"');
                    }
                    options.push('>');
                    options.push(arr[i][textField]);
                    options.push('</option>');
                }
            } else {
                if (sort) {
                    throw "data-sort is only available for array as items";
                }

                for (var name in items) {
                    var id: string = valueField ? items[name][valueField] : name;
                    var text: string = textField ? items[name][textField] : items[name];
                    var uid = items[name].uid;
                    options.push('<option value="');
                    options.push(id);
                    options.push('"');
                    // Check for unique id and add to the options
                    if (uid) {
                        // Check if translatable entry
                        if (items[name].text) {
                            options.push(' lang');
                        }
                        options.push(' uid="');
                        options.push(uid);
                        options.push('"');
                    }
                    options.push('>');
                    options.push(text);
                    options.push('</option>');
                }
            }

            return options.join('');
        }

        public setOptions(value: ChosenOptions): ChosenOptions {
            var self = <Chosen2>this;

            if (value !== undefined) {
                var selected = value.selectedValue || self.select.prop('value');
                self.options = $.extend(new ChosenDefaultOptions(), value);
                if (value.items) {
                    self.select.html(self.getOptionsHtml(value.items));
                }

                if (selected) {
                    self.select.prop('value', selected);
                    self.copyAttributes(selected);
                } else {
                    self.select.prop('value', '');
                }
                self.select.trigger('chosen:updated');
            }

            return self.options;
        }

        private copyAttributes(selectedValue: string) {
            var self: Chosen2 = this;
                
            // Set corresponding uid and lang attribute to the label        
            if (selectedValue) {
                if (self.label) {
                    var selectedOption = self.select.find('[value="' + selectedValue + '"]');
                    var attributes = selectedOption.prop('attributes');
                    if (attributes) {
                        for (var a = 0; a < attributes.length; a++) {
                            var att = attributes[a];
                            if (att.name !== 'value' && att.name !== 'class') {
                                self.label.attr(att.name, att.value);
                            }
                        }
                    }
                }
                self.select.removeClass('empty');
            } else {
                self.select.addClass('empty');
            }
        }

        protected valueField() {
            var self: Chosen2 = this;
            return self.element.attr('value-field') || (self.options && self.options.valueField) || 'value';
        }

        protected textField() {
            var self: Chosen2 = this;
            return self.element.attr('value-text') || (self.options && self.options.textField) || 'text';
        }

        public selectedItem = function(item?: any): any {
            var self: Chosen2 = this;
            var valueField = self.valueField();

            if (item !== undefined) {
                // If no items, push the field until the dropdown will be opened
                var currentValue = self.selectedValue();
                var fieldValue = item && item[valueField] && item[valueField].toString();
                if (fieldValue !== currentValue) {
                    if (self.options && self.options.items) {
                        self.selectedValue(fieldValue);
                    } else {
                        var textField = self.textField();
                        var options: ChosenOptions = {
                            allow_single_deselect: true,
                            selectedValue: fieldValue,
                            valueField: valueField,
                            textField: textField
                        }
                        if (item) {
                            options.items = [item];
                        }
                        self.setOptions(options);
                    }
                }
                return item;
            }

            var selectedValue = self.selectedValue();
            if (selectedValue) {
                var items = self.options.items;
                if (items instanceof Array) {
                    for (var i = 0; i < items.length; i++) {
                        var value = items[i][valueField] && items[i][valueField].toString();
                        if (value === selectedValue) {
                            return items[i];
                        }
                    }
                }
            }

            return null;
        }

        public clearItems() {
            var self = <Chosen2>this;
            self.itemsLocale = null;
            self.options.items = [];
            self.select.html('');
            self.selectedValue(null);
            return self.element;
        }

        public selectedValue(value?: string): string {
            var self = <Chosen2>this;

            if (value !== undefined) {
                self.select.prop('value', value).trigger('chosen:updated');
                self.copyAttributes(value);
            }

            return self.select.prop('value');
        }
    }
}

$.widget("modern.chosen2", new modern.Chosen2())

interface JQuery {
    chosen(): JQuery;
    chosen(options: modern.ChosenOptions): JQuery;
    chosen2(): JQuery,
    chosen2(optionLiteral: string): JQuery,
    chosen2(methodName: 'format'): JQuery,
    chosen2(optionLiteral: string): string,
    chosen2(methodName: 'selectedValue'): string,
    chosen2(optionLiteral: string, value: string): string,
    chosen2(methodName: 'selectedValue', value: string): string,
    chosen2(optionLiteral: string, value: modern.ChosenOptions): modern.ChosenOptions,
    chosen2(methodName: 'setOptions', value: modern.ChosenOptions): modern.ChosenOptions,
    chosen2(options: modern.ChosenOptions): JQuery,
    chosen2(methodName: 'clearItems'): JQuery,
}


'use strict';
var tct;
(function (tct) {
    var usages;
    (function (usages_1) {
        require(['tube'], function (app) {
            (function ($) {
                $.StartScreen = function () {
                    var settingsPanel;
                    var docSelector;
                    var selectorOptions;
                    var pageId = $('[translate]').attr('translate');
                    var graphContainer = $('#traces');
                    var body = $('body');
                    var translations = $('translations');
                    var chart;
                    var redrawTimer;
                    var tracesContainer = $('#tracesContainer');
                    var infoPanel;
                    window.onbeforeunload = function () {
                        if (body.is('.unsaved')) {
                            return translations.find('[uid="msg-leave"]').text();
                        }
                    };
                    var transArea = $('#trans-area');
                    var translatorControl = transArea.find('#translator').translatorControl().on('translatorcontrolchange', function () {
                        setTimeout(function () {
                            if (chart) {
                                chart.loadChartTitles();
                            }
                        }, 100);
                    }).data('modern-translatorControl');
                    var messagerControl = $('#messager').messager().data('modern-messager');
                    var tooltipManager = $('#container').tooltipmanager().data('modern-tooltipmanager');
                    var $dataForm = $('[data-form]');
                    var validatorControl = $('#validator-panel').formvalidator({
                        container: $dataForm
                    }).data('tct-formvalidator');
                    var refreshInfoPanel = function () {
                        var options = {
                            infos: chart.getInfos(),
                            usage: dataForm.dataset()
                        };
                        if (!infoPanel) {
                            requirejs(['anodeChartInfos'], function () {
                                infoPanel = $('#infoPanel').anodeChartInfos(options).data('tct-anodeChartInfos');
                            });
                        }
                        else {
                            infoPanel.setOptions(options);
                        }
                    };
                    var dataForm = $dataForm.dataform({}).on('dataformchange', function (e, data) {
                        validatorControl.validateField(data);
                        var redraw = function () {
                            if (redrawTimer) {
                                clearTimeout(redrawTimer);
                            }
                            redrawTimer = setTimeout(function () {
                                redrawTimer = undefined;
                                chart.updateVinLines();
                                chart.redraw(true);
                                refreshInfoPanel();
                            }, 500);
                        };
                        if (chart) {
                            switch (data.fieldName) {
                                case 'unit':
                                    chart.updatePmax();
                                case 'va':
                                case 'iazero':
                                case 'load':
                                case 'mode':
                                    chart.updateWorkingPoint();
                                    chart.updateLoadLines();
                                    chart.redraw(false);
                                case 'vinpp':
                                    redraw();
                                    break;
                            }
                        }
                    }).on('dataformload', function (e) {
                        validatorControl.prevalidateAll();
                        $.waitPanel.hide();
                    }).on('dataformfocus', function (e, data) {
                        validatorControl.hideValidationError(data.fieldName);
                        messagerControl.hideMessage(data.fieldName);
                    }).data('data-providing-dataform');
                    $('button[unsaved]').on('click', function () {
                        validatorControl.validateAll(function (verrors) {
                            if (verrors && verrors.length) {
                                messagerControl.showMessages(verrors);
                            }
                            else if (dataForm.dataset().isModified()) {
                                $.waitPanel.show();
                                var tubeUsage = dataForm.dataset();
                                var json = tubeUsage.toJson();
                                var qjson = JSON.stringify(json);
                                var reqstring = ['d=saveusage'];
                                var qstring = location.query();
                                reqstring.push('id=' + qstring['id']);
                                if (!tubeUsage.isNew) {
                                    reqstring.push('usageid=' + qstring['usageid']);
                                }
                                else {
                                    var userid = qstring['userid'];
                                    if (userid) {
                                        reqstring.push('userid=' + encodeURIComponent(userid));
                                    }
                                }
                                reqstring.push('j=' + encodeURIComponent(qjson));
                                var ajaxRequest = $.ajax({
                                    url: '/editor?' + reqstring.join('&'),
                                    type: 'post',
                                    timeout: 180000,
                                    success: function (json) {
                                        if (json.error) {
                                            messagerControl.show('error', json.error);
                                        }
                                        else {
                                            var saveUsageResult = json;
                                            $.pageState.replaceOrAddState(json);
                                            $.pageState.replaceUrlParams({
                                                id: String(saveUsageResult.tubeData._id),
                                                usageid: saveUsageResult.usageId,
                                                userid: ''
                                            });
                                            dataForm.dataset().resetOriginalValues();
                                            validatorControl.prevalidateAll();
                                            var message = {
                                                autoCloseDelay: 15,
                                                name: 'datassaved',
                                                message: 'Your datas was saved successfully.',
                                                type: 'ok'
                                            };
                                            $.translator.translateMessages([message], function (translated) {
                                                messagerControl.showMessages(translated);
                                            });
                                        }
                                        $.waitPanel.hide();
                                    },
                                    error: function (e) {
                                        if (e.statusText !== 'canceled') {
                                            if (/^[\[|\{](\s|.*|\w)*[\]|\}]$/.test(e.responseText)) {
                                                var verror = JSON.parse(e.responseText);
                                                validatorControl.showValidationsError(verror);
                                                messagerControl.showMessage(verror);
                                            }
                                            else {
                                                messagerControl.show('error', e);
                                            }
                                        }
                                        $.waitPanel.hide();
                                    },
                                });
                            }
                        });
                    });
                    var huser = $('#user').html();
                    var user = huser && JSON.parse(huser);
                    var userInfosPanel = body.find('#user-infos-panel').userInfos({
                        user: user
                    }).on('userinfoscreated', function () {
                        transArea.animate({ opacity: 1 }, 500);
                    });
                    $('button[home]').on('click', function () {
                        var searchparams = location.query()['rl'];
                        location.href = '/' + (searchparams ? searchparams : '');
                    });
                    $('button[settings-panel]').on('click', function () {
                        if (!settingsPanel) {
                            requirejs(['settingsPanel'], function (settingsPanel) {
                                settingsPanel = $('#settings-panel');
                                var settings = new tct.Settings(localStorage);
                                settingsPanel.settingsPanel(settings).bind('settingspanelchange', function (e, data) {
                                    if (data.colorSettings.scheme) {
                                        body.attr('id', 'scheme-' + data.colorSettings.scheme);
                                    }
                                    if (data.colorSettings.theme) {
                                        body.attr('theme', data.colorSettings.theme);
                                    }
                                });
                                settingsPanel.settingsPanel('toogle');
                            });
                        }
                        else {
                            settingsPanel.settingsPanel('toogle');
                        }
                    });
                    $('#edit').on('click', function () {
                        var queryString = location.query();
                        var reqstring = [];
                        var userid = queryString['userid'];
                        reqstring.push('id=' + queryString['id']);
                        if (userid) {
                            reqstring.push('userid=' + queryString['userid']);
                        }
                        reqstring.push('rl=' + encodeURIComponent(queryString['rl']));
                        location.href = '/editor?' + reqstring.join('&');
                    });
                    $('#file-selector').on('click', function () {
                        var usage = dataForm.dataset();
                        selectorOptions.selectedIds = usage.traces.value;
                        if (!docSelector) {
                            requirejs(['docSelector'], function () {
                                docSelector = $('[doc-selector]');
                                docSelector.docSelector(selectorOptions).on('docselectorcreated', function () {
                                    docSelector.docSelector('show');
                                }).on('docselectorokclicked', function (e) {
                                    var usage = dataForm.dataset();
                                    usage.traces.value = e.selectedIds.length && e.selectedIds[0];
                                    dataForm.refresh();
                                    updateGraphs();
                                    validatorControl.prevalidateAll();
                                });
                            });
                        }
                        else {
                            docSelector.docSelector('setOptions', selectorOptions);
                        }
                    });
                    var updateGraphs = function () {
                        var usage = dataForm.dataset();
                        var trace = usage.traces.value;
                        graphContainer.find('.anode-transfert-chart').remove();
                        var tube = validatorControl.options.tube;
                        var doc = tube.getDocumentById(trace);
                        usage.zoom.value = {};
                        if (doc) {
                            loadGraph(doc);
                        }
                    };
                    var loadGraph = function (doc) {
                        var usage = dataForm.dataset();
                        var refreshTimeout = window.browserVersion().browser === 'firefox' ? 250 : 1;
                        var refreshTimer;
                        requirejs(['anodeChartTransfert', 'ajaxDownloadFile'], function () {
                            var options = {
                                url: '/download?url=' + doc.filename.value,
                                dataType: 'json'
                            };
                            var unitIndex = usage.unit.value;
                            var pmax = 0;
                            if (unitIndex !== undefined) {
                                var tube = validatorControl.options.tube;
                                pmax = tube.units[unitIndex].pamax.value;
                            }
                            utils.AjaxDownloadFile.download(options).onSuccessFinish(function (status, statusText, res, headers) {
                                var json = res['json'];
                                var result = typeof json === 'string' ? JSON.parse(json) : json;
                                if (result.error) {
                                    messagerControl.showMessage(result.error);
                                }
                                else {
                                    var options = {
                                        element: $('<div class="anode-transfert-chart"></div>').attr('id', doc.getField('_id').value),
                                        tubeGraph: result.tubeGraph,
                                        pmax: pmax,
                                        usage: usage,
                                        title: doc.description.value,
                                        editable: true,
                                        height: function (chart) {
                                            var width = chart.width();
                                            return Math.max(400, width / 2);
                                        },
                                        optionschanged: function () {
                                            if (refreshTimer) {
                                                clearTimeout(refreshTimer);
                                            }
                                            refreshTimer = setTimeout(function () {
                                                refreshTimer = 0;
                                                dataForm.refresh();
                                                refreshInfoPanel();
                                                validatorControl.prevalidateAll();
                                            }, refreshTimeout);
                                        },
                                        ready: function () {
                                            refreshInfoPanel();
                                        }
                                    };
                                    chart = new tct.AnodeTransfertChart(options);
                                    graphContainer.append(options.element);
                                }
                            }).onErrorFinish(function (status, statusText, res, headers) {
                                messagerControl.show('error', statusText);
                            });
                        });
                    };
                    var loadGraphs = function (tube, usage) {
                        graphContainer.empty();
                        if (tube && usage && usage.traces && usage.traces.value) {
                            var doc = tube.getDocumentById(usage.traces.value);
                            if (doc) {
                                loadGraph(doc);
                            }
                        }
                    };
                    var loadDataset = function (tube, usage) {
                        dataForm.dataset(usage);
                        if (usage) {
                            validatorControl.setOptions({
                                tube: tube,
                                usage: usage
                            });
                            loadGraphs(tube, usage);
                        }
                    };
                    var loadTube = function (tube) {
                        var createNewUsage = function () {
                            var defaultUnit = parseInt(queryString['unit']);
                            usage = tube.createArrayFieldSubset('usage', {
                                unit: !isNaN(defaultUnit) ? defaultUnit : undefined,
                            });
                            usage.isNew = true;
                            return usage;
                        };
                        var queryString = location.query();
                        var usageid = queryString['usageid'];
                        var userid = queryString['userid'];
                        var usage;
                        if (!usageid || usageid === 'new') {
                            usage = createNewUsage();
                            if (queryString['docid']) {
                                usage.traces.value = queryString['docid'];
                            }
                        }
                        else {
                            var usages = tube.usages.value;
                            var selectedUsages = usages.filter(function (u) { return u._id === usageid; });
                            if (selectedUsages.length === 0) {
                                createNewUsage();
                            }
                            else {
                                usage = selectedUsages[0];
                                if (userid && userid !== (user && user._id)) {
                                    usage.isNew = true;
                                }
                            }
                        }
                        $dataForm.find('#name').text(tube.getField('name').value);
                        selectorOptions = {
                            tubeData: tube.toJson(),
                            title: translations.find('#seltitle').prop('innerHTML'),
                            mode: 'radio',
                            filter: new RegExp('^.+\.tds$'),
                            showUsages: false
                        };
                        loadDataset(tube, usage);
                    };
                    var loadFromUrl = function () {
                        $.waitPanel.show();
                        var queryString = location.query();
                        var tubeid = queryString['id'];
                        var userid = queryString['userid'];
                        var reqstring = [];
                        reqstring.push('id=' + tubeid);
                        if (userid) {
                            reqstring.push('userid=' + userid);
                        }
                        var ajaxRequest = $.ajax({
                            url: '/editor?' + reqstring.join('&'),
                            type: 'post',
                            dataType: 'json',
                            timeout: 60000,
                            success: function (json) {
                                if (json.error) {
                                    messagerControl.show('error', json.error);
                                }
                                else {
                                    var tube = new app.Tube(json);
                                    loadTube(tube);
                                    $.pageState.replaceOrAddState(json);
                                    $.waitPanel.hide();
                                }
                            },
                            error: function (e) {
                                if (e.statusText !== 'canceled') {
                                    messagerControl.show('error', e);
                                    loadDataset();
                                }
                                $.waitPanel.hide();
                            },
                        });
                    };
                    setTimeout(function () {
                        loadFromUrl();
                    }, 1);
                    $(window).on('popstate', function (e) {
                        var event = e.originalEvent;
                        if (event && event.state && event.state.j) {
                            var tube = new app.Tube(event.state.j);
                            loadTube(tube);
                        }
                        else {
                            loadFromUrl();
                        }
                    });
                    $('#test').on('click', function (e) {
                        chart.drawCrossPoints(true);
                    });
                };
            })(jQuery);
            $(function () {
                if (location.hostname === '127.0.0.1') {
                    var browserVersion = window.browserVersion();
                    if (browserVersion.version > 9) {
                        $('head').append('<script type="text/javascript" src="http://127.0.0.1:38080/livereload.js"><\/script>');
                    }
                }
                $.StartScreen();
            });
        });
    })(usages = tct.usages || (tct.usages = {}));
})(tct || (tct = {}));

'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var tct;
(function (tct) {
    var usages;
    (function (usages) {
        var FormValidator = (function (_super) {
            __extends(FormValidator, _super);
            function FormValidator() {
                _super.apply(this, arguments);
                this.oncreated = function () {
                    var self = this;
                    var field = self.container.find('[data-fields]');
                    self.$ = {
                        _body: $('body'),
                        _fields: field,
                        _dataPins: self.container.find('[data-pin]'),
                        _unsavedButton: $('[unsaved]'),
                        _savedButton: $('[saved]'),
                        _unitSelector: self.container.find('#unit-selector'),
                        _graphContainer: self.container.find('#traces')
                    };
                };
                this.setOptions = function (options) {
                    var self = this;
                    self.options = options;
                    self.prevalidateAll();
                };
                this.hideRow = function (row) {
                    if (!row.hasClass('hidden')) {
                        row.addClass('transitioning');
                        setTimeout(function () {
                            row.addClass('transitioning');
                            row.addClass('hidden');
                            setTimeout(function () {
                                row.removeClass('transitioning');
                            }, 400);
                        }, 0);
                    }
                };
                this.showRow = function (row) {
                    if (row.hasClass('hidden')) {
                        row.addClass('transitioning');
                        setTimeout(function () {
                            row.removeClass('hidden');
                            setTimeout(function () {
                                row.removeClass('transitioning');
                            }, 400);
                        }, 0);
                    }
                };
                this.prevalidateTypeUnit = function () {
                    var self = this;
                    var unitIndex = self.options.usage && self.options.usage.unit.value;
                    var unitConfig;
                    var type = self.options.tube.type.value;
                    var unitsCount = type.sym ? 0 : type.cfg.length;
                    if (!isNaN(unitIndex)) {
                        unitConfig = type.cfg[unitIndex];
                    }
                    if (unitsCount > 1) {
                        self.$._unitSelector.show();
                        self.$._unitSelector.find('#units').children().each(function (index, elem) {
                            if (index >= unitsCount) {
                                $(elem).hide();
                            }
                            else {
                                $(elem).show();
                            }
                        });
                    }
                    else {
                        self.$._unitSelector.hide();
                        if (isNaN(unitIndex) && self.options.usage) {
                            self.options.usage.unit.value = 0;
                        }
                    }
                    self.$._dataPins.each(function (index, element) {
                        var $element = $(element);
                        var dataPin = $element.attr('data-pin');
                        if (dataPin) {
                            if (!unitConfig || unitConfig.pins.indexOf(dataPin) >= 0) {
                                self.showRow($element);
                            }
                            else {
                                self.hideRow($element);
                            }
                        }
                    });
                    return unitIndex;
                };
                this.prevalidateSaveButton = function () {
                    var self = this;
                    if (self._saveTimer) {
                        clearTimeout(self._saveTimer);
                    }
                    self._saveTimer = setTimeout(function () {
                        self._saveTimer = 0;
                        if (self.options.usage.isModified()) {
                            self.$._body.addClass('unsaved');
                        }
                        else {
                            self.$._body.removeClass('unsaved');
                        }
                    }, 300);
                };
                this.prevalidateAll = function () {
                    var self = this;
                    if (!self.options.usage) {
                        return self;
                    }
                    self.prevalidateTypeUnit();
                    self.prevalidateSaveButton();
                    self.hideValidationErrors();
                    var ve = self.options.usage.validate();
                    if (ve.length) {
                        for (var i = 0; i < ve.length; i++) {
                            var field = self.options.usage.getField(ve[i].fieldName);
                            if (self.options.usage.isDirtyField(field)) {
                                self.showValidationsError(ve[i]);
                            }
                        }
                    }
                    return self;
                };
                this.validateAll = function (cb) {
                    var self = this;
                    var verrors = [];
                    var done = function (verrors) {
                        if (verrors && verrors.length) {
                            $.translator.translateMessages(verrors, function (translated) {
                                self.showValidationErrors(translated);
                                cb(translated);
                            });
                        }
                        else {
                            cb();
                        }
                    };
                    var mergeErrors = function (errors) {
                        errors.map(function (err) { return verrors.push(err); });
                    };
                    mergeErrors(self.options.usage.validate());
                    done(verrors);
                };
                this.validateField = function (data) {
                    var self = this;
                    var ve = self.options.usage.validateField(data.field, data.fieldName);
                    if (ve.length) {
                        self.showValidationErrors(ve);
                    }
                    else {
                        self.hideValidationError(data.fieldName);
                    }
                    self.prevalidateSaveButton();
                };
            }
            FormValidator.prototype.updateZoom = function (zoomData) {
                var self = this;
                var usage = self.options.usage;
                usage.zoom.value = zoomData;
                self.prevalidateSaveButton();
            };
            FormValidator.prototype.updateSeriesVisibility = function (series) {
                var self = this;
                var usage = self.options.usage;
                usage.series.value = series;
                self.prevalidateSaveButton();
            };
            return FormValidator;
        })(modern.FormValidator);
        usages.FormValidator = FormValidator;
    })(usages = tct.usages || (tct.usages = {}));
})(tct || (tct = {}));
$.widget("tct.formvalidator", $.modern.formvalidator, new tct.usages.FormValidator());

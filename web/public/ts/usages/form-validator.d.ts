declare module tct.usages {
    interface FormValidatorOptions extends modern.FormValidatorOptions {
        usage: TubeUsage.TubeUsage;
        tube: tct.Tube;
    }
    class FormValidator extends modern.FormValidator {
        private $;
        private _saveTimer;
        private _updateGraphTimer;
        private originalDataset;
        options: FormValidatorOptions;
        element: JQuery;
        protected oncreated: () => void;
        setOptions: (options: FormValidatorOptions) => void;
        private hideRow;
        private showRow;
        prevalidateTypeUnit: () => number;
        prevalidateSaveButton: () => void;
        updateZoom(zoomData: ZoomData): void;
        updateSeriesVisibility(series: {
            [vg1: string]: boolean;
        }): void;
        prevalidateAll: () => FormValidator;
        validateAll: (cb: (verrors?: validation.Message[]) => void) => void;
        validateField: (data: DataProviding.DataFieldEventData) => void;
    }
}
interface JQuery {
    formvalidator(options: tct.usages.FormValidatorOptions): JQuery;
}

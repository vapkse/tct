'use strict';

module tct.usages {
    export interface FormValidatorOptions extends modern.FormValidatorOptions {
        usage: TubeUsage.TubeUsage
        tube: tct.Tube
    }

    export class FormValidator extends modern.FormValidator {
        private $: {
            _fields: JQuery,
            _unitSelector: JQuery,
            _unsavedButton: JQuery,
            _savedButton: JQuery,
            _dataPins: JQuery,
            _body: JQuery,
            _graphContainer: JQuery,
        };
        private _saveTimer: number;
        private _updateGraphTimer: number;
        private originalDataset: DataProviding.Dataset;
        public options: FormValidatorOptions;
        public element: JQuery;
        protected oncreated = function() {
            var self = <FormValidator>this;

            var field = self.container.find('[data-fields]');
            self.$ = {
                _body: $('body'),
                _fields: field,
                _dataPins: self.container.find('[data-pin]'),
                _unsavedButton: $('[unsaved]'),
                _savedButton: $('[saved]'),
                _unitSelector: self.container.find('#unit-selector'),
                _graphContainer: self.container.find('#traces')
            }
        }

        public setOptions = function(options: FormValidatorOptions) {
            var self: FormValidator = this;
            self.options = options;
            self.prevalidateAll();
        }

        private hideRow = function(row: JQuery) {
            if (!row.hasClass('hidden')) {
                row.addClass('transitioning');
                setTimeout(function() {
                    row.addClass('transitioning');
                    row.addClass('hidden');
                    setTimeout(function() {
                        row.removeClass('transitioning');
                    }, 400);
                }, 0);
            }
        }

        private showRow = function(row: JQuery) {
            if (row.hasClass('hidden')) {
                row.addClass('transitioning');
                setTimeout(function() {
                    row.removeClass('hidden');
                    setTimeout(function() {
                        row.removeClass('transitioning');
                    }, 400);
                }, 0);
            }
        }

        public prevalidateTypeUnit = function() {
            var self = <FormValidator>this;

            var unitIndex: number = self.options.usage && self.options.usage.unit.value;
            var unitConfig: TubePins;
            var type: TubeType = self.options.tube.type.value;
            var unitsCount = type.sym ? 0 : type.cfg.length;

            if (!isNaN(unitIndex)) {
                unitConfig = type.cfg[unitIndex];
            }

            if (unitsCount > 1) {
                self.$._unitSelector.show();
                self.$._unitSelector.find('#units').children().each(function(index, elem) {
                    if (index >= unitsCount) {
                        $(elem).hide();
                    } else {
                        $(elem).show();
                    }
                })
            } else {
                self.$._unitSelector.hide();
                if (isNaN(unitIndex) && self.options.usage) {
                    self.options.usage.unit.value = 0;
                }
            }

            self.$._dataPins.each(function(index: number, element: Element) {
                var $element = $(element);
                var dataPin = $element.attr('data-pin');
                if (dataPin) {
                    if (!unitConfig || unitConfig.pins.indexOf(dataPin) >= 0) {
                        self.showRow($element);
                    } else {
                        self.hideRow($element);
                    }
                }
            });

            return unitIndex;
        }

        public prevalidateSaveButton = function() {
            var self = <FormValidator>this;

            if (self._saveTimer) {
                clearTimeout(self._saveTimer);
            }

            self._saveTimer = setTimeout(function() {
                self._saveTimer = 0;
                if (self.options.usage.isModified()) {
                    self.$._body.addClass('unsaved');
                } else {
                    self.$._body.removeClass('unsaved');
                }
            }, 300);
        }

        public updateZoom(zoomData: ZoomData) {
            var self = <FormValidator>this;
            var usage = self.options.usage;            
            usage.zoom.value = zoomData;
            self.prevalidateSaveButton();
        }
        
        public updateSeriesVisibility(series: {[vg1: string] : boolean }) {
            var self = <FormValidator>this;
            var usage = self.options.usage;            
            usage.series.value = series;
            self.prevalidateSaveButton();            
        }

        public prevalidateAll = function() {
            var self = <FormValidator>this;
            if (!self.options.usage) {
                return self;
            }

            self.prevalidateTypeUnit();
            self.prevalidateSaveButton();
            
            // Data validation for dirty fields
            self.hideValidationErrors();
            var ve = self.options.usage.validate();
            if (ve.length) {
                for (var i = 0; i < ve.length; i++) {
                    var field = self.options.usage.getField(ve[i].fieldName);
                    if (self.options.usage.isDirtyField(field)) {
                        self.showValidationsError(ve[i]);
                    }
                }
            }

            return self;
        }

        public validateAll = function(cb: (verrors?: Array<validation.Message>) => void) {
            var self = <FormValidator>this;
            var verrors: Array<validation.Message> = [];

            var done = function(verrors?: Array<validation.Message>) {
                if (verrors && verrors.length) {
                    $.translator.translateMessages(verrors, function(translated) {
                        self.showValidationErrors(translated);
                        cb(translated);
                    })
                } else {
                    cb();
                }
            }

            var mergeErrors = function(errors: Array<validation.Message>) {
                errors.map(err => verrors.push(err));
            }
            
            // Field generic validation
            mergeErrors(self.options.usage.validate());

            done(verrors);
        }

        public validateField = function(data: DataProviding.DataFieldEventData) {
            var self = <FormValidator>this;

            var ve = self.options.usage.validateField(data.field, data.fieldName);
            if (ve.length) {
                self.showValidationErrors(ve);
            } else {
                self.hideValidationError(data.fieldName);
            }

            self.prevalidateSaveButton();
        }
    }
}

$.widget("tct.formvalidator", $.modern.formvalidator, new tct.usages.FormValidator());

interface JQuery {
    formvalidator(options: tct.usages.FormValidatorOptions): JQuery;
}
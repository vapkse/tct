'use strict';

module tct.usages {
    require(['tube'], function(app: any) {
        (function($: JQueryStatic) {
            $.StartScreen = function() {
                var settingsPanel: JQuery;
                var docSelector: JQuery;
                var selectorOptions: DocSelectorOptions;
                var pageId = $('[translate]').attr('translate');
                var graphContainer = $('#traces');
                var body = $('body');
                var translations = $('translations');
                var chart: AnodeTransfertChart;
                var redrawTimer: number;
                var tracesContainer = $('#tracesContainer');
                var infoPanel: AnodeChartInfos;

                window.onbeforeunload = function() {
                    if (body.is('.unsaved')) {
                        return translations.find('[uid="msg-leave"]').text();
                    }
                }
                
                // Translation area visible only after the user control is ready
                var transArea = $('#trans-area');
                var translatorControl: modern.TranslatorControl = transArea.find('#translator').translatorControl().on('translatorcontrolchange', function() {
                    setTimeout(function() {
                        if (chart) {
                            chart.loadChartTitles();
                        }
                    }, 100)
                }).data('modern-translatorControl');

                var messagerControl: modern.Messager = $('#messager').messager().data('modern-messager');

                var tooltipManager: modern.TooltipManager = $('#container').tooltipmanager().data('modern-tooltipmanager');

                var $dataForm = $('[data-form]');

                var validatorControl: tct.usages.FormValidator = $('#validator-panel').formvalidator({
                    container: $dataForm
                }).data('tct-formvalidator');

                var refreshInfoPanel = function() {
                    var options = {
                        infos: chart.getInfos(),
                        usage: dataForm.dataset() as TubeUsage.TubeUsage
                    }

                    if (!infoPanel) {
                        requirejs(['anodeChartInfos'], function() {
                            infoPanel = $('#infoPanel').anodeChartInfos(options).data('tct-anodeChartInfos');
                        })
                    } else {
                        infoPanel.setOptions(options);
                    }
                }

                var dataForm = $dataForm.dataform({}).on('dataformchange', function(e: JQueryEventObject, data: DataProviding.DataFieldEventData) {
                    validatorControl.validateField(data);

                    var redraw = function() {
                        if (redrawTimer) {
                            clearTimeout(redrawTimer);
                        }
                        redrawTimer = setTimeout(function() {
                            redrawTimer = undefined;
                            chart.updateVinLines();
                            chart.redraw(true);
                            refreshInfoPanel();
                        }, 500);
                    }

                    if (chart) {
                        switch (data.fieldName) {
                            case 'unit':
                                chart.updatePmax();
                            // Continue, not break                            
                            case 'va':
                            case 'iazero':
                            case 'load':
                            case 'mode':
                                chart.updateWorkingPoint();
                                chart.updateLoadLines();
                                chart.redraw(false);
                            // Continue, not break      
                            case 'vinpp':
                                redraw();
                                break;
                        }
                    }

                }).on('dataformload', function(e: DataProviding.DataFormEvent) {
                    validatorControl.prevalidateAll();
                    $.waitPanel.hide();
                }).on('dataformfocus', function(e: JQueryEventObject, data: DataProviding.DataFieldEventData) {
                    validatorControl.hideValidationError(data.fieldName);
                    messagerControl.hideMessage(data.fieldName);
                }).data('data-providing-dataform') as DataProviding.DataForm;

                $('button[unsaved]').on('click', function() {
                    validatorControl.validateAll(function(verrors: Array<validation.Message>) {
                        if (verrors && verrors.length) {
                            messagerControl.showMessages(verrors);
                        } else if (dataForm.dataset().isModified()) {
                            $.waitPanel.show();
                            var tubeUsage = dataForm.dataset() as TubeUsage.TubeUsage;
                            var json = tubeUsage.toJson();
                            var qjson = JSON.stringify(json);
                            var reqstring: Array<string> = ['d=saveusage'];
                            var qstring = location.query();
                            reqstring.push('id=' + qstring['id'])
                            if (!tubeUsage.isNew) {
                                reqstring.push('usageid=' + qstring['usageid'])
                            } else {
                                var userid = qstring['userid'];
                                if (userid) {
                                    reqstring.push('userid=' + encodeURIComponent(userid));
                                }
                            }
                            reqstring.push('j=' + encodeURIComponent(qjson));
                            var ajaxRequest = $.ajax({
                                url: '/editor?' + reqstring.join('&'),
                                type: 'post',
                                timeout: 180000,
                                success: function(json) {
                                    if (json.error) {
                                        messagerControl.show('error', json.error);
                                    } else {
                                        var saveUsageResult = json as SaveUsageResult
                                        $.pageState.replaceOrAddState(json);
                                        
                                        // Update url params
                                        $.pageState.replaceUrlParams({
                                            id: String(saveUsageResult.tubeData._id),
                                            usageid: saveUsageResult.usageId,
                                            userid: ''
                                        });
                                        
                                        // var tube = new app.Tube(saveUsageResult) as tct.Tube;
                                        // loadTube(tube);
                                        dataForm.dataset().resetOriginalValues();
                                        validatorControl.prevalidateAll();
                                        var message = {
                                            autoCloseDelay: 15,
                                            name: 'datassaved',
                                            message: 'Your datas was saved successfully.',
                                            type: 'ok'
                                        } as translator.Message
                                        $.translator.translateMessages([message], function(translated) {
                                            messagerControl.showMessages(translated);
                                        })
                                    }
                                    $.waitPanel.hide();
                                },
                                error: function(e) {
                                    if (e.statusText !== 'canceled') {
                                        if (/^[\[|\{](\s|.*|\w)*[\]|\}]$/.test(e.responseText)) {
                                            // Validation error
                                            var verror = JSON.parse(e.responseText);
                                            validatorControl.showValidationsError(verror);
                                            messagerControl.showMessage(verror);
                                        } else {
                                            messagerControl.show('error', e);
                                        }
                                    }
                                    $.waitPanel.hide();
                                },
                            });
                        }
                    })
                });
            
                // User infos panel
                var huser = $('#user').html();
                var user: user.User = huser && JSON.parse(huser);
                var userInfosPanel = body.find('#user-infos-panel').userInfos({
                    user: user
                }).on('userinfoscreated', function() {
                    transArea.animate({ opacity: 1 }, 500);
                });

                $('button[home]').on('click', function() {
                    var searchparams = location.query()['rl'];
                    location.href = '/' + (searchparams ? searchparams : '');
                })
		                
                // Bind settings panel button
                $('button[settings-panel]').on('click', function() {
                    // Instianciate settings panel if the button is pressed for the first time
                    if (!settingsPanel) {
                        requirejs(['settingsPanel'], function(settingsPanel: JQuery) {
                            settingsPanel = $('#settings-panel');
                            var settings = new tct.Settings(localStorage);
                            settingsPanel.settingsPanel(settings).bind('settingspanelchange', function(e: tct.SettingsPanelEvent, data: tct.SettingsPanelOptions) {
                                if (data.colorSettings.scheme) {
                                    body.attr('id', 'scheme-' + data.colorSettings.scheme);
                                }
                                if (data.colorSettings.theme) {
                                    body.attr('theme', data.colorSettings.theme);
                                }
                            });
                            settingsPanel.settingsPanel('toogle');
                        })
                    } else {
                        settingsPanel.settingsPanel('toogle');
                    }
                });

                $('#edit').on('click', function() {
                    var queryString = location.query();
                    var reqstring: Array<string> = [];
                    var userid = queryString['userid'];
                    reqstring.push('id=' + queryString['id']);
                    if (userid) {
                        reqstring.push('userid=' + queryString['userid']);
                    }
                    reqstring.push('rl=' + encodeURIComponent(queryString['rl']));
                    location.href = '/editor?' + reqstring.join('&');
                })

                $('#file-selector').on('click', function() {
                    var usage = dataForm.dataset() as TubeUsage.TubeUsage;
                    selectorOptions.selectedIds = usage.traces.value;

                    if (!docSelector) {
                        requirejs(['docSelector'], function() {
                            docSelector = $('[doc-selector]');
                            docSelector.docSelector(selectorOptions).on('docselectorcreated', function() {
                                docSelector.docSelector('show');
                            }).on('docselectorokclicked', function(e: DocSelectorEvent) {
                                var usage = dataForm.dataset() as TubeUsage.TubeUsage;
                                usage.traces.value = e.selectedIds.length && e.selectedIds[0];
                                dataForm.refresh();
                                updateGraphs();
                                validatorControl.prevalidateAll();
                            });
                        })
                    } else {
                        docSelector.docSelector('setOptions', selectorOptions);
                    }
                })

                var updateGraphs = function() {
                    var usage = dataForm.dataset() as TubeUsage.TubeUsage;
                    var trace = usage.traces.value;

                    // Remove graphs
                    graphContainer.find('.anode-transfert-chart').remove();
                    var tube = validatorControl.options.tube;
                    var doc = tube.getDocumentById(trace);
                    usage.zoom.value = {};
                    if (doc) {
                        loadGraph(doc);
                    }
                }

                var loadGraph = function(doc: Document) {
                    var usage = dataForm.dataset() as TubeUsage.TubeUsage;
                    var refreshTimeout = window.browserVersion().browser === 'firefox' ? 250 : 1;
                    var refreshTimer: number;

                    requirejs(['anodeChartTransfert', 'ajaxDownloadFile'], function() {
                        var options: utils.AjaxDownloadFileOptions = {
                            url: '/download?url=' + doc.filename.value,
                            dataType: 'json'
                        }

                        // Calc pmax
                        var unitIndex = usage.unit.value;
                        var pmax = 0;
                        if (unitIndex !== undefined) {
                            var tube = validatorControl.options.tube;
                            pmax = tube.units[unitIndex].pamax.value;
                        }

                        utils.AjaxDownloadFile.download(options).onSuccessFinish(function(status, statusText, res, headers) {
                            var json = res['json'];
                            var result: UploadResultFile = typeof json ==='string' ? JSON.parse(json) : json;
                            if (result.error) {
                                messagerControl.showMessage(result.error);
                            } else {
                                var options: tct.AnodeTransfertChartOptions = {
                                    element: $('<div class="anode-transfert-chart"></div>').attr('id', doc.getField('_id').value),
                                    tubeGraph: result.tubeGraph,
                                    pmax: pmax,
                                    usage: usage,
                                    title: doc.description.value,
                                    editable: true,
                                    height: function(chart: JQuery) {
                                        var width = chart.width();
                                        return Math.max(400, width / 2);
                                    },
                                    optionschanged: function() {
                                        if (refreshTimer) {
                                            clearTimeout(refreshTimer);
                                        }
                                        refreshTimer = setTimeout(function() {
                                            refreshTimer = 0;
                                            dataForm.refresh();
                                            refreshInfoPanel();
                                            validatorControl.prevalidateAll();
                                        }, refreshTimeout);
                                    },
                                    ready: function() {
                                        refreshInfoPanel();
                                    }
                                }

                                chart = new tct.AnodeTransfertChart(options)
                                graphContainer.append(options.element);
                            }
                        }).onErrorFinish(function(status, statusText, res, headers) {
                            messagerControl.show('error', statusText);
                        });
                    })
                }

                var loadGraphs = function(tube?: tct.Tube, usage?: TubeUsage.TubeUsage) {
                    graphContainer.empty();
                    if (tube && usage && usage.traces && usage.traces.value) {
                        var doc = tube.getDocumentById(usage.traces.value);
                        if (doc) {
                            loadGraph(doc);
                        }
                    }
                }

                var loadDataset = function(tube?: tct.Tube, usage?: TubeUsage.TubeUsage) {
                    dataForm.dataset(usage);
                    if (usage) {
                        validatorControl.setOptions({
                            tube: tube,
                            usage: usage
                        });

                        loadGraphs(tube, usage);
                    }
                }

                var loadTube = function(tube?: tct.Tube) {
                    var createNewUsage = function() {
                        var defaultUnit = parseInt(queryString['unit']);
                        usage = tube.createArrayFieldSubset('usage', {
                            unit: !isNaN(defaultUnit) ? defaultUnit : undefined,
                        }) as TubeUsage.TubeUsage;
                        usage.isNew = true;
                        return usage;
                    }
                    var queryString = location.query();
                    var usageid = queryString['usageid'];
                    var userid = queryString['userid'];
                    var usage: TubeUsage.TubeUsage;
                    if (!usageid || usageid === 'new') {
                        usage = createNewUsage();
                        if (queryString['docid']) {
                            usage.traces.value = queryString['docid'];
                        }

                    } else {
                        var usages = tube.usages.value as Array<TubeUsage.TubeUsage>;
                        var selectedUsages = usages.filter(u => u._id === usageid)
                        if (selectedUsages.length === 0) {
                            createNewUsage();
                        } else {
                            usage = selectedUsages[0];
                            if (userid && userid !== (user && user._id)) {
                                usage.isNew = true;
                            }
                        }
                    }

                    // Load labels
                    $dataForm.find('#name').text(tube.getField('name').value);                                      

                    // Doc selector options
                    selectorOptions = {
                        tubeData: tube.toJson(),
                        title: translations.find('#seltitle').prop('innerHTML'),
                        mode: 'radio',
                        filter: new RegExp('^.+\.tds$'),
                        showUsages: false
                    };

                    loadDataset(tube, usage);
                }

                var loadFromUrl = function() {
                    $.waitPanel.show();
                    var queryString = location.query();
                    var tubeid = queryString['id'];
                    var userid = queryString['userid'];
                    var reqstring: Array<string> = [];
                    reqstring.push('id=' + tubeid);
                    if (userid) {
                        reqstring.push('userid=' + userid);
                    }

                    var ajaxRequest = $.ajax({
                        url: '/editor?' + reqstring.join('&'),
                        type: 'post',
                        dataType: 'json',
                        timeout: 60000,
                        success: function(json) {
                            if (json.error) {
                                messagerControl.show('error', json.error);
                            } else {
                                // Load tube class from the received TubeData json
                                var tube = new app.Tube(json) as tct.Tube;

                                loadTube(tube);

                                $.pageState.replaceOrAddState(json);
                                $.waitPanel.hide();
                            }
                        },
                        error: function(e) {
                            if (e.statusText !== 'canceled') {
                                messagerControl.show('error', e);
                                loadDataset();
                            }
                            $.waitPanel.hide();
                        },
                    });
                }            

                // Ask server for datas
                setTimeout(function() {
                    loadFromUrl();
                }, 1);

                $(window).on('popstate', function(e) {
                    var event = <PopStateEvent>e.originalEvent;
                    if (event && event.state && event.state.j) {
                        var tube = new app.Tube(event.state.j) as tct.Tube;
                        loadTube(tube);
                    } else {
                        // Reload datas from url
                        loadFromUrl();
                    }
                });

                $('#test').on('click', function(e) {
                    chart.drawCrossPoints(true);
                })
            };
        })(jQuery);

        $(function() {
            if (location.hostname === '127.0.0.1') {
                var browserVersion = window.browserVersion();
                if (browserVersion.version > 9) {
                    $('head').append('<script type="text/javascript" src="http://127.0.0.1:38080/livereload.js"><\/script>');
                }
            }

            $.StartScreen();
        });
    })
}

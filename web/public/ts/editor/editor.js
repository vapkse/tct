'use strict';
var tct;
(function (tct) {
    var editor;
    (function (editor) {
        require(['tube'], function (app) {
            (function ($) {
                $.StartScreen = function () {
                    var settingsPanel;
                    var docViewer;
                    var pageId = $('[translate]').attr('translate');
                    var body = $('body').attr('ismobile', window.mobilecheck().toString());
                    var translations = $('translations');
                    if (location.query()['merge']) {
                        body.addClass('merge');
                    }
                    window.onbeforeunload = function () {
                        if (body.is('.unsaved')) {
                            return translations.find('[uid="msg-leave"]').text();
                        }
                    };
                    var transArea = $('#trans-area');
                    var translatorControl = transArea.find('#translator').translatorControl().data('modern-translatorControl');
                    var messagerControl = $('#messager').messager().data('modern-messager');
                    var tooltipManager = $('#container').tooltipmanager().data('modern-tooltipmanager');
                    var $dataForm = $('[data-form]');
                    $dataForm.find('#create-graph').on('click', function () {
                        var qstring = location.query();
                        var tubeid = qstring['id'];
                        location.href = '/creator?id=' + tubeid + '&rl=' + encodeURIComponent(qstring['rl'] || location.search);
                    });
                    var $docsContainer = $dataForm.find('[data-container="documents"]').on('click', function (e) {
                        var clickInfo = $(e.target).closest('[data-click]').attr('data-click');
                        var row = $(e.target).closest('[data-index]');
                        var dataIndex = parseInt(row.attr('data-index'));
                        var docs = dataForm.dataset().getField('documents').value;
                        switch (clickInfo) {
                            case 'delete':
                                setTimeout(function () {
                                    row.css('overflow', 'hidden');
                                    row.animate({ height: 0, opacity: 0, marginTop: 0 }, 300, function () {
                                        row.remove();
                                        docs.splice(dataIndex, 1);
                                        $docsContainer.datacontainer('refresh');
                                        validatorControl.prevalidateFileControls();
                                        validatorControl.prevalidateSaveButton();
                                    });
                                }, 1);
                                break;
                            case 'download':
                                var doc = docs[dataIndex];
                                var docViewerOptions = {
                                    document: {
                                        _id: doc._id,
                                        filename: doc.filename.value,
                                        description: doc.description.value
                                    },
                                    tubeData: dataForm.dataset().toJson(),
                                    isNew: doc.isNew,
                                    error: function (error) {
                                        messagerControl.show('error', error);
                                    }
                                };
                                if (!docViewer) {
                                    requirejs(['docViewer'], function () {
                                        docViewer = new tct.DocViewer();
                                        docViewer.view(docViewerOptions);
                                    });
                                }
                                else {
                                    docViewer.view(docViewerOptions);
                                }
                                break;
                        }
                    });
                    var $usagesContainer = $('[data-container="usages"]').on('click', function (e) {
                        var clickInfo = $(e.target).closest('[data-click]').attr('data-click');
                        var row = $(e.target).closest('[data-index]');
                        var dataIndex = parseInt(row.attr('data-index'));
                        var usages = dataForm.dataset().getField('usages').value;
                        switch (clickInfo) {
                            case 'u-edit':
                                navigateToUsageEditor(usages[dataIndex]);
                                break;
                            case 'u-delete':
                                setTimeout(function () {
                                    row.css('overflow', 'hidden');
                                    row.animate({ height: 0, opacity: 0, marginTop: 0 }, 300, function () {
                                        row.remove();
                                        usages.splice(dataIndex, 1);
                                        $usagesContainer.datacontainer('refresh');
                                        validatorControl.prevalidateSaveButton();
                                    });
                                }, 1);
                                break;
                        }
                    });
                    var tubesTypes;
                    var tubesTypesLookupLoaded = false;
                    var tubesTypesCallback;
                    $.get('/resource?c=ttypes').done(function (json) {
                        tubesTypes = json;
                        if (tubesTypesCallback) {
                            tubesTypesCallback({ items: tubesTypes });
                            tubesTypesLookupLoaded = true;
                        }
                    }).fail(function (e) {
                        messagerControl.show('error', e);
                    });
                    var loadLookup = function (url, callback) {
                        $.get(url).done(function (json) {
                            callback({ items: json });
                        }).fail(function (e) {
                            messagerControl.show('error', e);
                        });
                    };
                    var validatorControl = $('#validator-panel').formvalidator({
                        container: $dataForm
                    }).data('tct-formvalidator');
                    var dataForm = $dataForm.dataform({}).on('dataformchange', function (e, data) {
                        validatorControl.validateField(data);
                    }).on('dataformload', function (e) {
                        validatorControl.prevalidateAll();
                        $.waitPanel.hide();
                    }).on('dataformlookuprequired', function (e, data) {
                        if (data.fieldName === "type" && !tubesTypesLookupLoaded) {
                            if (tubesTypes) {
                                data.callback({ items: tubesTypes });
                                tubesTypesLookupLoaded = true;
                                return true;
                            }
                            else {
                                tubesTypesCallback = data.callback;
                                return false;
                            }
                        }
                        else if (data.field.lookupurl) {
                            loadLookup(data.field.lookupurl, data.callback);
                            return false;
                        }
                    }).on('dataformfocus', function (e, data) {
                        validatorControl.hideValidationError(data.fieldName);
                        messagerControl.hideMessage(data.fieldName);
                    }).data('data-providing-dataform');
                    $('button[unsaved]').on('click', function () {
                        validatorControl.validateAll(function (verrors) {
                            var ds = dataForm.dataset();
                            if (verrors && verrors.length) {
                                messagerControl.showMessages(verrors);
                            }
                            else if (ds.isModified()) {
                                $.waitPanel.show();
                                var returnParams;
                                var json = ds.toJson();
                                var qjson = JSON.stringify(json);
                                var qstring = location.query();
                                var reqstring = [];
                                var mergeUserId = qstring['merge'];
                                var url = mergeUserId ? '/merge?' : '/editor?';
                                reqstring.push('d=save');
                                if (mergeUserId) {
                                    reqstring.push('userid=' + mergeUserId);
                                }
                                else if (ds.isNew) {
                                    returnParams = '?q=' + encodeURIComponent('name=' + ds.name.value);
                                }
                                else {
                                    var userid = qstring['userid'];
                                    if (userid) {
                                        reqstring.push('userid=' + encodeURIComponent(userid));
                                    }
                                }
                                reqstring.push('j=' + encodeURIComponent(qjson));
                                var ajaxRequest = $.ajax({
                                    url: url + reqstring.join('&'),
                                    type: 'post',
                                    timeout: 60000,
                                    success: function (json) {
                                        if (json.error) {
                                            messagerControl.show('error', json.error);
                                        }
                                        else {
                                            var tubeSearchResult = json;
                                            var tube = new app.Tube(tubeSearchResult);
                                            dataForm.dataset().resetOriginalValues();
                                            loadTube(tube);
                                            $.pageState.replaceOrAddState(tubeSearchResult);
                                            var replaceParams = {
                                                id: tubeSearchResult.tubeData._id,
                                                userid: ''
                                            };
                                            if (returnParams) {
                                                replaceParams['rl'] = encodeURIComponent(returnParams);
                                            }
                                            else {
                                                var message = {
                                                    autoCloseDelay: 15,
                                                    name: 'datassaved',
                                                    message: 'Your datas was saved successfully.',
                                                    type: 'ok'
                                                };
                                                $.translator.translateMessages([message], function (translated) {
                                                    messagerControl.showMessages(translated);
                                                });
                                            }
                                            $.pageState.replaceUrlParams(replaceParams);
                                        }
                                        $.waitPanel.hide();
                                    },
                                    error: function (e) {
                                        if (e.statusText !== 'canceled') {
                                            if (/^[\[|\{](\s|.*|\w)*[\]|\}]$/.test(e.responseText)) {
                                                var verror = JSON.parse(e.responseText);
                                                validatorControl.showValidationsError(verror);
                                                messagerControl.showMessage(verror);
                                            }
                                            else {
                                                messagerControl.show('error', e);
                                            }
                                        }
                                        $.waitPanel.hide();
                                    },
                                });
                            }
                        });
                    });
                    var huser = $('#user').html();
                    var user = huser && JSON.parse(huser);
                    var userInfosPanel = body.find('#user-infos-panel').userInfos({
                        user: user
                    }).on('userinfoscreated', function () {
                        transArea.animate({ opacity: 1 }, 500);
                    });
                    $('button[home]').on('click', function () {
                        var searchparams = location.query()['rl'];
                        location.href = '/' + (searchparams ? searchparams : '');
                    });
                    $('button[settings-panel]').on('click', function () {
                        if (!settingsPanel) {
                            requirejs(['settingsPanel'], function (settingsPanel) {
                                settingsPanel = $('#settings-panel');
                                var settings = new tct.Settings(localStorage);
                                settingsPanel.settingsPanel(settings).bind('settingspanelchange', function (e, data) {
                                    if (data.colorSettings.scheme) {
                                        body.attr('id', 'scheme-' + data.colorSettings.scheme);
                                    }
                                    if (data.colorSettings.theme) {
                                        body.attr('theme', data.colorSettings.theme);
                                    }
                                });
                                settingsPanel.settingsPanel('toogle');
                            });
                        }
                        else {
                            settingsPanel.settingsPanel('toogle');
                        }
                    });
                    $('#baseview').on('click', function () {
                        validatorControl.viewBase();
                    });
                    $('#pinoutview').on('click', function () {
                        validatorControl.viewPinout();
                    });
                    var unittab = $('.unittab');
                    var unitIndexContainer = $('[data-container="unit"]');
                    var tabItems = unittab.find('a').on('focus', function (e) {
                        var $element = $(e.target);
                        $element.addClass('fg-theme');
                    }).on('blur', function (e) {
                        var $element = $(e.target);
                        $element.removeClass('fg-theme');
                    }).on('click', function (e) {
                        var li = $(e.target).closest('li');
                        var unitIndex = li.index();
                        validatorControl.prevalidateTypeUnit(unitIndex);
                        e.stopPropagation();
                        return false;
                    });
                    unitIndexContainer.attr('data-index', location.query()['unit'] || '0');
                    var uploader = $('form#upload').show();
                    var preloader = $('#preloader').hide();
                    var progressLabel = preloader.find('#progress');
                    var uploadDone = function () {
                        preloader.hide();
                        uploader.show();
                    };
                    var navigateToUsageEditor = function (usage) {
                        var qstring = location.query();
                        var reqstring = [];
                        reqstring.push('id=' + qstring['id']);
                        reqstring.push('rl=' + encodeURIComponent(qstring['rl']));
                        if (usage) {
                            reqstring.push('usageid=' + usage._id);
                        }
                        else {
                            reqstring.push('unit=' + qstring['unit']);
                            reqstring.push('usageid=new');
                        }
                        var userid = qstring['userid'];
                        if (userid) {
                            reqstring.push('userid=' + encodeURIComponent(userid));
                        }
                        location.href = '/usages?' + reqstring.join('&');
                    };
                    var newUsage = $('#new-usage').on('click', function () {
                        navigateToUsageEditor();
                    });
                    $('.btn-file :file').on('change', function () {
                        var input = $(this);
                        var files = input.prop('files');
                        if (files.length) {
                            uploader.hide();
                            preloader.show();
                            progressLabel.text('');
                            var formElement = (uploader[0]);
                            var formData = new FormData(formElement);
                            $.ajax({
                                url: '/document?d=upload',
                                type: 'POST',
                                timeout: 1800000,
                                data: formData,
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function (result) {
                                    uploadDone();
                                    input.val('');
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    uploadDone();
                                    messagerControl.show('error', (jqXHR && (jqXHR.responseText || jqXHR.statusText)) || errorThrown);
                                    input.val('');
                                },
                                xhr: function () {
                                    var xhr = new XMLHttpRequest();
                                    xhr.upload.addEventListener("progress", function (evt) {
                                        if (evt.lengthComputable) {
                                            var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                                            progressLabel.text(percentComplete + '%');
                                        }
                                    }, false);
                                    xhr.addEventListener("progress", function (evt) {
                                        if (evt.lengthComputable) {
                                            var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                                            progressLabel.text(percentComplete + '%');
                                        }
                                    }, false);
                                    return xhr;
                                }
                            });
                        }
                    });
                    var registerForPush = function (user, tube) {
                        var refreshTimer;
                        var refresh = function () {
                            if (refreshTimer) {
                                clearTimeout(refreshTimer);
                            }
                            refreshTimer = setTimeout(function () {
                                refreshTimer = undefined;
                                $docsContainer.datacontainer('refresh');
                                validatorControl.prevalidateFileControls();
                            }, 500);
                        };
                        var io = $.io.connect({
                            query: {
                                event: 'uploadResult#' + user.userName
                            }
                        });
                        io.on('uploadResult', function (result) {
                            if (result.error) {
                                messagerControl.show('error', result.error);
                            }
                            else {
                                var fileName = result.filename;
                                var fileInfo = /(^.+)\.([^.]+)$/.exec(fileName);
                                var docData = {
                                    description: fileInfo.length > 1 ? fileInfo[1] : '',
                                    filename: fileName
                                };
                                var docsField = dataForm.dataset().getField('documents');
                                var docs = docsField.value;
                                var doc = dataForm.dataset().createArrayFieldSubset('document', docData);
                                doc.isNew = true;
                                docs.push(doc);
                                var msg = {
                                    type: 'info',
                                    name: 'msg-fileuploaded',
                                    message: 'File \\0 Uploaded',
                                    autoCloseDelay: 6,
                                    params: { 0: fileName }
                                };
                                $.translator.translateMessages([msg], function (translated) {
                                    messagerControl.showMessages(translated);
                                });
                                refresh();
                            }
                        });
                    };
                    var loadTube = function (tube) {
                        dataForm.dataset(tube);
                        if (tube) {
                            validatorControl.tube(tube);
                            validatorControl.prevalidateAll();
                        }
                        if (tube.isNew) {
                            newUsage.attr('disabled', 'disabled');
                        }
                        else {
                            newUsage.removeAttr('disabled');
                        }
                    };
                    var loadFromUrl = function () {
                        $.waitPanel.show();
                        var qstring = location.query();
                        var tubeid = qstring['id'];
                        var reqstring = [];
                        var mergeUserId = qstring['merge'];
                        var url = mergeUserId ? '/merge?' : '/editor?';
                        if (mergeUserId) {
                            reqstring.push('id=' + tubeid);
                            reqstring.push('userid=' + mergeUserId);
                        }
                        else {
                            var userid = qstring['userid'];
                            if (userid) {
                                reqstring.push('userid=' + userid);
                            }
                            if (tubeid) {
                                reqstring.push('id=' + tubeid);
                            }
                            else {
                                reqstring.push('d=new');
                            }
                        }
                        var ajaxRequest = $.ajax({
                            url: url + reqstring.join('&'),
                            type: 'post',
                            dataType: 'json',
                            timeout: 60000,
                            success: function (json) {
                                if (json.error) {
                                    messagerControl.show('error', json.error);
                                }
                                else {
                                    var tube = new app.Tube(json);
                                    if (mergeUserId) {
                                        tube.isNew = true;
                                    }
                                    loadTube(tube);
                                    registerForPush(user, tube);
                                    $.pageState.replaceOrAddState(json);
                                    $.waitPanel.hide();
                                }
                            },
                            error: function (e) {
                                if (e.statusText !== 'canceled') {
                                    messagerControl.show('error', e);
                                    loadTube(null);
                                }
                                $.waitPanel.hide();
                            },
                        });
                    };
                    setTimeout(function () {
                        loadFromUrl();
                    }, 1);
                    $(window).on('popstate', function (e) {
                        var event = e.originalEvent;
                        if (event && event.state && event.state.j) {
                            unitIndexContainer.attr('data-index', location.query()['unit'] || '0');
                            var tube = new app.Tube(event.state.j);
                            loadTube(tube);
                        }
                        else {
                            loadFromUrl();
                        }
                    });
                };
            })(jQuery);
            $(function () {
                if (location.hostname === '127.0.0.1') {
                    var browserVersion = window.browserVersion();
                    if (browserVersion.version > 9) {
                        $('head').append('<script type="text/javascript" src="http://127.0.0.1:38080/livereload.js"><\/script>');
                    }
                }
                $.StartScreen();
            });
        });
    })(editor = tct.editor || (tct.editor = {}));
})(tct || (tct = {}));

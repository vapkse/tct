'use strict';

module tct.editor {
    require(['tube'], function(app: any) {
        (function($: JQueryStatic) {
            $.StartScreen = function() {
                var settingsPanel: JQuery;
                var docViewer: tct.DocViewer;
                var pageId = $('[translate]').attr('translate');
                var body = $('body').attr('ismobile', window.mobilecheck().toString());
                var translations = $('translations');

                if (location.query()['merge']) {
                    body.addClass('merge');
                }

                window.onbeforeunload = function() {
                    if (body.is('.unsaved')) {
                        return translations.find('[uid="msg-leave"]').text();
                    }
                }

                // Translation area visible only after the user control is ready
                var transArea = $('#trans-area');
                var translatorControl: modern.TranslatorControl = transArea.find('#translator').translatorControl().data('modern-translatorControl');
                var messagerControl: modern.Messager = $('#messager').messager().data('modern-messager');
                var tooltipManager: modern.TooltipManager = $('#container').tooltipmanager().data('modern-tooltipmanager');

                var $dataForm = $('[data-form]');
                $dataForm.find('#create-graph').on('click', function() {
                    var qstring = location.query();
                    var tubeid = qstring['id'];
                    location.href = '/creator?id=' + tubeid + '&rl=' + encodeURIComponent(qstring['rl'] || location.search);
                })

                var $docsContainer = $dataForm.find('[data-container="documents"]').on('click', function(e: JQueryEventObject) {
                    // Get corresponding file field
                    var clickInfo = $(e.target).closest('[data-click]').attr('data-click');
                    var row = $(e.target).closest('[data-index]');
                    var dataIndex = parseInt(row.attr('data-index'));
                    var docs = dataForm.dataset().getField('documents').value as Array<Document>;

                    switch (clickInfo) {
                        case 'delete':
                            setTimeout(function() {
                                // Get right ds
                                row.css('overflow', 'hidden');
                                row.animate({ height: 0, opacity: 0, marginTop: 0 }, 300, function() {
                                    row.remove();
                                    // Remove corresponding index from the field
                                    docs.splice(dataIndex, 1);
                                    // Rebind container to update the index
                                    $docsContainer.datacontainer('refresh');
                                    validatorControl.prevalidateFileControls();
                                    validatorControl.prevalidateSaveButton();
                                })
                            }, 1);
                            break;

                        case 'download':
                            // Get right ds
                            var doc = docs[dataIndex];
                            var docViewerOptions: tct.DocViewerOptions = {
                                document: {
                                    _id: doc._id,
                                    filename: doc.filename.value,
                                    description: doc.description.value
                                } as TubeDoc,
                                tubeData: dataForm.dataset().toJson(),
                                isNew: doc.isNew,
                                error: function(error) {
                                    messagerControl.show('error', error);
                                }
                            }
                            if (!docViewer) {
                                requirejs(['docViewer'], function() {
                                    docViewer = new tct.DocViewer();
                                    docViewer.view(docViewerOptions);
                                })
                            } else {
                                docViewer.view(docViewerOptions);
                            }
                            break;
                    }
                });

                var $usagesContainer = $('[data-container="usages"]').on('click', function(e: JQueryEventObject) {
                    // Get corresponding file field
                    var clickInfo = $(e.target).closest('[data-click]').attr('data-click');
                    var row = $(e.target).closest('[data-index]');
                    var dataIndex = parseInt(row.attr('data-index'));
                    var usages = dataForm.dataset().getField('usages').value as Array<TubeUsage.TubeUsage>;

                    switch (clickInfo) {
                        case 'u-edit':
                            navigateToUsageEditor(usages[dataIndex]);
                            break;

                        case 'u-delete':
                            setTimeout(function() {
                                // Get right ds
                                row.css('overflow', 'hidden');
                                row.animate({ height: 0, opacity: 0, marginTop: 0 }, 300, function() {
                                    row.remove();
                                    // Remove corresponding index from the field
                                    usages.splice(dataIndex, 1);
                                    // Rebind container to update the index
                                    $usagesContainer.datacontainer('refresh');
                                    validatorControl.prevalidateSaveButton();
                                })
                            }, 1);
                            break;
                    }
                });

                // Get json for bases
                var tubesTypes: Array<TubeType>;
                var tubesTypesLookupLoaded = false;
                var tubesTypesCallback: (options: modern.ChosenOptions) => void;

                $.get('/resource?c=ttypes').done(function(json) {
                    tubesTypes = json;
                    if (tubesTypesCallback) {
                        tubesTypesCallback({ items: tubesTypes });
                        tubesTypesLookupLoaded = true;
                    }
                }).fail(function(e) {
                    messagerControl.show('error', e);
                });

                var loadLookup = function(url: string, callback: (options: any) => void) {
                    $.get(url).done(function(json) {
                        callback({ items: json });
                    }).fail(function(e) {
                        messagerControl.show('error', e);
                    });
                }

                var validatorControl: tct.editor.FormValidator = $('#validator-panel').formvalidator({
                    container: $dataForm
                }).data('tct-formvalidator');

                var dataForm = $dataForm.dataform({}).on('dataformchange', function(e: JQueryEventObject, data: DataProviding.DataFieldEventData) {
                    validatorControl.validateField(data);
                }).on('dataformload', function(e: DataProviding.DataFormEvent) {
                    validatorControl.prevalidateAll();
                    $.waitPanel.hide();
                }).on('dataformlookuprequired', function(e: JQueryEventObject, data: DataProviding.DataFieldRequiredEventData) {
                    if (data.fieldName === "type" && !tubesTypesLookupLoaded) {
                        if (tubesTypes) {
                            data.callback({ items: tubesTypes });
                            tubesTypesLookupLoaded = true;
                            return true;
                        } else {
                            tubesTypesCallback = data.callback;
                            return false;
                        }
                    } else if (data.field.lookupurl) {
                        loadLookup(data.field.lookupurl, data.callback);
                        return false;
                    }
                }).on('dataformfocus', function(e: JQueryEventObject, data: DataProviding.DataFieldEventData) {
                    validatorControl.hideValidationError(data.fieldName);
                    messagerControl.hideMessage(data.fieldName);
                }).data('data-providing-dataform') as DataProviding.DataForm;

                $('button[unsaved]').on('click', function() {
                    validatorControl.validateAll(function(verrors: Array<validation.Message>) {
                        var ds = dataForm.dataset() as Tube;
                        if (verrors && verrors.length) {
                            messagerControl.showMessages(verrors);
                        } else if (ds.isModified()) {
                            $.waitPanel.show();
                            var returnParams: string;
                            var json = ds.toJson();
                            var qjson = JSON.stringify(json);
                            var qstring = location.query();
                            var reqstring: Array<string> = [];
                            var mergeUserId = qstring['merge'];
                            var url = mergeUserId ? '/merge?' : '/editor?'
                            reqstring.push('d=save');
                            if (mergeUserId) {
                                reqstring.push('userid=' + mergeUserId);
                            } else if (ds.isNew) {
                                // Redefine return url in case of success
                                returnParams = '?q=' + encodeURIComponent('name=' + ds.name.value);
                            } else {
                                var userid = qstring['userid'];
                                if (userid) {
                                    reqstring.push('userid=' + encodeURIComponent(userid));
                                }
                            }
                            reqstring.push('j=' + encodeURIComponent(qjson));
                            var ajaxRequest = $.ajax({
                                url: url + reqstring.join('&'),
                                type: 'post',
                                timeout: 60000,
                                success: function(json) {
                                    if (json.error) {
                                        messagerControl.show('error', json.error);
                                    } else {
                                        var tubeSearchResult = json as TubeSearchResult
                                        var tube = new app.Tube(tubeSearchResult) as tct.Tube;
                                        dataForm.dataset().resetOriginalValues();
                                        loadTube(tube);
                                        $.pageState.replaceOrAddState(tubeSearchResult);

                                        // Update url params
                                        var replaceParams = {
                                            id: tubeSearchResult.tubeData._id,
                                            userid: ''
                                        } as { [name: string]: any }

                                        if (returnParams) {
                                            replaceParams['rl'] = encodeURIComponent(returnParams);
                                        } else {
                                            var message = {
                                                autoCloseDelay: 15,
                                                name: 'datassaved',
                                                message: 'Your datas was saved successfully.',
                                                type: 'ok'
                                            } as translator.Message
                                            $.translator.translateMessages([message], function(translated) {
                                                messagerControl.showMessages(translated);
                                            })
                                        }
                                        $.pageState.replaceUrlParams(replaceParams);
                                    }
                                    $.waitPanel.hide();
                                },
                                error: function(e) {
                                    if (e.statusText !== 'canceled') {
                                        if (/^[\[|\{](\s|.*|\w)*[\]|\}]$/.test(e.responseText)) {
                                            // Validation error
                                            var verror = JSON.parse(e.responseText);
                                            validatorControl.showValidationsError(verror);
                                            messagerControl.showMessage(verror);
                                        } else {
                                            messagerControl.show('error', e);
                                        }
                                    }
                                    $.waitPanel.hide();
                                },
                            });
                        }
                    })
                });

                // User infos panel
                var huser = $('#user').html();
                var user: user.User = huser && JSON.parse(huser);
                var userInfosPanel = body.find('#user-infos-panel').userInfos({
                    user: user
                }).on('userinfoscreated', function() {
                    transArea.animate({ opacity: 1 }, 500);
                });

                $('button[home]').on('click', function() {
                    var searchparams = location.query()['rl'];
                    location.href = '/' + (searchparams ? searchparams : '');
                })

                // Bind settings panel button
                $('button[settings-panel]').on('click', function() {
                    // Instianciate settings panel if the button is pressed for the first time
                    if (!settingsPanel) {
                        requirejs(['settingsPanel'], function(settingsPanel: JQuery) {
                            settingsPanel = $('#settings-panel');
                            var settings = new tct.Settings(localStorage);
                            settingsPanel.settingsPanel(settings).bind('settingspanelchange', function(e: tct.SettingsPanelEvent, data: tct.SettingsPanelOptions) {
                                if (data.colorSettings.scheme) {
                                    body.attr('id', 'scheme-' + data.colorSettings.scheme);
                                }
                                if (data.colorSettings.theme) {
                                    body.attr('theme', data.colorSettings.theme);
                                }
                            });
                            settingsPanel.settingsPanel('toogle');
                        })
                    } else {
                        settingsPanel.settingsPanel('toogle');
                    }
                });

                $('#baseview').on('click', function() {
                    validatorControl.viewBase();
                })

                $('#pinoutview').on('click', function() {
                    validatorControl.viewPinout();
                })

                // Unit tab control     
                var unittab = $('.unittab');
                var unitIndexContainer = $('[data-container="unit"]');
                var tabItems = unittab.find('a').on('focus', function(e: JQueryEventObject) {
                    var $element = $(e.target);
                    $element.addClass('fg-theme');
                }).on('blur', function(e: JQueryEventObject) {
                    var $element = $(e.target);
                    $element.removeClass('fg-theme');
                }).on('click', function(e: JQueryEventObject) {
                    var li = $(e.target).closest('li');
                    var unitIndex = li.index();
                    validatorControl.prevalidateTypeUnit(unitIndex);
                    e.stopPropagation();
                    return false;
                });

                // Set unit index from query string
                unitIndexContainer.attr('data-index', location.query()['unit'] || '0');

                var uploader = $('form#upload').show();
                var preloader = $('#preloader').hide();
                var progressLabel = preloader.find('#progress');

                var uploadDone = function() {
                    preloader.hide();
                    uploader.show();
                }

                var navigateToUsageEditor = function(usage?: TubeUsage.TubeUsage) {
                    var qstring = location.query();
                    var reqstring: Array<string> = [];
                    reqstring.push('id=' + qstring['id']);
                    reqstring.push('rl=' + encodeURIComponent(qstring['rl']));
                    if (usage) {
                        reqstring.push('usageid=' + usage._id);
                    } else {
                        reqstring.push('unit=' + qstring['unit']);
                        reqstring.push('usageid=new');
                    }
                    var userid = qstring['userid'];
                    if (userid) {
                        reqstring.push('userid=' + encodeURIComponent(userid));
                    }
                    location.href = '/usages?' + reqstring.join('&');
                }

                var newUsage = $('#new-usage').on('click', function() {
                    navigateToUsageEditor();
                })

                $('.btn-file :file').on('change', function() {
                    var input = $(this);
                    var files = input.prop('files');
                    if (files.length) {
                        uploader.hide();
                        preloader.show();
                        progressLabel.text('');

                        var formElement: HTMLFormElement = <HTMLFormElement>(uploader[0]);
                        var formData = new FormData(formElement);
                        $.ajax({
                            url: '/document?d=upload',
                            type: 'POST',
                            timeout: 1800000,
                            // Form data
                            data: formData,
                            //Options to tell jQuery not to process data or worry about content-type.
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(result: UploadResult) {
                                uploadDone();
                                input.val('');
                            },
                            error: function(jqXHR: JQueryXHR, textStatus: string, errorThrown: string) {
                                uploadDone();
                                messagerControl.show('error', (jqXHR && (jqXHR.responseText || jqXHR.statusText)) || errorThrown);
                                input.val('');
                            },
                            xhr: function() {
                                var xhr = new XMLHttpRequest();
                                xhr.upload.addEventListener("progress", function(evt: ProgressEvent) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                                        progressLabel.text(percentComplete + '%');
                                    }
                                }, false);

                                xhr.addEventListener("progress", function(evt: ProgressEvent) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                                        progressLabel.text(percentComplete + '%');
                                    }
                                }, false);

                                return xhr;
                            }
                        });
                    }
                });

                var registerForPush = function(user: user.User, tube: tct.Tube) {
                    var refreshTimer: number;

                    var refresh = function() {
                        if (refreshTimer) {
                            clearTimeout(refreshTimer);
                        }
                        refreshTimer = setTimeout(function() {
                            refreshTimer = undefined;
                            $docsContainer.datacontainer('refresh');
                            validatorControl.prevalidateFileControls();
                        }, 500);
                    }

                    // Socket io, register to upload result event
                    var io = $.io.connect({
                        query: {
                            event: 'uploadResult#' + user.userName
                        }
                    });
                    io.on('uploadResult', function(result: UploadResultFile) {
                        if (result.error) {
                            messagerControl.show('error', result.error);
                        } else {
                            var fileName = result.filename;
                            var fileInfo = /(^.+)\.([^.]+)$/.exec(fileName);
                            var docData: TubeDoc = {
                                description: fileInfo.length > 1 ? fileInfo[1] : '',
                                filename: fileName
                            }

                            // Get right field
                            var docsField = dataForm.dataset().getField('documents') as DataProviding.Field;
                            var docs = docsField.value as Array<Document>;

                            // Add a new file and refresh ds
                            var doc = dataForm.dataset().createArrayFieldSubset('document', docData) as Document
                            doc.isNew = true;
                            docs.push(doc);

                            var msg: modern.MessagerMessage = {
                                type: 'info',
                                name: 'msg-fileuploaded',
                                message: 'File \\0 Uploaded',
                                autoCloseDelay: 6,
                                params: { 0: fileName }
                            }

                            $.translator.translateMessages([msg], function(translated: Array<translator.Message>) {
                                messagerControl.showMessages(translated);
                            });

                            refresh();
                        }
                    });
                }

                var loadTube = function(tube?: tct.Tube) {
                    dataForm.dataset(tube);
                    if (tube) {
                        validatorControl.tube(tube);
                        validatorControl.prevalidateAll();
                    }

                    if (tube.isNew) {
                        newUsage.attr('disabled', 'disabled');
                    } else {
                        newUsage.removeAttr('disabled');
                    }
                }

                var loadFromUrl = function() {
                    $.waitPanel.show();
                    var qstring = location.query();
                    var tubeid = qstring['id'];
                    var reqstring: Array<string> = [];
                    var mergeUserId = qstring['merge'];
                    var url = mergeUserId ? '/merge?' : '/editor?'
                    if (mergeUserId) {
                        reqstring.push('id=' + tubeid);
                        reqstring.push('userid=' + mergeUserId);
                    } else {
                        var userid = qstring['userid'];
                        if (userid) {
                            reqstring.push('userid=' + userid);
                        }
                        if (tubeid) {
                            reqstring.push('id=' + tubeid);
                        } else {
                            reqstring.push('d=new');
                        }
                    }

                    var ajaxRequest = $.ajax({
                        url: url + reqstring.join('&'),
                        type: 'post',
                        dataType: 'json',
                        timeout: 60000,
                        success: function(json) {
                            if (json.error) {
                                messagerControl.show('error', json.error);
                            } else {
                                // Load tube class from the received TubeData json
                                var tube = new app.Tube(json) as tct.Tube;
                                if (mergeUserId) {
                                    tube.isNew = true;
                                }
                                loadTube(tube);

                                registerForPush(user, tube);

                                $.pageState.replaceOrAddState(json);
                                $.waitPanel.hide();
                            }
                        },
                        error: function(e) {
                            if (e.statusText !== 'canceled') {
                                messagerControl.show('error', e);
                                loadTube(null);
                            }
                            $.waitPanel.hide();
                        },
                    });
                }

                // Ask server for datas
                setTimeout(function() {
                    loadFromUrl();
                }, 1);

                $(window).on('popstate', function(e) {
                    var event = <PopStateEvent>e.originalEvent;
                    if (event && event.state && event.state.j) {
                        unitIndexContainer.attr('data-index', location.query()['unit'] || '0');
                        var tube = new app.Tube(event.state.j) as tct.Tube;
                        loadTube(tube);
                    } else {
                        // Reload datas from url
                        loadFromUrl();
                    }
                });
            };
        })(jQuery);

        $(function() {
            if (location.hostname === '127.0.0.1') {
                var browserVersion = window.browserVersion();
                if (browserVersion.version > 9) {
                    $('head').append('<script type="text/javascript" src="http://127.0.0.1:38080/livereload.js"><\/script>');
                }
            }

            $.StartScreen();
        });
    })
}

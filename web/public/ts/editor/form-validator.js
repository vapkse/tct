'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var tct;
(function (tct) {
    var editor;
    (function (editor) {
        var FormValidator = (function (_super) {
            __extends(FormValidator, _super);
            function FormValidator() {
                _super.apply(this, arguments);
                this.oncreated = function () {
                    var self = this;
                    var unitContainer = self.container.find('#unit');
                    var unitTab = self.container.find('.unittab');
                    var field = self.container.find('[data-fields]');
                    self.$ = {
                        _body: $('body'),
                        _fields: field,
                        _unsavedButton: $('[unsaved]'),
                        _savedButton: $('[saved]'),
                        _rovh2: field.filter('[data-fields="h2"]'),
                        _socket: self.container.find('.socket'),
                        _unitsContainer: unitContainer,
                        _dataPins: unitContainer.find('[data-pin]'),
                        _unittab: unitTab,
                        _tabItems: unitTab.find('li'),
                        _noTypeLabel: self.container.find('.notype'),
                        _docsContainer: self.container.find('#docs'),
                        _baseView: self.container.find('#baseview'),
                        _pinoutView: self.container.find('#pinoutview'),
                    };
                    setTimeout(function () {
                        self.prevalidateAll();
                    }, 0);
                };
                this.tube = function (tube) {
                    var self = this;
                    if (tube !== undefined) {
                        self.options.tube = tube;
                    }
                    return self.options.tube;
                };
                this.hideRow = function (row) {
                    if (!row.hasClass('hidden')) {
                        row.addClass('transitioning');
                        setTimeout(function () {
                            row.addClass('transitioning');
                            row.addClass('hidden');
                            setTimeout(function () {
                                row.removeClass('transitioning');
                            }, 400);
                        }, 0);
                    }
                };
                this.showRow = function (row) {
                    if (row.hasClass('hidden')) {
                        row.addClass('transitioning');
                        setTimeout(function () {
                            row.removeClass('hidden');
                            setTimeout(function () {
                                row.removeClass('transitioning');
                            }, 400);
                        }, 0);
                    }
                };
                this.prevalidateDoubleHeader = function (field) {
                    var self = this;
                    if (!field) {
                        field = self.options.tube.dh;
                    }
                    if (field && field.value) {
                        self.showRow(self.$._rovh2);
                    }
                    else {
                        self.hideRow(self.$._rovh2);
                    }
                };
                this.prevalidateType = function (field) {
                    var self = this;
                    if (!field) {
                        field = self.options.tube.type;
                    }
                    self.currentTypeField = field;
                    var unitIndex = parseInt(self.$._unitsContainer.attr('data-index'));
                    if (isNaN(unitIndex)) {
                        unitIndex = 0;
                    }
                    self.prevalidateTypeUnit(unitIndex);
                };
                this.prevalidateTypeUnit = function (unitIndex) {
                    var self = this;
                    var field = self.currentTypeField;
                    if (!field.value) {
                        self.$._noTypeLabel.show();
                        self.$._unittab.hide();
                        return;
                    }
                    var config = field.value.cfg;
                    if (field.value.sym || unitIndex >= config.length) {
                        unitIndex = 0;
                    }
                    var unitsField = self.options.tube.getField('units');
                    var forceRefresh = false;
                    while (unitIndex >= unitsField.value.length) {
                        unitsField.value.push(self.options.tube.createArrayFieldSubset('unit'));
                        forceRefresh = true;
                    }
                    var unitConfg = field.value.cfg.length && field.value.cfg[unitIndex];
                    var $a = self.$._tabItems.hide().find('a').removeClass('active bo-theme bg-darker fg-lighter');
                    if (field.value.cfg.length > 1 && !field.value.sym) {
                        for (var tb = 0; tb < field.value.cfg.length; tb++) {
                            $(self.$._tabItems[tb]).show();
                        }
                        self.$._unittab.show();
                    }
                    else {
                        self.$._unittab.hide();
                    }
                    $($a[unitIndex]).addClass('active bo-theme bg-darker fg-lighter');
                    var availablePins = [];
                    if (unitConfg) {
                        availablePins = unitConfg.pins;
                        self.$._noTypeLabel.hide();
                        self.$._unitsContainer.show();
                    }
                    else {
                        self.$._noTypeLabel.show();
                        self.$._unitsContainer.hide();
                    }
                    self.$._dataPins.each(function (index, element) {
                        var $element = $(element);
                        var dataPin = $element.attr('data-pin');
                        if (dataPin) {
                            if (availablePins.indexOf(dataPin) >= 0) {
                                self.showRow($element);
                            }
                            else {
                                self.hideRow($element);
                            }
                        }
                    });
                    $.pageState.replaceUrlParam('unit', unitIndex.toString());
                    if (forceRefresh || parseInt(self.$._unitsContainer.attr('data-index')) !== unitIndex) {
                        self.$._unitsContainer.attr('data-index', unitIndex).closest('[data-container]').datacontainer('refresh');
                    }
                    return unitIndex;
                };
                this.viewPinout = function (field) {
                    var self = this;
                    if (!field) {
                        field = self.options.tube.pinout;
                    }
                    self.$._pinoutView.addClass('disabled');
                    if (field && field.value && field.value.name) {
                        $.get('/resource?img=images%2Fpinouts%2F' + field.value.name + '.svg').done(function (svg) {
                            if (svg) {
                                var show = function () {
                                    self.$._socket.html(svg);
                                    self.$._socket.find('svg').attr('width', '100%').attr('height', '100%').find('g').attr('class', 'fl-lighter');
                                    self.$._socket.removeClass('empty');
                                    self.$._pinoutView.removeClass('disabled');
                                };
                                if (!self.$._socket.hasClass('empty')) {
                                    self.$._socket.addClass('empty');
                                    setTimeout(function () {
                                        show();
                                    }, 200);
                                }
                                else {
                                    show();
                                }
                            }
                        });
                    }
                };
                this.viewBase = function (field) {
                    var self = this;
                    if (!field) {
                        field = self.options.tube.base;
                    }
                    self.$._baseView.addClass('disabled');
                    if (field && field.value && field.value.name) {
                        $.get('/resource?img=images%2Fbases%2F' + field.value.name + '.svg').done(function (svg) {
                            if (svg) {
                                var show = function () {
                                    self.$._socket.html(svg);
                                    self.$._socket.find('svg').attr('width', '100%').attr('height', '100%').find('g').attr('class', 'fl-lighter');
                                    self.$._socket.removeClass('empty');
                                    self.$._baseView.removeClass('disabled');
                                };
                                if (!self.$._socket.hasClass('empty')) {
                                    self.$._socket.addClass('empty');
                                    setTimeout(function () {
                                        show();
                                    }, 200);
                                }
                                else {
                                    show();
                                }
                            }
                        });
                    }
                };
                this.prevalidateFileControls = function () {
                    var self = this;
                    self.$._docsContainer.find('[data-index]').each(function (index, element) {
                        var filename = $(element).find('[name="filename"]').val().toLowerCase();
                        var reext = /^.+\.([^.]+)$/.exec(filename);
                        var ext = reext && reext.length > 1 ? reext[1].toLowerCase() : '';
                        var icon = $(element).find('#icon');
                        if (icon.fileIcon('instance')) {
                            icon.fileIcon('setOptions', { filename: filename });
                        }
                        else {
                            icon.fileIcon({ filename: filename });
                        }
                    });
                    self.prevalidateSaveButton();
                };
                this.prevalidateSaveButton = function () {
                    var self = this;
                    if (self._saveTimer) {
                        clearTimeout(self._saveTimer);
                    }
                    self._saveTimer = setTimeout(function () {
                        if (self.options.tube.isModified()) {
                            self.$._body.addClass('unsaved');
                        }
                        else {
                            self.$._body.removeClass('unsaved');
                        }
                    }, 300);
                };
                this.prevalidateAll = function () {
                    var self = this;
                    if (!self.options.tube) {
                        return self;
                    }
                    self.prevalidateDoubleHeader();
                    self.prevalidateType();
                    self.viewBase();
                    self.prevalidateFileControls();
                    self.prevalidateSaveButton();
                    self.hideValidationErrors();
                    var ve = self.options.tube.validate();
                    if (ve.length) {
                        for (var i = 0; i < ve.length; i++) {
                            var field = self.options.tube.getField(ve[i].fieldName);
                            if (self.options.tube.isDirtyField(field)) {
                                self.showValidationsError(ve[i]);
                            }
                        }
                    }
                    return self;
                };
                this.validateAll = function (cb) {
                    var self = this;
                    var verrors = [];
                    var done = function (verrors) {
                        if (verrors && verrors.length) {
                            $.translator.translateMessages(verrors, function (translated) {
                                self.showValidationErrors(translated);
                                cb(translated);
                            });
                        }
                        else {
                            cb();
                        }
                    };
                    var mergeErrors = function (errors) {
                        errors.map(function (err) { return verrors.push(err); });
                    };
                    mergeErrors(self.options.tube.validate());
                    mergeErrors(self.validateDoubleDecriptions());
                    done(verrors);
                };
                this.validateField = function (data) {
                    var self = this;
                    if (data.fieldName === 'type') {
                        self.prevalidateType(data.field);
                    }
                    if (data.fieldName === 'dh') {
                        self.prevalidateDoubleHeader(data.field);
                    }
                    if (data.fieldName === 'base') {
                        self.viewBase(data.field);
                    }
                    if (data.fieldName === 'pinout') {
                        self.viewPinout(data.field);
                    }
                    var ve = self.options.tube.validateField(data.field, data.fieldName);
                    if (ve.length) {
                        self.showValidationErrors(ve);
                    }
                    else {
                        self.hideValidationError(data.fieldName);
                    }
                    self.prevalidateSaveButton();
                };
            }
            FormValidator.prototype.validateDoubleDecriptions = function () {
                var self = this;
                var verrors = [];
                var descrValues = {};
                if (self.options.tube) {
                    var docs = self.options.tube.documents.value;
                    if (docs) {
                        for (var findex = 0; findex < docs.length; findex++) {
                            var d = docs[findex];
                            var field = d.getField('description');
                            var descr = field.value;
                            if (descrValues[descr] !== undefined) {
                                verrors.push({
                                    type: 'error',
                                    name: 'msg-duplicatedescr',
                                    message: 'Duplicate decriptions in documents',
                                    fieldName: 'description',
                                    dataIndex: findex
                                });
                            }
                            descrValues[descr] = d;
                        }
                    }
                }
                return verrors;
            };
            return FormValidator;
        })(modern.FormValidator);
        editor.FormValidator = FormValidator;
    })(editor = tct.editor || (tct.editor = {}));
})(tct || (tct = {}));
$.widget("tct.formvalidator", $.modern.formvalidator, new tct.editor.FormValidator());

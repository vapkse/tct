'use strict';

module tct.Creator {
    (function($: JQueryStatic) {
        $.StartScreen = function() {
            var settingsPanel: JQuery;
            var docSelector: JQuery;
            var selectorOptions: DocSelectorOptions;
            var pageId = $('[translate]').attr('translate');
            var mergeUserId = location.query()['merge'];
            var body = $('body');
            var container = $('#container');
            var translations = $('translations');
            var tubeResult: TubeSearchResult;
            var wizardCtrl: Wizard;

            // Translation area visible only after the user control is ready
            var transArea = $('#trans-area');
            var translatorControl: modern.TranslatorControl = transArea.find('#translator').translatorControl().on('translatorcontrolchange', function() {
                setTimeout(function() {
                    if (wizardCtrl) {
                        wizardCtrl.loadChartTitles();
                    }
                }, 100)
            }).data('modern-translatorControl');

            var messagerControl: modern.Messager = $('#messager').messager().data('modern-messager');

            var validatorControl: modern.FormValidator = $('#validator-panel').formvalidator({
                container: container
            }).data('modern-formvalidator');

            var tooltipManager: modern.TooltipManager = $('#container').tooltipmanager().data('modern-tooltipmanager');

            // User infos panel
            var huser = $('#user').html();
            var user: user.User = huser && JSON.parse(huser);
            var userInfosPanel = body.find('#user-infos-panel').userInfos({
                user: user
            }).bind('userinfoscreated', function() {
                transArea.animate({ opacity: 1 }, 500);
            });

            $('button[home]').on('click', function() {
                var searchparams = location.query()['rl'];
                location.href = '/' + (searchparams ? searchparams : '');
            })
                    
            // Bind settings panel button
            $('button[settings-panel]').on('click', function() {
                // Instianciate settings panel if the button is pressed for the first time
                if (!settingsPanel) {
                    requirejs(['settingsPanel'], function(settingsPanel: JQuery) {
                        settingsPanel = $('#settings-panel');
                        var settings = new tct.Settings(localStorage);
                        settingsPanel.settingsPanel(settings).bind('settingspanelchange', function(e: tct.SettingsPanelEvent, data: tct.SettingsPanelOptions) {
                            if (data.colorSettings.scheme) {
                                body.attr('id', 'scheme-' + data.colorSettings.scheme);
                            }
                            if (data.colorSettings.theme) {
                                body.attr('theme', data.colorSettings.theme);
                            }
                        });
                        settingsPanel.settingsPanel('toogle');
                    })
                } else {
                    settingsPanel.settingsPanel('toogle');
                }
            });

            var showError = function(errorMessage: string) {
                var msg: modern.MessagerMessage = {
                    type: 'error',
                    name: 'msg-fileuploaderror',
                    message: 'File Upload error: \\0',
                    params: { 0: errorMessage }
                }
                $.translator.translateMessages([msg], function(translated: Array<translator.Message>) {
                    messagerControl.showMessages(translated);
                });
                console.error(errorMessage);
            }

            var loadDocument = function(docid: string) {
                requirejs(['ajaxDownloadFile'], function() {
                    var documents = docid && selectorOptions.tubeData.documents.filter(d=> d._id === docid);
                    if (!documents.length) {
                        return;
                    }

                    var options: utils.AjaxDownloadFileOptions = {
                        url: '/download?url=' + documents[0].filename,
                        dataType: 'blob'
                    }

                    preloaderStatus(true);
                    utils.AjaxDownloadFile.download(options).onSuccessFinish(function(status, statusText, res, headers) {
                        var downloadUrl = URL.createObjectURL(res['blob']);
                        preloaderStatus(false);
                        loadImage(downloadUrl, documents[0]._id, documents[0].description);
                    }).onErrorFinish(function(status, statusText, res, headers) {
                        preloaderStatus(false);
                        showError(statusText);
                    });
                })
            }

            var loadImage = function(src: string, docid: string, defaultDescription: string) {
                var graphData: GraphData;

                if (wizardCtrl && wizardCtrl.data.image === src) {
                    wizardCtrl.loadStep(2);
                    return;
                }

                var storageKey = defaultDescription.replace(/[^A-Za-z0-9]/gi, '_').toLowerCase();

                var next = function() {
                    var graphDataStr = localStorage.getItem(storageKey);

                    var graphData: GraphData;
                    if (graphDataStr) {
                        graphData = JSON.parse(graphDataStr);
                    } else {
                        graphData = {
                            tubeGraph: {
                                c: [],
                                vg2: null,
                                logX: false,
                                logY: false,
                                name: defaultDescription,
                                raw: false,
                                unit: null,
                                triode: false,
                            },
                            image: '',
                            bounds: {
                                left: 5, //%
                                top: 5,
                                width: 90,
                                height: 90
                            },
                            opacity: 50,
                            xmax: null,
                            ymax: null,
                            xmin: null,
                            ymin: null,
                            storageKey: storageKey,
                        }
                    }

                    if (!wizardCtrl) {
                        wizardCtrl = new Wizard(tubeResult, container, messagerControl, validatorControl);
                    }
                    wizardCtrl.data = graphData;
                    
                    // Load new image, the wizard continue after the img control event
                    container.find('#image img').attr('src', src);
                }

                if (wizardCtrl && wizardCtrl.data.tubeGraph.c.length) {
                    // Confirm dialog
                    var dialogCont = container.find('[confirm-dialog]');

                    var closeDialog = function() {
                        setTimeout(function() {
                            dialogCont.find('[cancel]').off('click', oncancel);
                            dialogCont.find('[validate]').off('click', onvalidate);
                            dialog.close();
                            dialogCont.dialog('destroy');
                        }, 1);
                    }

                    var onvalidate = function() {
                        next();
                        closeDialog();
                    }

                    var oncancel = function() {
                        closeDialog();
                    }

                    var dialog = dialogCont.dialog({
                        show: true,
                        closeButton: true,
                        overlay: true,
                        overlayClickClose: true,
                        overlayColor: 'op-dark'
                    } as metro.DialogOptions).data('dialog');

                    dialogCont.find('[cancel]').on('click', oncancel);
                    dialogCont.find('[validate]').on('click', onvalidate);

                    dialogCont.find('#text').html(translations.find('#msg-imgconfirm').prop('innerHTML'));
                    dialogCont.css('opacity', 0);
                    dialogCont.data('dialog').open();
                    dialogCont.animate({ opacity: 1 }, 500);

                } else {
                    next();
                }
            }

            container.find('#finish').on('click', function() {

                var graphData = wizardCtrl.data;
                var override = false;
                
                // Save graph in the local storage
                localStorage.setItem(graphData.storageKey, JSON.stringify(graphData));

                var sendRequest = function() {
                    $.waitPanel.show();
                    var reqstring: Array<string> = ['d=attachgraph'];
                    var qstring = location.query();
                    var userid = qstring['userid'];
                    reqstring.push('id=' + qstring['id']);
                    if (override) {
                        reqstring.push('override=true');
                    }
                    if (userid) {
                        reqstring.push('userid=' + userid);
                    }
                    var ajaxRequest = $.ajax({
                        url: '/document?' + reqstring.join('&'),
                        type: 'post',
                        contentType: 'application/json',
                        timeout: 180000,
                        data: JSON.stringify(graphData.tubeGraph),
                        success: function(json: UploadResultFile) {
                            if (!json.error && json.filename && json.tubeGraph) {                            
                                // redirect to returl url
                                wizardCtrl = undefined;
                                setTimeout(function() {
                                    location.href = '/' + (qstring['rl'] || '?q=name%3D"' + tubeResult.tubeData.name + '"');
                                }, 500);
                            } else if (json.error) {
                                messagerControl.show('error', json.error);
                            } else {
                                messagerControl.show('error', 'Unknown error');
                            }
                            $.waitPanel.hide();
                        },
                        error: function(e) {
                            if (e.statusText !== 'canceled') {
                                messagerControl.show('error', e);
                            }
                            $.waitPanel.hide();
                        },
                    });
                }

                var verrors: Array<validation.Message> = [];
                if (!graphData.tubeGraph.name) {
                    verrors.push({
                        name: 'err-noname',
                        message: 'Please specify a description for this graph before continuing.',
                        tooltip: true,
                        type: 'error',
                        fieldName: 'name-value'
                    });
                } else {
                    // Check name duplication
                    if (tubeResult.tubeData.documents) {
                        var duplicated = tubeResult.tubeData.documents.filter(d=> d.description === graphData.tubeGraph.name);
                        if (duplicated.length) {
                            // Confirm dialog
                            var dialogCont = container.find('[confirm-dialog]');

                            var closeDialog = function() {
                                setTimeout(function() {
                                    dialogCont.find('[cancel]').off('click', oncancel);
                                    dialogCont.find('[validate]').off('click', onvalidate);
                                    dialog.close();
                                    dialogCont.dialog('destroy');
                                }, 1);
                            }

                            var onvalidate = function() {
                                override = true;
                                sendRequest();
                                closeDialog();
                            }

                            var oncancel = function() {
                                closeDialog();
                            }

                            var dialog = dialogCont.dialog({
                                show: true,
                                closeButton: true,
                                overlay: true,
                                overlayClickClose: true,
                                overlayColor: 'op-dark'
                            } as metro.DialogOptions).data('dialog');

                            dialogCont.find('[cancel]').on('click', oncancel);
                            dialogCont.find('[validate]').on('click', onvalidate);

                            dialogCont.find('#text').html(translations.find('#msg-overconfirm').prop('innerHTML'));
                            dialogCont.css('opacity', 0);
                            dialogCont.data('dialog').open();
                            dialogCont.animate({ opacity: 1 }, 500);
                            return;
                        }
                    }
                }

                if (verrors && verrors.length) {
                    $.translator.translateMessages(verrors, function(translated) {
                        validatorControl.showValidationErrors(verrors);
                        messagerControl.showMessages(verrors);
                    })
                    return;
                }

                sendRequest();
            });

            container.find('#choose').on('click', function() {
                // Ensure title on the right language
                selectorOptions.title = translations.find('#seltitle').prop('innerHTML');

                if (!docSelector) {
                    requirejs(['docSelector'], function() {
                        docSelector = $('[doc-selector]');
                        docSelector.docSelector(selectorOptions).bind('docselectorcreated', function() {
                            docSelector.docSelector('show');
                        }).bind('docselectorokclicked', function(e: DocSelectorEvent) {
                            // Get documents
                            loadDocument(e.selectedIds.length && e.selectedIds[0]);
                        });
                    })
                } else {
                    docSelector.docSelector('setOptions', selectorOptions);
                }
            })

            var uploader = $('form#upload').show();
            var preloader = $('#upload-preloader').hide();
            var backdrop = $('#upload-backdrop').hide();

            var preloaderStatus = function(status: boolean) {
                if (status) {
                    preloader.show();
                    backdrop.show();
                    uploader.hide();
                } else {
                    preloader.hide();
                    backdrop.hide();
                    uploader.show();
                }
            }

            $('#upload :file').on('change', function() {
                var input = $(this);
                var files = input.prop('files');
                if (files.length) {
                    var reader = new FileReader();
                    
                    // inject an image with the src url
                    preloaderStatus(true);
                    reader.onloadend = function(event) {
                        var target = event.target as any;
                        preloaderStatus(false);
                        var fileInfo = /(^.+)(\.[^.]+)$/.exec(files[0].name);
                        var fileBase = fileInfo ? fileInfo[1] : files[0].name;
                        loadImage(target.result, null, fileBase);
                    }

                    reader.onerror = function(event) {
                        preloaderStatus(false);
                        showError(reader.error.toString());
                    }
 
                    // when the file is read it triggers the onload event above.
                    reader.readAsDataURL(files[0]);
                }
            });

            // Ask server for datas
            setTimeout(function() {
                $.waitPanel.show();
                var queryString = location.query();
                var reqstring: Array<string> = [];
                var tubeid = queryString['id'];
                var userid = queryString['userid'];
                if (userid) {
                    reqstring.push('&userid=' + encodeURIComponent(userid));
                }
                reqstring.push('id=' + tubeid);

                var ajaxRequest = $.ajax({
                    url: '/editor?' + reqstring.join('&'),
                    type: 'post',
                    dataType: 'json',
                    timeout: 180000,
                    success: function(json) {
                        if (json.error) {
                            messagerControl.show('error', json.error);
                        } else {
                            tubeResult = json as TubeSearchResult;
                            $('#title-content').find('#name').text(tubeResult.tubeData.name);
                
                            // Doc selector options
                            selectorOptions = {
                                tubeData: tubeResult.tubeData,
                                title: '',
                                mode: 'radio',
                                filter: new RegExp('^.+\.(jpg|png|jpeg|gif|bmp|tif|tiff)$', 'i'),
                                showUsages: false
                            };

                            if (queryString['docid']) {
                                loadDocument(queryString['docid']);
                            }

                            $.waitPanel.hide();
                        }
                    },
                    error: function(e) {
                        if (e.statusText !== 'canceled') {
                            messagerControl.show('error', e);
                        }
                        $.waitPanel.hide();
                    },
                });
            }, 1);

            window.onbeforeunload = function() {
                if (wizardCtrl && wizardCtrl.data) {
                    return translations.find('[uid="msg-leave"]').text();
                }
            }

            $(window).on('popstate', function(e) {
                var event = <PopStateEvent>e.originalEvent;
                var step = parseInt(location.query()['step']);
                wizardCtrl.loadStep(isNaN(step) ? 1 : step);
            });
        };
    })(jQuery);

    $(function() {
        if (location.hostname === '127.0.0.1') {
            var browserVersion = window.browserVersion();
            if (browserVersion.version > 9) {
                $('head').append('<script type="text/javascript" src="http://127.0.0.1:38080/livereload.js"><\/script>');
            }
        }

        $.StartScreen();
    });
}

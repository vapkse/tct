'use strict';

module tct.Creator {
    export interface GraphData {
        tubeGraph?: TubeGraph,
        image?: string,
        bounds?: {
            left?: number,
            top?: number,
            width?: number,
            height?: number
        },
        opacity?: number,
        xmax?: number,
        ymax?: number,
        xmin?: number,
        ymin?: number,
        storageKey?: string,
    }

    export class Wizard {
        private chart: HighchartsChartObject;
        private finalChart: AnodeTransfertChart;
        private currentStep: number;
        private stepContainer: JQuery;
        private stepper: metro.Stepper;
        private translations: JQuery;
        private titleContent: JQuery;
        private imageContainer: JQuery;
        private imageContent: JQuery;
        private chartContainer: JQuery;
        private ctrlContainer: JQuery;
        private dialogCont: JQuery;
        private sliders: JQuery;
        private units: JQuery;
        private g1Input: JQuery;
        private g2Input: JQuery;
        private nameInput: JQuery;
        private xmaxInput: JQuery;
        private ymaxInput: JQuery;
        private xminInput: JQuery;
        private yminInput: JQuery;
        private triode: JQuery;
        private xscaleLogCheckbox: JQuery;
        private yscaleLogCheckbox: JQuery;
        private g2: JQuery;
        private graphData: GraphData;
        private positionableElement: HTMLElement;
        private interactable: Interact.Interactable;
        private messager: modern.Messager;
        private validator: modern.FormValidator;
        private tuberesult: TubeSearchResult;
        private numberOfUnits: number;
        private currentSerieIndex: number;
        private colors: Array<string>;
        private dragDropPoint: HighchartsPointObject = undefined;
        private sizingTimer = 0;
        private windowWidth: number = 0;
        private windowHeight: number = 0;
        private onresize: (e: Event) => void;
        private browserVersion: BrowserVersion;

        public get data() {
            var self = this as Wizard;
            return self.graphData
        }

        public set data(data: GraphData) {
            var self = this as Wizard;
            self.graphData = data;
        }

        constructor(tuberesult: TubeSearchResult, container: JQuery, messager: modern.Messager, validator: modern.FormValidator) {
            var self = this as Wizard;
            self.stepContainer = container.find('#step-container');
            self.translations = $('translations');
            self.titleContent = container.find('#title-content');
            self.units = self.stepContainer.find('#units');
            self.g1Input = self.stepContainer.find('#vg1-value');
            self.g2 = self.stepContainer.find('#g2');
            self.sliders = self.stepContainer.find('[opacity-slider]');
            self.g2Input = self.stepContainer.find('#vg2-value');
            self.triode = self.stepContainer.find('#triode');
            self.dialogCont = container.find('[confirm-dialog]');
            self.nameInput = self.stepContainer.find('#name-value');
            self.xmaxInput = self.stepContainer.find('#xmax-value');
            self.ymaxInput = self.stepContainer.find('#ymax-value');
            self.xminInput = self.stepContainer.find('#xmin-value');
            self.yminInput = self.stepContainer.find('#ymin-value');
            self.xscaleLogCheckbox = self.stepContainer.find('#xscale-log');
            self.yscaleLogCheckbox = self.stepContainer.find('#yscale-log');
            self.messager = messager;
            self.validator = validator;
            self.tuberesult = tuberesult;
            self.graphData = {};
            self.currentStep = 1;
            self.browserVersion = window.browserVersion();

            self.imageContainer = container.find('#image').on('mouseleave', function() {
                $('html').removeAttr('style');
            }).on('mouseup', function() {
                setTimeout(function() {
                    self.adjustControlsMargin();
                }, 200);
            });

            self.imageContent = self.imageContainer.find('img').on('load', function(e) {
                setTimeout(function() {
                    self.graphData.image = self.imageContent.attr('src');
                    self.imageContent.css('opacity', self.graphData.opacity / 100)
                    self.loadStep(2);
                }, 1);
            });

            self.chartContainer = self.imageContainer.find('#positionable');

            var stepperOptions: metro.StepperOptions = {
                steps: 4,
                onStepClick: function(e) {
                    self.loadStep(++e);
                }
            }

            self.loadSerie(-1);

            self.stepper = container.find('#stepper').stepper(stepperOptions).data('stepper') as metro.Stepper;

            $('#container').on('mousedown', function(e) {
                if ($(e.target).closest('.highcharts-container').length > 0) {
                    self.imageContainer.addClass('toporder');
                } else {
                    self.imageContainer.removeClass('toporder');
                }
            })

            self.onresize = function(e: Event) {
                if (self.sizingTimer) {
                    clearTimeout(self.sizingTimer);
                    self.sizingTimer = 0;
                }

                var target = $(e.target);
                if (self.windowWidth !== target.width() || self.windowHeight !== target.height()) {
                    self.windowWidth = target.width();
                    self.windowHeight = target.height();
                    self.sizingTimer = setTimeout(function() {
                        self.stepper._positioningSteps();
                        self.sizeChart();
                        self.sizingTimer = setTimeout(function() {
                            self.sizingTimer = 0;
                            self.sizeChart();
                        }, 100)
                    }, 100)
                }
            }
            window.addEventListener('resize', self.onresize);

            self.stepContainer.find('[data-click]').on('click', function(e) {
                var func = $(e.target).closest('[data-click]').attr('data-click');
                switch (func) {
                    case 'previous':
                        self.loadStep(self.currentStep - 1);
                        break;
                    case 'next':
                        self.loadStep(self.currentStep + 1);
                        break;

                    case 'terminate-serie':
                        var verrors = self.saveCurrentSerie();
                        if (verrors && verrors.length) {
                            // Display errors
                            $.translator.translateMessages(verrors, function(translated) {
                                self.validator.showValidationErrors(verrors);
                                self.messager.showMessages(verrors);
                            })
                        }
                        break;

                    case 'add-serie':
                        // Attention name is used as a keyword to detect a new serie 
                        var serie = self.addSerie('new');
                        self.g1Input.numericinput('value', null);
                        self.loadSerie(serie);
                        break;

                    case 'cancel-serie':
                        // Check if edition or new
                        var vg1 = self.g1Input.numericinput('value');
                        var cs = self.graphData.tubeGraph.c.filter(c => c.vg1 === vg1);
                        if (cs.length === 0) {
                            // Delete serie
                            self.chart.series[self.currentSerieIndex].remove(true);
                        } else {
                            // Reload serie
                            var points = [] as Array<[]>;
                            cs[0].p.map(p => points.push([p.va, p.ik * 1000]));
                            self.chart.series[self.currentSerieIndex].update({
                                data: points
                            }, true)
                        }
                        self.loadSerie(-1);
                        break;
                }
            })

            self.numberOfUnits = Math.min(!tuberesult.type || tuberesult.type.sym ? 1 : tuberesult.type.cfg.length);
            if (self.numberOfUnits > 1) {
                self.units.show().find('button').each(function(index, element) {
                    if (index < self.numberOfUnits) {
                        $(element).css('visibility', 'visible');
                    } else {
                        $(element).css('visibility', 'hidden');
                    }
                })
                self.triode.hide();
                self.g2.hide();
            } else if (self.tuberesult.type.cfg.length) {
                self.units.hide();
                if (self.tuberesult.type.cfg[0].pins.indexOf('g2') >= 0) {
                    self.triode.show();
                    self.g2.show();
                } else {
                    self.triode.hide();
                    self.g2.hide();
                }
            }
        }

        public loadChartTitles = function() {
            var self = this as Wizard;
            if (self.finalChart) {
                self.finalChart.loadChartTitles();
            }
        }

        private confirm = function(msg: string, validate?: () => void, cancel?: () => void) {
            var self = this as Wizard;

            var closeDialog = function() {
                setTimeout(function() {
                    self.dialogCont.find('[cancel]').off('click', oncancel);
                    self.dialogCont.find('[validate]').off('click', onvalidate);
                    dialog.close();
                    self.dialogCont.dialog('destroy');
                }, 1);
            }

            var oncancel = function() {
                if (cancel) {
                    cancel();
                }
                closeDialog();
            }

            var onvalidate = function() {
                if (validate) {
                    validate();
                }
                closeDialog();
            }

            // Confirm dialog
            var dialog = self.dialogCont.dialog({
                show: true,
                closeButton: true,
                overlay: true,
                overlayClickClose: true,
                overlayColor: 'op-dark'
            } as metro.DialogOptions).data('dialog');

            self.dialogCont.find('[cancel]').on('click', oncancel);
            self.dialogCont.find('[validate]').on('click', onvalidate);

            self.dialogCont.find('#text').html(msg);
            self.dialogCont.css('opacity', 0);
            dialog.open();
            self.dialogCont.animate({ opacity: 1 }, 500);
        }

        private saveCurrentSerie = function() {
            var self = this as Wizard;
            var ve = [] as Array<validation.Message>;
            
            // Check if g1 is defined
            var vg1 = self.g1Input.numericinput('value');
            if (vg1 === undefined || vg1 === null) {
                ve.push({
                    name: 'err-novg1',
                    message: 'Please specify the grid1 voltage for the current curve before continuing.',
                    tooltip: true,
                    type: 'error',
                    fieldName: 'vg1-value'
                });
            } else if (vg1 < -999 || vg1 > 999) {
                ve.push({
                    name: 'err-vg1',
                    message: 'The Grid1 voltage is out of range.',
                    tooltip: true,
                    type: 'error',
                    fieldName: 'vg1-value'
                });
            }
                   
            // Get serie and points
            var serie = self.chart.series[self.currentSerieIndex];
            if (serie.data.length < 2) {
                ve.push({
                    name: 'err-nopoints',
                    message: 'Please specify at least two points to define a transfert curve.',
                    tooltip: true,
                    type: 'error'
                });
            }

            // Search an existing curve for this voltage
            var c: TubeCurve;
            var cs = self.graphData.tubeGraph.c.filter(c => c.vg1 === vg1);

            // Check duplication
            if (serie.name === 'new' && cs.length) {
                ve.push({
                    name: 'err-dupvg1',
                    message: 'A existing curve with this vg1 voltage already exists.',
                    tooltip: true,
                    type: 'error',
                    fieldName: 'vg1-value'
                });
            }

            // Set name (vg1), draggable x et y false
            if (ve.length === 0) {
                var self = this as Wizard;

                if (cs.length) {
                    c = cs[0];
                    c.p = [] as Array<TubeMeasure>;
                } else {
                    c = {
                        p: [] as Array<TubeMeasure>,
                        vg1: vg1
                    };
                    self.graphData.tubeGraph.c.push(c);
                }

                var vg2 = self.graphData.tubeGraph.vg2;
                serie.data.map(p => c.p.push({
                    va: p.x,
                    vg1: vg1,
                    ig1: 0,
                    vg2: vg2,
                    ig2: 0,
                    ik: p.y / 1000
                }));
                
                // Order points
                c.p.sort(function(a, b) {
                    return a.va - b.va;
                })

                // Reload serie
                var points = [] as Array<[]>;
                c.p.map(p => points.push([p.va, p.ik * 1000]));
                serie.update({
                    data: points,
                    name: vg1.toString() + 'V'
                }, true)

                self.loadSerie(-1);
            }

            return ve;
        }

        private validateStep = function(step: number) {
            var self = this as Wizard;
            var ve: Array<validation.Message> = [];

            if (step >= 2) {
                var imageElement = self.imageContent[0] as any;
                if (imageElement.height === 0 || imageElement.width === 0) {
                    ve.push({
                        name: 'err-noimage',
                        message: 'No image loaded, or the image is invalid.',
                        tooltip: false,
                        type: 'error',
                    });
                }
            }

            if (step >= 3 && ve.length === 0) {
                if (self.graphData.xmax === undefined || self.graphData.xmax === null) {
                    ve.push({
                        name: 'err-noxmax',
                        message: 'Please specify the horizontal scale before continuing.',
                        tooltip: true,
                        type: 'error',
                        fieldName: 'xmax-value'
                    });
                } else if (self.graphData.xmax <= 0 || self.graphData.xmax > 99999) {
                    ve.push({
                        name: 'err-xmax',
                        message: 'The horizontal scale is out of range.',
                        tooltip: true,
                        type: 'error',
                        fieldName: 'xmax-value'
                    });
                }

                if (self.graphData.xmin === undefined || self.graphData.xmin === null) {
                    ve.push({
                        name: 'err-noxmin',
                        message: 'Please specify the horizontal minimum value before continuing.',
                        tooltip: true,
                        type: 'error',
                        fieldName: 'xmin-value'
                    });
                } else {
                    var minOk = self.graphData.tubeGraph.logX ? self.graphData.xmin > 0 : self.graphData.xmin >= -99999;
                    if (!minOk || self.graphData.xmin > 99999) {
                        ve.push({
                            name: 'err-xmin',
                            message: 'The horizontal minimum value is out of range.',
                            tooltip: true,
                            type: 'error',
                            fieldName: 'xmin-value'
                        });
                    }
                }
                if (self.graphData.ymax === undefined || self.graphData.ymax === null) {
                    ve.push({
                        name: 'err-noymax',
                        message: 'Please specify the vertical scale before continuing.',
                        tooltip: true,
                        type: 'error',
                        fieldName: 'ymax-value'
                    });
                } else if (self.graphData.ymax <= 0 || self.graphData.ymax > 99999) {
                    ve.push({
                        name: 'err-ymax',
                        message: 'The vertical scale is out of range.',
                        tooltip: true,
                        type: 'error',
                        fieldName: 'ymax-value'
                    });
                }
                if (self.graphData.ymin === undefined || self.graphData.ymin === null) {
                    ve.push({
                        name: 'err-noymin',
                        message: 'Please specify the vertical minimum value before continuing.',
                        tooltip: true,
                        type: 'error',
                        fieldName: 'ymin-value'
                    });
                } else {
                    var minOk = self.graphData.tubeGraph.logX ? self.graphData.ymin > 0 : self.graphData.ymin >= -99999;
                    if (!minOk || self.graphData.ymin > 99999) {
                        ve.push({
                            name: 'err-ymin',
                            message: 'The vertical minimum value is out of range.',
                            tooltip: true,
                            type: 'error',
                            fieldName: 'ymin-value'
                        });
                    }
                }
                if (self.numberOfUnits > 1 && (self.graphData.tubeGraph.unit === undefined || self.graphData.tubeGraph.unit === null)) {
                    ve.push({
                        name: 'err-nounit',
                        message: 'Please specify the unit before continuing.',
                        tooltip: true,
                        type: 'error',
                        fieldName: 'unit'
                    });
                }
                if (self.g2.is(':visible') && !self.graphData.tubeGraph.triode) {
                    if (self.graphData.tubeGraph.vg2 === undefined || self.graphData.tubeGraph.vg2 === null) {
                        ve.push({
                            name: 'err-novg2',
                            message: 'Please specify the grid2 voltage before continuing.',
                            tooltip: true,
                            type: 'error',
                            fieldName: 'vg2-value'
                        });
                    } else if (self.graphData.tubeGraph.vg2 < 0 || self.graphData.tubeGraph.vg2 > 9999) {
                        ve.push({
                            name: 'err-vg2',
                            message: 'Grid2 voltage is out of range.',
                            tooltip: true,
                            type: 'error',
                            fieldName: 'vg2-value'
                        });
                    }
                }
            }

            if (step >= 4 && ve.length === 0) {
                if (!self.chart || self.chart.series.length === 0) {
                    ve.push({
                        name: 'err-noserie',
                        message: 'At least one curve must be defined before continuing.',
                        tooltip: false,
                        type: 'error',
                    });
                }
            }

            return ve;
        }

        private sizeChart = function() {
            var self = this as Wizard;
            if (!self.chart) {
                return;
            }
            // Calc width and height at 100%
            var imageElement = self.imageContent[0] as any;
            var width = self.stepContainer.width();
            var height = width * imageElement.height / imageElement.width;
            self.chart.options.chart.width = width * self.graphData.bounds.width / 100;
            self.chart.options.chart.height = height * self.graphData.bounds.height / 100;
            self.chart.reflow();
        }

        private editSerie = function(s: HighchartsSeriesObject) {
            var self = this as Wizard;
            // Switch the double-clicked serie in edition            
            var index = s.index;
            self.g1Input.numericinput('value', self.graphData.tubeGraph.c[index].vg1);
            self.loadSerie(index);
        }

        private loadSerie = function(serie: number) {
            var self = this as Wizard;
            if (self.chart && self.currentSerieIndex !== serie) {
                if (serie >= 0) {
                    // Edit, hide all other series
                    self.chart.series.forEach(function(s, index) {
                        if (index !== serie) {
                            s.hide();
                        }
                    })
                } else {
                    self.chart.series.forEach(function(s) {
                        s.show();
                    })
                }
            }

            self.currentSerieIndex = serie;
            self.stepContainer.attr('current-serie', serie);
        }

        private refreshUnitSelector = function() {
            var self = this as Wizard;
            self.units.find('button').each(function(index, element) {
                if (index === self.graphData.tubeGraph.unit) {
                    $(element).removeClass('no-border').addClass('bo-lighter');
                } else {
                    $(element).addClass('no-border').removeClass('bo-lighter');
                }
            })
        }

        private refreshSliders = function() {
            var self = this as Wizard;
            if (self.currentStep === 1) {
                return;
            }

            var imageOpacity = self.graphData.opacity;
            self.imageContent.css('opacity', imageOpacity / 100);
            self.sliders.slider('value', imageOpacity);
        }

        private refreshG2Visibility = function() {
            var self = this as Wizard;

            if (self.graphData.tubeGraph.triode) {
                self.g2.css('visibility', 'hidden');
            } else {
                self.g2.css('visibility', 'visible');
            }
        }

        private adjustControlsMargin = function() {
            var self = this as Wizard;
            var marginTop = Math.max(0, -1 * self.graphData.bounds.top - 2)
            self.ctrlContainer.css('margin-bottom', marginTop + '%');
        }

        private loadControls = function() {
            var self = this as Wizard;

            var xAxisChangeTimer = 0;
            var applyXAxisChanges = function() {
                if (xAxisChangeTimer) {
                    clearTimeout(xAxisChangeTimer);
                    xAxisChangeTimer = 0;
                }
                xAxisChangeTimer = setTimeout(function() {
                    xAxisChangeTimer = 0;
                    var options = {
                        type: self.graphData.tubeGraph.logX ? 'logarithmic' : 'linear'
                    } as HighchartsAxisOptions;
                    if (self.graphData.xmax) {
                        options.max = self.graphData.xmax;
                    } else {
                        options.max = 100;
                    }
                    if (self.graphData.tubeGraph.logX) {
                        options.min = self.graphData.xmin > 0 ? self.graphData.xmin : options.max / 1000;
                    } else {
                        options.min = 0;
                    }
                    self.chart.xAxis[0].update(options);
                }, 300)
            }

            var yAxisChangeTimer = 0;
            var applyYAxisChanges = function() {
                if (yAxisChangeTimer) {
                    clearTimeout(yAxisChangeTimer);
                    yAxisChangeTimer = 0;
                }
                yAxisChangeTimer = setTimeout(function() {
                    yAxisChangeTimer = 0;
                    var options = {
                        type: self.graphData.tubeGraph.logY ? 'logarithmic' : 'linear'
                    } as HighchartsAxisOptions;
                    if (self.graphData.ymax) {
                        options.max = self.graphData.ymax;
                    } else {
                        options.max = 100;
                    }
                    if (self.graphData.tubeGraph.logY) {
                        options.min = self.graphData.ymin > 0 ? self.graphData.ymin : options.max / 1000;
                    } else {
                        options.min = 0;
                    }
                    self.chart.yAxis[0].update(options);
                }, 300)
            }

            if (self.ctrlContainer) {
                // Already defined, just bind
                self.xmaxInput.numericinput('value', self.graphData.xmax);
                self.ymaxInput.numericinput('value', self.graphData.ymax);
                self.xminInput.numericinput('value', self.graphData.xmin);
                self.yminInput.numericinput('value', self.graphData.ymin);
                self.g2Input.numericinput('value', self.graphData.tubeGraph.vg2);
                self.triode.checkbox('value', self.graphData.tubeGraph.triode);
                self.xscaleLogCheckbox.checkbox('value', self.graphData.tubeGraph.logX);
                self.yscaleLogCheckbox.checkbox('value', self.graphData.tubeGraph.logY);
                self.refreshG2Visibility();
                self.refreshUnitSelector();
                self.refreshSliders();
                return;
            }

            self.ctrlContainer = self.stepContainer.find('#controls');

            self.xmaxInput.numericinput({
                value: self.graphData.xmax
            }).on('numericinputchange', function(e: modern.NumericInputEvent) {
                self.graphData.xmax = e.value;
                applyXAxisChanges();
            }).on('numericinputfocus', function() {
                self.validator.hideValidationError('xmin-value');
                self.messager.hideMessage('xmin-value');
            });

            self.xminInput.numericinput({
                value: self.graphData.xmin
            }).on('numericinputchange', function(e: modern.NumericInputEvent) {
                self.graphData.xmin = e.value;
                applyXAxisChanges();
            }).on('numericinputfocus', function() {
                self.validator.hideValidationError('xmin-value');
                self.messager.hideMessage('xmin-value');
            });

            self.xscaleLogCheckbox.checkbox({
                value: self.graphData.tubeGraph.logX
            }).on('checkboxchange', function(e: modern.CheckboxEvent) {
                var next = function() {
                    self.graphData.tubeGraph.logX = e.value;
                    self.graphData.tubeGraph.c = [];
                    applyXAxisChanges();
                }
                if (self.graphData.tubeGraph.c.length) {
                    var msg = self.translations.find('#msg-chkconfirm').prop('innerHTML');
                    self.confirm(msg, function() {
                        // Validate
                        next();
                    }, function() {
                        // Cancel, restore previous value
                        self.xscaleLogCheckbox.checkbox('value', self.graphData.tubeGraph.logX);
                    })

                } else {
                    next();
                }
            });

            self.ymaxInput.numericinput({
                value: self.graphData.ymax
            }).on('numericinputchange', function(e: modern.NumericInputEvent) {
                self.graphData.ymax = e.value;
                applyYAxisChanges();
            }).on('numericinputfocus', function() {
                self.validator.hideValidationError('ymax-value');
                self.messager.hideMessage('ymax-value');
            });

            self.yminInput.numericinput({
                value: self.graphData.ymin
            }).on('numericinputchange', function(e: modern.NumericInputEvent) {
                self.graphData.ymin = e.value;
                applyYAxisChanges();
            }).on('numericinputfocus', function() {
                self.validator.hideValidationError('ymin-value');
                self.messager.hideMessage('ymin-value');
            });

            self.yscaleLogCheckbox.checkbox({
                value: self.graphData.tubeGraph.logY
            }).on('checkboxchange', function(e: modern.CheckboxEvent) {
                var next = function() {
                    self.graphData.tubeGraph.logY = e.value;
                    self.graphData.tubeGraph.c = [];
                    applyYAxisChanges();
                }
                if (self.graphData.tubeGraph.c.length) {
                    var msg = self.translations.find('#msg-chkconfirm').prop('innerHTML');
                    self.confirm(msg, function() {
                        // Validate
                        next();
                    }, function() {
                        // Cancel, restore previous value
                        self.yscaleLogCheckbox.checkbox('value', self.graphData.tubeGraph.logY);
                    })

                } else {
                    next();
                }
            });

            self.g2Input.numericinput({
                value: self.graphData.tubeGraph.vg2
            }).on('numericinputchange', function(e: modern.NumericInputEvent) {
                self.graphData.tubeGraph.vg2 = e.value;
            }).on('numericinputfocus', function() {
                self.validator.hideValidationError('vg2-value')
                self.messager.hideMessage('vg2-value');
            });

            self.triode.checkbox({
                value: self.graphData.tubeGraph.triode
            }).on('checkboxchange', function(e: modern.CheckboxEvent) {
                self.graphData.tubeGraph.triode = e.value;
                self.refreshG2Visibility();
            });

            self.g1Input.numericinput({}).on('numericinputfocus', function() {
                self.validator.hideValidationError('vg1-value')
                self.messager.hideMessage('vg1-value');
            }).find('input').on('keypress', function(e) {
                if (e.keyCode === 13) {
                    self.stepContainer.find('[data-click="terminate-serie"]').click();
                }
            });

            self.nameInput.textinput({
                value: self.graphData.tubeGraph.name
            }).on('textinputchange', function(e: modern.TextInputEvent) {
                self.graphData.tubeGraph.name = e.value;
            }).on('textinputfocus', function() {
                self.validator.hideValidationError('name-value')
                self.messager.hideMessage('name-value');
            });

            self.units.find('button').on('click', function(e) {
                self.validator.hideValidationError('unit');
                self.messager.hideMessage('unit');
                self.graphData.tubeGraph.unit = parseInt($(e.target).closest('[u-index]').attr('u-index'));
                if (self.tuberesult.type.cfg[self.graphData.tubeGraph.unit].pins.indexOf('g2') >= 0) {
                    self.g2.show();
                } else {
                    self.g2.hide();
                }
                self.refreshUnitSelector();
            })
            
            // Opacity slider
            self.sliders.on('slidercreate', function(event, ui) {
                $(event.target).find('.marker').addClass('bg-lighter');
                $(event.target).find('.complete').addClass('bg-theme');
            }).slider({}).on('changed', function(e: JQueryEventObject, value: number) {
                self.graphData.opacity = value;
                self.refreshSliders();
            });

            self.refreshSliders();
        }

        private getChartEventTarget = function(e: JQueryMouseEventObject, serie?: HighchartsSeriesObject): { element: any, type: string } {
            var self = this as Wizard;
            var bestSerie: HighchartsSeriesObject;
            
            // Priority on point
            var searchPoint = function(serie: HighchartsSeriesObject) {
                for (var p = 0; p < serie.points.length; p++) {
                    var point = serie.points[p];
                    if (point.graphic && $(e.target).is(point.graphic.element)) {
                        return {
                            element: point,
                            type: 'point'
                        }
                    }
                }
            }

            if (serie) {
                var point = searchPoint(serie);
                if (point) {
                    return point;
                }
            } else {
                for (var s = 0; s < self.chart.series.length; s++) {
                    var serie = self.chart.series[s];
                    if (serie.visible) {
                        if ($(e.target).closest('.highcharts-series').is(serie.group.element)) {
                            bestSerie = serie;
                        }
                        var point = searchPoint(serie);
                        if (point) {
                            return point;
                        }
                    }
                }
            }

            return bestSerie ?
                {
                    element: bestSerie,
                    type: 'serie'
                } : {
                    element: self.chart,
                    type: 'chart'
                }
        }

        private loadChart = function(forStep: number, cb?: Function) {
            var self = this as Wizard;
            var clickedPoint: HighchartsPointObject;
            var clickedPosition: { x: number, y: number } = { x: 0, y: 0 };

            if (self.chart) {
                if (cb) {
                    cb();
                };
                return;
            }

            var getOffset = function(e: JQueryMouseEventObject) {
                var retval: { x: number, y: number };
                if (self.browserVersion.browser === 'firefox') {
                    retval = {
                        x: (e.originalEvent && e.originalEvent.layerX) || e.offsetX,
                        y: (e.originalEvent && e.originalEvent.layerY) || e.offsetY
                    }
                } else {
                    retval = {
                        x: e.offsetX,
                        y: e.offsetY
                    }
                }
                return retval;
            }

            if (forStep < 4) {
                requirejs(['highcharts', 'colors'], function() {
                    self.colors = utils.Colors.highchartsColors();

                    var options = {
                        chart: {
                            backgroundColor: '#rgba(0,0,0,0)',
                            type: 'line',
                            margin: [0, 0, 0, 0],
                            borderWidth: 2,
                        },
                        title: {
                            text: ''
                        },
                        subTitle: {
                            text: ''
                        },
                        xAxis: {
                            gridLineWidth: 3,
                            lineWidth: 3,
                            title: undefined,
                            type: self.graphData.tubeGraph.logX ? 'logarithmic' : 'linear',
                            min: self.graphData.xmin,
                            max: self.graphData.xmax,
                        },
                        yAxis: {
                            gridLineWidth: 3,
                            lineWidth: 3,
                            title: undefined,
                            type: self.graphData.tubeGraph.logY ? 'logarithmic' : 'linear',
                            min: self.graphData.ymin,
                            max: self.graphData.ymax
                        },
                        legend: {
                            enabled: false
                        },
                        exporting: {
                            enabled: false
                        },
                        tooltip: {
                            enabled: false,
                        },
                        plotOptions: {
                            series: {
                                lineWidth: 3
                            } as HighchartsPlotOptions
                        }
                    } as HighstockOptions

                    if (forStep === 3) {
                        options.chart.type = 'spline';

                        options.plotOptions.series.marker = {
                            enabled: true,
                            symbol: 'circle',
                            radius: 6
                        };

                        self.chartContainer.highcharts(options, function(c) {
                            self.chart = c;
                            
                            // Register chart events
                            self.chartContainer.find('svg').on('dblclick', function(e) {
                                // console.log('dblclick');
                                if (self.currentSerieIndex >= 0) {
                                    return;
                                }
                                
                                // Check if point or serie
                                var clickedInfos = self.getChartEventTarget(e);
                                var serie: HighchartsSeriesObject;
                                if (clickedInfos.type === 'serie') {
                                    serie = clickedInfos.element;
                                } else if (clickedInfos.type === 'point') {
                                    serie = (clickedInfos.element as HighchartsPointObject).series;
                                }
                                if (serie) {
                                    // Not in edition, edit the serie
                                    self.editSerie(serie);
                                }

                            }).on('mousedown', function(e) {
                                // Check if point
                                var clickedInfos = self.getChartEventTarget(e);
                                clickedPosition = getOffset(e);
                                if (clickedInfos.type === 'point') {
                                    clickedPoint = clickedInfos.element as HighchartsPointObject;
                                }

                            }).on('mousemove', function(e: JQueryMouseEventObject) {
                                // console.log('mousemove on chart ' + $(e.target).prop('tag'));
                                var offset = getOffset(e);

                                if (clickedPoint) {
                                    var serieIndex = clickedPoint.series.index;
                                    if (serieIndex === self.currentSerieIndex) {
                                        if (Math.abs(offset.x - clickedPosition.x) > 5 || Math.abs(offset.y - clickedPosition.y) > 5) {
                                            // Start drag
                                            self.dragDropPoint = clickedPoint;
                                            clickedPoint = null;
                                        }
                                    }
                                }

                                if (self.dragDropPoint && !self.dragDropPoint.update) {
                                    // Destroyed point
                                    self.dragDropPoint = undefined;
                                }

                                if (e.buttons === 0) {
                                    // Button not yet pressed 
                                    self.dragDropPoint = undefined;
                                }

                                if (self.dragDropPoint) {
                                    // console.log('drag point');

                                    // Because the chart has no borders, we can use offsetX and offsetY to get the coordonates relatively to the axis
                                    var x = self.dragDropPoint.series.xAxis.toValue(offset.x);
                                    var y = self.dragDropPoint.series.yAxis.toValue(offset.y);

                                    var dragMinX = self.graphData.xmin;
                                    var dragMinY = self.graphData.ymin;
                                    var dragMaxX = self.graphData.xmax;
                                    var dragMaxY = self.graphData.ymax;

                                    if (x < dragMinX) {
                                        x = dragMinX;
                                    } else if (x > dragMaxX) {
                                        x = dragMaxX;
                                    }
                                    if (y < dragMinY) {
                                        y = dragMinY;
                                    } else if (y > dragMaxY) {
                                        y = dragMaxY;
                                    }
                                    
                                    // Move point
                                    self.dragDropPoint.update({
                                        x: x,
                                        y: y
                                    }, true, false)
                                }

                            }).on('mouseup', function(e) {
                                // console.log('mouseup');
                                var offset = getOffset(e);

                                if (clickedPoint) {
                                    // Point clicked, delete the point if edition                           
                                    var serieIndex = clickedPoint.series.index;
                                    if (serieIndex === self.currentSerieIndex) {
                                        // Editable, delete point
                                        clickedPoint.remove(true);
                                    }
                                    clickedPoint = null;

                                } else if (self.dragDropPoint) {
                                    // console.log('drop point');
                                    self.dragDropPoint = null;

                                } else if (Math.abs(offset.x - clickedPosition.x) < 5 || Math.abs(offset.y - clickedPosition.y) < 5) {
                                    // console.log('click');

                                    var option = this as HighchartsChartObject;

                                    if (self.currentSerieIndex < 0) {
                                        return;
                                    }

                                    var currentSerie = self.chart.series[self.currentSerieIndex];

                                    // Because the chart has no borders, we can use offsetX and offsetY to get the coordonates relatively to the axis
                                    var x = currentSerie.xAxis.toValue(offset.x);
                                    var y = currentSerie.yAxis.toValue(offset.y);
                                
                                    // Add point
                                    currentSerie.addPoint([x, y]);
                                }

                            });

                            if (self.graphData.tubeGraph.c.length) {
                                self.bindCurves();
                            }
                            if (cb) {
                                cb();
                            };
                        });

                    } else if (forStep === 2) {
                        // Load empty chart
                        options.series = [{
                            name: 'empty'
                        }];

                        self.chartContainer.highcharts(options, function(c) {
                            self.chart = c;
                            if (cb) {
                                cb();
                            };
                        });
                    }
                });
            } else {
                if (self.finalChart) {
                    return;
                }

                requirejs(['anodeChartTransfert'], function() {                
                    // Place chart at the right place and right sizeChart
                    var unitindex = self.graphData.tubeGraph.unit || 0;
                    var unit = self.tuberesult.tubeData.units.length && self.tuberesult.tubeData.units[unitindex];
                    var options = {
                        element: self.imageContainer.find('#final-chart'),
                        tubeGraph: self.graphData.tubeGraph,
                        title: self.tuberesult.tubeData.name,
                        pmax: unit && unit.pamax,
                        editable: false,
                    } as AnodeTransfertChartOptions;
                    self.finalChart = new tct.AnodeTransfertChart(options);
                    if (cb) {
                        cb();
                    };
                })
            }
        }

        private loadSizer = function(cb: Function) {
            var self = this as Wizard;

            var next = function() {
                self.positionableElement.style.left = self.graphData.bounds.left + '%';
                self.positionableElement.style.top = self.graphData.bounds.top + '%';
                self.positionableElement.style.width = self.graphData.bounds.width + '%';
                self.positionableElement.style.height = self.graphData.bounds.height + '%';
                cb();
            }

            if (self.positionableElement) {
                next();
                return;
            }

            requirejs(['interact'], function(interact: Interact.InteractStatic) {
                self.positionableElement = self.imageContainer.find('#positionable')[0];
                self.interactable = interact(self.positionableElement)
                    .draggable({
                        onmove: function(event: Interact.InteractEvent) {
                            var target = $(event.target);

                            // keep the dragged position in the data-x/data-y attributes
                            var x = (parseFloat(target.attr('data-x')) || 0) + event.dx;
                            var y = (parseFloat(target.attr('data-y')) || 0) + event.dy;

                            var width = target.width();
                            var height = target.height();

                            self.graphData.bounds.left = 5 + x * 100 / width;
                            self.graphData.bounds.top = 5 + y * 100 / height;
                            
                            // update the posiion attributes
                            target.attr('data-x', x);
                            target.attr('data-y', y);

                            self.positionableElement.style.left = self.graphData.bounds.left + '%';
                            self.positionableElement.style.top = self.graphData.bounds.top + '%';
                        },
                    }).resizable({
                        preserveAspectRatio: false,
                        edges: { left: true, right: true, bottom: true, top: true }
                    }).on('resizemove', function(event: Interact.InteractEvent) {
                        var target = $(event.target);

                        var x = (parseFloat(target.attr('data-x')) || 0);
                        var y = (parseFloat(target.attr('data-y')) || 0);

                        // Calc width and height at 100%
                        var imageElement = self.imageContent[0] as any;
                        var width = self.imageContainer.width();
                        var height = width * imageElement.height / imageElement.width;
                        self.graphData.bounds.width = 100 * event.rect.width / width;
                        self.graphData.bounds.height = 100 * event.rect.height / height;
                        self.positionableElement.style.width = self.graphData.bounds.width + '%';
                        self.positionableElement.style.height = self.graphData.bounds.height + '%';

                        // translate when resizing from top or left edges
                        if (event.deltaRect.left) {
                            x += event.deltaRect.left;
                            target.attr('data-x', x);
                            self.graphData.bounds.left = 5 + x * 100 / event.rect.width;
                            self.positionableElement.style.left = self.graphData.bounds.left + '%';
                        }

                        if (event.deltaRect.top) {
                            y += event.deltaRect.top;
                            target.attr('data-y', y);
                            self.graphData.bounds.top = 5 + y * 100 / event.rect.height;
                            self.positionableElement.style.top = self.graphData.bounds.top + '%';
                        }

                        self.sizeChart();
                    });

                next();
            })
        }

        private addSerie = function(name: string, points?: Array<[number, number]>) {
            var self = this as Wizard;

            if (!points) {
                points = [[self.graphData.xmin, self.graphData.ymin]];
            }

            // Register event in case of dbl-click on point
            // Add a new serie with the new serie class instance and options
            var serieIndex = self.chart.series.length;
            var options = {
                allowPointSelect: false,
                cursor: 'pointer',
                data: points,
                name: name,
                color: self.colors[serieIndex % self.colors.length],
            } as HighchartsSeriesOptions;

            self.chart.addSeries(options, true);
            return serieIndex;
        }

        private bindCurves = function() {
            var self = this as Wizard;

            var gdatas = self.graphData.tubeGraph.c;
            gdatas.forEach(function(curve, index) {
                var points: Array<any> = [];
                curve.p.map(p => points.push([p.va, p.ik * 1000]));
                self.addSerie(curve.vg1 + 'V', points);
            });
        }

        public loadStep = function(step: number) {
            var self = this as Wizard;

            self.validator.hideValidationErrors();

            var verrors: Array<validation.Message>;
            if (step !== 3 && self.currentSerieIndex !== -1) {
                verrors = self.saveCurrentSerie();
            }

            if (!verrors || verrors.length === 0) {
                verrors = self.validateStep(step);
            }

            if (verrors && verrors.length) {
                $.translator.translateMessages(verrors, function(translated) {
                    self.validator.showValidationErrors(verrors);
                    self.messager.showMessages(verrors);
                })
                return self;
            }

            if (self.currentStep === step) {
                return self;
            }

            var layoutStep = function() {
                // Register current step in differents places
                self.currentStep = step || 1;
                self.stepContainer.attr('current-step', self.currentStep);
                self.stepper.stepTo(self.currentStep);

                // Add a new url state
                $.pageState.replaceUrlParam('step', self.currentStep.toString());
                
                // Set step title
                self.titleContent.find('#step-title').html(self.translations.find('#step-title-' + step).prop('outerHTML'));

                if (step === 2) {
                    self.loadControls();
                    self.adjustControlsMargin();
                    self.interactable.resizable(true);
                    self.interactable.draggable(true);
                    self.interactable.gesturable(true);

                } else if (step > 2) {
                    self.refreshSliders()
                    self.interactable.resizable(false);
                    self.interactable.draggable(false);
                    self.interactable.gesturable(false);
                    
                    // Save current work to local storage
                    localStorage.setItem(self.graphData.storageKey, JSON.stringify(self.graphData));
                }

                if (step >= 2) {
                    self.loadChart(step, function() {
                        self.sizeChart();
                    })
                }

                if (step >= 4) {
                    self.imageContent.css('opacity', 0);
                }
            }

            var showNext = function() {
                var stepc = self.stepContainer.find('.step' + step + ':not(:visible)').css('opacity', 0).show();
                layoutStep();
                stepc.animate({ opacity: 1 }, 300);
            }

            var hideCurrent = function() {
                if (self.currentStep) {
                    var $currentStep = self.stepContainer.find('.step' + self.currentStep + ':not(.step' + step + ')')
                    if ($currentStep.length) {
                        $currentStep.animate({ opacity: 0 }, 200);
                        setTimeout(function() {
                            $currentStep.hide();
                            showNext();
                        }, 250);
                    } else {
                        showNext();
                    }
                } else {
                    showNext();
                }
            }

            self.messager.hideMessages();
            self.loadSerie(-1);

            if (step === 1) {
                hideCurrent();

            } else if (step === 2) {
                if (self.chart) {
                    self.chart.destroy();
                    self.chart = undefined;
                }

                self.loadSizer(function() {
                    hideCurrent();
                })

            } else if (step > 2 && step < 4) {
                if (self.chart) {
                    self.chart.destroy();
                    self.chart = undefined;
                }
                if (self.finalChart) {
                    self.finalChart.destroy;
                    self.finalChart = undefined;
                }
                hideCurrent();

            } else {
                self.chart.destroy();
                self.chart = undefined;
                hideCurrent();
            }

            return self;
        }
    }
}

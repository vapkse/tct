var tct;
(function (tct) {
    var DocViewer = (function () {
        function DocViewer() {
        }
        DocViewer.prototype.view = function (opts) {
            var self = this;
            requirejs(['ajaxDownloadFile'], function () {
                var filename = opts.document.filename;
                var url = encodeURIComponent(filename);
                if (opts.isNew) {
                    url += '&isnew=true';
                }
                var fileInfo = /^.+\.([^.]+)$/.exec(filename);
                var ext = fileInfo.length > 1 && fileInfo[1].toLowerCase();
                switch (ext) {
                    case 'tds':
                        var options = {
                            url: '/download?url=' + url,
                            dataType: 'json'
                        };
                        utils.AjaxDownloadFile.download(options).onSuccessFinish(function (status, statusText, res, headers) {
                            var json = res['json'];
                            var result = typeof json === 'string' ? JSON.parse(json) : json;
                            if (result.error) {
                                if (opts.error) {
                                    opts.error(result.error.message);
                                }
                            }
                            else {
                                if (!result.tubeGraph.name) {
                                    result.tubeGraph.name = opts.document.description;
                                }
                                var viewerOptions = {
                                    tubeId: opts.tubeData._id,
                                    dataId: opts.document._id,
                                    tubeGraph: result.tubeGraph,
                                    graphTitle: opts.document.description,
                                    graphUrl: opts.document.filename,
                                    isNew: opts.isNew
                                };
                                if (!self.fileViewer) {
                                    requirejs(['fileViewer'], function () {
                                        self.fileViewer = $('[file-viewer]');
                                        self.fileViewer.fileViewer(viewerOptions).on('fileviewercreated', function () {
                                            self.fileViewer.fileViewer('show');
                                        });
                                    });
                                }
                                else {
                                    self.fileViewer.fileViewer('setOptions', viewerOptions);
                                }
                            }
                            if (opts.success) {
                                opts.success();
                            }
                        }).onErrorFinish(function (status, statusText, res, headers) {
                            if (opts.error) {
                                opts.error(statusText);
                            }
                        });
                        break;
                    case 'png':
                    case 'jpg':
                    case 'jpeg':
                    case 'gif':
                    case 'bmp':
                    case 'tif':
                    case 'tiff':
                        var options = {
                            url: '/download?url=' + url,
                            dataType: 'blob'
                        };
                        utils.AjaxDownloadFile.download(options).onSuccessFinish(function (status, statusText, res, headers) {
                            var downloadUrl = URL.createObjectURL(res['blob']);
                            var vwrOptions = {
                                tubeId: opts.tubeData._id,
                                dataId: opts.document._id,
                                file: downloadUrl,
                                isNew: opts.isNew
                            };
                            if (!self.fileViewer) {
                                requirejs(['fileViewer'], function () {
                                    self.fileViewer = $('[file-viewer]');
                                    self.fileViewer.fileViewer(vwrOptions).bind('fileviewercreated', function () {
                                        self.fileViewer.fileViewer('show');
                                    });
                                });
                            }
                            else {
                                self.fileViewer.fileViewer('setOptions', vwrOptions);
                            }
                            if (opts.success) {
                                opts.success();
                            }
                        }).onErrorFinish(function (status, statusText, res, headers) {
                            if (opts.error) {
                                opts.error(statusText);
                            }
                        });
                        break;
                    default:
                        var options = {
                            url: '/download?url=' + url,
                            dataType: 'blob'
                        };
                        utils.AjaxDownloadFile.download(options).onSuccessFinish(function (response, status, res, headers) {
                            var downloadUrl = URL.createObjectURL(res['blob']);
                            var a = document.createElement("a");
                            a.href = downloadUrl;
                            a.download = filename;
                            document.body.appendChild(a);
                            a.click();
                            if (opts.success) {
                                opts.success();
                            }
                        }).onErrorFinish(function (status, statusText, res, headers) {
                            if (opts.error) {
                                opts.error(statusText);
                            }
                        });
                        break;
                }
            });
            return self;
        };
        return DocViewer;
    })();
    tct.DocViewer = DocViewer;
})(tct || (tct = {}));

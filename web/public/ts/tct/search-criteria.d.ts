declare module tct {
    interface SearchCriteriaOptions {
        modelFields: {
            [name: string]: DataProviding.Field;
        };
        expression: tct.QueryExpression;
        template: JQuery;
        lookups: {
            [name: string]: any;
        };
        user: user.User;
    }
    interface SearchCriteriaRequiredEvent extends modern.ChosenRequiredEvent {
        fieldName: string;
        selectedValue: string;
    }
    class SearchCriteria {
        options: SearchCriteriaOptions;
        protected $: {
            fieldSelector: JQuery;
            fieldLookup: JQuery;
            fieldValue: JQuery;
            fieldMin: JQuery;
            fieldMax: JQuery;
            fieldWholeWord: JQuery;
        };
        protected element: JQuery;
        private _trigger;
        protected _create: () => void;
        private showError;
        private validateFieldSelection;
        expression(exp: tct.QueryExpression): QueryExpression;
        protected onsearch: () => void;
        protected onchange: () => void;
        protected ondelete: () => void;
    }
}
interface JQuery {
    searchCriteria(): JQuery;
    searchCriteria(options: tct.SearchCriteriaOptions): JQuery;
    searchCriteria(method: string, expression?: tct.QueryExpression): void;
    searchCriteria(method: 'expression', expression: tct.QueryExpression): void;
    searchCriteria(method: string): tct.QueryExpression;
    searchCriteria(method: 'expression'): tct.QueryExpression;
}

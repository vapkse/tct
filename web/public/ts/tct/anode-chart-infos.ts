'use strict';

module tct {
    export interface AnodeChartInfosOptions {
        infos: GraphInfos,
        usage: TubeUsage.TubeUsage
    }

    export class AnodeChartInfos {
        protected element: JQuery;
        protected options: AnodeChartInfosOptions;
        private $: {
            vg1zero: JQuery;
            vapp: JQuery;
            pmax: JQuery;
            gain: JQuery;
            trans: JQuery;
            ri: JQuery;
            h2: JQuery;
            h3: JQuery;
            h4: JQuery;
            htot: JQuery;
        }

        protected _create = function() {
            var self = this as AnodeChartInfos;

            // Resource need to be renderized, so the call must be like this
            $.get('/resource?r=widgets%2Fanode-chart-infos.html').done(function(html) {

                $(html).appendTo(self.element);

                self.$ = {
                    vg1zero: self.element.find('#vg1zero'),
                    vapp: self.element.find('#vapp'),
                    pmax: self.element.find('#pmax'),
                    gain: self.element.find('#gain'),
                    trans: self.element.find('#trans'),
                    ri: self.element.find('#ri'),
                    h2: self.element.find('#h2'),
                    h3: self.element.find('#h3'),
                    h4: self.element.find('#h4'),
                    htot: self.element.find('#htot'),
                }

                self.refresh();
            })
        }

        public setOptions(options: AnodeChartInfosOptions) {
            var self: AnodeChartInfos = this;
            self.options = options;
            self.refresh();
        }

        public refresh() {
            var self: AnodeChartInfos = this;
            var infos = self.options.infos;
            var mode = self.options.usage.mode.value;

            if (!self.$ || !infos || !mode || mode === 'na') {
                self.element.hide();
                return;
            }

            self.element.show();
            var vg1zero = infos.workingPoint && infos.workingPoint.vg1;
            self.$.vg1zero.html(!isNaN(vg1zero) && vg1zero !== null ? vg1zero.toFixed(2) + 'V' : '-');

            /* Calc pmax with ui methode 
            var pmax = 0;
            var vap = 0;
            var iap = 0;
            if (self.options.usage) {
                if (self.options.usage.mode.value === 'se') {
                    vap = infos.vg1Max && infos.vg1Min && (infos.vg1Max.va - infos.vg1Min.va) / 2;
                    iap = infos.vg1Max && infos.vg1Min && (infos.vg1Min.ia - infos.vg1Max.ia) / 2;
                    pmax = vap && iap && vap * iap / 2000;
                }
                else {
                    vap = infos.vg1Max && infos.workingPoint && infos.workingPoint.va - infos.vg1Max.va;
                    iap = infos.vg1Max && infos.workingPoint && infos.vg1Max.ia - infos.workingPoint.ia;
                    pmax = vap && iap && vap * iap / 2000;
                    // Calc new load param
                    switch (self.options.usage.mode.value) {
                        case '2pp':
                            pmax *= 2;
                            break;
                        case '3pp':
                            pmax *= 3;
                            break;
                        case '4pp':
                            pmax *= 4;
                            break;
                        case '5pp':
                            pmax *= 5;
                            break;
                        case '6pp':
                            pmax *= 6;
                            break;
                    }
                }
            }*/

            /* Calc pmax with ui2 methode 
            var pmax = 0;
            var vapp = 0;
            if (self.options.usage) {
                if (infos.vg1Max && infos.vg1Min) {
                    vapp = infos.vg1Min.va - infos.vg1Max.va;
                    var iapp = infos.vg1Max.ia - infos.vg1Min.ia;
                    pmax = vapp * iapp / 8000;
                }
                if (self.options.usage.mode.value !== 'se') {
                    pmax *= 2;     
                }
            } */

            /* Calc pmax with z1 methode */
            var pmax = 0;
            var vapp = 0;
            var vp = 0;
            if (self.options.usage) {
                // Calc new load param
                if (infos.vg1Max && infos.vg1Min) {
                    vp = infos.vg1Min.va - infos.vg1Max.va;
                    if (self.options.usage.mode.value !== 'se') {
                        vp *= 2;                       
                        var vcross = infos.ab && (infos.ab.va - 0.15 * infos.ab.va);
                        if (vcross <= infos.vg1Min.va && vcross >= infos.vg1Max.va) {
                            var vaa = infos.vg1Min.va - vcross;
                            var vab = vcross - infos.vg1Max.va;
                            vp = (vaa + vab * 2) * 2;
                        } 
                    }
                }
                pmax = self.options.usage.load.value && vp && vp * vp / 8 / self.options.usage.load.value;

                // Calc voltage peak-peak anode-anode
                if (infos.workingPoint && infos.vg1Max) {
                    if (self.options.usage.mode.value === 'se') {
                        vapp = vp;
                    } else {
                        vapp = 2 * (infos.workingPoint.va - infos.vg1Max.va);                        
                    }
                }
            }
                       
            /* Calc pmax with z2 methode       
            var pmax = 0;
            var vapp = 0;
            if (self.options.usage) {
                if (self.options.usage.mode.value === 'se') {
                    if (infos.vg1Max && infos.vg1Min) {
                        vapp = infos.vg1Min.va - infos.vg1Max.va;
                        pmax = self.options.usage.load.value && vapp * vapp / 8 / self.options.usage.load.value;
                    }
                }
                else {
                    var vaa = 0;
                    var vab = 0;

                    // Calc new load param
                    var load = self.options.usage.load.value;
                    /* switch (self.options.usage.mode.value) {
                        case '2pp':
                            load /= 2;
                            break;
                        case '3pp':
                            load /= 3;
                            break;
                        case '4pp':
                            load /= 4;
                            break;
                        case '5pp':
                            load /= 5;
                            break;
                        case '6pp':
                            load /= 6;
                            break;
                    }*/

            /* if (infos.vg1Max && infos.vg1Min) {
                var vcross = infos.ab;
                if (vcross && vcross.va <= infos.vg1Min.va && vcross.va >= infos.vg1Max.va) {
                    vaa = infos.vg1Min.va - vcross.va;
                    vab = vcross.va - infos.vg1Max.va;
                } else {
                    vaa = infos.vg1Min.va - infos.vg1Max.va;
                    vab = 0;
                }
            }
            
            vapp = 2 * (vaa + vab);
            var pa = load && vaa * vaa / load;
            var pb = load && 2 * vab * vab / load; 
            pmax = pa + pb;
        }
    } */

            if (pmax) {
                var unit = 'W';
                if (pmax < 1) {
                    pmax = pmax * 1000;
                    unit = 'mW';
                }

                if (pmax < 10) {
                    self.$.pmax.html(pmax.toFixed(2) + unit);
                } else if (pmax < 100) {
                    self.$.pmax.html(pmax.toFixed(1) + unit);
                } else {
                    self.$.pmax.html(pmax.toFixed(0) + unit);
                }
            } else {
                self.$.pmax.html('-');
            }

            self.$.vapp.html(!isNaN(vapp) && vapp !== null ? vapp.toFixed(0) + 'Vpp' : '-');

            var gain = infos.vg1Min && infos.vg1Max && (infos.vg1Min.va - infos.vg1Max.va) / (infos.vg1Max.vg1 - infos.vg1Min.vg1);
            self.$.gain.html(!isNaN(gain) ? gain.toFixed(1) + 'V/V' : '-');

            self.$.trans.html(!isNaN(infos.s) ? infos.s.toFixed(2) + 'mA/V' : '-');
            self.$.ri.html(!isNaN(infos.ri) ? infos.ri.toFixed(2) + 'kohms' : '-');

            var hInfos = infos.calcHarmonics();
            var htot = hInfos.h2 + hInfos.h3 + hInfos.h4;
            self.$.h2.html(!isNaN(hInfos.h2) ? hInfos.h2.toFixed(2) + '%' : '-');
            self.$.h3.html(!isNaN(hInfos.h3) ? hInfos.h3.toFixed(2) + '%' : '-');
            self.$.h4.html(!isNaN(hInfos.h4) ? hInfos.h4.toFixed(2) + '%' : '-');
            self.$.htot.html(!isNaN(htot) ? htot.toFixed(2) + '%' : '-');
        }
    }
}

$.widget("tct.anodeChartInfos", new tct.AnodeChartInfos());

interface JQuery {
    anodeChartInfos(): JQuery;
    anodeChartInfos(method: string): JQuery;
    anodeChartInfos(method: string, options: tct.AnodeChartInfosOptions): JQuery;
    anodeChartInfos(method: 'setOptions', options: tct.AnodeChartInfosOptions): JQuery;
    anodeChartInfos(method: 'refresh'): JQuery;
    anodeChartInfos(options: tct.AnodeChartInfosOptions): JQuery;
}
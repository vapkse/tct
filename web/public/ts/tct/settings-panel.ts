'use strict';

module tct {
    export interface SettingsPanelEvent extends JQueryEventObject {
        data: SettingsPanelOptions
    }

    export interface SettingsPanel extends JQueryUI.Widget {
        refresh(): void,
        show(): void,
        hide(): void,
        toogle(): void
    }

    export interface SettingsPanelOptions extends Settings {

    }

    export class SettingsPanel {
        private options: SettingsPanelOptions;
        private backdrop: JQuery;
        private element: JQuery;
        private _trigger: (eventName: string, event: JQueryEventObject, data: SettingsPanelOptions) => void;
        private _create() {
            var self = <SettingsPanel>this;

            var options = <SettingsPanelOptions>this.options;
            
            // Resource need to be renderized, so the call must be like this
            $.get('/resource?r=widgets%2Fsettings-panel.html').done(function(html) {
                self.element.append(html).addClass('charm right-side padding20 bg-dark fg-lighter');

                self.backdrop = $('<div class="backdrop bg-transparent"></div>').appendTo('body').on('click', function() {
                    self.hide();
                })

                $('#color-settings-panel').colorSettings(options.colorSettings).bind('colorsettingschange', function(e: modern.ColorSettingsEvent) {
                    var data = new Settings();
                    data.colorSettings.scheme = e.scheme;
                    options.colorSettings.scheme = e.scheme;
                    data.colorSettings.theme = e.theme;
                    options.colorSettings.theme = e.theme;
                    self._trigger('change', e, data);

                }).bind('colorsettingsenter', function(e: modern.ColorSettingsEvent) {
                    var data = new Settings();
                    data.colorSettings.scheme = e.scheme;
                    data.colorSettings.theme = e.theme;
                    self._trigger('change', e, data);

                }).bind('colorsettingsleave', function(e: modern.ColorSettingsEvent) {
                    var data = new Settings();
                    data.colorSettings.scheme = options.colorSettings.scheme;
                    data.colorSettings.theme = options.colorSettings.theme;
                    self._trigger('change', e, data);
                });

                self.element.find('button[settings-panel]').on('click', function() {
                    self.hide();
                });
                self.oncreated();
            });
        };

        protected oncreated = function() {
            // For overrides
        }

        public toogle() {
            var self = <SettingsPanel>this;
            if (self.element.data('hidden') === undefined) { self.element.data('hidden', true); }
            if (!self.element.data('hidden')) {
                self.hide();
            } else {
                self.show();
            }
        }
        public show() {
            var self = <SettingsPanel>this;
            self.element.animate({
                right: 0
            });
            if (self.backdrop) {
                self.backdrop.show();
            }
            self.element.data('hidden', false);
        }
        public hide() {
            var self = <SettingsPanel>this;
            self.element.animate({
                right: '-29rem'
            });
            self.backdrop.hide();
            self.element.data('hidden', true);
        }
    }
}

$.widget("tct.settingsPanel", new tct.SettingsPanel());

interface JQuery {
    settingsPanel(): tct.SettingsPanel;
    settingsPanel(method: string): tct.SettingsPanel;
    settingsPanel(options: tct.SettingsPanelOptions): tct.SettingsPanel;
}
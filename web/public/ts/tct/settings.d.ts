declare module tct {
    class Settings {
        colorSettings: {
            scheme: string;
            theme: string;
        };
        change: (e: JQueryEventObject, data: Settings) => void;
        constructor(storage?: Storage);
    }
}

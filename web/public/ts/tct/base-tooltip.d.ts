declare module tct {
    class BaseTooltip {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _create: () => void;
        protected oncreated: () => void;
    }
}
interface JQuery {
    baseTooltip(): JQuery;
}

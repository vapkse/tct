'use strict';

module tct {
    export interface SearchCriteriaOptions {
        modelFields: { [name: string]: DataProviding.Field },
        expression: tct.QueryExpression,
        template: JQuery,
        lookups: { [name: string]: any },
        user: user.User
    }

    export interface SearchCriteriaRequiredEvent extends modern.ChosenRequiredEvent {
        fieldName: string,
        selectedValue: string
    }

    export class SearchCriteria {
        public options: SearchCriteriaOptions;
        protected $: {
            fieldSelector: JQuery,
            fieldLookup: JQuery,
            fieldValue: JQuery,
            fieldMin: JQuery,
            fieldMax: JQuery,
            fieldWholeWord: JQuery,
        }
        protected element: JQuery;
        private _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _create = function() {
            var self = <SearchCriteria>this;
            var onchangeTimeout: number;

            var defaultExpression = {
                fieldName: '_text',
                operator: '=',
                value: '',
                wholeWord: false
            } as tct.QueryExpression

            self.element.append(self.options.template);

            var onchangedelayed = function() {
                if (onchangeTimeout) {
                    clearTimeout(onchangeTimeout);
                    onchangeTimeout = 0;
                }

                onchangeTimeout = setTimeout(function() {
                    onchangeTimeout = 0;
                    self.onchange();
                }, 100);
            }

            // Advanced fields
            self.$ = {
                fieldSelector: self.element.find('#field-selector').hide(),
                fieldLookup: self.element.find('#field-lookup').hide().bind('chosen2change', onchangedelayed),
                fieldValue: self.element.find('#field-value').hide().textinput({}).bind('textinputchange', onchangedelayed),
                fieldMin: self.element.find('#field-min').hide().numericinput({ decimalCount: 6 }).bind('numericinputchange', onchangedelayed),
                fieldMax: self.element.find('#field-max').hide().numericinput({ decimalCount: 6 }).bind('numericinputchange', onchangedelayed),
                fieldWholeWord: self.element.find('#whole-word').hide().checkbox({}).bind('checkboxchange', onchangedelayed),
            }

            self.element.find('#remove-field').on('click', function() {
                self.ondelete();
            })

            self.element.find('input').on('keypress', function(e: JQueryKeyEventObject) {
                if (e.keyCode === 13) {
                    self.onsearch();
                }
            })

            // Field combobox
            var items: Array<{ name: string, text: string }> = [];
            if (self.options.user && self.options.user.role === 'admin') {
                items.push({
                    name: 'user',
                    text: 'user'
                });
            }
            Object.keys(self.options.modelFields).map(function(name: string) {
                items.push({
                    name: name,
                    text: name.match(/_?(.*)/)[1] // trim underscore for system fields
                });
            })

            var options: modern.ChosenOptions = {
                valueField: 'name',
                textField: 'text',
                items: items
            }

            self.$.fieldSelector.show().chosen2(options).bind('chosen2change', function(e: modern.ChosenEvent) {
                var expression = $.extend({}, defaultExpression, {
                    fieldName: e.value,
                }) as QueryExpression;
                if (e.value === 'user') {
                    expression.value = 'all';
                }
                self.validateFieldSelection(expression);
                onchangedelayed();
            });

            // Look up combobox
            self.$.fieldLookup.chosen2({});

            setTimeout(function() {
                self.expression(self.options.expression || defaultExpression);
            }, 0);
        }

        private showError = function(msg: string) {
            var messagerControl: modern.Messager = $('#messager').messager().data('modern-messager');
            if (messagerControl) {
                messagerControl.show('error', msg);
            }
        }

        private validateFieldSelection = function(exp?: tct.QueryExpression) {
            var self: SearchCriteria = this;

            var fieldDescription = self.options.modelFields[exp.fieldName];
            var type = fieldDescription && fieldDescription.type || 'string';
            var lookupValue: string;

            switch (type) {
                case 'number':
                    self.$.fieldMin.show();
                    self.$.fieldMax.show();
                    self.$.fieldValue.hide();
                    self.$.fieldLookup.hide();
                    self.$.fieldWholeWord.hide();

                    // Bind expression
                    var from: number;
                    var to: number;
                    if (exp.value !== undefined) {
                        from = exp.value as number;
                        to = from;
                    } else {
                        from = exp.from;
                        to = exp.to;
                    }
                    self.$.fieldMin.numericinput('value', from);
                    self.$.fieldMax.numericinput('value', to);
                    break;

                case 'string':
                    self.$.fieldMin.hide();
                    self.$.fieldMax.hide();
                    self.$.fieldValue.show();
                    self.$.fieldLookup.hide();
                    self.$.fieldWholeWord.show();

                    // Bind expression
                    self.$.fieldValue.textinput('value', exp.value.toString())
                    break;

                default:
                    // Lookups
                    self.$.fieldMin.hide();
                    self.$.fieldMax.hide();
                    self.$.fieldValue.hide();
                    self.$.fieldLookup.show();
                    self.$.fieldLookup.show().chosen2('clearItems');
                    self.$.fieldWholeWord.hide();

                    // Bind values and selection
                    self.$.fieldLookup.chosen2('setOptions', {
                        items: self.options.lookups[exp.fieldName],
                        selectedValue: exp.value.toString(),
                        textField: fieldDescription.textField,
                        valueField: fieldDescription.valueField
                    } as modern.ChosenOptions);
                    break;
            }

            // Bind lookup expression
            if (lookupValue) {
            }
        }

        public expression(exp: tct.QueryExpression) {
            var self: SearchCriteria = this;

            if (exp !== undefined) {
                self.$.fieldSelector.chosen2('selectedValue', exp.fieldName);
                self.$.fieldWholeWord.checkbox('value', exp.wholeWord);
                self.validateFieldSelection(exp);
                self.options.expression = exp;
                return exp;
            }

            var self: SearchCriteria = this;
            var qe = {
                fieldName: self.$.fieldSelector.chosen2('selectedValue'),
            } as QueryExpression

            var fieldDescription = self.options.modelFields[qe.fieldName];
            var type = fieldDescription && fieldDescription.type || 'string';
            switch (type) {
                case 'number':
                    var from = self.$.fieldMin.numericinput('value');
                    var to = self.$.fieldMax.numericinput('value');
                    if (Math.abs(from - to) < 0.0000001) {
                        qe.value = from;
                        qe.from = undefined;
                        qe.to = undefined;
                    } else {
                        qe.value = undefined;
                        if (!isNaN(from) && !isNaN(to) && from !== null && to !== null && from > to) {
                            qe.from = to;
                            qe.to = from;
                        } else {
                            qe.from = from;
                            qe.to = to;
                        }
                    }
                    qe.wholeWord = false;
                    break;

                case 'objectid':
                    qe.value = self.$.fieldLookup.chosen2('selectedValue');
                    qe.wholeWord = false;
                    break;

                default:
                    qe.value = self.$.fieldValue.textinput('value');
                    qe.wholeWord = self.$.fieldWholeWord.checkbox('value');
                    break;
            }

            return qe;
        }

        protected onsearch = function() {
            var self: SearchCriteria = this;
            var e = jQuery.Event('search');
            self._trigger('search', e);
        }

        protected onchange = function() {
            var self: SearchCriteria = this;
            var e = jQuery.Event('change');
            self._trigger('change', e);
        }

        protected ondelete = function() {
            var self: SearchCriteria = this;
            var e = jQuery.Event('delete');
            self._trigger('delete', e);
        }
    }
}

$.widget("tct.searchCriteria", new tct.SearchCriteria());

interface JQuery {
    searchCriteria(): JQuery;
    searchCriteria(options: tct.SearchCriteriaOptions): JQuery;
    searchCriteria(method: string, expression?: tct.QueryExpression): void;
    searchCriteria(method: 'expression', expression: tct.QueryExpression): void;
    searchCriteria(method: string): tct.QueryExpression;
    searchCriteria(method: 'expression'): tct.QueryExpression;
}
declare module tct {
    interface FileViewerOptions {
        tubeId: number;
        dataId: string;
        file?: string;
        tubeGraph?: TubeGraph;
        graphTitle?: string;
        graphUrl?: string;
        unitIndex?: number;
        isNew: boolean;
    }
    class FileViewer {
        private options;
        private backdrop;
        private window;
        private element;
        private chart;
        private docViewer;
        private download;
        private usage;
        private create;
        private dropDownMenu;
        private tubeGraph;
        private messagerControl;
        private _trigger;
        private _create();
        private createFunction;
        private showDownloadMenu;
        protected oncreated: () => void;
        refresh(): void;
        setOptions(options: FileViewerOptions): FileViewer;
        show(): FileViewer;
        hide(): FileViewer;
    }
}
interface JQuery {
    fileViewer(): JQuery;
    fileViewer(method: string): tct.FileViewer;
    fileViewer(method: 'show'): tct.FileViewer;
    fileViewer(method: 'hide'): tct.FileViewer;
    fileViewer(options: tct.FileViewerOptions): JQuery;
    fileViewer(method: string, options: tct.FileViewerOptions): tct.FileViewer;
    fileViewer(method: 'setOptions', options: tct.FileViewerOptions): tct.FileViewer;
}

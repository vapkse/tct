declare module tct {
    interface DocSelectorOptions {
        tubeData: TubeData;
        title: string;
        mode: string;
        selectedIds?: Array<string>;
        filter?: RegExp;
        showUsages: boolean;
        userid?: string;
    }
    interface DocSelectorEvent extends JQueryEventObject {
        selectedIds: Array<string>;
    }
    class DocSelector {
        private options;
        private backdrop;
        private window;
        private docViewer;
        private docTemplate;
        private docContainer;
        private element;
        private messagerControl;
        private _trigger;
        private _create();
        protected oncreated: () => void;
        protected onokclicked: () => void;
        refresh(): void;
        setOptions(options: DocSelectorOptions): DocSelector;
        show(): DocSelector;
        hide(): DocSelector;
    }
}
interface JQuery {
    docSelector(): JQuery;
    docSelector(method: string): tct.DocSelector;
    docSelector(method: 'show'): tct.DocSelector;
    docSelector(method: 'hide'): tct.DocSelector;
    docSelector(options: tct.DocSelectorOptions): JQuery;
    docSelector(method: string, options: tct.DocSelectorOptions): tct.DocSelector;
    docSelector(method: 'setOptions', options: tct.DocSelectorOptions): tct.DocSelector;
}

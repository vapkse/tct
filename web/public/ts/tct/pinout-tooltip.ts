'use strict';

module tct {
    export class PinoutTooltip {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _create = function() {
            var self: PinoutTooltip = this;

            self.element.addClass('pinoutTooltip inline bg-transparent fg-ligter');
                       
            // Search base to display
            var base = self.element.closest('.tube-result').find('[data-bind="pinout"]').text();
            if (base && base !== '-') {
                $.get('/resource?img=images%2Fpinouts%2F' + base + '.svg').done(function(svg) {
                    if (svg) {
                        self.element.html(svg).find('svg').attr('width', '100%').attr('height', '100%').find('g').attr('class', 'fl-lighter');
                        setTimeout(function() {
                            self.oncreated();
                        }, 100);
                    }
                })
            } else {
                self.element.text('-')
            }
        };

        protected oncreated = function() {
            var self: PinoutTooltip = this;
            var e = jQuery.Event("created");
            self._trigger('created', e);
        }
    }
}

$.widget("tct.pinoutTooltip", new tct.PinoutTooltip())

interface JQuery {
    PinoutTooltip(): JQuery,
}

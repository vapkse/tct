'use strict';
var tct;
(function (tct) {
    var BaseTooltip = (function () {
        function BaseTooltip() {
            this._create = function () {
                var self = this;
                self.element.addClass('baseTooltip inline bg-transparent fg-ligter');
                var base = self.element.closest('.tube-result').find('[data-bind="base"]').text();
                if (base && base !== '-') {
                    $.get('/resource?img=images%2Fbases%2F' + base + '.svg').done(function (svg) {
                        if (svg) {
                            self.element.html(svg).find('svg').attr('width', '100%').attr('height', '100%').find('g').attr('class', 'fl-lighter');
                            setTimeout(function () {
                                self.oncreated();
                            }, 100);
                        }
                    });
                }
                else {
                    self.element.text('-');
                }
            };
            this.oncreated = function () {
                var self = this;
                var e = jQuery.Event("created");
                self._trigger('created', e);
            };
        }
        return BaseTooltip;
    })();
    tct.BaseTooltip = BaseTooltip;
})(tct || (tct = {}));
$.widget("tct.baseTooltip", new tct.BaseTooltip());

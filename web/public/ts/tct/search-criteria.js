'use strict';
var tct;
(function (tct) {
    var SearchCriteria = (function () {
        function SearchCriteria() {
            this._create = function () {
                var self = this;
                var onchangeTimeout;
                var defaultExpression = {
                    fieldName: '_text',
                    operator: '=',
                    value: '',
                    wholeWord: false
                };
                self.element.append(self.options.template);
                var onchangedelayed = function () {
                    if (onchangeTimeout) {
                        clearTimeout(onchangeTimeout);
                        onchangeTimeout = 0;
                    }
                    onchangeTimeout = setTimeout(function () {
                        onchangeTimeout = 0;
                        self.onchange();
                    }, 100);
                };
                self.$ = {
                    fieldSelector: self.element.find('#field-selector').hide(),
                    fieldLookup: self.element.find('#field-lookup').hide().bind('chosen2change', onchangedelayed),
                    fieldValue: self.element.find('#field-value').hide().textinput({}).bind('textinputchange', onchangedelayed),
                    fieldMin: self.element.find('#field-min').hide().numericinput({ decimalCount: 6 }).bind('numericinputchange', onchangedelayed),
                    fieldMax: self.element.find('#field-max').hide().numericinput({ decimalCount: 6 }).bind('numericinputchange', onchangedelayed),
                    fieldWholeWord: self.element.find('#whole-word').hide().checkbox({}).bind('checkboxchange', onchangedelayed),
                };
                self.element.find('#remove-field').on('click', function () {
                    self.ondelete();
                });
                self.element.find('input').on('keypress', function (e) {
                    if (e.keyCode === 13) {
                        self.onsearch();
                    }
                });
                var items = [];
                if (self.options.user && self.options.user.role === 'admin') {
                    items.push({
                        name: 'user',
                        text: 'user'
                    });
                }
                Object.keys(self.options.modelFields).map(function (name) {
                    items.push({
                        name: name,
                        text: name.match(/_?(.*)/)[1]
                    });
                });
                var options = {
                    valueField: 'name',
                    textField: 'text',
                    items: items
                };
                self.$.fieldSelector.show().chosen2(options).bind('chosen2change', function (e) {
                    var expression = $.extend({}, defaultExpression, {
                        fieldName: e.value,
                    });
                    if (e.value === 'user') {
                        expression.value = 'all';
                    }
                    self.validateFieldSelection(expression);
                    onchangedelayed();
                });
                self.$.fieldLookup.chosen2({});
                setTimeout(function () {
                    self.expression(self.options.expression || defaultExpression);
                }, 0);
            };
            this.showError = function (msg) {
                var messagerControl = $('#messager').messager().data('modern-messager');
                if (messagerControl) {
                    messagerControl.show('error', msg);
                }
            };
            this.validateFieldSelection = function (exp) {
                var self = this;
                var fieldDescription = self.options.modelFields[exp.fieldName];
                var type = fieldDescription && fieldDescription.type || 'string';
                var lookupValue;
                switch (type) {
                    case 'number':
                        self.$.fieldMin.show();
                        self.$.fieldMax.show();
                        self.$.fieldValue.hide();
                        self.$.fieldLookup.hide();
                        self.$.fieldWholeWord.hide();
                        var from;
                        var to;
                        if (exp.value !== undefined) {
                            from = exp.value;
                            to = from;
                        }
                        else {
                            from = exp.from;
                            to = exp.to;
                        }
                        self.$.fieldMin.numericinput('value', from);
                        self.$.fieldMax.numericinput('value', to);
                        break;
                    case 'string':
                        self.$.fieldMin.hide();
                        self.$.fieldMax.hide();
                        self.$.fieldValue.show();
                        self.$.fieldLookup.hide();
                        self.$.fieldWholeWord.show();
                        self.$.fieldValue.textinput('value', exp.value.toString());
                        break;
                    default:
                        self.$.fieldMin.hide();
                        self.$.fieldMax.hide();
                        self.$.fieldValue.hide();
                        self.$.fieldLookup.show();
                        self.$.fieldLookup.show().chosen2('clearItems');
                        self.$.fieldWholeWord.hide();
                        self.$.fieldLookup.chosen2('setOptions', {
                            items: self.options.lookups[exp.fieldName],
                            selectedValue: exp.value.toString(),
                            textField: fieldDescription.textField,
                            valueField: fieldDescription.valueField
                        });
                        break;
                }
                if (lookupValue) {
                }
            };
            this.onsearch = function () {
                var self = this;
                var e = jQuery.Event('search');
                self._trigger('search', e);
            };
            this.onchange = function () {
                var self = this;
                var e = jQuery.Event('change');
                self._trigger('change', e);
            };
            this.ondelete = function () {
                var self = this;
                var e = jQuery.Event('delete');
                self._trigger('delete', e);
            };
        }
        SearchCriteria.prototype.expression = function (exp) {
            var self = this;
            if (exp !== undefined) {
                self.$.fieldSelector.chosen2('selectedValue', exp.fieldName);
                self.$.fieldWholeWord.checkbox('value', exp.wholeWord);
                self.validateFieldSelection(exp);
                self.options.expression = exp;
                return exp;
            }
            var self = this;
            var qe = {
                fieldName: self.$.fieldSelector.chosen2('selectedValue'),
            };
            var fieldDescription = self.options.modelFields[qe.fieldName];
            var type = fieldDescription && fieldDescription.type || 'string';
            switch (type) {
                case 'number':
                    var from = self.$.fieldMin.numericinput('value');
                    var to = self.$.fieldMax.numericinput('value');
                    if (Math.abs(from - to) < 0.0000001) {
                        qe.value = from;
                        qe.from = undefined;
                        qe.to = undefined;
                    }
                    else {
                        qe.value = undefined;
                        if (!isNaN(from) && !isNaN(to) && from !== null && to !== null && from > to) {
                            qe.from = to;
                            qe.to = from;
                        }
                        else {
                            qe.from = from;
                            qe.to = to;
                        }
                    }
                    qe.wholeWord = false;
                    break;
                case 'objectid':
                    qe.value = self.$.fieldLookup.chosen2('selectedValue');
                    qe.wholeWord = false;
                    break;
                default:
                    qe.value = self.$.fieldValue.textinput('value');
                    qe.wholeWord = self.$.fieldWholeWord.checkbox('value');
                    break;
            }
            return qe;
        };
        return SearchCriteria;
    })();
    tct.SearchCriteria = SearchCriteria;
})(tct || (tct = {}));
$.widget("tct.searchCriteria", new tct.SearchCriteria());

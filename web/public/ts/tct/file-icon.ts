'use strict';

module tct {
    export interface FileIconOptions {
        filename: string
    }

    export class FileIcon {
        private options: FileIconOptions;
        private element: JQuery;
        private _create() {
            var self = this as FileIcon;
            self.refresh();
        }
        
        public setOptions(options: FileIconOptions) {
            var self = this as FileIcon;
            self.options = options;
            self.refresh();
            return self;       
        }
        
        public refresh() {
            var self = this as FileIcon;
            var reext = /^.+\.([^.]+)$/.exec(self.options.filename);
            var ext = reext && reext.length > 1 ? reext[1].toLowerCase() : '';
            self.element;
            switch (ext) {
                case 'pdf':
                    self.element.attr('class', 'fileicon mif-file-pdf').attr('tooltip', 'pdf');
                    break;
                case 'tdf':
                case 'tds':
                    self.element.attr('class', 'fileicon icon-curves').attr('tooltip', 'graph');
                    break;
                case 'png':
                case 'jpg':
                case 'jpeg':
                case 'gif':
                case 'bmp':
                case 'tif':
                case 'tiff':
                    self.element.attr('class', 'fileicon mif-file-image').attr('tooltip', 'img');
                    break;
                default:
                    self.element.attr('class', 'fileicon mif-file-empty').attr('tooltip', 'doc');
                    break;
            }     
            
            return self;       
        };
    }
}

$.widget("tct.fileIcon", new tct.FileIcon());

interface JQuery {
    fileIcon(): JQuery;
    fileIcon(method: string, options: tct.FileIconOptions): tct.FileIcon;
    fileIcon(method: 'setOptions', options: tct.FileIconOptions): tct.FileIcon;
    fileIcon(method: 'refresh'): tct.FileIcon;
    fileIcon(method: 'instance'): tct.FileIcon;
    fileIcon(options: tct.FileIconOptions): JQuery;
}
declare module tct {
    class PinoutTooltip {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _create: () => void;
        protected oncreated: () => void;
    }
}
interface JQuery {
    PinoutTooltip(): JQuery;
}

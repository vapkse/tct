'use strict';

module tct {
    export interface AnodeTransfertChartOptions extends TubeChartOptions {
        tubeGraph?: TubeGraph,
        pmax?: number,
        editable: boolean,
        ready?: Function
    }

    interface ClickInfos {
        clickedPosition?: { x: number, y: number },
        clickedElement?: JQuery
    }

    interface DragDropInfos extends ClickInfos {
        name?: string,
        point?: HighchartsPointObject,
        serie?: HighchartsSeriesObject,
        startX?: number,
        startY?: number;
        dragdropStarted?: boolean,
        extremesX?: HighchartsExtremes,
        extremesY?: HighchartsExtremes,
        workingPoint?: VPoint
    }

    export interface VPoint {
        vg1: number,
        va: number,
        ia: number,
        classA: boolean,
        overflow?: boolean,
        vt?: number,
        it?: number,
    }

    export interface LoadLine {
        vazero: number,
        iazero: number,
        vamin: number,
        vamax: number,
        iamin: number,
        iamax: number,
        load: number
    }

    export interface GraphInfos {
        loadPoints: Array<VPoint>;
        workingPoint: VPoint,
        aLoad: LoadLine,
        bLoad: LoadLine,
        vg1Max: VPoint,
        vg1Min: VPoint,
        ab: VPoint,
        s: number,
        ri: number,
        calcHarmonics: () => {
            h2: number,
            h3: number,
            h4: number
        }
    }

    export class AnodeTransfertChart extends TubeChart {
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected options: AnodeTransfertChartOptions;
        protected longClickMenu: JQuery;

        // Modified highcharts.d.ts for addSerie function
        private workingPointSerie: HighchartsSeriesObject;
        private workingPointSerieIndex: number;
        private classASerie: HighchartsSeriesObject;
        private classASerieIndex: number;
        private classBSerie: HighchartsSeriesObject;
        private classBSerieIndex: number;
        private crossSerie: HighchartsSeriesObject;
        private crossSerieIndex: number;
        private vinSerie0: HighchartsSeriesObject;
        private vinSerie0Index: number;
        private vinSerie1: HighchartsSeriesObject;
        private vinSerie1Index: number;
        private pmaxSerie: HighchartsSeriesObject;
        private pmaxSerieIndex: number;
        private crossPointsData: Array<VPoint>;
        private abPoint: { va: number, ia: number };
        private aLoad: LoadLine;
        private bLoad: LoadLine;
        private vg1Max: VPoint;
        private vg1Min: VPoint;
        private seriesVisibility: any;
        private serieEvents: any;
        private browserVersion: BrowserVersion;

        constructor(options: AnodeTransfertChartOptions) {
            super();
            super.contructor(options);
            var self: AnodeTransfertChart = this;
            self.browserVersion = window.browserVersion();
            self.seriesVisibility = self.options.usage ? self.options.usage.series.value : {};

            self.serieEvents = {
                show: function() {
                    if (!self.options.usage) {
                        return;
                    }
                    self.options.usage.series.value[this.options.id] = true;
                    self.refreshLines(true);
                    self.onoptionschanged();
                },
                hide: function() {
                    self.options.usage.series.value[this.options.id] = false;
                    self.refreshLines(true);
                    self.onoptionschanged();
                }
            }
        };

        protected oncreated() {
            var self: AnodeTransfertChart = this;
            super.oncreated();
            self.options.element.addClass('anode-transfert-chart modern-highcharts');
        }

        public getInfos = function() {
            var self = this as AnodeTransfertChart;

            if (!self.options.usage || !self.classASerie) {
                return null;
            }

            var wu = self.options.usage.va.value;
            var wp = wu && self.calcPointFromVa(wu, true);

            // calc transconductance and internal resistance
            var calcSAndRi = function(): { s?: number, ri?: number } {
                if (!wp) {
                    return {};
                }
                var pts = self.getNearestTrueCrossingLinePoints(wp.va);
                if (!pts.p1) {
                    return {};
                }
                // Calc transconductance at point 1
                var op = pts.isLast && !pts.p2 ? -1 : 1;
                var pta = self.calcPointFromVa(pts.p1.va + op * Math.abs(pts.p1.vt - pts.p1.va), true);
                if (!pta) {
                    return {};
                }
                var s = Math.abs((Math.abs(pts.p1.it - pts.p1.ia) + Math.abs(pts.p1.ia - pta.ia)) / (pts.p1.vg1 - pta.vg1));
                var ri = Math.abs((pts.p1.vt - pts.p1.va) / (pts.p1.it - pts.p1.ia));
                var op = pts.isLast ? -1 : 1;
                var ptb = pts.p2 && self.calcPointFromVa(pts.p2.va + op * Math.abs(pts.p2.vt - pts.p2.va), true);
                if (ptb) {
                    var s2 = Math.abs((Math.abs(pts.p2.it - pts.p2.ia) + Math.abs(pts.p2.ia - ptb.ia)) / (pts.p2.vg1 - ptb.vg1));
                    s = s - (s - s2) * (wp.va - pts.p1.va) / (pts.p2.va - pts.p1.va);
                    var ri2 = Math.abs((pts.p2.vt - pts.p2.va) / (pts.p2.it - pts.p2.ia));
                    ri = ri - (ri - ri2) * (wp.va - pts.p1.va) / (pts.p2.va - pts.p1.va);
                }

                return {
                    s: s,
                    ri: ri
                }
            }
            var p = calcSAndRi();

            return {
                loadPoints: self.crossPointsData,
                workingPoint: wp,
                aLoad: self.aLoad,
                bLoad: self.bLoad,
                vg1Max: self.vg1Max,
                vg1Min: self.vg1Min,
                ab: self.abPoint,
                s: p.s,
                ri: p.ri,
                calcHarmonics: function() {
                    if (!wp || !self.vg1Min || !self.vg1Max) {
                        return {};
                    }
                    var h2: number;
                    var h3: number;
                    var h4: number;
                    if (self.options.usage.mode.value === 'se') {
                        var vga = wp.vg1 - (wp.vg1 - self.vg1Min.vg1) / 2;
                        var vgb = wp.vg1 - (wp.vg1 - self.vg1Max.vg1) / 2;
                        var pta = self.calcPointFromVg1(vga);
                        var ptb = self.calcPointFromVg1(vgb);
                        var div = self.vg1Max.ia + ptb.ia - pta.ia - self.vg1Min.ia;
                        h2 = 75 * (self.vg1Max.ia - 2 * wp.ia + self.vg1Min.ia) / div;
                        h3 = 50 * (self.vg1Max.ia - 2 * ptb.ia + 2 * pta.ia - self.vg1Min.ia) / div;
                        h4 = 25 * (self.vg1Max.ia - 4 * ptb.ia + 6 * wp.ia - 4 * pta.ia + self.vg1Min.ia) / div;
                    } else {
                        var vgm = wp.vg1 - (wp.vg1 - self.vg1Max.vg1) / 2;
                        var ptm = self.calcPointFromVg1(vgm);
                        var div = 2 * (self.vg1Max.ia + ptm.ia);
                        var ia = self.vg1Max.ia - wp.ia;
                        var ib = ptm.ia - wp.ia;
                        h2 = 0;
                        h3 = 50 * (2 * ia - 4 * ib) / div;
                        h4 = 0;
                    }

                    return {
                        h2: Math.abs(h2),
                        h3: Math.abs(h3),
                        h4: Math.abs(h4)
                    }
                    /*var serie = self.chart.addSeries({
                        type: 'spline',
                        name: 'Test Points Serie',
                        color: 'pink',
                        lineWidth: 0,
                        marker: {
                            enabled: true,
                            symbol: 'circle',
                            radius: 5
                        }
                    });

                    serie.addPoint({
                        x: pta.va,
                        y: pta.ia,
                        name: String(pta.vg1),
                        value: pta.vg1
                    } as HighchartsDataPoint, false);

                    serie.addPoint({
                        x: ptb.va,
                        y: ptb.ia,
                        name: String(ptb.vg1),
                        value: ptb.vg1
                    } as HighchartsDataPoint, false);*/
                }
            } as GraphInfos;
        }

        public updateWorkingPoint() {
            var self: AnodeTransfertChart = this;
            var usage = self.options.usage;
            var va = usage && usage.va.value;

            if (va !== null && va !== undefined) {
                if (!self.workingPointSerie) {
                    self.workingPointSerie = self.chart.addSeries({
                        type: 'spline',
                        name: self.translations.filter('#c-wp').text(),
                        data: [[va, usage.iazero.value]],
                        color: 'orange',
                        index: self.workingPointSerieIndex,
                        zIndex: self.workingPointSerieIndex,
                        id: 'wp',
                        visible: self.seriesVisibility['wp'] !== false,
                        events: self.serieEvents,
                        marker: {
                            symbol: 'square',
                            radius: 6,
                            lineWidth: 1
                        }
                    });
                } else {
                    self.workingPointSerie.setData([[va, usage.iazero.value]], false, false);
                }
            }
            else {
                if (self.workingPointSerie) {
                    self.workingPointSerie.remove();
                    self.workingPointSerie = undefined;
                }
            }

            return self;
        }

        public updateLoadLines() {
            var self: AnodeTransfertChart = this;
            self.crossPointsData = null;

            // Hide vertical lines series
            if (self.vinSerie0 && self.vinSerie0.graph) {
                $(self.vinSerie0.graph.element).hide();
                $(self.vinSerie0.markerGroup.element).hide();
            }
            if (self.vinSerie1 && self.vinSerie1.graph) {
                $(self.vinSerie1.graph.element).hide();
                $(self.vinSerie1.markerGroup.element).hide();
            }

            self.updateALoadLines();
            self.updateBLoadLines();
            self.updateWorkingPoint();
            return self;
        }

        private updateALoadLines = function() {
            var self: AnodeTransfertChart = this;
            var ALoad: number;
            var dataA: Array<[number, number]>;

            var xaxis = self.chart.xAxis[0] as HighchartsAxisObject;
            var yaxis = self.chart.yAxis[0] as HighchartsAxisObject;
            var umin = Math.max(0, xaxis.dataMin);
            var imin = Math.max(0, yaxis.dataMin);
            var uzero: number;
            var izero: number;

            if (self.options.usage) {
                uzero = self.options.usage.va.value;
                var ALoad = self.options.usage.effectiveLoad;
                if (ALoad && uzero) {
                    izero = self.options.usage.iazero.value;
                    var uamax = (izero - imin) * ALoad / 1000 + uzero;
                    var iamax = 0;
                    var uamin = 0;
                    var iamin = (uzero - umin) * 1000 / ALoad + izero;
                    dataA = [[uamin, iamin], [uamax, iamax]];
                }
            }

            // Add class A curve
            if (dataA) {
                if (!self.classASerie) {
                    self.classASerie = self.chart.addSeries({
                        type: 'spline',
                        name: 'Class A',
                        data: dataA,
                        color: 'orange',
                        index: self.classASerieIndex,
                        zIndex: self.classASerieIndex,
                        id: 'classa',
                        visible: self.seriesVisibility['classa'] !== false,
                        events: self.serieEvents,
                        marker: {
                            enabled: true,
                            symbol: 'circle',
                            radius: 6,
                            lineWidth: 1
                        }
                    });
                } else {
                    self.classASerie.setData(dataA, false, false);
                }

                self.aLoad = {
                    vazero: uzero,
                    iazero: izero,
                    vamin: uamin,
                    vamax: uamax,
                    iamin: iamin,
                    iamax: iamax,
                    load: ALoad
                }
            } else if (self.classASerie) {
                self.classASerie.remove();
                self.classASerie = undefined;
                self.aLoad = undefined;
            }

            return self;
        }

        private updateBLoadLines = function() {
            var self: AnodeTransfertChart = this;
            var dataB: Array<[number, number]>;

            var xaxis = self.chart.xAxis[0] as HighchartsAxisObject;
            var yaxis = self.chart.yAxis[0] as HighchartsAxisObject;
            var umin = Math.max(0, xaxis.dataMin);
            var imin = Math.max(0, yaxis.dataMin);

            self.abPoint = undefined;

            if (self.options.usage && self.aLoad) {
                var mode = self.options.usage.mode.value;
                if (mode !== 'se' && mode !== 'na') {
                    // Add class B curve
                    var ubmax = self.aLoad.vazero;
                    var ibmax = imin;
                    var ubmin = umin;
                    var ibmin = (ubmax - umin) * 2000 / self.aLoad.load;
                    if (ibmin > self.classASerie.points[0].y) {
                        dataB = [[ubmin, ibmin], [ubmax, ibmax]];
                        var point = [0, 0];
                        self.calcLineIntersection(self.aLoad.vamin, self.aLoad.iamin, self.aLoad.vamax, self.aLoad.iamax, ubmin, ibmin, ubmax, ibmax, point);
                        if (point) {
                            self.abPoint = {
                                va: point[0],
                                ia: point[1]
                            }
                        }
                    }
                }
            }

            if (dataB) {
                // Add class B curve
                if (!self.classBSerie) {
                    self.classBSerie = self.chart.addSeries({
                        type: 'spline',
                        name: 'Class B',
                        data: dataB,
                        color: 'blue',
                        index: self.classBSerieIndex,
                        zIndex: self.classBSerieIndex,
                        id: 'classb',
                        visible: self.seriesVisibility['classb'] !== false,
                        events: self.serieEvents,
                        marker: {
                            symbol: 'circle',
                            radius: 3,
                            lineWidth: 1
                        }
                    });
                } else {
                    self.classBSerie.setData(dataB, false, false);
                }

                self.bLoad = {
                    vazero: self.aLoad.vazero,
                    iazero: self.aLoad.iazero,
                    vamin: ubmin,
                    vamax: ubmax,
                    iamin: ibmin,
                    iamax: ibmax,
                    load: self.aLoad.load / 2
                }
            } else if (self.classBSerie) {
                self.classBSerie.remove();
                self.classBSerie = undefined;
                self.bLoad = undefined;
            }

            return self;
        }

        public updateVinLines = function() {
            var self: AnodeTransfertChart = this;
            var vinpp: number;
            var vg0: VPoint;
            self.vg1Min = undefined;
            self.vg1Max = undefined;

            if (self.options.usage && self.classASerie) {
                vg0 = self.calcPointFromVa(self.options.usage.va.value, true);
                vinpp = self.options.usage.vinpp.value;
            }

            if (vg0 && !isNaN(vinpp)) {
                var vinp = vinpp / 2;
                self.vg1Max = self.calcPointFromVg1(vg0.vg1 + vinp);
                var maxOverflow = self.vg1Max.overflow;
                var dataMax = [[self.vg1Max.va, 0], [self.vg1Max.va, self.vg1Max.ia]];
                self.vg1Min = self.calcPointFromVg1(vg0.vg1 - vinp);
                var minOverflow = self.vg1Min.overflow;
                var dataMin = [[self.vg1Min.va, 0], [self.vg1Min.va, self.vg1Min.ia]];
                if (minOverflow && !self.vg1Max.classA) {
                    self.vg1Min = {
                        vg1: undefined,
                        va: self.aLoad.vamax,
                        ia: 0,
                        classA: false,
                        overflow: true,
                    };
                    dataMin = [[self.aLoad.vamax, 0]];
                }

                if (!self.vinSerie0) {
                    self.vinSerie0 = self.chart.addSeries({
                        type: 'spline',
                        name: 'vg1-min',
                        color: minOverflow ? 'red' : 'green',
                        data: dataMin,
                        lineWidth: 1,
                        index: self.vinSerie0Index,
                        zIndex: self.vinSerie0Index,
                        id: 'vgimin',
                        visible: self.seriesVisibility['vgimin'] !== false,
                        events: self.serieEvents,
                        marker: {
                            enabled: true,
                            symbol: 'circle',
                            radius: 6
                        }
                    });
                } else {
                    $(self.vinSerie0.graph.element).show();
                    $(self.vinSerie0.markerGroup.element).show();
                    self.vinSerie0.setData(dataMin, false, false);
                    self.vinSerie0.update({
                        color: minOverflow ? 'red' : 'green'
                    }, false)
                }

                if (!self.vinSerie1) {
                    self.vinSerie1 = self.chart.addSeries({
                        type: 'spline',
                        name: 'vg1-max',
                        color: maxOverflow ? 'red' : 'green',
                        data: dataMax,
                        lineWidth: 1,
                        index: self.vinSerie1Index,
                        zIndex: self.vinSerie1Index,
                        id: 'vgimax',
                        visible: self.seriesVisibility['vgimax'] !== false,
                        events: self.serieEvents,
                        marker: {
                            enabled: true,
                            symbol: 'circle',
                            radius: 6
                        }
                    });
                } else {
                    $(self.vinSerie1.graph.element).show();
                    $(self.vinSerie1.markerGroup.element).show();
                    self.vinSerie1.setData(dataMax, false, false);
                    self.vinSerie1.update({
                        color: maxOverflow ? 'red' : 'green'
                    }, false)
                }
            } else {
                if (self.vinSerie0) {
                    self.vinSerie0.remove();
                    self.vinSerie0 = undefined;
                }
                if (self.vinSerie1) {
                    self.vinSerie1.remove();
                    self.vinSerie1 = undefined;
                }
            }

            return self;
        }

        public updatePmax = function() {
            var self: AnodeTransfertChart = this;

            var pmax = self.options.pmax * 1000; // En mW
            var maxAxis = [0, 0];
            if (pmax) {
                maxAxis = self.getMaxAxis();
            }

            if (maxAxis[1] > 0) {
                var pamax = [] as Array<Array<number>>;
                var u = pmax / maxAxis[1];
                var step = (maxAxis[0] - u) / 50;
                var last = maxAxis[0] + step;
                while (u <= last) {
                    pamax.push([u, pmax / u]);
                    u += step;
                }

                var options = {
                    type: 'spline',
                    lineWidth: 2,
                    name: 'pmax (' + self.options.pmax + 'W)',
                    data: pamax,
                    color: 'red',
                    index: self.pmaxSerieIndex,
                    zIndex: self.pmaxSerieIndex,
                    id: 'pmax',
                    visible: self.seriesVisibility['pmax'] !== false,
                    events: self.serieEvents,
                    marker: {
                        enabled: false,
                    }
                }

                if (!self.pmaxSerie) {
                    self.pmaxSerie = self.chart.addSeries(options);
                } else {
                    self.pmaxSerie.update(options);
                }
            } else {
                if (self.pmaxSerie) {
                    self.pmaxSerie.remove();
                    self.pmaxSerie = undefined;
                }
            }

            return self;
        }

        private getMaxAxis = function() {
            var self: AnodeTransfertChart = this;
            var retVal = [0, 0];

            if (self.chart) {
                self.chart.series.forEach(function(serie) {
                    if (serie.options.vg1 !== undefined && serie.visible) {
                        if (serie.options.umax > retVal[0]) {
                            retVal[0] = serie.options.umax
                        }
                        if (serie.options.imax > retVal[1]) {
                            retVal[1] = serie.options.imax
                        }
                    }
                })
            }

            return retVal;
        }

        public refreshLines(redraw?: boolean) {
            var self: AnodeTransfertChart = this;
            self.updatePmax();
            self.updateLoadLines();
            self.updateVinLines();
            if (redraw) {
                self.chart.redraw(false);
            }
        }

        public refresh() {
            var self: AnodeTransfertChart = this;
            super.refresh();
            
            if (!self.options.tubeGraph) {
                return;
            }

            var colors = utils.Colors.highchartsColors();
            var dragdropInfos: DragDropInfos;
            var clickInfos: ClickInfos;
            var setExtremesTimeout = 600;
            var setExtremesXTimer: number;
            var setExtremesYTimer: number;
            var gdatas = self.options.tubeGraph.c;
            var series: Array<HighchartsSeriesOptions> = [];
            var ug1max = 0;
            var ug1min = 0;

            var next = function() {
                self.loadChartTitles();
                self.refreshLines();
                if (self.options.ready) {
                    self.options.ready();
                }
            }

            // Order is important, class A first
            var datas = gdatas && gdatas.sort(function(a, b) {
                return a.vg1 - b.vg1
            });

            var getOffset = function(e: JQueryMouseEventObject) {
                var retval: { x: number, y: number };
                if (self.browserVersion.browser === 'firefox') {
                    retval = {
                        x: (e.originalEvent && e.originalEvent.layerX) || e.offsetX,
                        y: (e.originalEvent && e.originalEvent.layerY) || e.offsetY
                    }
                } else {
                    retval = {
                        x: e.offsetX,
                        y: e.offsetY
                    }
                }
                return retval;
            }

            for (var c = 0; c < gdatas.length; c++) {
                var imax = 0;
                var umax = 0;
                var curve = gdatas[c];
                var points: Array<any> = [];
                for (var p = 0; p < curve.p.length; p++) {
                    var point = curve.p[p];
                    var i = point.ik * 1000;
                    points.push([point.va, i]);
                    if (umax < point.va) {
                        umax = point.va;
                    }
                    if (imax < i) {
                        imax = i;
                    }
                }

                var seriesVisibility = self.options.usage ? self.options.usage.series.value : {};
                var name = curve.vg1 + 'V';
                series.push({
                    type: 'spline',
                    lineWidth: 2,
                    name: name,
                    vg1: curve.vg1,
                    data: points,
                    umax: umax,
                    imax: imax,
                    color: colors[c % colors.length],
                    index: c,
                    zIndex: c,
                    id: String(curve.vg1),
                    visible: self.seriesVisibility[String(curve.vg1)] !== false,
                    events: self.serieEvents
                } as HighchartsIndividualSeriesOptions)

                if (curve.vg1 < ug1min) {
                    ug1min = curve.vg1;
                }
                if (curve.vg1 > ug1max) {
                    ug1max = curve.vg1;
                }
            }

            self.crossSerieIndex = c++;
            self.pmaxSerieIndex = c++;
            self.classBSerieIndex = c++;
            self.classASerieIndex = c++;
            self.vinSerie0Index = c++;
            self.vinSerie1Index = c++;
            self.workingPointSerieIndex = c++;

            if (!self.chart) {
                requirejs(['highcharts'], function() {
                    self.$chart.highcharts({
                        chart: {
                            borderWidth: 1,
                            zoomType: 'xy'
                        },
                        title: {
                            text: 'Anode Transfert Chart'
                        },
                        subtitle: {
                            text: 'c-tt'
                        },                            
                        xAxis: {
                            type: self.options.tubeGraph.logX ? 'logarithmic' : 'linear',
                            title: {
                                text: 'c-av'
                            },
                            labels: {
                                overflow: 'justify'
                            },
                            events: {
                                afterSetExtremes: function(e) {
                                    if (!self.options.usage) {
                                        return;
                                    }

                                    var zoom: ZoomData = self.options.usage.zoom.value || {};
                                    if (zoom.minX !== e.userMin || zoom.maxX !== e.userMax) {
                                        zoom.minX = e.userMin;
                                        zoom.maxX = e.userMax;
                                        self.options.usage.zoom.value = zoom;
                                        self.onoptionschanged();
                                    }
                                }
                            }
                        },
                        yAxis: {
                            type: self.options.tubeGraph.logY ? 'logarithmic' : 'linear',
                            title: {
                                text: 'c-ac'
                            },
                            lineWidth: 1,
                            minorGridLineWidth: 1,
                            gridLineWidth: 1,
                            alternateGridColor: null,
                            events: {
                                afterSetExtremes: function(e) {
                                    if (!self.options.usage) {
                                        return;
                                    }

                                    var zoom: ZoomData = self.options.usage.zoom.value || {};
                                    if (zoom.minY !== e.userMin || zoom.maxY !== e.userMax) {
                                        zoom.minY = e.userMin;
                                        zoom.maxY = e.userMax;
                                        self.options.usage.zoom.value = zoom;
                                        self.onoptionschanged();
                                    }
                                }
                            }
                        },
                        tooltip: {
                            headerFormat: '',
                            pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.x:.1f} V</b>, <b>{point.y:.2f} mA</b><br/>'
                        },
                        plotOptions: {
                            line: {
                                lineWidth: 4,
                                states: {
                                    hover: {
                                        lineWidth: 5
                                    }
                                },
                                marker: {
                                    enabled: false
                                },
                                pointInterval: 1,
                            }
                        },
                        series: series,
                        navigation: {
                            menuItemStyle: {
                                fontSize: '10px'
                            }
                        }
                    }, function(c) {
                        self.chart = c;
                        self.setZoom();
                        self.setHeight();

                        $(window).on('beforeprint', function(e) {
                            c.options.chart.width = 600;
                            c.options.chart.height = 400;
                            c.reflow();
                        }).on('afterprint', function(e) {
                            c.options.chart.width = undefined;
                            c.options.chart.height = undefined;
                            c.reflow();
                        });

                        // Register chart events for load lines drag and drop                            
                        self.$chart.find('svg').on('mousedown', function(e) {
                            var offset = getOffset(e);
                            var target = $(e.target);
                            clickInfos = undefined;

                            var va = self.options.usage && self.options.usage.va.value;
                            if (!self.options.editable || va === null || va === undefined) {
                                return;
                            }

                            var closest = $(e.target).closest('.highcharts-series');
                            if (self.workingPointSerie && self.workingPointSerie.visible) {
                                if (self.workingPointSerie.points[0].graphic && target.is(self.workingPointSerie.points[0].graphic.element)) {
                                    // Working point
                                    var serie = self.workingPointSerie as HighchartsSeriesObject;
                                    dragdropInfos = {
                                        clickedPosition: offset,
                                        serie: serie,
                                        point: serie.points[0],
                                        name: 'wpoint'
                                    }
                                    return false;
                                }
                            }

                            if (self.classASerie && self.classASerie.visible) {
                                // Check first and last point
                                if (self.classASerie.points[0].graphic && target.is(self.classASerie.points[0].graphic.element)) {
                                    // Left point
                                    var serie = self.classASerie as HighchartsSeriesObject;
                                    dragdropInfos = {
                                        clickedPosition: offset,
                                        serie: serie,
                                        point: serie.points[0],
                                        name: 'ypoint',
                                        extremesY: serie.yAxis.getExtremes()
                                    };
                                    setExtremesTimeout = 600;
                                    return false;

                                } else if (self.classASerie.points[1].graphic && target.is(self.classASerie.points[1].graphic.element)) {
                                    // Right point
                                    var serie = self.classASerie as HighchartsSeriesObject;
                                    dragdropInfos = {
                                        clickedPosition: offset,
                                        serie: serie,
                                        point: serie.points[1],
                                        name: 'xpoint',
                                        extremesX: serie.xAxis.getExtremes(),
                                    }
                                    setExtremesTimeout = 600;
                                    return false;

                                } else if (closest.is(self.classASerie.group.element)) {
                                    // Serie
                                    var serie = self.classASerie as HighchartsSeriesObject;
                                    dragdropInfos = {
                                        clickedPosition: offset,
                                        startY: serie.points[0].y,
                                        point: {
                                            x: serie.xAxis.toValue(offset.x),
                                            y: serie.yAxis.toValue(offset.y)
                                        } as HighchartsPointObject,
                                        serie: serie,
                                        name: 'aserie',
                                        extremesX: serie.xAxis.getExtremes(),
                                        extremesY: serie.yAxis.getExtremes(),
                                    }
                                    setExtremesTimeout = 1100;
                                    return false;
                                }
                            }

                            if (self.vinSerie0 && self.vinSerie0.visible) {
                                if ((self.vinSerie0.points[0].graphic && target.is(self.vinSerie0.points[0].graphic.element)) ||
                                    (self.vinSerie0.points[1] && self.vinSerie0.points[1].graphic && target.is(self.vinSerie0.points[1].graphic.element)) ||
                                    closest.is(self.vinSerie0.group.element)) {
                                    // Serie or point
                                    var serie = self.vinSerie0 as HighchartsSeriesObject;
                                    dragdropInfos = {
                                        clickedPosition: offset,
                                        startX: serie.points[0].x,
                                        point: {
                                            x: serie.xAxis.toValue(offset.x),
                                            y: serie.yAxis.toValue(offset.y)
                                        } as HighchartsPointObject,
                                        serie: serie,
                                        name: 'vg1-max',
                                        extremesX: serie.xAxis.getExtremes(),
                                        extremesY: serie.yAxis.getExtremes(),
                                        workingPoint: self.calcPointFromVa(self.options.usage.va.value, true)
                                    }
                                    setExtremesTimeout = 1100;
                                    return false;
                                }

                            }

                            if (self.vinSerie1 && self.vinSerie1.visible) {
                                if ((self.vinSerie1.points[0].graphic && target.is(self.vinSerie1.points[0].graphic.element)) ||
                                    (self.vinSerie1.points[1] && self.vinSerie1.points[1].graphic && target.is(self.vinSerie1.points[1].graphic.element)) ||
                                    closest.is(self.vinSerie1.group.element)) {
                                    // Serie or point
                                    var serie = self.vinSerie1 as HighchartsSeriesObject;
                                    dragdropInfos = {
                                        clickedPosition: offset,
                                        startX: serie.points[0].x,
                                        point: {
                                            x: serie.xAxis.toValue(offset.x),
                                            y: serie.yAxis.toValue(offset.y)
                                        } as HighchartsPointObject,
                                        serie: serie,
                                        name: 'vg1-min',
                                        extremesX: serie.xAxis.getExtremes(),
                                        extremesY: serie.yAxis.getExtremes(),
                                        workingPoint: self.calcPointFromVa(self.options.usage.va.value, true)
                                    }
                                    setExtremesTimeout = 1100;
                                    return false;
                                }
                            }

                            if (self.workingPointSerie) {
                                clickInfos = {
                                    clickedPosition: offset,
                                    clickedElement: target.closest('#chart-cont').find('.highcharts-background')
                                }

                                setTimeout(function() {
                                    if (!clickInfos) {
                                        return;
                                    }
                                    var showMenu = function() {
                                        requirejs(['dropdown'], function(ns: any) {
                                            if (!clickInfos) {
                                                return;
                                            }

                                            var dropDown = new ns.DropDown();
                                            var va = self.workingPointSerie.xAxis.toValue(offset.x);
                                            var ia = self.workingPointSerie.yAxis.toValue(offset.y);
                                            var $menu = $('body').find('#anodechartmenu');
                                            $menu.find('#va').text(va.toFixed(0) + 'V');
                                            $menu.find('#ia').text(ia.toFixed(2) + 'mA');

                                            dropDown.show({
                                                template: $menu.html(),
                                                backdrop: true,
                                                backdropCancel: true,
                                                backdropId: 'chart-menu-backdrop',
                                                modalId: 'chart-menu-body',
                                                cancelOnEsc: true,
                                                position: {
                                                    of: clickInfos.clickedElement,
                                                    my: 'left top',
                                                    at: 'left+' + offset.x + 'px top+' + offset.y + 'px',
                                                }
                                            }).modalBody.on('click', function(e: JQueryEventObject) {
                                                // Bind menu
                                                var $dataClick = $(e.target).closest('[data-click]');
                                                if ($dataClick.is('.disabled')) {
                                                    return;
                                                }

                                                var func = $dataClick.attr('data-click');
                                                if (func) {
                                                    switch (func) {
                                                        case 'apply':
                                                            self.options.usage.va.value = Math.max(va, 0.001);
                                                            self.options.usage.iazero.value = ia;

                                                            self.updateLoadLines();
                                                            self.updateVinLines();

                                                            self.chart.redraw(false);
                                                            self.onoptionschanged();
                                                            dropDown.close();
                                                    }
                                                }
                                            })

                                            clickInfos = undefined;
                                        })
                                    }

                                    if (!self.longClickMenu) {
                                        $.get('/resource?r=widgets%2Fanode-chart-menu.html').done(function(html) {
                                            self.longClickMenu = $(html).appendTo($('body'));
                                            showMenu();
                                        })
                                    } else {
                                        showMenu();
                                    }
                                }, 400);
                            }

                        }).on('mousemove', function(e: JQueryMouseEventObject) {
                            var target = $(e.target);
                            if (!target.is('.highcharts-background,.highcharts-tracker')) {
                                //console.log(target.attr('class'));
                                return;
                            }

                            var offset = getOffset(e);
                            if (clickInfos && (Math.abs(offset.x - clickInfos.clickedPosition.x) >= 5 || Math.abs(offset.y - clickInfos.clickedPosition.y) >= 5)) {
                                clickInfos = undefined;
                            }

                            if (dragdropInfos) {
                                if (!dragdropInfos.dragdropStarted) {
                                    if (Math.abs(offset.x - dragdropInfos.clickedPosition.x) > 5 || Math.abs(offset.y - dragdropInfos.clickedPosition.y) > 5) {
                                        // Start drag
                                        dragdropInfos.dragdropStarted = true;
                                        $('body').addClass('dragdrop');
                                        switch (dragdropInfos.name) {
                                            case 'ypoint':
                                                dragdropInfos.serie.yAxis.setExtremes(dragdropInfos.extremesY.min, dragdropInfos.extremesY.max);
                                                break;

                                            case 'xpoint':
                                                dragdropInfos.serie.xAxis.setExtremes(dragdropInfos.extremesX.min, dragdropInfos.extremesX.max);
                                                break;

                                            case 'aserie':
                                            case 'vg1-min':
                                            case 'vg1-max':
                                                dragdropInfos.serie.xAxis.setExtremes(dragdropInfos.extremesX.min, dragdropInfos.extremesX.max);
                                                dragdropInfos.serie.yAxis.setExtremes(dragdropInfos.extremesY.min, dragdropInfos.extremesY.max);
                                                break;
                                        }
                                    }
                                } else {
                                    var updateLoadLines = function() {
                                        self.updateLoadLines();
                                        self.updateVinLines();
                                        self.chart.redraw(false);
                                        self.onoptionschanged();
                                    }

                                    switch (dragdropInfos.name) {
                                        case 'ypoint':
                                            var y = Math.max(0, dragdropInfos.serie.yAxis.toValue(offset.y));
                                            dragdropInfos.point.update({
                                                x: 0,
                                                y: y
                                            }, false, false);

                                            // Adjust extrême 
                                            if (y > dragdropInfos.extremesY.max) {
                                                if (!setExtremesYTimer) {
                                                    setExtremesYTimer = setTimeout(function() {
                                                        setExtremesYTimer = undefined;
                                                        if (!dragdropInfos || !dragdropInfos.extremesY) {
                                                            return;
                                                        }
                                                        dragdropInfos.extremesY.max += dragdropInfos.extremesY.max;
                                                        dragdropInfos.serie.yAxis.setExtremes(dragdropInfos.extremesY.min, dragdropInfos.extremesY.max);
                                                        setExtremesTimeout = 1500;
                                                    }, setExtremesTimeout);
                                                }
                                            } else if (setExtremesYTimer) {
                                                clearTimeout(setExtremesYTimer);
                                                setExtremesYTimer = undefined;
                                            }

                                            // Load changed
                                            var xx = self.classASerie.points[1].x;
                                            self.options.usage.effectiveLoad = xx * 1000 / y;

                                            // Calc working point
                                            self.options.usage.iazero.value = (xx - self.options.usage.va.value) * y / xx
                                            updateLoadLines();
                                            break;

                                        case 'xpoint':
                                            var x = Math.max(dragdropInfos.serie.xAxis.toValue(offset.x), 0);
                                            dragdropInfos.point.update({
                                                x: x,
                                                y: 0
                                            }, false, false);

                                            // Adjust extrême 
                                            if (x > dragdropInfos.extremesX.max) {
                                                if (!setExtremesXTimer) {
                                                    setExtremesXTimer = setTimeout(function() {
                                                        setExtremesXTimer = undefined;
                                                        if (!dragdropInfos || !dragdropInfos.extremesX) {
                                                            return;
                                                        }
                                                        dragdropInfos.extremesX.max += dragdropInfos.extremesX.max;
                                                        dragdropInfos.serie.xAxis.setExtremes(dragdropInfos.extremesX.min, dragdropInfos.extremesX.max);
                                                        setExtremesTimeout = 1500;
                                                    }, setExtremesTimeout);
                                                }
                                            } else if (setExtremesXTimer) {
                                                clearTimeout(setExtremesXTimer);
                                                setExtremesXTimer = undefined;
                                            }

                                            // Load changed
                                            var yy = self.classASerie.points[0].y;
                                            self.options.usage.effectiveLoad = x * 1000 / yy;

                                            // Calc working point                                                
                                            self.options.usage.va.value = x - (self.options.usage.iazero.value * x / yy);
                                            updateLoadLines();
                                            break;

                                        case 'aserie':
                                            var dy = dragdropInfos.serie.yAxis.toValue(offset.y) - dragdropInfos.point.y;
                                            var y = dragdropInfos.startY + dy;
                                            var ALoad = self.options.usage.effectiveLoad;
                                            var x = y * ALoad / 1000;

                                            // Adjust extrême 
                                            if (y > dragdropInfos.extremesY.max) {
                                                if (!setExtremesYTimer) {
                                                    setExtremesYTimer = setTimeout(function() {
                                                        dragdropInfos.extremesY.max += dragdropInfos.extremesY.max;
                                                        dragdropInfos.serie.yAxis.setExtremes(dragdropInfos.extremesY.min, dragdropInfos.extremesY.max);
                                                        setExtremesTimeout = 1500;
                                                        setExtremesYTimer = undefined;
                                                    }, setExtremesTimeout);
                                                }
                                            } else if (setExtremesYTimer) {
                                                clearTimeout(setExtremesYTimer);
                                                setExtremesYTimer = undefined;
                                            }
                                            if (x > dragdropInfos.extremesX.max) {
                                                if (!setExtremesXTimer) {
                                                    setExtremesXTimer = setTimeout(function() {
                                                        dragdropInfos.extremesX.max += dragdropInfos.extremesX.max;
                                                        dragdropInfos.serie.xAxis.setExtremes(dragdropInfos.extremesX.min, dragdropInfos.extremesX.max);
                                                        setExtremesTimeout = 1500;
                                                        setExtremesXTimer = undefined;
                                                    }, setExtremesTimeout);
                                                }
                                            } else if (setExtremesXTimer) {
                                                clearTimeout(setExtremesXTimer);
                                                setExtremesXTimer = undefined;
                                            }

                                            self.options.usage.iazero.value = Math.max(0, (x - self.options.usage.va.value) * y / x);
                                            updateLoadLines();
                                            break;

                                        case 'wpoint':
                                            var x = Math.max(dragdropInfos.serie.xAxis.toValue(offset.x), 0.001);
                                            self.options.usage.va.value = x;
                                            var ax = self.classASerie.points[1].x;
                                            var ay = self.classASerie.points[0].y;
                                            self.options.usage.iazero.value = Math.max(0, (ax - self.options.usage.va.value) * ay / ax);

                                            self.updateLoadLines();
                                            self.updateVinLines();

                                            self.chart.redraw(false);
                                            self.onoptionschanged();
                                            break;

                                        case 'vg1-min':
                                        case 'vg1-max':
                                            var dx = dragdropInfos.serie.xAxis.toValue(offset.x) - dragdropInfos.point.x;
                                            var x = dragdropInfos.startX + dx;

                                            // Calc new vg1 min or max
                                            var newPoint = self.calcPointFromVa(x, true);

                                            self.options.usage.vinpp.value = 2 * Math.max(0, Math.abs(newPoint.vg1 - dragdropInfos.workingPoint.vg1));
                                            self.updateVinLines();

                                            self.chart.redraw(false);
                                            self.onoptionschanged();
                                            break;

                                    }
                                }
                                return false;
                            }

                        }).on('mouseup', function(e) { onmouseup(e) }).on('mouseenter', function(e) { onmouseenter(e) });

                        var onmouseenter = function(e: JQueryMouseEventObject) {
                            clickInfos = undefined;
                            if (dragdropInfos) {
                                if (e.buttons !== 1 || e.button !== 0) {
                                    onmouseup(e);
                                }
                            }
                        }

                        var onmouseup = function(e: JQueryMouseEventObject) {
                            clickInfos = undefined;

                            if (setExtremesYTimer) {
                                clearTimeout(setExtremesYTimer);
                                setExtremesYTimer = undefined;
                            }

                            if (setExtremesXTimer) {
                                clearTimeout(setExtremesXTimer);
                                setExtremesXTimer = undefined;
                            }

                            if (dragdropInfos && dragdropInfos.dragdropStarted) {
                                switch (dragdropInfos.name) {
                                    case 'ypoint':
                                        dragdropInfos.serie.yAxis.setExtremes(null, null, true);
                                        break;

                                    case 'xpoint':
                                        dragdropInfos.serie.xAxis.setExtremes(null, null, true);
                                        break;

                                    case 'aserie':
                                    case 'vg1-min':
                                    case 'vg1-max':
                                        dragdropInfos.serie.xAxis.setExtremes(null, null, false);
                                        dragdropInfos.serie.yAxis.setExtremes(null, null, true);
                                        break;
                                }

                                dragdropInfos = undefined;
                                $('body').removeClass('dragdrop');

                            }
                        }

                        next();
                    });
                });
            } else {
                while (self.chart.series.length > 0) {
                    self.chart.series[0].remove(false);
                }
                series.map(s => self.chart.addSeries(s, false));
                self.setZoom();
                next();
                self.chart.redraw();
            }

            return self;
        }

        public exportToJson = function(pointsPerLines?: number) {
            var self = this as AnodeTransfertChart;

            // ************* FOR TEST **********************
            /*function y(serie: HighchartsSeriesObject, x: number) {
                var y = [] as Array<number>;
                var pcount = serie.points.length;
                var path = serie.graph.element;
     
                var getPointLength = function(x: number, start: number, end: number) {
                    while (true) {
                        var p1 = path.getPointAtLength(start);
                        var p2 = path.getPointAtLength(end);
                        var halfLength = start + (end - start) / 2;
                        var m = path.getPointAtLength(halfLength);
                        if (x >= p1.x && x <= m.x) {
                            end = halfLength;
                        } else {
                            start = halfLength;
                        }
                        if (Math.abs(p1.x - x) < 0.0001) {
                            return start;                                    
                        } 
                        if (Math.abs(p2.x - x) < 0.0001) {
                            return end;                                    
                        }                            
                    }
     
                    return -1;
                }
     
                
                // For each points, calc the length in the serie curve
                var previousLength = 0;
                var totalLength = path.getTotalLength()
                for (var p = 0; p < pcount; p++) {
                    var pt = serie.points[p]
                    pt.pathLength = getPointLength(pt.plotX, previousLength, totalLength);
                    if (pt.pathLength >= 0) {
                        previousLength = pt.pathLength;
                    }
                }
     
     
                var segments = [] as Array<{
                    p1: HighchartsPointObject,
                    p2: HighchartsPointObject
                }>
                for (var p = 1; p < pcount; p++) {
                    var p1 = serie.points[p - 1];
                    var p2 = serie.points[p];
                    if (x >= p1.options.x && x <= p2.options.y) {
                        segments.push({
                            p1: p1,
                            p2: p2
                        });
                    } else if (x >= p2.options.x && x <= p1.options.y) {
                        segments.push({
                            p1: p2,
                            p2: p1
                        });
                    }
                }
                
                // For each segments search the corresponding point on the curve
                segments.forEach(function(sg) {
                    // Start of the segment
                    
                    // Length of the segment
                    
                    
                })
                
                for (var l = 0; l < path.getTotalLength(); l++) {
                    var p = path.getPointAtLength(l);
                    points.push([p.x, p.y]);
                }
     
     
                return y;
            }
            
            var gdatas = self.options.tubeGraph.c;
            var series = [] as Array<HighchartsSeriesOptions>;
    
            //var index = chart.series.length - 2;
            //var serie = chart.series[index];
            self.chart.series.forEach(function(serie, index) {
                var path = serie.graph.element;
    
                // Calc ratio between plotX and x value
                var points = [] as Array<[number, number]>;
                var length = path.getTotalLength();
                var pos = 0;
                // Calc n points
                var ppl = pointsPerLines || 1000;
                var step = length / ppl;                  
                // Get position for each points of the serie
                while (pos < length) {
                    var p = path.getPointAtLength(pos);
                    // Calc x and y values
                    var x = serie.xAxis.toValue(p.x, true);
                    var y = serie.yAxis.toValue(p.y, true);
                    points.push([x, y]);
                    pos += step;
                }
    
                series.push({
                    type: 'line',
                    data: points,
                    name: 'test ' + serie.name
                } as HighchartsSeriesOptions)
            });            
            
            */
            // ************* END FOR TEST **********************
            var gdatas = self.options.tubeGraph.c;
            var tubeGraph = $.extend({}, self.options.tubeGraph) as TubeGraph;

            if (pointsPerLines) {
                self.chart.series.forEach(function(serie, index) {
                    var path = serie.graph.element;

                    // Calc ratio between plotX and x value
                    var points = [] as Array<[number, number]>;
                    var length = path.getTotalLength();
                    var pos = 0;
                    // Calc n points
                    var ppl = pointsPerLines;
                    var step = length / ppl;

                    // Create a new curve
                    var curve = tubeGraph.c[index];
                    curve.p = [];

                    // Get position for each points of the serie
                    while (pos < length) {
                        var p = path.getPointAtLength(pos);
                        // Calc x and y values
                        curve.p.push({
                            vg1: curve.vg1,
                            va: serie.xAxis.toValue(p.x, true),
                            ik: serie.yAxis.toValue(p.y, true) / 1000,
                        })
                        pos += step;
                    }
                })

                delete tubeGraph.logX;
                delete tubeGraph.logY;
                tubeGraph.raw = true;
            }

            return tubeGraph;
        }

        private getNearestTrueCrossingLinePoints(va: number): { p1?: VPoint, p2?: VPoint, isLast?: boolean } {
            var self = this as AnodeTransfertChart;
            var cp = self.crossingPoints;
            if (!cp || cp.length === 0) {
                return {};
            }

            var i = -1;
            var n = 0;
            var last: VPoint = undefined;
            while (++i < cp.length) {
                var p = cp[i];
                if (p.vt && p.it) {
                    if (p.va === va) {
                        return {
                            p1: p,
                            isLast: n === 0
                        };
                    } else if (p.va < va) {
                        return {
                            p1: p,
                            p2: last,
                            isLast: n <= 1,
                        }
                    }
                    last = p;
                    n++;
                }
            }

            return {};
        }

        private calcPointFromVg1 = function(vg1: number) {
            var self = this as AnodeTransfertChart;

            var point = {
                va: undefined,
                ia: undefined,
                vg1: vg1,
                overflow: false,
                classA: true,
            } as VPoint

            var p0: VPoint;
            var p1: VPoint;
            var crossPoints = self.crossingPoints;
            var i = crossPoints.length
            p0 = p1 = crossPoints[0];
            for (var i = 0; i < crossPoints.length; i++) {
                var p = crossPoints[i];
                if (p.vg1 > vg1) {
                    p1 = p;
                    break;
                }
                p0 = p1 = p;
            }

            if (p1 && p0) {
                point.va = p0.va;
                point.ia = p0.ia;
                point.classA = p1.classA;
                if (p0.vg1 !== p1.vg1) {
                    // Calc best point between p0 and p1
                    point.va += (vg1 - p0.vg1) * (p1.va - p0.va) / (p1.vg1 - p0.vg1);
                    point.ia -= (p0.ia - p1.ia) * (point.va - p0.va) / (p1.va - p0.va);
                } else {
                    point.overflow = true;
                }
            }

            return point;
        }

        private calcPointFromVa = function(u: number, nearest: boolean) {
            var self = this as AnodeTransfertChart;

            var point = {
                va: u,
                ia: null,
                vg1: null,
                overflow: false,
                classA: true,
            } as VPoint

            var p0: VPoint;
            var p1: VPoint;
            var crossPoints = self.crossingPoints;
            if (nearest) {
                p0 = p1 = crossPoints[0];
                for (var i = 0; i < crossPoints.length; i++) {
                    var p = crossPoints[i];
                    if (p.va < u) {
                        p1 = p;
                        break;
                    }
                    p0 = p1 = p;
                }
            } else {
                var i = crossPoints.length
                p0 = p1 = crossPoints[i - 1];
                while (--i >= 0) {
                    var p = crossPoints[i];
                    if (p.va > u) {
                        p0 = p;
                        break;
                    }
                    p0 = p1 = p;
                }
            }

            if (p1 && p0) {
                // Calc best point between p0 and p1
                point.vg1 = p0.vg1;
                point.ia = p0.ia;
                point.classA = p1.classA;
                if (p0.va !== p1.va) {
                    point.vg1 += (u - p0.va) * (p1.vg1 - p0.vg1) / (p1.va - p0.va);
                    point.ia -= (p0.ia - p1.ia) * (u - p0.va) / (p1.va - p0.va);
                } else {
                    point.overflow = true;
                }
            }

            return point;
        }

        private get crossingPoints() {
            var self = this as AnodeTransfertChart;
            if (!self.crossPointsData) {
                self.crossPointsData = self.calcCrossPoints();

                // For testing
                // self.drawCrossPoints();
            }
            return self.crossPointsData;
        }

        private calcLineIntersection = function(ax1: number, ay1: number, ax2: number, ay2: number, bx1: number, by1: number, bx2: number, by2: number, returnPoint?: Array<number>) {
            // if the lines intersect, the result contains the x and y of the intersection (treating the lines as infinite) and booleans for whether line segment 1 or line segment 2 contain the point
            var d = ((by2 - by1) * (ax2 - ax1)) - ((bx2 - bx1) * (ay2 - ay1));
            if (d === 0) {
                return false;
            }

            var a = ay1 - by1;
            var b = ax1 - bx1;
            var aa = ((bx2 - bx1) * a - (by2 - by1) * b) / d;
            if (aa <= 0 || aa > 1) {
                return false;
            }

            var bb = ((ax2 - ax1) * a - (ay2 - ay1) * b) / d;
            if (bb <= 0 || bb > 1) {
                return false;
            }

            if (returnPoint) {
                returnPoint[0] = ax1 + (aa * (ax2 - ax1));
                returnPoint[1] = ay1 + (aa * (ay2 - ay1));
            }

            return true;
        };

        private calcCrossPoints = function() {
            var self = this as AnodeTransfertChart;

            if (!self.options.usage) {
                return [];
            }

            // Working point and load
            var wu = self.options.usage.va.value;
            var load = self.options.usage.effectiveLoad;

            if (wu === null || wu === undefined || !load) {
                return [];
            }

            // A coordonates in units
            var wi = self.options.usage.iazero.value;
            var uamin = 0;
            var uamax = wu + load * wi / 1000;
            var iamin = wi + 1000 * wu / load;
            var iamax = 0;

            // B load and coordonates in unit
            var bload = load / 2;
            var ubmin = 0;
            var ubmax = wu;
            var ibmin = (wu - ubmin) * 1000 / bload;
            var ibmax = 0;

            // Pixels ab coordonates and crossing point
            var a1x = self.classASerie.xAxis.toPixels(uamin, true);
            var a2x = self.classASerie.xAxis.toPixels(uamax, true);
            var a1y = self.classASerie.yAxis.toPixels(iamin, true);
            var a2y = self.classASerie.yAxis.toPixels(iamax, true);
            var b1x: number;
            var b2x: number;
            var b1y: number;
            var b2y: number;
            var classA = true;
            var abx: number;
            var limitedRange = false; // If last curve are hidden, calculation is stopped at the last visible one

            var calcOutOfRangePoints = function(data: Array<VPoint>) {
                var dv = [] as Array<VPoint>;
                var va = data[0].va;
                var va0 = (uamax - va);
                var ia0 = data[0].ia;
                var vg = data[0].vg1;
                var vgstep = -data[0].vg1 + data[1].vg1;
                var vastep = va0 / 20;
                var next = va;

                while (next < uamax) {
                    // Add a last point
                    va = next;
                    next = Math.min(uamax, next + (uamax - va) / 2 + vastep);
                    var ia = ia0 * (uamax - next) / va0
                    vg -= vgstep;
                    data.splice(0, 0, {
                        vg1: vg,
                        va: next,
                        ia: ia,
                        classA: true,
                        overflow: false
                    })
                }
            }

            var calcMorePoints = function(data: Array<VPoint>, maxCorrectionFactor: number) {
                var dv = [] as Array<VPoint>;
                var bckp = [] as Array<VPoint>;
                // calc distance between points
                var length = data.length;
                for (var i = 0; i < length - 1; i++) {
                    dv.push({
                        va: data[i + 1].va - data[i].va,
                        ia: 0,
                        vg1: data[i + 1].vg1 - data[i].vg1,
                        classA: undefined
                    });
                    bckp.push(data[i]);
                }
                bckp.push(data[length - 1]);

                var insertpoint = 1;
                for (var i = 0; i < length - 1; i++) {
                    // Correcteur
                    var m = 1;
                    var p = 1;
                    var ratio = 1;

                    if (i > 0) {
                        m = p;
                    }
                    if (i < dv.length - 1) {
                        p = Math.sqrt(Math.sqrt(dv[i].va * dv[i + 1].vg1 / (dv[i].vg1 * dv[i + 1].va)));
                        ratio = p / m;
                        // Pentode in the vertical lines, forget the correction
                        if (isNaN(ratio)) {
                            ratio = 1;
                            p = 1;
                        } else if (ratio > (1 + maxCorrectionFactor)) {
                            ratio = 1 + maxCorrectionFactor;
                            p = ratio / m;
                        } else if (ratio < (1 - maxCorrectionFactor)) {
                            ratio = 1 - maxCorrectionFactor;
                            p = ratio / m;
                        }
                    }

                    var vg1 = bckp[i].vg1 + (bckp[i + 1].vg1 - bckp[i].vg1) / 2;
                    var x = bckp[i].va + (dv[i].va / 2) * ratio;
                    var bclass = self.abPoint && x <= self.abPoint.va;
                    var y: number;
                    if (bclass) {
                        y = ibmin * (ubmax - x) / ubmax;
                    } else {
                        y = iamin * (uamax - x) / uamax;
                    }
                    data.splice(insertpoint, 0, {
                        va: x,
                        ia: y,
                        vg1: vg1,
                        classA: !bclass
                    });
                    insertpoint += 2;
                }
            }

            var calcSerie = function(data: Array<VPoint>, serie: HighchartsSeriesObject, cb: Function) {
                if (serie.options.vg1 !== undefined) {
                    if (serie.visible) {
                        var path = serie.graph.element;
                        var length = path.getTotalLength();
                        var endOffset = length / 10000;
                        var tmOffset = length / 50;
                        var cm: [number, number];
                        var offset = length / 2;
                        var c1 = path.getPointAtLength(0);
                        var c2 = path.getPointAtLength(length);
                        var l1 = 0;
                        var l2 = length;
                        var n = 0;

                        var calcPointAtLength = function() {
                            var lmid = l1 + (l2 - l1) / 2;
                            var cmid = path.getPointAtLength(lmid);
                            var x1: number;
                            var x2: number;
                            var y1: number;
                            var y2: number;

                            if (classA) {
                                x1 = a1x;
                                y1 = a1y;
                                x2 = a2x;
                                y2 = a2y;
                            } else {
                                x1 = b1x;
                                y1 = b1y;
                                x2 = b2x;
                                y2 = b2y;
                            }

                            var i1 = self.calcLineIntersection(x1, y1, x2, y2, c1.x, c1.y, cmid.x, cmid.y);
                            if (i1) {
                                l2 = lmid;
                            } else {
                                var i2 = self.calcLineIntersection(x1, y1, x2, y2, cmid.x, cmid.y, c2.x, c2.y);
                                if (i2) {
                                    l1 = lmid;
                                } else {
                                    cb();
                                    return;
                                }
                            }

                            n++;
                            var offset = Math.abs(l2 - l1);
                            if (offset < tmOffset) {
                                tmOffset = 0;
                                cm = [cmid.x, cmid.y];
                            }

                            if (offset > endOffset) {
                                calcPointAtLength();
                                return;
                            } else {
                                if (classA && cmid.x <= abx) {
                                    // Reste
                                    classA = false;
                                    l1 = 0;
                                    l2 = length;
                                    c1 = path.getPointAtLength(0);
                                    c2 = path.getPointAtLength(length);
                                    // Occurs again for classB
                                    calcPointAtLength();
                                    return;
                                } else {
                                    data.push({
                                        va: serie.xAxis.toValue(cmid.x, true),
                                        ia: serie.yAxis.toValue(cmid.y, true),
                                        vg1: serie.options.vg1,
                                        classA: classA,
                                        // Add point for internal resistance calculation
                                        vt: cm && serie.xAxis.toValue(cm[0], true),
                                        it: cm && serie.yAxis.toValue(cm[1], true),
                                    });
                                    // console.log(serie.name + ' ' + n + ' iterations');
                                    cb();
                                    return;
                                }
                            }
                        }

                        calcPointAtLength();
                    } else {
                        if (serie.index <= 1) {
                            limitedRange = true;
                        }
                        cb();
                    }
                } else {
                    cb();
                }
            }

            // If class B serie, calc cross point with class A
            if (self.abPoint) {
                // B coordonate and crossing point in pixels                
                b1x = self.classBSerie.xAxis.toPixels(ubmin, true);
                b2x = self.classBSerie.xAxis.toPixels(ubmax, true);
                b1y = self.classBSerie.yAxis.toPixels(ibmin, true);
                b2y = self.classBSerie.yAxis.toPixels(ibmax, true);
                abx = self.classBSerie.xAxis.toPixels(self.abPoint.va, true);
            }

            var data: Array<VPoint> = [];
            // Order is important, class A first
            var series = self.chart.series;
            var calcNextSerie = function(data: Array<VPoint>, s: number) {
                if (s >= series.length) {
                    if (!limitedRange) {
                        if (data.length < 2) {
                            limitedRange = true;
                        } else {
                            // Because two last curves are not hidden, interpolate the vg1 points until x axis
                            calcOutOfRangePoints(data);
                        }
                    }
                    calcMorePoints(data, 0.1);
                    calcMorePoints(data, 0.3);
                    calcMorePoints(data, 0.5);
                    return;
                }

                calcSerie(data, series[s], function() {
                    calcNextSerie(data, s + 1);
                })
            }

            calcNextSerie(data, 0);
            return data;
        }

        public drawCrossPoints = function(redraw?: boolean) {
            var self = this as AnodeTransfertChart;

            if (self.crossPointsData && self.crossPointsData.length) {
                if (!self.crossSerie) {
                    self.crossSerie = self.chart.addSeries({
                        type: 'spline',
                        name: 'Cross Points',
                        color: 'red',
                        lineWidth: 0,
                        marker: {
                            enabled: true,
                            symbol: 'circle',
                            radius: 5
                        }
                    });
                } else {
                    self.crossSerie.setData([], false, false);
                }

                self.crossPointsData.forEach(function(point) {
                    self.crossSerie.addPoint({
                        x: point.va,
                        y: point.ia,
                        name: String(point.vg1),
                        value: point.vg1
                    } as HighchartsDataPoint, false);
                });

            } else if (self.crossSerie) {
                self.crossSerie.remove();
                self.crossSerie = undefined;
            }

            if (redraw) {
                self.chart.redraw(false);
            }

            return self;
        }
    }
}

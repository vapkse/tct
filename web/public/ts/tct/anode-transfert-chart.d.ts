declare module tct {
    interface AnodeTransfertChartOptions extends TubeChartOptions {
        tubeGraph?: TubeGraph;
        pmax?: number;
        editable: boolean;
        ready?: Function;
    }
    interface VPoint {
        vg1: number;
        va: number;
        ia: number;
        classA: boolean;
        overflow?: boolean;
        vt?: number;
        it?: number;
    }
    interface LoadLine {
        vazero: number;
        iazero: number;
        vamin: number;
        vamax: number;
        iamin: number;
        iamax: number;
        load: number;
    }
    interface GraphInfos {
        loadPoints: Array<VPoint>;
        workingPoint: VPoint;
        aLoad: LoadLine;
        bLoad: LoadLine;
        vg1Max: VPoint;
        vg1Min: VPoint;
        ab: VPoint;
        s: number;
        ri: number;
        calcHarmonics: () => {
            h2: number;
            h3: number;
            h4: number;
        };
    }
    class AnodeTransfertChart extends TubeChart {
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected options: AnodeTransfertChartOptions;
        protected longClickMenu: JQuery;
        private workingPointSerie;
        private workingPointSerieIndex;
        private classASerie;
        private classASerieIndex;
        private classBSerie;
        private classBSerieIndex;
        private crossSerie;
        private crossSerieIndex;
        private vinSerie0;
        private vinSerie0Index;
        private vinSerie1;
        private vinSerie1Index;
        private pmaxSerie;
        private pmaxSerieIndex;
        private crossPointsData;
        private abPoint;
        private aLoad;
        private bLoad;
        private vg1Max;
        private vg1Min;
        private seriesVisibility;
        private serieEvents;
        private browserVersion;
        constructor(options: AnodeTransfertChartOptions);
        protected oncreated(): void;
        getInfos: () => GraphInfos;
        updateWorkingPoint(): AnodeTransfertChart;
        updateLoadLines(): AnodeTransfertChart;
        private updateALoadLines;
        private updateBLoadLines;
        updateVinLines: () => AnodeTransfertChart;
        updatePmax: () => AnodeTransfertChart;
        private getMaxAxis;
        refreshLines(redraw?: boolean): void;
        refresh(): AnodeTransfertChart;
        exportToJson: (pointsPerLines?: number) => TubeGraph;
        private getNearestTrueCrossingLinePoints(va);
        private calcPointFromVg1;
        private calcPointFromVa;
        private crossingPoints;
        private calcLineIntersection;
        private calcCrossPoints;
        drawCrossPoints: (redraw?: boolean) => AnodeTransfertChart;
    }
}

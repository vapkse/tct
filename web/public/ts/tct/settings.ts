'option: strict';

module tct {
    export class Settings {
        public colorSettings: {
            scheme: string,
            theme: string
        }
        public change: (e: JQueryEventObject, data: Settings) => void = $.noop;

        constructor(storage?: Storage) {
            var self = <Settings>this;
            if (storage) {
                self.colorSettings = {
                    scheme: storage.getItem('tct-scheme') || "dark",
                    theme: storage.getItem('tct-theme') || "cyan"
                }
                self.change = function(e: JQueryEventObject, data: Settings) {
                    if (e.originalEvent.type === 'colorsettingschange') {
                        if (data.colorSettings.scheme) {
                            localStorage.setItem('tct-scheme', data.colorSettings.scheme);
                        }
                        if (data.colorSettings.theme) {
                            localStorage.setItem('tct-theme', data.colorSettings.theme);
                        }
                    }
                }
            } else {
                self.colorSettings = {
                    scheme: "dark",
                    theme: "cyan"
                };
            }
        }
    }
}

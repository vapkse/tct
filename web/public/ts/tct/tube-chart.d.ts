declare module tct {
    interface TubeChartOptions {
        element: JQuery;
        title?: string;
        height?: string | number | Function;
        usage?: TubeUsage.TubeUsage;
        created?: (chart: TubeChart) => void;
        optionschanged?: Function;
    }
    class TubeChart {
        protected options: TubeChartOptions;
        protected chart: HighchartsChartObject;
        protected $chart: JQuery;
        protected translations: JQuery;
        protected translationMapping: {
            [key: string]: JQuery;
        };
        private sizingTimer;
        contructor(options: TubeChartOptions): void;
        protected onoptionschanged: () => void;
        protected oncreated(): void;
        protected setHeight: () => void;
        protected onresize: (context: TubeChart) => () => void;
        protected setZoom: () => void;
        loadChartTitles: () => TubeChart;
        setOptions: (options: TubeChartOptions) => void;
        highcharts: HighchartsChartObject;
        destroy: () => void;
        redraw(animation?: boolean): void;
        refresh(): void;
    }
}

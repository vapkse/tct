declare module tct {
    interface UserInfosOptions {
        user: user.User;
    }
    class UserInfos {
        options: UserInfosOptions;
        protected element: JQuery;
        protected $login: JQuery;
        protected $logout: JQuery;
        private _trigger;
        protected _create: () => void;
        protected oncreated: () => void;
        setOptions: (options: UserInfosOptions) => JQuery;
        refresh: () => JQuery;
        protected onlogin: () => void;
    }
}
interface JQuery {
    userInfos(): JQuery;
    userInfos(method: string): JQuery;
    userInfos(method: string, options: tct.UserInfosOptions): JQuery;
    userInfos(method: 'setOptions', options: tct.UserInfosOptions): JQuery;
    userInfos(method: 'refresh'): JQuery;
    userInfos(options: tct.UserInfosOptions): JQuery;
}

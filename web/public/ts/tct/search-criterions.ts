'use strict';

module tct {
    export interface SearchCriterionsOptions {
        modelFields: { [name: string]: DataProviding.Field }
        expressions: tct.TubeSearch;
        bases: Array<TubeBase>,
        pinouts: Array<TubePinout>,
        types: Array<TubeType>,
        user: user.User
    }

    export interface searchCriterionsEvent extends JQueryEventObject {
        expressions: Array<tct.QueryExpression>
    }

    export class SearchCriterions {
        private criteriaTemplate: JQuery;
        public options: SearchCriterionsOptions;
        protected element: JQuery;
        private _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _create = function() {
            var self = <SearchCriterions>this;
            $.get('/resource?r=widgets%2Fsearch-criteria.html').done(function(crhtml) {
                self.criteriaTemplate = $('<div style="display:none;">'+ crhtml + '</div>').appendTo(self.element).children();
                self.refresh();
                self.onready();
            });
        }

        public setExpressions = function(expr: tct.TubeSearch) {
            var self = <SearchCriterions>this;
            self.options.expressions = expr;
            self.refresh();
            return self;
        }

        public refresh = function() {
            var self = <SearchCriterions>this;

            self.element.children('[field-criteria]').remove();
            if (self.options.expressions) {
                var searchExprs = self.options.expressions.queryExpressions;
                if (searchExprs && searchExprs.length) {
                    searchExprs.map(searchExpr => self.addNew(searchExpr));
                    return;
                }
            }

            self.addNew();
        }

        public addNew = function(expression?: tct.QueryExpression) {
            var self = this as SearchCriterions;
            var criteria: JQuery;

            criteria = $('<div field-criteria></div>').appendTo(self.element).searchCriteria({
                modelFields: self.options.modelFields,
                user: self.options.user,
                expression: expression,
                template: self.criteriaTemplate.clone(),
                lookups: {
                    base: self.options.bases,
                    pinout: self.options.pinouts,
                    type: self.options.types
                }
            }).bind('searchcriteriadelete', function(e: JQueryEventObject) {
                setTimeout(function() {
                    $(e.target).closest('[field-criteria]').remove();
                    self.onchange();
                }, 0);
            }).bind('searchcriteriachange', function(e: JQueryEventObject) {
                self.onchange();
            }).bind('searchcriteriasearch', function(e: JQueryEventObject) {
                self.onsearch();
            });

            return self;
        }

        protected onready = function() {
            var self = this as SearchCriterions;
            var e = jQuery.Event("ready");
            self._trigger('ready', e);
        }

        protected onchange = function() {
            var self = this as SearchCriterions;
            var e = <searchCriterionsEvent>jQuery.Event("change");
                       
            // Create expression array
            var expressions: Array<tct.QueryExpression> = [];
            
            // Fill from criteria
            var children = self.element.find('[field-criteria]');
            for (var i = 0; i < children.length; i++) {
                expressions.push($(children[i]).searchCriteria('expression'));
            }

            e.expressions = expressions
            self._trigger('change', e);
        }

        protected onsearch = function() {
            var self = this as SearchCriterions;
            var e = jQuery.Event('search');
            self._trigger('search', e);
        }
    }
}

$.widget("tct.searchCriterions", new tct.SearchCriterions());

interface JQuery {
    searchCriterions(): JQuery;
    searchCriterions(options: tct.SearchCriterionsOptions): JQuery;
    searchCriterions(method: string): tct.SearchCriterions;
    searchCriterions(method: 'addNew'): tct.SearchCriterions;
    searchCriterions(method: string, options: tct.TubeSearch): tct.SearchCriterions;
    searchCriterions(method: 'setExpressions', options: tct.TubeSearch): tct.SearchCriterions;
}
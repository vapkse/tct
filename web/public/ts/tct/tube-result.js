'use strict';
var tct;
(function (tct) {
    var TubeResult = (function () {
        function TubeResult() {
            this._create = function () {
                var self = this;
                self.element.append(self.options.template).addClass('tube-result');
                self.$ = {
                    binding: self.element.find('[data-bind]'),
                    h2: self.element.find('[data-show="dh"]'),
                    a: self.element.find('[data-show="a"]'),
                    g2: self.element.find('[data-show="g2"]'),
                    baseview: self.element.find('#baseview'),
                    pinoutview: self.element.find('#pinoutview'),
                    menu: self.element.find('.menu'),
                    units: self.element.find('[data-show="units"]'),
                    unitSelector: self.element.find('#units'),
                    type: self.element.find('#type'),
                    docs: self.element.find('.docs'),
                    userInfos: self.element.find('#user'),
                };
                self.refresh();
            };
            this.setOptions = function (options) {
                var self = this;
                self.options.datas = options.datas;
                self.options.id = options.id;
                return self.refresh();
            };
            this.refresh = function () {
                var self = this;
                var options = this.options;
                self.element.attr('data-id', options.id);
                self.$.binding.each(function (index, element) {
                    var $element = $(element);
                    var valueName = $element.attr('data-bind');
                    var value = options.datas[valueName];
                    if (value !== undefined) {
                        $element.text(value);
                    }
                });
                if (options.datas.typeuid) {
                    self.$.type.attr('lang', '').attr('uid', options.datas.typeuid);
                }
                else {
                    self.$.type.removeAttr('lang').removeAttr('uid');
                }
                if (options.datas.base !== '-') {
                    self.$.baseview.removeClass('disabled').addClass('fg-theme-over');
                }
                else {
                    self.$.baseview.addClass('disabled').removeClass('fg-theme-over');
                }
                if (options.datas.pinout !== '-') {
                    self.$.pinoutview.removeClass('disabled').addClass('fg-theme-over');
                }
                else {
                    self.$.pinoutview.addClass('disabled').removeClass('fg-theme-over');
                }
                if (options.datas.docsCount !== 0) {
                    self.$.docs.removeClass('disabled').addClass('fg-theme-over');
                }
                else {
                    self.$.docs.addClass('disabled').removeClass('fg-theme-over');
                }
                if (options.datas.h2tt) {
                    self.$.h2.show().closest('.cell').attr('tooltip', options.datas.h2tt);
                }
                else {
                    self.$.h2.hide();
                }
                if (options.datas.vamax || options.datas.pamax) {
                    self.$.a.show();
                }
                else {
                    self.$.a.hide();
                }
                if (options.datas.userId) {
                    self.element.attr('user-id', options.datas.userId);
                }
                else {
                    self.element.removeAttr('user-id');
                }
                if (options.datas.userEmail) {
                    self.$.userInfos.show().find('#user-name').text(options.datas.userEmail.userName || options.datas.userEmail.email);
                }
                else {
                    self.$.userInfos.hide();
                }
                if (self.options.datas.unitsCount > 1) {
                    self.$.units.show();
                    self.$.unitSelector.children().each(function (index, elem) {
                        if (index >= self.options.datas.unitsCount) {
                            $(elem).hide();
                        }
                        else {
                            $(elem).show();
                            if (index === self.options.datas.unitIndex) {
                                $(elem).removeClass('no-border').addClass('bo-lighter');
                            }
                            else {
                                $(elem).addClass('no-border').removeClass('bo-lighter');
                            }
                        }
                    });
                    self.$.g2.show();
                    if (options.datas.vg2max || options.datas.pg2max) {
                        self.$.g2.css('visibility', 'visible');
                    }
                    else {
                        self.$.g2.css('visibility', 'hidden');
                    }
                }
                else {
                    self.$.units.hide();
                    self.$.g2.css('visibility', 'visible');
                    if (options.datas.vg2max || options.datas.pg2max) {
                        self.$.g2.show();
                    }
                    else {
                        self.$.g2.hide();
                    }
                }
                return self.element;
            };
        }
        return TubeResult;
    })();
    tct.TubeResult = TubeResult;
})(tct || (tct = {}));
$.widget("tct.tubeResult", new tct.TubeResult());

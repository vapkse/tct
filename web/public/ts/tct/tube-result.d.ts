declare module tct {
    interface TubeResultOptions {
        datas: TubeResultData;
        template?: string;
        id: number;
    }
    interface TubeResultEvent extends JQueryEventObject {
        id: number;
    }
    class TubeResult {
        options: TubeResultOptions;
        private $;
        protected element: JQuery;
        protected _create: () => void;
        setOptions: (options: TubeResultOptions) => JQuery;
        refresh: () => JQuery;
    }
}
interface JQuery {
    tubeResult(): tct.TubeResult;
    tubeResult(method: string): JQuery;
    tubeResult(method: string, options: tct.TubeResultOptions): JQuery;
    tubeResult(method: 'setOptions', options: tct.TubeResultOptions): JQuery;
    tubeResult(method: 'refresh'): JQuery;
    tubeResult(options: tct.TubeResultOptions): JQuery;
}

declare module tct {
    interface FileIconOptions {
        filename: string;
    }
    class FileIcon {
        private options;
        private element;
        private _create();
        setOptions(options: FileIconOptions): FileIcon;
        refresh(): FileIcon;
    }
}
interface JQuery {
    fileIcon(): JQuery;
    fileIcon(method: string, options: tct.FileIconOptions): tct.FileIcon;
    fileIcon(method: 'setOptions', options: tct.FileIconOptions): tct.FileIcon;
    fileIcon(method: 'refresh'): tct.FileIcon;
    fileIcon(method: 'instance'): tct.FileIcon;
    fileIcon(options: tct.FileIconOptions): JQuery;
}

'use strict';
var tct;
(function (tct) {
    var DocSelector = (function () {
        function DocSelector() {
            this.oncreated = function () {
                var self = this;
                var e = jQuery.Event('created');
                self._trigger('created', e);
            };
            this.onokclicked = function () {
                var self = this;
                var e = jQuery.Event('okclicked');
                e.selectedIds = [];
                self.window.find('input[type="checkbox"][checked="checked"],input[type="radio"]:checked').closest('[data-id]').each(function (index, element) {
                    e.selectedIds.push($(element).attr('data-id'));
                });
                self._trigger('okclicked', e);
            };
        }
        DocSelector.prototype._create = function () {
            var self = this;
            var options = this.options;
            requirejs(['fileIcon'], function () {
                $.get('/resource?r=widgets%2Fdoc-selector.html').done(function (html) {
                    self.window = $(html).appendTo(self.element).addClass(options.mode);
                    self.window.find('.btn-close').on('click', function () {
                        self.hide();
                    });
                    self.backdrop = $('<div id="doc-selector-backdrop" class="backdrop bg-darker"></div>').appendTo('body').on('click', function () {
                        self.hide();
                    });
                    self.window.find('#cancel').on('click', function () {
                        self.hide();
                    });
                    self.window.find('#ok').on('click', function () {
                        self.onokclicked();
                        self.hide();
                    });
                    self.docContainer = self.window.find('#docs').on('click', function (e) {
                        var target = $(e.target).closest('[data-click]');
                        var func = target.attr('data-click');
                        var dataId = target.closest('[data-id]').attr('data-id');
                        switch (func) {
                            case 'go':
                                var stb = [];
                                stb.push('/usages?usageid=');
                                stb.push(encodeURIComponent(dataId));
                                stb.push('&id=');
                                stb.push(String(self.options.tubeData._id));
                                if (self.options.userid) {
                                    stb.push('&userid=');
                                    stb.push(encodeURIComponent(self.options.userid));
                                }
                                stb.push('&rl=');
                                stb.push(encodeURIComponent(location.search));
                                location.href = stb.join('');
                            case 'download':
                                var docs = self.options.tubeData.documents.filter(function (d) { return d._id === dataId; });
                                var docViewerOptions = {
                                    document: docs[0],
                                    tubeData: self.options.tubeData,
                                    isNew: false,
                                    error: function (error) {
                                        self.messagerControl.show('error', error);
                                    }
                                };
                                if (!self.docViewer) {
                                    requirejs(['docViewer'], function () {
                                        self.docViewer = new tct.DocViewer();
                                        self.docViewer.view(docViewerOptions);
                                    });
                                }
                                else {
                                    self.docViewer.view(docViewerOptions);
                                }
                                break;
                        }
                    });
                    self.docTemplate = self.element.find('#docTemplate').children();
                    self.docContainer.tooltipmanager().data('modern-tooltipmanager');
                    self.messagerControl = $('#notify').messager().data('modern-messager');
                    self.refresh();
                    self.oncreated();
                });
            });
        };
        ;
        DocSelector.prototype.refresh = function () {
            var self = this;
            self.docContainer.empty();
            self.window.find('#title').html(self.options.title);
            self.window.find('#tname').text(' - ' + self.options.tubeData.name);
            var count = 0;
            if (self.options.showUsages) {
                var usages = self.options.tubeData.usages;
                if (usages) {
                    usages.map(function (usage) {
                        var item = self.docTemplate.clone().appendTo(self.docContainer).attr('data-id', usage._id);
                        count++;
                        item.find('.label').text(usage.name);
                        item.find('#icon').attr('class', 'mif-apps');
                        item.find('button').attr('data-click', 'go').find('#action').attr('class', 'mif-arrow-right');
                    });
                }
            }
            var documents = self.options.tubeData.documents;
            if (documents) {
                documents.map(function (doc) {
                    if (!self.options.filter || self.options.filter.test(doc.filename)) {
                        var selected = self.options.selectedIds && self.options.selectedIds.indexOf(doc._id) >= 0;
                        var item = self.docTemplate.clone().attr('data-id', doc._id).appendTo(self.docContainer);
                        count++;
                        item.find('.label').text(doc.description);
                        item.find('#icon').fileIcon({ filename: doc.filename });
                        item.find('button').attr('data-click', 'download').find('#action').attr('class', 'mif-file-download');
                        if (self.options.mode === 'selector') {
                            item.find('.input-control.checkbox').checkbox({ value: selected });
                        }
                        else if (self.options.mode === 'radio') {
                            if (selected) {
                                item.find('.input-control.radio input').prop('checked', true);
                            }
                        }
                    }
                });
            }
            if (count === 0) {
                self.window.find('#no-doc').show();
            }
            else {
                self.window.find('#no-doc').hide();
            }
        };
        DocSelector.prototype.setOptions = function (options) {
            var self = this;
            self.options = options;
            self.refresh();
            setTimeout(function () {
                self.show();
            }, 1);
            return self;
        };
        DocSelector.prototype.show = function () {
            var self = this;
            if (self.backdrop) {
                self.backdrop.show();
            }
            self.window.css('opacity', 0);
            self.window.show();
            self.window.animate({ opacity: 1 }, 500);
            return self;
        };
        DocSelector.prototype.hide = function () {
            var self = this;
            self.window.animate({ opacity: 0 }, 200, function () {
                self.backdrop.hide();
                self.window.hide();
            });
            return self;
        };
        return DocSelector;
    })();
    tct.DocSelector = DocSelector;
})(tct || (tct = {}));
$.widget("tct.docSelector", new tct.DocSelector());

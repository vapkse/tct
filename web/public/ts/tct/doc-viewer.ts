module tct {
    export interface DocViewerOptions {
        tubeData?: TubeData, // Id of the tube
        document: TubeDoc,
        isNew: boolean,
        success?: () => void,
        error?: (error: string) => void
    }

    export class DocViewer {
        private fileViewer: JQuery;

        constructor() {

        }

        public view(opts: DocViewerOptions) {
            var self = this as DocViewer;

            requirejs(['ajaxDownloadFile'], function() {
                // Get file extension
                var filename = opts.document.filename;

                var url = encodeURIComponent(filename);
                if (opts.isNew) {
                    url += '&isnew=true';
                }

                var fileInfo = /^.+\.([^.]+)$/.exec(filename);
                var ext = fileInfo.length > 1 && fileInfo[1].toLowerCase();
                switch (ext) {
                    case 'tds':
                        var options: utils.AjaxDownloadFileOptions = {
                            url: '/download?url=' + url,
                            dataType: 'json'
                        }

                        utils.AjaxDownloadFile.download(options).onSuccessFinish(function(status, statusText, res, headers) {
                            var json = res['json'];
                            var result: UploadResultFile = typeof json === 'string' ? JSON.parse(json) : json;
                            if (result.error) {
                                if (opts.error) {
                                    opts.error(result.error.message);
                                }
                            } else {
                                if (!result.tubeGraph.name) {
                                    result.tubeGraph.name = opts.document.description;
                                }
                                var viewerOptions: FileViewerOptions = {
                                    tubeId: opts.tubeData._id,
                                    dataId: opts.document._id,
                                    tubeGraph: result.tubeGraph,
                                    graphTitle: opts.document.description,
                                    graphUrl: opts.document.filename,
                                    isNew: opts.isNew
                                }
                                if (!self.fileViewer) {
                                    requirejs(['fileViewer'], function() {
                                        self.fileViewer = $('[file-viewer]');
                                        self.fileViewer.fileViewer(viewerOptions).on('fileviewercreated', function() {
                                            self.fileViewer.fileViewer('show');
                                        });
                                    })
                                } else {
                                    self.fileViewer.fileViewer('setOptions', viewerOptions);
                                }
                            }
                            if (opts.success) {
                                opts.success();
                            }
                        }).onErrorFinish(function(status, statusText, res, headers) {
                            if (opts.error) {
                                opts.error(statusText);
                            }
                        });
                        break;

                    case 'png':
                    case 'jpg':
                    case 'jpeg':
                    case 'gif':
                    case 'bmp':
                    case 'tif':
                    case 'tiff':
                        var options: utils.AjaxDownloadFileOptions = {
                            url: '/download?url=' + url,
                            dataType: 'blob'
                        }

                        utils.AjaxDownloadFile.download(options).onSuccessFinish(function(status, statusText, res, headers) {
                            var downloadUrl = URL.createObjectURL(res['blob']);
                            var vwrOptions: FileViewerOptions = {
                                tubeId: opts.tubeData._id,
                                dataId: opts.document._id,
                                file: downloadUrl,
                                isNew: opts.isNew
                            }
                            if (!self.fileViewer) {
                                requirejs(['fileViewer'], function() {
                                    self.fileViewer = $('[file-viewer]');
                                    self.fileViewer.fileViewer(vwrOptions).bind('fileviewercreated', function() {
                                        self.fileViewer.fileViewer('show');
                                    });
                                })
                            } else {
                                self.fileViewer.fileViewer('setOptions', vwrOptions);
                            }
                            if (opts.success) {
                                opts.success();
                            }
                        }).onErrorFinish(function(status, statusText, res, headers) {
                            if (opts.error) {
                                opts.error(statusText);
                            }
                        });
                        break;

                    default:
                        var options: utils.AjaxDownloadFileOptions = {
                            url: '/download?url=' + url,
                            dataType: 'blob'
                        }

                        utils.AjaxDownloadFile.download(options).onSuccessFinish(function(response, status, res, headers) {
                            var downloadUrl = URL.createObjectURL(res['blob']);

                            // Use HTML5 a[download] attribute to specify filename.                                        
                            var a = document.createElement("a") as HTMLAnchorElement;
                            a.href = downloadUrl;
                            a.download = filename;
                            document.body.appendChild(a);
                            a.click();
                            if (opts.success) {
                                opts.success();
                            }
                        }).onErrorFinish(function(status, statusText, res, headers) {
                            if (opts.error) {
                                opts.error(statusText);
                            }
                        });
                        break;
                }
            })

            return self;
        }
    }
}

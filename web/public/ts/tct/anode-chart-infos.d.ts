declare module tct {
    interface AnodeChartInfosOptions {
        infos: GraphInfos;
        usage: TubeUsage.TubeUsage;
    }
    class AnodeChartInfos {
        protected element: JQuery;
        protected options: AnodeChartInfosOptions;
        private $;
        protected _create: () => void;
        setOptions(options: AnodeChartInfosOptions): void;
        refresh(): void;
    }
}
interface JQuery {
    anodeChartInfos(): JQuery;
    anodeChartInfos(method: string): JQuery;
    anodeChartInfos(method: string, options: tct.AnodeChartInfosOptions): JQuery;
    anodeChartInfos(method: 'setOptions', options: tct.AnodeChartInfosOptions): JQuery;
    anodeChartInfos(method: 'refresh'): JQuery;
    anodeChartInfos(options: tct.AnodeChartInfosOptions): JQuery;
}

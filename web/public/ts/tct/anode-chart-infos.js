'use strict';
var tct;
(function (tct) {
    var AnodeChartInfos = (function () {
        function AnodeChartInfos() {
            this._create = function () {
                var self = this;
                $.get('/resource?r=widgets%2Fanode-chart-infos.html').done(function (html) {
                    $(html).appendTo(self.element);
                    self.$ = {
                        vg1zero: self.element.find('#vg1zero'),
                        vapp: self.element.find('#vapp'),
                        pmax: self.element.find('#pmax'),
                        gain: self.element.find('#gain'),
                        trans: self.element.find('#trans'),
                        ri: self.element.find('#ri'),
                        h2: self.element.find('#h2'),
                        h3: self.element.find('#h3'),
                        h4: self.element.find('#h4'),
                        htot: self.element.find('#htot'),
                    };
                    self.refresh();
                });
            };
        }
        AnodeChartInfos.prototype.setOptions = function (options) {
            var self = this;
            self.options = options;
            self.refresh();
        };
        AnodeChartInfos.prototype.refresh = function () {
            var self = this;
            var infos = self.options.infos;
            var mode = self.options.usage.mode.value;
            if (!self.$ || !infos || !mode || mode === 'na') {
                self.element.hide();
                return;
            }
            self.element.show();
            var vg1zero = infos.workingPoint && infos.workingPoint.vg1;
            self.$.vg1zero.html(!isNaN(vg1zero) && vg1zero !== null ? vg1zero.toFixed(2) + 'V' : '-');
            var pmax = 0;
            var vapp = 0;
            var vp = 0;
            if (self.options.usage) {
                if (infos.vg1Max && infos.vg1Min) {
                    vp = infos.vg1Min.va - infos.vg1Max.va;
                    if (self.options.usage.mode.value !== 'se') {
                        vp *= 2;
                        var vcross = infos.ab && (infos.ab.va - 0.15 * infos.ab.va);
                        if (vcross <= infos.vg1Min.va && vcross >= infos.vg1Max.va) {
                            var vaa = infos.vg1Min.va - vcross;
                            var vab = vcross - infos.vg1Max.va;
                            vp = (vaa + vab * 2) * 2;
                        }
                    }
                }
                pmax = self.options.usage.load.value && vp && vp * vp / 8 / self.options.usage.load.value;
                if (infos.workingPoint && infos.vg1Max) {
                    if (self.options.usage.mode.value === 'se') {
                        vapp = vp;
                    }
                    else {
                        vapp = 2 * (infos.workingPoint.va - infos.vg1Max.va);
                    }
                }
            }
            if (pmax) {
                var unit = 'W';
                if (pmax < 1) {
                    pmax = pmax * 1000;
                    unit = 'mW';
                }
                if (pmax < 10) {
                    self.$.pmax.html(pmax.toFixed(2) + unit);
                }
                else if (pmax < 100) {
                    self.$.pmax.html(pmax.toFixed(1) + unit);
                }
                else {
                    self.$.pmax.html(pmax.toFixed(0) + unit);
                }
            }
            else {
                self.$.pmax.html('-');
            }
            self.$.vapp.html(!isNaN(vapp) && vapp !== null ? vapp.toFixed(0) + 'Vpp' : '-');
            var gain = infos.vg1Min && infos.vg1Max && (infos.vg1Min.va - infos.vg1Max.va) / (infos.vg1Max.vg1 - infos.vg1Min.vg1);
            self.$.gain.html(!isNaN(gain) ? gain.toFixed(1) + 'V/V' : '-');
            self.$.trans.html(!isNaN(infos.s) ? infos.s.toFixed(2) + 'mA/V' : '-');
            self.$.ri.html(!isNaN(infos.ri) ? infos.ri.toFixed(2) + 'kohms' : '-');
            var hInfos = infos.calcHarmonics();
            var htot = hInfos.h2 + hInfos.h3 + hInfos.h4;
            self.$.h2.html(!isNaN(hInfos.h2) ? hInfos.h2.toFixed(2) + '%' : '-');
            self.$.h3.html(!isNaN(hInfos.h3) ? hInfos.h3.toFixed(2) + '%' : '-');
            self.$.h4.html(!isNaN(hInfos.h4) ? hInfos.h4.toFixed(2) + '%' : '-');
            self.$.htot.html(!isNaN(htot) ? htot.toFixed(2) + '%' : '-');
        };
        return AnodeChartInfos;
    })();
    tct.AnodeChartInfos = AnodeChartInfos;
})(tct || (tct = {}));
$.widget("tct.anodeChartInfos", new tct.AnodeChartInfos());

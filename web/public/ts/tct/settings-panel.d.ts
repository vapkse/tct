declare module tct {
    interface SettingsPanelEvent extends JQueryEventObject {
        data: SettingsPanelOptions;
    }
    interface SettingsPanel extends JQueryUI.Widget {
        refresh(): void;
        show(): void;
        hide(): void;
        toogle(): void;
    }
    interface SettingsPanelOptions extends Settings {
    }
    class SettingsPanel {
        private options;
        private backdrop;
        private element;
        private _trigger;
        private _create();
        protected oncreated: () => void;
    }
}
interface JQuery {
    settingsPanel(): tct.SettingsPanel;
    settingsPanel(method: string): tct.SettingsPanel;
    settingsPanel(options: tct.SettingsPanelOptions): tct.SettingsPanel;
}

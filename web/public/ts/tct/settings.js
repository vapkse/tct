'option: strict';
var tct;
(function (tct) {
    var Settings = (function () {
        function Settings(storage) {
            this.change = $.noop;
            var self = this;
            if (storage) {
                self.colorSettings = {
                    scheme: storage.getItem('tct-scheme') || "dark",
                    theme: storage.getItem('tct-theme') || "cyan"
                };
                self.change = function (e, data) {
                    if (e.originalEvent.type === 'colorsettingschange') {
                        if (data.colorSettings.scheme) {
                            localStorage.setItem('tct-scheme', data.colorSettings.scheme);
                        }
                        if (data.colorSettings.theme) {
                            localStorage.setItem('tct-theme', data.colorSettings.theme);
                        }
                    }
                };
            }
            else {
                self.colorSettings = {
                    scheme: "dark",
                    theme: "cyan"
                };
            }
        }
        return Settings;
    })();
    tct.Settings = Settings;
})(tct || (tct = {}));

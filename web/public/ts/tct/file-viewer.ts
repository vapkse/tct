'use strict';

module tct {
    export interface FileViewerOptions {
        tubeId: number,
        dataId: string,
        file?: string,
        tubeGraph?: TubeGraph,
        graphTitle?: string,
        graphUrl?: string,
        unitIndex?: number,
        isNew: boolean,
    }

    export class FileViewer {
        private options: FileViewerOptions;
        private backdrop: JQuery;
        private window: JQuery;
        private element: JQuery;
        private chart: AnodeTransfertChart;
        private docViewer: tct.DocViewer;
        private download: JQuery;
        private usage: JQuery;
        private create: JQuery;
        private dropDownMenu: JQuery;
        private tubeGraph: TubeGraph;
        private messagerControl: modern.Messager;
        private _trigger: (eventName: string, event: JQueryEventObject) => void;
        private _create() {
            var self = <FileViewer>this;

            var options = <FileViewerOptions>this.options;

            // Resource need to be renderized, so the call must be like this
            $.get('/resource?r=widgets%2Ffile-viewer.html').done(function(html) {
                self.window = $(html).appendTo(self.element);

                self.window.find('.btn-close').on('click', function() {
                    self.hide();
                })

                self.backdrop = $('<div id="file-viewer-backdrop" class="backdrop bg-darker"></div>').appendTo('body').on('click', function() {
                    self.hide();
                })

                self.download = self.element.find('#download').hide().on('click', function(e) {
                    self.showDownloadMenu($(e.target));
                });

                self.element.find('#spicemodel').on('click', function(e) {
                    self.createFunction();
                })

                self.usage = self.element.find('#usage').hide();
                self.create = self.element.find('#create').hide();
                self.window.tooltipmanager().data('modern-tooltipmanager');

                self.messagerControl = $('#notify').messager().data('modern-messager');

                self.refresh();
                self.oncreated();
            });
        };

        private createFunction = function() {
            var self = <FileViewer>this;
            var kg11 = 300;
            var kg1m = 2000;
            var kg12 = 4000;
            var kp1 = 10;
            var kpm = 60;
            var kp2 = 500;
            var kvb1 = 1;
            var kvbm = 1;
            var kvb2 = 100;
            var mu1 = 2;
            var mum = 65;
            var mu2 = 100;
            var ex1 = 1.1;
            var exm = 1.22;
            var ex2 = 1.6;

            // 12ax7b
            // var exm = 1.20;
            // var mum = 75;
            // var kvbm = 1;           

            // Ec86
            // var exm = 1.45;
            // var mum = 60;
            // var kvbm = 1;
            // u = mu + vg1 * vg1;

            var iat = function(vg1: number, va: number, kp: number, kg1: number, kvb: number, mu: number, ex: number) {
                /*if (vg1 + va / kp < 0) {
                    return 0;
                } else {
                    return Math.pow((vg1 + va / kp), 1.25) / kg1;
                }*/
                var u = mu + vg1 * vg1 * 1.32;
                var e1 = (va / kp) * Math.log(1 + Math.exp(kp * (1 / u + vg1 / va)))
                var pow = Math.pow(e1, ex);
                if (isNaN(pow)) {
                    pow = 0;
                }
                return (pow / kg1) * (1 + Math.sign(e1))
            }

            var tubeGraph = self.chart.exportToJson(300);

            var calcMu = function() {
                var dmu1 = 0;
                var dmu2 = 0;
                var dmum = 0;
                for (var c = 0; c < tubeGraph.c.length; c++) {
                    var curve = tubeGraph.c[c];
                    for (var p = 0; p < curve.p.length; p++) {
                        var point = curve.p[p];
                        var d1 = (point.ik - iat(point.vg1, point.va, kpm, kg1m, kvbm, mu1, exm));
                        dmu1 += (d1 * d1);
                        var d2 = (point.ik - iat(point.vg1, point.va, kpm, kg1m, kvbm, mu2, exm));
                        dmu2 += (d2 * d2);
                        var dm = (point.ik - iat(point.vg1, point.va, kpm, kg1m, kvbm, mum, exm));
                        dmum += (dm * dm);
                    }
                }

                if (dmu1 < dmum && dmum < dmu2 && mu1 > 0) {
                    mu2 = mum;
                    mu1 = Math.max(mu1 / 2, mu1 - (mum - mu1));
                } else if (dmu2 < dmum && dmum < dmu1) {
                    mu1 = mum;
                    mu2 += (mu2 - mum);
                } else if (dmu2 - dmum < dmu1 - dmum) {
                    mu1 = mum;
                } else {
                    mu2 = mum;
                }
                mum = mu1 + (mu2 - mu1) / 2;
            }

            var calcExm = function() {
                var dex1 = 0;
                var dex2 = 0;
                var dexm = 0;
                for (var c = 0; c < tubeGraph.c.length; c++) {
                    var curve = tubeGraph.c[c];
                    for (var p = 0; p < curve.p.length; p++) {
                        var point = curve.p[p];
                        var d1 = (point.ik - iat(point.vg1, point.va, kpm, kg1m, kvbm, mum, ex1));
                        dex1 += (d1 * d1);
                        var d2 = (point.ik - iat(point.vg1, point.va, kpm, kg1m, kvbm, mum, ex2));
                        dex2 += (d2 * d2);
                        var dm = (point.ik - iat(point.vg1, point.va, kpm, kg1m, kvbm, mum, exm));
                        dexm += (dm * dm);
                    }
                }

                if (dex1 < dexm && dexm < dex2 && ex1 > 0) {
                    ex2 = exm;
                    ex1 = Math.max(ex1 / 2, ex1 - (exm - ex1));
                } else if (dex2 < dexm && dexm < dex1) {
                    ex1 = exm;
                    ex2 += (ex2 - exm);
                } else if (dex2 - dexm < dex1 - dexm) {
                    ex1 = exm;
                } else {
                    ex2 = exm;
                }
                exm = ex1 + (ex2 - ex1) / 2;
            }

            var calcKg1 = function() {
                var dg11 = 0;
                var dg12 = 0;
                var dg1m = 0;
                for (var c = 0; c < tubeGraph.c.length; c++) {
                    var curve = tubeGraph.c[c];
                    for (var p = 0; p < curve.p.length; p++) {
                        var point = curve.p[p];
                        var d1 = (point.ik - iat(point.vg1, point.va, kpm, kg11, kvbm, mum, exm));
                        dg11 += (d1 * d1);
                        var d2 = (point.ik - iat(point.vg1, point.va, kpm, kg12, kvbm, mum, exm));
                        dg12 += (d2 * d2);
                        var dm = (point.ik - iat(point.vg1, point.va, kpm, kg1m, kvbm, mum, exm));
                        dg1m += (dm * dm);
                    }
                }

                if (dg11 < dg1m && dg1m < dg12 && kg11 > 0) {
                    kg12 = kg1m;
                    kg11 = Math.max(kg11 / 2, kg11 - (kg1m - kg11));
                } else if (dg12 < dg1m && dg1m < dg11) {
                    kg11 = kg1m;
                    kg12 += (kg12 - kg1m);
                } else if (dg12 - dg1m < dg11 - dg1m) {
                    kg11 = kg1m;
                } else {
                    kg12 = kg1m;
                }
                kg1m = kg11 + (kg12 - kg11) / 2;
            }

            var calcKp = function() {
                var dp1 = 0;
                var dp2 = 0;
                var dpm = 0;
                for (var c = 0; c < tubeGraph.c.length; c++) {
                    var curve = tubeGraph.c[c];
                    for (var p = 0; p < curve.p.length; p++) {
                        var point = curve.p[p];
                        var d1 = (point.ik - iat(point.vg1, point.va, kp1, kg1m, kvbm, mum, exm));
                        dp1 += (d1 * d1);
                        var d2 = (point.ik - iat(point.vg1, point.va, kp2, kg1m, kvbm, mum, exm));
                        dp2 += (d2 * d2);
                        var dm = (point.ik - iat(point.vg1, point.va, kpm, kg1m, kvbm, mum, exm));
                        dpm += (dm * dm);
                    }
                }

                if (dp1 < dpm && dpm < dp2 && kp1 > 0) {
                    kp2 = kpm;
                    kp1 = Math.max(kp1 / 2, kp1 - (kpm - kp1));
                } else if (dp2 < dpm && dpm < dp1) {
                    kp1 = kpm;
                    kp2 += (kp2 - kpm);
                } else if (dp2 - dpm < dp1 - dpm) {
                    kp1 = kpm;
                } else {
                    kp2 = kpm;
                }
                kpm = kp1 + (kp2 - kp1) / 2;
            }

            var calcKvb = function() {
                var dvb1 = 0;
                var dvb2 = 0;
                var dvbm = 0;
                for (var c = 0; c < tubeGraph.c.length; c++) {
                    var curve = tubeGraph.c[c];
                    for (var p = 0; p < curve.p.length; p++) {
                        var point = curve.p[p];
                        var d1 = (point.ik - iat(point.vg1, point.va, kpm, kg1m, kvb1, mum, exm));
                        dvb1 += (d1 * d1);
                        var d2 = (point.ik - iat(point.vg1, point.va, kpm, kg1m, kvb2, mum, exm));
                        dvb2 += (d2 * d2);
                        var dm = (point.ik - iat(point.vg1, point.va, kpm, kg1m, kvbm, mum, exm));
                        dvbm += (dm * dm);
                    }
                }

                if (dvb1 < dvbm && dvbm < dvb2 && kvb1 > 0) {
                    kvb2 = kvbm;
                    kvb1 = Math.max(kvb1 / 2, kvb1 - (kvbm - kvb1));
                } else if (dvb2 < dvbm && dvbm < dvb1) {
                    kvb1 = kvbm;
                    kvb2 += (kvb2 - kvbm);
                } else if (dvb2 - dvbm < dvb1 - dvbm) {
                    kvb1 = kvbm;
                } else {
                    kvb2 = kvbm;
                }
                kvbm = kvb1 + (kvb2 - kvb1) / 2;
            }
            
            var j = 0;
            /*
            while (j++ < 33) {
                calcMu();
            }

            var j = 0;
            while (j++ < 33) {
                calcExm();
            }

            j = 0;
            while (j++ < 33) {
                calcKvb();
            }*/

            var i = 0;
            while (i++ < 100) {



                j = 0;
                while (j++ < 1) {
                    calcKg1();
                }

                j = 0;
                while (j++ < 1) {
                    calcKp();
                }
            }

            // Generate equation tube graph
            for (var c = 0; c < tubeGraph.c.length; c++) {
                var curve = tubeGraph.c[c];
                for (var p = 0; p < curve.p.length; p++) {
                    var point = curve.p[p];
                    point.ik = iat(point.vg1, point.va, kpm, kg1m, kvbm, mum, exm);
                }
            }

            var colors = utils.Colors.highchartsColors();
            var series: Array<HighchartsSeriesOptions> = [];
            //for (var c = 0; c < tubeGraph.c.length; c++) {
            var c = tubeGraph.c.length - 1;
            var imax = 0;
            var umax = 0;
            var curve = tubeGraph.c[c];
            var points: Array<any> = [];
            for (var p = 0; p < curve.p.length; p++) {
                var point = curve.p[p];
                var i = point.ik * 1000;
                points.push([point.va, i]);
                if (umax < point.va) {
                    umax = point.va;
                }
                if (imax < i) {
                    imax = i;
                }
            }

            var name = curve.vg1 + 'V (eq)';
            self.chart.highcharts.addSeries({
                type: 'spline',
                lineWidth: 2,
                name: name,
                vg1: curve.vg1,
                data: points,
                umax: umax,
                imax: imax,
                color: colors[c % colors.length],
                index: c,
                zIndex: c,
                id: String('t' + curve.vg1),
            } as HighchartsIndividualSeriesOptions, false)
            //}

            self.chart.highcharts.redraw();
        }

        private showDownloadMenu = function(owner: JQuery) {
            var self = <FileViewer>this;
            requirejs(['dropdown'], function(ns: any) {
                var dropDown = new ns.DropDown();

                if (!self.dropDownMenu) {
                    var href: string;
                    var filename: string;

                    self.dropDownMenu = self.element.find('#downloadmenu').find('#dmenu').clone();
                    self.tubeGraph = self.chart.exportToJson();

                    // Generate json link                    
                    href = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(self.tubeGraph));
                    filename = self.tubeGraph.name ? self.tubeGraph.name.replace(/[\/:*?"<>|]/gi, '_') : 'json';
                    self.dropDownMenu.find('#download-json').attr('href', href).attr('download', filename + '.json');

                    filename = self.tubeGraph.name ? self.tubeGraph.name.replace(/[\/:*?"<>|]/gi, '_') + 'raw' : 'jsonraw';
                    self.dropDownMenu.find('#download-json-raw').attr('href', href).attr('download', filename + '.json');
                }

                dropDown.show({
                    template: self.dropDownMenu,
                    backdrop: true,
                    backdropCancel: true,
                    backdropId: 'file-viewer-dmenu-backdrop',
                    modalId: 'dmenu-body',
                    position: {
                        of: owner,
                        within: self.window,
                        my: 'right top',
                        at: 'right bottom'
                    },
                    cancelOnEsc: true
                }).modalBody.on('mousedown', function(e: JQueryEventObject) {
                    var $dataClick = $(e.target).closest('[data-click]');
                    var func = $dataClick.attr('data-click');
                    if (func) {
                        switch (func) {
                            case 'xls':
                                requirejs(['export'], function() {
                                    var chart = self.chart.highcharts;
                                    chart.options.exporting = chart.options.exporting || {};
                                    chart.options.exporting.filename = self.tubeGraph.name ? self.tubeGraph.name.replace(/[\/:*?"<>|]/gi, '_') : 'excel'
                                    chart.downloadXLS();
                                });
                                dropDown.close();
                                return false;

                            case 'csv':
                                requirejs(['export'], function() {
                                    var chart = self.chart.highcharts;
                                    chart.options.exporting = chart.options.exporting || {};
                                    chart.options.exporting.filename = self.tubeGraph.name ? self.tubeGraph.name.replace(/[\/:*?"<>|]/gi, '_') : 'csv'
                                    chart.downloadCSV();
                                });
                                dropDown.close();
                                return false;

                            case 'raw':
                                $('body').css('cursor', 'wait');
                                var tubeGraphRaw = self.chart.exportToJson(300) as TubeGraph;
                                href = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(tubeGraphRaw));
                                var alink = self.dropDownMenu.find('#download-json-raw').attr('href', href);
                                setTimeout(function() {
                                    alink.click();
                                }, 100);
                                $('body').css('cursor', 'default');
                                dropDown.close();
                                return false;
                        }
                    }

                    dropDown.close();
                });
            })
        }

        protected oncreated = function() {
            var self = <FileViewer>this;
            var e = jQuery.Event("created");
            self._trigger('created', e);
        }

        public refresh() {
            var self = <FileViewer>this;
            var $image = self.window.find('#image').hide();
            var $chart = self.window.find('#chart').hide();
            self.dropDownMenu = null;

            if (self.options.file) {
                $image.children('img').attr('src', self.options.file);
                $image.show();
                self.download.hide();
                self.usage.hide();
                self.create.show().attr('href', '/creator?id=' + self.options.tubeId + '&docid=' + self.options.dataId + '&rl=' + encodeURIComponent(location.search));
            }

            var nextForChart = function() {
                self.download.show();
                self.usage.show().attr('href', '/usages?usageid=new&id=' + self.options.tubeId + '&docid=' + self.options.dataId + '&rl=' + encodeURIComponent(location.search));
            }

            if (self.options.tubeGraph) {
                $chart.show();

                var options: AnodeTransfertChartOptions = {
                    element: $chart,
                    tubeGraph: self.options.tubeGraph,
                    title: self.options.graphTitle,
                    editable: false,
                    created: function() {
                        setTimeout(function() {
                            nextForChart();
                        }, 100);
                    }
                }
                if (!self.chart) {
                    requirejs(['anodeChartTransfert'], function() {
                        self.chart = new tct.AnodeTransfertChart(options);
                    })
                } else {
                    self.chart.setOptions(options);
                    nextForChart();
                }
            }
        }

        public setOptions(options: FileViewerOptions) {
            var self = <FileViewer>this;
            self.options = options;
            self.refresh();
            setTimeout(function() {
                self.show();
            }, 1);
            return self;
        }

        public show() {
            var self = <FileViewer>this;
            if (self.backdrop) {
                self.backdrop.show();
            }
            self.window.css('opacity', 0);
            self.window.show();
            self.window.animate({ opacity: 1 }, 500);
            return self;
        }

        public hide() {
            var self = <FileViewer>this;
            self.window.animate({ opacity: 0 }, 200, function() {
                self.backdrop.hide();
                self.window.hide();
            });
            return self;
        }
    }
}

$.widget("tct.fileViewer", new tct.FileViewer());

interface JQuery {
    fileViewer(): JQuery;
    fileViewer(method: string): tct.FileViewer;
    fileViewer(method: 'show'): tct.FileViewer;
    fileViewer(method: 'hide'): tct.FileViewer;
    fileViewer(options: tct.FileViewerOptions): JQuery;
    fileViewer(method: string, options: tct.FileViewerOptions): tct.FileViewer;
    fileViewer(method: 'setOptions', options: tct.FileViewerOptions): tct.FileViewer;
}
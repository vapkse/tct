'use strict';
var tct;
(function (tct) {
    var SearchCriterions = (function () {
        function SearchCriterions() {
            this._create = function () {
                var self = this;
                $.get('/resource?r=widgets%2Fsearch-criteria.html').done(function (crhtml) {
                    self.criteriaTemplate = $('<div style="display:none;">' + crhtml + '</div>').appendTo(self.element).children();
                    self.refresh();
                    self.onready();
                });
            };
            this.setExpressions = function (expr) {
                var self = this;
                self.options.expressions = expr;
                self.refresh();
                return self;
            };
            this.refresh = function () {
                var self = this;
                self.element.children('[field-criteria]').remove();
                if (self.options.expressions) {
                    var searchExprs = self.options.expressions.queryExpressions;
                    if (searchExprs && searchExprs.length) {
                        searchExprs.map(function (searchExpr) { return self.addNew(searchExpr); });
                        return;
                    }
                }
                self.addNew();
            };
            this.addNew = function (expression) {
                var self = this;
                var criteria;
                criteria = $('<div field-criteria></div>').appendTo(self.element).searchCriteria({
                    modelFields: self.options.modelFields,
                    user: self.options.user,
                    expression: expression,
                    template: self.criteriaTemplate.clone(),
                    lookups: {
                        base: self.options.bases,
                        pinout: self.options.pinouts,
                        type: self.options.types
                    }
                }).bind('searchcriteriadelete', function (e) {
                    setTimeout(function () {
                        $(e.target).closest('[field-criteria]').remove();
                        self.onchange();
                    }, 0);
                }).bind('searchcriteriachange', function (e) {
                    self.onchange();
                }).bind('searchcriteriasearch', function (e) {
                    self.onsearch();
                });
                return self;
            };
            this.onready = function () {
                var self = this;
                var e = jQuery.Event("ready");
                self._trigger('ready', e);
            };
            this.onchange = function () {
                var self = this;
                var e = jQuery.Event("change");
                var expressions = [];
                var children = self.element.find('[field-criteria]');
                for (var i = 0; i < children.length; i++) {
                    expressions.push($(children[i]).searchCriteria('expression'));
                }
                e.expressions = expressions;
                self._trigger('change', e);
            };
            this.onsearch = function () {
                var self = this;
                var e = jQuery.Event('search');
                self._trigger('search', e);
            };
        }
        return SearchCriterions;
    })();
    tct.SearchCriterions = SearchCriterions;
})(tct || (tct = {}));
$.widget("tct.searchCriterions", new tct.SearchCriterions());

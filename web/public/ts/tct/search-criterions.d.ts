declare module tct {
    interface SearchCriterionsOptions {
        modelFields: {
            [name: string]: DataProviding.Field;
        };
        expressions: tct.TubeSearch;
        bases: Array<TubeBase>;
        pinouts: Array<TubePinout>;
        types: Array<TubeType>;
        user: user.User;
    }
    interface searchCriterionsEvent extends JQueryEventObject {
        expressions: Array<tct.QueryExpression>;
    }
    class SearchCriterions {
        private criteriaTemplate;
        options: SearchCriterionsOptions;
        protected element: JQuery;
        private _trigger;
        protected _create: () => void;
        setExpressions: (expr: TubeSearch) => SearchCriterions;
        refresh: () => void;
        addNew: (expression?: QueryExpression) => SearchCriterions;
        protected onready: () => void;
        protected onchange: () => void;
        protected onsearch: () => void;
    }
}
interface JQuery {
    searchCriterions(): JQuery;
    searchCriterions(options: tct.SearchCriterionsOptions): JQuery;
    searchCriterions(method: string): tct.SearchCriterions;
    searchCriterions(method: 'addNew'): tct.SearchCriterions;
    searchCriterions(method: string, options: tct.TubeSearch): tct.SearchCriterions;
    searchCriterions(method: 'setExpressions', options: tct.TubeSearch): tct.SearchCriterions;
}

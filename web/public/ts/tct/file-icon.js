'use strict';
var tct;
(function (tct) {
    var FileIcon = (function () {
        function FileIcon() {
        }
        FileIcon.prototype._create = function () {
            var self = this;
            self.refresh();
        };
        FileIcon.prototype.setOptions = function (options) {
            var self = this;
            self.options = options;
            self.refresh();
            return self;
        };
        FileIcon.prototype.refresh = function () {
            var self = this;
            var reext = /^.+\.([^.]+)$/.exec(self.options.filename);
            var ext = reext && reext.length > 1 ? reext[1].toLowerCase() : '';
            self.element;
            switch (ext) {
                case 'pdf':
                    self.element.attr('class', 'fileicon mif-file-pdf').attr('tooltip', 'pdf');
                    break;
                case 'tdf':
                case 'tds':
                    self.element.attr('class', 'fileicon icon-curves').attr('tooltip', 'graph');
                    break;
                case 'png':
                case 'jpg':
                case 'jpeg':
                case 'gif':
                case 'bmp':
                case 'tif':
                case 'tiff':
                    self.element.attr('class', 'fileicon mif-file-image').attr('tooltip', 'img');
                    break;
                default:
                    self.element.attr('class', 'fileicon mif-file-empty').attr('tooltip', 'doc');
                    break;
            }
            return self;
        };
        ;
        return FileIcon;
    })();
    tct.FileIcon = FileIcon;
})(tct || (tct = {}));
$.widget("tct.fileIcon", new tct.FileIcon());

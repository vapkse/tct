declare module tct {
    interface DocViewerOptions {
        tubeData?: TubeData;
        document: TubeDoc;
        isNew: boolean;
        success?: () => void;
        error?: (error: string) => void;
    }
    class DocViewer {
        private fileViewer;
        constructor();
        view(opts: DocViewerOptions): DocViewer;
    }
}

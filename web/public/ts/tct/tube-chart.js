'use strict';
var tct;
(function (tct) {
    var TubeChart = (function () {
        function TubeChart() {
            this.sizingTimer = 0;
            this.onoptionschanged = function () {
                var self = this;
                if (self.options.optionschanged) {
                    self.options.optionschanged();
                }
            };
            this.setHeight = function () {
                var self = this;
                if (!self.chart) {
                    return;
                }
                var width = self.options.element.width();
                var height;
                if (self.options.height) {
                    if (self.options.height instanceof Function) {
                        var fn = self.options.height;
                        height = fn(self.$chart);
                    }
                    else {
                        height = self.options.height;
                    }
                }
                else {
                    height = self.options.element.height();
                }
                self.chart.options.chart.width = width;
                self.chart.options.chart.height = height;
                self.chart.reflow();
            };
            this.onresize = function (context) {
                var self = context;
                return function () {
                    if (self.sizingTimer) {
                        clearTimeout(self.sizingTimer);
                        self.sizingTimer = 0;
                    }
                    self.sizingTimer = setTimeout(function () {
                        self.setHeight();
                        self.sizingTimer = setTimeout(function () {
                            self.sizingTimer = 0;
                            self.loadChartTitles();
                        }, 100);
                    }, 100);
                };
            };
            this.setZoom = function () {
                var self = this;
                var hasZoom = false;
                var zoom = (self.options.usage && self.options.usage.zoom.value);
                if (!zoom) {
                    return;
                }
                if (zoom.minX || zoom.maxX) {
                    self.chart.xAxis[0].setExtremes(zoom.minX, zoom.maxX, false);
                    hasZoom = true;
                }
                if (zoom.minY || zoom.maxY) {
                    self.chart.yAxis[0].setExtremes(zoom.minY, zoom.maxY, false);
                    hasZoom = true;
                }
                if (hasZoom) {
                    self.chart.showResetZoom();
                }
            };
            this.loadChartTitles = function () {
                var self = this;
                self.$chart.find('.highcharts-title').text(self.options.title);
                var chartingLabelIds = ['c-ac', 'c-av', 'c-tt'];
                for (var l = 0; l < chartingLabelIds.length; l++) {
                    var id = chartingLabelIds[l];
                    var selector = 'text:contains("' + id + '")';
                    var tspan = self.translationMapping[id];
                    if (!tspan) {
                        tspan = self.$chart.find(selector);
                        if (tspan.length) {
                            self.translationMapping[id] = tspan;
                        }
                    }
                    if (tspan && tspan.length) {
                        tspan.text(self.translations.filter('#' + id).text());
                    }
                }
                return self;
            };
            this.setOptions = function (options) {
                var self = this;
                self.options = options;
                self.refresh();
            };
            this.destroy = function () {
                var self = this;
                if (self.chart) {
                    self.chart.destroy();
                    self.chart = undefined;
                    window.removeEventListener('resize', self.onresize(self));
                    self.options.element.empty();
                }
            };
        }
        TubeChart.prototype.contructor = function (options) {
            var self = this;
            self.options = options;
            $.get('/resource?r=widgets%2Ftube-chart.html').done(function (html) {
                var element = self.options.element;
                element.append(html).addClass('modern-highcharts');
                self.translations = element.find('translations lang');
                self.translationMapping = {};
                self.$chart = element.find('#chart-cont');
                window.addEventListener('resize', self.onresize(self));
                self.setHeight();
                self.refresh();
                self.oncreated();
            });
        };
        TubeChart.prototype.oncreated = function () {
            var self = this;
            if (self.options.created) {
                self.options.created(self);
            }
        };
        Object.defineProperty(TubeChart.prototype, "highcharts", {
            get: function () {
                var self = this;
                return self.chart;
            },
            enumerable: true,
            configurable: true
        });
        TubeChart.prototype.redraw = function (animation) {
            var self = this;
            if (self.chart) {
                self.chart.redraw(animation);
            }
        };
        TubeChart.prototype.refresh = function () {
            var self = this;
        };
        return TubeChart;
    })();
    tct.TubeChart = TubeChart;
})(tct || (tct = {}));

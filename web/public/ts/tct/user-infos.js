'use strict';
var tct;
(function (tct) {
    var UserInfos = (function () {
        function UserInfos() {
            this._create = function () {
                var self = this;
                self.element.css('opacity', 0);
                $.get('/resource?r=widgets%2Fuser-infos.html').done(function (html) {
                    self.element.append(html);
                    self.$login = self.element.find('.login').on('click', function (e) {
                        self.onlogin();
                    });
                    self.refresh();
                    self.oncreated();
                    self.element.animate({ opacity: 1 }, 500);
                });
            };
            this.oncreated = function () {
                var self = this;
                var e = jQuery.Event("created");
                self._trigger('created', e);
            };
            this.setOptions = function (options) {
                var self = this;
                self.options = options;
                return self.refresh();
            };
            this.refresh = function () {
                var self = this;
                var options = this.options;
                var userInfos = self.element.find('.user-infos');
                if (options.user) {
                    self.$login.hide();
                    userInfos.show().find('.user-name').html(options.user.displayName);
                    if (options.user.photo) {
                        userInfos.find('.avatar').removeClass('noavatar').find('.image').attr('style', 'background-image: url("' + options.user.photo + '");');
                    }
                    else {
                        userInfos.find('.avatar').addClass('noavatar');
                    }
                }
                else {
                    self.$login.show();
                    userInfos.hide();
                }
                return self.element;
            };
            this.onlogin = function () {
                var self = this;
                var e = jQuery.Event("login");
                self._trigger('login', e);
            };
        }
        return UserInfos;
    })();
    tct.UserInfos = UserInfos;
})(tct || (tct = {}));
$.widget("tct.userInfos", new tct.UserInfos());

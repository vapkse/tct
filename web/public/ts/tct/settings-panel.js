'use strict';
var tct;
(function (tct) {
    var SettingsPanel = (function () {
        function SettingsPanel() {
            this.oncreated = function () {
            };
        }
        SettingsPanel.prototype._create = function () {
            var self = this;
            var options = this.options;
            $.get('/resource?r=widgets%2Fsettings-panel.html').done(function (html) {
                self.element.append(html).addClass('charm right-side padding20 bg-dark fg-lighter');
                self.backdrop = $('<div class="backdrop bg-transparent"></div>').appendTo('body').on('click', function () {
                    self.hide();
                });
                $('#color-settings-panel').colorSettings(options.colorSettings).bind('colorsettingschange', function (e) {
                    var data = new tct.Settings();
                    data.colorSettings.scheme = e.scheme;
                    options.colorSettings.scheme = e.scheme;
                    data.colorSettings.theme = e.theme;
                    options.colorSettings.theme = e.theme;
                    self._trigger('change', e, data);
                }).bind('colorsettingsenter', function (e) {
                    var data = new tct.Settings();
                    data.colorSettings.scheme = e.scheme;
                    data.colorSettings.theme = e.theme;
                    self._trigger('change', e, data);
                }).bind('colorsettingsleave', function (e) {
                    var data = new tct.Settings();
                    data.colorSettings.scheme = options.colorSettings.scheme;
                    data.colorSettings.theme = options.colorSettings.theme;
                    self._trigger('change', e, data);
                });
                self.element.find('button[settings-panel]').on('click', function () {
                    self.hide();
                });
                self.oncreated();
            });
        };
        ;
        SettingsPanel.prototype.toogle = function () {
            var self = this;
            if (self.element.data('hidden') === undefined) {
                self.element.data('hidden', true);
            }
            if (!self.element.data('hidden')) {
                self.hide();
            }
            else {
                self.show();
            }
        };
        SettingsPanel.prototype.show = function () {
            var self = this;
            self.element.animate({
                right: 0
            });
            if (self.backdrop) {
                self.backdrop.show();
            }
            self.element.data('hidden', false);
        };
        SettingsPanel.prototype.hide = function () {
            var self = this;
            self.element.animate({
                right: '-29rem'
            });
            self.backdrop.hide();
            self.element.data('hidden', true);
        };
        return SettingsPanel;
    })();
    tct.SettingsPanel = SettingsPanel;
})(tct || (tct = {}));
$.widget("tct.settingsPanel", new tct.SettingsPanel());

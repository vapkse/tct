'use strict';
var tct;
(function (tct) {
    var PinoutTooltip = (function () {
        function PinoutTooltip() {
            this._create = function () {
                var self = this;
                self.element.addClass('pinoutTooltip inline bg-transparent fg-ligter');
                var base = self.element.closest('.tube-result').find('[data-bind="pinout"]').text();
                if (base && base !== '-') {
                    $.get('/resource?img=images%2Fpinouts%2F' + base + '.svg').done(function (svg) {
                        if (svg) {
                            self.element.html(svg).find('svg').attr('width', '100%').attr('height', '100%').find('g').attr('class', 'fl-lighter');
                            setTimeout(function () {
                                self.oncreated();
                            }, 100);
                        }
                    });
                }
                else {
                    self.element.text('-');
                }
            };
            this.oncreated = function () {
                var self = this;
                var e = jQuery.Event("created");
                self._trigger('created', e);
            };
        }
        return PinoutTooltip;
    })();
    tct.PinoutTooltip = PinoutTooltip;
})(tct || (tct = {}));
$.widget("tct.pinoutTooltip", new tct.PinoutTooltip());

'use strict';

module tct {
    export interface TubeChartOptions {
        element: JQuery,
        title?: string,
        height?: string | number | Function,
        usage?: TubeUsage.TubeUsage,
        created?: (chart: TubeChart) => void,
        optionschanged?: Function
    }

    export class TubeChart {
        protected options: TubeChartOptions;
        protected chart: HighchartsChartObject;
        protected $chart: JQuery;
        protected translations: JQuery;
        protected translationMapping: { [key: string]: JQuery };
        private sizingTimer = 0;

        contructor(options: TubeChartOptions) {
            var self = this as TubeChart;
            self.options = options;

            // Resource need to be renderized, so the call must be like this
            $.get('/resource?r=widgets%2Ftube-chart.html').done(function(html) {
                var element = self.options.element;
                element.append(html).addClass('modern-highcharts');
                self.translations = element.find('translations lang');
                self.translationMapping = {};
                self.$chart = element.find('#chart-cont');

                window.addEventListener('resize', self.onresize(self));

                self.setHeight();
                self.refresh();
                self.oncreated();
            })
        }

        protected onoptionschanged = function() {
            var self: TubeChart = this;
            if (self.options.optionschanged) {
                self.options.optionschanged();
            }
        }

        protected oncreated() {
            var self = this as TubeChart;
            if (self.options.created) {
                self.options.created(self);
            }
        }

        protected setHeight = function() {
            var self = this as TubeChart;
            if (!self.chart) {
                return;
            }

            var width = self.options.element.width();
            var height: number;

            if (self.options.height) {
                if (self.options.height instanceof Function) {
                    var fn = <Function>self.options.height;
                    height = fn(self.$chart);
                } else {
                    height = self.options.height as number;
                }
            } else {
                height = self.options.element.height();
            }

            self.chart.options.chart.width = width;
            self.chart.options.chart.height = height;
            self.chart.reflow();
        }

        protected onresize = function(context: TubeChart) {
            var self = context;

            return function() {
                if (self.sizingTimer) {
                    clearTimeout(self.sizingTimer);
                    self.sizingTimer = 0;
                }

                self.sizingTimer = setTimeout(function() {
                    self.setHeight();
                    self.sizingTimer = setTimeout(function() {
                        self.sizingTimer = 0;
                        self.loadChartTitles();
                    }, 100)
                }, 100)
            }
        }

        protected setZoom = function() {
            var self = this as TubeChart;
            var hasZoom = false;
            var zoom = (self.options.usage && self.options.usage.zoom.value) as ZoomData;
            if (!zoom) {
                return;
            }

            if (zoom.minX || zoom.maxX) {
                self.chart.xAxis[0].setExtremes(zoom.minX, zoom.maxX, false);
                hasZoom = true;
            }
            if (zoom.minY || zoom.maxY) {
                self.chart.yAxis[0].setExtremes(zoom.minY, zoom.maxY, false);
                hasZoom = true;
            }

            if (hasZoom) {
                self.chart.showResetZoom();
            }
        }

        public loadChartTitles = function() {
            var self = this as TubeChart;

            self.$chart.find('.highcharts-title').text(self.options.title);

            var chartingLabelIds = ['c-ac', 'c-av', 'c-tt'];
            for (var l = 0; l < chartingLabelIds.length; l++) {
                var id = chartingLabelIds[l];
                var selector = 'text:contains("' + id + '")';
                var tspan = self.translationMapping[id]
                if (!tspan) {
                    tspan = self.$chart.find(selector)
                    if (tspan.length) {
                        self.translationMapping[id] = tspan;
                    }
                }

                if (tspan && tspan.length) {
                    tspan.text(self.translations.filter('#' + id).text());
                }
            }

            return self;
        }

        public setOptions = function(options: TubeChartOptions) {
            var self = this as TubeChart;

            self.options = options;
            self.refresh();
        }

        public get highcharts() {
            var self = this as TubeChart;
            return self.chart;
        }

        public destroy = function() {
            var self = this as TubeChart;
            if (self.chart) {
                self.chart.destroy();
                self.chart = undefined;
                window.removeEventListener('resize', self.onresize(self));
                self.options.element.empty();
            }
        }

        public redraw(animation?: boolean) {
            var self = this as TubeChart;
            if (self.chart) {
                self.chart.redraw(animation);
            }
        }

        public refresh() {
            var self = this as TubeChart;

        }     
    }
}

'use strict';

module tct {
    export interface UserInfosOptions {
        user: user.User; 
    }

    export class UserInfos {
        public options:UserInfosOptions;
        protected element: JQuery;
        protected $login: JQuery;
        protected $logout: JQuery;
        private _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected _create = function() {
            var self = <UserInfos>this;
            self.element.css('opacity', 0);
            
            // Resource need to be renderized, so the call must be like this
            $.get('/resource?r=widgets%2Fuser-infos.html').done(function(html) {                
                self.element.append(html);
                self.$login = self.element.find('.login').on('click', function(e: JQueryEventObject) {
                    self.onlogin();                    
                });
                self.refresh();
                self.oncreated();
                self.element.animate({ opacity: 1 }, 500);
            })
        }
        
        protected oncreated = function() {
            var self = <UserInfos>this;
            var e = jQuery.Event("created");
            self._trigger('created', e);   
        }
        
        public setOptions = function(options: UserInfosOptions) {
            var self = <UserInfos>this;
            self.options = options;
            return self.refresh();
        }
        
        public refresh = function() {
            var self = <UserInfos>this;
            var options = <UserInfosOptions>this.options;
            var userInfos = self.element.find('.user-infos');
            if (options.user) {
                self.$login.hide();
                userInfos.show().find('.user-name').html(options.user.displayName);
                if (options.user.photo){
                    userInfos.find('.avatar').removeClass('noavatar').find('.image').attr('style', 'background-image: url("' + options.user.photo + '");');
                } else {
                    userInfos.find('.avatar').addClass('noavatar');
                }
            } else {
                self.$login.show();
                userInfos.hide();
            }

            return self.element;
        }
        
        protected onlogin = function() {
            var self = <UserInfos>this;
            var e = jQuery.Event("login");
            self._trigger('login', e);            
        }
    }
}

$.widget("tct.userInfos", new tct.UserInfos());

interface JQuery {
    userInfos(): JQuery;
    userInfos(method: string): JQuery;
    userInfos(method: string, options: tct.UserInfosOptions): JQuery;
    userInfos(method: 'setOptions', options: tct.UserInfosOptions): JQuery;
    userInfos(method: 'refresh'): JQuery;
    userInfos(options: tct.UserInfosOptions): JQuery;
}
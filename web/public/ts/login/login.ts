'use strict';

module tct.login {    
    // Do not use this is a google analytics for Metro UI CSS         
    if (window.location.hostname !== '127.0.0.1') {
        /*jshint -W030 */
        (function(i: any,s: any,o: any,g: any,r: any,a: any,m: any){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments);},i[r].l=new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m);
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga', '', '');
        
        /* global ga */
        ga('create', 'UA-58849249-3', 'auto');
        /* global ga */
        ga('send', 'pageview');
    }                

    (function($: JQueryStatic) {
        $.StartScreen = function(){            
            var body = $('body');
            
            var bodyAttr = location.pathname.substr(1);
            var isLogin = location.pathname === '/login';      
            var isPwdReset = location.pathname === '/pwdreset'; 
            var isPwdChange = isPwdReset && location.query()['id']; 
            if (isPwdChange) {
                isPwdReset = false;
                bodyAttr = "pwdchange";
            }     
                 
            // Set the type of form to render as attribute on the body
            body.addClass(bodyAttr);

            var loginForm = $('#light-theme');
            var form = loginForm.find('form');
            setTimeout(function() {
                loginForm.addClass('loaded');
                setTimeout(function() {
                    $('#trans-area').animate({opacity: 1}, 500);
                }, 500);
            }, 10);
                                    
            var translatorControl: modern.TranslatorControl = $('#translator').translatorControl().data('modern-translatorControl');
            var messagerControl: modern.Messager = $('#messager').messager().data('modern-messager');
            var tooltipManager: modern.TooltipManager = loginForm.tooltipmanager().data('modern-tooltipmanager');
            
            loginForm.find('.input-control').textinput();
                                    
            var ajaxRequest: JQueryXHR;
            var formActions = form.find('.form-actions');
            var preloader = formActions.find('.preloader-status');
            var submit = formActions.find('[type="submit"]');
            formActions.find('[cancel]').on('click',function(){
                if (ajaxRequest && ajaxRequest.state() === 'pending'){
                    ajaxRequest.abort('canceled');
                } else if (isPwdReset) {
                    window.location.href = '/login';
                } else {			
                    window.location.href = '/';			
                }
            })
            
            formActions.find('[forgot]').on('click',function(){
                var location: Array<string> = [];
                location.push('/pwdreset');
                var login = form.find('[name="user_login"]');
                if (login.val()){
                    location.push('?user=');
                    location.push(login.val());
                }
                window.location.href = location.join('');			
            })
                
            var waitingStatus = function(status: boolean) {
                if (isLogin && status) {
                    preloader.show().children('.circle').addClass('bg-theme');
                    form.addClass('loading');
                    submit.attr('disabled', 'disabled');					
                } else {
                    preloader.hide();
                    form.removeClass('loading');
                    submit.removeAttr('disabled');
                }                
                if (!isLogin && status){
                    $.waitPanel.show();      
                } else {
                    $.waitPanel.hide();
                }
            }	
            
            form.submit(function() {
                waitingStatus(true);
                require(['user', 'loginValidator'], function(user_ns: any) {
                    var validatorControl: tct.login.FormValidator = $('#validator-panel').formvalidator({
                        messagerControl: messagerControl
                    }).data('tct-formvalidator');
                    validatorControl.validateAll(user_ns, function(verrors?: Array<validation.Message>){
                        if (verrors && verrors.length){
                            waitingStatus(false);                                                        
                        } else {        
                            ajaxRequest = $.ajax({
                                url: location.pathname + location.search,
                                type: 'post',
                                timeout: 60000,
                                data : form.serialize(),
                                success: function(e){
                                    if (e.messages){
                                        // JSON
                                        var messages = e.messages as Array<modern.MessagerMessage>;
                                        if (messages.length) {
                                            if (messages.length === 1 && messages[0].type !== 'error') {
                                                // Success info message
                                                //showBackDrop();
                                                messages[0].onclose = function() {
                                                    if (isPwdChange) {
                                                        window.location.href = '/login';
                                                    } else {
                                                        window.location.href = location.query()['rl'] || '/';
                                                    }
                                                }
                                                messagerControl.showMessages(messages);                                        
                                            } else {
                                                // Validation message
                                                validatorControl.showValidationErrors(messages);
                                                messagerControl.showMessages(messages);
                                                waitingStatus(false);          
                                            }
                                        }
                                    } else {
                                        // Probably ok 
                                        window.location.href = location.query()['rl'] || '/';
                                        waitingStatus(false);
                                    }                            
                                }, 
                                error: function(e){
                                    if (e.statusText !== 'canceled'){
                                        messagerControl.show('error', e);				
                                    }
                                    waitingStatus(false);
                                }, 			
                            });     
                        }       
                    });      
                })
                return false;
            })
                    
            body.find('[data-auth]').on('click', function(e: JQueryEventObject) {
                var provider = $(e.target).closest('[data-auth]').attr('data-auth');
                window.location.href = provider + location.search;    
            });

            waitingStatus(false);
        };
    })(jQuery);

    $(function(){
        if (location.hostname === '127.0.0.1') {
            var browserVersion = window.browserVersion();
            if (browserVersion.version > 9){        
                $('head').append('<script type="text/javascript" src="http://127.0.0.1:38080/livereload.js"><\/script>');
            }
        }        

        $.StartScreen();
    });
}
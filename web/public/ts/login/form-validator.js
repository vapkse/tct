'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var tct;
(function (tct) {
    var login;
    (function (login) {
        var FormValidator = (function (_super) {
            __extends(FormValidator, _super);
            function FormValidator() {
                _super.apply(this, arguments);
                this.options = {};
                this.oncreated = function () {
                    this._super();
                    var self = this;
                    self.$ = {
                        body: $('body'),
                        login: self.container.find('[name="user_login"]').on('focus', function () {
                            self.hideValidationError('#user_login');
                            self.options.messagerControl.hideMessage('#user_login');
                        }),
                        password: self.container.find('[name="user_password"]').on('focus', function () {
                            self.hideValidationError('#user_password');
                            self.options.messagerControl.hideMessage('#user_password');
                        }),
                        password2: self.container.find('#pwd2 input').on('focus', function () {
                            self.hideValidationError('#pwd2');
                            self.options.messagerControl.hideMessage('#pwd2');
                        }),
                        email: self.container.find('[name="user_email"]').on('focus', function () {
                            self.hideValidationError('#user_email');
                            self.options.messagerControl.hideMessage('#user_email');
                        }),
                        username: self.container.find('[name="user_username"]').on('focus', function () {
                            self.hideValidationError('#user_username');
                            self.options.messagerControl.hideMessage('#user_username');
                        })
                    };
                };
            }
            FormValidator.prototype.validateAll = function (user_ns, cb) {
                var self = this;
                var validator = new user_ns.Validator();
                var done = function (verrors) {
                    if (verrors && verrors.length) {
                        $.translator.translateMessages(verrors, function (translated) {
                            self.showValidationErrors(verrors);
                            self.options.messagerControl.showMessages(verrors, true);
                            cb(verrors);
                        });
                    }
                    else {
                        cb();
                    }
                };
                if (self.$.body.is('.pwdchange')) {
                    validator.validatePasswordChange(self.$.password.val(), self.$.password2.val(), function (verrors) {
                        done(verrors);
                    });
                }
                else if (self.$.body.is('.pwdreset')) {
                    validator.validatePasswordReset(self.$.email.val(), function (verrors) {
                        done(verrors);
                    });
                }
                else if (self.$.body.is('.login')) {
                    validator.validateLogin(self.$.login.val(), self.$.password.val(), function (verrors) {
                        done(verrors);
                    });
                }
                else {
                    var userEmail = {
                        userName: self.$.username.val(),
                        email: self.$.email.val(),
                    };
                    validator.validateSignup(userEmail, self.$.password.val(), self.$.password2.val(), function (verrors) {
                        done(verrors);
                    });
                }
                return self;
            };
            return FormValidator;
        })(modern.FormValidator);
        login.FormValidator = FormValidator;
    })(login = tct.login || (tct.login = {}));
})(tct || (tct = {}));
$.widget("tct.formvalidator", $.modern.formvalidator, new tct.login.FormValidator());

'use strict';

module tct.login {
    export interface FormValidatorOptions {
        messagerControl?: modern.Messager;
    }
    
    export class FormValidator extends modern.FormValidator {
        private $: {
            login: JQuery;
            password: JQuery;
            password2: JQuery;
            email: JQuery;
            username: JQuery;
            body: JQuery;
        }
        protected options: FormValidatorOptions = {};
        
        protected oncreated = function() {
            this._super();
            var self = <FormValidator>this;
            self.$ = {
                body: $('body'),
                login: self.container.find('[name="user_login"]').on('focus', function() { 
                    self.hideValidationError('#user_login');
                    self.options.messagerControl.hideMessage('#user_login');
                }),
                password: self.container.find('[name="user_password"]').on('focus', function() {                     
                    self.hideValidationError('#user_password') 
                    self.options.messagerControl.hideMessage('#user_password');
                }),
                password2: self.container.find('#pwd2 input').on('focus', function() { 
                    self.hideValidationError('#pwd2') 
                    self.options.messagerControl.hideMessage('#pwd2');
                }),
                email: self.container.find('[name="user_email"]').on('focus', function() { 
                    self.hideValidationError('#user_email') 
                    self.options.messagerControl.hideMessage('#user_email');
                }),
                username: self.container.find('[name="user_username"]').on('focus', function() { 
                    self.hideValidationError('#user_username') 
                    self.options.messagerControl.hideMessage('#user_username');
                })
            }
        };
    
        public validateAll(user_ns: any, cb: (verrors?: Array<validation.Message>) => void) {
            var self = <FormValidator>this;
            var validator = new user_ns.Validator() as user.Validator;
            
            var done = function(verrors?: Array<validation.Message>) {
                if (verrors && verrors.length) {
                    $.translator.translateMessages(verrors, function(translated) {
                        self.showValidationErrors(verrors);
                        self.options.messagerControl.showMessages(verrors, true);
                        cb(verrors);
                    })
                } else {
                    cb();
                }
            }

            if (self.$.body.is('.pwdchange')) {
                validator.validatePasswordChange(self.$.password.val(), self.$.password2.val(), function(verrors: Array<validation.Message>) {
                    done(verrors);       
                })            
            } else if (self.$.body.is('.pwdreset')) {
                validator.validatePasswordReset(self.$.email.val(), function(verrors: Array<validation.Message>) {
                    done(verrors);       
                })
            } else if (self.$.body.is('.login')) {
                validator.validateLogin(self.$.login.val(), self.$.password.val(), function(verrors: Array<validation.Message>) {
                    done(verrors);       
                })
            } else {
                var userEmail: user.UserEmail = {
                    userName: self.$.username.val(),
                    email: self.$.email.val(),
                }
                
                validator.validateSignup(userEmail, self.$.password.val(), self.$.password2.val(), function(verrors: Array<validation.Message>) {
                    done(verrors);       
                })
            }

            return self;
        }
    }
}

$.widget("tct.formvalidator", $.modern.formvalidator, new tct.login.FormValidator());

interface JQuery {
    formvalidator(options: tct.login.FormValidatorOptions): JQuery;
}

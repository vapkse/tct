declare module tct.login {
    interface FormValidatorOptions {
        messagerControl?: modern.Messager;
    }
    class FormValidator extends modern.FormValidator {
        private $;
        protected options: FormValidatorOptions;
        protected oncreated: () => void;
        validateAll(user_ns: any, cb: (verrors?: Array<validation.Message>) => void): FormValidator;
    }
}
interface JQuery {
    formvalidator(options: tct.login.FormValidatorOptions): JQuery;
}

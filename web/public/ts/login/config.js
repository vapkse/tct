'option: strict';
requirejs.config({
    baseUrl: './',
    waitSeconds: 200,
    paths: {
        require: "bower_components/requirejs/require",
        jquery: 'bower_components/jquery/dist/jquery',
        jqueryui: 'custom_components/jquery-ui-1.11.4.custom/jquery-ui',
        metro: 'bower_components/metro/build/js/metro',
        cookie: 'bower_components/js-cookie/src/js.cookie',
        window: 'ts/utils/window',
        string: 'ts/utils/string',
        translator: 'ts/utils/translator',
        chosen: 'ts/widgets/chosen.jquery',
        chosen2: 'ts/widgets/chosen2',
        waitPanel: 'ts/widgets/wait-panel',
        queryStringParser: 'ts/utils/query-string-parser',
        translatorControl: 'ts/widgets/translator-control',
        messager: 'ts/widgets/messager',
        formValidator: 'ts/widgets/form-validator',
        loginValidator: 'ts/login/form-validator',
        settings: 'ts/tct/settings',
        textInput: 'ts/widgets/text-input',
        user: 'common/ts/user',
        tooltip: 'ts/widgets/tooltip',
        tooltipManager: 'ts/widgets/tooltip-manager',
        login: 'ts/login/login'
    },
    shim: {
        jqueryui: ['jquery'],
        metro: ['jquery'],
        waitPanel: ['jquery'],
        translator: ['jquery'],
        queryStringParser: ['jquery'],
        string: ['jquery'],
        tooltip: ['metro'],
        tooltipManager: ['tooltip', 'jqueryui', 'window'],
        formValidator: ['metro'],
        loginValidator: ['user', 'formValidator', 'translator'],
        textInput: ['metro'],
        messager: ['metro'],
        chosen: ['metro'],
        chosen2: ['chosen'],
        translatorControl: ['chosen2', 'string', 'translator'],
        login: ['jquery', 'metro', 'messager', 'window', 'settings', 'waitPanel', 'translatorControl', 'tooltipManager', 'queryStringParser', 'textInput']
    }
})(['jquery', 'login'], function (jquery) {
});

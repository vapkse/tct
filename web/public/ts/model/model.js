'use strict';
var tct;
(function (tct) {
    var model;
    (function (model) {
        require(['tube'], function (app) {
            (function ($) {
                $.StartScreen = function () {
                    var settingsPanel;
                    var docViewer;
                    var pageId = $('[translate]').attr('translate');
                    var body = $('body').attr('ismobile', window.mobilecheck().toString());
                    var translations = $('translations');
                    var chart;
                    var graphContainer = $('#model');
                    window.onbeforeunload = function () {
                        if (body.is('.unsaved')) {
                            return translations.find('[uid="msg-leave"]').text();
                        }
                    };
                    var transArea = $('#trans-area');
                    var translatorControl = transArea.find('#translator').translatorControl().data('modern-translatorControl');
                    var messagerControl = $('#messager').messager().data('modern-messager');
                    var tooltipManager = $('#container').tooltipmanager().data('modern-tooltipmanager');
                    $('button[unsaved]').on('click', function () {
                    });
                    var huser = $('#user').html();
                    var user = huser && JSON.parse(huser);
                    var userInfosPanel = body.find('#user-infos-panel').userInfos({
                        user: user
                    }).on('userinfoscreated', function () {
                        transArea.animate({ opacity: 1 }, 500);
                    });
                    $('button[home]').on('click', function () {
                        var searchparams = location.query()['rl'];
                        location.href = '/' + (searchparams ? searchparams : '');
                    });
                    $('button[settings-panel]').on('click', function () {
                        if (!settingsPanel) {
                            requirejs(['settingsPanel'], function (settingsPanel) {
                                settingsPanel = $('#settings-panel');
                                var settings = new tct.Settings(localStorage);
                                settingsPanel.settingsPanel(settings).bind('settingspanelchange', function (e, data) {
                                    if (data.colorSettings.scheme) {
                                        body.attr('id', 'scheme-' + data.colorSettings.scheme);
                                    }
                                    if (data.colorSettings.theme) {
                                        body.attr('theme', data.colorSettings.theme);
                                    }
                                });
                                settingsPanel.settingsPanel('toogle');
                            });
                        }
                        else {
                            settingsPanel.settingsPanel('toogle');
                        }
                    });
                    var uploader = $('form#upload').show();
                    var preloader = $('#preloader').hide();
                    var progressLabel = preloader.find('#progress');
                    var uploadDone = function () {
                        preloader.hide();
                        uploader.show();
                    };
                    $('.btn-file :file').on('change', function () {
                        var input = $(this);
                        var files = input.prop('files');
                        if (files.length) {
                            uploader.hide();
                            preloader.show();
                            progressLabel.text('');
                            var formElement = (uploader[0]);
                            var formData = new FormData(formElement);
                            $.ajax({
                                url: '/document?d=upload',
                                type: 'POST',
                                timeout: 1800000,
                                data: formData,
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function (result) {
                                    uploadDone();
                                    input.val('');
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    uploadDone();
                                    messagerControl.show('error', (jqXHR && (jqXHR.responseText || jqXHR.statusText)) || errorThrown);
                                    input.val('');
                                },
                                xhr: function () {
                                    var xhr = new XMLHttpRequest();
                                    xhr.upload.addEventListener("progress", function (evt) {
                                        if (evt.lengthComputable) {
                                            var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                                            progressLabel.text(percentComplete + '%');
                                        }
                                    }, false);
                                    xhr.addEventListener("progress", function (evt) {
                                        if (evt.lengthComputable) {
                                            var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                                            progressLabel.text(percentComplete + '%');
                                        }
                                    }, false);
                                    return xhr;
                                }
                            });
                        }
                    });
                    var registerForPush = function (user, tube) {
                        var refreshTimer;
                        var refresh = function () {
                            if (refreshTimer) {
                                clearTimeout(refreshTimer);
                            }
                            refreshTimer = setTimeout(function () {
                                refreshTimer = undefined;
                            }, 500);
                        };
                        var io = $.io.connect({
                            query: {
                                event: 'uploadResult#' + user.userName
                            }
                        });
                        io.on('uploadResult', function (result) {
                            if (result.error) {
                                messagerControl.show('error', result.error);
                            }
                            else {
                                var fileName = result.filename;
                                var fileInfo = /(^.+)\.([^.]+)$/.exec(fileName);
                                var docData = {
                                    description: fileInfo.length > 1 ? fileInfo[1] : '',
                                    filename: fileName
                                };
                                var msg = {
                                    type: 'info',
                                    name: 'msg-fileuploaded',
                                    message: 'File \\0 Uploaded',
                                    autoCloseDelay: 6,
                                    params: { 0: fileName }
                                };
                                $.translator.translateMessages([msg], function (translated) {
                                    messagerControl.showMessages(translated);
                                });
                                refresh();
                            }
                        });
                    };
                    var refreshParamsPanel = function () {
                    };
                    var loadModel = function () {
                        var refreshTimeout = window.browserVersion().browser === 'firefox' ? 250 : 1;
                        var refreshTimer;
                        requirejs(['anodeChartTransfert'], function () {
                            var options = {
                                element: $('<div class="anode-transfert-chart"></div>'),
                                editable: true,
                                height: function (chart) {
                                    var width = chart.width();
                                    return Math.max(400, width / 2);
                                },
                                optionschanged: function () {
                                    if (refreshTimer) {
                                        clearTimeout(refreshTimer);
                                    }
                                    refreshTimer = setTimeout(function () {
                                        refreshTimer = 0;
                                        refreshParamsPanel();
                                    }, refreshTimeout);
                                },
                                ready: function () {
                                    refreshParamsPanel();
                                }
                            };
                            chart = new tct.AnodeTransfertChart(options);
                            graphContainer.append(options.element);
                        });
                    };
                    var loadGraph = function () {
                        var refreshTimeout = window.browserVersion().browser === 'firefox' ? 250 : 1;
                        var refreshTimer;
                        requirejs(['ajaxDownloadFile'], function () {
                        });
                    };
                    var loadFromUrl = function () {
                        var qstring = location.query();
                        var tubeid = qstring['id'];
                        if (!tubeid) {
                            loadModel();
                            $.waitPanel.hide();
                            return;
                        }
                        var reqstring = [];
                        $.waitPanel.show();
                        var ajaxRequest = $.ajax({
                            url: '/model?' + reqstring.join('&'),
                            type: 'post',
                            dataType: 'json',
                            timeout: 60000,
                            success: function (json) {
                                if (json.error) {
                                    messagerControl.show('error', json.error);
                                }
                                else {
                                    var tube = new app.Tube(json);
                                    loadModel();
                                    registerForPush(user, tube);
                                    $.pageState.replaceOrAddState(json);
                                    $.waitPanel.hide();
                                }
                            },
                            error: function (e) {
                                if (e.statusText !== 'canceled') {
                                    messagerControl.show('error', e);
                                    loadModel();
                                }
                                $.waitPanel.hide();
                            },
                        });
                    };
                    setTimeout(function () {
                        loadFromUrl();
                    }, 1);
                    $(window).on('popstate', function (e) {
                        var event = e.originalEvent;
                        if (event && event.state && event.state.j) {
                            var tube = new app.Tube(event.state.j);
                            loadModel();
                        }
                        else {
                            loadFromUrl();
                        }
                    });
                };
            })(jQuery);
            $(function () {
                if (location.hostname === '127.0.0.1') {
                    var browserVersion = window.browserVersion();
                    if (browserVersion.version > 9) {
                        $('head').append('<script type="text/javascript" src="http://127.0.0.1:38080/livereload.js"><\/script>');
                    }
                }
                $.StartScreen();
            });
        });
    })(model = tct.model || (tct.model = {}));
})(tct || (tct = {}));

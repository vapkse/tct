declare module tct.model {
    interface FormValidatorOptions extends modern.FormValidatorOptions {
        tube: tct.Tube;
    }
    class FormValidator extends modern.FormValidator {
        private $;
        private _saveTimer;
        private originalDataset;
        options: FormValidatorOptions;
        element: JQuery;
        protected oncreated: () => void;
        tube: (tube?: Tube) => Tube;
        private hideRow;
        private showRow;
        prevalidateDoubleHeader: (field?: DataProviding.Field) => void;
        private currentTypeField;
        prevalidateType: (field?: DataProviding.Field) => void;
        prevalidateTypeUnit: (unitIndex: number) => number;
        viewPinout: (field?: DataProviding.Field) => void;
        viewBase: (field?: DataProviding.Field) => void;
        prevalidateFileControls: () => void;
        private validateDoubleDecriptions();
        prevalidateSaveButton: () => void;
        prevalidateAll: () => FormValidator;
        validateAll: (cb: (verrors?: validation.Message[]) => void) => void;
        validateField: (data: DataProviding.DataFieldEventData) => void;
    }
}
interface JQuery {
    formvalidator(options: tct.model.FormValidatorOptions): JQuery;
}

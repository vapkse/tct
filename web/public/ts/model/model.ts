'use strict';

module tct.model {
    require(['tube'], function(app: any) {
        (function($: JQueryStatic) {
            $.StartScreen = function() {
                var settingsPanel: JQuery;
                var docViewer: tct.DocViewer;
                var pageId = $('[translate]').attr('translate');
                var body = $('body').attr('ismobile', window.mobilecheck().toString());
                var translations = $('translations');
                var chart: AnodeTransfertChart;
                var graphContainer = $('#model');

                window.onbeforeunload = function() {
                    if (body.is('.unsaved')) {
                        return translations.find('[uid="msg-leave"]').text();
                    }
                }

                // Translation area visible only after the user control is ready
                var transArea = $('#trans-area');
                var translatorControl: modern.TranslatorControl = transArea.find('#translator').translatorControl().data('modern-translatorControl');
                var messagerControl: modern.Messager = $('#messager').messager().data('modern-messager');
                var tooltipManager: modern.TooltipManager = $('#container').tooltipmanager().data('modern-tooltipmanager');

                $('button[unsaved]').on('click', function() {
                });

                // User infos panel
                var huser = $('#user').html();
                var user: user.User = huser && JSON.parse(huser);
                var userInfosPanel = body.find('#user-infos-panel').userInfos({
                    user: user
                }).on('userinfoscreated', function() {
                    transArea.animate({ opacity: 1 }, 500);
                });

                $('button[home]').on('click', function() {
                    var searchparams = location.query()['rl'];
                    location.href = '/' + (searchparams ? searchparams : '');
                })

                // Bind settings panel button
                $('button[settings-panel]').on('click', function() {
                    // Instianciate settings panel if the button is pressed for the first time
                    if (!settingsPanel) {
                        requirejs(['settingsPanel'], function(settingsPanel: JQuery) {
                            settingsPanel = $('#settings-panel');
                            var settings = new tct.Settings(localStorage);
                            settingsPanel.settingsPanel(settings).bind('settingspanelchange', function(e: tct.SettingsPanelEvent, data: tct.SettingsPanelOptions) {
                                if (data.colorSettings.scheme) {
                                    body.attr('id', 'scheme-' + data.colorSettings.scheme);
                                }
                                if (data.colorSettings.theme) {
                                    body.attr('theme', data.colorSettings.theme);
                                }
                            });
                            settingsPanel.settingsPanel('toogle');
                        })
                    } else {
                        settingsPanel.settingsPanel('toogle');
                    }
                });

                var uploader = $('form#upload').show();
                var preloader = $('#preloader').hide();
                var progressLabel = preloader.find('#progress');

                var uploadDone = function() {
                    preloader.hide();
                    uploader.show();
                }

                $('.btn-file :file').on('change', function() {
                    var input = $(this);
                    var files = input.prop('files');
                    if (files.length) {
                        uploader.hide();
                        preloader.show();
                        progressLabel.text('');

                        var formElement: HTMLFormElement = <HTMLFormElement>(uploader[0]);
                        var formData = new FormData(formElement);
                        $.ajax({
                            url: '/document?d=upload',
                            type: 'POST',
                            timeout: 1800000,
                            // Form data
                            data: formData,
                            //Options to tell jQuery not to process data or worry about content-type.
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(result: UploadResult) {
                                uploadDone();
                                input.val('');
                            },
                            error: function(jqXHR: JQueryXHR, textStatus: string, errorThrown: string) {
                                uploadDone();
                                messagerControl.show('error', (jqXHR && (jqXHR.responseText || jqXHR.statusText)) || errorThrown);
                                input.val('');
                            },
                            xhr: function() {
                                var xhr = new XMLHttpRequest();
                                xhr.upload.addEventListener("progress", function(evt: ProgressEvent) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                                        progressLabel.text(percentComplete + '%');
                                    }
                                }, false);

                                xhr.addEventListener("progress", function(evt: ProgressEvent) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                                        progressLabel.text(percentComplete + '%');
                                    }
                                }, false);

                                return xhr;
                            }
                        });
                    }
                });

                var registerForPush = function(user: user.User, tube: tct.Tube) {
                    var refreshTimer: number;

                    var refresh = function() {
                        if (refreshTimer) {
                            clearTimeout(refreshTimer);
                        }
                        refreshTimer = setTimeout(function() {
                            refreshTimer = undefined;
                            // $docsContainer.datacontainer('refresh');
                            // validatorControl.prevalidateFileControls();
                        }, 500);
                    }

                    // Socket io, register to upload result event
                    var io = $.io.connect({
                        query: {
                            event: 'uploadResult#' + user.userName
                        }
                    });
                    io.on('uploadResult', function(result: UploadResultFile) {
                        if (result.error) {
                            messagerControl.show('error', result.error);
                        } else {
                            var fileName = result.filename;
                            var fileInfo = /(^.+)\.([^.]+)$/.exec(fileName);
                            var docData: TubeDoc = {
                                description: fileInfo.length > 1 ? fileInfo[1] : '',
                                filename: fileName
                            }

                            // Get right field
                            // var docsField = dataForm.dataset().getField('documents') as DataProviding.Field;
                            // var docs = docsField.value as Array<Document>;

                            // Add a new file and refresh ds
                            // var doc = dataForm.dataset().createArrayFieldSubset('document', docData) as Document
                            // doc.isNew = true;
                            // docs.push(doc);

                            var msg: modern.MessagerMessage = {
                                type: 'info',
                                name: 'msg-fileuploaded',
                                message: 'File \\0 Uploaded',
                                autoCloseDelay: 6,
                                params: { 0: fileName }
                            }

                            $.translator.translateMessages([msg], function(translated: Array<translator.Message>) {
                                messagerControl.showMessages(translated);
                            });

                            refresh();
                        }
                    });
                }

                var refreshParamsPanel = function() {

                }

                var loadModel = function() {
                    var refreshTimeout = window.browserVersion().browser === 'firefox' ? 250 : 1;
                    var refreshTimer: number;

                    requirejs(['anodeChartTransfert'], function() {
                        var options: tct.AnodeTransfertChartOptions = {
                            element: $('<div class="anode-transfert-chart"></div>'),
                            editable: true,
                            height: function(chart: JQuery) {
                                var width = chart.width();
                                return Math.max(400, width / 2);
                            },
                            optionschanged: function() {
                                if (refreshTimer) {
                                    clearTimeout(refreshTimer);
                                }
                                refreshTimer = setTimeout(function() {
                                    refreshTimer = 0;
                                    refreshParamsPanel();
                                }, refreshTimeout);
                            },
                            ready: function() {
                                refreshParamsPanel();
                            }
                        }

                        chart = new tct.AnodeTransfertChart(options)
                        graphContainer.append(options.element);

                    })
                }

                var loadGraph = function() {
                    var refreshTimeout = window.browserVersion().browser === 'firefox' ? 250 : 1;
                    var refreshTimer: number;

                    requirejs(['ajaxDownloadFile'], function() {
                        /*var options: utils.AjaxDownloadFileOptions = {
                            url: '/download?url=' + doc.filename.value,
                            dataType: 'json'
                        }

                        utils.AjaxDownloadFile.download(options).onSuccessFinish(function(status, statusText, res, headers) {
                            var json = res['json'];
                            var result: UploadResultFile = typeof json === 'string' ? JSON.parse(json) : json;
                            if (result.error) {
                                messagerControl.showMessage(result.error);
                            } else {
                                var options: tct.AnodeTransfertChartOptions = {
                                    element: $('<div class="anode-transfert-chart"></div>').attr('id', doc.getField('_id').value),
                                    tubeGraph: result.tubeGraph,
                                    title: doc.description.value,
                                    editable: true,
                                    height: function(chart: JQuery) {
                                        var width = chart.width();
                                        return Math.max(400, width / 2);
                                    },
                                    optionschanged: function() {
                                        if (refreshTimer) {
                                            clearTimeout(refreshTimer);
                                        }
                                        refreshTimer = setTimeout(function() {
                                            refreshTimer = 0;
                                            refreshParamsPanel();
                                        }, refreshTimeout);
                                    },
                                    ready: function() {
                                        refreshParamsPanel();
                                    }
                                }

                                chart = new tct.AnodeTransfertChart(options)
                                graphContainer.append(options.element);
                            }
                        }).onErrorFinish(function(status, statusText, res, headers) {
                            messagerControl.show('error', statusText);
                        });*/
                    })
                }

                var loadFromUrl = function() {
                    var qstring = location.query();
                    var tubeid = qstring['id'];
                    if (!tubeid) {
                        loadModel();
                        $.waitPanel.hide();
                        return;
                    }
                    var reqstring: Array<string> = [];

                    $.waitPanel.show();
                    var ajaxRequest = $.ajax({
                        url: '/model?' + reqstring.join('&'),
                        type: 'post',
                        dataType: 'json',
                        timeout: 60000,
                        success: function(json) {
                            if (json.error) {
                                messagerControl.show('error', json.error);
                            } else {
                                // Load tube class from the received TubeData json
                                var tube = new app.Tube(json) as tct.Tube;
                                loadModel();

                                registerForPush(user, tube);

                                $.pageState.replaceOrAddState(json);
                                $.waitPanel.hide();
                            }
                        },
                        error: function(e) {
                            if (e.statusText !== 'canceled') {
                                messagerControl.show('error', e);
                                loadModel();
                            }
                            $.waitPanel.hide();
                        },
                    });
                }

                // Ask server for datas
                setTimeout(function() {
                    loadFromUrl();
                }, 1);

                $(window).on('popstate', function(e) {
                    var event = <PopStateEvent>e.originalEvent;
                    if (event && event.state && event.state.j) {
                        var tube = new app.Tube(event.state.j) as tct.Tube;
                        loadModel();
                    } else {
                        // Reload datas from url
                        loadFromUrl();
                    }
                });
            };
        })(jQuery);

        $(function() {
            if (location.hostname === '127.0.0.1') {
                var browserVersion = window.browserVersion();
                if (browserVersion.version > 9) {
                    $('head').append('<script type="text/javascript" src="http://127.0.0.1:38080/livereload.js"><\/script>');
                }
            }

            $.StartScreen();
        });
    })
}

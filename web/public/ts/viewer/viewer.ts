'use strict';

module tct.editor {
    (function($: JQueryStatic) {
        $.StartScreen = function() {
            var settingsPanel: JQuery;
            var docViewer: tct.DocViewer;
            var pageId = $('[translate]').attr('translate');
            var body = $('body');
            var mainContent = $('#main-content');
            var usageContainer = mainContent.find('#c-usages');
            var docContainer: JQuery;
            var charts = [] as Array<AnodeTransfertChart>;

            // Translation area visible only after the user control is ready
            var transArea = $('#trans-area');
            var translatorControl: modern.TranslatorControl = transArea.find('#translator').translatorControl().on('translatorcontrolchange', function() {
                setTimeout(function() {
                    charts.forEach(c => c.loadChartTitles());
                }, 100)
            }).data('modern-translatorControl');

            var messagerControl: modern.Messager = $('#messager').messager().data('modern-messager');
            var tooltipManager: modern.TooltipManager = $('#container').tooltipmanager().data('modern-tooltipmanager');
            var translations = $('translations');

            // User infos panel
            var huser = $('#user').html();
            var user: user.User = huser && JSON.parse(huser);
            if (user && user.role === 'admin') {
                $('#admin').show();
            }
            var userInfosPanel = body.find('#user-infos-panel').userInfos({
                user: user
            }).bind('userinfoslogin', function() {
                location.href = '/login?rl=' + encodeURIComponent(location.pathname + location.search);
            }).bind('userinfoscreated', function() {
                transArea.animate({ opacity: 1 }, 500);
            });

            var valueTemplate = $('#value-template').children();
            var usageTemplate = $('#utemplate').children();
            var docTemplate = $('#dtemplate').children();
            var noinfTemplate = $('#noinf-template').children();

            $('button[home]').on('click', function() {
                var searchparams = location.query()['rl'];
                location.href = '/' + (searchparams ? searchparams : '');
            })

            // Bind settings panel button
            $('button[settings-panel]').on('click', function() {
                // Instianciate settings panel if the button is pressed for the first time
                if (!settingsPanel) {
                    requirejs(['settingsPanel'], function(settingsPanel: JQuery) {
                        settingsPanel = $('#settings-panel');
                        var settings = new tct.Settings(localStorage);
                        settingsPanel.settingsPanel(settings).bind('settingspanelchange', function(e: tct.SettingsPanelEvent, data: tct.SettingsPanelOptions) {
                            if (data.colorSettings.scheme) {
                                body.attr('id', 'scheme-' + data.colorSettings.scheme);
                            }
                            if (data.colorSettings.theme) {
                                body.attr('theme', data.colorSettings.theme);
                            }
                        });
                        settingsPanel.settingsPanel('toogle');
                    })
                } else {
                    settingsPanel.settingsPanel('toogle');
                }
            });

            var showError = function(errorMessage: string) {
                var msg: modern.MessagerMessage = {
                    type: 'error',
                    name: 'msg-fileuploaderror',
                    message: 'File Upload error: \\0',
                    params: { 0: errorMessage }
                }
                $.translator.translateMessages([msg], function(translated: Array<translator.Message>) {
                    messagerControl.showMessages(translated);
                });
                console.error(errorMessage);
            }

            var loadBase = function(name: string) {
                var socket = mainContent.find('#socket');

                var nobase = function() {
                    noinfTemplate.clone().attr('data-append', '').appendTo(mainContent.find('#c-socket'));
                    socket.hide();
                }

                $.get('/resource?img=images%2Fbases%2F' + name + '.svg').done(function(svg) {
                    if (svg) {
                        var show = function() {
                            socket.html(svg);
                            socket.find('svg').attr('width', '100%').attr('height', '100%').find('g').attr('class', 'fl-lighter');
                            socket.removeClass('empty');
                        }

                        setTimeout(function() {
                            show();
                        }, 200);
                    } else {
                        nobase();
                    }
                }).fail(function() {
                    nobase();
                })
            }

            var loadPinout = function(name: string) {
                var connection = mainContent.find('#connection');

                var nopinout = function() {
                    noinfTemplate.clone().attr('data-append', '').appendTo(mainContent.find('#c-pinout'));
                    connection.hide();
                }

                $.get('/resource?img=images%2Fpinouts%2F' + name + '.svg').done(function(svg) {
                    if (svg) {
                        var show = function() {
                            connection.html(svg);
                            connection.find('svg').attr('width', '100%').attr('height', '100%').find('g').attr('class', 'fl-lighter');
                            connection.removeClass('empty');
                        }

                        setTimeout(function() {
                            show();
                        }, 200);
                    } else {
                        nopinout();
                    }
                }).fail(function() {
                    nopinout();
                })
            }

            var loadGraph = function(tubeData: TubeData, usageData: TubeUsageData, doc: TubeDoc, infosContainer: JQuery, chartContainer: JQuery) {
                requirejs(['tubeUsage', 'anodeChartTransfert', 'ajaxDownloadFile', 'anodeChartInfos'], function(tubeUsage: any) {
                    var options: utils.AjaxDownloadFileOptions = {
                        url: '/download?url=' + doc.filename,
                        dataType: 'json'
                    }

                    // Calc pmax
                    var unitIndex = usageData.unit;
                    var pmax = 0;
                    if (unitIndex !== undefined) {
                        pmax = tubeData.units[unitIndex].pamax;
                    }

                    utils.AjaxDownloadFile.download(options).onSuccessFinish(function(status, statusText, res, headers) {
                        var json = res['json'];
                        var result: UploadResultFile = typeof json === 'string' ? JSON.parse(json) : json;
                        if (result.error) {
                            messagerControl.showMessage(result.error);
                        } else {
                            var usage = new tubeUsage.TubeUsage(usageData) as TubeUsage.TubeUsage;

                            var options: tct.AnodeTransfertChartOptions = {
                                element: $('<div class="anode-transfert-chart"></div>').attr('id', doc._id),
                                tubeGraph: result.tubeGraph,
                                usage: usage,
                                title: doc.description,
                                pmax: pmax,
                                editable: false,
                                height: function(chart: JQuery) {
                                    var width = chart.width();
                                    return Math.max(400, width / 2);
                                },
                                created: function(chart: TubeChart) {
                                    // Add chart infos
                                    setTimeout(function() {
                                        var c = chart as AnodeTransfertChart;
                                        infosContainer.anodeChartInfos({
                                            infos: c.getInfos(),
                                            usage: usage
                                        })
                                    }, 1000);
                                }
                            }

                            var chart = new tct.AnodeTransfertChart(options);
                            charts.push(chart);
                            chartContainer.append(options.element);
                        }
                    }).onErrorFinish(function(status, statusText, res, headers) {
                        messagerControl.show('error', statusText);
                    });
                })
            }

            var loadJson = function(json?: UserTubeSearchResult) {
                var tubeJson = json.tubeData;

                // Clear all added elements
                body.find('[data-append]').remove();

                // Bind owner
                var userid = user && user._id;
                if (json.userInfos && json.userInfos._id !== userid) {
                    mainContent.find('#userInfo').show().find('#user-name').text(json.userInfos.userName);
                } else {
                    mainContent.find('#userInfo').hide();
                }

                // Bind lookups 
                if (json.type) {
                    mainContent.find('#type').attr('lang', '').attr('uid', json.type.uid).text(json.type.text);
                }

                if (json.base && json.base.name) {
                    mainContent.find('#base').text(json.base.name);
                    loadBase(json.base.name);
                } else {
                    mainContent.find('#socket').hide();
                    noinfTemplate.clone().appendTo(mainContent.find('#c-socket'));
                }

                if (json.pinout && json.pinout.name) {
                    mainContent.find('#pinout').text(json.pinout.name);
                    loadPinout(json.pinout.name);
                } else {
                    mainContent.find('#connection').hide();
                    noinfTemplate.clone().appendTo(mainContent.find('#c-pinout'));
                }

                // Bind general params    
                for (var name in tubeJson) {
                    var row = mainContent.find('.' + name);
                    var value = (<any>tubeJson)[name];
                    var ctrl = mainContent.find('[data-bind="' + name + '"]');
                    if (row.length > 0 && value) {
                        row.show();
                    }

                    if (ctrl && ctrl.length) {
                        var unit = ctrl.attr('unit') || '';
                        ctrl.text(value.toString() + unit);
                    }
                }

                var genparams = mainContent.find('#genparams')
                if (!genparams.children(':visible').length) {
                    noinfTemplate.clone().attr('data-append', '').appendTo(genparams);
                }

                var bindValue = function(json: any, name: string, parent: JQuery) {
                    var value = json[name];
                    if (value !== undefined && value !== null) {
                        var titem = translations.find('#' + name);
                        var item = valueTemplate.clone().attr('data-append', '').appendTo(parent);
                        var unit = titem.attr('unit');
                        var decimal = parseInt(titem.attr('decimal'));
                        if (!isNaN(decimal)) {
                            var places = Math.pow(10, decimal);
                            value = Math.round(value * places) / places;
                        }
                        if (unit) {
                            item.find('.value').text(value + unit);
                            item.find('.unit').text(name);
                        } else {
                            item.find('.value').text(value);
                        }
                        item.find('.caption').text(titem.text()).attr('id', name);
                        return item;
                    } else {
                        return $();
                    }
                }

                // Bind units
                var unitContainer = mainContent.find('#c-max');
                var units = tubeJson.units;
                if (!units || units.length === 0) {
                    noinfTemplate.clone().attr('data-append', '').appendTo(unitContainer);
                } else if (units.length === 1) {
                    // Bind one in two columns
                    unitContainer.addClass('grid');
                    var hasInfos = false;
                    var col = 0;
                    for (var name in units[0]) {
                        if (col === 0) {
                            var row = $('<span class="row cells2" data-append></span>').appendTo(unitContainer);
                        }
                        bindValue(units[0], name, row).addClass('cell');
                        if (++col >= 2) {
                            col = 0;
                        }
                        hasInfos = true;
                    }

                    if (!hasInfos) {
                        noinfTemplate.clone().attr('data-append', '').appendTo(unitContainer);
                    }
                } else if (units.length === 2) {
                    // Bind two in one column per unit
                    var row = $('<span class="row cells2" data-append></span>').appendTo(unitContainer.addClass('grid'));
                    for (var u = 0; u < units.length; u++) {
                        var column = $('<span class="cell"></span>').appendTo(row);
                        $('<h5 class="align-center bo-lighter">Unit ' + u + '</h5>').appendTo(column);
                        var hasInfos = false;
                        for (var name in units[u]) {
                            bindValue(units[u], name, column).addClass('row');
                            hasInfos = true;
                        }

                        if (!hasInfos) {
                            noinfTemplate.clone().appendTo(column);
                        }
                    }
                } else {
                    // Bind three or four in one column per 2 units
                    var row = $('<span class="row cells2" data-append></span>').appendTo(unitContainer.addClass('grid'));
                    var column = $('<span class="cell"></span>').appendTo(row);
                    for (var u = 0; u < units.length; u++) {
                        $('<h5 class="align-center bo-lighter">Unit ' + u + '</h5>').appendTo(column);
                        var hasInfos = false;
                        for (var name in units[u]) {
                            bindValue(units[u], name, column).addClass('row');
                            hasInfos = true;
                        }

                        if (!hasInfos) {
                            noinfTemplate.clone().appendTo(column);
                        }

                        if (u === 1) {
                            column = $('<span class="cell"></span>').appendTo(row);
                        }
                    }
                }

                // Bind usages
                usageContainer.accordion({
                    active: true,
                    onFrameOpen: function(frame: JQuery) {
                        var usageid = frame.attr('data-id');
                        if (usageid) {
                            frame.removeAttr('data-id');
                            var usages = tubeJson.usages.filter(u => u._id === usageid)
                            var usage = usages.length && usages[0];
                            if (usage) {
                                var content = frame.find('#ucontent');

                                // Bind one in two columns
                                var col = 0;
                                for (var name in usage) {
                                    if (name === 'traces' || name === 'name' || name === 'note' || name === '_id' || name === 'zoom' || name === 'series') {
                                        continue;
                                    }

                                    if (name === 'unit' && tubeJson.units.length <= 1) {
                                        continue;
                                    }

                                    if (col === 0) {
                                        var row = $('<span class="row cells2"></span>').appendTo(content);
                                    }
                                    bindValue(usage, name, row).addClass('cell');
                                    if (++col >= 2) {
                                        col = 0;
                                    }
                                }

                                if (usage.note) {
                                    $('<span class="row vrow note"></span>').appendTo(content).text(usage.note);
                                }

                                if (usage.traces) {
                                    var docs = tubeJson.documents.filter(d => d._id === usage.traces);
                                    var gcont = $('<span class="row c-chart"></span>').appendTo(content);
                                    var icont = $('<span class="row i-chart"></span>').appendTo(content);
                                    loadGraph(tubeJson, usage, docs[0], icont, gcont);
                                }
                            }
                        }
                        return true;
                    }
                });

                var usages = tubeJson.usages;
                usageContainer.children('.frame').remove();
                if (usages && usages.length) {
                    usageContainer.find('.nousageex').hide();
                    usages.forEach(function(usage) {
                        var uitem = usageTemplate.clone().appendTo(usageContainer).attr('data-id', usage._id).attr('data-append', '');
                        uitem.find('.heading .caption').text(usage.name);
                    })

                    setTimeout(function() {
                        usageContainer.find('>.frame>.heading').first().click();
                    }, 1000)

                } else {
                    usageContainer.find('.nousageex').show();
                }

                // Bind documents 
                if (docContainer) {
                    docContainer.off('click');
                } else {
                    docContainer = mainContent.find('#c-docs');
                }

                var documents = tubeJson.documents;
                if (documents && documents.length) {
                    docContainer.find('.nodoc').hide();
                    documents.map(function(doc) {
                        var item = docTemplate.clone().attr('id', doc._id).attr('data-append', '').appendTo(docContainer);
                        item.find('.label').text(doc.description);
                        item.find('#icon').fileIcon({ filename: doc.filename });
                    })
                } else {
                    docContainer.find('.nodoc').show();
                }

                docContainer.on('click', function(e) {
                    var target = $(e.target).closest('[data-click]');
                    var func = target.attr('data-click');
                    switch (func) {
                        case 'download':
                            var docId = target.closest('[data-append]').attr('id');
                            var docs = tubeJson.documents.filter(d => d._id === docId);
                            var docViewerOptions: tct.DocViewerOptions = {
                                document: docs[0],
                                tubeData: tubeJson,
                                isNew: false,
                                error: function(error) {
                                    messagerControl.show('error', error);
                                }
                            }
                            if (!docViewer) {
                                requirejs(['docViewer'], function() {
                                    docViewer = new tct.DocViewer();
                                    docViewer.view(docViewerOptions);
                                })
                            } else {
                                docViewer.view(docViewerOptions);
                            }
                            break;
                    }
                })
            }

            var loadFromUrl = function() {
                $.waitPanel.show();
                var qstring = location.query();
                var tubeid = qstring['id'];
                var userid = qstring['userid'];
                var reqstring: Array<string> = [];
                reqstring.push('id=' + tubeid);
                if (userid) {
                    reqstring.push('userid=' + userid);
                }

                var ajaxRequest = $.ajax({
                    url: '/viewer?' + reqstring.join('&'),
                    type: 'post',
                    dataType: 'json',
                    timeout: 60000,
                    success: function(json) {
                        if (json.error) {
                            messagerControl.show('error', json.error);
                        } else {
                            loadJson(json);
                            $.waitPanel.hide();
                        }
                    },
                    error: function(e) {
                        if (e.statusText !== 'canceled') {
                            messagerControl.show('error', e);
                            loadJson(null);
                        }
                        $.waitPanel.hide();
                    },
                });
            }

            // Ask server for datas
            setTimeout(function() {
                loadFromUrl();
            }, 1);
        };
    })(jQuery);

    $(function() {
        if (location.hostname === '127.0.0.1') {
            var browserVersion = window.browserVersion();
            if (browserVersion.version > 9) {
                $('head').append('<script type="text/javascript" src="http://127.0.0.1:38080/livereload.js"><\/script>');
            }
        }

        $.StartScreen();
    });
}

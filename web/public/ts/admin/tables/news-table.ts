'use strict';

module tct.admin {
    export class NewsTable {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected tableCont: JQuery;
        protected messagerControl: modern.Messager;
        protected dataTable: DataTables.DataTable;

        protected _create = function() {
            var self = this as NewsTable;
            var expandColumnIndex = 0;
            var dateColumnIndex = 2;
            var paramsColumnIndex = 5;
            var actionsColumnIndex = 6;
            var editedNews = {} as NewsData;

            $.get('/resource?r=widgets%2Fadmin%2Fnews-table.html').done(function(html) {

                self.tableCont = $(html).appendTo(self.element);

                self.messagerControl = $('#notify').messager().data('modern-messager');

                var setTableLanguage = function(locale: string) {
                    $.get('/resource?j=ts%2Fwidgets%2Fdatatables%2Fi18n%2F' + locale + '.lang').done(function(json) {
                        var language = (<any>self.dataTable.settings())[0].oLanguage;
                        for (var field in json) {
                            language[field] = json[field];
                        }
                        self.dataTable.draw();
                    })
                }

                var translatorControl = $('#translator').on('translatorcontrolchange', function(e: modern.TranslatorControlEvent) {
                    setTableLanguage(e.value);
                })

                var $dialog = self.tableCont.find('#light-theme[confirm-dialog]');
                $dialog.find('[cancel]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[confirm-dialog]').data('dialog').close();
                });
                $dialog.find('[validate]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[confirm-dialog]').data('dialog').validate();
                });

                var $editDialog = self.tableCont.find('[edit-dialog]');
                $editDialog.find('[cancel]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[edit-dialog]').data('dialog').close();
                });
                var editOkButton = $editDialog.find('[validate]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[edit-dialog]').data('dialog').validate();
                });

                // Edit dialog field
                var editDateField = $editDialog.find('[data-field="date"]').textinput().on('change', function(e: JQueryEventObject, date: Date) {
                    editedNews.date = date.toUTCString();
                });
                var editTextField = $editDialog.find('[data-field="text"]').textinput();
                var editTextInput = editTextField.find('input').focus(1, function() {
                    this.select();
                }).on('keypress', function(e) {
                    if (e.keyCode === 13) {
                        editUidInput.focus();
                    }
                });
                var editUidField = $editDialog.find('[data-field="uid"]').textinput();
                var editUidInput = editUidField.find('input').focus(1, function() {
                    this.select();
                }).on('keypress', function(e) {
                    if (e.keyCode === 13) {
                        editOkButton.click();
                    }
                });

                self.tableCont.find('#addnew').on('click', function(e: JQueryEventObject) {
                    edit({
                        date: new Date().toUTCString(),
                        text: '',
                        uid: ''
                    }, function(json) {
                        self.dataTable.rows.add(json).draw();
                    })
                })

                var edit = function(news: NewsData, done: (json?: any) => void) {
                    var editDialog = $editDialog.data('dialog');
                    editedNews = $.extend({}, news);

                    if (editedNews) {
                        var date = new Date(editedNews.date);
                        editDateField.datepicker('setDate', date);
                        editTextField.textinput('value', editedNews.text);
                        editUidField.textinput('value', editedNews.uid);
                    }

                    editDialog.validate = function() {
                        editedNews.text = editTextField.textinput('value');
                        editedNews.uid = editUidField.textinput('value');

                        // Post request
                        $.waitPanel.show();
                        var ajaxRequest = $.ajax({
                            url: '/admin?d=news-edit&q=' + encodeURIComponent(JSON.stringify(editedNews)),
                            dataType: 'json',
                            type: 'post',
                            timeout: 60000,
                            success: function(json) {
                                if (json && json.error) {
                                    self.messagerControl.show('error', json.error);
                                } else {
                                    done(json);
                                }
                                editDialog.close();
                                $.waitPanel.hide();
                            },
                            error: function(e) {
                                self.messagerControl.show('error', e);
                                editDialog.close();
                                $.waitPanel.hide();
                            },
                        });
                    }

                    $editDialog.css('opacity', 0);
                    editDialog.open();
                    editTextInput.focus();
                    $editDialog.animate({ opacity: 1 }, 500);
                }

                var request = function(req: string, done: (json?: any) => void) {
                    var dialog = $dialog.data('dialog');
                    dialog.validate = function() {
                        // Post request
                        $.waitPanel.show();
                        var ajaxRequest = $.ajax({
                            url: '/admin?' + req,
                            dataType: 'json',
                            type: 'post',
                            timeout: 60000,
                            success: function(json) {
                                if (json && json.error) {
                                    self.messagerControl.show('error', json.error);
                                } else {
                                    done(json);
                                }
                                dialog.close();
                                $.waitPanel.hide();
                            },
                            error: function(e) {
                                if (e.status === 200) {
                                    done();
                                } else {
                                    self.messagerControl.show('error', e);
                                }
                                dialog.close();
                                $.waitPanel.hide();
                            },
                        });
                    }

                    $dialog.css('opacity', 0);
                    dialog.open();
                    $dialog.animate({ opacity: 1 }, 500);
                }

                self.tableCont.find('table').on('click', '[data-click]', function(e) {
                    var link = $(e.target).closest('[data-click]');
                    var func = link.attr('data-click');
                    if (func) {
                        var rowIdx = link.closest('ul').data('dtr-index');
                        var rowApi = rowIdx !== undefined ? self.dataTable.row(parseInt(rowIdx)) : self.dataTable.row(link.closest('tr'));
                        var news = rowApi.data() as NewsData;
                        if (func === 'edit') {
                            edit(news, function(json) {
                                rowApi.data(json).draw();
                            })

                        } else {
                            var date = new Date(news.date);
                            $dialog.find('#dlg-text').html('<span class="fg-theme">' + link.text() + ' ' + news._id + '</span><br>Date: ' + date.toLocaleDateString() + ' ' + date.toLocaleTimeString() + '<br>' + news.text);
                            var qstring: Array<string> = [];
                            qstring.push('d=news-' + func);
                            qstring.push('id=' + news._id);
                            request(qstring.join('&'), function(json) {
                                if (func === 'delete') {
                                    // Delete row
                                    rowApi.remove();
                                    rowApi.draw(false);
                                }
                            })
                        }
                    }
                })

                self.dataTable = self.tableCont.find('table').DataTable({
                    processing: true,
                    responsive: true,
                    order: [[dateColumnIndex, "desc"]],
                    dom: 'lf<"maintable_toolbar">rtip',
                    ajax: {
                        url: '/admin?d=news',
                        type: "POST"
                    },
                    columnDefs: [
                        {
                            className: 'control fg-theme',
                            orderable: false,
                            targets: expandColumnIndex,
                            render: function(data, type, row) {
                                return '<span expand class="mif-chevron-right"></span><span collapse class="mif-expand-more"></span>';
                            },
                        },
                        {
                            render: function(data, type, row) {
                                if (!data) {
                                    return '';
                                }
                                var date = new Date(data);
                                return date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
                            },
                            targets: dateColumnIndex
                        },
                        {
                            render: function(data, type, row) {
                                var params = data as { [key: number]: string };
                                var sb = [] as Array<string>;
                                sb.push('<ul>');
                                if (params) {
                                    for (var key in params) {
                                        sb.push('<li>' + String(key) + ': ' + params[key] + '</li>');
                                    }
                                }
                                sb.push('</ul>');
                                return sb.join('');
                            },
                            targets: paramsColumnIndex
                        },
                        {
                            targets: actionsColumnIndex,
                            render: function(data, type, row) {
                                return self.tableCont.find('#actions').html();
                            },
                        }
                    ],
                    columns: [
                        {
                            defaultContent: '',
                        },
                        {
                            data: '_id'
                        },
                        {
                            data: 'date',
                            type: 'date',
                            defaultContent: '',
                            className: 'nowrap'
                        },
                        {
                            data: 'text',
                            type: 'string',
                            defaultContent: ''
                        },
                        {
                            data: 'uid',
                            type: 'number',
                            defaultContent: '',
                            className: 'nowrap'
                        },
                        {
                            data: 'params',
                            className: 'nowrap'
                        },
                        {
                            defaultContent: '',
                            className: 'nowrap'
                        },
                    ],
                    headerCallback: function(tr: Element) {
                        $(tr).find('td').addClass('fg-theme');
                    },
                });

                var toolbar = self.tableCont.find('#toolbar');
                toolbar.children().appendTo(self.tableCont.find('.maintable_toolbar'));

                var locale = translatorControl.translatorControl('selectedValue');
                setTableLanguage(locale);

                self.oncreated();
            })
        };

        protected oncreated = function() {
            var self = this as NewsTable;
            var e = jQuery.Event("created");
            self._trigger('created', e);
        }

        public show = function() {
            var self = this as NewsTable;
            self.tableCont.show();
            self.dataTable.columns.adjust();
            return self;
        }
    }
}

$.widget("tct.newsTable", new tct.admin.NewsTable())

interface JQuery {
    newsTable(): JQuery,
    newsTable(method: string): tct.admin.NewsTable;
    newsTable(method: 'show'): tct.admin.NewsTable;
    newsTable(method: string): JQuery;
    newsTable(method: 'instance'): JQuery;
}

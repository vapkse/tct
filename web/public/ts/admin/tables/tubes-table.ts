'use strict';

module tct.admin {
    export class TubesTable {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected tableCont: JQuery;
        protected messagerControl: modern.Messager;
        protected dataTable: DataTables.DataTable;

        protected _create = function() {
            var self = this as TubesTable;
            var expandColumnIndex = 0;
            var idColumnIndex = 2;
            var nameColumnIndex = 2;
            var typeColumnIndex = 3;
            var actionsColumnIndex = 4;

            $.get('/resource?r=widgets%2Fadmin%2Ftubes-table.html').done(function(html) {

                self.tableCont = $(html).appendTo(self.element);

                self.messagerControl = $('#notify').messager().data('modern-messager');

                var setTableLanguage = function(locale: string) {
                    $.get('/resource?j=ts%2Fwidgets%2Fdatatables%2Fi18n%2F' + locale + '.lang').done(function(json) {
                        var language = (<any>self.dataTable.settings())[0].oLanguage;
                        for (var field in json) {
                            language[field] = json[field];
                        }
                        self.dataTable.draw();
                    })
                }

                var translatorControl = $('#translator').on('translatorcontrolchange', function(e: modern.TranslatorControlEvent) {
                    setTableLanguage(e.value);
                })

                var $dialog = self.tableCont.find('#light-theme[confirm-dialog]');
                $dialog.find('[cancel]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[confirm-dialog]').data('dialog').close();
                });
                $dialog.find('[validate]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[confirm-dialog]').data('dialog').validate();
                });

                var request = function(req: string, done: (json?: any) => void) {
                    var dialog = $dialog.data('dialog');
                    dialog.validate = function() {
                        // Post request
                        $.waitPanel.show();
                        var ajaxRequest = $.ajax({
                            url: '/admin?' + req,
                            dataType: 'json',
                            type: 'post',
                            timeout: 60000,
                            success: function(json) {
                                if (json && json.error) {
                                    self.messagerControl.show('error', json.error);
                                } else {
                                    done(json);
                                }
                                dialog.close();
                                $.waitPanel.hide();
                            },
                            error: function(e) {
                                if (e.status === 200) {
                                    done();
                                } else {
                                    self.messagerControl.show('error', e);
                                }
                                dialog.close();
                                $.waitPanel.hide();
                            },
                        });
                    }

                    $dialog.css('opacity', 0);
                    dialog.open();
                    $dialog.animate({ opacity: 1 }, 500);
                }

                self.tableCont.find('table').on('click', '[data-click]', function(e) {
                    var link = $(e.target).closest('[data-click]');
                    var func = link.attr('data-click');
                    if (func) {
                        var rowIdx = link.closest('ul').data('dtr-index');
                        var rowApi = rowIdx !== undefined ? self.dataTable.row(parseInt(rowIdx)) : self.dataTable.row(link.closest('tr'));
                        var tube = rowApi.data() as TubeData;
                        $dialog.find('#dlg-text').html('<span class="fg-theme">' + link.text() + ' ' + tube._id + '</span><br>' + tube.name + ' ' + (tube.type || ''));
                        var qstring: Array<string> = [];
                        qstring.push('d=tube-' + func);
                        qstring.push('id=' + tube._id);
                        request(qstring.join('&'), function(json) {
                            if (func === 'delete') {
                                // Delete row
                                rowApi.remove();
                                rowApi.draw(false);
                            }
                        })
                    }
                })

                self.dataTable = self.tableCont.find('table').DataTable({
                    processing: true,
                    responsive: true,
                    dom: 'lf<"maintable_toolbar">rtip',
                    ajax: {
                        url: '/admin?d=tubes',
                        type: "POST"
                    },
                    columnDefs: [
                        {
                            className: 'control fg-theme',
                            orderable: false,
                            targets: expandColumnIndex,
                            render: function(data, type, row) {
                                return '<span expand class="mif-chevron-right"></span><span collapse class="mif-expand-more"></span>';
                            },
                        },
                        {
                            targets: actionsColumnIndex,
                            render: function(data, type, row) {
                                return self.tableCont.find('#actions').html();
                            },
                        }
                    ],
                    columns: [
                        {
                            defaultContent: '',
                        },
                        {
                            data: '_id'
                        },
                        {
                            data: 'name',
                            type: 'string',
                            defaultContent: '',
                            className: 'nowrap'
                        },
                        {
                            data: 'type',
                            type: 'string',
                            defaultContent: '',
                            className: 'nowrap'
                        },
                        {
                            defaultContent: '',
                            className: 'nowrap'
                        },
                    ],
                    headerCallback: function(tr: Element) {
                        $(tr).find('td').addClass('fg-theme');
                    },
                });

                var toolbar = self.tableCont.find('#toolbar');
                toolbar.children().appendTo(self.tableCont.find('.maintable_toolbar'));

                var locale = translatorControl.translatorControl('selectedValue');
                setTableLanguage(locale);

                self.oncreated();
            })
        };

        protected oncreated = function() {
            var self = this as TubesTable;
            var e = jQuery.Event("created");
            self._trigger('created', e);
        }

        public show = function() {
            var self = this as TubesTable;
            self.tableCont.show();
            self.dataTable.columns.adjust();
            return self;
        }
    }
}

$.widget("tct.tubesTable", new tct.admin.TubesTable())

interface JQuery {
    tubesTable(): JQuery,
    tubesTable(method: string): tct.admin.TubesTable;
    tubesTable(method: 'show'): tct.admin.TubesTable;
    tubesTable(method: string): JQuery;
    tubesTable(method: 'instance'): JQuery;
}

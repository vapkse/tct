'use strict';
var tct;
(function (tct) {
    var admin;
    (function (admin) {
        var UserTable = (function () {
            function UserTable() {
                this._create = function () {
                    var self = this;
                    var expandColumnIndex = 0;
                    var roleColumnIndex = 6;
                    var validatedColumnIndex = 7;
                    var bockedColumnIndex = 8;
                    var lastLoginColumnIndex = 10;
                    var validationTimeColumnIndex = 13;
                    var actionsColumnIndex = 14;
                    $.get('/resource?r=widgets%2Fadmin%2Fuser-table.html').done(function (html) {
                        self.tableCont = $(html).appendTo(self.element);
                        self.messagerControl = $('#notify').messager().data('modern-messager');
                        var setTableLanguage = function (locale) {
                            $.get('/resource?j=ts%2Fwidgets%2Fdatatables%2Fi18n%2F' + locale + '.lang').done(function (json) {
                                var language = self.dataTable.settings()[0].oLanguage;
                                for (var field in json) {
                                    language[field] = json[field];
                                }
                                self.dataTable.draw();
                            });
                        };
                        var translatorControl = $('#translator').on('translatorcontrolchange', function (e) {
                            setTableLanguage(e.value);
                        });
                        self.dialog = self.tableCont.find('#light-theme[confirm-dialog]');
                        self.dialog.find('[cancel]').on('click', function (e) {
                            $(e.target).closest('[confirm-dialog]').data('dialog').close();
                        });
                        self.dialog.find('[validate]').on('click', function (e) {
                            $(e.target).closest('[confirm-dialog]').data('dialog').validate();
                        });
                        self.tableCont.find('table').on('click', '[data-click]', function (e) {
                            var link = $(e.target).closest('[data-click]');
                            var func = link.attr('data-click');
                            if (func) {
                                var rowIdx = link.closest('ul').data('dtr-index');
                                var rowApi = rowIdx !== undefined ? self.dataTable.row(parseInt(rowIdx)) : self.dataTable.row(link.closest('tr'));
                                var user = rowApi.data();
                                self.dialog.find('#dlg-text').html('<span class="fg-theme">' + link.text() + ' ' + (user.userName || user.displayName) + '?</span><br>' + (user.email || ''));
                                var qstring = [];
                                qstring.push('d=user-' + func);
                                qstring.push('id=' + user._id);
                                self.sendrequest(qstring.join('&'), function (json) {
                                    if (func === 'block' || func === 'unblock') {
                                        user.blocked = json.blocked;
                                        rowApi.data(user).draw();
                                    }
                                    else if (func === 'validate') {
                                        user.validated = json.validated;
                                        var cell = self.dataTable.cell({ row: rowApi.index(), column: validationTimeColumnIndex });
                                        cell.data(user.validated).draw(true);
                                    }
                                    else if (func === 'delete') {
                                        rowApi.remove();
                                        rowApi.draw(false);
                                    }
                                });
                            }
                        });
                        self.dataTable = self.tableCont.find('table').DataTable({
                            processing: true,
                            responsive: true,
                            ajax: {
                                url: '/admin?d=users',
                                type: "POST"
                            },
                            columnDefs: [
                                {
                                    className: 'control fg-theme',
                                    orderable: false,
                                    targets: expandColumnIndex,
                                    render: function (data, type, row) {
                                        return '<span expand class="mif-chevron-right"></span><span collapse class="mif-expand-more"></span>';
                                    },
                                },
                                {
                                    render: function (data, type, row) {
                                        if (data) {
                                            return '<span class="mif-checkmark fg-green"></span>';
                                        }
                                        return '<span class="mif-stop fg-red"></span>';
                                    },
                                    targets: validatedColumnIndex
                                },
                                {
                                    render: function (data, type, row) {
                                        if (data) {
                                            return '<span class="mif-checkmark fg-red"></span>';
                                        }
                                        return '<span class="mif-stop fg-green"></span>';
                                    },
                                    targets: bockedColumnIndex
                                },
                                {
                                    render: function (data, type, row) {
                                        if (!data) {
                                            return '';
                                        }
                                        var date = new Date(data);
                                        return date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
                                    },
                                    targets: [lastLoginColumnIndex, validationTimeColumnIndex]
                                },
                                {
                                    render: function (data, type, row) {
                                        var user = row;
                                        var role = user.role || 'user';
                                        return '<span class="fg-theme-over link" userid="' + user._id + '" role="' + role + '" onclick="tct.admin.UserTable.roleMenuClick(event)">' + role + '</span>';
                                    },
                                    targets: roleColumnIndex
                                },
                                {
                                    render: function (data, type, row) {
                                        var actions = self.tableCont.find('#actions');
                                        var user = row;
                                        if (user.blocked) {
                                            actions.find('[data-click="unblock"]').show();
                                            actions.find('[data-click="block"]').hide();
                                        }
                                        else {
                                            actions.find('[data-click="unblock"]').hide();
                                            actions.find('[data-click="block"]').show();
                                        }
                                        return actions.html();
                                    },
                                    targets: actionsColumnIndex
                                },
                            ],
                            columns: [
                                {
                                    defaultContent: '',
                                },
                                {
                                    data: '_id'
                                },
                                {
                                    data: 'userName',
                                    type: 'string',
                                    defaultContent: '',
                                    className: 'nowrap'
                                },
                                {
                                    data: 'email',
                                    type: 'string',
                                    defaultContent: '',
                                    className: 'nowrap'
                                },
                                {
                                    data: 'displayName',
                                    type: 'string',
                                    defaultContent: '',
                                    className: 'nowrap'
                                },
                                {
                                    data: 'provider',
                                    className: 'nowrap'
                                },
                                {
                                    data: 'role',
                                    className: 'nowrap'
                                },
                                {
                                    data: 'validated',
                                    type: 'boolean',
                                    defaultContent: 'false',
                                    className: 'align-center'
                                },
                                {
                                    data: 'blocked',
                                    type: 'boolean',
                                    defaultContent: 'false',
                                    className: 'align-center'
                                },
                                {
                                    data: 'failAttempts',
                                    type: 'number',
                                    defaultContent: '0',
                                    className: 'align-right'
                                },
                                {
                                    data: 'lastLogin',
                                    type: 'number',
                                    defaultContent: '0',
                                    className: 'nowrap'
                                },
                                {
                                    data: 'lastIP',
                                    type: 'string',
                                    defaultContent: '',
                                    className: 'nowrap'
                                },
                                {
                                    data: 'validationToken',
                                    type: 'string',
                                    defaultContent: ''
                                },
                                {
                                    data: 'validationTime',
                                    type: 'date',
                                    defaultContent: '',
                                    className: 'nowrap'
                                },
                                {
                                    defaultContent: '',
                                },
                            ],
                            headerCallback: function (tr) {
                                $(tr).find('td').addClass('fg-theme');
                            },
                        });
                        var locale = translatorControl.translatorControl('selectedValue');
                        setTableLanguage(locale);
                        self.oncreated();
                    });
                };
                this.oncreated = function () {
                    var self = this;
                    var e = jQuery.Event("created");
                    self._trigger('created', e);
                };
                this.show = function () {
                    var self = this;
                    self.tableCont.show();
                    self.dataTable.columns.adjust();
                    return self;
                };
                this.sendrequest = function (req, done) {
                    var self = this;
                    var dialog = self.dialog.data('dialog');
                    dialog.validate = function () {
                        $.waitPanel.show();
                        var ajaxRequest = $.ajax({
                            url: '/admin?' + req,
                            dataType: 'json',
                            type: 'post',
                            timeout: 60000,
                            success: function (json) {
                                if (json && json.error) {
                                    self.messagerControl.show('error', json.error);
                                }
                                else {
                                    done(json);
                                }
                                dialog.close();
                                $.waitPanel.hide();
                            },
                            error: function (e) {
                                if (e.status === 200) {
                                    done();
                                }
                                else {
                                    self.messagerControl.show('error', e);
                                }
                                dialog.close();
                                $.waitPanel.hide();
                            },
                        });
                    };
                    self.dialog.css('opacity', 0);
                    dialog.open();
                    self.dialog.animate({ opacity: 1 }, 500);
                };
                this.showRoleMenu = function (clickedElement, role, userid) {
                    var self = this;
                    requirejs(['dropdown'], function (ns) {
                        var dropDown = new ns.DropDown();
                        dropDown.show({
                            template: self.tableCont.find('#rolesmenu').html(),
                            backdrop: true,
                            backdropCancel: true,
                            backdropId: 'dmenu-backdrop',
                            modalId: 'dmenu-body',
                            position: {
                                of: clickedElement,
                                my: 'right top',
                                at: 'right top'
                            },
                            cancelOnEsc: true
                        }).modalBody.on('click', function (e) {
                            var $dataClick = $(e.target).closest('[data-click]');
                            if ($dataClick.is('.disabled')) {
                                return;
                            }
                            var func = $dataClick.attr('data-click');
                            var currentRole = role || 'user';
                            dropDown.close();
                            if (func === currentRole) {
                                return;
                            }
                            if (func) {
                                self.dialog.find('#dlg-text').html('Change role of <span class="fg-theme">' + userid + '</span> to: <span class="fg-theme">' + func + '</span> ?');
                                var qstring = [];
                                qstring.push('d=user-role');
                                qstring.push('r=' + func);
                                qstring.push('id=' + userid);
                                self.sendrequest(qstring.join('&'), function (json) {
                                    var role = json.role || 'user';
                                    clickedElement.html(role).attr('role', role);
                                });
                            }
                        });
                    });
                };
            }
            UserTable.roleMenuClick = function (e) {
                var clickedElement = $(e.target);
                var userTable = clickedElement.closest('#usertable').parent().userTable().data('tct-userTable');
                userTable.showRoleMenu(clickedElement, clickedElement.attr('role'), clickedElement.attr('userid'));
            };
            return UserTable;
        })();
        admin.UserTable = UserTable;
    })(admin = tct.admin || (tct.admin = {}));
})(tct || (tct = {}));
$.widget("tct.userTable", new tct.admin.UserTable());

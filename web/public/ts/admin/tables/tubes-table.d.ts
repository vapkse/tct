declare module tct.admin {
    class TubesTable {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected tableCont: JQuery;
        protected messagerControl: modern.Messager;
        protected dataTable: DataTables.DataTable;
        protected _create: () => void;
        protected oncreated: () => void;
        show: () => TubesTable;
    }
}
interface JQuery {
    tubesTable(): JQuery;
    tubesTable(method: string): tct.admin.TubesTable;
    tubesTable(method: 'show'): tct.admin.TubesTable;
    tubesTable(method: string): JQuery;
    tubesTable(method: 'instance'): JQuery;
}

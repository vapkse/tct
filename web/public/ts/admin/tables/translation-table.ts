'use strict';

module tct.admin {
    export class TransTable {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected tableCont: JQuery;
        protected messagerControl: modern.Messager;
        protected dataTable: DataTables.DataTable;
        protected translations: JQuery;
        protected $table: JQuery;

        protected _create = function() {
            var self = this as TransTable;
            var expandColumnIndex = 0;
            var idColumnIndex = 2;
            var originalColumnIndex = 2;
            var selectedColumnIndex = 3;
            var currentColumnIndex = 4;
            var isBestColumnIndex = 5;
            var versionColumnIndex = 6;
            var actionsColumnIndex = 7;
            var editedRecord = {} as TranslationRecord;

            $.get('/resource?r=widgets%2Fadmin%2Ftrans-table.html').done(function(html) {

                self.tableCont = $(html).appendTo(self.element);

                self.$table = self.tableCont.find('table');
                
                var setTableLanguage = function(locale: string) {
                    $.get('/resource?j=ts%2Fwidgets%2Fdatatables%2Fi18n%2F' + locale + '.lang').done(function(json) {
                        var language = (<any>self.dataTable.settings())[0].oLanguage;
                        for (var field in json) {
                            language[field] = json[field];
                        }
                        self.dataTable.draw();
                    })
                }

                var translatorControl = $('#translator').on('translatorcontrolchange', function(e: modern.TranslatorControlEvent) {
                    self.dataTable.ajax.url(getAjaxUrl()).load();
                    setTableLanguage(e.value);
                })

                self.messagerControl = $('#notify').messager().data('modern-messager');
                self.translations = self.tableCont.find('translations');

                var $dialog = self.tableCont.find('#light-theme[confirm-dialog]');
                $dialog.find('[cancel]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[confirm-dialog]').data('dialog').close();
                });
                $dialog.find('[validate]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[confirm-dialog]').data('dialog').validate();
                });

                var $editDialog = self.tableCont.find('[edit-dialog]');
                $editDialog.find('[cancel]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[edit-dialog]').data('dialog').close();
                });
                var editOkButton = $editDialog.find('[validate]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[edit-dialog]').data('dialog').validate();
                });

                // Edit dialog field
                var editUidField = $editDialog.find('[data-field="uid"]').textinput();
                var editUidInput = editUidField.find('input').focus(1, function() {
                    this.select();
                }).on('keypress', function(e) {
                    if (e.keyCode === 13) {
                        editOriginalInput.focus();
                    }
                });

                var editOriginalField = $editDialog.find('[data-field="original"]').textinput();
                var editOriginalInput = editOriginalField.find('textarea').focus(1, function() {
                    this.select();
                }).on('keypress', function(e) {
                    if (e.keyCode === 13) {
                        editTranslationInput.focus();
                    }
                });

                var editTranslationField = $editDialog.find('[data-field="translation"]').textinput();
                var editTranslationInput = editTranslationField.find('textarea').focus(1, function() {
                    this.select();
                }).on('keypress', function(e) {
                    if (e.keyCode === 13) {
                        editOkButton.click();
                    }
                });

                var delButton = self.tableCont.find('#delsel').on('click', function(e: JQueryEventObject) {
                    if (delButton.is('[disabled]')) {
                        return;
                    }

                    var rows = self.dataTable.rows('.selected');
                    var datas = rows.data() as any;
                    if (datas.length === 0) {
                        return;
                    }

                    var ids = [] as Array<string>
                    for (var i = 0; i < datas.length; i++) {
                        var trans = datas[i] as TranslationRecord;
                        ids.push(trans._id);
                    }
                    $dialog.find('#dlg-text').html('<span class="fg-theme">' + self.translations.find('#delselrows').html() + '</span>');
                    var qstring: Array<string> = [];
                    qstring.push('d=trans-delete');
                    qstring.push('ids=' + encodeURIComponent(JSON.stringify(ids)));
                    request(qstring.join('&'), function(json) {
                        // Delete row
                        rows.remove();
                        rows.draw(false);
                        delButton.attr('disabled', 'disabled');
                    })
                })

                var edit = function(trans: TranslationRecord, done: (json?: any) => void) {
                    var editDialog = $editDialog.data('dialog');
                    editedRecord = $.extend({}, trans);

                    editUidField.textinput('value', editedRecord._id);
                    editOriginalField.textinput('value', editedRecord.value);
                    editTranslationField.textinput('value', editedRecord.selected);

                    editDialog.validate = function() {
                        var newid = editUidField.textinput('value');
                        editedRecord.value = editOriginalField.textinput('value');
                        editedRecord.selected = editTranslationField.textinput('value');

                        var sr = [] as Array<string>;
                        sr.push('d=trans-edit');
                        if (editedRecord._id !== newid) {
                            sr.push('newid=' + newid);
                        }
                        var sl = self.tableCont.find('.maintable_toolbar [language-select]').chosen2('selectedValue');
                        sr.push('sl=' + sl);
                        sr.push('q=' + encodeURIComponent(JSON.stringify(editedRecord)));

                        // Post request
                        $.waitPanel.show();
                        var ajaxRequest = $.ajax({
                            url: '/admin?' + sr.join('&'),
                            dataType: 'json',
                            type: 'post',
                            timeout: 60000,
                            success: function(json) {
                                if (json && json.error) {
                                    self.messagerControl.show('error', json.error);
                                } else {
                                    done(json);
                                }
                                editDialog.close();
                                $.waitPanel.hide();
                            },
                            error: function(e) {
                                self.messagerControl.show('error', e);
                                editDialog.close();
                                $.waitPanel.hide();
                            },
                        });
                    }

                    $editDialog.css('opacity', 0);
                    editDialog.open();
                    editUidInput.focus();
                    $editDialog.animate({ opacity: 1 }, 500);
                }

                var request = function(req: string, done: (json?: any) => void) {
                    var dialog = $dialog.data('dialog');
                    dialog.validate = function() {
                        // Post request
                        $.waitPanel.show();
                        var ajaxRequest = $.ajax({
                            url: '/admin?' + req,
                            dataType: 'json',
                            type: 'post',
                            timeout: 60000,
                            success: function(json) {
                                if (json && json.error) {
                                    self.messagerControl.show('error', json.error);
                                } else {
                                    done(json);
                                }
                                dialog.close();
                                $.waitPanel.hide();
                            },
                            error: function(e) {
                                if (e.status === 200) {
                                    done();
                                } else {
                                    self.messagerControl.show('error', e);
                                }
                                dialog.close();
                                $.waitPanel.hide();
                            },
                        });
                    }

                    $dialog.css('opacity', 0);
                    dialog.open();
                    $dialog.animate({ opacity: 1 }, 500);
                }

                self.$table.on('click', '[data-click]', function(e) {
                    var link = $(e.target).closest('[data-click]');
                    var func = link.attr('data-click');
                    if (func) {
                        var rowIdx = link.closest('ul').data('dtr-index');
                        var rowApi = rowIdx !== undefined ? self.dataTable.row(parseInt(rowIdx)) : self.dataTable.row(link.closest('tr'));
                        var rec = rowApi.data() as TranslationRecord;
                        if (func === 'edit') {
                            edit(rec, function(json) {
                                rowApi.data(json).draw();
                            })

                        } else {
                            // Not implemented                            
                        }
                    }
                }).selection().on('selectionchange', function(e: datatables.SelectionEvent) {
                    if (e.value) {
                        delButton.removeAttr('disabled');
                    } else {
                        delButton.attr('disabled', 'disabled');
                    }
                })

                var getAjaxUrl = function() {
                    var query = location.query();
                    var qstring = [] as Array<string>;
                    qstring.push('d=translations');
                    if (query['sl']) {
                        qstring.push('sl=' + query['sl']);
                    }
                    var current = translatorControl.translatorControl('selectedValue');
                    if (current) {
                        qstring.push('cl=' + current);
                    }
                    return '/admin?' + qstring.join('&');
                }

                self.dataTable = self.tableCont.find('table').DataTable({
                    processing: true,
                    responsive: true,
                    order: [[idColumnIndex, "asc"]],
                    dom: 'lf<"maintable_toolbar">rtip',
                    ajax: {
                        url: getAjaxUrl(),
                        type: "POST"
                    },
                    columnDefs: [
                        {
                            className: 'control fg-theme',
                            orderable: false,
                            targets: expandColumnIndex,
                            render: function(data, type, row) {
                                return '<span expand class="mif-chevron-right"></span><span collapse class="mif-expand-more"></span>';
                            },
                        },
                        {
                            render: function(data, type, row) {
                                if (data) {
                                    return '<span class="mif-checkmark fg-green"></span>'
                                }
                            },
                            targets: isBestColumnIndex
                        },
                        {
                            targets: actionsColumnIndex,
                            render: function(data, type, row) {
                                return self.tableCont.find('#actions').html();
                            },
                        },
                    ],
                    columns: [
                        {
                            name: 'default',
                            defaultContent: '',
                        },
                        {
                            name: '_id',
                            data: '_id',
                            className: 'nowrap'
                        },
                        {
                            name: 'value',
                            data: 'value',
                            type: 'string',
                            defaultContent: ''
                        },
                        {
                            name: 'selected',
                            data: 'selected',
                            type: 'string',
                            defaultContent: ''
                        },
                        {
                            name: 'current',
                            data: 'current',
                            type: 'string',
                            defaultContent: ''
                        },
                        {
                            name: 'isBest',
                            data: 'isBest',
                            type: 'boolean',
                            defaultContent: '',
                            className: 'align-center'
                        },
                        {
                            name: 'version',
                            data: 'version',
                            type: 'number',
                            defaultContent: '',
                            className: 'nowrap'
                        },
                        {
                            name: 'action',
                            defaultContent: '',
                            className: 'nowrap'
                        },
                    ],
                    headerCallback: function(tr: Element) {
                        $(tr).find('td').addClass('fg-theme');
                    },
                });

                var toolbar = self.tableCont.find('#toolbar');
                toolbar.find('[language-select]').chosen2({
                    selectedValue: location.query()['sl'] || 'en'
                }).on('chosen2change', function(e: modern.ChosenEvent) {
                    $.pageState.replaceUrlParam('sl', e.value)
                    self.dataTable.ajax.url(getAjaxUrl()).load();
                });
                toolbar.children().appendTo(self.tableCont.find('.maintable_toolbar'));

                var locale = translatorControl.translatorControl('selectedValue');
                setTableLanguage(locale);

                self.oncreated();
            })
        };

        protected oncreated = function() {
            var self = this as TransTable;
            var e = jQuery.Event("created");
            self._trigger('created', e);
        }

        public show = function() {
            var self = this as TransTable;
            self.tableCont.show();
            self.dataTable.columns.adjust();
            return self;
        }
    }
}

$.widget("tct.transTable", new tct.admin.TransTable())

interface JQuery {
    transTable(): JQuery,
    transTable(method: string): tct.admin.TransTable;
    transTable(method: 'show'): tct.admin.TransTable;
    transTable(method: string): JQuery;
    transTable(method: 'instance'): JQuery;
}

declare module tct.admin {
    class Documents {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected tableCont: JQuery;
        protected messagerControl: modern.Messager;
        protected dataTable: DataTables.DataTable;
        protected _create: () => void;
        protected oncreated: () => void;
        show: () => Documents;
    }
}
interface JQuery {
    documents(): JQuery;
    documents(method: string): tct.admin.Documents;
    documents(method: 'show'): tct.admin.Documents;
    documents(method: string): JQuery;
    documents(method: 'instance'): JQuery;
}

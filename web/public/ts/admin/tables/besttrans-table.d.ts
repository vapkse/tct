declare module tct.admin {
    class BestTransTable {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected tableCont: JQuery;
        protected messagerControl: modern.Messager;
        protected dataTable: DataTables.DataTable;
        protected translations: JQuery;
        protected $table: JQuery;
        protected _create: () => void;
        protected oncreated: () => void;
        show: () => BestTransTable;
    }
}
interface JQuery {
    besttransTable(): JQuery;
    besttransTable(method: string): tct.admin.BestTransTable;
    besttransTable(method: 'show'): tct.admin.BestTransTable;
    besttransTable(method: string): JQuery;
    besttransTable(method: 'instance'): JQuery;
}

'use strict';
var tct;
(function (tct) {
    var admin;
    (function (admin) {
        var BestTransTable = (function () {
            function BestTransTable() {
                this._create = function () {
                    var self = this;
                    var expandColumnIndex = 0;
                    var idColumnIndex = 2;
                    var originalColumnIndex = 3;
                    var proposalColumnIndex = 5;
                    var currentColumnIndex = 6;
                    var userNameColumnIndex = 7;
                    var actionsColumnIndex = 8;
                    var editedRecord = {};
                    $.get('/resource?r=widgets%2Fadmin%2Fbesttrans-table.html').done(function (html) {
                        self.tableCont = $(html).appendTo(self.element);
                        self.$table = self.tableCont.find('table');
                        var setTableLanguage = function (locale) {
                            $.get('/resource?j=ts%2Fwidgets%2Fdatatables%2Fi18n%2F' + locale + '.lang').done(function (json) {
                                var language = self.dataTable.settings()[0].oLanguage;
                                for (var field in json) {
                                    language[field] = json[field];
                                }
                                self.dataTable.draw();
                            });
                        };
                        var translatorControl = $('#translator').on('translatorcontrolchange', function (e) {
                            setTableLanguage(e.value);
                            self.dataTable.ajax.url(getAjaxUrl()).load();
                        });
                        self.messagerControl = $('#notify').messager().data('modern-messager');
                        self.translations = self.tableCont.find('translations');
                        var $dialog = self.tableCont.find('#light-theme[confirm-dialog]');
                        $dialog.find('[cancel]').on('click', function (e) {
                            $(e.target).closest('[confirm-dialog]').data('dialog').close();
                        });
                        $dialog.find('[validate]').on('click', function (e) {
                            $(e.target).closest('[confirm-dialog]').data('dialog').validate();
                        });
                        var $editDialog = self.tableCont.find('[edit-dialog]');
                        $editDialog.find('[cancel]').on('click', function (e) {
                            $(e.target).closest('[edit-dialog]').data('dialog').close();
                        });
                        var editOkButton = $editDialog.find('[validate]').on('click', function (e) {
                            $(e.target).closest('[edit-dialog]').data('dialog').validate();
                        });
                        var editUidField = $editDialog.find('[data-field="uid"]').textinput();
                        var editUidInput = editUidField.find('input').focus(1, function () {
                            this.select();
                        }).on('keypress', function (e) {
                            if (e.keyCode === 13) {
                                editOriginalInput.focus();
                            }
                        });
                        var editOriginalField = $editDialog.find('[data-field="original"]').textinput();
                        var editOriginalInput = editOriginalField.find('textarea').focus(1, function () {
                            this.select();
                        }).on('keypress', function (e) {
                            if (e.keyCode === 13) {
                                editProposalInput.focus();
                            }
                        });
                        var editProposalField = $editDialog.find('[data-field="proposal"]').textinput();
                        var editProposalInput = editProposalField.find('textarea').focus(1, function () {
                            this.select();
                        }).on('keypress', function (e) {
                            if (e.keyCode === 13) {
                                editOkButton.click();
                            }
                        });
                        var delButton = self.tableCont.find('#delsel').on('click', function (e) {
                            if (delButton.is('[disabled]')) {
                                return;
                            }
                            var rows = self.dataTable.rows('.selected');
                            var datas = rows.data();
                            if (datas.length === 0) {
                                return;
                            }
                            var ids = [];
                            for (var i = 0; i < datas.length; i++) {
                                var trans = datas[i];
                                ids.push(trans._id);
                            }
                            $dialog.find('#dlg-text').html('<span class="fg-theme">' + self.translations.find('#delselrows').html() + '</span>');
                            var qstring = [];
                            qstring.push('d=besttrans-delete');
                            qstring.push('ids=' + encodeURIComponent(JSON.stringify(ids)));
                            request(qstring.join('&'), function (json) {
                                rows.remove();
                                rows.draw(false);
                                delButton.attr('disabled', 'disabled');
                            });
                        });
                        var merge = function (proposal, done) {
                            $.waitPanel.show();
                            var ajaxRequest = $.ajax({
                                url: '/admin?d=besttrans-merge&q=' + encodeURIComponent(JSON.stringify(proposal)),
                                dataType: 'json',
                                type: 'post',
                                timeout: 60000,
                                success: function (json) {
                                    $.waitPanel.hide();
                                    done(null, json);
                                },
                                error: function (e) {
                                    $.waitPanel.hide();
                                    done(e);
                                },
                            });
                        };
                        var edit = function (proposal, done) {
                            var editDialog = $editDialog.data('dialog');
                            editedRecord = $.extend({}, proposal);
                            editUidField.textinput('value', editedRecord.uid);
                            editOriginalField.textinput('value', editedRecord.original);
                            editProposalField.textinput('value', editedRecord.proposal);
                            editDialog.validate = function () {
                                editedRecord.proposal = editProposalField.textinput('value');
                                merge(editedRecord, function (error, result) {
                                    editDialog.close();
                                    if (error) {
                                        self.messagerControl.show('error', error);
                                    }
                                    else {
                                        if (result && result.error) {
                                            self.messagerControl.show('error', result.error);
                                        }
                                        else {
                                            done(result);
                                        }
                                    }
                                });
                            };
                            $editDialog.css('opacity', 0);
                            editDialog.open();
                            editProposalInput.focus();
                            $editDialog.animate({ opacity: 1 }, 500);
                        };
                        var request = function (req, done) {
                            var dialog = $dialog.data('dialog');
                            dialog.validate = function () {
                                $.waitPanel.show();
                                var ajaxRequest = $.ajax({
                                    url: '/admin?' + req,
                                    dataType: 'json',
                                    type: 'post',
                                    timeout: 60000,
                                    success: function (json) {
                                        if (json && json.error) {
                                            self.messagerControl.show('error', json.error);
                                        }
                                        else {
                                            done(json);
                                        }
                                        dialog.close();
                                        $.waitPanel.hide();
                                    },
                                    error: function (e) {
                                        if (e.status === 200) {
                                            done();
                                        }
                                        else {
                                            self.messagerControl.show('error', e);
                                        }
                                        dialog.close();
                                        $.waitPanel.hide();
                                    },
                                });
                            };
                            $dialog.css('opacity', 0);
                            dialog.open();
                            $dialog.animate({ opacity: 1 }, 500);
                        };
                        self.$table.on('click', '[data-click]', function (e) {
                            var link = $(e.target).closest('[data-click]');
                            var func = link.attr('data-click');
                            if (func) {
                                var rowIdx = link.closest('ul').data('dtr-index');
                                var rowApi = rowIdx !== undefined ? self.dataTable.row(parseInt(rowIdx)) : self.dataTable.row(link.closest('tr'));
                                var rec = rowApi.data();
                                if (func === 'edit') {
                                    edit(rec, function (json) {
                                        rowApi.remove();
                                        rowApi.draw(false);
                                        delButton.attr('disabled', 'disabled');
                                    });
                                }
                                else if (func === 'merge') {
                                    merge(rec, function (error, json) {
                                        if (error || (json && json.error)) {
                                            self.messagerControl.show('error', json || json.error);
                                        }
                                        else {
                                            rowApi.remove();
                                            rowApi.draw(false);
                                            delButton.attr('disabled', 'disabled');
                                        }
                                    });
                                }
                            }
                        }).selection().on('selectionchange', function (e) {
                            if (e.value) {
                                delButton.removeAttr('disabled');
                            }
                            else {
                                delButton.attr('disabled', 'disabled');
                            }
                        });
                        var getAjaxUrl = function () {
                            var query = location.query();
                            var qstring = [];
                            qstring.push('d=besttrans');
                            var current = translatorControl.translatorControl('selectedValue');
                            if (current) {
                                qstring.push('cl=' + current);
                            }
                            return '/admin?' + qstring.join('&');
                        };
                        self.dataTable = self.tableCont.find('table').DataTable({
                            processing: true,
                            responsive: true,
                            order: [[idColumnIndex, "asc"]],
                            dom: 'lf<"maintable_toolbar">rtip',
                            ajax: {
                                url: getAjaxUrl(),
                                type: "POST"
                            },
                            columnDefs: [
                                {
                                    className: 'control fg-theme',
                                    orderable: false,
                                    targets: expandColumnIndex,
                                    render: function (data, type, row) {
                                        return '<span expand class="mif-chevron-right"></span><span collapse class="mif-expand-more"></span>';
                                    },
                                },
                                {
                                    targets: actionsColumnIndex,
                                    render: function (data, type, row) {
                                        return self.tableCont.find('#actions').html();
                                    },
                                },
                            ],
                            columns: [
                                {
                                    name: 'default',
                                    defaultContent: '',
                                },
                                {
                                    name: '_id',
                                    data: '_id',
                                    className: 'nowrap'
                                },
                                {
                                    name: 'uid',
                                    data: 'uid',
                                    className: 'nowrap'
                                },
                                {
                                    name: 'original',
                                    data: 'original',
                                    type: 'string',
                                    defaultContent: ''
                                },
                                {
                                    name: 'locale',
                                    data: 'locale',
                                    type: 'string',
                                    defaultContent: ''
                                },
                                {
                                    name: 'proposal',
                                    data: 'proposal',
                                    type: 'string',
                                    defaultContent: ''
                                },
                                {
                                    name: 'current',
                                    data: 'current',
                                    type: 'string',
                                    defaultContent: ''
                                },
                                {
                                    name: 'userName',
                                    data: 'userName',
                                    type: 'string',
                                    defaultContent: '',
                                    className: 'nowrap'
                                },
                                {
                                    name: 'action',
                                    defaultContent: '',
                                    className: 'nowrap'
                                },
                            ],
                            headerCallback: function (tr) {
                                $(tr).find('td').addClass('fg-theme');
                            },
                        });
                        var toolbar = self.tableCont.find('#toolbar');
                        toolbar.children().appendTo(self.tableCont.find('.maintable_toolbar'));
                        var locale = translatorControl.translatorControl('selectedValue');
                        setTableLanguage(locale);
                        self.oncreated();
                    });
                };
                this.oncreated = function () {
                    var self = this;
                    var e = jQuery.Event("created");
                    self._trigger('created', e);
                };
                this.show = function () {
                    var self = this;
                    self.tableCont.show();
                    self.dataTable.columns.adjust();
                    return self;
                };
            }
            return BestTransTable;
        })();
        admin.BestTransTable = BestTransTable;
    })(admin = tct.admin || (tct.admin = {}));
})(tct || (tct = {}));
$.widget("tct.besttransTable", new tct.admin.BestTransTable());

'use strict';

module tct.admin {
    export class BestTransTable {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected tableCont: JQuery;
        protected messagerControl: modern.Messager;
        protected dataTable: DataTables.DataTable;
        protected translations: JQuery;
        protected $table: JQuery;

        protected _create = function() {
            var self = this as BestTransTable;
            var expandColumnIndex = 0;
            var idColumnIndex = 2;
            var originalColumnIndex = 3;
            var proposalColumnIndex = 5;
            var currentColumnIndex = 6;
            var userNameColumnIndex = 7;
            var actionsColumnIndex = 8;
            var editedRecord = {} as ProposalData;

            $.get('/resource?r=widgets%2Fadmin%2Fbesttrans-table.html').done(function(html) {

                self.tableCont = $(html).appendTo(self.element);

                self.$table = self.tableCont.find('table');

                var setTableLanguage = function(locale: string) {
                    $.get('/resource?j=ts%2Fwidgets%2Fdatatables%2Fi18n%2F' + locale + '.lang').done(function(json) {
                        var language = (<any>self.dataTable.settings())[0].oLanguage;
                        for (var field in json) {
                            language[field] = json[field];
                        }
                        self.dataTable.draw();
                    })
                }

                var translatorControl = $('#translator').on('translatorcontrolchange', function(e: modern.TranslatorControlEvent) {
                    setTableLanguage(e.value);
                    self.dataTable.ajax.url(getAjaxUrl()).load();
                })

                self.messagerControl = $('#notify').messager().data('modern-messager');
                self.translations = self.tableCont.find('translations');

                var $dialog = self.tableCont.find('#light-theme[confirm-dialog]');
                $dialog.find('[cancel]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[confirm-dialog]').data('dialog').close();
                });
                $dialog.find('[validate]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[confirm-dialog]').data('dialog').validate();
                });

                var $editDialog = self.tableCont.find('[edit-dialog]');
                $editDialog.find('[cancel]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[edit-dialog]').data('dialog').close();
                });
                var editOkButton = $editDialog.find('[validate]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[edit-dialog]').data('dialog').validate();
                });

                // Edit dialog field
                var editUidField = $editDialog.find('[data-field="uid"]').textinput();
                var editUidInput = editUidField.find('input').focus(1, function() {
                    this.select();
                }).on('keypress', function(e) {
                    if (e.keyCode === 13) {
                        editOriginalInput.focus();
                    }
                });

                var editOriginalField = $editDialog.find('[data-field="original"]').textinput();
                var editOriginalInput = editOriginalField.find('textarea').focus(1, function() {
                    this.select();
                }).on('keypress', function(e) {
                    if (e.keyCode === 13) {
                        editProposalInput.focus();
                    }
                });

                var editProposalField = $editDialog.find('[data-field="proposal"]').textinput();
                var editProposalInput = editProposalField.find('textarea').focus(1, function() {
                    this.select();
                }).on('keypress', function(e) {
                    if (e.keyCode === 13) {
                        editOkButton.click();
                    }
                });

                var delButton = self.tableCont.find('#delsel').on('click', function(e: JQueryEventObject) {
                    if (delButton.is('[disabled]')) {
                        return;
                    }

                    var rows = self.dataTable.rows('.selected');
                    var datas = rows.data() as any;
                    if (datas.length === 0) {
                        return;
                    }

                    var ids = [] as Array<string>
                    for (var i = 0; i < datas.length; i++) {
                        var trans = datas[i] as ProposalData;
                        ids.push(trans._id);
                    }
                    $dialog.find('#dlg-text').html('<span class="fg-theme">' + self.translations.find('#delselrows').html() + '</span>');
                    var qstring: Array<string> = [];
                    qstring.push('d=besttrans-delete');
                    qstring.push('ids=' + encodeURIComponent(JSON.stringify(ids)));
                    request(qstring.join('&'), function(json) {
                        // Delete row
                        rows.remove();
                        rows.draw(false);
                        delButton.attr('disabled', 'disabled');
                    })
                })

                var merge = function(proposal: ProposalData, done: (error: JQueryXHR, json?: any) => void) {
                    // Post request
                    $.waitPanel.show();
                    var ajaxRequest = $.ajax({
                        url: '/admin?d=besttrans-merge&q=' + encodeURIComponent(JSON.stringify(proposal)),
                        dataType: 'json',
                        type: 'post',
                        timeout: 60000,
                        success: function(json) {
                            $.waitPanel.hide();
                            done(null, json);
                        },
                        error: function(e) {
                            $.waitPanel.hide();
                            done(e);
                        },
                    });
                }

                var edit = function(proposal: ProposalData, done: (json?: ProposalData) => void) {
                    var editDialog = $editDialog.data('dialog');
                    editedRecord = $.extend({}, proposal);

                    editUidField.textinput('value', editedRecord.uid);
                    editOriginalField.textinput('value', editedRecord.original);
                    editProposalField.textinput('value', editedRecord.proposal);

                    editDialog.validate = function() {
                        editedRecord.proposal = editProposalField.textinput('value');

                        merge(editedRecord, function(error, result) {
                            editDialog.close();
                            if (error) {
                                self.messagerControl.show('error', error);
                            } else {
                                if (result && result.error) {
                                    self.messagerControl.show('error', result.error);
                                } else {
                                    done(result);
                                }
                            }
                        })
                    }

                    $editDialog.css('opacity', 0);
                    editDialog.open();
                    editProposalInput.focus();
                    $editDialog.animate({ opacity: 1 }, 500);
                }

                var request = function(req: string, done: (json?: any) => void) {
                    var dialog = $dialog.data('dialog');
                    dialog.validate = function() {
                        // Post request
                        $.waitPanel.show();
                        var ajaxRequest = $.ajax({
                            url: '/admin?' + req,
                            dataType: 'json',
                            type: 'post',
                            timeout: 60000,
                            success: function(json) {
                                if (json && json.error) {
                                    self.messagerControl.show('error', json.error);
                                } else {
                                    done(json);
                                }
                                dialog.close();
                                $.waitPanel.hide();
                            },
                            error: function(e) {
                                if (e.status === 200) {
                                    done();
                                } else {
                                    self.messagerControl.show('error', e);
                                }
                                dialog.close();
                                $.waitPanel.hide();
                            },
                        });
                    }

                    $dialog.css('opacity', 0);
                    dialog.open();
                    $dialog.animate({ opacity: 1 }, 500);
                }

                self.$table.on('click', '[data-click]', function(e) {
                    var link = $(e.target).closest('[data-click]');
                    var func = link.attr('data-click');
                    if (func) {
                        var rowIdx = link.closest('ul').data('dtr-index');
                        var rowApi = rowIdx !== undefined ? self.dataTable.row(parseInt(rowIdx)) : self.dataTable.row(link.closest('tr'));
                        var rec = rowApi.data() as ProposalData;
                        if (func === 'edit') {
                            edit(rec, function(json) {
                                // Remove merged row
                                rowApi.remove();
                                rowApi.draw(false);
                                delButton.attr('disabled', 'disabled');
                            })

                        } else if (func === 'merge') {
                            merge(rec, function(error, json) {
                                if (error || (json && json.error)) {
                                    self.messagerControl.show('error', json || json.error);
                                } else {
                                    // Remove merged row
                                    rowApi.remove();
                                    rowApi.draw(false);
                                    delButton.attr('disabled', 'disabled');
                                }
                            })
                        }
                    }
                }).selection().on('selectionchange', function(e: datatables.SelectionEvent) {
                    if (e.value) {
                        delButton.removeAttr('disabled');
                    } else {
                        delButton.attr('disabled', 'disabled');
                    }
                })

                var getAjaxUrl = function() {
                    var query = location.query();
                    var qstring = [] as Array<string>;
                    qstring.push('d=besttrans');
                    var current = translatorControl.translatorControl('selectedValue');
                    if (current) {
                        qstring.push('cl=' + current);
                    }
                    return '/admin?' + qstring.join('&');
                }

                self.dataTable = self.tableCont.find('table').DataTable({
                    processing: true,
                    responsive: true,
                    order: [[idColumnIndex, "asc"]],
                    dom: 'lf<"maintable_toolbar">rtip',
                    ajax: {
                        url: getAjaxUrl(),
                        type: "POST"
                    },
                    columnDefs: [
                        {
                            className: 'control fg-theme',
                            orderable: false,
                            targets: expandColumnIndex,
                            render: function(data, type, row) {
                                return '<span expand class="mif-chevron-right"></span><span collapse class="mif-expand-more"></span>';
                            },
                        },
                        {
                            targets: actionsColumnIndex,
                            render: function(data, type, row) {
                                return self.tableCont.find('#actions').html();
                            },
                        },
                    ],
                    columns: [
                        {
                            name: 'default',
                            defaultContent: '',
                        },
                        {
                            name: '_id',
                            data: '_id',
                            className: 'nowrap'
                        },
                        {
                            name: 'uid',
                            data: 'uid',
                            className: 'nowrap'
                        },
                        {
                            name: 'original',
                            data: 'original',
                            type: 'string',
                            defaultContent: ''
                        },
                        {
                            name: 'locale',
                            data: 'locale',
                            type: 'string',
                            defaultContent: ''
                        },
                        {
                            name: 'proposal',
                            data: 'proposal',
                            type: 'string',
                            defaultContent: ''
                        },
                        {
                            name: 'current',
                            data: 'current',
                            type: 'string',
                            defaultContent: ''
                        },
                        {
                            name: 'userName',
                            data: 'userName',
                            type: 'string',
                            defaultContent: '',
                            className: 'nowrap'
                        },
                        {
                            name: 'action',
                            defaultContent: '',
                            className: 'nowrap'
                        },
                    ],
                    headerCallback: function(tr: Element) {
                        $(tr).find('td').addClass('fg-theme');
                    },
                });

                var toolbar = self.tableCont.find('#toolbar');
                toolbar.children().appendTo(self.tableCont.find('.maintable_toolbar'));

                var locale = translatorControl.translatorControl('selectedValue');
                setTableLanguage(locale);

                self.oncreated();
            })
        };

        protected oncreated = function() {
            var self = this as BestTransTable;
            var e = jQuery.Event("created");
            self._trigger('created', e);
        }

        public show = function() {
            var self = this as BestTransTable;
            self.tableCont.show();
            self.dataTable.columns.adjust();
            return self;
        }
    }
}

$.widget("tct.besttransTable", new tct.admin.BestTransTable())

interface JQuery {
    besttransTable(): JQuery,
    besttransTable(method: string): tct.admin.BestTransTable;
    besttransTable(method: 'show'): tct.admin.BestTransTable;
    besttransTable(method: string): JQuery;
    besttransTable(method: 'instance'): JQuery;
}

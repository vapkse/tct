declare module tct.admin {
    class DatabaseTables {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected tableCont: JQuery;
        protected messagerControl: modern.Messager;
        protected dataTable: DataTables.DataTable;
        protected _create: () => void;
        protected oncreated: () => void;
        show: () => DatabaseTables;
    }
}
interface JQuery {
    databaseTables(): JQuery;
    databaseTables(method: string): tct.admin.DatabaseTables;
    databaseTables(method: 'show'): tct.admin.DatabaseTables;
    databaseTables(method: string): JQuery;
    databaseTables(method: 'instance'): JQuery;
}

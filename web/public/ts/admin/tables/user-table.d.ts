declare module tct.admin {
    class UserTable {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected tableCont: JQuery;
        protected messagerControl: modern.Messager;
        protected dataTable: DataTables.DataTable;
        protected dialog: JQuery;
        protected _create: () => void;
        protected oncreated: () => void;
        show: () => UserTable;
        sendrequest: (req: string, done: (json?: any) => void) => void;
        showRoleMenu: (clickedElement: JQuery, role: string, userid: string) => void;
        static roleMenuClick(e: Event): void;
    }
}
interface JQuery {
    userTable(): JQuery;
    userTable(method: string): tct.admin.UserTable;
    userTable(method: 'show'): tct.admin.UserTable;
    userTable(method: string): JQuery;
    userTable(method: 'instance'): JQuery;
}

'use strict';

module tct.admin {
    export class Documents {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected tableCont: JQuery;
        protected messagerControl: modern.Messager;
        protected dataTable: DataTables.DataTable;

        protected _create = function() {
            var self = this as Documents;
            var expandColumnIndex = 0;
            var nameColumnIndex = 1;
            var sizeColumnIndex = 2;
            var linkedColumnIndex = 3;
            var linkedToColumnIndex = 4;
            var missingColumnIndex = 5;
            var actionColumnIndex = 6;

            var fileSizeString = function(bytes: number, si: boolean) {
                var thresh = si ? 1000 : 1024;
                if (Math.abs(bytes) < thresh) {
                    return bytes + ' B';
                }
                var units = si
                    ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
                    : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
                var u = -1;
                do {
                    bytes /= thresh;
                    ++u;
                } while (Math.abs(bytes) >= thresh && u < units.length - 1);
                return bytes.toFixed(1) + ' ' + units[u];
            }

            $.get('/resource?r=widgets%2Fadmin%2Fdocuments.html').done(function(html) {
                self.tableCont = $(html).appendTo(self.element);

                self.messagerControl = $('#notify').messager().data('modern-messager');

                var setTableLanguage = function(locale: string) {
                    $.get('/resource?j=ts%2Fwidgets%2Fdatatables%2Fi18n%2F' + locale + '.lang').done(function(json) {
                        var language = (<any>self.dataTable.settings())[0].oLanguage;
                        for (var field in json) {
                            language[field] = json[field];
                        }
                        self.dataTable.draw();
                    })
                }

                var translatorControl = $('#translator').on('translatorcontrolchange', function(e: modern.TranslatorControlEvent) {
                    setTableLanguage(e.value);
                })
                
                var $dialog = self.tableCont.find('#light-theme[confirm-dialog]');
                $dialog.find('[cancel]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[confirm-dialog]').data('dialog').close();
                });
                $dialog.find('[validate]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[confirm-dialog]').data('dialog').validate();
                });

                var $renameDialog = self.tableCont.find('[rename-dialog]');
                $renameDialog.find('[cancel]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[rename-dialog]').data('dialog').close();
                });
                var renameOkButton = $renameDialog.find('[validate]').on('click', function(e: JQueryEventObject) {
                    $(e.target).closest('[rename-dialog]').data('dialog').validate();
                });

                // Rename dialog field
                var renameField = $renameDialog.find('[data-field="name"]').textinput();
                var renameInput = renameField.find('input').focus(1, function() {
                    this.select();
                }).on('keypress', function(e) {
                    if (e.keyCode === 13) {
                        renameOkButton.click();
                    }
                });

                var rename = function(doc: DocumentFile, done: (json?: any) => void) {
                    var renameDialog = $renameDialog.data('dialog');

                    renameField.textinput('value', doc.name.trim());

                    renameDialog.validate = function() {
                        var dstName = renameField.textinput('value');

                        // Post request
                        $.waitPanel.show();
                        var ajaxRequest = $.ajax({
                            url: '/admin?d=docs-rename&src=' + encodeURIComponent(doc.name) + '&dst=' + encodeURIComponent(dstName),
                            dataType: 'json',
                            type: 'post',
                            timeout: 60000,
                            success: function(json) {
                                if (json && json.error) {
                                    self.messagerControl.show('error', json.error);
                                } else {
                                    done(json);
                                }
                                renameDialog.close();
                                $.waitPanel.hide();
                            },
                            error: function(e) {
                                if (e.status === 200) {
                                    done();
                                } else {
                                    self.messagerControl.show('error', e);
                                }
                                renameDialog.close();
                                $.waitPanel.hide();
                            },
                        });
                    }

                    $renameDialog.css('opacity', 0);
                    renameDialog.open();
                    renameInput.focus();
                    $renameDialog.animate({ opacity: 1 }, 500);
                }

                var request = function(req: string, done: (json?: any) => void) {
                    var dialog = $dialog.data('dialog');
                    dialog.validate = function() {
                        // Post request
                        $.waitPanel.show();
                        var ajaxRequest = $.ajax({
                            url: '/admin?' + req,
                            dataType: 'json',
                            type: 'post',
                            timeout: 60000,
                            success: function(json) {
                                if (json && json.error) {
                                    self.messagerControl.show('error', json.error);
                                } else {
                                    done(json);
                                }
                                dialog.close();
                                $.waitPanel.hide();
                            },
                            error: function(e) {
                                if (e.status === 200) {
                                    done();
                                } else {
                                    self.messagerControl.show('error', e);
                                }
                                dialog.close();
                                $.waitPanel.hide();
                            },
                        });
                    }

                    $dialog.css('opacity', 0);
                    dialog.open();
                    $dialog.animate({ opacity: 1 }, 500);
                }

                self.tableCont.find('table').on('click', '[data-click]', function(e) {
                    var link = $(e.target).closest('[data-click]');
                    if (link.is('[disabled]')) {
                        return;
                    }

                    var func = link.attr('data-click');
                    if (func) {
                        var rowIdx = link.closest('ul').data('dtr-index');
                        var rowApi = rowIdx !== undefined ? self.dataTable.row(parseInt(rowIdx)) : self.dataTable.row(link.closest('tr'));
                        var doc = rowApi.data() as DocumentFile;
                        if (func === 'rename') {
                            rename(doc, function(json: DocumentFile) {
                                if (json) {
                                    // Refresh cell                               
                                    var cell = self.dataTable.cell({ row: rowApi.index(), column: nameColumnIndex });
                                    cell.data(json.name).draw(true);
                                }
                            })
                        } else {
                            $dialog.find('#dlg-text').html('<span class="fg-theme">' + link.text() + ' ' + doc.name + '</span>');
                            var qstring: Array<string> = [];
                            qstring.push('d=docs-' + func);
                            qstring.push('name=' + doc.name);
                            request(qstring.join('&'), function(json) {
                                if (func === 'delete') {
                                    // Delete row
                                    rowApi.remove();
                                    rowApi.draw(false);
                                }
                            })
                        }
                    }
                })

                self.dataTable = self.tableCont.find('table').DataTable({
                    processing: true,
                    order: [[nameColumnIndex, 'asc']],
                    responsive: {
                        details: {
                            type: 'column'
                        }
                    },
                    ajax: {
                        url: '/admin?d=docs',
                        type: "POST"
                    },
                    columnDefs: [
                        {
                            className: 'control fg-theme',
                            orderable: false,
                            targets: expandColumnIndex,
                            render: function(data, type, row) {
                                return '<span expand class="mif-chevron-right"></span><span collapse class="mif-expand-more"></span>';
                            },
                        },
                        {
                            render: function(data, type, row) {
                                return fileSizeString(data, true);
                            },
                            targets: sizeColumnIndex
                        },
                        {
                            render: function(data, type, row) {
                                if (data) {
                                    return '<span class="mif-checkmark fg-green"></span>'
                                }
                                return '<span class="mif-stop fg-red"></span>'
                            },
                            targets: linkedColumnIndex
                        },
                        {
                            render: function(data, type, row) {
                                var doc = row as DocumentFile;
                                var sb = [] as Array<string>;
                                sb.push('<span');
                                if (doc.missing) {
                                    sb.push(' class="fg-red"');
                                }
                                sb.push('>');
                                sb.push(doc.name);
                                sb.push('</span>');
                                return sb.join('');
                            },
                            targets: nameColumnIndex
                        },
                        {
                            render: function(data, type, row) {
                                if (!data) {
                                    return '<span class="mif-stop fg-green"></span>'
                                }
                                return '<span class="mif-checkmark fg-red"></span>'
                            },
                            targets: missingColumnIndex
                        },
                        {
                            render: function(data, type, row) {
                                var link = data as Array<string>;
                                var sb = [] as Array<string>;
                                sb.push('<ul>');
                                if (link) {
                                    link.forEach(function(value) {
                                        sb.push('<li>' + value + '</li>');
                                    })
                                }
                                sb.push('</ul>');
                                return sb.join('');
                            },
                            targets: linkedToColumnIndex
                        },
                        {
                            render: function(data, type, row) {
                                var actions = self.tableCont.find('#actions');
                                var doc = row as DocumentFile;
                                if (doc.linked) {
                                    actions.find('[data-click="delete"]').attr('disabled', 'disabled');
                                } else {
                                    actions.find('[data-click="delete"]').removeAttr('disabled');
                                }
                                return actions.html();
                            },
                            targets: actionColumnIndex
                        },
                    ],
                    columns: [
                        {
                            defaultContent: '',
                        },
                        {
                            data: 'name',
                            type: 'string',
                            defaultContent: '',
                            className: 'nowrap',
                            responsivePriority: 1,
                        },
                        {
                            data: 'size',
                            type: 'number',
                            defaultContent: '0',
                            className: 'nowrap',
                            responsivePriority: 5,
                        },
                        {
                            data: 'linked',
                            type: 'boolean',
                            defaultContent: 'true',
                            className: 'align-center',
                            responsivePriority: 2,
                        },
                        {
                            data: 'linkedTo',
                            defaultContent: '',
                            className: 'nowrap',
                            responsivePriority: 6,
                        },
                        {
                            data: 'missing',
                            type: 'boolean',
                            defaultContent: 'false',
                            className: 'align-center',
                            responsivePriority: 4,
                        },
                        {
                            defaultContent: '',
                            responsivePriority: 3,
                        },
                    ],
                    headerCallback: function(tr: Element) {
                        $(tr).find('td').addClass('fg-theme');
                    },
                });

                var locale = translatorControl.translatorControl('selectedValue');
                setTableLanguage(locale);

                self.oncreated();                
            })
        };

        protected oncreated = function() {
            var self = this as Documents;
            var e = jQuery.Event("created");
            self._trigger('created', e);
        }

        public show = function() {
            var self = this as Documents;
            self.tableCont.show();
            self.dataTable.columns.adjust();
            return self;
        }
    }
}

$.widget("tct.documents", new tct.admin.Documents())

interface JQuery {
    documents(): JQuery,
    documents(method: string): tct.admin.Documents;
    documents(method: 'show'): tct.admin.Documents;
    documents(method: string): JQuery;
    documents(method: 'instance'): JQuery;
}

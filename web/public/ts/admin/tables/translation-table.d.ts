declare module tct.admin {
    class TransTable {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected tableCont: JQuery;
        protected messagerControl: modern.Messager;
        protected dataTable: DataTables.DataTable;
        protected translations: JQuery;
        protected $table: JQuery;
        protected _create: () => void;
        protected oncreated: () => void;
        show: () => TransTable;
    }
}
interface JQuery {
    transTable(): JQuery;
    transTable(method: string): tct.admin.TransTable;
    transTable(method: 'show'): tct.admin.TransTable;
    transTable(method: string): JQuery;
    transTable(method: 'instance'): JQuery;
}

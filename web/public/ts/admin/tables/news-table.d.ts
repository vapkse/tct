declare module tct.admin {
    class NewsTable {
        protected element: JQuery;
        protected _trigger: (eventName: string, event: JQueryEventObject) => void;
        protected tableCont: JQuery;
        protected messagerControl: modern.Messager;
        protected dataTable: DataTables.DataTable;
        protected _create: () => void;
        protected oncreated: () => void;
        show: () => NewsTable;
    }
}
interface JQuery {
    newsTable(): JQuery;
    newsTable(method: string): tct.admin.NewsTable;
    newsTable(method: 'show'): tct.admin.NewsTable;
    newsTable(method: string): JQuery;
    newsTable(method: 'instance'): JQuery;
}

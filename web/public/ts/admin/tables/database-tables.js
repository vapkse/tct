'use strict';
var tct;
(function (tct) {
    var admin;
    (function (admin) {
        var DatabaseTables = (function () {
            function DatabaseTables() {
                this._create = function () {
                    var self = this;
                    var expandColumnIndex = 0;
                    var nameColumnIndex = 1;
                    var sizeColumnIndex = 2;
                    var versionColumnIndex = 3;
                    var actionsColumnIndex = 4;
                    var fileSizeString = function (bytes, si) {
                        var thresh = si ? 1000 : 1024;
                        if (Math.abs(bytes) < thresh) {
                            return bytes + ' B';
                        }
                        var units = si
                            ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
                            : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
                        var u = -1;
                        do {
                            bytes /= thresh;
                            ++u;
                        } while (Math.abs(bytes) >= thresh && u < units.length - 1);
                        return bytes.toFixed(1) + ' ' + units[u];
                    };
                    $.get('/resource?r=widgets%2Fadmin%2Fdatabase-tables.html').done(function (html) {
                        self.tableCont = $(html).appendTo(self.element);
                        self.messagerControl = $('#notify').messager().data('modern-messager');
                        var setTableLanguage = function (locale) {
                            $.get('/resource?j=ts%2Fwidgets%2Fdatatables%2Fi18n%2F' + locale + '.lang').done(function (json) {
                                var language = self.dataTable.settings()[0].oLanguage;
                                for (var field in json) {
                                    language[field] = json[field];
                                }
                                self.dataTable.draw();
                            });
                        };
                        var translatorControl = $('#translator').on('translatorcontrolchange', function (e) {
                            setTableLanguage(e.value);
                        });
                        var $dialog = self.tableCont.find('#light-theme[confirm-dialog]');
                        $dialog.find('[cancel]').on('click', function (e) {
                            $(e.target).closest('[confirm-dialog]').data('dialog').close();
                        });
                        $dialog.find('[validate]').on('click', function (e) {
                            $(e.target).closest('[confirm-dialog]').data('dialog').validate();
                        });
                        var request = function (req, done) {
                            var dialog = $dialog.data('dialog');
                            dialog.validate = function () {
                                $.waitPanel.show();
                                var ajaxRequest = $.ajax({
                                    url: '/admin?' + req,
                                    dataType: 'json',
                                    type: 'post',
                                    timeout: 60000,
                                    success: function (json) {
                                        if (json && json.error) {
                                            self.messagerControl.show('error', json.error);
                                        }
                                        else {
                                            done(json);
                                        }
                                        dialog.close();
                                        $.waitPanel.hide();
                                    },
                                    error: function (e) {
                                        if (e.status === 200) {
                                            done();
                                        }
                                        else {
                                            self.messagerControl.show('error', e);
                                        }
                                        dialog.close();
                                        $.waitPanel.hide();
                                    },
                                });
                            };
                            $dialog.css('opacity', 0);
                            dialog.open();
                            $dialog.animate({ opacity: 1 }, 500);
                        };
                        self.tableCont.find('table').on('click', '[data-click]', function (e) {
                            var link = $(e.target).closest('[data-click]');
                            var func = link.attr('data-click');
                            if (func) {
                                var rowIdx = link.closest('ul').data('dtr-index');
                                var row = rowIdx !== undefined ? self.dataTable.row(parseInt(rowIdx)) : self.dataTable.row(link.closest('tr'));
                                var table = row.data();
                                $dialog.find('#dlg-text').html('<span class="fg-theme">' + link.text() + ' ' + table.name + '</span>');
                                var qstring = [];
                                qstring.push('d=table-' + func);
                                qstring.push('name=' + table.name);
                                request(qstring.join('&'), function (json) {
                                    if (func === 'compress') {
                                        table.size = json.size;
                                        var cell = self.dataTable.cell({ row: row.index(), column: sizeColumnIndex });
                                        cell.data(table.size).draw(true);
                                    }
                                });
                            }
                        });
                        self.dataTable = self.tableCont.find('table').DataTable({
                            processing: true,
                            order: [[nameColumnIndex, 'asc']],
                            responsive: {
                                details: {
                                    type: 'column'
                                }
                            },
                            ajax: {
                                url: '/admin?d=tables',
                                type: "POST"
                            },
                            columnDefs: [
                                {
                                    className: 'control fg-theme',
                                    orderable: false,
                                    targets: expandColumnIndex,
                                    render: function (data, type, row) {
                                        return '<span expand class="mif-chevron-right"></span><span collapse class="mif-expand-more"></span>';
                                    },
                                },
                                {
                                    render: function (data, type, row) {
                                        return fileSizeString(data, true);
                                    },
                                    targets: sizeColumnIndex
                                },
                                {
                                    render: function (data, type, row) {
                                        return data ? data.major + '.' + data.minor : '';
                                    },
                                    targets: versionColumnIndex
                                },
                                {
                                    targets: actionsColumnIndex,
                                    render: function (data, type, row) {
                                        return self.tableCont.find('#actions').html();
                                    },
                                },
                            ],
                            columns: [
                                {
                                    defaultContent: '',
                                },
                                {
                                    data: 'name',
                                    type: 'string',
                                    defaultContent: '',
                                    className: 'nowrap'
                                },
                                {
                                    data: 'size',
                                    type: 'number',
                                    defaultContent: '0',
                                    className: 'nowrap'
                                },
                                {
                                    data: 'version',
                                    type: 'string',
                                    defaultContent: '',
                                    className: 'nowrap'
                                },
                                {
                                    defaultContent: '',
                                    className: 'nowrap'
                                },
                            ],
                            headerCallback: function (tr) {
                                $(tr).find('td').addClass('fg-theme');
                            },
                        });
                        var locale = translatorControl.translatorControl('selectedValue');
                        setTableLanguage(locale);
                        self.oncreated();
                    });
                };
                this.oncreated = function () {
                    var self = this;
                    var e = jQuery.Event("created");
                    self._trigger('created', e);
                };
                this.show = function () {
                    var self = this;
                    self.tableCont.show();
                    self.dataTable.columns.adjust();
                    return self;
                };
            }
            return DatabaseTables;
        })();
        admin.DatabaseTables = DatabaseTables;
    })(admin = tct.admin || (tct.admin = {}));
})(tct || (tct = {}));
$.widget("tct.databaseTables", new tct.admin.DatabaseTables());

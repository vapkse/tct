'use strict';
var tct;
(function (tct) {
    var admin;
    (function (admin) {
        var TubesTable = (function () {
            function TubesTable() {
                this._create = function () {
                    var self = this;
                    var expandColumnIndex = 0;
                    var idColumnIndex = 2;
                    var nameColumnIndex = 2;
                    var typeColumnIndex = 3;
                    var actionsColumnIndex = 4;
                    $.get('/resource?r=widgets%2Fadmin%2Ftubes-table.html').done(function (html) {
                        self.tableCont = $(html).appendTo(self.element);
                        self.messagerControl = $('#notify').messager().data('modern-messager');
                        var setTableLanguage = function (locale) {
                            $.get('/resource?j=ts%2Fwidgets%2Fdatatables%2Fi18n%2F' + locale + '.lang').done(function (json) {
                                var language = self.dataTable.settings()[0].oLanguage;
                                for (var field in json) {
                                    language[field] = json[field];
                                }
                                self.dataTable.draw();
                            });
                        };
                        var translatorControl = $('#translator').on('translatorcontrolchange', function (e) {
                            setTableLanguage(e.value);
                        });
                        var $dialog = self.tableCont.find('#light-theme[confirm-dialog]');
                        $dialog.find('[cancel]').on('click', function (e) {
                            $(e.target).closest('[confirm-dialog]').data('dialog').close();
                        });
                        $dialog.find('[validate]').on('click', function (e) {
                            $(e.target).closest('[confirm-dialog]').data('dialog').validate();
                        });
                        var request = function (req, done) {
                            var dialog = $dialog.data('dialog');
                            dialog.validate = function () {
                                $.waitPanel.show();
                                var ajaxRequest = $.ajax({
                                    url: '/admin?' + req,
                                    dataType: 'json',
                                    type: 'post',
                                    timeout: 60000,
                                    success: function (json) {
                                        if (json && json.error) {
                                            self.messagerControl.show('error', json.error);
                                        }
                                        else {
                                            done(json);
                                        }
                                        dialog.close();
                                        $.waitPanel.hide();
                                    },
                                    error: function (e) {
                                        if (e.status === 200) {
                                            done();
                                        }
                                        else {
                                            self.messagerControl.show('error', e);
                                        }
                                        dialog.close();
                                        $.waitPanel.hide();
                                    },
                                });
                            };
                            $dialog.css('opacity', 0);
                            dialog.open();
                            $dialog.animate({ opacity: 1 }, 500);
                        };
                        self.tableCont.find('table').on('click', '[data-click]', function (e) {
                            var link = $(e.target).closest('[data-click]');
                            var func = link.attr('data-click');
                            if (func) {
                                var rowIdx = link.closest('ul').data('dtr-index');
                                var rowApi = rowIdx !== undefined ? self.dataTable.row(parseInt(rowIdx)) : self.dataTable.row(link.closest('tr'));
                                var tube = rowApi.data();
                                $dialog.find('#dlg-text').html('<span class="fg-theme">' + link.text() + ' ' + tube._id + '</span><br>' + tube.name + ' ' + (tube.type || ''));
                                var qstring = [];
                                qstring.push('d=tube-' + func);
                                qstring.push('id=' + tube._id);
                                request(qstring.join('&'), function (json) {
                                    if (func === 'delete') {
                                        rowApi.remove();
                                        rowApi.draw(false);
                                    }
                                });
                            }
                        });
                        self.dataTable = self.tableCont.find('table').DataTable({
                            processing: true,
                            responsive: true,
                            dom: 'lf<"maintable_toolbar">rtip',
                            ajax: {
                                url: '/admin?d=tubes',
                                type: "POST"
                            },
                            columnDefs: [
                                {
                                    className: 'control fg-theme',
                                    orderable: false,
                                    targets: expandColumnIndex,
                                    render: function (data, type, row) {
                                        return '<span expand class="mif-chevron-right"></span><span collapse class="mif-expand-more"></span>';
                                    },
                                },
                                {
                                    targets: actionsColumnIndex,
                                    render: function (data, type, row) {
                                        return self.tableCont.find('#actions').html();
                                    },
                                }
                            ],
                            columns: [
                                {
                                    defaultContent: '',
                                },
                                {
                                    data: '_id'
                                },
                                {
                                    data: 'name',
                                    type: 'string',
                                    defaultContent: '',
                                    className: 'nowrap'
                                },
                                {
                                    data: 'type',
                                    type: 'string',
                                    defaultContent: '',
                                    className: 'nowrap'
                                },
                                {
                                    defaultContent: '',
                                    className: 'nowrap'
                                },
                            ],
                            headerCallback: function (tr) {
                                $(tr).find('td').addClass('fg-theme');
                            },
                        });
                        var toolbar = self.tableCont.find('#toolbar');
                        toolbar.children().appendTo(self.tableCont.find('.maintable_toolbar'));
                        var locale = translatorControl.translatorControl('selectedValue');
                        setTableLanguage(locale);
                        self.oncreated();
                    });
                };
                this.oncreated = function () {
                    var self = this;
                    var e = jQuery.Event("created");
                    self._trigger('created', e);
                };
                this.show = function () {
                    var self = this;
                    self.tableCont.show();
                    self.dataTable.columns.adjust();
                    return self;
                };
            }
            return TubesTable;
        })();
        admin.TubesTable = TubesTable;
    })(admin = tct.admin || (tct.admin = {}));
})(tct || (tct = {}));
$.widget("tct.tubesTable", new tct.admin.TubesTable());

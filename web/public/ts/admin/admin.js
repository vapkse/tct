'use strict';
var tct;
(function (tct) {
    var admin;
    (function (admin) {
        (function ($) {
            $.StartScreen = function () {
                var settingsPanel;
                var pageId = $('[translate]').attr('translate');
                var body = $('body');
                var mainContent = $('#main-content');
                var tableWrapper = mainContent.find('#table-wrapper');
                var transArea = $('#trans-area');
                var translatorControl = transArea.find('#translator').translatorControl().data('modern-translatorControl');
                var messagerControl = $('#messager').messager().data('modern-messager');
                var tooltipManager = $('#container').tooltipmanager().data('modern-tooltipmanager');
                var translations = $('translations');
                var huser = $('#user').html();
                var user = huser && JSON.parse(huser);
                var userInfosPanel = body.find('#user-infos-panel').userInfos({
                    user: user
                }).bind('userinfoscreated', function () {
                    transArea.animate({ opacity: 1 }, 500);
                });
                $('button[home]').on('click', function () {
                    var searchparams = location.query()['rl'];
                    location.href = '/' + (searchparams ? searchparams : '');
                });
                $('#sidebar').on('click', 'li', function () {
                    var disp = $(this).closest('[data-disp]');
                    $.pageState.replaceUrlParam('d', disp.attr('data-disp'));
                    loadFromUrl();
                });
                $('button[settings-panel]').on('click', function () {
                    if (!settingsPanel) {
                        requirejs(['settingsPanel'], function (settingsPanel) {
                            settingsPanel = $('#settings-panel');
                            var settings = new tct.Settings(localStorage);
                            settingsPanel.settingsPanel(settings).bind('settingspanelchange', function (e, data) {
                                if (data.colorSettings.scheme) {
                                    body.attr('id', 'scheme-' + data.colorSettings.scheme);
                                }
                                if (data.colorSettings.theme) {
                                    body.attr('theme', data.colorSettings.theme);
                                }
                            });
                            settingsPanel.settingsPanel('toogle');
                        });
                    }
                    else {
                        settingsPanel.settingsPanel('toogle');
                    }
                });
                var showError = function (errorMessage) {
                    var msg = {
                        type: 'error',
                        name: 'msg-fileuploaderror',
                        message: 'File Upload error: \\0',
                        params: { 0: errorMessage }
                    };
                    $.translator.translateMessages([msg], function (translated) {
                        messagerControl.showMessages(translated);
                    });
                    console.error(errorMessage);
                };
                var loadFromUrl = function () {
                    var qstring = location.query();
                    var disp = qstring['d'];
                    if (!disp) {
                        disp = 'best';
                        $.pageState.replaceUrlParam('d', disp);
                    }
                    var li = mainContent.find('#sidebar li').removeClass('active').filter('[data-disp="' + disp + '"]').addClass('active');
                    var icon = li.find('.icon');
                    var title = li.find('.title');
                    mainContent.find('#table-title lang').text(title.text()).attr('uid', title.attr('uid'));
                    mainContent.find('#table-title span').attr('class', 'place-right ' + icon.attr('class'));
                    tableWrapper.find('.table-cont').hide();
                    switch (disp) {
                        case 'users':
                            if (tableWrapper.userTable('instance')) {
                                tableWrapper.userTable('show');
                            }
                            else {
                                tableWrapper.userTable();
                            }
                            break;
                        case 'news':
                            if (tableWrapper.newsTable('instance')) {
                                tableWrapper.newsTable('show');
                            }
                            else {
                                tableWrapper.newsTable();
                            }
                            break;
                        case 'tables':
                            if (tableWrapper.databaseTables('instance')) {
                                tableWrapper.databaseTables('show');
                            }
                            else {
                                tableWrapper.databaseTables();
                            }
                            break;
                        case 'docs':
                            if (tableWrapper.documents('instance')) {
                                tableWrapper.documents('show');
                            }
                            else {
                                tableWrapper.documents();
                            }
                            break;
                        case 'trans':
                            if (tableWrapper.transTable('instance')) {
                                tableWrapper.transTable('show');
                            }
                            else {
                                tableWrapper.transTable();
                            }
                            break;
                        case 'best':
                            if (tableWrapper.besttransTable('instance')) {
                                tableWrapper.besttransTable('show');
                            }
                            else {
                                tableWrapper.besttransTable();
                            }
                            break;
                        case 'tubes':
                            if (tableWrapper.tubesTable('instance')) {
                                tableWrapper.tubesTable('show');
                            }
                            else {
                                tableWrapper.tubesTable();
                            }
                            break;
                    }
                    $.waitPanel.hide();
                };
                setTimeout(function () {
                    loadFromUrl();
                }, 1);
                $(window).on('popstate', function (e) {
                    var event = e.originalEvent;
                    if (event && event.state && event.state.j) {
                    }
                });
            };
        })(jQuery);
        $(function () {
            if (location.hostname === '127.0.0.1') {
                var browserVersion = window.browserVersion();
                if (browserVersion.version > 9) {
                    $('head').append('<script type="text/javascript" src="http://127.0.0.1:38080/livereload.js"><\/script>');
                }
            }
            $.StartScreen();
        });
    })(admin = tct.admin || (tct.admin = {}));
})(tct || (tct = {}));

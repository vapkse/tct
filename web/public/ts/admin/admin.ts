'use strict';

module tct.admin {
    (function($: JQueryStatic) {
        $.StartScreen = function() {
            var settingsPanel: JQuery;
            var pageId = $('[translate]').attr('translate');
            var body = $('body');
            var mainContent = $('#main-content');
            var tableWrapper = mainContent.find('#table-wrapper');

            // Translation area visible only after the user control is ready
            var transArea = $('#trans-area');
            var translatorControl: modern.TranslatorControl = transArea.find('#translator').translatorControl().data('modern-translatorControl');
            var messagerControl: modern.Messager = $('#messager').messager().data('modern-messager');
            var tooltipManager: modern.TooltipManager = $('#container').tooltipmanager().data('modern-tooltipmanager');
            var translations = $('translations');

            // User infos panel
            var huser = $('#user').html();
            var user: user.User = huser && JSON.parse(huser);
            var userInfosPanel = body.find('#user-infos-panel').userInfos({
                user: user
            }).bind('userinfoscreated', function() {
                transArea.animate({ opacity: 1 }, 500);
            });

            $('button[home]').on('click', function() {
                var searchparams = location.query()['rl'];
                location.href = '/' + (searchparams ? searchparams : '');
            })

            $('#sidebar').on('click', 'li', function() {
                var disp = $(this).closest('[data-disp]');
                $.pageState.replaceUrlParam('d', disp.attr('data-disp'));
                loadFromUrl();
            })

            // Bind settings panel button
            $('button[settings-panel]').on('click', function() {
                // Instianciate settings panel if the button is pressed for the first time
                if (!settingsPanel) {
                    requirejs(['settingsPanel'], function(settingsPanel: JQuery) {
                        settingsPanel = $('#settings-panel');
                        var settings = new tct.Settings(localStorage);
                        settingsPanel.settingsPanel(settings).bind('settingspanelchange', function(e: tct.SettingsPanelEvent, data: tct.SettingsPanelOptions) {
                            if (data.colorSettings.scheme) {
                                body.attr('id', 'scheme-' + data.colorSettings.scheme);
                            }
                            if (data.colorSettings.theme) {
                                body.attr('theme', data.colorSettings.theme);
                            }
                        });
                        settingsPanel.settingsPanel('toogle');
                    })
                } else {
                    settingsPanel.settingsPanel('toogle');
                }
            });

            var showError = function(errorMessage: string) {
                var msg: modern.MessagerMessage = {
                    type: 'error',
                    name: 'msg-fileuploaderror',
                    message: 'File Upload error: \\0',
                    params: { 0: errorMessage }
                }
                $.translator.translateMessages([msg], function(translated: Array<translator.Message>) {
                    messagerControl.showMessages(translated);
                });
                console.error(errorMessage);
            }

            var loadFromUrl = function() {
                var qstring = location.query();
                var disp = qstring['d'];
                if (!disp) {
                    disp = 'best';
                    $.pageState.replaceUrlParam('d', disp);
                }
                var li = mainContent.find('#sidebar li').removeClass('active').filter('[data-disp="' + disp + '"]').addClass('active');
                var icon = li.find('.icon');
                var title = li.find('.title');

                // Copy title and icon
                mainContent.find('#table-title lang').text(title.text()).attr('uid', title.attr('uid'));
                mainContent.find('#table-title span').attr('class', 'place-right ' + icon.attr('class'));

                tableWrapper.find('.table-cont').hide();
                switch (disp) {
                    case 'users':
                        if (tableWrapper.userTable('instance')) {
                            tableWrapper.userTable('show');
                        } else {
                            tableWrapper.userTable();
                        }
                        break;

                    case 'news':
                        if (tableWrapper.newsTable('instance')) {
                            tableWrapper.newsTable('show');
                        } else {
                            tableWrapper.newsTable();
                        }
                        break;

                    case 'tables':
                        if (tableWrapper.databaseTables('instance')) {
                            tableWrapper.databaseTables('show');
                        } else {
                            tableWrapper.databaseTables();
                        }
                        break;

                    case 'docs':
                        if (tableWrapper.documents('instance')) {
                            tableWrapper.documents('show');
                        } else {
                            tableWrapper.documents();
                        }
                        break;

                    case 'trans':
                        if (tableWrapper.transTable('instance')) {
                            tableWrapper.transTable('show');
                        } else {
                            tableWrapper.transTable();
                        }
                        break;

                    case 'best':
                        if (tableWrapper.besttransTable('instance')) {
                            tableWrapper.besttransTable('show');
                        } else {
                            tableWrapper.besttransTable();
                        }
                        break;

                    case 'tubes':
                        if (tableWrapper.tubesTable('instance')) {
                            tableWrapper.tubesTable('show');
                        } else {
                            tableWrapper.tubesTable();
                        }
                        break;
                }

                /*$.waitPanel.show();
                var tubeid = qstring['id'];
                var userid = qstring['userid'];
                var reqstring: Array<string> = [];
                reqstring.push('id=' + tubeid);
                if (userid) {
                    reqstring.push('userid=' + userid);
                }
                var ajaxRequest = $.ajax({
                    url: '/admin?' + reqstring.join('&'),
                    type: 'post',
                    dataType: 'json',
                    timeout: 60000,
                    success: function(json) {
                        if (json.error) {
                            messagerControl.show('error', json.error);
                        } else {
                            loadJson(json);
                            $.pageState.replaceOrAddState(json);
                            $.waitPanel.hide();
                        }
                    },
                    error: function(e) {
                        if (e.statusText !== 'canceled') {
                            messagerControl.show('error', e);
                            loadJson(null);
                        }
                        $.waitPanel.hide();
                    },
                });*/

                $.waitPanel.hide();
            }

            // Ask server for datas
            setTimeout(function() {
                loadFromUrl();
            }, 1);

            $(window).on('popstate', function(e) {
                var event = <PopStateEvent>e.originalEvent;
                if (event && event.state && event.state.j) {
                    //loadJson(event.state.j);
                }
            });
        };
    })(jQuery);

    $(function() {
        if (location.hostname === '127.0.0.1') {
            var browserVersion = window.browserVersion();
            if (browserVersion.version > 9) {
                $('head').append('<script type="text/javascript" src="http://127.0.0.1:38080/livereload.js"><\/script>');
            }
        }

        $.StartScreen();
    });
}

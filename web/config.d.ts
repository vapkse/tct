/// <reference path="server/typings/node/node.d.ts" />
declare var config: {
    maxFailLoginAttempts: number;
    path: {
        base: string;
        data: any;
        backup: any;
        views: any;
        app: any;
        server: any;
        common: any;
        test: any;
        temp: any;
        json: any;
        upload: any;
        documents: any;
    };
    dev: {
        listenPort: number;
        livereload: number;
        debugPort: number;
    };
    prod: {
        listenPort: number;
        livereload: boolean;
    };
    db: {
        engine: string;
        importPath: any;
        mongo: {
            host: string;
            port: number;
            db: string;
            opts: {
                auto_reconnect: boolean;
                safe: boolean;
            };
        };
        tingo: {
            path: any;
            opts: {};
        };
    };
    auth: {
        prod: {
            google: {
                id: string;
                secret: string;
                redirect: string;
            };
            facebook: {
                id: string;
                secret: string;
                redirect: string;
            };
            twitter: {
                consumerKey: string;
                consumerSecret: string;
                redirect: string;
            };
            github: {
                id: string;
                secret: string;
                redirect: string;
            };
            local: {
                validationUrl: string;
                declineUrl: string;
                changePwdUrl: string;
                unlockUrl: string;
            };
        };
        dev: {
            google: {
                id: string;
                secret: string;
                redirect: string;
            };
            facebook: {
                id: string;
                secret: string;
                redirect: string;
            };
            twitter: {
                consumerKey: string;
                consumerSecret: string;
                redirect: string;
            };
            github: {
                id: string;
                secret: string;
                redirect: string;
            };
            local: {
                validationUrl: string;
                declineUrl: string;
                changePwdUrl: string;
                unlockUrl: string;
            };
        };
    };
};
export = config;
